# Sage 50 Web API SDK - HyperAccounts SDK

The HyperAccounts API SDK provides an easy-to-use interface for handling the Sage 50 Accounts HyperAccounts web API, version 2 and onwards.
If you are interested in gaining access to the API, [contact Hyperext directly](https://www.hyperext.com/contact-us/).
This tool has been developed to simplify the process of making HTTP requests to the web API, abstracting away the complexity of the underlying technology.
Additionally, the SDK provides models for both the request body and the API's response, streamlining the development process.

## Usage

  ```php
$request = ClientConstructor::construct($url,$username,$password);

$query = SalesInvoicesRequestBuilderGetRequestConfiguration::addQueryParameters(null,null,'items',null,'3');
$requestConfiguration = new SalesInvoicesRequestBuilderGetRequestConfiguration(null,null,$query);

$request->api()->v2()->salesInvoices()->get($requestConfiguration)->then(

        function (SalesInvoiceCollection $response) {
            foreach ($response->getData() as $data){
                echo $data->getInvoiceNumber();
                echo $data->getAttributes()->getAddress1();
                echo $data->getIncluded()->getItems()->getData()[0]->getRelationships()->getStock()->getData()->getStockCode();              
            }
        },
        function ($exception) {
            // Gets full API error but will throw a php error if it gets a CURL error
            echo $exception->getResponse()->getBody()->getContents();   
            
            // gets CURL errors but will truncate API errors
            echo $exception->getMessage();

        }

);

  ```
