<?php
require_once 'vendor/autoload.php';


use HyperAccountsV2Sdk\ClientConstructor;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Customers\CustomersRequestBuilderGetRequestConfiguration;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\SalesInvoices\Item\WithInvoiceNumberItemRequestBuilderGetRequestConfiguration;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\SalesInvoices\Item\WithInvoiceNumberItemRequestBuilderPatchRequestConfiguration;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\SalesInvoices\SalesInvoicesRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\SalesInvoices\SalesInvoicesRequestBuilderGetRequestConfiguration;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\SalesInvoices\SalesInvoicesRequestBuilderPostRequestConfiguration;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\SalesOrders\SalesOrdersRequestBuilderPostRequestConfiguration;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\GetHyperAccountsV2ApiClient;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models\BankCollection;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models\CustomerCollection;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models\CustomerRelatedRequired;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models\CustomerRelatedRequiredRelationshipWrite;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models\NominalRelatedRequired;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models\NominalRelatedRequiredRelationshipWrite;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models\SalesInvoiceAttributesEdit;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models\SalesInvoiceAttributesEdit_globalTaxCode;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models\SalesInvoiceAttributesWrite;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models\SalesInvoiceAttributesWrite_globalTaxCode;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models\SalesInvoiceCollection;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models\SalesInvoiceGetDto;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models\SalesInvoiceItemAttributesWrite;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models\SalesInvoiceItemPostDto;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models\SalesInvoiceItemRelationshipsWrite;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models\SalesInvoicePatchDto;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models\SalesInvoicePostDto;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models\SalesInvoiceRelationshipsWrite;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models\SalesInvoiceReliantChilds;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models\SalesOrderAttributesWrite;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models\SalesOrderGetDto;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models\SalesOrderItemAttributesWrite;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models\SalesOrderItemPostDto;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models\SalesOrderItemRelationshipsWrite;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models\SalesOrderPostDto;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models\SalesOrderRelationshipsWrite;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models\SalesOrderReliantChilds;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models\StockRelatedRequired;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models\StockRelatedRequiredRelationshipWrite;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\SalesOrders\Item\Actions\FullyAllocate\FullyAllocateRequestBuilderPostRequestConfiguration;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

//$request = ClientConstructor::construct('https://sbx.hypersage.co.uk:29000','V2FreeSandbox','V2Token');
$request = ClientConstructor::construct('https://magneto.hypersage.co.uk:28001','ONTrack','1xC8?L307-7$Uu_Q');

//getAllInvoices($request);
//getSingleInvoice($request);

//postInvoice($request);

//postOrder($request);

//patchInvoice($request);

allocateOrder($request);

// example of get all invoices
function getAllInvoices(GetHyperAccountsV2ApiClient $request)
{
    // 8
    //$query = SalesInvoicesRequestBuilderGetRequestConfiguration::addQueryParameters(included: 'items',offset: 3);
    $query = SalesInvoicesRequestBuilderGetRequestConfiguration::addQueryParameters(null,null,'items',null,'3');

    //8
    //$requestConfiguration = new SalesInvoicesRequestBuilderGetRequestConfiguration(queryParameters: $query);
    $requestConfiguration = new SalesInvoicesRequestBuilderGetRequestConfiguration(null,null,$query);

    $request->api()->v2()->salesInvoices()->get($requestConfiguration)->then(

        function (SalesInvoiceCollection $response) {
            // Handle the response here
            echo $response->getMeta()->getCount();

            foreach ($response->getData() as $data)
            {
                error_log($data->getIncluded()->getItems()->getData()[0]->getRelationships()->getStock()->getData()->getStockCode());
                error_log($data->getAttributes()->getAddress1());
                error_log($data->getInvoiceNumber());
                //error_log($data->getIncludes()->getItems()->getData()[0]->getAttributes()->getQuantity());
                //error_log($data->getLinks());
                //error_log($data->getRelationships()->getCustomer()->getData()->getAccountRef());
            }
        },
        function ($exception) {
            // Handle the exception here
            //error_log($exception->getMessage());

            error_log($exception->getResponse()->getBody()->getContents());
        }

    );
}

// get all customers
function getAllCustomers(GetHyperAccountsV2ApiClient $request)
{
    // 8
    //$query = SalesInvoicesRequestBuilderGetRequestConfiguration::addQueryParameters(included: 'items',offset: 3);
    $query = CustomersRequestBuilderGetRequestConfiguration::addQueryParameters(null,null,'items',null,'3');

    //8
    //$requestConfiguration = new SalesInvoicesRequestBuilderGetRequestConfiguration(queryParameters: $query);
    $requestConfiguration = new CustomersRequestBuilderGetRequestConfiguration(null,null,$query);

    $request->api()->v2()->customers()->get($requestConfiguration)->then(

        function (SalesInvoiceCollection $response) {
            // Handle the response here
            echo $response->getMeta()->getCount();

            echo json_encode_private($response);

            foreach ($response->getData() as $data)
            {
                //error_log($data->getAttributes()->getAddress1());
                //error_log($data->getInvoiceNumber());
                //error_log($data->getIncludes()->getItems()->getData()[0]->getAttributes()->getQuantity());
                //error_log($data->getLinks());
                //error_log($data->getRelationships()->getCustomer()->getData()->getAccountRef());
            }
        },
        function ($exception) {
            // Handle the exception here
            //error_log($exception->getMessage());

            error_log($exception->getResponse()->getBody()->getContents());
        }

    );
}

// example of get single invoice
/*
*/
function getSingleInvoice(GetHyperAccountsV2ApiClient $request)
{
    $query = WithInvoiceNumberItemRequestBuilderGetRequestConfiguration::addQueryParameters(null,null,'items');
    $requestConfiguration = new WithInvoiceNumberItemRequestBuilderGetRequestConfiguration(null,null,$query);

    $request->api()->v2()->salesInvoicesById("4")->get($requestConfiguration)->then(

        function (SalesInvoiceGetDto $data) {
            // Handle the response here

            error_log($data->getInvoiceNumber());
            error_log($data->getIncluded()->getItems()->getData()[0]->getRelationships()->getStock()->getData()->getStockCode());
            error_log($data->getAttributes()->getCAddress1());
        },
        function ($exception) {
            // Handle the exception here
            error_log($exception->getResponse()->getBody()->getContents());
        }

    );
}

// example of posting a new invoice
function postInvoice(GetHyperAccountsV2ApiClient $request)
{
    $query = new SalesInvoicesRequestBuilderPostRequestConfiguration();
    $attributes = new SalesInvoiceAttributesWrite();
    $relationships = new SalesInvoiceRelationshipsWrite();
    $reliantChilds = new SalesInvoiceReliantChilds();

    // set account ref
    $customerRelatedRequired = new CustomerRelatedRequired();
    $customerRelatedRequired->setAccountRef('A1D001');

    $customer = new CustomerRelatedRequiredRelationshipWrite();
    $customer->setData($customerRelatedRequired);

    $relationships->setCustomer($customer);


    // set items
    $items = [];

    $item = new SalesInvoiceItemPostDto();

    $itemAttribute = new SalesInvoiceItemAttributesWrite();
    $itemAttribute->setDescription('Description4Item');
    $item->setAttributes($itemAttribute);

    $itemRelationship = new SalesInvoiceItemRelationshipsWrite();
    $stockData = new StockRelatedRequired();
    $stockData->setStockCode('BOARD001');
    $stockRelationship= new StockRelatedRequiredRelationshipWrite();
    $stockRelationship->setData($stockData);
    $itemRelationship->setStock($stockRelationship);

    $nominalRelationship= new NominalRelatedRequiredRelationshipWrite();
    $nominalData = new NominalRelatedRequired();
    $nominalData->setAccountRef('0011');
    $nominalRelationship->setData($nominalData);
    $itemRelationship->setNominalCode($nominalRelationship);

    $item->setRelationships($itemRelationship);

    $items[] = $item;

    $reliantChilds->setItems($items);

    // set attributes
    $attributes->setAddress1('1 Main Street');
    $attributes->setGlobalTaxCode(new SalesInvoiceAttributesWrite_globalTaxCode('T5'));
    $attributes->setCurrency(1);

    $body =  new SalesInvoicePostDto();
    $body->setRelationships($relationships);
    $body->setAttributes($attributes);
    $body->setReliantChilds($reliantChilds);

    error_log('test');
    //print_r($body->getFieldDeserializers());
    //error_log(serialize($body->getFieldDeserializers()));
    error_log('ENDtest');

    //print_r($body->getFieldDeserializers());
    error_log(str_replace('[]','null',json_encode_private($body)));

    //die();

    $request->api()->v2()->salesInvoices()->post($body,$query)->then(

        function (SalesInvoiceGetDto $data) {
            error_log($data->getInvoiceNumber());
        },
        function ($exception) {
            error_log($exception->getMessage());
            error_log($exception->getResponse()->getBody()->getContents());
        }
    );
}
function postOrder(GetHyperAccountsV2ApiClient $request)
{
    $query = new SalesOrdersRequestBuilderPostRequestConfiguration();
    $attributes = new SalesOrderAttributesWrite();
    $relationships = new SalesOrderRelationshipsWrite();
    $reliantChilds = new SalesOrderReliantChilds();

    // set account ref
    $customerRelatedRequired = new CustomerRelatedRequired();
    $customerRelatedRequired->setAccountRef('A1D001');

    $customer = new CustomerRelatedRequiredRelationshipWrite();
    $customer->setData($customerRelatedRequired);

    $relationships->setCustomer($customer);


    // set items
    $items = [];

    $item = new SalesOrderItemPostDto();

    $itemAttribute = new SalesOrderItemAttributesWrite();
    $itemAttribute->setDescription('Description4Item');
    $itemAttribute->setUnitPrice(100);
    $itemAttribute->setQtyOrder(4);
    $item->setAttributes($itemAttribute);

    $itemRelationship = new SalesOrderItemRelationshipsWrite();

    $stockData = new StockRelatedRequired();
    $stockData->setStockCode('BOARD001');
    $stockRelationship = new StockRelatedRequiredRelationshipWrite();
    $stockRelationship->setData($stockData);
    $itemRelationship->setStock($stockRelationship);

    $nominalRelationship = new NominalRelatedRequiredRelationshipWrite();
    $nominalData = new NominalRelatedRequired();
    $nominalData->setAccountRef('0011');
    $nominalRelationship->setData($nominalData);
    $itemRelationship->setNominalCode($nominalRelationship);

    $item->setRelationships($itemRelationship);

    $items[] = $item;

    $reliantChilds->setItems($items);

    // set attributes
    $attributes->setAddress1('1 Main Street');
    $attributes->setDelName('test name');
    $attributes->setCurrency(1);

    $body =  new SalesOrderPostDto();
    $body->setRelationships($relationships);
    $body->setAttributes($attributes);
    $body->setReliantChilds($reliantChilds);

    error_log('test');

    error_log(str_replace('[]','null',json_encode_private($body)));

    $request->api()->v2()->salesOrders()->post($body,$query)->then(

        function (SalesOrderGetDto $data) {
            error_log($data->getOrderNumber());
            error_log($data->getAttributes()->getDelName());
        },
        function ($exception) {
            error_log(json_encode($exception));
            error_log($exception->getMessage());
            //error_log($exception->getResponse()->getBody()->getContents());
        }
    );
}

function json_encode_private($object) {

    function extract_props($object) {
        $public = [];

        $reflection = new ReflectionClass(get_class($object));

        foreach ($reflection->getProperties() as $property) {
            $property->setAccessible(true);

            $value = $property->getValue($object);
            $name = $property->getName();

            if(is_array($value)) {
                $public[$name] = [];

                foreach ($value as $item) {
                    if (is_object($item)) {
                        $itemArray = extract_props($item);
                        $public[$name][] = $itemArray;
                    } else {
                        $public[$name][] = $item;
                    }
                }
            } else if(is_object($value)) {
                $public[$name] = extract_props($value);
            } else $public[$name] = $value;
        }

        return $public;
    }

    return json_encode(extract_props($object));
}
// example of patching an invoice
function patchInvoice(GetHyperAccountsV2ApiClient $request)
{
    $requestConfiguration = new WithInvoiceNumberItemRequestBuilderPatchRequestConfiguration();
    $attributes = new SalesInvoiceAttributesEdit();

    // set attributes
    $attributes->setAddress1('2 Main Street');
    $attributes->setNotes1('TESTING NOTES');
    $attributes->setGlobalTaxCode(new SalesInvoiceAttributesEdit_globalTaxCode('T5'));

    $body =  new SalesInvoicePatchDto();
    $body->setAttributes($attributes);

    $request->api()->v2()->salesInvoicesById("85")->patch($body,$requestConfiguration)->then(

        function (SalesInvoiceGetDto $data) {
            error_log($data->getInvoiceNumber());
            error_log($data->getAttributes()->getNotes1());
            error_log($data->getAttributes()->getDelName());
        },
        function ($exception) {
            error_log($exception->getMessage());
            //error_log($exception->getResponse()->getBody()->getContents());
        }
    );

}

function allocateOrder(GetHyperAccountsV2ApiClient $request)
{
    $request->api()->v2()->salesOrdersById(73)->actions()->fullyAllocate()->post()->then(
        function (SalesOrderGetDto $data)
        {
            error_log($data->getOrderNumber());
        },
        function ($exception) {
            error_log($exception->getMessage());
            error_log($exception->getResponse()->getBody()->getContents());
        }
    );
}
