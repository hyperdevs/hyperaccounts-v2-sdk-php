<?php
namespace HyperAccountsV2Sdk;

use GuzzleHttp\Client;
use HyperAccountsV2Sdk\Authentication\AuthenticationProvider;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\GetHyperAccountsV2ApiClient;
use Microsoft\Kiota\Http\GuzzleRequestAdapter;

class ClientConstructor
{
    public static function construct(string $baseURL, string $username, string $password):GetHyperAccountsV2ApiClient
    {
        $auth = new AuthenticationProvider($username,$password);
        $client = new GuzzleRequestAdapter($auth,null,null, new Client());
        $client->setBaseUrl($baseURL);

        return new GetHyperAccountsV2ApiClient($client);
    }
}
