<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\StockTransactions;

use Microsoft\Kiota\Abstractions\RequestOption;

/**
 * Configuration for the request such as headers, query parameters, and middleware options.
*/
class StockTransactionsRequestBuilderGetRequestConfiguration
{
    /**
     * @var array<string, array<string>|string>|null $headers Request headers
    */
    public ?array $headers = null;

    /**
     * @var array<RequestOption>|null $options Request options
    */
    public ?array $options = null;

    /**
     * @var StockTransactionsRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public ?StockTransactionsRequestBuilderGetQueryParameters $queryParameters = null;

    /**
     * Instantiates a new StockTransactionsRequestBuilderGetQueryParameters.
     * @param string|null $included Include related resources (stock)
     * @param int|null $limit Number of items to return
     * @param int|null $offset Ignore number of items
     * @param string|null $q Filter collection. Url encode the following string: [{"Property":"Reference","Method":"!=","Value":""}]
     * @return StockTransactionsRequestBuilderGetQueryParameters
    */
    public static function addQueryParameters(?string $included = null, ?int $limit = null, ?int $offset = null, ?string $q = null): StockTransactionsRequestBuilderGetQueryParameters {
        return new StockTransactionsRequestBuilderGetQueryParameters($included, $limit, $offset, $q);
    }

    /**
     * Instantiates a new StockTransactionsRequestBuilderGetRequestConfiguration and sets the default values.
     * @param array<string, array<string>|string>|null $headers Request headers
     * @param array<RequestOption>|null $options Request options
     * @param StockTransactionsRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public function __construct(?array $headers = null, ?array $options = null, ?StockTransactionsRequestBuilderGetQueryParameters $queryParameters = null) {
        $this->headers = $headers;
        $this->options = $options;
        $this->queryParameters = $queryParameters;
    }

}
