<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\StockTransactions\Item;

/**
 * Gets a specific stock transaction.
*/
class WithTranNumberItemRequestBuilderGetQueryParameters
{
    /**
     * @var string|null $included Include related resources (stock)
    */
    public ?string $included = null;

    /**
     * Instantiates a new WithTranNumberItemRequestBuilderGetQueryParameters and sets the default values.
     * @param string|null $included Include related resources (stock)
    */
    public function __construct(?string $included = null) {
        $this->included = $included;
    }

}
