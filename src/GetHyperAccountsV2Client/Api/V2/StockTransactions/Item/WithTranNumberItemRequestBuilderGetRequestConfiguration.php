<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\StockTransactions\Item;

use Microsoft\Kiota\Abstractions\RequestOption;

/**
 * Configuration for the request such as headers, query parameters, and middleware options.
*/
class WithTranNumberItemRequestBuilderGetRequestConfiguration
{
    /**
     * @var array<string, array<string>|string>|null $headers Request headers
    */
    public ?array $headers = null;

    /**
     * @var array<RequestOption>|null $options Request options
    */
    public ?array $options = null;

    /**
     * @var WithTranNumberItemRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public ?WithTranNumberItemRequestBuilderGetQueryParameters $queryParameters = null;

    /**
     * Instantiates a new WithTranNumberItemRequestBuilderGetQueryParameters.
     * @param string|null $included Include related resources (stock)
     * @return WithTranNumberItemRequestBuilderGetQueryParameters
    */
    public static function addQueryParameters(?string $included = null): WithTranNumberItemRequestBuilderGetQueryParameters {
        return new WithTranNumberItemRequestBuilderGetQueryParameters($included);
    }

    /**
     * Instantiates a new WithTranNumberItemRequestBuilderGetRequestConfiguration and sets the default values.
     * @param array<string, array<string>|string>|null $headers Request headers
     * @param array<RequestOption>|null $options Request options
     * @param WithTranNumberItemRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public function __construct(?array $headers = null, ?array $options = null, ?WithTranNumberItemRequestBuilderGetQueryParameters $queryParameters = null) {
        $this->headers = $headers;
        $this->options = $options;
        $this->queryParameters = $queryParameters;
    }

}
