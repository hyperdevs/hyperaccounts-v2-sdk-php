<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Suppliers\Item\GoodsReceivedNotes;

/**
 * Gets related grns to supplier.
*/
class GoodsReceivedNotesRequestBuilderGetQueryParameters
{
    /**
     * @var int|null $limit Number of grns to return
    */
    public ?int $limit = null;

    /**
     * @var int|null $offset Number of grns to ignore
    */
    public ?int $offset = null;

    /**
     * Instantiates a new GoodsReceivedNotesRequestBuilderGetQueryParameters and sets the default values.
     * @param int|null $limit Number of grns to return
     * @param int|null $offset Number of grns to ignore
    */
    public function __construct(?int $limit = null, ?int $offset = null) {
        $this->limit = $limit;
        $this->offset = $offset;
    }

}
