<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\PurchaseOrders\Item;

/**
 * Gets a specific purchase order.
*/
class WithOrderNumberItemRequestBuilderGetQueryParameters
{
    /**
     * @var Int|null $childCollectionLimit
    */
    public ?Int $childCollectionLimit = null;

    /**
     * @var Int|null $childCollectionOffset
    */
    public ?Int $childCollectionOffset = null;

    /**
     * @var string|null $included Include related resources (supplier;items;project)
    */
    public ?string $included = null;

    /**
     * Instantiates a new WithOrderNumberItemRequestBuilderGetQueryParameters and sets the default values.
     * @param Int|null $childCollectionLimit
     * @param Int|null $childCollectionOffset
     * @param string|null $included Include related resources (supplier;items;project)
    */
    public function __construct(?Int $childCollectionLimit = null, ?Int $childCollectionOffset = null, ?string $included = null) {
        $this->childCollectionLimit = $childCollectionLimit;
        $this->childCollectionOffset = $childCollectionOffset;
        $this->included = $included;
    }

}
