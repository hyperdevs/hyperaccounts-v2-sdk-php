<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\PurchaseOrders;

/**
 * Gets a list of puchase orders.
*/
class PurchaseOrdersRequestBuilderGetQueryParameters
{
    /**
     * @var Int|null $childCollectionLimit
    */
    public ?Int $childCollectionLimit = null;

    /**
     * @var Int|null $childCollectionOffset
    */
    public ?Int $childCollectionOffset = null;

    /**
     * @var string|null $included Include related resources (supplier;items;project)
    */
    public ?string $included = null;

    /**
     * @var int|null $limit Number of items to return
    */
    public ?int $limit = null;

    /**
     * @var int|null $offset Ignore number of items
    */
    public ?int $offset = null;

    /**
     * @var string|null $q Filter collection. Url encode the following string: [{"Property":"Address4","Method":"=","Value":"Hyperext"}]
    */
    public ?string $q = null;

    /**
     * Instantiates a new PurchaseOrdersRequestBuilderGetQueryParameters and sets the default values.
     * @param Int|null $childCollectionLimit
     * @param Int|null $childCollectionOffset
     * @param string|null $included Include related resources (supplier;items;project)
     * @param int|null $limit Number of items to return
     * @param int|null $offset Ignore number of items
     * @param string|null $q Filter collection. Url encode the following string: [{"Property":"Address4","Method":"=","Value":"Hyperext"}]
    */
    public function __construct(?Int $childCollectionLimit = null, ?Int $childCollectionOffset = null, ?string $included = null, ?int $limit = null, ?int $offset = null, ?string $q = null) {
        $this->childCollectionLimit = $childCollectionLimit;
        $this->childCollectionOffset = $childCollectionOffset;
        $this->included = $included;
        $this->limit = $limit;
        $this->offset = $offset;
        $this->q = $q;
    }

}
