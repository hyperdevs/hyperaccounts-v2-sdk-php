<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Stocks\Item\InvoiceItems;

use Microsoft\Kiota\Abstractions\RequestOption;

/**
 * Configuration for the request such as headers, query parameters, and middleware options.
*/
class InvoiceItemsRequestBuilderGetRequestConfiguration
{
    /**
     * @var array<string, array<string>|string>|null $headers Request headers
    */
    public ?array $headers = null;

    /**
     * @var array<RequestOption>|null $options Request options
    */
    public ?array $options = null;

    /**
     * @var InvoiceItemsRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public ?InvoiceItemsRequestBuilderGetQueryParameters $queryParameters = null;

    /**
     * Instantiates a new InvoiceItemsRequestBuilderGetQueryParameters.
     * @param int|null $limit Number of items to return
     * @param int|null $offset Ignore number of items
     * @return InvoiceItemsRequestBuilderGetQueryParameters
    */
    public static function addQueryParameters(?int $limit = null, ?int $offset = null): InvoiceItemsRequestBuilderGetQueryParameters {
        return new InvoiceItemsRequestBuilderGetQueryParameters($limit, $offset);
    }

    /**
     * Instantiates a new InvoiceItemsRequestBuilderGetRequestConfiguration and sets the default values.
     * @param array<string, array<string>|string>|null $headers Request headers
     * @param array<RequestOption>|null $options Request options
     * @param InvoiceItemsRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public function __construct(?array $headers = null, ?array $options = null, ?InvoiceItemsRequestBuilderGetQueryParameters $queryParameters = null) {
        $this->headers = $headers;
        $this->options = $options;
        $this->queryParameters = $queryParameters;
    }

}
