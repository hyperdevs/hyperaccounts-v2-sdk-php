<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Stocks\Item\PurchaseOrderItems;

use Microsoft\Kiota\Abstractions\RequestOption;

/**
 * Configuration for the request such as headers, query parameters, and middleware options.
*/
class PurchaseOrderItemsRequestBuilderGetRequestConfiguration
{
    /**
     * @var array<string, array<string>|string>|null $headers Request headers
    */
    public ?array $headers = null;

    /**
     * @var array<RequestOption>|null $options Request options
    */
    public ?array $options = null;

    /**
     * @var PurchaseOrderItemsRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public ?PurchaseOrderItemsRequestBuilderGetQueryParameters $queryParameters = null;

    /**
     * Instantiates a new PurchaseOrderItemsRequestBuilderGetQueryParameters.
     * @param int|null $limit Number of items to return
     * @param int|null $offset Ignore number of items
     * @return PurchaseOrderItemsRequestBuilderGetQueryParameters
    */
    public static function addQueryParameters(?int $limit = null, ?int $offset = null): PurchaseOrderItemsRequestBuilderGetQueryParameters {
        return new PurchaseOrderItemsRequestBuilderGetQueryParameters($limit, $offset);
    }

    /**
     * Instantiates a new PurchaseOrderItemsRequestBuilderGetRequestConfiguration and sets the default values.
     * @param array<string, array<string>|string>|null $headers Request headers
     * @param array<RequestOption>|null $options Request options
     * @param PurchaseOrderItemsRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public function __construct(?array $headers = null, ?array $options = null, ?PurchaseOrderItemsRequestBuilderGetQueryParameters $queryParameters = null) {
        $this->headers = $headers;
        $this->options = $options;
        $this->queryParameters = $queryParameters;
    }

}
