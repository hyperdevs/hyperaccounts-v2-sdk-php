<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Stocks\Item\Prices;

/**
 * Gets related prices to stock.
*/
class PricesRequestBuilderGetQueryParameters
{
    /**
     * @var int|null $limit Number of items to return
    */
    public ?int $limit = null;

    /**
     * @var int|null $offset Ignore number of items
    */
    public ?int $offset = null;

    /**
     * Instantiates a new PricesRequestBuilderGetQueryParameters and sets the default values.
     * @param int|null $limit Number of items to return
     * @param int|null $offset Ignore number of items
    */
    public function __construct(?int $limit = null, ?int $offset = null) {
        $this->limit = $limit;
        $this->offset = $offset;
    }

}
