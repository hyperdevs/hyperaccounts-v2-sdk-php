<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Stocks\Item\GoodsReceivedNotes;

use Microsoft\Kiota\Abstractions\RequestOption;

/**
 * Configuration for the request such as headers, query parameters, and middleware options.
*/
class GoodsReceivedNotesRequestBuilderGetRequestConfiguration
{
    /**
     * @var array<string, array<string>|string>|null $headers Request headers
    */
    public ?array $headers = null;

    /**
     * @var array<RequestOption>|null $options Request options
    */
    public ?array $options = null;

    /**
     * @var GoodsReceivedNotesRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public ?GoodsReceivedNotesRequestBuilderGetQueryParameters $queryParameters = null;

    /**
     * Instantiates a new GoodsReceivedNotesRequestBuilderGetQueryParameters.
     * @param int|null $limit Number of grns to return
     * @param int|null $offset Number of grns to ignore
     * @return GoodsReceivedNotesRequestBuilderGetQueryParameters
    */
    public static function addQueryParameters(?int $limit = null, ?int $offset = null): GoodsReceivedNotesRequestBuilderGetQueryParameters {
        return new GoodsReceivedNotesRequestBuilderGetQueryParameters($limit, $offset);
    }

    /**
     * Instantiates a new GoodsReceivedNotesRequestBuilderGetRequestConfiguration and sets the default values.
     * @param array<string, array<string>|string>|null $headers Request headers
     * @param array<RequestOption>|null $options Request options
     * @param GoodsReceivedNotesRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public function __construct(?array $headers = null, ?array $options = null, ?GoodsReceivedNotesRequestBuilderGetQueryParameters $queryParameters = null) {
        $this->headers = $headers;
        $this->options = $options;
        $this->queryParameters = $queryParameters;
    }

}
