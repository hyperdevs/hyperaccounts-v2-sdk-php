<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Stocks\Item;

/**
 * Gets a specific stock.
*/
class WithStockCodeItemRequestBuilderGetQueryParameters
{
    /**
     * @var Int|null $childCollectionLimit
    */
    public ?Int $childCollectionLimit = null;

    /**
     * @var Int|null $childCollectionOffset
    */
    public ?Int $childCollectionOffset = null;

    /**
     * @var string|null $included Include related resources (invoiceItems;purchaseOrderItems;salesInvoiceItems;transactions;nominalCode;prices)
    */
    public ?string $included = null;

    /**
     * Instantiates a new WithStockCodeItemRequestBuilderGetQueryParameters and sets the default values.
     * @param Int|null $childCollectionLimit
     * @param Int|null $childCollectionOffset
     * @param string|null $included Include related resources (invoiceItems;purchaseOrderItems;salesInvoiceItems;transactions;nominalCode;prices)
    */
    public function __construct(?Int $childCollectionLimit = null, ?Int $childCollectionOffset = null, ?string $included = null) {
        $this->childCollectionLimit = $childCollectionLimit;
        $this->childCollectionOffset = $childCollectionOffset;
        $this->included = $included;
    }

}
