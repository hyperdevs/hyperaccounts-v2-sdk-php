<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Stocks\Item;

use Exception;
use Http\Promise\Promise;
use Http\Promise\RejectedPromise;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Stocks\Item\GoodsReceivedNotes\GoodsReceivedNotesRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Stocks\Item\InvoiceItems\InvoiceItemsRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Stocks\Item\NominalCode\NominalCodeRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Stocks\Item\Prices\PricesRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Stocks\Item\PurchaseOrderItems\PurchaseOrderItemsRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Stocks\Item\SalesOrderItems\SalesOrderItemsRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Stocks\Item\StockTransactions\StockTransactionsRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models\StockGetDto;
use Microsoft\Kiota\Abstractions\HttpMethod;
use Microsoft\Kiota\Abstractions\RequestAdapter;
use Microsoft\Kiota\Abstractions\RequestInformation;
use Microsoft\Kiota\Abstractions\ResponseHandler;
use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParsableFactory;

/**
 * Builds and executes requests for operations under /api/v2/Stocks/{stockCode}
*/
class WithStockCodeItemRequestBuilder
{
    /**
     * The GoodsReceivedNotes property
    */
    public function goodsReceivedNotes(): GoodsReceivedNotesRequestBuilder {
        return new GoodsReceivedNotesRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The InvoiceItems property
    */
    public function invoiceItems(): InvoiceItemsRequestBuilder {
        return new InvoiceItemsRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The NominalCode property
    */
    public function nominalCode(): NominalCodeRequestBuilder {
        return new NominalCodeRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * @var array<string, mixed> $pathParameters Path parameters for the request
    */
    private array $pathParameters;

    /**
     * The Prices property
    */
    public function prices(): PricesRequestBuilder {
        return new PricesRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The PurchaseOrderItems property
    */
    public function purchaseOrderItems(): PurchaseOrderItemsRequestBuilder {
        return new PurchaseOrderItemsRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * @var RequestAdapter $requestAdapter The request adapter to use to execute the requests.
    */
    private RequestAdapter $requestAdapter;

    /**
     * The SalesOrderItems property
    */
    public function salesOrderItems(): SalesOrderItemsRequestBuilder {
        return new SalesOrderItemsRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The StockTransactions property
    */
    public function stockTransactions(): StockTransactionsRequestBuilder {
        return new StockTransactionsRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * @var string $urlTemplate Url template to use to build the URL for the current request builder
    */
    private string $urlTemplate;

    /**
     * Instantiates a new WithStockCodeItemRequestBuilder and sets the default values.
     * @param array<string, mixed>|string $pathParametersOrRawUrl Path parameters for the request or a String representing the raw URL.
     * @param RequestAdapter $requestAdapter The request adapter to use to execute the requests.
    */
    public function __construct($pathParametersOrRawUrl, RequestAdapter $requestAdapter) {
        $this->urlTemplate = '{+baseurl}/api/v2/Stocks/{stockCode}{?included*,childCollectionLimit*,childCollectionOffset*}';
        $this->requestAdapter = $requestAdapter;
        if (is_array($pathParametersOrRawUrl)) {
            $this->pathParameters = $pathParametersOrRawUrl;
        } else {
            $this->pathParameters = ['request-raw-url' => $pathParametersOrRawUrl];
        }
    }

    /**
     * Gets a specific stock.
     * @param WithStockCodeItemRequestBuilderGetRequestConfiguration|null $requestConfiguration Configuration for the request such as headers, query parameters, and middleware options.
     * @return Promise
    */
    public function get(?WithStockCodeItemRequestBuilderGetRequestConfiguration $requestConfiguration = null): Promise {
        $requestInfo = $this->toGetRequestInformation($requestConfiguration);
        try {
            return $this->requestAdapter->sendAsync($requestInfo, [StockGetDto::class, 'createFromDiscriminatorValue'], null);
        } catch(Exception $ex) {
            return new RejectedPromise($ex);
        }
    }

    /**
     * Gets a specific stock.
     * @param WithStockCodeItemRequestBuilderGetRequestConfiguration|null $requestConfiguration Configuration for the request such as headers, query parameters, and middleware options.
     * @return RequestInformation
    */
    public function toGetRequestInformation(?WithStockCodeItemRequestBuilderGetRequestConfiguration $requestConfiguration = null): RequestInformation {
        $requestInfo = new RequestInformation();
        $requestInfo->urlTemplate = $this->urlTemplate;
        $requestInfo->pathParameters = $this->pathParameters;
        $requestInfo->httpMethod = HttpMethod::GET;
        $requestInfo->addHeader('Accept', "application/json");
        if ($requestConfiguration !== null) {
            if ($requestConfiguration->headers !== null) {
                $requestInfo->addHeaders($requestConfiguration->headers);
            }
            if ($requestConfiguration->queryParameters !== null) {
                $requestInfo->setQueryParameters($requestConfiguration->queryParameters);
            }
            if ($requestConfiguration->options !== null) {
                $requestInfo->addRequestOptions(...$requestConfiguration->options);
            }
        }
        return $requestInfo;
    }

}
