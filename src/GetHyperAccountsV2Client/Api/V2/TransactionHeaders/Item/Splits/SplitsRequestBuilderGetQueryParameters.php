<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\TransactionHeaders\Item\Splits;

/**
 * Gets related splits to transaction header.
*/
class SplitsRequestBuilderGetQueryParameters
{
    /**
     * @var int|null $limit Number of items to return
    */
    public ?int $limit = null;

    /**
     * @var int|null $offset Ignore number of splits
    */
    public ?int $offset = null;

    /**
     * Instantiates a new SplitsRequestBuilderGetQueryParameters and sets the default values.
     * @param int|null $limit Number of items to return
     * @param int|null $offset Ignore number of splits
    */
    public function __construct(?int $limit = null, ?int $offset = null) {
        $this->limit = $limit;
        $this->offset = $offset;
    }

}
