<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\TransactionHeaders\Item\Splits;

use Microsoft\Kiota\Abstractions\RequestOption;

/**
 * Configuration for the request such as headers, query parameters, and middleware options.
*/
class SplitsRequestBuilderGetRequestConfiguration
{
    /**
     * @var array<string, array<string>|string>|null $headers Request headers
    */
    public ?array $headers = null;

    /**
     * @var array<RequestOption>|null $options Request options
    */
    public ?array $options = null;

    /**
     * @var SplitsRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public ?SplitsRequestBuilderGetQueryParameters $queryParameters = null;

    /**
     * Instantiates a new SplitsRequestBuilderGetQueryParameters.
     * @param int|null $limit Number of items to return
     * @param int|null $offset Ignore number of splits
     * @return SplitsRequestBuilderGetQueryParameters
    */
    public static function addQueryParameters(?int $limit = null, ?int $offset = null): SplitsRequestBuilderGetQueryParameters {
        return new SplitsRequestBuilderGetQueryParameters($limit, $offset);
    }

    /**
     * Instantiates a new SplitsRequestBuilderGetRequestConfiguration and sets the default values.
     * @param array<string, array<string>|string>|null $headers Request headers
     * @param array<RequestOption>|null $options Request options
     * @param SplitsRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public function __construct(?array $headers = null, ?array $options = null, ?SplitsRequestBuilderGetQueryParameters $queryParameters = null) {
        $this->headers = $headers;
        $this->options = $options;
        $this->queryParameters = $queryParameters;
    }

}
