<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\TransactionHeaders\Item;

use Microsoft\Kiota\Abstractions\RequestOption;

/**
 * Configuration for the request such as headers, query parameters, and middleware options.
*/
class WithHeaderNumberItemRequestBuilderGetRequestConfiguration
{
    /**
     * @var array<string, array<string>|string>|null $headers Request headers
    */
    public ?array $headers = null;

    /**
     * @var array<RequestOption>|null $options Request options
    */
    public ?array $options = null;

    /**
     * @var WithHeaderNumberItemRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public ?WithHeaderNumberItemRequestBuilderGetQueryParameters $queryParameters = null;

    /**
     * Instantiates a new WithHeaderNumberItemRequestBuilderGetQueryParameters.
     * @param Int|null $childCollectionLimit
     * @param Int|null $childCollectionOffset
     * @param string|null $included Include items (splits)
     * @return WithHeaderNumberItemRequestBuilderGetQueryParameters
    */
    public static function addQueryParameters(?Int $childCollectionLimit = null, ?Int $childCollectionOffset = null, ?string $included = null): WithHeaderNumberItemRequestBuilderGetQueryParameters {
        return new WithHeaderNumberItemRequestBuilderGetQueryParameters($childCollectionLimit, $childCollectionOffset, $included);
    }

    /**
     * Instantiates a new WithHeaderNumberItemRequestBuilderGetRequestConfiguration and sets the default values.
     * @param array<string, array<string>|string>|null $headers Request headers
     * @param array<RequestOption>|null $options Request options
     * @param WithHeaderNumberItemRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public function __construct(?array $headers = null, ?array $options = null, ?WithHeaderNumberItemRequestBuilderGetQueryParameters $queryParameters = null) {
        $this->headers = $headers;
        $this->options = $options;
        $this->queryParameters = $queryParameters;
    }

}
