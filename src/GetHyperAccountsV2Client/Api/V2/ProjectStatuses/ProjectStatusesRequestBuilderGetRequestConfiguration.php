<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\ProjectStatuses;

use Microsoft\Kiota\Abstractions\RequestOption;

/**
 * Configuration for the request such as headers, query parameters, and middleware options.
*/
class ProjectStatusesRequestBuilderGetRequestConfiguration
{
    /**
     * @var array<string, array<string>|string>|null $headers Request headers
    */
    public ?array $headers = null;

    /**
     * @var array<RequestOption>|null $options Request options
    */
    public ?array $options = null;

    /**
     * @var ProjectStatusesRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public ?ProjectStatusesRequestBuilderGetQueryParameters $queryParameters = null;

    /**
     * Instantiates a new ProjectStatusesRequestBuilderGetQueryParameters.
     * @param Int|null $childCollectionLimit
     * @param Int|null $childCollectionOffset
     * @param string|null $included Include related resources (projects)
     * @param int|null $limit Number of items to return
     * @param int|null $offset Ignore number of items
     * @param string|null $q Filter collection. Url encode the following string: [{"Property":"Number","Method":">","Value":"5"}]
     * @return ProjectStatusesRequestBuilderGetQueryParameters
    */
    public static function addQueryParameters(?Int $childCollectionLimit = null, ?Int $childCollectionOffset = null, ?string $included = null, ?int $limit = null, ?int $offset = null, ?string $q = null): ProjectStatusesRequestBuilderGetQueryParameters {
        return new ProjectStatusesRequestBuilderGetQueryParameters($childCollectionLimit, $childCollectionOffset, $included, $limit, $offset, $q);
    }

    /**
     * Instantiates a new ProjectStatusesRequestBuilderGetRequestConfiguration and sets the default values.
     * @param array<string, array<string>|string>|null $headers Request headers
     * @param array<RequestOption>|null $options Request options
     * @param ProjectStatusesRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public function __construct(?array $headers = null, ?array $options = null, ?ProjectStatusesRequestBuilderGetQueryParameters $queryParameters = null) {
        $this->headers = $headers;
        $this->options = $options;
        $this->queryParameters = $queryParameters;
    }

}
