<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Prices\Item;

use Microsoft\Kiota\Abstractions\RequestOption;

/**
 * Configuration for the request such as headers, query parameters, and middleware options.
*/
class WithPricingItemRequestBuilderGetRequestConfiguration
{
    /**
     * @var array<string, array<string>|string>|null $headers Request headers
    */
    public ?array $headers = null;

    /**
     * @var array<RequestOption>|null $options Request options
    */
    public ?array $options = null;

    /**
     * @var WithPricingItemRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public ?WithPricingItemRequestBuilderGetQueryParameters $queryParameters = null;

    /**
     * Instantiates a new WithPricingItemRequestBuilderGetQueryParameters.
     * @param string|null $included Include related resources (stock)
     * @return WithPricingItemRequestBuilderGetQueryParameters
    */
    public static function addQueryParameters(?string $included = null): WithPricingItemRequestBuilderGetQueryParameters {
        return new WithPricingItemRequestBuilderGetQueryParameters($included);
    }

    /**
     * Instantiates a new WithPricingItemRequestBuilderGetRequestConfiguration and sets the default values.
     * @param array<string, array<string>|string>|null $headers Request headers
     * @param array<RequestOption>|null $options Request options
     * @param WithPricingItemRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public function __construct(?array $headers = null, ?array $options = null, ?WithPricingItemRequestBuilderGetQueryParameters $queryParameters = null) {
        $this->headers = $headers;
        $this->options = $options;
        $this->queryParameters = $queryParameters;
    }

}
