<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Prices\Item;

/**
 * Gets a specific price.
*/
class WithPricingItemRequestBuilderGetQueryParameters
{
    /**
     * @var string|null $included Include related resources (stock)
    */
    public ?string $included = null;

    /**
     * Instantiates a new WithPricingItemRequestBuilderGetQueryParameters and sets the default values.
     * @param string|null $included Include related resources (stock)
    */
    public function __construct(?string $included = null) {
        $this->included = $included;
    }

}
