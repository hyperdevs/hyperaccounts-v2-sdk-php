<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\CustomerAddresses;

/**
 * Gets a list of customer addresss.
*/
class CustomerAddressesRequestBuilderGetQueryParameters
{
    /**
     * @var string|null $included Include related resources (customer)
    */
    public ?string $included = null;

    /**
     * @var int|null $limit Number of items to return
    */
    public ?int $limit = null;

    /**
     * @var int|null $offset Ignore number of items
    */
    public ?int $offset = null;

    /**
     * @var string|null $q Filter collection. Url encode the following string: [{"Property":"Number","Method":">","Value":"5"}]
    */
    public ?string $q = null;

    /**
     * Instantiates a new CustomerAddressesRequestBuilderGetQueryParameters and sets the default values.
     * @param string|null $included Include related resources (customer)
     * @param int|null $limit Number of items to return
     * @param int|null $offset Ignore number of items
     * @param string|null $q Filter collection. Url encode the following string: [{"Property":"Number","Method":">","Value":"5"}]
    */
    public function __construct(?string $included = null, ?int $limit = null, ?int $offset = null, ?string $q = null) {
        $this->included = $included;
        $this->limit = $limit;
        $this->offset = $offset;
        $this->q = $q;
    }

}
