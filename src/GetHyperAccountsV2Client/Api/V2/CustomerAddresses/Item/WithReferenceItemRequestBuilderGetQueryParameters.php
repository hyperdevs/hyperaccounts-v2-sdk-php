<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\CustomerAddresses\Item;

/**
 * Gets a specific customer addresss.
*/
class WithReferenceItemRequestBuilderGetQueryParameters
{
    /**
     * @var string|null $included Include related resources (customer)
    */
    public ?string $included = null;

    /**
     * @var string|null $reference Reference linked to customer addresss
    */
    public ?string $reference = null;

    /**
     * Instantiates a new WithReferenceItemRequestBuilderGetQueryParameters and sets the default values.
     * @param string|null $included Include related resources (customer)
     * @param string|null $reference Reference linked to customer addresss
    */
    public function __construct(?string $included = null, ?string $reference = null) {
        $this->included = $included;
        $this->reference = $reference;
    }

}
