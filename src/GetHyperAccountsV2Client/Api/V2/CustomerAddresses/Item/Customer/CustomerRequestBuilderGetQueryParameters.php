<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\CustomerAddresses\Item\Customer;

/**
 * Gets related customer from customer address.
*/
class CustomerRequestBuilderGetQueryParameters
{
    /**
     * @var string|null $reference Reference linked to customer address
    */
    public ?string $reference = null;

    /**
     * Instantiates a new CustomerRequestBuilderGetQueryParameters and sets the default values.
     * @param string|null $reference Reference linked to customer address
    */
    public function __construct(?string $reference = null) {
        $this->reference = $reference;
    }

}
