<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\CustomerAddresses\Item\Customer;

use Microsoft\Kiota\Abstractions\RequestOption;

/**
 * Configuration for the request such as headers, query parameters, and middleware options.
*/
class CustomerRequestBuilderGetRequestConfiguration
{
    /**
     * @var array<string, array<string>|string>|null $headers Request headers
    */
    public ?array $headers = null;

    /**
     * @var array<RequestOption>|null $options Request options
    */
    public ?array $options = null;

    /**
     * @var CustomerRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public ?CustomerRequestBuilderGetQueryParameters $queryParameters = null;

    /**
     * Instantiates a new CustomerRequestBuilderGetQueryParameters.
     * @param string|null $reference Reference linked to customer address
     * @return CustomerRequestBuilderGetQueryParameters
    */
    public static function addQueryParameters(?string $reference = null): CustomerRequestBuilderGetQueryParameters {
        return new CustomerRequestBuilderGetQueryParameters($reference);
    }

    /**
     * Instantiates a new CustomerRequestBuilderGetRequestConfiguration and sets the default values.
     * @param array<string, array<string>|string>|null $headers Request headers
     * @param array<RequestOption>|null $options Request options
     * @param CustomerRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public function __construct(?array $headers = null, ?array $options = null, ?CustomerRequestBuilderGetQueryParameters $queryParameters = null) {
        $this->headers = $headers;
        $this->options = $options;
        $this->queryParameters = $queryParameters;
    }

}
