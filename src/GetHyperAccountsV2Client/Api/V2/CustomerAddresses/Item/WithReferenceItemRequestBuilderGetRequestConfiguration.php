<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\CustomerAddresses\Item;

use Microsoft\Kiota\Abstractions\RequestOption;

/**
 * Configuration for the request such as headers, query parameters, and middleware options.
*/
class WithReferenceItemRequestBuilderGetRequestConfiguration
{
    /**
     * @var array<string, array<string>|string>|null $headers Request headers
    */
    public ?array $headers = null;

    /**
     * @var array<RequestOption>|null $options Request options
    */
    public ?array $options = null;

    /**
     * @var WithReferenceItemRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public ?WithReferenceItemRequestBuilderGetQueryParameters $queryParameters = null;

    /**
     * Instantiates a new WithReferenceItemRequestBuilderGetQueryParameters.
     * @param string|null $included Include related resources (customer)
     * @param string|null $reference Reference linked to customer addresss
     * @return WithReferenceItemRequestBuilderGetQueryParameters
    */
    public static function addQueryParameters(?string $included = null, ?string $reference = null): WithReferenceItemRequestBuilderGetQueryParameters {
        return new WithReferenceItemRequestBuilderGetQueryParameters($included, $reference);
    }

    /**
     * Instantiates a new WithReferenceItemRequestBuilderGetRequestConfiguration and sets the default values.
     * @param array<string, array<string>|string>|null $headers Request headers
     * @param array<RequestOption>|null $options Request options
     * @param WithReferenceItemRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public function __construct(?array $headers = null, ?array $options = null, ?WithReferenceItemRequestBuilderGetQueryParameters $queryParameters = null) {
        $this->headers = $headers;
        $this->options = $options;
        $this->queryParameters = $queryParameters;
    }

}
