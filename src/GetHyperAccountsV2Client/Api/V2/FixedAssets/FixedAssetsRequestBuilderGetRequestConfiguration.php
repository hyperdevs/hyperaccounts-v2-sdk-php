<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\FixedAssets;

use Microsoft\Kiota\Abstractions\RequestOption;

/**
 * Configuration for the request such as headers, query parameters, and middleware options.
*/
class FixedAssetsRequestBuilderGetRequestConfiguration
{
    /**
     * @var array<string, array<string>|string>|null $headers Request headers
    */
    public ?array $headers = null;

    /**
     * @var array<RequestOption>|null $options Request options
    */
    public ?array $options = null;

    /**
     * @var FixedAssetsRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public ?FixedAssetsRequestBuilderGetQueryParameters $queryParameters = null;

    /**
     * Instantiates a new FixedAssetsRequestBuilderGetQueryParameters.
     * @param int|null $limit Number of items to return
     * @param int|null $offset Ignore number of items
     * @param string|null $q Filter collection. Url encode the following string: [{"Property":"AccountRef","Method":"!=","Value":""}]
     * @return FixedAssetsRequestBuilderGetQueryParameters
    */
    public static function addQueryParameters(?int $limit = null, ?int $offset = null, ?string $q = null): FixedAssetsRequestBuilderGetQueryParameters {
        return new FixedAssetsRequestBuilderGetQueryParameters($limit, $offset, $q);
    }

    /**
     * Instantiates a new FixedAssetsRequestBuilderGetRequestConfiguration and sets the default values.
     * @param array<string, array<string>|string>|null $headers Request headers
     * @param array<RequestOption>|null $options Request options
     * @param FixedAssetsRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public function __construct(?array $headers = null, ?array $options = null, ?FixedAssetsRequestBuilderGetQueryParameters $queryParameters = null) {
        $this->headers = $headers;
        $this->options = $options;
        $this->queryParameters = $queryParameters;
    }

}
