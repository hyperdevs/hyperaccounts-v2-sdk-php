<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\TransactionSplits\Item\Usages;

use Microsoft\Kiota\Abstractions\RequestOption;

/**
 * Configuration for the request such as headers, query parameters, and middleware options.
*/
class UsagesRequestBuilderGetRequestConfiguration
{
    /**
     * @var array<string, array<string>|string>|null $headers Request headers
    */
    public ?array $headers = null;

    /**
     * @var array<RequestOption>|null $options Request options
    */
    public ?array $options = null;

    /**
     * @var UsagesRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public ?UsagesRequestBuilderGetQueryParameters $queryParameters = null;

    /**
     * Instantiates a new UsagesRequestBuilderGetQueryParameters.
     * @param int|null $limit Number of items to return
     * @param int|null $offset Ignore number of items
     * @return UsagesRequestBuilderGetQueryParameters
    */
    public static function addQueryParameters(?int $limit = null, ?int $offset = null): UsagesRequestBuilderGetQueryParameters {
        return new UsagesRequestBuilderGetQueryParameters($limit, $offset);
    }

    /**
     * Instantiates a new UsagesRequestBuilderGetRequestConfiguration and sets the default values.
     * @param array<string, array<string>|string>|null $headers Request headers
     * @param array<RequestOption>|null $options Request options
     * @param UsagesRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public function __construct(?array $headers = null, ?array $options = null, ?UsagesRequestBuilderGetQueryParameters $queryParameters = null) {
        $this->headers = $headers;
        $this->options = $options;
        $this->queryParameters = $queryParameters;
    }

}
