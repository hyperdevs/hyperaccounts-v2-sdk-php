<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\SalesInvoiceItems;

use Microsoft\Kiota\Abstractions\RequestOption;

/**
 * Configuration for the request such as headers, query parameters, and middleware options.
*/
class SalesInvoiceItemsRequestBuilderGetRequestConfiguration
{
    /**
     * @var array<string, array<string>|string>|null $headers Request headers
    */
    public ?array $headers = null;

    /**
     * @var array<RequestOption>|null $options Request options
    */
    public ?array $options = null;

    /**
     * @var SalesInvoiceItemsRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public ?SalesInvoiceItemsRequestBuilderGetQueryParameters $queryParameters = null;

    /**
     * Instantiates a new SalesInvoiceItemsRequestBuilderGetQueryParameters.
     * @param string|null $included Include items (invoice;stock;project;nominalCode)
     * @param int|null $limit Number of items to return
     * @param int|null $offset Ignore number of items
     * @param string|null $q Filters
     * @return SalesInvoiceItemsRequestBuilderGetQueryParameters
    */
    public static function addQueryParameters(?string $included = null, ?int $limit = null, ?int $offset = null, ?string $q = null): SalesInvoiceItemsRequestBuilderGetQueryParameters {
        return new SalesInvoiceItemsRequestBuilderGetQueryParameters($included, $limit, $offset, $q);
    }

    /**
     * Instantiates a new SalesInvoiceItemsRequestBuilderGetRequestConfiguration and sets the default values.
     * @param array<string, array<string>|string>|null $headers Request headers
     * @param array<RequestOption>|null $options Request options
     * @param SalesInvoiceItemsRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public function __construct(?array $headers = null, ?array $options = null, ?SalesInvoiceItemsRequestBuilderGetQueryParameters $queryParameters = null) {
        $this->headers = $headers;
        $this->options = $options;
        $this->queryParameters = $queryParameters;
    }

}
