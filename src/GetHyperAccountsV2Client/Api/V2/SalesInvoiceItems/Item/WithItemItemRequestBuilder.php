<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\SalesInvoiceItems\Item;

use Exception;
use Http\Promise\Promise;
use Http\Promise\RejectedPromise;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\SalesInvoiceItems\Item\Invoice\InvoiceRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\SalesInvoiceItems\Item\NominalCode\NominalCodeRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\SalesInvoiceItems\Item\Project\ProjectRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\SalesInvoiceItems\Item\Stock\StockRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models\SalesInvoiceItemGetDto;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models\SalesInvoiceItemPatchDto;
use Microsoft\Kiota\Abstractions\HttpMethod;
use Microsoft\Kiota\Abstractions\RequestAdapter;
use Microsoft\Kiota\Abstractions\RequestInformation;
use Microsoft\Kiota\Abstractions\ResponseHandler;
use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParsableFactory;

/**
 * Builds and executes requests for operations under /api/v2/SalesInvoiceItems/{itemId}
*/
class WithItemItemRequestBuilder
{
    /**
     * The Invoice property
    */
    public function invoice(): InvoiceRequestBuilder {
        return new InvoiceRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The NominalCode property
    */
    public function nominalCode(): NominalCodeRequestBuilder {
        return new NominalCodeRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * @var array<string, mixed> $pathParameters Path parameters for the request
    */
    private array $pathParameters;

    /**
     * The Project property
    */
    public function project(): ProjectRequestBuilder {
        return new ProjectRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * @var RequestAdapter $requestAdapter The request adapter to use to execute the requests.
    */
    private RequestAdapter $requestAdapter;

    /**
     * The Stock property
    */
    public function stock(): StockRequestBuilder {
        return new StockRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * @var string $urlTemplate Url template to use to build the URL for the current request builder
    */
    private string $urlTemplate;

    /**
     * Instantiates a new WithItemItemRequestBuilder and sets the default values.
     * @param array<string, mixed>|string $pathParametersOrRawUrl Path parameters for the request or a String representing the raw URL.
     * @param RequestAdapter $requestAdapter The request adapter to use to execute the requests.
    */
    public function __construct($pathParametersOrRawUrl, RequestAdapter $requestAdapter) {
        $this->urlTemplate = '{+baseurl}/api/v2/SalesInvoiceItems/{itemId}{?included*}';
        $this->requestAdapter = $requestAdapter;
        if (is_array($pathParametersOrRawUrl)) {
            $this->pathParameters = $pathParametersOrRawUrl;
        } else {
            $this->pathParameters = ['request-raw-url' => $pathParametersOrRawUrl];
        }
    }

    /**
     * Gets a specific sales invoice item.
     * @param WithItemItemRequestBuilderGetRequestConfiguration|null $requestConfiguration Configuration for the request such as headers, query parameters, and middleware options.
     * @return Promise
    */
    public function get(?WithItemItemRequestBuilderGetRequestConfiguration $requestConfiguration = null): Promise {
        $requestInfo = $this->toGetRequestInformation($requestConfiguration);
        try {
            return $this->requestAdapter->sendAsync($requestInfo, [SalesInvoiceItemGetDto::class, 'createFromDiscriminatorValue'], null);
        } catch(Exception $ex) {
            return new RejectedPromise($ex);
        }
    }

    /**
     * Patch a sales invoice item.
     * @param SalesInvoiceItemPatchDto $body The request body
     * @param WithItemItemRequestBuilderPatchRequestConfiguration|null $requestConfiguration Configuration for the request such as headers, query parameters, and middleware options.
     * @return Promise
    */
    public function patch(SalesInvoiceItemPatchDto $body, ?WithItemItemRequestBuilderPatchRequestConfiguration $requestConfiguration = null): Promise {
        $requestInfo = $this->toPatchRequestInformation($body, $requestConfiguration);
        try {
            return $this->requestAdapter->sendAsync($requestInfo, [SalesInvoiceItemGetDto::class, 'createFromDiscriminatorValue'], null);
        } catch(Exception $ex) {
            return new RejectedPromise($ex);
        }
    }

    /**
     * Gets a specific sales invoice item.
     * @param WithItemItemRequestBuilderGetRequestConfiguration|null $requestConfiguration Configuration for the request such as headers, query parameters, and middleware options.
     * @return RequestInformation
    */
    public function toGetRequestInformation(?WithItemItemRequestBuilderGetRequestConfiguration $requestConfiguration = null): RequestInformation {
        $requestInfo = new RequestInformation();
        $requestInfo->urlTemplate = $this->urlTemplate;
        $requestInfo->pathParameters = $this->pathParameters;
        $requestInfo->httpMethod = HttpMethod::GET;
        $requestInfo->addHeader('Accept', "application/json");
        if ($requestConfiguration !== null) {
            if ($requestConfiguration->headers !== null) {
                $requestInfo->addHeaders($requestConfiguration->headers);
            }
            if ($requestConfiguration->queryParameters !== null) {
                $requestInfo->setQueryParameters($requestConfiguration->queryParameters);
            }
            if ($requestConfiguration->options !== null) {
                $requestInfo->addRequestOptions(...$requestConfiguration->options);
            }
        }
        return $requestInfo;
    }

    /**
     * Patch a sales invoice item.
     * @param SalesInvoiceItemPatchDto $body The request body
     * @param WithItemItemRequestBuilderPatchRequestConfiguration|null $requestConfiguration Configuration for the request such as headers, query parameters, and middleware options.
     * @return RequestInformation
    */
    public function toPatchRequestInformation(SalesInvoiceItemPatchDto $body, ?WithItemItemRequestBuilderPatchRequestConfiguration $requestConfiguration = null): RequestInformation {
        $requestInfo = new RequestInformation();
        $requestInfo->urlTemplate = $this->urlTemplate;
        $requestInfo->pathParameters = $this->pathParameters;
        $requestInfo->httpMethod = HttpMethod::PATCH;
        $requestInfo->addHeader('Accept', "application/json");
        if ($requestConfiguration !== null) {
            if ($requestConfiguration->headers !== null) {
                $requestInfo->addHeaders($requestConfiguration->headers);
            }
            if ($requestConfiguration->options !== null) {
                $requestInfo->addRequestOptions(...$requestConfiguration->options);
            }
        }
        $requestInfo->setContentFromParsable($this->requestAdapter, "application/json", $body);
        return $requestInfo;
    }

}
