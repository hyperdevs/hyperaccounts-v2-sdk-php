<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\SalesInvoiceItems\Item;

/**
 * Gets a specific sales invoice item.
*/
class WithItemItemRequestBuilderGetQueryParameters
{
    /**
     * @var string|null $included Include items (invoice;stock;project;nominalCode)
    */
    public ?string $included = null;

    /**
     * Instantiates a new WithItemItemRequestBuilderGetQueryParameters and sets the default values.
     * @param string|null $included Include items (invoice;stock;project;nominalCode)
    */
    public function __construct(?string $included = null) {
        $this->included = $included;
    }

}
