<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Nominals\Item\PurchaseOrderItems;

/**
 * Gets a list of purchase order item.
*/
class PurchaseOrderItemsRequestBuilderGetQueryParameters
{
    /**
     * @var int|null $limit Number of items to return
    */
    public ?int $limit = null;

    /**
     * @var int|null $offset Number of items to ignore
    */
    public ?int $offset = null;

    /**
     * Instantiates a new PurchaseOrderItemsRequestBuilderGetQueryParameters and sets the default values.
     * @param int|null $limit Number of items to return
     * @param int|null $offset Number of items to ignore
    */
    public function __construct(?int $limit = null, ?int $offset = null) {
        $this->limit = $limit;
        $this->offset = $offset;
    }

}
