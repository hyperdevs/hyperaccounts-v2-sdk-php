<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Nominals\Item;

use Exception;
use Http\Promise\Promise;
use Http\Promise\RejectedPromise;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Nominals\Item\PurchaseOrderItems\PurchaseOrderItemsRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Nominals\Item\SalesInvoiceItems\SalesInvoiceItemsRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Nominals\Item\SalesOrderItems\SalesOrderItemsRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Nominals\Item\Stocks\StocksRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Nominals\Item\TransactionSplits\TransactionSplitsRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models\NominalGetDto;
use Microsoft\Kiota\Abstractions\HttpMethod;
use Microsoft\Kiota\Abstractions\RequestAdapter;
use Microsoft\Kiota\Abstractions\RequestInformation;
use Microsoft\Kiota\Abstractions\ResponseHandler;
use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParsableFactory;

/**
 * Builds and executes requests for operations under /api/v2/Nominals/{accountRef}
*/
class WithAccountRefItemRequestBuilder
{
    /**
     * @var array<string, mixed> $pathParameters Path parameters for the request
    */
    private array $pathParameters;

    /**
     * The PurchaseOrderItems property
    */
    public function purchaseOrderItems(): PurchaseOrderItemsRequestBuilder {
        return new PurchaseOrderItemsRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * @var RequestAdapter $requestAdapter The request adapter to use to execute the requests.
    */
    private RequestAdapter $requestAdapter;

    /**
     * The SalesInvoiceItems property
    */
    public function salesInvoiceItems(): SalesInvoiceItemsRequestBuilder {
        return new SalesInvoiceItemsRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The SalesOrderItems property
    */
    public function salesOrderItems(): SalesOrderItemsRequestBuilder {
        return new SalesOrderItemsRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The Stocks property
    */
    public function stocks(): StocksRequestBuilder {
        return new StocksRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The TransactionSplits property
    */
    public function transactionSplits(): TransactionSplitsRequestBuilder {
        return new TransactionSplitsRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * @var string $urlTemplate Url template to use to build the URL for the current request builder
    */
    private string $urlTemplate;

    /**
     * Instantiates a new WithAccountRefItemRequestBuilder and sets the default values.
     * @param array<string, mixed>|string $pathParametersOrRawUrl Path parameters for the request or a String representing the raw URL.
     * @param RequestAdapter $requestAdapter The request adapter to use to execute the requests.
    */
    public function __construct($pathParametersOrRawUrl, RequestAdapter $requestAdapter) {
        $this->urlTemplate = '{+baseurl}/api/v2/Nominals/{accountRef}{?included*,childCollectionLimit*,childCollectionOffset*}';
        $this->requestAdapter = $requestAdapter;
        if (is_array($pathParametersOrRawUrl)) {
            $this->pathParameters = $pathParametersOrRawUrl;
        } else {
            $this->pathParameters = ['request-raw-url' => $pathParametersOrRawUrl];
        }
    }

    /**
     * Gets a specific Nominal.
     * @param WithAccountRefItemRequestBuilderGetRequestConfiguration|null $requestConfiguration Configuration for the request such as headers, query parameters, and middleware options.
     * @return Promise
    */
    public function get(?WithAccountRefItemRequestBuilderGetRequestConfiguration $requestConfiguration = null): Promise {
        $requestInfo = $this->toGetRequestInformation($requestConfiguration);
        try {
            return $this->requestAdapter->sendAsync($requestInfo, [NominalGetDto::class, 'createFromDiscriminatorValue'], null);
        } catch(Exception $ex) {
            return new RejectedPromise($ex);
        }
    }

    /**
     * Gets a specific Nominal.
     * @param WithAccountRefItemRequestBuilderGetRequestConfiguration|null $requestConfiguration Configuration for the request such as headers, query parameters, and middleware options.
     * @return RequestInformation
    */
    public function toGetRequestInformation(?WithAccountRefItemRequestBuilderGetRequestConfiguration $requestConfiguration = null): RequestInformation {
        $requestInfo = new RequestInformation();
        $requestInfo->urlTemplate = $this->urlTemplate;
        $requestInfo->pathParameters = $this->pathParameters;
        $requestInfo->httpMethod = HttpMethod::GET;
        $requestInfo->addHeader('Accept', "application/json");
        if ($requestConfiguration !== null) {
            if ($requestConfiguration->headers !== null) {
                $requestInfo->addHeaders($requestConfiguration->headers);
            }
            if ($requestConfiguration->queryParameters !== null) {
                $requestInfo->setQueryParameters($requestConfiguration->queryParameters);
            }
            if ($requestConfiguration->options !== null) {
                $requestInfo->addRequestOptions(...$requestConfiguration->options);
            }
        }
        return $requestInfo;
    }

}
