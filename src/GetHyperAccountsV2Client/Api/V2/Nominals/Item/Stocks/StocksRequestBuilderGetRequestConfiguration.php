<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Nominals\Item\Stocks;

use Microsoft\Kiota\Abstractions\RequestOption;

/**
 * Configuration for the request such as headers, query parameters, and middleware options.
*/
class StocksRequestBuilderGetRequestConfiguration
{
    /**
     * @var array<string, array<string>|string>|null $headers Request headers
    */
    public ?array $headers = null;

    /**
     * @var array<RequestOption>|null $options Request options
    */
    public ?array $options = null;

    /**
     * @var StocksRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public ?StocksRequestBuilderGetQueryParameters $queryParameters = null;

    /**
     * Instantiates a new StocksRequestBuilderGetQueryParameters.
     * @param int|null $limit Number of items to return
     * @param int|null $offset Number of items to ignore
     * @return StocksRequestBuilderGetQueryParameters
    */
    public static function addQueryParameters(?int $limit = null, ?int $offset = null): StocksRequestBuilderGetQueryParameters {
        return new StocksRequestBuilderGetQueryParameters($limit, $offset);
    }

    /**
     * Instantiates a new StocksRequestBuilderGetRequestConfiguration and sets the default values.
     * @param array<string, array<string>|string>|null $headers Request headers
     * @param array<RequestOption>|null $options Request options
     * @param StocksRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public function __construct(?array $headers = null, ?array $options = null, ?StocksRequestBuilderGetQueryParameters $queryParameters = null) {
        $this->headers = $headers;
        $this->options = $options;
        $this->queryParameters = $queryParameters;
    }

}
