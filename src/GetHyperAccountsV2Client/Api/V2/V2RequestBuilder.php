<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2;

use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Banks\BanksRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\ChartOfAccounts\ChartOfAccountsRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\ChartOfAccounts\Item\WithChartItemRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\ChartOfAccountsCategories\ChartOfAccountsCategoriesRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\ChartOfAccountsCategoryTitles\ChartOfAccountsCategoryTitlesRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\ChartOfAccountsCategoryTitles\Item\WithCompositeKeyChartAndCategoryItemRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Company\CompanyRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Currencies\CurrenciesRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Currencies\Item\WithNumberItemRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\CustomerAddresses\CustomerAddressesRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Customers\CustomersRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Departments\DepartmentsRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\DocumentLinks\DocumentLinksRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\DocumentLinks\Item\WithTransactionItemRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\FixedAssets\FixedAssetsRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\FixedAssets\Item\WithAssetRefItemRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\GoodsDispatchNotes\GoodsDispatchNotesRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\GoodsDispatchNotes\Item\WithCompositeKeyGdnNumberAndItemNumberItemRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\GoodsReceivedNotes\GoodsReceivedNotesRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\GoodsReceivedNotes\Item\WithCompositeKeyGrnNumberAndItemNumberItemRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Nominals\NominalsRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\PriceLists\Item\WithPricingRefItemRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\PriceLists\PriceListsRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Prices\Item\WithPricingItemRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Prices\PricesRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\ProjectBudgets\Item\WithCompositeKeyProjectIdAndCostCodeItemRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\ProjectBudgets\ProjectBudgetsRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\ProjectCostCodes\Item\WithCostCodeItemRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\ProjectCostCodes\ProjectCostCodesRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\ProjectOnlyTransactions\ProjectOnlyTransactionsRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\ProjectResources\Item\WithResourceItemRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\ProjectResources\ProjectResourcesRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Projects\Item\WithProjectItemRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Projects\ProjectsRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\ProjectStatuses\Item\WithStatusItemRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\ProjectStatuses\ProjectStatusesRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\ProjectTrans\ProjectTransRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\PurchaseOrderItems\PurchaseOrderItemsRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\PurchaseOrders\PurchaseOrdersRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\SalesInvoiceItems\SalesInvoiceItemsRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\SalesInvoices\Item\WithInvoiceNumberItemRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\SalesInvoices\SalesInvoicesRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\SalesOrderItems\SalesOrderItemsRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\SalesOrders\SalesOrdersRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Stocks\Item\WithStockCodeItemRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Stocks\StocksRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\StockTransactions\Item\WithTranNumberItemRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\StockTransactions\StockTransactionsRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Suppliers\SuppliersRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\System\SystemRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\TaxCodes\Item\WithTaxCodeItemRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\TaxCodes\TaxCodesRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\TransactionHeaders\Item\WithHeaderNumberItemRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\TransactionHeaders\TransactionHeadersRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\TransactionSplits\Item\WithSplitNumberItemRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\TransactionSplits\TransactionSplitsRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\TransactionUsages\Item\WithUsageNumberItemRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\TransactionUsages\TransactionUsagesRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Webhooks\Item\WebhooksItemRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Webhooks\WebhooksRequestBuilder;
use Microsoft\Kiota\Abstractions\RequestAdapter;

/**
 * Builds and executes requests for operations under /api/v2
*/
class V2RequestBuilder
{
    /**
     * The Banks property
    */
    public function banks(): BanksRequestBuilder {
        return new BanksRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The ChartOfAccounts property
    */
    public function chartOfAccounts(): ChartOfAccountsRequestBuilder {
        return new ChartOfAccountsRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The ChartOfAccountsCategories property
    */
    public function chartOfAccountsCategories(): ChartOfAccountsCategoriesRequestBuilder {
        return new ChartOfAccountsCategoriesRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The ChartOfAccountsCategoryTitles property
    */
    public function chartOfAccountsCategoryTitles(): ChartOfAccountsCategoryTitlesRequestBuilder {
        return new ChartOfAccountsCategoryTitlesRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The Company property
    */
    public function company(): CompanyRequestBuilder {
        return new CompanyRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The Currencies property
    */
    public function currencies(): CurrenciesRequestBuilder {
        return new CurrenciesRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The CustomerAddresses property
    */
    public function customerAddresses(): CustomerAddressesRequestBuilder {
        return new CustomerAddressesRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The Customers property
    */
    public function customers(): CustomersRequestBuilder {
        return new CustomersRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The Departments property
    */
    public function departments(): DepartmentsRequestBuilder {
        return new DepartmentsRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The DocumentLinks property
    */
    public function documentLinks(): DocumentLinksRequestBuilder {
        return new DocumentLinksRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The FixedAssets property
    */
    public function fixedAssets(): FixedAssetsRequestBuilder {
        return new FixedAssetsRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The GoodsDispatchNotes property
    */
    public function goodsDispatchNotes(): GoodsDispatchNotesRequestBuilder {
        return new GoodsDispatchNotesRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The GoodsReceivedNotes property
    */
    public function goodsReceivedNotes(): GoodsReceivedNotesRequestBuilder {
        return new GoodsReceivedNotesRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The Nominals property
    */
    public function nominals(): NominalsRequestBuilder {
        return new NominalsRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * @var array<string, mixed> $pathParameters Path parameters for the request
    */
    private array $pathParameters;

    /**
     * The PriceLists property
    */
    public function priceLists(): PriceListsRequestBuilder {
        return new PriceListsRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The Prices property
    */
    public function prices(): PricesRequestBuilder {
        return new PricesRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The ProjectBudgets property
    */
    public function projectBudgets(): ProjectBudgetsRequestBuilder {
        return new ProjectBudgetsRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The ProjectCostCodes property
    */
    public function projectCostCodes(): ProjectCostCodesRequestBuilder {
        return new ProjectCostCodesRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The ProjectOnlyTransactions property
    */
    public function projectOnlyTransactions(): ProjectOnlyTransactionsRequestBuilder {
        return new ProjectOnlyTransactionsRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The ProjectResources property
    */
    public function projectResources(): ProjectResourcesRequestBuilder {
        return new ProjectResourcesRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The Projects property
    */
    public function projects(): ProjectsRequestBuilder {
        return new ProjectsRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The ProjectStatuses property
    */
    public function projectStatuses(): ProjectStatusesRequestBuilder {
        return new ProjectStatusesRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The ProjectTrans property
    */
    public function projectTrans(): ProjectTransRequestBuilder {
        return new ProjectTransRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The PurchaseOrderItems property
    */
    public function purchaseOrderItems(): PurchaseOrderItemsRequestBuilder {
        return new PurchaseOrderItemsRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The PurchaseOrders property
    */
    public function purchaseOrders(): PurchaseOrdersRequestBuilder {
        return new PurchaseOrdersRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * @var RequestAdapter $requestAdapter The request adapter to use to execute the requests.
    */
    private RequestAdapter $requestAdapter;

    /**
     * The SalesInvoiceItems property
    */
    public function salesInvoiceItems(): SalesInvoiceItemsRequestBuilder {
        return new SalesInvoiceItemsRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The SalesInvoices property
    */
    public function salesInvoices(): SalesInvoicesRequestBuilder {
        return new SalesInvoicesRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The SalesOrderItems property
    */
    public function salesOrderItems(): SalesOrderItemsRequestBuilder {
        return new SalesOrderItemsRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The SalesOrders property
    */
    public function salesOrders(): SalesOrdersRequestBuilder {
        return new SalesOrdersRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The Stocks property
    */
    public function stocks(): StocksRequestBuilder {
        return new StocksRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The StockTransactions property
    */
    public function stockTransactions(): StockTransactionsRequestBuilder {
        return new StockTransactionsRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The Suppliers property
    */
    public function suppliers(): SuppliersRequestBuilder {
        return new SuppliersRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The System property
    */
    public function system(): SystemRequestBuilder {
        return new SystemRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The TaxCodes property
    */
    public function taxCodes(): TaxCodesRequestBuilder {
        return new TaxCodesRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The TransactionHeaders property
    */
    public function transactionHeaders(): TransactionHeadersRequestBuilder {
        return new TransactionHeadersRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The TransactionSplits property
    */
    public function transactionSplits(): TransactionSplitsRequestBuilder {
        return new TransactionSplitsRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The TransactionUsages property
    */
    public function transactionUsages(): TransactionUsagesRequestBuilder {
        return new TransactionUsagesRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * @var string $urlTemplate Url template to use to build the URL for the current request builder
    */
    private string $urlTemplate;

    /**
     * The Webhooks property
    */
    public function webhooks(): WebhooksRequestBuilder {
        return new WebhooksRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * Gets an item from the HyperAccountsV2Sdk.GetHyperAccountsV2Client.api.v2.Banks.item collection
     * @param string $id Unique identifier of the item
     * @return \HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Banks\Item\WithAccountRefItemRequestBuilder
    */
    public function banksById(string $id): \HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Banks\Item\WithAccountRefItemRequestBuilder {
        $urlTplParams = $this->pathParameters;
        $urlTplParams['accountRef'] = $id;
        return new \HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Banks\Item\WithAccountRefItemRequestBuilder($urlTplParams, $this->requestAdapter);
    }

    /**
     * Gets an item from the HyperAccountsV2Sdk.GetHyperAccountsV2Client.api.v2.ChartOfAccounts.item collection
     * @param string $id Unique identifier of the item
     * @return WithChartItemRequestBuilder
    */
    public function chartOfAccountsById(string $id): WithChartItemRequestBuilder {
        $urlTplParams = $this->pathParameters;
        $urlTplParams['chart'] = $id;
        return new WithChartItemRequestBuilder($urlTplParams, $this->requestAdapter);
    }

    /**
     * Gets an item from the HyperAccountsV2Sdk.GetHyperAccountsV2Client.api.v2.ChartOfAccountsCategoryTitles.item collection
     * @param string $id Unique identifier of the item
     * @return WithCompositeKeyChartAndCategoryItemRequestBuilder
    */
    public function chartOfAccountsCategoryTitlesById(string $id): WithCompositeKeyChartAndCategoryItemRequestBuilder {
        $urlTplParams = $this->pathParameters;
        $urlTplParams['compositeKeyChartAndCategory'] = $id;
        return new WithCompositeKeyChartAndCategoryItemRequestBuilder($urlTplParams, $this->requestAdapter);
    }

    /**
     * Instantiates a new V2RequestBuilder and sets the default values.
     * @param array<string, mixed>|string $pathParametersOrRawUrl Path parameters for the request or a String representing the raw URL.
     * @param RequestAdapter $requestAdapter The request adapter to use to execute the requests.
    */
    public function __construct($pathParametersOrRawUrl, RequestAdapter $requestAdapter) {
        $this->urlTemplate = '{+baseurl}/api/v2';
        $this->requestAdapter = $requestAdapter;
        if (is_array($pathParametersOrRawUrl)) {
            $this->pathParameters = $pathParametersOrRawUrl;
        } else {
            $this->pathParameters = ['request-raw-url' => $pathParametersOrRawUrl];
        }
    }

    /**
     * Gets an item from the HyperAccountsV2Sdk.GetHyperAccountsV2Client.api.v2.Currencies.item collection
     * @param string $id Unique identifier of the item
     * @return WithNumberItemRequestBuilder
    */
    public function currenciesById(string $id): WithNumberItemRequestBuilder {
        $urlTplParams = $this->pathParameters;
        $urlTplParams['number'] = $id;
        return new WithNumberItemRequestBuilder($urlTplParams, $this->requestAdapter);
    }

    /**
     * Gets an item from the HyperAccountsV2Sdk.GetHyperAccountsV2Client.api.v2.CustomerAddresses.item collection
     * @param string $id Unique identifier of the item
     * @return \HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\CustomerAddresses\Item\WithReferenceItemRequestBuilder
    */
    public function customerAddressesById(string $id): \HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\CustomerAddresses\Item\WithReferenceItemRequestBuilder {
        $urlTplParams = $this->pathParameters;
        $urlTplParams['reference'] = $id;
        return new \HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\CustomerAddresses\Item\WithReferenceItemRequestBuilder($urlTplParams, $this->requestAdapter);
    }

    /**
     * Gets an item from the HyperAccountsV2Sdk.GetHyperAccountsV2Client.api.v2.Customers.item collection
     * @param string $id Unique identifier of the item
     * @return \HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Customers\Item\WithAccountRefItemRequestBuilder
    */
    public function customersById(string $id): \HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Customers\Item\WithAccountRefItemRequestBuilder {
        $urlTplParams = $this->pathParameters;
        $urlTplParams['accountRef'] = $id;
        return new \HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Customers\Item\WithAccountRefItemRequestBuilder($urlTplParams, $this->requestAdapter);
    }

    /**
     * Gets an item from the HyperAccountsV2Sdk.GetHyperAccountsV2Client.api.v2.Departments.item collection
     * @param string $id Unique identifier of the item
     * @return \HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Departments\Item\WithReferenceItemRequestBuilder
    */
    public function departmentsById(string $id): \HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Departments\Item\WithReferenceItemRequestBuilder {
        $urlTplParams = $this->pathParameters;
        $urlTplParams['reference'] = $id;
        return new \HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Departments\Item\WithReferenceItemRequestBuilder($urlTplParams, $this->requestAdapter);
    }

    /**
     * Gets an item from the HyperAccountsV2Sdk.GetHyperAccountsV2Client.api.v2.DocumentLinks.item collection
     * @param string $id Unique identifier of the item
     * @return WithTransactionItemRequestBuilder
    */
    public function documentLinksById(string $id): WithTransactionItemRequestBuilder {
        $urlTplParams = $this->pathParameters;
        $urlTplParams['transactionId'] = $id;
        return new WithTransactionItemRequestBuilder($urlTplParams, $this->requestAdapter);
    }

    /**
     * Gets an item from the HyperAccountsV2Sdk.GetHyperAccountsV2Client.api.v2.FixedAssets.item collection
     * @param string $id Unique identifier of the item
     * @return WithAssetRefItemRequestBuilder
    */
    public function fixedAssetsById(string $id): WithAssetRefItemRequestBuilder {
        $urlTplParams = $this->pathParameters;
        $urlTplParams['assetRef'] = $id;
        return new WithAssetRefItemRequestBuilder($urlTplParams, $this->requestAdapter);
    }

    /**
     * Gets an item from the HyperAccountsV2Sdk.GetHyperAccountsV2Client.api.v2.GoodsDispatchNotes.item collection
     * @param string $id Unique identifier of the item
     * @return WithCompositeKeyGdnNumberAndItemNumberItemRequestBuilder
    */
    public function goodsDispatchNotesById(string $id): WithCompositeKeyGdnNumberAndItemNumberItemRequestBuilder {
        $urlTplParams = $this->pathParameters;
        $urlTplParams['compositeKeyGdnNumberAndItemNumber'] = $id;
        return new WithCompositeKeyGdnNumberAndItemNumberItemRequestBuilder($urlTplParams, $this->requestAdapter);
    }

    /**
     * Gets an item from the HyperAccountsV2Sdk.GetHyperAccountsV2Client.api.v2.GoodsReceivedNotes.item collection
     * @param string $id Unique identifier of the item
     * @return WithCompositeKeyGrnNumberAndItemNumberItemRequestBuilder
    */
    public function goodsReceivedNotesById(string $id): WithCompositeKeyGrnNumberAndItemNumberItemRequestBuilder {
        $urlTplParams = $this->pathParameters;
        $urlTplParams['compositeKeyGrnNumberAndItemNumber'] = $id;
        return new WithCompositeKeyGrnNumberAndItemNumberItemRequestBuilder($urlTplParams, $this->requestAdapter);
    }

    /**
     * Gets an item from the HyperAccountsV2Sdk.GetHyperAccountsV2Client.api.v2.Nominals.item collection
     * @param string $id Unique identifier of the item
     * @return \HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Nominals\Item\WithAccountRefItemRequestBuilder
    */
    public function nominalsById(string $id): \HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Nominals\Item\WithAccountRefItemRequestBuilder {
        $urlTplParams = $this->pathParameters;
        $urlTplParams['accountRef'] = $id;
        return new \HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Nominals\Item\WithAccountRefItemRequestBuilder($urlTplParams, $this->requestAdapter);
    }

    /**
     * Gets an item from the HyperAccountsV2Sdk.GetHyperAccountsV2Client.api.v2.PriceLists.item collection
     * @param string $id Unique identifier of the item
     * @return WithPricingRefItemRequestBuilder
    */
    public function priceListsById(string $id): WithPricingRefItemRequestBuilder {
        $urlTplParams = $this->pathParameters;
        $urlTplParams['pricingRef'] = $id;
        return new WithPricingRefItemRequestBuilder($urlTplParams, $this->requestAdapter);
    }

    /**
     * Gets an item from the HyperAccountsV2Sdk.GetHyperAccountsV2Client.api.v2.Prices.item collection
     * @param string $id Unique identifier of the item
     * @return WithPricingItemRequestBuilder
    */
    public function pricesById(string $id): WithPricingItemRequestBuilder {
        $urlTplParams = $this->pathParameters;
        $urlTplParams['pricingId'] = $id;
        return new WithPricingItemRequestBuilder($urlTplParams, $this->requestAdapter);
    }

    /**
     * Gets an item from the HyperAccountsV2Sdk.GetHyperAccountsV2Client.api.v2.ProjectBudgets.item collection
     * @param string $id Unique identifier of the item
     * @return WithCompositeKeyProjectIdAndCostCodeItemRequestBuilder
    */
    public function projectBudgetsById(string $id): WithCompositeKeyProjectIdAndCostCodeItemRequestBuilder {
        $urlTplParams = $this->pathParameters;
        $urlTplParams['compositeKeyProjectIdAndCostCodeId'] = $id;
        return new WithCompositeKeyProjectIdAndCostCodeItemRequestBuilder($urlTplParams, $this->requestAdapter);
    }

    /**
     * Gets an item from the HyperAccountsV2Sdk.GetHyperAccountsV2Client.api.v2.ProjectCostCodes.item collection
     * @param string $id Unique identifier of the item
     * @return WithCostCodeItemRequestBuilder
    */
    public function projectCostCodesById(string $id): WithCostCodeItemRequestBuilder {
        $urlTplParams = $this->pathParameters;
        $urlTplParams['costCodeId'] = $id;
        return new WithCostCodeItemRequestBuilder($urlTplParams, $this->requestAdapter);
    }

    /**
     * Gets an item from the HyperAccountsV2Sdk.GetHyperAccountsV2Client.api.v2.ProjectOnlyTransactions.item collection
     * @param string $id Unique identifier of the item
     * @return \HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\ProjectOnlyTransactions\Item\WithProjectTranItemRequestBuilder
    */
    public function projectOnlyTransactionsById(string $id): \HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\ProjectOnlyTransactions\Item\WithProjectTranItemRequestBuilder {
        $urlTplParams = $this->pathParameters;
        $urlTplParams['projectTranId'] = $id;
        return new \HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\ProjectOnlyTransactions\Item\WithProjectTranItemRequestBuilder($urlTplParams, $this->requestAdapter);
    }

    /**
     * Gets an item from the HyperAccountsV2Sdk.GetHyperAccountsV2Client.api.v2.ProjectResources.item collection
     * @param string $id Unique identifier of the item
     * @return WithResourceItemRequestBuilder
    */
    public function projectResourcesById(string $id): WithResourceItemRequestBuilder {
        $urlTplParams = $this->pathParameters;
        $urlTplParams['resourceId'] = $id;
        return new WithResourceItemRequestBuilder($urlTplParams, $this->requestAdapter);
    }

    /**
     * Gets an item from the HyperAccountsV2Sdk.GetHyperAccountsV2Client.api.v2.Projects.item collection
     * @param string $id Unique identifier of the item
     * @return WithProjectItemRequestBuilder
    */
    public function projectsById(string $id): WithProjectItemRequestBuilder {
        $urlTplParams = $this->pathParameters;
        $urlTplParams['projectId'] = $id;
        return new WithProjectItemRequestBuilder($urlTplParams, $this->requestAdapter);
    }

    /**
     * Gets an item from the HyperAccountsV2Sdk.GetHyperAccountsV2Client.api.v2.ProjectStatuses.item collection
     * @param string $id Unique identifier of the item
     * @return WithStatusItemRequestBuilder
    */
    public function projectStatusesById(string $id): WithStatusItemRequestBuilder {
        $urlTplParams = $this->pathParameters;
        $urlTplParams['statusId'] = $id;
        return new WithStatusItemRequestBuilder($urlTplParams, $this->requestAdapter);
    }

    /**
     * Gets an item from the HyperAccountsV2Sdk.GetHyperAccountsV2Client.api.v2.ProjectTrans.item collection
     * @param string $id Unique identifier of the item
     * @return \HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\ProjectTrans\Item\WithProjectTranItemRequestBuilder
    */
    public function projectTransById(string $id): \HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\ProjectTrans\Item\WithProjectTranItemRequestBuilder {
        $urlTplParams = $this->pathParameters;
        $urlTplParams['projectTranId'] = $id;
        return new \HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\ProjectTrans\Item\WithProjectTranItemRequestBuilder($urlTplParams, $this->requestAdapter);
    }

    /**
     * Gets an item from the HyperAccountsV2Sdk.GetHyperAccountsV2Client.api.v2.PurchaseOrderItems.item collection
     * @param string $id Unique identifier of the item
     * @return \HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\PurchaseOrderItems\Item\WithItemItemRequestBuilder
    */
    public function purchaseOrderItemsById(string $id): \HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\PurchaseOrderItems\Item\WithItemItemRequestBuilder {
        $urlTplParams = $this->pathParameters;
        $urlTplParams['itemId'] = $id;
        return new \HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\PurchaseOrderItems\Item\WithItemItemRequestBuilder($urlTplParams, $this->requestAdapter);
    }

    /**
     * Gets an item from the HyperAccountsV2Sdk.GetHyperAccountsV2Client.api.v2.PurchaseOrders.item collection
     * @param string $id Unique identifier of the item
     * @return \HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\PurchaseOrders\Item\WithOrderNumberItemRequestBuilder
    */
    public function purchaseOrdersById(string $id): \HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\PurchaseOrders\Item\WithOrderNumberItemRequestBuilder {
        $urlTplParams = $this->pathParameters;
        $urlTplParams['orderNumber'] = $id;
        return new \HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\PurchaseOrders\Item\WithOrderNumberItemRequestBuilder($urlTplParams, $this->requestAdapter);
    }

    /**
     * Gets an item from the HyperAccountsV2Sdk.GetHyperAccountsV2Client.api.v2.SalesInvoiceItems.item collection
     * @param string $id Unique identifier of the item
     * @return \HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\SalesInvoiceItems\Item\WithItemItemRequestBuilder
    */
    public function salesInvoiceItemsById(string $id): \HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\SalesInvoiceItems\Item\WithItemItemRequestBuilder {
        $urlTplParams = $this->pathParameters;
        $urlTplParams['itemId'] = $id;
        return new \HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\SalesInvoiceItems\Item\WithItemItemRequestBuilder($urlTplParams, $this->requestAdapter);
    }

    /**
     * Gets an item from the HyperAccountsV2Sdk.GetHyperAccountsV2Client.api.v2.SalesInvoices.item collection
     * @param string $id Unique identifier of the item
     * @return WithInvoiceNumberItemRequestBuilder
    */
    public function salesInvoicesById(string $id): WithInvoiceNumberItemRequestBuilder {
        $urlTplParams = $this->pathParameters;
        $urlTplParams['invoiceNumber'] = $id;
        return new WithInvoiceNumberItemRequestBuilder($urlTplParams, $this->requestAdapter);
    }

    /**
     * Gets an item from the HyperAccountsV2Sdk.GetHyperAccountsV2Client.api.v2.SalesOrderItems.item collection
     * @param string $id Unique identifier of the item
     * @return \HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\SalesOrderItems\Item\WithItemItemRequestBuilder
    */
    public function salesOrderItemsById(string $id): \HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\SalesOrderItems\Item\WithItemItemRequestBuilder {
        $urlTplParams = $this->pathParameters;
        $urlTplParams['itemId'] = $id;
        return new \HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\SalesOrderItems\Item\WithItemItemRequestBuilder($urlTplParams, $this->requestAdapter);
    }

    /**
     * Gets an item from the HyperAccountsV2Sdk.GetHyperAccountsV2Client.api.v2.SalesOrders.item collection
     * @param string $id Unique identifier of the item
     * @return \HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\SalesOrders\Item\WithOrderNumberItemRequestBuilder
    */
    public function salesOrdersById(string $id): \HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\SalesOrders\Item\WithOrderNumberItemRequestBuilder {
        $urlTplParams = $this->pathParameters;
        $urlTplParams['orderNumber'] = $id;
        return new \HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\SalesOrders\Item\WithOrderNumberItemRequestBuilder($urlTplParams, $this->requestAdapter);
    }

    /**
     * Gets an item from the HyperAccountsV2Sdk.GetHyperAccountsV2Client.api.v2.Stocks.item collection
     * @param string $id Unique identifier of the item
     * @return WithStockCodeItemRequestBuilder
    */
    public function stocksById(string $id): WithStockCodeItemRequestBuilder {
        $urlTplParams = $this->pathParameters;
        $urlTplParams['stockCode'] = $id;
        return new WithStockCodeItemRequestBuilder($urlTplParams, $this->requestAdapter);
    }

    /**
     * Gets an item from the HyperAccountsV2Sdk.GetHyperAccountsV2Client.api.v2.StockTransactions.item collection
     * @param string $id Unique identifier of the item
     * @return WithTranNumberItemRequestBuilder
    */
    public function stockTransactionsById(string $id): WithTranNumberItemRequestBuilder {
        $urlTplParams = $this->pathParameters;
        $urlTplParams['tranNumber'] = $id;
        return new WithTranNumberItemRequestBuilder($urlTplParams, $this->requestAdapter);
    }

    /**
     * Gets an item from the HyperAccountsV2Sdk.GetHyperAccountsV2Client.api.v2.Suppliers.item collection
     * @param string $id Unique identifier of the item
     * @return \HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Suppliers\Item\WithAccountRefItemRequestBuilder
    */
    public function suppliersById(string $id): \HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Suppliers\Item\WithAccountRefItemRequestBuilder {
        $urlTplParams = $this->pathParameters;
        $urlTplParams['accountRef'] = $id;
        return new \HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Suppliers\Item\WithAccountRefItemRequestBuilder($urlTplParams, $this->requestAdapter);
    }

    /**
     * Gets an item from the HyperAccountsV2Sdk.GetHyperAccountsV2Client.api.v2.TaxCodes.item collection
     * @param string $id Unique identifier of the item
     * @return WithTaxCodeItemRequestBuilder
    */
    public function taxCodesById(string $id): WithTaxCodeItemRequestBuilder {
        $urlTplParams = $this->pathParameters;
        $urlTplParams['taxCodeId'] = $id;
        return new WithTaxCodeItemRequestBuilder($urlTplParams, $this->requestAdapter);
    }

    /**
     * Gets an item from the HyperAccountsV2Sdk.GetHyperAccountsV2Client.api.v2.TransactionHeaders.item collection
     * @param string $id Unique identifier of the item
     * @return WithHeaderNumberItemRequestBuilder
    */
    public function transactionHeadersById(string $id): WithHeaderNumberItemRequestBuilder {
        $urlTplParams = $this->pathParameters;
        $urlTplParams['headerNumber'] = $id;
        return new WithHeaderNumberItemRequestBuilder($urlTplParams, $this->requestAdapter);
    }

    /**
     * Gets an item from the HyperAccountsV2Sdk.GetHyperAccountsV2Client.api.v2.TransactionSplits.item collection
     * @param string $id Unique identifier of the item
     * @return WithSplitNumberItemRequestBuilder
    */
    public function transactionSplitsById(string $id): WithSplitNumberItemRequestBuilder {
        $urlTplParams = $this->pathParameters;
        $urlTplParams['splitNumber'] = $id;
        return new WithSplitNumberItemRequestBuilder($urlTplParams, $this->requestAdapter);
    }

    /**
     * Gets an item from the HyperAccountsV2Sdk.GetHyperAccountsV2Client.api.v2.TransactionUsages.item collection
     * @param string $id Unique identifier of the item
     * @return WithUsageNumberItemRequestBuilder
    */
    public function transactionUsagesById(string $id): WithUsageNumberItemRequestBuilder {
        $urlTplParams = $this->pathParameters;
        $urlTplParams['usageNumber'] = $id;
        return new WithUsageNumberItemRequestBuilder($urlTplParams, $this->requestAdapter);
    }

    /**
     * Gets an item from the HyperAccountsV2Sdk.GetHyperAccountsV2Client.api.v2.Webhooks.item collection
     * @param string $id Unique identifier of the item
     * @return WebhooksItemRequestBuilder
    */
    public function webhooksById(string $id): WebhooksItemRequestBuilder {
        $urlTplParams = $this->pathParameters;
        $urlTplParams['id'] = $id;
        return new WebhooksItemRequestBuilder($urlTplParams, $this->requestAdapter);
    }

}
