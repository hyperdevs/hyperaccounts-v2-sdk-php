<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Banks;

use Microsoft\Kiota\Abstractions\RequestOption;

/**
 * Configuration for the request such as headers, query parameters, and middleware options.
*/
class BanksRequestBuilderGetRequestConfiguration
{
    /**
     * @var array<string, array<string>|string>|null $headers Request headers
    */
    public ?array $headers = null;

    /**
     * @var array<RequestOption>|null $options Request options
    */
    public ?array $options = null;

    /**
     * @var BanksRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public ?BanksRequestBuilderGetQueryParameters $queryParameters = null;

    /**
     * Instantiates a new BanksRequestBuilderGetQueryParameters.
     * @param int|null $limit Number of items to return
     * @param int|null $offset Ignore number of items
     * @param string|null $q Filter collection. Url encode the following string: [{"Property":"AccountRef","Method":"!=","Value":""}]
     * @return BanksRequestBuilderGetQueryParameters
    */
    public static function addQueryParameters(?int $limit = null, ?int $offset = null, ?string $q = null): BanksRequestBuilderGetQueryParameters {
        return new BanksRequestBuilderGetQueryParameters($limit, $offset, $q);
    }

    /**
     * Instantiates a new BanksRequestBuilderGetRequestConfiguration and sets the default values.
     * @param array<string, array<string>|string>|null $headers Request headers
     * @param array<RequestOption>|null $options Request options
     * @param BanksRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public function __construct(?array $headers = null, ?array $options = null, ?BanksRequestBuilderGetQueryParameters $queryParameters = null) {
        $this->headers = $headers;
        $this->options = $options;
        $this->queryParameters = $queryParameters;
    }

}
