<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\DocumentLinks;

use Microsoft\Kiota\Abstractions\RequestOption;

/**
 * Configuration for the request such as headers, query parameters, and middleware options.
*/
class DocumentLinksRequestBuilderGetRequestConfiguration
{
    /**
     * @var array<string, array<string>|string>|null $headers Request headers
    */
    public ?array $headers = null;

    /**
     * @var array<RequestOption>|null $options Request options
    */
    public ?array $options = null;

    /**
     * @var DocumentLinksRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public ?DocumentLinksRequestBuilderGetQueryParameters $queryParameters = null;

    /**
     * Instantiates a new DocumentLinksRequestBuilderGetQueryParameters.
     * @param int|null $limit Number of items to return
     * @param int|null $offset Ignore number of items
     * @param string|null $q Filter collection. Url encode the following string: [{"Property":"Reference","Method":"!=","Value":""}]
     * @return DocumentLinksRequestBuilderGetQueryParameters
    */
    public static function addQueryParameters(?int $limit = null, ?int $offset = null, ?string $q = null): DocumentLinksRequestBuilderGetQueryParameters {
        return new DocumentLinksRequestBuilderGetQueryParameters($limit, $offset, $q);
    }

    /**
     * Instantiates a new DocumentLinksRequestBuilderGetRequestConfiguration and sets the default values.
     * @param array<string, array<string>|string>|null $headers Request headers
     * @param array<RequestOption>|null $options Request options
     * @param DocumentLinksRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public function __construct(?array $headers = null, ?array $options = null, ?DocumentLinksRequestBuilderGetQueryParameters $queryParameters = null) {
        $this->headers = $headers;
        $this->options = $options;
        $this->queryParameters = $queryParameters;
    }

}
