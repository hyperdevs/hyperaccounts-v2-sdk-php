<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\ProjectBudgets;

use Microsoft\Kiota\Abstractions\RequestOption;

/**
 * Configuration for the request such as headers, query parameters, and middleware options.
*/
class ProjectBudgetsRequestBuilderGetRequestConfiguration
{
    /**
     * @var array<string, array<string>|string>|null $headers Request headers
    */
    public ?array $headers = null;

    /**
     * @var array<RequestOption>|null $options Request options
    */
    public ?array $options = null;

    /**
     * @var ProjectBudgetsRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public ?ProjectBudgetsRequestBuilderGetQueryParameters $queryParameters = null;

    /**
     * Instantiates a new ProjectBudgetsRequestBuilderGetQueryParameters.
     * @param string|null $included Include related resources (project;projectCostCode)
     * @param int|null $limit Number of items to return
     * @param int|null $offset Ignore number of items
     * @param string|null $q Filter collection. Url encode the following string: [{"Property":"Address4","Method":"=","Value":"Hyperext"}]
     * @return ProjectBudgetsRequestBuilderGetQueryParameters
    */
    public static function addQueryParameters(?string $included = null, ?int $limit = null, ?int $offset = null, ?string $q = null): ProjectBudgetsRequestBuilderGetQueryParameters {
        return new ProjectBudgetsRequestBuilderGetQueryParameters($included, $limit, $offset, $q);
    }

    /**
     * Instantiates a new ProjectBudgetsRequestBuilderGetRequestConfiguration and sets the default values.
     * @param array<string, array<string>|string>|null $headers Request headers
     * @param array<RequestOption>|null $options Request options
     * @param ProjectBudgetsRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public function __construct(?array $headers = null, ?array $options = null, ?ProjectBudgetsRequestBuilderGetQueryParameters $queryParameters = null) {
        $this->headers = $headers;
        $this->options = $options;
        $this->queryParameters = $queryParameters;
    }

}
