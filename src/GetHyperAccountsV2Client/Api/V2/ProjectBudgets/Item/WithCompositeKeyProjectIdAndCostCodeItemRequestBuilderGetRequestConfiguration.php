<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\ProjectBudgets\Item;

use Microsoft\Kiota\Abstractions\RequestOption;

/**
 * Configuration for the request such as headers, query parameters, and middleware options.
*/
class WithCompositeKeyProjectIdAndCostCodeItemRequestBuilderGetRequestConfiguration
{
    /**
     * @var array<string, array<string>|string>|null $headers Request headers
    */
    public ?array $headers = null;

    /**
     * @var array<RequestOption>|null $options Request options
    */
    public ?array $options = null;

    /**
     * @var WithCompositeKeyProjectIdAndCostCodeItemRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public ?WithCompositeKeyProjectIdAndCostCodeItemRequestBuilderGetQueryParameters $queryParameters = null;

    /**
     * Instantiates a new WithCompositeKeyProjectIdAndCostCodeItemRequestBuilderGetQueryParameters.
     * @param string|null $included Include related resources (project;projectCostCode)
     * @return WithCompositeKeyProjectIdAndCostCodeItemRequestBuilderGetQueryParameters
    */
    public static function addQueryParameters(?string $included = null): WithCompositeKeyProjectIdAndCostCodeItemRequestBuilderGetQueryParameters {
        return new WithCompositeKeyProjectIdAndCostCodeItemRequestBuilderGetQueryParameters($included);
    }

    /**
     * Instantiates a new WithCompositeKeyProjectIdAndCostCodeItemRequestBuilderGetRequestConfiguration and sets the default values.
     * @param array<string, array<string>|string>|null $headers Request headers
     * @param array<RequestOption>|null $options Request options
     * @param WithCompositeKeyProjectIdAndCostCodeItemRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public function __construct(?array $headers = null, ?array $options = null, ?WithCompositeKeyProjectIdAndCostCodeItemRequestBuilderGetQueryParameters $queryParameters = null) {
        $this->headers = $headers;
        $this->options = $options;
        $this->queryParameters = $queryParameters;
    }

}
