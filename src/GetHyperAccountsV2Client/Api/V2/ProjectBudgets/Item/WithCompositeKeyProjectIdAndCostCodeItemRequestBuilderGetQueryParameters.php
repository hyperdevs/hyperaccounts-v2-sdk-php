<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\ProjectBudgets\Item;

/**
 * Gets a specific project budget.
*/
class WithCompositeKeyProjectIdAndCostCodeItemRequestBuilderGetQueryParameters
{
    /**
     * @var string|null $included Include related resources (project;projectCostCode)
    */
    public ?string $included = null;

    /**
     * Instantiates a new WithCompositeKeyProjectIdAndCostCodeItemRequestBuilderGetQueryParameters and sets the default values.
     * @param string|null $included Include related resources (project;projectCostCode)
    */
    public function __construct(?string $included = null) {
        $this->included = $included;
    }

}
