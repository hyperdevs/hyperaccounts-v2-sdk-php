<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\TransactionUsages\Item;

/**
 * Gets a specific transaction usage
*/
class WithUsageNumberItemRequestBuilderGetQueryParameters
{
    /**
     * @var string|null $included Include items (split)
    */
    public ?string $included = null;

    /**
     * Instantiates a new WithUsageNumberItemRequestBuilderGetQueryParameters and sets the default values.
     * @param string|null $included Include items (split)
    */
    public function __construct(?string $included = null) {
        $this->included = $included;
    }

}
