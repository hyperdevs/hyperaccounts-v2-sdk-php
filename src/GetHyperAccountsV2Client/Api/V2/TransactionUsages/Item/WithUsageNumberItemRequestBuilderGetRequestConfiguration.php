<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\TransactionUsages\Item;

use Microsoft\Kiota\Abstractions\RequestOption;

/**
 * Configuration for the request such as headers, query parameters, and middleware options.
*/
class WithUsageNumberItemRequestBuilderGetRequestConfiguration
{
    /**
     * @var array<string, array<string>|string>|null $headers Request headers
    */
    public ?array $headers = null;

    /**
     * @var array<RequestOption>|null $options Request options
    */
    public ?array $options = null;

    /**
     * @var WithUsageNumberItemRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public ?WithUsageNumberItemRequestBuilderGetQueryParameters $queryParameters = null;

    /**
     * Instantiates a new WithUsageNumberItemRequestBuilderGetQueryParameters.
     * @param string|null $included Include items (split)
     * @return WithUsageNumberItemRequestBuilderGetQueryParameters
    */
    public static function addQueryParameters(?string $included = null): WithUsageNumberItemRequestBuilderGetQueryParameters {
        return new WithUsageNumberItemRequestBuilderGetQueryParameters($included);
    }

    /**
     * Instantiates a new WithUsageNumberItemRequestBuilderGetRequestConfiguration and sets the default values.
     * @param array<string, array<string>|string>|null $headers Request headers
     * @param array<RequestOption>|null $options Request options
     * @param WithUsageNumberItemRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public function __construct(?array $headers = null, ?array $options = null, ?WithUsageNumberItemRequestBuilderGetQueryParameters $queryParameters = null) {
        $this->headers = $headers;
        $this->options = $options;
        $this->queryParameters = $queryParameters;
    }

}
