<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Departments;

use Microsoft\Kiota\Abstractions\RequestOption;

/**
 * Configuration for the request such as headers, query parameters, and middleware options.
*/
class DepartmentsRequestBuilderGetRequestConfiguration
{
    /**
     * @var array<string, array<string>|string>|null $headers Request headers
    */
    public ?array $headers = null;

    /**
     * @var array<RequestOption>|null $options Request options
    */
    public ?array $options = null;

    /**
     * @var DepartmentsRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public ?DepartmentsRequestBuilderGetQueryParameters $queryParameters = null;

    /**
     * Instantiates a new DepartmentsRequestBuilderGetQueryParameters.
     * @param int|null $limit Number of items to return
     * @param int|null $offset Ignore number of items
     * @param string|null $q Filter collection. Url encode the following string: [{"Property":"Number","Method":">","Value":"5"}]
     * @return DepartmentsRequestBuilderGetQueryParameters
    */
    public static function addQueryParameters(?int $limit = null, ?int $offset = null, ?string $q = null): DepartmentsRequestBuilderGetQueryParameters {
        return new DepartmentsRequestBuilderGetQueryParameters($limit, $offset, $q);
    }

    /**
     * Instantiates a new DepartmentsRequestBuilderGetRequestConfiguration and sets the default values.
     * @param array<string, array<string>|string>|null $headers Request headers
     * @param array<RequestOption>|null $options Request options
     * @param DepartmentsRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public function __construct(?array $headers = null, ?array $options = null, ?DepartmentsRequestBuilderGetQueryParameters $queryParameters = null) {
        $this->headers = $headers;
        $this->options = $options;
        $this->queryParameters = $queryParameters;
    }

}
