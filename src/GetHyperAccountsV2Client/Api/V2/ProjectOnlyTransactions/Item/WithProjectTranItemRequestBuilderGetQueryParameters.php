<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\ProjectOnlyTransactions\Item;

/**
 * Gets a specific project only transaction.
*/
class WithProjectTranItemRequestBuilderGetQueryParameters
{
    /**
     * @var string|null $included Include related resources (projectResource;projectTran)
    */
    public ?string $included = null;

    /**
     * Instantiates a new WithProjectTranItemRequestBuilderGetQueryParameters and sets the default values.
     * @param string|null $included Include related resources (projectResource;projectTran)
    */
    public function __construct(?string $included = null) {
        $this->included = $included;
    }

}
