<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\TaxCodes;

use Microsoft\Kiota\Abstractions\RequestOption;

/**
 * Configuration for the request such as headers, query parameters, and middleware options.
*/
class TaxCodesRequestBuilderGetRequestConfiguration
{
    /**
     * @var array<string, array<string>|string>|null $headers Request headers
    */
    public ?array $headers = null;

    /**
     * @var array<RequestOption>|null $options Request options
    */
    public ?array $options = null;

    /**
     * @var TaxCodesRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public ?TaxCodesRequestBuilderGetQueryParameters $queryParameters = null;

    /**
     * Instantiates a new TaxCodesRequestBuilderGetQueryParameters.
     * @param int|null $limit Number of items to return
     * @param int|null $offset Ignore number of items
     * @param string|null $q Filter collection. Url encode the following string: [{"Property":"taxRate","Method":">","Value":0.1}]
     * @return TaxCodesRequestBuilderGetQueryParameters
    */
    public static function addQueryParameters(?int $limit = null, ?int $offset = null, ?string $q = null): TaxCodesRequestBuilderGetQueryParameters {
        return new TaxCodesRequestBuilderGetQueryParameters($limit, $offset, $q);
    }

    /**
     * Instantiates a new TaxCodesRequestBuilderGetRequestConfiguration and sets the default values.
     * @param array<string, array<string>|string>|null $headers Request headers
     * @param array<RequestOption>|null $options Request options
     * @param TaxCodesRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public function __construct(?array $headers = null, ?array $options = null, ?TaxCodesRequestBuilderGetQueryParameters $queryParameters = null) {
        $this->headers = $headers;
        $this->options = $options;
        $this->queryParameters = $queryParameters;
    }

}
