<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Projects\Item;

use Exception;
use Http\Promise\Promise;
use Http\Promise\RejectedPromise;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Projects\Item\Customer\CustomerRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Projects\Item\Project\ProjectRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Projects\Item\ProjectBudgets\ProjectBudgetsRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Projects\Item\ProjectTrans\ProjectTransRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Projects\Item\PurchaseOrders\PurchaseOrdersRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Projects\Item\SalesInvoiceItems\SalesInvoiceItemsRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Projects\Item\SalesInvoices\SalesInvoicesRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Projects\Item\SalesOrderItems\SalesOrderItemsRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Projects\Item\SalesOrders\SalesOrdersRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Projects\Item\TransactionSplits\TransactionSplitsRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models\ProjectGetDto;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models\ProjectPatchDto;
use Microsoft\Kiota\Abstractions\HttpMethod;
use Microsoft\Kiota\Abstractions\RequestAdapter;
use Microsoft\Kiota\Abstractions\RequestInformation;
use Microsoft\Kiota\Abstractions\ResponseHandler;
use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParsableFactory;

/**
 * Builds and executes requests for operations under /api/v2/Projects/{projectId}
*/
class WithProjectItemRequestBuilder
{
    /**
     * The Customer property
    */
    public function customer(): CustomerRequestBuilder {
        return new CustomerRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * @var array<string, mixed> $pathParameters Path parameters for the request
    */
    private array $pathParameters;

    /**
     * The Project property
    */
    public function project(): ProjectRequestBuilder {
        return new ProjectRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The ProjectBudgets property
    */
    public function projectBudgets(): ProjectBudgetsRequestBuilder {
        return new ProjectBudgetsRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The ProjectTrans property
    */
    public function projectTrans(): ProjectTransRequestBuilder {
        return new ProjectTransRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The PurchaseOrders property
    */
    public function purchaseOrders(): PurchaseOrdersRequestBuilder {
        return new PurchaseOrdersRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * @var RequestAdapter $requestAdapter The request adapter to use to execute the requests.
    */
    private RequestAdapter $requestAdapter;

    /**
     * The SalesInvoiceItems property
    */
    public function salesInvoiceItems(): SalesInvoiceItemsRequestBuilder {
        return new SalesInvoiceItemsRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The SalesInvoices property
    */
    public function salesInvoices(): SalesInvoicesRequestBuilder {
        return new SalesInvoicesRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The SalesOrderItems property
    */
    public function salesOrderItems(): SalesOrderItemsRequestBuilder {
        return new SalesOrderItemsRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The SalesOrders property
    */
    public function salesOrders(): SalesOrdersRequestBuilder {
        return new SalesOrdersRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The TransactionSplits property
    */
    public function transactionSplits(): TransactionSplitsRequestBuilder {
        return new TransactionSplitsRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * @var string $urlTemplate Url template to use to build the URL for the current request builder
    */
    private string $urlTemplate;

    /**
     * Instantiates a new WithProjectItemRequestBuilder and sets the default values.
     * @param array<string, mixed>|string $pathParametersOrRawUrl Path parameters for the request or a String representing the raw URL.
     * @param RequestAdapter $requestAdapter The request adapter to use to execute the requests.
    */
    public function __construct($pathParametersOrRawUrl, RequestAdapter $requestAdapter) {
        $this->urlTemplate = '{+baseurl}/api/v2/Projects/{projectId}{?included*,childCollectionLimit*,childCollectionOffset*}';
        $this->requestAdapter = $requestAdapter;
        if (is_array($pathParametersOrRawUrl)) {
            $this->pathParameters = $pathParametersOrRawUrl;
        } else {
            $this->pathParameters = ['request-raw-url' => $pathParametersOrRawUrl];
        }
    }

    /**
     * Gets a specific project.
     * @param WithProjectItemRequestBuilderGetRequestConfiguration|null $requestConfiguration Configuration for the request such as headers, query parameters, and middleware options.
     * @return Promise
    */
    public function get(?WithProjectItemRequestBuilderGetRequestConfiguration $requestConfiguration = null): Promise {
        $requestInfo = $this->toGetRequestInformation($requestConfiguration);
        try {
            return $this->requestAdapter->sendAsync($requestInfo, [ProjectGetDto::class, 'createFromDiscriminatorValue'], null);
        } catch(Exception $ex) {
            return new RejectedPromise($ex);
        }
    }

    /**
     * Patch a project.
     * @param ProjectPatchDto $body The request body
     * @param WithProjectItemRequestBuilderPatchRequestConfiguration|null $requestConfiguration Configuration for the request such as headers, query parameters, and middleware options.
     * @return Promise
    */
    public function patch(ProjectPatchDto $body, ?WithProjectItemRequestBuilderPatchRequestConfiguration $requestConfiguration = null): Promise {
        $requestInfo = $this->toPatchRequestInformation($body, $requestConfiguration);
        try {
            return $this->requestAdapter->sendAsync($requestInfo, [ProjectGetDto::class, 'createFromDiscriminatorValue'], null);
        } catch(Exception $ex) {
            return new RejectedPromise($ex);
        }
    }

    /**
     * Gets a specific project.
     * @param WithProjectItemRequestBuilderGetRequestConfiguration|null $requestConfiguration Configuration for the request such as headers, query parameters, and middleware options.
     * @return RequestInformation
    */
    public function toGetRequestInformation(?WithProjectItemRequestBuilderGetRequestConfiguration $requestConfiguration = null): RequestInformation {
        $requestInfo = new RequestInformation();
        $requestInfo->urlTemplate = $this->urlTemplate;
        $requestInfo->pathParameters = $this->pathParameters;
        $requestInfo->httpMethod = HttpMethod::GET;
        $requestInfo->addHeader('Accept', "application/json");
        if ($requestConfiguration !== null) {
            if ($requestConfiguration->headers !== null) {
                $requestInfo->addHeaders($requestConfiguration->headers);
            }
            if ($requestConfiguration->queryParameters !== null) {
                $requestInfo->setQueryParameters($requestConfiguration->queryParameters);
            }
            if ($requestConfiguration->options !== null) {
                $requestInfo->addRequestOptions(...$requestConfiguration->options);
            }
        }
        return $requestInfo;
    }

    /**
     * Patch a project.
     * @param ProjectPatchDto $body The request body
     * @param WithProjectItemRequestBuilderPatchRequestConfiguration|null $requestConfiguration Configuration for the request such as headers, query parameters, and middleware options.
     * @return RequestInformation
    */
    public function toPatchRequestInformation(ProjectPatchDto $body, ?WithProjectItemRequestBuilderPatchRequestConfiguration $requestConfiguration = null): RequestInformation {
        $requestInfo = new RequestInformation();
        $requestInfo->urlTemplate = $this->urlTemplate;
        $requestInfo->pathParameters = $this->pathParameters;
        $requestInfo->httpMethod = HttpMethod::PATCH;
        $requestInfo->addHeader('Accept', "application/json");
        if ($requestConfiguration !== null) {
            if ($requestConfiguration->headers !== null) {
                $requestInfo->addHeaders($requestConfiguration->headers);
            }
            if ($requestConfiguration->options !== null) {
                $requestInfo->addRequestOptions(...$requestConfiguration->options);
            }
        }
        $requestInfo->setContentFromParsable($this->requestAdapter, "application/json", $body);
        return $requestInfo;
    }

}
