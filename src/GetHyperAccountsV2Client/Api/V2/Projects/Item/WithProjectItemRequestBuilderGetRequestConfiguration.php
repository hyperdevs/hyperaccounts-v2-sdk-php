<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Projects\Item;

use Microsoft\Kiota\Abstractions\RequestOption;

/**
 * Configuration for the request such as headers, query parameters, and middleware options.
*/
class WithProjectItemRequestBuilderGetRequestConfiguration
{
    /**
     * @var array<string, array<string>|string>|null $headers Request headers
    */
    public ?array $headers = null;

    /**
     * @var array<RequestOption>|null $options Request options
    */
    public ?array $options = null;

    /**
     * @var WithProjectItemRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public ?WithProjectItemRequestBuilderGetQueryParameters $queryParameters = null;

    /**
     * Instantiates a new WithProjectItemRequestBuilderGetQueryParameters.
     * @param Int|null $childCollectionLimit
     * @param Int|null $childCollectionOffset
     * @param string|null $included Include related resources (customer;purchaseOrders;salesInvoices;salesInvoiceItems;salesOrders;salesOrderItems;transactionSplits;projectBudgets)
     * @return WithProjectItemRequestBuilderGetQueryParameters
    */
    public static function addQueryParameters(?Int $childCollectionLimit = null, ?Int $childCollectionOffset = null, ?string $included = null): WithProjectItemRequestBuilderGetQueryParameters {
        return new WithProjectItemRequestBuilderGetQueryParameters($childCollectionLimit, $childCollectionOffset, $included);
    }

    /**
     * Instantiates a new WithProjectItemRequestBuilderGetRequestConfiguration and sets the default values.
     * @param array<string, array<string>|string>|null $headers Request headers
     * @param array<RequestOption>|null $options Request options
     * @param WithProjectItemRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public function __construct(?array $headers = null, ?array $options = null, ?WithProjectItemRequestBuilderGetQueryParameters $queryParameters = null) {
        $this->headers = $headers;
        $this->options = $options;
        $this->queryParameters = $queryParameters;
    }

}
