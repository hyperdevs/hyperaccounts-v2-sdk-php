<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Projects\Item\PurchaseOrders;

use Microsoft\Kiota\Abstractions\RequestOption;

/**
 * Configuration for the request such as headers, query parameters, and middleware options.
*/
class PurchaseOrdersRequestBuilderGetRequestConfiguration
{
    /**
     * @var array<string, array<string>|string>|null $headers Request headers
    */
    public ?array $headers = null;

    /**
     * @var array<RequestOption>|null $options Request options
    */
    public ?array $options = null;

    /**
     * @var PurchaseOrdersRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public ?PurchaseOrdersRequestBuilderGetQueryParameters $queryParameters = null;

    /**
     * Instantiates a new PurchaseOrdersRequestBuilderGetQueryParameters.
     * @param int|null $limit Number of items to return
     * @param int|null $offset Number of items to ignore
     * @return PurchaseOrdersRequestBuilderGetQueryParameters
    */
    public static function addQueryParameters(?int $limit = null, ?int $offset = null): PurchaseOrdersRequestBuilderGetQueryParameters {
        return new PurchaseOrdersRequestBuilderGetQueryParameters($limit, $offset);
    }

    /**
     * Instantiates a new PurchaseOrdersRequestBuilderGetRequestConfiguration and sets the default values.
     * @param array<string, array<string>|string>|null $headers Request headers
     * @param array<RequestOption>|null $options Request options
     * @param PurchaseOrdersRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public function __construct(?array $headers = null, ?array $options = null, ?PurchaseOrdersRequestBuilderGetQueryParameters $queryParameters = null) {
        $this->headers = $headers;
        $this->options = $options;
        $this->queryParameters = $queryParameters;
    }

}
