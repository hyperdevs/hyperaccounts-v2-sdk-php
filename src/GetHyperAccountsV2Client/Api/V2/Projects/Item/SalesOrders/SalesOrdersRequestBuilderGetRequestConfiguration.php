<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Projects\Item\SalesOrders;

use Microsoft\Kiota\Abstractions\RequestOption;

/**
 * Configuration for the request such as headers, query parameters, and middleware options.
*/
class SalesOrdersRequestBuilderGetRequestConfiguration
{
    /**
     * @var array<string, array<string>|string>|null $headers Request headers
    */
    public ?array $headers = null;

    /**
     * @var array<RequestOption>|null $options Request options
    */
    public ?array $options = null;

    /**
     * @var SalesOrdersRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public ?SalesOrdersRequestBuilderGetQueryParameters $queryParameters = null;

    /**
     * Instantiates a new SalesOrdersRequestBuilderGetQueryParameters.
     * @param int|null $limit Number of items to return
     * @param int|null $offset Number of items to ignore
     * @return SalesOrdersRequestBuilderGetQueryParameters
    */
    public static function addQueryParameters(?int $limit = null, ?int $offset = null): SalesOrdersRequestBuilderGetQueryParameters {
        return new SalesOrdersRequestBuilderGetQueryParameters($limit, $offset);
    }

    /**
     * Instantiates a new SalesOrdersRequestBuilderGetRequestConfiguration and sets the default values.
     * @param array<string, array<string>|string>|null $headers Request headers
     * @param array<RequestOption>|null $options Request options
     * @param SalesOrdersRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public function __construct(?array $headers = null, ?array $options = null, ?SalesOrdersRequestBuilderGetQueryParameters $queryParameters = null) {
        $this->headers = $headers;
        $this->options = $options;
        $this->queryParameters = $queryParameters;
    }

}
