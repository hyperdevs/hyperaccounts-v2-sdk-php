<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\ChartOfAccountsCategories;

use Microsoft\Kiota\Abstractions\RequestOption;

/**
 * Configuration for the request such as headers, query parameters, and middleware options.
*/
class ChartOfAccountsCategoriesRequestBuilderGetRequestConfiguration
{
    /**
     * @var array<string, array<string>|string>|null $headers Request headers
    */
    public ?array $headers = null;

    /**
     * @var array<RequestOption>|null $options Request options
    */
    public ?array $options = null;

    /**
     * @var ChartOfAccountsCategoriesRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public ?ChartOfAccountsCategoriesRequestBuilderGetQueryParameters $queryParameters = null;

    /**
     * Instantiates a new ChartOfAccountsCategoriesRequestBuilderGetQueryParameters.
     * @param string|null $included Include related resources (chart;categoryTitle)
     * @param int|null $limit Number of items to return
     * @param int|null $offset Ignore number of items
     * @param string|null $q Filter collection. Url encode the following string: [{"Property":"Title","Method":"!=","Value":""}]
     * @return ChartOfAccountsCategoriesRequestBuilderGetQueryParameters
    */
    public static function addQueryParameters(?string $included = null, ?int $limit = null, ?int $offset = null, ?string $q = null): ChartOfAccountsCategoriesRequestBuilderGetQueryParameters {
        return new ChartOfAccountsCategoriesRequestBuilderGetQueryParameters($included, $limit, $offset, $q);
    }

    /**
     * Instantiates a new ChartOfAccountsCategoriesRequestBuilderGetRequestConfiguration and sets the default values.
     * @param array<string, array<string>|string>|null $headers Request headers
     * @param array<RequestOption>|null $options Request options
     * @param ChartOfAccountsCategoriesRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public function __construct(?array $headers = null, ?array $options = null, ?ChartOfAccountsCategoriesRequestBuilderGetQueryParameters $queryParameters = null) {
        $this->headers = $headers;
        $this->options = $options;
        $this->queryParameters = $queryParameters;
    }

}
