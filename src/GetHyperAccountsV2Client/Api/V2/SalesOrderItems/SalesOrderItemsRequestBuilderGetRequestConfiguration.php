<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\SalesOrderItems;

use Microsoft\Kiota\Abstractions\RequestOption;

/**
 * Configuration for the request such as headers, query parameters, and middleware options.
*/
class SalesOrderItemsRequestBuilderGetRequestConfiguration
{
    /**
     * @var array<string, array<string>|string>|null $headers Request headers
    */
    public ?array $headers = null;

    /**
     * @var array<RequestOption>|null $options Request options
    */
    public ?array $options = null;

    /**
     * @var SalesOrderItemsRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public ?SalesOrderItemsRequestBuilderGetQueryParameters $queryParameters = null;

    /**
     * Instantiates a new SalesOrderItemsRequestBuilderGetQueryParameters.
     * @param Int|null $childCollectionLimit
     * @param Int|null $childCollectionOffset
     * @param string|null $included Include items (salesOrder;stock;project;nominalCode)
     * @param int|null $limit Number of items to return
     * @param int|null $offset Ignore number of items
     * @param string|null $q Filters
     * @return SalesOrderItemsRequestBuilderGetQueryParameters
    */
    public static function addQueryParameters(?Int $childCollectionLimit = null, ?Int $childCollectionOffset = null, ?string $included = null, ?int $limit = null, ?int $offset = null, ?string $q = null): SalesOrderItemsRequestBuilderGetQueryParameters {
        return new SalesOrderItemsRequestBuilderGetQueryParameters($childCollectionLimit, $childCollectionOffset, $included, $limit, $offset, $q);
    }

    /**
     * Instantiates a new SalesOrderItemsRequestBuilderGetRequestConfiguration and sets the default values.
     * @param array<string, array<string>|string>|null $headers Request headers
     * @param array<RequestOption>|null $options Request options
     * @param SalesOrderItemsRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public function __construct(?array $headers = null, ?array $options = null, ?SalesOrderItemsRequestBuilderGetQueryParameters $queryParameters = null) {
        $this->headers = $headers;
        $this->options = $options;
        $this->queryParameters = $queryParameters;
    }

}
