<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\ProjectTrans\Item;

use Microsoft\Kiota\Abstractions\RequestOption;

/**
 * Configuration for the request such as headers, query parameters, and middleware options.
*/
class WithProjectTranItemRequestBuilderGetRequestConfiguration
{
    /**
     * @var array<string, array<string>|string>|null $headers Request headers
    */
    public ?array $headers = null;

    /**
     * @var array<RequestOption>|null $options Request options
    */
    public ?array $options = null;

    /**
     * @var WithProjectTranItemRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public ?WithProjectTranItemRequestBuilderGetQueryParameters $queryParameters = null;

    /**
     * Instantiates a new WithProjectTranItemRequestBuilderGetQueryParameters.
     * @param string|null $included Include related resources (projectCostCode;project;projectOnlyTransaction)
     * @return WithProjectTranItemRequestBuilderGetQueryParameters
    */
    public static function addQueryParameters(?string $included = null): WithProjectTranItemRequestBuilderGetQueryParameters {
        return new WithProjectTranItemRequestBuilderGetQueryParameters($included);
    }

    /**
     * Instantiates a new WithProjectTranItemRequestBuilderGetRequestConfiguration and sets the default values.
     * @param array<string, array<string>|string>|null $headers Request headers
     * @param array<RequestOption>|null $options Request options
     * @param WithProjectTranItemRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public function __construct(?array $headers = null, ?array $options = null, ?WithProjectTranItemRequestBuilderGetQueryParameters $queryParameters = null) {
        $this->headers = $headers;
        $this->options = $options;
        $this->queryParameters = $queryParameters;
    }

}
