<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\ProjectTrans\Item;

/**
 * Gets a specific project tran.
*/
class WithProjectTranItemRequestBuilderGetQueryParameters
{
    /**
     * @var string|null $included Include related resources (projectCostCode;project;projectOnlyTransaction)
    */
    public ?string $included = null;

    /**
     * Instantiates a new WithProjectTranItemRequestBuilderGetQueryParameters and sets the default values.
     * @param string|null $included Include related resources (projectCostCode;project;projectOnlyTransaction)
    */
    public function __construct(?string $included = null) {
        $this->included = $included;
    }

}
