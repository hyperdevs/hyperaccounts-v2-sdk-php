<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\GoodsDispatchNotes\Item;

/**
 * Gets a specific goods dispatch note.
*/
class WithCompositeKeyGdnNumberAndItemNumberItemRequestBuilderGetQueryParameters
{
    /**
     * @var Int|null $childCollectionLimit
    */
    public ?Int $childCollectionLimit = null;

    /**
     * @var Int|null $childCollectionOffset
    */
    public ?Int $childCollectionOffset = null;

    /**
     * @var string|null $compositeKeyGdnNumberAndItemNumber Reference linked to goods dispatch note
    */
    public ?string $compositeKeyGdnNumberAndItemNumber = null;

    /**
     * @var string|null $included Include related resources (salesOrder;customer;stock)
    */
    public ?string $included = null;

    /**
     * Instantiates a new WithCompositeKeyGdnNumberAndItemNumberItemRequestBuilderGetQueryParameters and sets the default values.
     * @param Int|null $childCollectionLimit
     * @param Int|null $childCollectionOffset
     * @param string|null $compositeKeyGdnNumberAndItemNumber Reference linked to goods dispatch note
     * @param string|null $included Include related resources (salesOrder;customer;stock)
    */
    public function __construct(?Int $childCollectionLimit = null, ?Int $childCollectionOffset = null, ?string $compositeKeyGdnNumberAndItemNumber = null, ?string $included = null) {
        $this->childCollectionLimit = $childCollectionLimit;
        $this->childCollectionOffset = $childCollectionOffset;
        $this->compositeKeyGdnNumberAndItemNumber = $compositeKeyGdnNumberAndItemNumber;
        $this->included = $included;
    }

}
