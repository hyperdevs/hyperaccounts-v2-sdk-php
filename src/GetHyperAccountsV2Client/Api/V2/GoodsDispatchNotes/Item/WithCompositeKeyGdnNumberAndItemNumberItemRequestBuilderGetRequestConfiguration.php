<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\GoodsDispatchNotes\Item;

use Microsoft\Kiota\Abstractions\RequestOption;

/**
 * Configuration for the request such as headers, query parameters, and middleware options.
*/
class WithCompositeKeyGdnNumberAndItemNumberItemRequestBuilderGetRequestConfiguration
{
    /**
     * @var array<string, array<string>|string>|null $headers Request headers
    */
    public ?array $headers = null;

    /**
     * @var array<RequestOption>|null $options Request options
    */
    public ?array $options = null;

    /**
     * @var WithCompositeKeyGdnNumberAndItemNumberItemRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public ?WithCompositeKeyGdnNumberAndItemNumberItemRequestBuilderGetQueryParameters $queryParameters = null;

    /**
     * Instantiates a new WithCompositeKeyGdnNumberAndItemNumberItemRequestBuilderGetQueryParameters.
     * @param Int|null $childCollectionLimit
     * @param Int|null $childCollectionOffset
     * @param string|null $compositeKeyGdnNumberAndItemNumber Reference linked to goods dispatch note
     * @param string|null $included Include related resources (salesOrder;customer;stock)
     * @return WithCompositeKeyGdnNumberAndItemNumberItemRequestBuilderGetQueryParameters
    */
    public static function addQueryParameters(?Int $childCollectionLimit = null, ?Int $childCollectionOffset = null, ?string $compositeKeyGdnNumberAndItemNumber = null, ?string $included = null): WithCompositeKeyGdnNumberAndItemNumberItemRequestBuilderGetQueryParameters {
        return new WithCompositeKeyGdnNumberAndItemNumberItemRequestBuilderGetQueryParameters($childCollectionLimit, $childCollectionOffset, $compositeKeyGdnNumberAndItemNumber, $included);
    }

    /**
     * Instantiates a new WithCompositeKeyGdnNumberAndItemNumberItemRequestBuilderGetRequestConfiguration and sets the default values.
     * @param array<string, array<string>|string>|null $headers Request headers
     * @param array<RequestOption>|null $options Request options
     * @param WithCompositeKeyGdnNumberAndItemNumberItemRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public function __construct(?array $headers = null, ?array $options = null, ?WithCompositeKeyGdnNumberAndItemNumberItemRequestBuilderGetQueryParameters $queryParameters = null) {
        $this->headers = $headers;
        $this->options = $options;
        $this->queryParameters = $queryParameters;
    }

}
