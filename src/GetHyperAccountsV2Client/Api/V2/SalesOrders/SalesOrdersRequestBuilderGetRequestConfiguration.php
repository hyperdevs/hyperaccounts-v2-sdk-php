<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\SalesOrders;

use Microsoft\Kiota\Abstractions\RequestOption;

/**
 * Configuration for the request such as headers, query parameters, and middleware options.
*/
class SalesOrdersRequestBuilderGetRequestConfiguration
{
    /**
     * @var array<string, array<string>|string>|null $headers Request headers
    */
    public ?array $headers = null;

    /**
     * @var array<RequestOption>|null $options Request options
    */
    public ?array $options = null;

    /**
     * @var SalesOrdersRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public ?SalesOrdersRequestBuilderGetQueryParameters $queryParameters = null;

    /**
     * Instantiates a new SalesOrdersRequestBuilderGetQueryParameters.
     * @param Int|null $childCollectionLimit
     * @param Int|null $childCollectionOffset
     * @param string|null $included Include related resources (customer;items;project)
     * @param int|null $limit Number of items to return
     * @param int|null $offset Ignore number of items
     * @param string|null $q Filter collection. Url encode the following string: [{"Property":"Address4","Method":"=","Value":"Hyperext"}]
     * @return SalesOrdersRequestBuilderGetQueryParameters
    */
    public static function addQueryParameters(?Int $childCollectionLimit = null, ?Int $childCollectionOffset = null, ?string $included = null, ?int $limit = null, ?int $offset = null, ?string $q = null): SalesOrdersRequestBuilderGetQueryParameters {
        return new SalesOrdersRequestBuilderGetQueryParameters($childCollectionLimit, $childCollectionOffset, $included, $limit, $offset, $q);
    }

    /**
     * Instantiates a new SalesOrdersRequestBuilderGetRequestConfiguration and sets the default values.
     * @param array<string, array<string>|string>|null $headers Request headers
     * @param array<RequestOption>|null $options Request options
     * @param SalesOrdersRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public function __construct(?array $headers = null, ?array $options = null, ?SalesOrdersRequestBuilderGetQueryParameters $queryParameters = null) {
        $this->headers = $headers;
        $this->options = $options;
        $this->queryParameters = $queryParameters;
    }

}
