<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\SalesOrders\Item\GoodsDispatchNotes;

use Microsoft\Kiota\Abstractions\RequestOption;

/**
 * Configuration for the request such as headers, query parameters, and middleware options.
*/
class GoodsDispatchNotesRequestBuilderGetRequestConfiguration
{
    /**
     * @var array<string, array<string>|string>|null $headers Request headers
    */
    public ?array $headers = null;

    /**
     * @var array<RequestOption>|null $options Request options
    */
    public ?array $options = null;

    /**
     * @var GoodsDispatchNotesRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public ?GoodsDispatchNotesRequestBuilderGetQueryParameters $queryParameters = null;

    /**
     * Instantiates a new GoodsDispatchNotesRequestBuilderGetQueryParameters.
     * @param int|null $limit Number of items to return
     * @param int|null $offset Ignore number of items
     * @return GoodsDispatchNotesRequestBuilderGetQueryParameters
    */
    public static function addQueryParameters(?int $limit = null, ?int $offset = null): GoodsDispatchNotesRequestBuilderGetQueryParameters {
        return new GoodsDispatchNotesRequestBuilderGetQueryParameters($limit, $offset);
    }

    /**
     * Instantiates a new GoodsDispatchNotesRequestBuilderGetRequestConfiguration and sets the default values.
     * @param array<string, array<string>|string>|null $headers Request headers
     * @param array<RequestOption>|null $options Request options
     * @param GoodsDispatchNotesRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public function __construct(?array $headers = null, ?array $options = null, ?GoodsDispatchNotesRequestBuilderGetQueryParameters $queryParameters = null) {
        $this->headers = $headers;
        $this->options = $options;
        $this->queryParameters = $queryParameters;
    }

}
