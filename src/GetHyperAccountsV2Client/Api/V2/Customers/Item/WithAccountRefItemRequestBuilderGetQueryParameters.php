<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Customers\Item;

/**
 * Gets a specific customer.
*/
class WithAccountRefItemRequestBuilderGetQueryParameters
{
    /**
     * @var Int|null $childCollectionLimit
    */
    public ?Int $childCollectionLimit = null;

    /**
     * @var Int|null $childCollectionOffset
    */
    public ?Int $childCollectionOffset = null;

    /**
     * @var string|null $included Include related resources (addresses;invoices;projects)
    */
    public ?string $included = null;

    /**
     * Instantiates a new WithAccountRefItemRequestBuilderGetQueryParameters and sets the default values.
     * @param Int|null $childCollectionLimit
     * @param Int|null $childCollectionOffset
     * @param string|null $included Include related resources (addresses;invoices;projects)
    */
    public function __construct(?Int $childCollectionLimit = null, ?Int $childCollectionOffset = null, ?string $included = null) {
        $this->childCollectionLimit = $childCollectionLimit;
        $this->childCollectionOffset = $childCollectionOffset;
        $this->included = $included;
    }

}
