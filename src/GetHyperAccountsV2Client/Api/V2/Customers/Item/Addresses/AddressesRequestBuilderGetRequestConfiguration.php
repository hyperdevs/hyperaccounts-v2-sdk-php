<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Customers\Item\Addresses;

use Microsoft\Kiota\Abstractions\RequestOption;

/**
 * Configuration for the request such as headers, query parameters, and middleware options.
*/
class AddressesRequestBuilderGetRequestConfiguration
{
    /**
     * @var array<string, array<string>|string>|null $headers Request headers
    */
    public ?array $headers = null;

    /**
     * @var array<RequestOption>|null $options Request options
    */
    public ?array $options = null;

    /**
     * @var AddressesRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public ?AddressesRequestBuilderGetQueryParameters $queryParameters = null;

    /**
     * Instantiates a new AddressesRequestBuilderGetQueryParameters.
     * @param int|null $limit Number of items to return
     * @param int|null $offset Number of items to ignore
     * @return AddressesRequestBuilderGetQueryParameters
    */
    public static function addQueryParameters(?int $limit = null, ?int $offset = null): AddressesRequestBuilderGetQueryParameters {
        return new AddressesRequestBuilderGetQueryParameters($limit, $offset);
    }

    /**
     * Instantiates a new AddressesRequestBuilderGetRequestConfiguration and sets the default values.
     * @param array<string, array<string>|string>|null $headers Request headers
     * @param array<RequestOption>|null $options Request options
     * @param AddressesRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public function __construct(?array $headers = null, ?array $options = null, ?AddressesRequestBuilderGetQueryParameters $queryParameters = null) {
        $this->headers = $headers;
        $this->options = $options;
        $this->queryParameters = $queryParameters;
    }

}
