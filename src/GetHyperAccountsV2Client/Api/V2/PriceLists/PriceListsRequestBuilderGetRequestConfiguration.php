<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\PriceLists;

use Microsoft\Kiota\Abstractions\RequestOption;

/**
 * Configuration for the request such as headers, query parameters, and middleware options.
*/
class PriceListsRequestBuilderGetRequestConfiguration
{
    /**
     * @var array<string, array<string>|string>|null $headers Request headers
    */
    public ?array $headers = null;

    /**
     * @var array<RequestOption>|null $options Request options
    */
    public ?array $options = null;

    /**
     * @var PriceListsRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public ?PriceListsRequestBuilderGetQueryParameters $queryParameters = null;

    /**
     * Instantiates a new PriceListsRequestBuilderGetQueryParameters.
     * @param int|null $limit Number of items to return
     * @param int|null $offset Ignore number of items
     * @param string|null $q Filter collection. Url encode the following string: [{"Property":"Address4","Method":"=","Value":"Hyperext"}]
     * @return PriceListsRequestBuilderGetQueryParameters
    */
    public static function addQueryParameters(?int $limit = null, ?int $offset = null, ?string $q = null): PriceListsRequestBuilderGetQueryParameters {
        return new PriceListsRequestBuilderGetQueryParameters($limit, $offset, $q);
    }

    /**
     * Instantiates a new PriceListsRequestBuilderGetRequestConfiguration and sets the default values.
     * @param array<string, array<string>|string>|null $headers Request headers
     * @param array<RequestOption>|null $options Request options
     * @param PriceListsRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public function __construct(?array $headers = null, ?array $options = null, ?PriceListsRequestBuilderGetQueryParameters $queryParameters = null) {
        $this->headers = $headers;
        $this->options = $options;
        $this->queryParameters = $queryParameters;
    }

}
