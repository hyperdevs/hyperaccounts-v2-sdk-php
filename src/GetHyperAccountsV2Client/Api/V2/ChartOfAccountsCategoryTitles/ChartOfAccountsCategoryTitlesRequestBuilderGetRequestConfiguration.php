<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\ChartOfAccountsCategoryTitles;

use Microsoft\Kiota\Abstractions\RequestOption;

/**
 * Configuration for the request such as headers, query parameters, and middleware options.
*/
class ChartOfAccountsCategoryTitlesRequestBuilderGetRequestConfiguration
{
    /**
     * @var array<string, array<string>|string>|null $headers Request headers
    */
    public ?array $headers = null;

    /**
     * @var array<RequestOption>|null $options Request options
    */
    public ?array $options = null;

    /**
     * @var ChartOfAccountsCategoryTitlesRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public ?ChartOfAccountsCategoryTitlesRequestBuilderGetQueryParameters $queryParameters = null;

    /**
     * Instantiates a new ChartOfAccountsCategoryTitlesRequestBuilderGetQueryParameters.
     * @param string|null $included Include related resources (chart;categories)
     * @param int|null $limit Number of items to return
     * @param int|null $offset Ignore number of items
     * @param string|null $q Filter collection. Url encode the following string: [{"Property":"AccountRef","Method":"!=","Value":""}]
     * @return ChartOfAccountsCategoryTitlesRequestBuilderGetQueryParameters
    */
    public static function addQueryParameters(?string $included = null, ?int $limit = null, ?int $offset = null, ?string $q = null): ChartOfAccountsCategoryTitlesRequestBuilderGetQueryParameters {
        return new ChartOfAccountsCategoryTitlesRequestBuilderGetQueryParameters($included, $limit, $offset, $q);
    }

    /**
     * Instantiates a new ChartOfAccountsCategoryTitlesRequestBuilderGetRequestConfiguration and sets the default values.
     * @param array<string, array<string>|string>|null $headers Request headers
     * @param array<RequestOption>|null $options Request options
     * @param ChartOfAccountsCategoryTitlesRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public function __construct(?array $headers = null, ?array $options = null, ?ChartOfAccountsCategoryTitlesRequestBuilderGetQueryParameters $queryParameters = null) {
        $this->headers = $headers;
        $this->options = $options;
        $this->queryParameters = $queryParameters;
    }

}
