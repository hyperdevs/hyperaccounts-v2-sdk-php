<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\ChartOfAccountsCategoryTitles\Item;

/**
 * Gets a specific chart of accounts.
*/
class WithCompositeKeyChartAndCategoryItemRequestBuilderGetQueryParameters
{
    /**
     * @var string|null $included Include related resources (chart;categories)
    */
    public ?string $included = null;

    /**
     * Instantiates a new WithCompositeKeyChartAndCategoryItemRequestBuilderGetQueryParameters and sets the default values.
     * @param string|null $included Include related resources (chart;categories)
    */
    public function __construct(?string $included = null) {
        $this->included = $included;
    }

}
