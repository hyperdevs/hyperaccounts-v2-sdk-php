<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\ChartOfAccountsCategoryTitles\Item;

use Microsoft\Kiota\Abstractions\RequestOption;

/**
 * Configuration for the request such as headers, query parameters, and middleware options.
*/
class WithCompositeKeyChartAndCategoryItemRequestBuilderGetRequestConfiguration
{
    /**
     * @var array<string, array<string>|string>|null $headers Request headers
    */
    public ?array $headers = null;

    /**
     * @var array<RequestOption>|null $options Request options
    */
    public ?array $options = null;

    /**
     * @var WithCompositeKeyChartAndCategoryItemRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public ?WithCompositeKeyChartAndCategoryItemRequestBuilderGetQueryParameters $queryParameters = null;

    /**
     * Instantiates a new WithCompositeKeyChartAndCategoryItemRequestBuilderGetQueryParameters.
     * @param string|null $included Include related resources (chart;categories)
     * @return WithCompositeKeyChartAndCategoryItemRequestBuilderGetQueryParameters
    */
    public static function addQueryParameters(?string $included = null): WithCompositeKeyChartAndCategoryItemRequestBuilderGetQueryParameters {
        return new WithCompositeKeyChartAndCategoryItemRequestBuilderGetQueryParameters($included);
    }

    /**
     * Instantiates a new WithCompositeKeyChartAndCategoryItemRequestBuilderGetRequestConfiguration and sets the default values.
     * @param array<string, array<string>|string>|null $headers Request headers
     * @param array<RequestOption>|null $options Request options
     * @param WithCompositeKeyChartAndCategoryItemRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public function __construct(?array $headers = null, ?array $options = null, ?WithCompositeKeyChartAndCategoryItemRequestBuilderGetQueryParameters $queryParameters = null) {
        $this->headers = $headers;
        $this->options = $options;
        $this->queryParameters = $queryParameters;
    }

}
