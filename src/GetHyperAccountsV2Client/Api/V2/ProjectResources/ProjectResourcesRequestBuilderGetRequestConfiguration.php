<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\ProjectResources;

use Microsoft\Kiota\Abstractions\RequestOption;

/**
 * Configuration for the request such as headers, query parameters, and middleware options.
*/
class ProjectResourcesRequestBuilderGetRequestConfiguration
{
    /**
     * @var array<string, array<string>|string>|null $headers Request headers
    */
    public ?array $headers = null;

    /**
     * @var array<RequestOption>|null $options Request options
    */
    public ?array $options = null;

    /**
     * @var ProjectResourcesRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public ?ProjectResourcesRequestBuilderGetQueryParameters $queryParameters = null;

    /**
     * Instantiates a new ProjectResourcesRequestBuilderGetQueryParameters.
     * @param string|null $included Include related resources (projectOnlyTransactions)
     * @param int|null $limit Number of items to return
     * @param int|null $offset Ignore number of items
     * @param string|null $q Filter collection. Url encode the following string: [{"Property":"Number","Method":">","Value":"5"}]
     * @return ProjectResourcesRequestBuilderGetQueryParameters
    */
    public static function addQueryParameters(?string $included = null, ?int $limit = null, ?int $offset = null, ?string $q = null): ProjectResourcesRequestBuilderGetQueryParameters {
        return new ProjectResourcesRequestBuilderGetQueryParameters($included, $limit, $offset, $q);
    }

    /**
     * Instantiates a new ProjectResourcesRequestBuilderGetRequestConfiguration and sets the default values.
     * @param array<string, array<string>|string>|null $headers Request headers
     * @param array<RequestOption>|null $options Request options
     * @param ProjectResourcesRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public function __construct(?array $headers = null, ?array $options = null, ?ProjectResourcesRequestBuilderGetQueryParameters $queryParameters = null) {
        $this->headers = $headers;
        $this->options = $options;
        $this->queryParameters = $queryParameters;
    }

}
