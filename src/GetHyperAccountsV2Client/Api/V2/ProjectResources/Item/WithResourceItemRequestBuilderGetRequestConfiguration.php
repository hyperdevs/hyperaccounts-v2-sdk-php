<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\ProjectResources\Item;

use Microsoft\Kiota\Abstractions\RequestOption;

/**
 * Configuration for the request such as headers, query parameters, and middleware options.
*/
class WithResourceItemRequestBuilderGetRequestConfiguration
{
    /**
     * @var array<string, array<string>|string>|null $headers Request headers
    */
    public ?array $headers = null;

    /**
     * @var array<RequestOption>|null $options Request options
    */
    public ?array $options = null;

    /**
     * @var WithResourceItemRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public ?WithResourceItemRequestBuilderGetQueryParameters $queryParameters = null;

    /**
     * Instantiates a new WithResourceItemRequestBuilderGetQueryParameters.
     * @param string|null $included Include related resources (projectOnlyTransactions)
     * @return WithResourceItemRequestBuilderGetQueryParameters
    */
    public static function addQueryParameters(?string $included = null): WithResourceItemRequestBuilderGetQueryParameters {
        return new WithResourceItemRequestBuilderGetQueryParameters($included);
    }

    /**
     * Instantiates a new WithResourceItemRequestBuilderGetRequestConfiguration and sets the default values.
     * @param array<string, array<string>|string>|null $headers Request headers
     * @param array<RequestOption>|null $options Request options
     * @param WithResourceItemRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public function __construct(?array $headers = null, ?array $options = null, ?WithResourceItemRequestBuilderGetQueryParameters $queryParameters = null) {
        $this->headers = $headers;
        $this->options = $options;
        $this->queryParameters = $queryParameters;
    }

}
