<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\ProjectResources\Item\ProjectOnlyTransactions;

use Microsoft\Kiota\Abstractions\RequestOption;

/**
 * Configuration for the request such as headers, query parameters, and middleware options.
*/
class ProjectOnlyTransactionsRequestBuilderGetRequestConfiguration
{
    /**
     * @var array<string, array<string>|string>|null $headers Request headers
    */
    public ?array $headers = null;

    /**
     * @var array<RequestOption>|null $options Request options
    */
    public ?array $options = null;

    /**
     * @var ProjectOnlyTransactionsRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public ?ProjectOnlyTransactionsRequestBuilderGetQueryParameters $queryParameters = null;

    /**
     * Instantiates a new ProjectOnlyTransactionsRequestBuilderGetQueryParameters.
     * @param int|null $limit Number of items to return
     * @param int|null $offset Number of items to ignore
     * @return ProjectOnlyTransactionsRequestBuilderGetQueryParameters
    */
    public static function addQueryParameters(?int $limit = null, ?int $offset = null): ProjectOnlyTransactionsRequestBuilderGetQueryParameters {
        return new ProjectOnlyTransactionsRequestBuilderGetQueryParameters($limit, $offset);
    }

    /**
     * Instantiates a new ProjectOnlyTransactionsRequestBuilderGetRequestConfiguration and sets the default values.
     * @param array<string, array<string>|string>|null $headers Request headers
     * @param array<RequestOption>|null $options Request options
     * @param ProjectOnlyTransactionsRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public function __construct(?array $headers = null, ?array $options = null, ?ProjectOnlyTransactionsRequestBuilderGetQueryParameters $queryParameters = null) {
        $this->headers = $headers;
        $this->options = $options;
        $this->queryParameters = $queryParameters;
    }

}
