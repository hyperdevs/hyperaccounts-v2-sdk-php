<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\ProjectResources\Item;

/**
 * Gets a specific project resource.
*/
class WithResourceItemRequestBuilderGetQueryParameters
{
    /**
     * @var string|null $included Include related resources (projectOnlyTransactions)
    */
    public ?string $included = null;

    /**
     * Instantiates a new WithResourceItemRequestBuilderGetQueryParameters and sets the default values.
     * @param string|null $included Include related resources (projectOnlyTransactions)
    */
    public function __construct(?string $included = null) {
        $this->included = $included;
    }

}
