<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\PurchaseOrderItems\Item;

/**
 * Gets a specific purchase order item.
*/
class WithItemItemRequestBuilderGetQueryParameters
{
    /**
     * @var string|null $included Include items (purchaseOrder;stock;nominalCode)
    */
    public ?string $included = null;

    /**
     * Instantiates a new WithItemItemRequestBuilderGetQueryParameters and sets the default values.
     * @param string|null $included Include items (purchaseOrder;stock;nominalCode)
    */
    public function __construct(?string $included = null) {
        $this->included = $included;
    }

}
