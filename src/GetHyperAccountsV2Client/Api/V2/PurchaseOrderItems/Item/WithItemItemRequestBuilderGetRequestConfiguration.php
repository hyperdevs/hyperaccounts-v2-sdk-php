<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\PurchaseOrderItems\Item;

use Microsoft\Kiota\Abstractions\RequestOption;

/**
 * Configuration for the request such as headers, query parameters, and middleware options.
*/
class WithItemItemRequestBuilderGetRequestConfiguration
{
    /**
     * @var array<string, array<string>|string>|null $headers Request headers
    */
    public ?array $headers = null;

    /**
     * @var array<RequestOption>|null $options Request options
    */
    public ?array $options = null;

    /**
     * @var WithItemItemRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public ?WithItemItemRequestBuilderGetQueryParameters $queryParameters = null;

    /**
     * Instantiates a new WithItemItemRequestBuilderGetQueryParameters.
     * @param string|null $included Include items (purchaseOrder;stock;nominalCode)
     * @return WithItemItemRequestBuilderGetQueryParameters
    */
    public static function addQueryParameters(?string $included = null): WithItemItemRequestBuilderGetQueryParameters {
        return new WithItemItemRequestBuilderGetQueryParameters($included);
    }

    /**
     * Instantiates a new WithItemItemRequestBuilderGetRequestConfiguration and sets the default values.
     * @param array<string, array<string>|string>|null $headers Request headers
     * @param array<RequestOption>|null $options Request options
     * @param WithItemItemRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public function __construct(?array $headers = null, ?array $options = null, ?WithItemItemRequestBuilderGetQueryParameters $queryParameters = null) {
        $this->headers = $headers;
        $this->options = $options;
        $this->queryParameters = $queryParameters;
    }

}
