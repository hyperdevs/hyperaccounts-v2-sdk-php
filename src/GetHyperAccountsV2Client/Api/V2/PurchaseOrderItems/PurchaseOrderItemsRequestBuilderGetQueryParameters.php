<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\PurchaseOrderItems;

/**
 * Gets a list of purchase order items.
*/
class PurchaseOrderItemsRequestBuilderGetQueryParameters
{
    /**
     * @var string|null $included Include items (purchaseOrder;stock;nominalCode)
    */
    public ?string $included = null;

    /**
     * @var int|null $limit Number of items to return
    */
    public ?int $limit = null;

    /**
     * @var int|null $offset Ignore number of items
    */
    public ?int $offset = null;

    /**
     * @var string|null $q Filters
    */
    public ?string $q = null;

    /**
     * Instantiates a new PurchaseOrderItemsRequestBuilderGetQueryParameters and sets the default values.
     * @param string|null $included Include items (purchaseOrder;stock;nominalCode)
     * @param int|null $limit Number of items to return
     * @param int|null $offset Ignore number of items
     * @param string|null $q Filters
    */
    public function __construct(?string $included = null, ?int $limit = null, ?int $offset = null, ?string $q = null) {
        $this->included = $included;
        $this->limit = $limit;
        $this->offset = $offset;
        $this->q = $q;
    }

}
