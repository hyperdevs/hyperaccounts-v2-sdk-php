<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Currencies;

/**
 * Gets a list of currencies.
*/
class CurrenciesRequestBuilderGetQueryParameters
{
    /**
     * @var int|null $limit Number of items to return
    */
    public ?int $limit = null;

    /**
     * @var int|null $offset Ignore number of items
    */
    public ?int $offset = null;

    /**
     * @var string|null $q Filter collection. Url encode the following string: [{"Property":"Name","Method":"=","Value":"Zloty"}]
    */
    public ?string $q = null;

    /**
     * Instantiates a new CurrenciesRequestBuilderGetQueryParameters and sets the default values.
     * @param int|null $limit Number of items to return
     * @param int|null $offset Ignore number of items
     * @param string|null $q Filter collection. Url encode the following string: [{"Property":"Name","Method":"=","Value":"Zloty"}]
    */
    public function __construct(?int $limit = null, ?int $offset = null, ?string $q = null) {
        $this->limit = $limit;
        $this->offset = $offset;
        $this->q = $q;
    }

}
