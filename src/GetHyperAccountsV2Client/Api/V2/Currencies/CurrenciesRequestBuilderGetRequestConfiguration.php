<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Currencies;

use Microsoft\Kiota\Abstractions\RequestOption;

/**
 * Configuration for the request such as headers, query parameters, and middleware options.
*/
class CurrenciesRequestBuilderGetRequestConfiguration
{
    /**
     * @var array<string, array<string>|string>|null $headers Request headers
    */
    public ?array $headers = null;

    /**
     * @var array<RequestOption>|null $options Request options
    */
    public ?array $options = null;

    /**
     * @var CurrenciesRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public ?CurrenciesRequestBuilderGetQueryParameters $queryParameters = null;

    /**
     * Instantiates a new CurrenciesRequestBuilderGetQueryParameters.
     * @param int|null $limit Number of items to return
     * @param int|null $offset Ignore number of items
     * @param string|null $q Filter collection. Url encode the following string: [{"Property":"Name","Method":"=","Value":"Zloty"}]
     * @return CurrenciesRequestBuilderGetQueryParameters
    */
    public static function addQueryParameters(?int $limit = null, ?int $offset = null, ?string $q = null): CurrenciesRequestBuilderGetQueryParameters {
        return new CurrenciesRequestBuilderGetQueryParameters($limit, $offset, $q);
    }

    /**
     * Instantiates a new CurrenciesRequestBuilderGetRequestConfiguration and sets the default values.
     * @param array<string, array<string>|string>|null $headers Request headers
     * @param array<RequestOption>|null $options Request options
     * @param CurrenciesRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public function __construct(?array $headers = null, ?array $options = null, ?CurrenciesRequestBuilderGetQueryParameters $queryParameters = null) {
        $this->headers = $headers;
        $this->options = $options;
        $this->queryParameters = $queryParameters;
    }

}
