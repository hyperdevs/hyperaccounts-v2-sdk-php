<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\ChartOfAccounts\Item;

use Microsoft\Kiota\Abstractions\RequestOption;

/**
 * Configuration for the request such as headers, query parameters, and middleware options.
*/
class WithChartItemRequestBuilderGetRequestConfiguration
{
    /**
     * @var array<string, array<string>|string>|null $headers Request headers
    */
    public ?array $headers = null;

    /**
     * @var array<RequestOption>|null $options Request options
    */
    public ?array $options = null;

    /**
     * @var WithChartItemRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public ?WithChartItemRequestBuilderGetQueryParameters $queryParameters = null;

    /**
     * Instantiates a new WithChartItemRequestBuilderGetQueryParameters.
     * @param string|null $included Include related resources (categoryTitles;categories)
     * @return WithChartItemRequestBuilderGetQueryParameters
    */
    public static function addQueryParameters(?string $included = null): WithChartItemRequestBuilderGetQueryParameters {
        return new WithChartItemRequestBuilderGetQueryParameters($included);
    }

    /**
     * Instantiates a new WithChartItemRequestBuilderGetRequestConfiguration and sets the default values.
     * @param array<string, array<string>|string>|null $headers Request headers
     * @param array<RequestOption>|null $options Request options
     * @param WithChartItemRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public function __construct(?array $headers = null, ?array $options = null, ?WithChartItemRequestBuilderGetQueryParameters $queryParameters = null) {
        $this->headers = $headers;
        $this->options = $options;
        $this->queryParameters = $queryParameters;
    }

}
