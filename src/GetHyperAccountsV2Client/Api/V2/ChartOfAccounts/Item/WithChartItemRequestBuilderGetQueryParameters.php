<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\ChartOfAccounts\Item;

/**
 * Gets a specific chart of accounts.
*/
class WithChartItemRequestBuilderGetQueryParameters
{
    /**
     * @var string|null $included Include related resources (categoryTitles;categories)
    */
    public ?string $included = null;

    /**
     * Instantiates a new WithChartItemRequestBuilderGetQueryParameters and sets the default values.
     * @param string|null $included Include related resources (categoryTitles;categories)
    */
    public function __construct(?string $included = null) {
        $this->included = $included;
    }

}
