<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\ChartOfAccounts;

/**
 * Gets a list of chart of accounts.
*/
class ChartOfAccountsRequestBuilderGetQueryParameters
{
    /**
     * @var string|null $included Include related resources (categoryTitles;categories)
    */
    public ?string $included = null;

    /**
     * @var int|null $limit Number of items to return
    */
    public ?int $limit = null;

    /**
     * @var int|null $offset Ignore number of items
    */
    public ?int $offset = null;

    /**
     * @var string|null $q Filter collection. Url encode the following string: [{"Property":"Name","Method":"!=","Value":""}]
    */
    public ?string $q = null;

    /**
     * Instantiates a new ChartOfAccountsRequestBuilderGetQueryParameters and sets the default values.
     * @param string|null $included Include related resources (categoryTitles;categories)
     * @param int|null $limit Number of items to return
     * @param int|null $offset Ignore number of items
     * @param string|null $q Filter collection. Url encode the following string: [{"Property":"Name","Method":"!=","Value":""}]
    */
    public function __construct(?string $included = null, ?int $limit = null, ?int $offset = null, ?string $q = null) {
        $this->included = $included;
        $this->limit = $limit;
        $this->offset = $offset;
        $this->q = $q;
    }

}
