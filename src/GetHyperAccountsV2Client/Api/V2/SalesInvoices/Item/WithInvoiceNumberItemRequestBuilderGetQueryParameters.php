<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\SalesInvoices\Item;

/**
 * Gets a specific sales invoice.
*/
class WithInvoiceNumberItemRequestBuilderGetQueryParameters
{
    /**
     * @var Int|null $childCollectionLimit
    */
    public ?Int $childCollectionLimit = null;

    /**
     * @var Int|null $childCollectionOffset
    */
    public ?Int $childCollectionOffset = null;

    /**
     * @var string|null $included Include related resources (customer;items;project)
    */
    public ?string $included = null;

    /**
     * Instantiates a new WithInvoiceNumberItemRequestBuilderGetQueryParameters and sets the default values.
     * @param Int|null $childCollectionLimit
     * @param Int|null $childCollectionOffset
     * @param string|null $included Include related resources (customer;items;project)
    */
    public function __construct(?Int $childCollectionLimit = null, ?Int $childCollectionOffset = null, ?string $included = null) {
        $this->childCollectionLimit = $childCollectionLimit;
        $this->childCollectionOffset = $childCollectionOffset;
        $this->included = $included;
    }

}
