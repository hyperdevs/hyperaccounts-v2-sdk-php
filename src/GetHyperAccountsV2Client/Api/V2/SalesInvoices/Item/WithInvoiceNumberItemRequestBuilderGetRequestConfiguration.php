<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\SalesInvoices\Item;

use Microsoft\Kiota\Abstractions\RequestOption;

/**
 * Configuration for the request such as headers, query parameters, and middleware options.
*/
class WithInvoiceNumberItemRequestBuilderGetRequestConfiguration
{
    /**
     * @var array<string, array<string>|string>|null $headers Request headers
    */
    public ?array $headers = null;

    /**
     * @var array<RequestOption>|null $options Request options
    */
    public ?array $options = null;

    /**
     * @var WithInvoiceNumberItemRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public ?WithInvoiceNumberItemRequestBuilderGetQueryParameters $queryParameters = null;

    /**
     * Instantiates a new WithInvoiceNumberItemRequestBuilderGetQueryParameters.
     * @param Int|null $childCollectionLimit
     * @param Int|null $childCollectionOffset
     * @param string|null $included Include related resources (customer;items;project)
     * @return WithInvoiceNumberItemRequestBuilderGetQueryParameters
    */
    public static function addQueryParameters(?Int $childCollectionLimit = null, ?Int $childCollectionOffset = null, ?string $included = null): WithInvoiceNumberItemRequestBuilderGetQueryParameters {
        return new WithInvoiceNumberItemRequestBuilderGetQueryParameters($childCollectionLimit, $childCollectionOffset, $included);
    }

    /**
     * Instantiates a new WithInvoiceNumberItemRequestBuilderGetRequestConfiguration and sets the default values.
     * @param array<string, array<string>|string>|null $headers Request headers
     * @param array<RequestOption>|null $options Request options
     * @param WithInvoiceNumberItemRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public function __construct(?array $headers = null, ?array $options = null, ?WithInvoiceNumberItemRequestBuilderGetQueryParameters $queryParameters = null) {
        $this->headers = $headers;
        $this->options = $options;
        $this->queryParameters = $queryParameters;
    }

}
