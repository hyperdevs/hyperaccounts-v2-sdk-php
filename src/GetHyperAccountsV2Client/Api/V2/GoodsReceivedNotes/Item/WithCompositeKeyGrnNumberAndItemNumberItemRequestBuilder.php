<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\GoodsReceivedNotes\Item;

use Exception;
use Http\Promise\Promise;
use Http\Promise\RejectedPromise;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\GoodsReceivedNotes\Item\PurchaseOrder\PurchaseOrderRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\GoodsReceivedNotes\Item\Stock\StockRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\GoodsReceivedNotes\Item\Supplier\SupplierRequestBuilder;
use HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models\GoodsReceivedNoteGetDto;
use Microsoft\Kiota\Abstractions\HttpMethod;
use Microsoft\Kiota\Abstractions\RequestAdapter;
use Microsoft\Kiota\Abstractions\RequestInformation;
use Microsoft\Kiota\Abstractions\ResponseHandler;
use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParsableFactory;

/**
 * Builds and executes requests for operations under /api/v2/GoodsReceivedNotes/{compositeKeyGrnNumberAndItemNumber}
*/
class WithCompositeKeyGrnNumberAndItemNumberItemRequestBuilder
{
    /**
     * @var array<string, mixed> $pathParameters Path parameters for the request
    */
    private array $pathParameters;

    /**
     * The PurchaseOrder property
    */
    public function purchaseOrder(): PurchaseOrderRequestBuilder {
        return new PurchaseOrderRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * @var RequestAdapter $requestAdapter The request adapter to use to execute the requests.
    */
    private RequestAdapter $requestAdapter;

    /**
     * The Stock property
    */
    public function stock(): StockRequestBuilder {
        return new StockRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * The Supplier property
    */
    public function supplier(): SupplierRequestBuilder {
        return new SupplierRequestBuilder($this->pathParameters, $this->requestAdapter);
    }

    /**
     * @var string $urlTemplate Url template to use to build the URL for the current request builder
    */
    private string $urlTemplate;

    /**
     * Instantiates a new WithCompositeKeyGrnNumberAndItemNumberItemRequestBuilder and sets the default values.
     * @param array<string, mixed>|string $pathParametersOrRawUrl Path parameters for the request or a String representing the raw URL.
     * @param RequestAdapter $requestAdapter The request adapter to use to execute the requests.
    */
    public function __construct($pathParametersOrRawUrl, RequestAdapter $requestAdapter) {
        $this->urlTemplate = '{+baseurl}/api/v2/GoodsReceivedNotes/{compositeKeyGrnNumberAndItemNumber}{?compositeKeyGrnNumberAndItemNumber*,included*,childCollectionLimit*,childCollectionOffset*}';
        $this->requestAdapter = $requestAdapter;
        if (is_array($pathParametersOrRawUrl)) {
            $this->pathParameters = $pathParametersOrRawUrl;
        } else {
            $this->pathParameters = ['request-raw-url' => $pathParametersOrRawUrl];
        }
    }

    /**
     * Gets a specific goods received note.
     * @param WithCompositeKeyGrnNumberAndItemNumberItemRequestBuilderGetRequestConfiguration|null $requestConfiguration Configuration for the request such as headers, query parameters, and middleware options.
     * @return Promise
    */
    public function get(?WithCompositeKeyGrnNumberAndItemNumberItemRequestBuilderGetRequestConfiguration $requestConfiguration = null): Promise {
        $requestInfo = $this->toGetRequestInformation($requestConfiguration);
        try {
            return $this->requestAdapter->sendAsync($requestInfo, [GoodsReceivedNoteGetDto::class, 'createFromDiscriminatorValue'], null);
        } catch(Exception $ex) {
            return new RejectedPromise($ex);
        }
    }

    /**
     * Gets a specific goods received note.
     * @param WithCompositeKeyGrnNumberAndItemNumberItemRequestBuilderGetRequestConfiguration|null $requestConfiguration Configuration for the request such as headers, query parameters, and middleware options.
     * @return RequestInformation
    */
    public function toGetRequestInformation(?WithCompositeKeyGrnNumberAndItemNumberItemRequestBuilderGetRequestConfiguration $requestConfiguration = null): RequestInformation {
        $requestInfo = new RequestInformation();
        $requestInfo->urlTemplate = $this->urlTemplate;
        $requestInfo->pathParameters = $this->pathParameters;
        $requestInfo->httpMethod = HttpMethod::GET;
        $requestInfo->addHeader('Accept', "application/json");
        if ($requestConfiguration !== null) {
            if ($requestConfiguration->headers !== null) {
                $requestInfo->addHeaders($requestConfiguration->headers);
            }
            if ($requestConfiguration->queryParameters !== null) {
                $requestInfo->setQueryParameters($requestConfiguration->queryParameters);
            }
            if ($requestConfiguration->options !== null) {
                $requestInfo->addRequestOptions(...$requestConfiguration->options);
            }
        }
        return $requestInfo;
    }

}
