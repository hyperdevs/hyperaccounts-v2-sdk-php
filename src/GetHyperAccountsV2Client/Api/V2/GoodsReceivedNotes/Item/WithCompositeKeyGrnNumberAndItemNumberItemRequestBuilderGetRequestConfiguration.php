<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\GoodsReceivedNotes\Item;

use Microsoft\Kiota\Abstractions\RequestOption;

/**
 * Configuration for the request such as headers, query parameters, and middleware options.
*/
class WithCompositeKeyGrnNumberAndItemNumberItemRequestBuilderGetRequestConfiguration
{
    /**
     * @var array<string, array<string>|string>|null $headers Request headers
    */
    public ?array $headers = null;

    /**
     * @var array<RequestOption>|null $options Request options
    */
    public ?array $options = null;

    /**
     * @var WithCompositeKeyGrnNumberAndItemNumberItemRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public ?WithCompositeKeyGrnNumberAndItemNumberItemRequestBuilderGetQueryParameters $queryParameters = null;

    /**
     * Instantiates a new WithCompositeKeyGrnNumberAndItemNumberItemRequestBuilderGetQueryParameters.
     * @param Int|null $childCollectionLimit
     * @param Int|null $childCollectionOffset
     * @param string|null $compositeKeyGrnNumberAndItemNumber Reference linked to goods received note
     * @param string|null $included Include related resources (purchaseOrder;supplier;stock)
     * @return WithCompositeKeyGrnNumberAndItemNumberItemRequestBuilderGetQueryParameters
    */
    public static function addQueryParameters(?Int $childCollectionLimit = null, ?Int $childCollectionOffset = null, ?string $compositeKeyGrnNumberAndItemNumber = null, ?string $included = null): WithCompositeKeyGrnNumberAndItemNumberItemRequestBuilderGetQueryParameters {
        return new WithCompositeKeyGrnNumberAndItemNumberItemRequestBuilderGetQueryParameters($childCollectionLimit, $childCollectionOffset, $compositeKeyGrnNumberAndItemNumber, $included);
    }

    /**
     * Instantiates a new WithCompositeKeyGrnNumberAndItemNumberItemRequestBuilderGetRequestConfiguration and sets the default values.
     * @param array<string, array<string>|string>|null $headers Request headers
     * @param array<RequestOption>|null $options Request options
     * @param WithCompositeKeyGrnNumberAndItemNumberItemRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public function __construct(?array $headers = null, ?array $options = null, ?WithCompositeKeyGrnNumberAndItemNumberItemRequestBuilderGetQueryParameters $queryParameters = null) {
        $this->headers = $headers;
        $this->options = $options;
        $this->queryParameters = $queryParameters;
    }

}
