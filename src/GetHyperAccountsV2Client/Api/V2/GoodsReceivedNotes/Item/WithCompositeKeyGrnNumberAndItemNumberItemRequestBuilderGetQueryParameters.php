<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\GoodsReceivedNotes\Item;

/**
 * Gets a specific goods received note.
*/
class WithCompositeKeyGrnNumberAndItemNumberItemRequestBuilderGetQueryParameters
{
    /**
     * @var Int|null $childCollectionLimit
    */
    public ?Int $childCollectionLimit = null;

    /**
     * @var Int|null $childCollectionOffset
    */
    public ?Int $childCollectionOffset = null;

    /**
     * @var string|null $compositeKeyGrnNumberAndItemNumber Reference linked to goods received note
    */
    public ?string $compositeKeyGrnNumberAndItemNumber = null;

    /**
     * @var string|null $included Include related resources (purchaseOrder;supplier;stock)
    */
    public ?string $included = null;

    /**
     * Instantiates a new WithCompositeKeyGrnNumberAndItemNumberItemRequestBuilderGetQueryParameters and sets the default values.
     * @param Int|null $childCollectionLimit
     * @param Int|null $childCollectionOffset
     * @param string|null $compositeKeyGrnNumberAndItemNumber Reference linked to goods received note
     * @param string|null $included Include related resources (purchaseOrder;supplier;stock)
    */
    public function __construct(?Int $childCollectionLimit = null, ?Int $childCollectionOffset = null, ?string $compositeKeyGrnNumberAndItemNumber = null, ?string $included = null) {
        $this->childCollectionLimit = $childCollectionLimit;
        $this->childCollectionOffset = $childCollectionOffset;
        $this->compositeKeyGrnNumberAndItemNumber = $compositeKeyGrnNumberAndItemNumber;
        $this->included = $included;
    }

}
