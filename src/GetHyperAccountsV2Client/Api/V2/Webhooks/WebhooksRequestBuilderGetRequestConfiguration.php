<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Api\V2\Webhooks;

use Microsoft\Kiota\Abstractions\RequestOption;

/**
 * Configuration for the request such as headers, query parameters, and middleware options.
*/
class WebhooksRequestBuilderGetRequestConfiguration
{
    /**
     * @var array<string, array<string>|string>|null $headers Request headers
    */
    public ?array $headers = null;

    /**
     * @var array<RequestOption>|null $options Request options
    */
    public ?array $options = null;

    /**
     * @var WebhooksRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public ?WebhooksRequestBuilderGetQueryParameters $queryParameters = null;

    /**
     * Instantiates a new WebhooksRequestBuilderGetQueryParameters.
     * @param int|null $limit Number of items to return
     * @param int|null $offset Ignore number of items
     * @return WebhooksRequestBuilderGetQueryParameters
    */
    public static function addQueryParameters(?int $limit = null, ?int $offset = null): WebhooksRequestBuilderGetQueryParameters {
        return new WebhooksRequestBuilderGetQueryParameters($limit, $offset);
    }

    /**
     * Instantiates a new WebhooksRequestBuilderGetRequestConfiguration and sets the default values.
     * @param array<string, array<string>|string>|null $headers Request headers
     * @param array<RequestOption>|null $options Request options
     * @param WebhooksRequestBuilderGetQueryParameters|null $queryParameters Request query parameters
    */
    public function __construct(?array $headers = null, ?array $options = null, ?WebhooksRequestBuilderGetQueryParameters $queryParameters = null) {
        $this->headers = $headers;
        $this->options = $options;
        $this->queryParameters = $queryParameters;
    }

}
