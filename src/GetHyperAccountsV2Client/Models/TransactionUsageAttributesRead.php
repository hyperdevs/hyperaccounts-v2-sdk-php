<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use DateTime;
use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class TransactionUsageAttributesRead implements Parsable
{
    /**
     * @var float|null $amount The amount property
    */
    private ?float $amount = null;

    /**
     * @var float|null $cisVat The cisVat property
    */
    private ?float $cisVat = null;

    /**
     * @var DateTime|null $date The date property
    */
    private ?DateTime $date = null;

    /**
     * @var int|null $deletedFlag The deletedFlag property
    */
    private ?int $deletedFlag = null;

    /**
     * @var string|null $details The details property
    */
    private ?string $details = null;

    /**
     * @var float|null $foreignAmount The foreignAmount property
    */
    private ?float $foreignAmount = null;

    /**
     * @var DateTime|null $recordCreateDate The recordCreateDate property
    */
    private ?DateTime $recordCreateDate = null;

    /**
     * @var int|null $recordDeleted The recordDeleted property
    */
    private ?int $recordDeleted = null;

    /**
     * @var DateTime|null $recordModifyDate The recordModifyDate property
    */
    private ?DateTime $recordModifyDate = null;

    /**
     * @var string|null $reference The reference property
    */
    private ?string $reference = null;

    /**
     * @var int|null $splitCrossref The splitCrossref property
    */
    private ?int $splitCrossref = null;

    /**
     * @var TransactionUsageAttributesRead_type|null $type The type property
    */
    private ?TransactionUsageAttributesRead_type $type = null;

    /**
     * @var string|null $userName The userName property
    */
    private ?string $userName = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return TransactionUsageAttributesRead
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): TransactionUsageAttributesRead {
        return new TransactionUsageAttributesRead();
    }

    /**
     * Gets the amount property value. The amount property
     * @return float|null
    */
    public function getAmount(): ?float {
        return $this->amount;
    }

    /**
     * Gets the cisVat property value. The cisVat property
     * @return float|null
    */
    public function getCisVat(): ?float {
        return $this->cisVat;
    }

    /**
     * Gets the date property value. The date property
     * @return DateTime|null
    */
    public function getDate(): ?DateTime {
        return $this->date;
    }

    /**
     * Gets the deletedFlag property value. The deletedFlag property
     * @return int|null
    */
    public function getDeletedFlag(): ?int {
        return $this->deletedFlag;
    }

    /**
     * Gets the details property value. The details property
     * @return string|null
    */
    public function getDetails(): ?string {
        return $this->details;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'amount' => fn(ParseNode $n) => $o->setAmount($n->getFloatValue()),
            'cisVat' => fn(ParseNode $n) => $o->setCisVat($n->getFloatValue()),
            'date' => fn(ParseNode $n) => $o->setDate($n->getDateTimeValue()),
            'deletedFlag' => fn(ParseNode $n) => $o->setDeletedFlag($n->getIntegerValue()),
            'details' => fn(ParseNode $n) => $o->setDetails($n->getStringValue()),
            'foreignAmount' => fn(ParseNode $n) => $o->setForeignAmount($n->getFloatValue()),
            'recordCreateDate' => fn(ParseNode $n) => $o->setRecordCreateDate($n->getDateTimeValue()),
            'recordDeleted' => fn(ParseNode $n) => $o->setRecordDeleted($n->getIntegerValue()),
            'recordModifyDate' => fn(ParseNode $n) => $o->setRecordModifyDate($n->getDateTimeValue()),
            'reference' => fn(ParseNode $n) => $o->setReference($n->getStringValue()),
            'splitCrossref' => fn(ParseNode $n) => $o->setSplitCrossref($n->getIntegerValue()),
            'type' => fn(ParseNode $n) => $o->setType($n->getEnumValue(TransactionUsageAttributesRead_type::class)),
            'userName' => fn(ParseNode $n) => $o->setUserName($n->getStringValue()),
        ];
    }

    /**
     * Gets the foreignAmount property value. The foreignAmount property
     * @return float|null
    */
    public function getForeignAmount(): ?float {
        return $this->foreignAmount;
    }

    /**
     * Gets the recordCreateDate property value. The recordCreateDate property
     * @return DateTime|null
    */
    public function getRecordCreateDate(): ?DateTime {
        return $this->recordCreateDate;
    }

    /**
     * Gets the recordDeleted property value. The recordDeleted property
     * @return int|null
    */
    public function getRecordDeleted(): ?int {
        return $this->recordDeleted;
    }

    /**
     * Gets the recordModifyDate property value. The recordModifyDate property
     * @return DateTime|null
    */
    public function getRecordModifyDate(): ?DateTime {
        return $this->recordModifyDate;
    }

    /**
     * Gets the reference property value. The reference property
     * @return string|null
    */
    public function getReference(): ?string {
        return $this->reference;
    }

    /**
     * Gets the splitCrossref property value. The splitCrossref property
     * @return int|null
    */
    public function getSplitCrossref(): ?int {
        return $this->splitCrossref;
    }

    /**
     * Gets the type property value. The type property
     * @return TransactionUsageAttributesRead_type|null
    */
    public function getType(): ?TransactionUsageAttributesRead_type {
        return $this->type;
    }

    /**
     * Gets the userName property value. The userName property
     * @return string|null
    */
    public function getUserName(): ?string {
        return $this->userName;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeFloatValue('amount', $this->getAmount());
        $writer->writeFloatValue('cisVat', $this->getCisVat());
        $writer->writeDateTimeValue('date', $this->getDate());
        $writer->writeIntegerValue('deletedFlag', $this->getDeletedFlag());
        $writer->writeStringValue('details', $this->getDetails());
        $writer->writeFloatValue('foreignAmount', $this->getForeignAmount());
        $writer->writeDateTimeValue('recordCreateDate', $this->getRecordCreateDate());
        $writer->writeIntegerValue('recordDeleted', $this->getRecordDeleted());
        $writer->writeDateTimeValue('recordModifyDate', $this->getRecordModifyDate());
        $writer->writeStringValue('reference', $this->getReference());
        $writer->writeIntegerValue('splitCrossref', $this->getSplitCrossref());
        $writer->writeEnumValue('type', $this->getType());
        $writer->writeStringValue('userName', $this->getUserName());
    }

    /**
     * Sets the amount property value. The amount property
     * @param float|null $value Value to set for the amount property.
    */
    public function setAmount(?float $value): void {
        $this->amount = $value;
    }

    /**
     * Sets the cisVat property value. The cisVat property
     * @param float|null $value Value to set for the cisVat property.
    */
    public function setCisVat(?float $value): void {
        $this->cisVat = $value;
    }

    /**
     * Sets the date property value. The date property
     * @param DateTime|null $value Value to set for the date property.
    */
    public function setDate(?DateTime $value): void {
        $this->date = $value;
    }

    /**
     * Sets the deletedFlag property value. The deletedFlag property
     * @param int|null $value Value to set for the deletedFlag property.
    */
    public function setDeletedFlag(?int $value): void {
        $this->deletedFlag = $value;
    }

    /**
     * Sets the details property value. The details property
     * @param string|null $value Value to set for the details property.
    */
    public function setDetails(?string $value): void {
        $this->details = $value;
    }

    /**
     * Sets the foreignAmount property value. The foreignAmount property
     * @param float|null $value Value to set for the foreignAmount property.
    */
    public function setForeignAmount(?float $value): void {
        $this->foreignAmount = $value;
    }

    /**
     * Sets the recordCreateDate property value. The recordCreateDate property
     * @param DateTime|null $value Value to set for the recordCreateDate property.
    */
    public function setRecordCreateDate(?DateTime $value): void {
        $this->recordCreateDate = $value;
    }

    /**
     * Sets the recordDeleted property value. The recordDeleted property
     * @param int|null $value Value to set for the recordDeleted property.
    */
    public function setRecordDeleted(?int $value): void {
        $this->recordDeleted = $value;
    }

    /**
     * Sets the recordModifyDate property value. The recordModifyDate property
     * @param DateTime|null $value Value to set for the recordModifyDate property.
    */
    public function setRecordModifyDate(?DateTime $value): void {
        $this->recordModifyDate = $value;
    }

    /**
     * Sets the reference property value. The reference property
     * @param string|null $value Value to set for the reference property.
    */
    public function setReference(?string $value): void {
        $this->reference = $value;
    }

    /**
     * Sets the splitCrossref property value. The splitCrossref property
     * @param int|null $value Value to set for the splitCrossref property.
    */
    public function setSplitCrossref(?int $value): void {
        $this->splitCrossref = $value;
    }

    /**
     * Sets the type property value. The type property
     * @param TransactionUsageAttributesRead_type|null $value Value to set for the type property.
    */
    public function setType(?TransactionUsageAttributesRead_type $value): void {
        $this->type = $value;
    }

    /**
     * Sets the userName property value. The userName property
     * @param string|null $value Value to set for the userName property.
    */
    public function setUserName(?string $value): void {
        $this->userName = $value;
    }

}
