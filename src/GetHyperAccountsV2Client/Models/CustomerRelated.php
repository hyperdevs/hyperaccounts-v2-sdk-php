<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class CustomerRelated implements Parsable
{
    /**
     * @var string|null $accountRef The accountRef property
    */
    private ?string $accountRef = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return CustomerRelated
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): CustomerRelated {
        return new CustomerRelated();
    }

    /**
     * Gets the accountRef property value. The accountRef property
     * @return string|null
    */
    public function getAccountRef(): ?string {
        return $this->accountRef;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'accountRef' => fn(ParseNode $n) => $o->setAccountRef($n->getStringValue()),
        ];
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeStringValue('accountRef', $this->getAccountRef());
    }

    /**
     * Sets the accountRef property value. The accountRef property
     * @param string|null $value Value to set for the accountRef property.
    */
    public function setAccountRef(?string $value): void {
        $this->accountRef = $value;
    }

}
