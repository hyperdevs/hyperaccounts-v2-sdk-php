<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class SalesOrderRelationshipsWrite implements Parsable
{
    /**
     * @var CustomerRelatedRequiredRelationshipWrite|null $customer The customer property
    */
    private ?CustomerRelatedRequiredRelationshipWrite $customer = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return SalesOrderRelationshipsWrite
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): SalesOrderRelationshipsWrite {
        return new SalesOrderRelationshipsWrite();
    }

    /**
     * Gets the customer property value. The customer property
     * @return CustomerRelatedRequiredRelationshipWrite|null
    */
    public function getCustomer(): ?CustomerRelatedRequiredRelationshipWrite {
        return $this->customer;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'customer' => fn(ParseNode $n) => $o->setCustomer($n->getObjectValue([CustomerRelatedRequiredRelationshipWrite::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('customer', $this->getCustomer());
    }

    /**
     * Sets the customer property value. The customer property
     * @param CustomerRelatedRequiredRelationshipWrite|null $value Value to set for the customer property.
    */
    public function setCustomer(?CustomerRelatedRequiredRelationshipWrite $value): void {
        $this->customer = $value;
    }

}
