<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use DateTime;
use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class PriceListAttributesRead implements Parsable
{
    /**
     * @var int|null $currency The currency property
    */
    private ?int $currency = null;

    /**
     * @var int|null $hasStaticPrices The hasStaticPrices property
    */
    private ?int $hasStaticPrices = null;

    /**
     * @var string|null $name The name property
    */
    private ?string $name = null;

    /**
     * @var DateTime|null $recordCreateDate The recordCreateDate property
    */
    private ?DateTime $recordCreateDate = null;

    /**
     * @var int|null $recordDeleted The recordDeleted property
    */
    private ?int $recordDeleted = null;

    /**
     * @var DateTime|null $recordModifyDate The recordModifyDate property
    */
    private ?DateTime $recordModifyDate = null;

    /**
     * @var PriceListAttributesRead_type|null $type The type property
    */
    private ?PriceListAttributesRead_type $type = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return PriceListAttributesRead
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): PriceListAttributesRead {
        return new PriceListAttributesRead();
    }

    /**
     * Gets the currency property value. The currency property
     * @return int|null
    */
    public function getCurrency(): ?int {
        return $this->currency;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'currency' => fn(ParseNode $n) => $o->setCurrency($n->getIntegerValue()),
            'hasStaticPrices' => fn(ParseNode $n) => $o->setHasStaticPrices($n->getIntegerValue()),
            'name' => fn(ParseNode $n) => $o->setName($n->getStringValue()),
            'recordCreateDate' => fn(ParseNode $n) => $o->setRecordCreateDate($n->getDateTimeValue()),
            'recordDeleted' => fn(ParseNode $n) => $o->setRecordDeleted($n->getIntegerValue()),
            'recordModifyDate' => fn(ParseNode $n) => $o->setRecordModifyDate($n->getDateTimeValue()),
            'type' => fn(ParseNode $n) => $o->setType($n->getEnumValue(PriceListAttributesRead_type::class)),
        ];
    }

    /**
     * Gets the hasStaticPrices property value. The hasStaticPrices property
     * @return int|null
    */
    public function getHasStaticPrices(): ?int {
        return $this->hasStaticPrices;
    }

    /**
     * Gets the name property value. The name property
     * @return string|null
    */
    public function getName(): ?string {
        return $this->name;
    }

    /**
     * Gets the recordCreateDate property value. The recordCreateDate property
     * @return DateTime|null
    */
    public function getRecordCreateDate(): ?DateTime {
        return $this->recordCreateDate;
    }

    /**
     * Gets the recordDeleted property value. The recordDeleted property
     * @return int|null
    */
    public function getRecordDeleted(): ?int {
        return $this->recordDeleted;
    }

    /**
     * Gets the recordModifyDate property value. The recordModifyDate property
     * @return DateTime|null
    */
    public function getRecordModifyDate(): ?DateTime {
        return $this->recordModifyDate;
    }

    /**
     * Gets the type property value. The type property
     * @return PriceListAttributesRead_type|null
    */
    public function getType(): ?PriceListAttributesRead_type {
        return $this->type;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeIntegerValue('currency', $this->getCurrency());
        $writer->writeIntegerValue('hasStaticPrices', $this->getHasStaticPrices());
        $writer->writeStringValue('name', $this->getName());
        $writer->writeDateTimeValue('recordCreateDate', $this->getRecordCreateDate());
        $writer->writeIntegerValue('recordDeleted', $this->getRecordDeleted());
        $writer->writeDateTimeValue('recordModifyDate', $this->getRecordModifyDate());
        $writer->writeEnumValue('type', $this->getType());
    }

    /**
     * Sets the currency property value. The currency property
     * @param int|null $value Value to set for the currency property.
    */
    public function setCurrency(?int $value): void {
        $this->currency = $value;
    }

    /**
     * Sets the hasStaticPrices property value. The hasStaticPrices property
     * @param int|null $value Value to set for the hasStaticPrices property.
    */
    public function setHasStaticPrices(?int $value): void {
        $this->hasStaticPrices = $value;
    }

    /**
     * Sets the name property value. The name property
     * @param string|null $value Value to set for the name property.
    */
    public function setName(?string $value): void {
        $this->name = $value;
    }

    /**
     * Sets the recordCreateDate property value. The recordCreateDate property
     * @param DateTime|null $value Value to set for the recordCreateDate property.
    */
    public function setRecordCreateDate(?DateTime $value): void {
        $this->recordCreateDate = $value;
    }

    /**
     * Sets the recordDeleted property value. The recordDeleted property
     * @param int|null $value Value to set for the recordDeleted property.
    */
    public function setRecordDeleted(?int $value): void {
        $this->recordDeleted = $value;
    }

    /**
     * Sets the recordModifyDate property value. The recordModifyDate property
     * @param DateTime|null $value Value to set for the recordModifyDate property.
    */
    public function setRecordModifyDate(?DateTime $value): void {
        $this->recordModifyDate = $value;
    }

    /**
     * Sets the type property value. The type property
     * @param PriceListAttributesRead_type|null $value Value to set for the type property.
    */
    public function setType(?PriceListAttributesRead_type $value): void {
        $this->type = $value;
    }

}
