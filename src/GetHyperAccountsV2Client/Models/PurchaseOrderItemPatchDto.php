<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class PurchaseOrderItemPatchDto implements Parsable
{
    /**
     * @var PurchaseOrderItemAttributesEdit|null $attributes The attributes property
    */
    private ?PurchaseOrderItemAttributesEdit $attributes = null;

    /**
     * @var PurchaseOrderItemRelationshipsEdit|null $relationships The relationships property
    */
    private ?PurchaseOrderItemRelationshipsEdit $relationships = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return PurchaseOrderItemPatchDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): PurchaseOrderItemPatchDto {
        return new PurchaseOrderItemPatchDto();
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return PurchaseOrderItemAttributesEdit|null
    */
    public function getAttributes(): ?PurchaseOrderItemAttributesEdit {
        return $this->attributes;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([PurchaseOrderItemAttributesEdit::class, 'createFromDiscriminatorValue'])),
            'relationships' => fn(ParseNode $n) => $o->setRelationships($n->getObjectValue([PurchaseOrderItemRelationshipsEdit::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the relationships property value. The relationships property
     * @return PurchaseOrderItemRelationshipsEdit|null
    */
    public function getRelationships(): ?PurchaseOrderItemRelationshipsEdit {
        return $this->relationships;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('attributes', $this->getAttributes());
        $writer->writeObjectValue('relationships', $this->getRelationships());
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param PurchaseOrderItemAttributesEdit|null $value Value to set for the attributes property.
    */
    public function setAttributes(?PurchaseOrderItemAttributesEdit $value): void {
        $this->attributes = $value;
    }

    /**
     * Sets the relationships property value. The relationships property
     * @param PurchaseOrderItemRelationshipsEdit|null $value Value to set for the relationships property.
    */
    public function setRelationships(?PurchaseOrderItemRelationshipsEdit $value): void {
        $this->relationships = $value;
    }

}
