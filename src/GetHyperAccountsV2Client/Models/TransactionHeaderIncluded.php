<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class TransactionHeaderIncluded implements Parsable
{
    /**
     * @var TransactionSplitCollection|null $splits The splits property
    */
    private ?TransactionSplitCollection $splits = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return TransactionHeaderIncluded
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): TransactionHeaderIncluded {
        return new TransactionHeaderIncluded();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'splits' => fn(ParseNode $n) => $o->setSplits($n->getObjectValue([TransactionSplitCollection::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the splits property value. The splits property
     * @return TransactionSplitCollection|null
    */
    public function getSplits(): ?TransactionSplitCollection {
        return $this->splits;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('splits', $this->getSplits());
    }

    /**
     * Sets the splits property value. The splits property
     * @param TransactionSplitCollection|null $value Value to set for the splits property.
    */
    public function setSplits(?TransactionSplitCollection $value): void {
        $this->splits = $value;
    }

}
