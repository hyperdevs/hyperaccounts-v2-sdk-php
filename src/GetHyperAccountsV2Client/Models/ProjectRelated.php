<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class ProjectRelated implements Parsable
{
    /**
     * @var int|null $projectId The projectId property
    */
    private ?int $projectId = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return ProjectRelated
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): ProjectRelated {
        return new ProjectRelated();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'projectId' => fn(ParseNode $n) => $o->setProjectId($n->getIntegerValue()),
        ];
    }

    /**
     * Gets the projectId property value. The projectId property
     * @return int|null
    */
    public function getProjectId(): ?int {
        return $this->projectId;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeIntegerValue('projectId', $this->getProjectId());
    }

    /**
     * Sets the projectId property value. The projectId property
     * @param int|null $value Value to set for the projectId property.
    */
    public function setProjectId(?int $value): void {
        $this->projectId = $value;
    }

}
