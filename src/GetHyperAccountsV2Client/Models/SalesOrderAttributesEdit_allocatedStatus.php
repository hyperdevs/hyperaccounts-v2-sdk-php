<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Enum;

class SalesOrderAttributesEdit_allocatedStatus extends Enum {
    public const ZERO_ALLOCATION = 'ZeroAllocation';
    public const PART_ALLOCATION = 'PartAllocation';
    public const FULL_ALLOCATION = 'FullAllocation';
    public const CANCELLED = 'Cancelled';
    public const PART_ZERO_ALLOCATION = 'PartZeroAllocation';
    public const PART_PART_ALLOCATION = 'PartPartAllocation';
    public const PART_FULL_ALLOCATION = 'PartFullAllocation';
    public const PART_CANCELLED = 'PartCancelled';
    public const COMPLETE = 'Complete';
    public const HELD_PART = 'HeldPart';
    public const HELD = 'Held';
}
