<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class PriceGetDto implements Parsable
{
    /**
     * @var PriceAttributesRead|null $attributes The attributes property
    */
    private ?PriceAttributesRead $attributes = null;

    /**
     * @var PriceIncluded|null $included The included property
    */
    private ?PriceIncluded $included = null;

    /**
     * @var ResourceLinks|null $links The links property
    */
    private ?ResourceLinks $links = null;

    /**
     * @var string|null $priceId The priceId property
    */
    private ?string $priceId = null;

    /**
     * @var PriceRelationships|null $relationships The relationships property
    */
    private ?PriceRelationships $relationships = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return PriceGetDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): PriceGetDto {
        return new PriceGetDto();
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return PriceAttributesRead|null
    */
    public function getAttributes(): ?PriceAttributesRead {
        return $this->attributes;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([PriceAttributesRead::class, 'createFromDiscriminatorValue'])),
            'included' => fn(ParseNode $n) => $o->setIncluded($n->getObjectValue([PriceIncluded::class, 'createFromDiscriminatorValue'])),
            'links' => fn(ParseNode $n) => $o->setLinks($n->getObjectValue([ResourceLinks::class, 'createFromDiscriminatorValue'])),
            'priceId' => fn(ParseNode $n) => $o->setPriceId($n->getStringValue()),
            'relationships' => fn(ParseNode $n) => $o->setRelationships($n->getObjectValue([PriceRelationships::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the included property value. The included property
     * @return PriceIncluded|null
    */
    public function getIncluded(): ?PriceIncluded {
        return $this->included;
    }

    /**
     * Gets the links property value. The links property
     * @return ResourceLinks|null
    */
    public function getLinks(): ?ResourceLinks {
        return $this->links;
    }

    /**
     * Gets the priceId property value. The priceId property
     * @return string|null
    */
    public function getPriceId(): ?string {
        return $this->priceId;
    }

    /**
     * Gets the relationships property value. The relationships property
     * @return PriceRelationships|null
    */
    public function getRelationships(): ?PriceRelationships {
        return $this->relationships;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('attributes', $this->getAttributes());
        $writer->writeObjectValue('included', $this->getIncluded());
        $writer->writeObjectValue('links', $this->getLinks());
        $writer->writeStringValue('priceId', $this->getPriceId());
        $writer->writeObjectValue('relationships', $this->getRelationships());
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param PriceAttributesRead|null $value Value to set for the attributes property.
    */
    public function setAttributes(?PriceAttributesRead $value): void {
        $this->attributes = $value;
    }

    /**
     * Sets the included property value. The included property
     * @param PriceIncluded|null $value Value to set for the included property.
    */
    public function setIncluded(?PriceIncluded $value): void {
        $this->included = $value;
    }

    /**
     * Sets the links property value. The links property
     * @param ResourceLinks|null $value Value to set for the links property.
    */
    public function setLinks(?ResourceLinks $value): void {
        $this->links = $value;
    }

    /**
     * Sets the priceId property value. The priceId property
     * @param string|null $value Value to set for the priceId property.
    */
    public function setPriceId(?string $value): void {
        $this->priceId = $value;
    }

    /**
     * Sets the relationships property value. The relationships property
     * @param PriceRelationships|null $value Value to set for the relationships property.
    */
    public function setRelationships(?PriceRelationships $value): void {
        $this->relationships = $value;
    }

}
