<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class SalesOrderItemRelationshipsWrite implements Parsable
{
    /**
     * @var NominalRelatedRequiredRelationshipWrite|null $nominalCode The nominalCode property
    */
    private ?NominalRelatedRequiredRelationshipWrite $nominalCode = null;

    /**
     * @var ProjectRelatedRelationshipWrite|null $project The project property
    */
    private ?ProjectRelatedRelationshipWrite $project = null;

    /**
     * @var StockRelatedRequiredRelationshipWrite|null $stock The stock property
    */
    private ?StockRelatedRequiredRelationshipWrite $stock = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return SalesOrderItemRelationshipsWrite
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): SalesOrderItemRelationshipsWrite {
        return new SalesOrderItemRelationshipsWrite();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'nominalCode' => fn(ParseNode $n) => $o->setNominalCode($n->getObjectValue([NominalRelatedRequiredRelationshipWrite::class, 'createFromDiscriminatorValue'])),
            'project' => fn(ParseNode $n) => $o->setProject($n->getObjectValue([ProjectRelatedRelationshipWrite::class, 'createFromDiscriminatorValue'])),
            'stock' => fn(ParseNode $n) => $o->setStock($n->getObjectValue([StockRelatedRequiredRelationshipWrite::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the nominalCode property value. The nominalCode property
     * @return NominalRelatedRequiredRelationshipWrite|null
    */
    public function getNominalCode(): ?NominalRelatedRequiredRelationshipWrite {
        return $this->nominalCode;
    }

    /**
     * Gets the project property value. The project property
     * @return ProjectRelatedRelationshipWrite|null
    */
    public function getProject(): ?ProjectRelatedRelationshipWrite {
        return $this->project;
    }

    /**
     * Gets the stock property value. The stock property
     * @return StockRelatedRequiredRelationshipWrite|null
    */
    public function getStock(): ?StockRelatedRequiredRelationshipWrite {
        return $this->stock;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('nominalCode', $this->getNominalCode());
        $writer->writeObjectValue('project', $this->getProject());
        $writer->writeObjectValue('stock', $this->getStock());
    }

    /**
     * Sets the nominalCode property value. The nominalCode property
     * @param NominalRelatedRequiredRelationshipWrite|null $value Value to set for the nominalCode property.
    */
    public function setNominalCode(?NominalRelatedRequiredRelationshipWrite $value): void {
        $this->nominalCode = $value;
    }

    /**
     * Sets the project property value. The project property
     * @param ProjectRelatedRelationshipWrite|null $value Value to set for the project property.
    */
    public function setProject(?ProjectRelatedRelationshipWrite $value): void {
        $this->project = $value;
    }

    /**
     * Sets the stock property value. The stock property
     * @param StockRelatedRequiredRelationshipWrite|null $value Value to set for the stock property.
    */
    public function setStock(?StockRelatedRequiredRelationshipWrite $value): void {
        $this->stock = $value;
    }

}
