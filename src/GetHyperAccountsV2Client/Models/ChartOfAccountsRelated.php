<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class ChartOfAccountsRelated implements Parsable
{
    /**
     * @var int|null $chart The chart property
    */
    private ?int $chart = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return ChartOfAccountsRelated
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): ChartOfAccountsRelated {
        return new ChartOfAccountsRelated();
    }

    /**
     * Gets the chart property value. The chart property
     * @return int|null
    */
    public function getChart(): ?int {
        return $this->chart;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'chart' => fn(ParseNode $n) => $o->setChart($n->getIntegerValue()),
        ];
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeIntegerValue('chart', $this->getChart());
    }

    /**
     * Sets the chart property value. The chart property
     * @param int|null $value Value to set for the chart property.
    */
    public function setChart(?int $value): void {
        $this->chart = $value;
    }

}
