<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class PurchaseOrderItemPostDto implements Parsable
{
    /**
     * @var PurchaseOrderItemAttributesWrite|null $attributes The attributes property
    */
    private ?PurchaseOrderItemAttributesWrite $attributes = null;

    /**
     * @var PurchaseOrderItemRelationshipsWrite|null $relationships The relationships property
    */
    private ?PurchaseOrderItemRelationshipsWrite $relationships = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return PurchaseOrderItemPostDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): PurchaseOrderItemPostDto {
        return new PurchaseOrderItemPostDto();
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return PurchaseOrderItemAttributesWrite|null
    */
    public function getAttributes(): ?PurchaseOrderItemAttributesWrite {
        return $this->attributes;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([PurchaseOrderItemAttributesWrite::class, 'createFromDiscriminatorValue'])),
            'relationships' => fn(ParseNode $n) => $o->setRelationships($n->getObjectValue([PurchaseOrderItemRelationshipsWrite::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the relationships property value. The relationships property
     * @return PurchaseOrderItemRelationshipsWrite|null
    */
    public function getRelationships(): ?PurchaseOrderItemRelationshipsWrite {
        return $this->relationships;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('attributes', $this->getAttributes());
        $writer->writeObjectValue('relationships', $this->getRelationships());
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param PurchaseOrderItemAttributesWrite|null $value Value to set for the attributes property.
    */
    public function setAttributes(?PurchaseOrderItemAttributesWrite $value): void {
        $this->attributes = $value;
    }

    /**
     * Sets the relationships property value. The relationships property
     * @param PurchaseOrderItemRelationshipsWrite|null $value Value to set for the relationships property.
    */
    public function setRelationships(?PurchaseOrderItemRelationshipsWrite $value): void {
        $this->relationships = $value;
    }

}
