<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class PurchaseOrderPatchDto implements Parsable
{
    /**
     * @var PurchaseOrderAttributesEdit|null $attributes The attributes property
    */
    private ?PurchaseOrderAttributesEdit $attributes = null;

    /**
     * @var PurchaseOrderRelationshipsEdit|null $relationships The relationships property
    */
    private ?PurchaseOrderRelationshipsEdit $relationships = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return PurchaseOrderPatchDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): PurchaseOrderPatchDto {
        return new PurchaseOrderPatchDto();
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return PurchaseOrderAttributesEdit|null
    */
    public function getAttributes(): ?PurchaseOrderAttributesEdit {
        return $this->attributes;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([PurchaseOrderAttributesEdit::class, 'createFromDiscriminatorValue'])),
            'relationships' => fn(ParseNode $n) => $o->setRelationships($n->getObjectValue([PurchaseOrderRelationshipsEdit::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the relationships property value. The relationships property
     * @return PurchaseOrderRelationshipsEdit|null
    */
    public function getRelationships(): ?PurchaseOrderRelationshipsEdit {
        return $this->relationships;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('attributes', $this->getAttributes());
        $writer->writeObjectValue('relationships', $this->getRelationships());
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param PurchaseOrderAttributesEdit|null $value Value to set for the attributes property.
    */
    public function setAttributes(?PurchaseOrderAttributesEdit $value): void {
        $this->attributes = $value;
    }

    /**
     * Sets the relationships property value. The relationships property
     * @param PurchaseOrderRelationshipsEdit|null $value Value to set for the relationships property.
    */
    public function setRelationships(?PurchaseOrderRelationshipsEdit $value): void {
        $this->relationships = $value;
    }

}
