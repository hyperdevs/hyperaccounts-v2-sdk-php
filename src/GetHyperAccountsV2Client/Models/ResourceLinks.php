<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class ResourceLinks implements Parsable
{
    /**
     * @var string|null $before The before property
    */
    private ?string $before = null;

    /**
     * @var string|null $escapedSelf The self property
    */
    private ?string $escapedSelf = null;

    /**
     * @var string|null $next The next property
    */
    private ?string $next = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return ResourceLinks
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): ResourceLinks {
        return new ResourceLinks();
    }

    /**
     * Gets the before property value. The before property
     * @return string|null
    */
    public function getBefore(): ?string {
        return $this->before;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'before' => fn(ParseNode $n) => $o->setBefore($n->getStringValue()),
            'self' => fn(ParseNode $n) => $o->setSelf($n->getStringValue()),
            'next' => fn(ParseNode $n) => $o->setNext($n->getStringValue()),
        ];
    }

    /**
     * Gets the next property value. The next property
     * @return string|null
    */
    public function getNext(): ?string {
        return $this->next;
    }

    /**
     * Gets the self property value. The self property
     * @return string|null
    */
    public function getSelf(): ?string {
        return $this->escapedSelf;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeStringValue('before', $this->getBefore());
        $writer->writeStringValue('self', $this->getSelf());
        $writer->writeStringValue('next', $this->getNext());
    }

    /**
     * Sets the before property value. The before property
     * @param string|null $value Value to set for the before property.
    */
    public function setBefore(?string $value): void {
        $this->before = $value;
    }

    /**
     * Sets the next property value. The next property
     * @param string|null $value Value to set for the next property.
    */
    public function setNext(?string $value): void {
        $this->next = $value;
    }

    /**
     * Sets the self property value. The self property
     * @param string|null $value Value to set for the EscapedSelf property.
    */
    public function setSelf(?string $value): void {
        $this->escapedSelf = $value;
    }

}
