<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class ProjectResourceGetDto implements Parsable
{
    /**
     * @var ProjectResourceAttributesRead|null $attributes The attributes property
    */
    private ?ProjectResourceAttributesRead $attributes = null;

    /**
     * @var ProjectResourceIncluded|null $included The included property
    */
    private ?ProjectResourceIncluded $included = null;

    /**
     * @var ResourceLinks|null $links The links property
    */
    private ?ResourceLinks $links = null;

    /**
     * @var ProjectResourceRelationships|null $relationships The relationships property
    */
    private ?ProjectResourceRelationships $relationships = null;

    /**
     * @var int|null $resourceId The resourceId property
    */
    private ?int $resourceId = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return ProjectResourceGetDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): ProjectResourceGetDto {
        return new ProjectResourceGetDto();
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return ProjectResourceAttributesRead|null
    */
    public function getAttributes(): ?ProjectResourceAttributesRead {
        return $this->attributes;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([ProjectResourceAttributesRead::class, 'createFromDiscriminatorValue'])),
            'included' => fn(ParseNode $n) => $o->setIncluded($n->getObjectValue([ProjectResourceIncluded::class, 'createFromDiscriminatorValue'])),
            'links' => fn(ParseNode $n) => $o->setLinks($n->getObjectValue([ResourceLinks::class, 'createFromDiscriminatorValue'])),
            'relationships' => fn(ParseNode $n) => $o->setRelationships($n->getObjectValue([ProjectResourceRelationships::class, 'createFromDiscriminatorValue'])),
            'resourceId' => fn(ParseNode $n) => $o->setResourceId($n->getIntegerValue()),
        ];
    }

    /**
     * Gets the included property value. The included property
     * @return ProjectResourceIncluded|null
    */
    public function getIncluded(): ?ProjectResourceIncluded {
        return $this->included;
    }

    /**
     * Gets the links property value. The links property
     * @return ResourceLinks|null
    */
    public function getLinks(): ?ResourceLinks {
        return $this->links;
    }

    /**
     * Gets the relationships property value. The relationships property
     * @return ProjectResourceRelationships|null
    */
    public function getRelationships(): ?ProjectResourceRelationships {
        return $this->relationships;
    }

    /**
     * Gets the resourceId property value. The resourceId property
     * @return int|null
    */
    public function getResourceId(): ?int {
        return $this->resourceId;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('attributes', $this->getAttributes());
        $writer->writeObjectValue('included', $this->getIncluded());
        $writer->writeObjectValue('links', $this->getLinks());
        $writer->writeObjectValue('relationships', $this->getRelationships());
        $writer->writeIntegerValue('resourceId', $this->getResourceId());
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param ProjectResourceAttributesRead|null $value Value to set for the attributes property.
    */
    public function setAttributes(?ProjectResourceAttributesRead $value): void {
        $this->attributes = $value;
    }

    /**
     * Sets the included property value. The included property
     * @param ProjectResourceIncluded|null $value Value to set for the included property.
    */
    public function setIncluded(?ProjectResourceIncluded $value): void {
        $this->included = $value;
    }

    /**
     * Sets the links property value. The links property
     * @param ResourceLinks|null $value Value to set for the links property.
    */
    public function setLinks(?ResourceLinks $value): void {
        $this->links = $value;
    }

    /**
     * Sets the relationships property value. The relationships property
     * @param ProjectResourceRelationships|null $value Value to set for the relationships property.
    */
    public function setRelationships(?ProjectResourceRelationships $value): void {
        $this->relationships = $value;
    }

    /**
     * Sets the resourceId property value. The resourceId property
     * @param int|null $value Value to set for the resourceId property.
    */
    public function setResourceId(?int $value): void {
        $this->resourceId = $value;
    }

}
