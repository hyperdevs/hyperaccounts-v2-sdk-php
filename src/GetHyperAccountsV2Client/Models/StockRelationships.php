<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class StockRelationships implements Parsable
{
    /**
     * @var GoodsReceivedNoteRelatedListRelationship|null $goodsReceivedNotes The goodsReceivedNotes property
    */
    private ?GoodsReceivedNoteRelatedListRelationship $goodsReceivedNotes = null;

    /**
     * @var SalesInvoiceItemRelatedListRelationship|null $invoiceItems The invoiceItems property
    */
    private ?SalesInvoiceItemRelatedListRelationship $invoiceItems = null;

    /**
     * @var NominalRelatedRelationship|null $nominalCode The nominalCode property
    */
    private ?NominalRelatedRelationship $nominalCode = null;

    /**
     * @var PriceRelatedListRelationship|null $prices The prices property
    */
    private ?PriceRelatedListRelationship $prices = null;

    /**
     * @var PurchaseOrderItemRelatedListRelationship|null $purchaseOrderItems The purchaseOrderItems property
    */
    private ?PurchaseOrderItemRelatedListRelationship $purchaseOrderItems = null;

    /**
     * @var SalesOrderItemRelatedListRelationship|null $salesOrderItems The salesOrderItems property
    */
    private ?SalesOrderItemRelatedListRelationship $salesOrderItems = null;

    /**
     * @var StockTransactionRelatedListRelationship|null $transactions The transactions property
    */
    private ?StockTransactionRelatedListRelationship $transactions = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return StockRelationships
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): StockRelationships {
        return new StockRelationships();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'goodsReceivedNotes' => fn(ParseNode $n) => $o->setGoodsReceivedNotes($n->getObjectValue([GoodsReceivedNoteRelatedListRelationship::class, 'createFromDiscriminatorValue'])),
            'invoiceItems' => fn(ParseNode $n) => $o->setInvoiceItems($n->getObjectValue([SalesInvoiceItemRelatedListRelationship::class, 'createFromDiscriminatorValue'])),
            'nominalCode' => fn(ParseNode $n) => $o->setNominalCode($n->getObjectValue([NominalRelatedRelationship::class, 'createFromDiscriminatorValue'])),
            'prices' => fn(ParseNode $n) => $o->setPrices($n->getObjectValue([PriceRelatedListRelationship::class, 'createFromDiscriminatorValue'])),
            'purchaseOrderItems' => fn(ParseNode $n) => $o->setPurchaseOrderItems($n->getObjectValue([PurchaseOrderItemRelatedListRelationship::class, 'createFromDiscriminatorValue'])),
            'salesOrderItems' => fn(ParseNode $n) => $o->setSalesOrderItems($n->getObjectValue([SalesOrderItemRelatedListRelationship::class, 'createFromDiscriminatorValue'])),
            'transactions' => fn(ParseNode $n) => $o->setTransactions($n->getObjectValue([StockTransactionRelatedListRelationship::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the goodsReceivedNotes property value. The goodsReceivedNotes property
     * @return GoodsReceivedNoteRelatedListRelationship|null
    */
    public function getGoodsReceivedNotes(): ?GoodsReceivedNoteRelatedListRelationship {
        return $this->goodsReceivedNotes;
    }

    /**
     * Gets the invoiceItems property value. The invoiceItems property
     * @return SalesInvoiceItemRelatedListRelationship|null
    */
    public function getInvoiceItems(): ?SalesInvoiceItemRelatedListRelationship {
        return $this->invoiceItems;
    }

    /**
     * Gets the nominalCode property value. The nominalCode property
     * @return NominalRelatedRelationship|null
    */
    public function getNominalCode(): ?NominalRelatedRelationship {
        return $this->nominalCode;
    }

    /**
     * Gets the prices property value. The prices property
     * @return PriceRelatedListRelationship|null
    */
    public function getPrices(): ?PriceRelatedListRelationship {
        return $this->prices;
    }

    /**
     * Gets the purchaseOrderItems property value. The purchaseOrderItems property
     * @return PurchaseOrderItemRelatedListRelationship|null
    */
    public function getPurchaseOrderItems(): ?PurchaseOrderItemRelatedListRelationship {
        return $this->purchaseOrderItems;
    }

    /**
     * Gets the salesOrderItems property value. The salesOrderItems property
     * @return SalesOrderItemRelatedListRelationship|null
    */
    public function getSalesOrderItems(): ?SalesOrderItemRelatedListRelationship {
        return $this->salesOrderItems;
    }

    /**
     * Gets the transactions property value. The transactions property
     * @return StockTransactionRelatedListRelationship|null
    */
    public function getTransactions(): ?StockTransactionRelatedListRelationship {
        return $this->transactions;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('goodsReceivedNotes', $this->getGoodsReceivedNotes());
        $writer->writeObjectValue('invoiceItems', $this->getInvoiceItems());
        $writer->writeObjectValue('nominalCode', $this->getNominalCode());
        $writer->writeObjectValue('prices', $this->getPrices());
        $writer->writeObjectValue('purchaseOrderItems', $this->getPurchaseOrderItems());
        $writer->writeObjectValue('salesOrderItems', $this->getSalesOrderItems());
        $writer->writeObjectValue('transactions', $this->getTransactions());
    }

    /**
     * Sets the goodsReceivedNotes property value. The goodsReceivedNotes property
     * @param GoodsReceivedNoteRelatedListRelationship|null $value Value to set for the goodsReceivedNotes property.
    */
    public function setGoodsReceivedNotes(?GoodsReceivedNoteRelatedListRelationship $value): void {
        $this->goodsReceivedNotes = $value;
    }

    /**
     * Sets the invoiceItems property value. The invoiceItems property
     * @param SalesInvoiceItemRelatedListRelationship|null $value Value to set for the invoiceItems property.
    */
    public function setInvoiceItems(?SalesInvoiceItemRelatedListRelationship $value): void {
        $this->invoiceItems = $value;
    }

    /**
     * Sets the nominalCode property value. The nominalCode property
     * @param NominalRelatedRelationship|null $value Value to set for the nominalCode property.
    */
    public function setNominalCode(?NominalRelatedRelationship $value): void {
        $this->nominalCode = $value;
    }

    /**
     * Sets the prices property value. The prices property
     * @param PriceRelatedListRelationship|null $value Value to set for the prices property.
    */
    public function setPrices(?PriceRelatedListRelationship $value): void {
        $this->prices = $value;
    }

    /**
     * Sets the purchaseOrderItems property value. The purchaseOrderItems property
     * @param PurchaseOrderItemRelatedListRelationship|null $value Value to set for the purchaseOrderItems property.
    */
    public function setPurchaseOrderItems(?PurchaseOrderItemRelatedListRelationship $value): void {
        $this->purchaseOrderItems = $value;
    }

    /**
     * Sets the salesOrderItems property value. The salesOrderItems property
     * @param SalesOrderItemRelatedListRelationship|null $value Value to set for the salesOrderItems property.
    */
    public function setSalesOrderItems(?SalesOrderItemRelatedListRelationship $value): void {
        $this->salesOrderItems = $value;
    }

    /**
     * Sets the transactions property value. The transactions property
     * @param StockTransactionRelatedListRelationship|null $value Value to set for the transactions property.
    */
    public function setTransactions(?StockTransactionRelatedListRelationship $value): void {
        $this->transactions = $value;
    }

}
