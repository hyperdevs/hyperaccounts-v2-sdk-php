<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class ProjectCostCodeGetDto implements Parsable
{
    /**
     * @var ProjectCostCodeAttributesRead|null $attributes The attributes property
    */
    private ?ProjectCostCodeAttributesRead $attributes = null;

    /**
     * @var int|null $costCodeId The costCodeId property
    */
    private ?int $costCodeId = null;

    /**
     * @var ProjectCostCodeIncluded|null $included The included property
    */
    private ?ProjectCostCodeIncluded $included = null;

    /**
     * @var ResourceLinks|null $links The links property
    */
    private ?ResourceLinks $links = null;

    /**
     * @var ProjectCostCodeRelationships|null $relationships The relationships property
    */
    private ?ProjectCostCodeRelationships $relationships = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return ProjectCostCodeGetDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): ProjectCostCodeGetDto {
        return new ProjectCostCodeGetDto();
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return ProjectCostCodeAttributesRead|null
    */
    public function getAttributes(): ?ProjectCostCodeAttributesRead {
        return $this->attributes;
    }

    /**
     * Gets the costCodeId property value. The costCodeId property
     * @return int|null
    */
    public function getCostCodeId(): ?int {
        return $this->costCodeId;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([ProjectCostCodeAttributesRead::class, 'createFromDiscriminatorValue'])),
            'costCodeId' => fn(ParseNode $n) => $o->setCostCodeId($n->getIntegerValue()),
            'included' => fn(ParseNode $n) => $o->setIncluded($n->getObjectValue([ProjectCostCodeIncluded::class, 'createFromDiscriminatorValue'])),
            'links' => fn(ParseNode $n) => $o->setLinks($n->getObjectValue([ResourceLinks::class, 'createFromDiscriminatorValue'])),
            'relationships' => fn(ParseNode $n) => $o->setRelationships($n->getObjectValue([ProjectCostCodeRelationships::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the included property value. The included property
     * @return ProjectCostCodeIncluded|null
    */
    public function getIncluded(): ?ProjectCostCodeIncluded {
        return $this->included;
    }

    /**
     * Gets the links property value. The links property
     * @return ResourceLinks|null
    */
    public function getLinks(): ?ResourceLinks {
        return $this->links;
    }

    /**
     * Gets the relationships property value. The relationships property
     * @return ProjectCostCodeRelationships|null
    */
    public function getRelationships(): ?ProjectCostCodeRelationships {
        return $this->relationships;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('attributes', $this->getAttributes());
        $writer->writeIntegerValue('costCodeId', $this->getCostCodeId());
        $writer->writeObjectValue('included', $this->getIncluded());
        $writer->writeObjectValue('links', $this->getLinks());
        $writer->writeObjectValue('relationships', $this->getRelationships());
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param ProjectCostCodeAttributesRead|null $value Value to set for the attributes property.
    */
    public function setAttributes(?ProjectCostCodeAttributesRead $value): void {
        $this->attributes = $value;
    }

    /**
     * Sets the costCodeId property value. The costCodeId property
     * @param int|null $value Value to set for the costCodeId property.
    */
    public function setCostCodeId(?int $value): void {
        $this->costCodeId = $value;
    }

    /**
     * Sets the included property value. The included property
     * @param ProjectCostCodeIncluded|null $value Value to set for the included property.
    */
    public function setIncluded(?ProjectCostCodeIncluded $value): void {
        $this->included = $value;
    }

    /**
     * Sets the links property value. The links property
     * @param ResourceLinks|null $value Value to set for the links property.
    */
    public function setLinks(?ResourceLinks $value): void {
        $this->links = $value;
    }

    /**
     * Sets the relationships property value. The relationships property
     * @param ProjectCostCodeRelationships|null $value Value to set for the relationships property.
    */
    public function setRelationships(?ProjectCostCodeRelationships $value): void {
        $this->relationships = $value;
    }

}
