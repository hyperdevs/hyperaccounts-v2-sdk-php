<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class NoReturnDtoMetaInformation implements Parsable
{
    /**
     * @var NoReturnDto|null $meta The meta property
    */
    private ?NoReturnDto $meta = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return NoReturnDtoMetaInformation
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): NoReturnDtoMetaInformation {
        return new NoReturnDtoMetaInformation();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'meta' => fn(ParseNode $n) => $o->setMeta($n->getObjectValue([NoReturnDto::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the meta property value. The meta property
     * @return NoReturnDto|null
    */
    public function getMeta(): ?NoReturnDto {
        return $this->meta;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('meta', $this->getMeta());
    }

    /**
     * Sets the meta property value. The meta property
     * @param NoReturnDto|null $value Value to set for the meta property.
    */
    public function setMeta(?NoReturnDto $value): void {
        $this->meta = $value;
    }

}
