<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class ChartOfAccountsGetDto implements Parsable
{
    /**
     * @var ChartOfAccountsAttributesRead|null $attributes The attributes property
    */
    private ?ChartOfAccountsAttributesRead $attributes = null;

    /**
     * @var int|null $chart The chart property
    */
    private ?int $chart = null;

    /**
     * @var ChartOfAccountsIncluded|null $included The included property
    */
    private ?ChartOfAccountsIncluded $included = null;

    /**
     * @var ResourceLinks|null $links The links property
    */
    private ?ResourceLinks $links = null;

    /**
     * @var ChartOfAccountsRelationships|null $relationships The relationships property
    */
    private ?ChartOfAccountsRelationships $relationships = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return ChartOfAccountsGetDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): ChartOfAccountsGetDto {
        return new ChartOfAccountsGetDto();
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return ChartOfAccountsAttributesRead|null
    */
    public function getAttributes(): ?ChartOfAccountsAttributesRead {
        return $this->attributes;
    }

    /**
     * Gets the chart property value. The chart property
     * @return int|null
    */
    public function getChart(): ?int {
        return $this->chart;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([ChartOfAccountsAttributesRead::class, 'createFromDiscriminatorValue'])),
            'chart' => fn(ParseNode $n) => $o->setChart($n->getIntegerValue()),
            'included' => fn(ParseNode $n) => $o->setIncluded($n->getObjectValue([ChartOfAccountsIncluded::class, 'createFromDiscriminatorValue'])),
            'links' => fn(ParseNode $n) => $o->setLinks($n->getObjectValue([ResourceLinks::class, 'createFromDiscriminatorValue'])),
            'relationships' => fn(ParseNode $n) => $o->setRelationships($n->getObjectValue([ChartOfAccountsRelationships::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the included property value. The included property
     * @return ChartOfAccountsIncluded|null
    */
    public function getIncluded(): ?ChartOfAccountsIncluded {
        return $this->included;
    }

    /**
     * Gets the links property value. The links property
     * @return ResourceLinks|null
    */
    public function getLinks(): ?ResourceLinks {
        return $this->links;
    }

    /**
     * Gets the relationships property value. The relationships property
     * @return ChartOfAccountsRelationships|null
    */
    public function getRelationships(): ?ChartOfAccountsRelationships {
        return $this->relationships;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('attributes', $this->getAttributes());
        $writer->writeIntegerValue('chart', $this->getChart());
        $writer->writeObjectValue('included', $this->getIncluded());
        $writer->writeObjectValue('links', $this->getLinks());
        $writer->writeObjectValue('relationships', $this->getRelationships());
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param ChartOfAccountsAttributesRead|null $value Value to set for the attributes property.
    */
    public function setAttributes(?ChartOfAccountsAttributesRead $value): void {
        $this->attributes = $value;
    }

    /**
     * Sets the chart property value. The chart property
     * @param int|null $value Value to set for the chart property.
    */
    public function setChart(?int $value): void {
        $this->chart = $value;
    }

    /**
     * Sets the included property value. The included property
     * @param ChartOfAccountsIncluded|null $value Value to set for the included property.
    */
    public function setIncluded(?ChartOfAccountsIncluded $value): void {
        $this->included = $value;
    }

    /**
     * Sets the links property value. The links property
     * @param ResourceLinks|null $value Value to set for the links property.
    */
    public function setLinks(?ResourceLinks $value): void {
        $this->links = $value;
    }

    /**
     * Sets the relationships property value. The relationships property
     * @param ChartOfAccountsRelationships|null $value Value to set for the relationships property.
    */
    public function setRelationships(?ChartOfAccountsRelationships $value): void {
        $this->relationships = $value;
    }

}
