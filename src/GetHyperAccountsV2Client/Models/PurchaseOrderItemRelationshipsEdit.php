<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class PurchaseOrderItemRelationshipsEdit implements Parsable
{
    /**
     * @var NominalRelatedRelationshipWrite|null $nominalCode The nominalCode property
    */
    private ?NominalRelatedRelationshipWrite $nominalCode = null;

    /**
     * @var ProjectRelatedRelationshipWrite|null $project The project property
    */
    private ?ProjectRelatedRelationshipWrite $project = null;

    /**
     * @var ProjectCostCodeRelatedRelationshipWrite|null $projectCostCode The projectCostCode property
    */
    private ?ProjectCostCodeRelatedRelationshipWrite $projectCostCode = null;

    /**
     * @var StockRelatedRelationshipWrite|null $stock The stock property
    */
    private ?StockRelatedRelationshipWrite $stock = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return PurchaseOrderItemRelationshipsEdit
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): PurchaseOrderItemRelationshipsEdit {
        return new PurchaseOrderItemRelationshipsEdit();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'nominalCode' => fn(ParseNode $n) => $o->setNominalCode($n->getObjectValue([NominalRelatedRelationshipWrite::class, 'createFromDiscriminatorValue'])),
            'project' => fn(ParseNode $n) => $o->setProject($n->getObjectValue([ProjectRelatedRelationshipWrite::class, 'createFromDiscriminatorValue'])),
            'projectCostCode' => fn(ParseNode $n) => $o->setProjectCostCode($n->getObjectValue([ProjectCostCodeRelatedRelationshipWrite::class, 'createFromDiscriminatorValue'])),
            'stock' => fn(ParseNode $n) => $o->setStock($n->getObjectValue([StockRelatedRelationshipWrite::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the nominalCode property value. The nominalCode property
     * @return NominalRelatedRelationshipWrite|null
    */
    public function getNominalCode(): ?NominalRelatedRelationshipWrite {
        return $this->nominalCode;
    }

    /**
     * Gets the project property value. The project property
     * @return ProjectRelatedRelationshipWrite|null
    */
    public function getProject(): ?ProjectRelatedRelationshipWrite {
        return $this->project;
    }

    /**
     * Gets the projectCostCode property value. The projectCostCode property
     * @return ProjectCostCodeRelatedRelationshipWrite|null
    */
    public function getProjectCostCode(): ?ProjectCostCodeRelatedRelationshipWrite {
        return $this->projectCostCode;
    }

    /**
     * Gets the stock property value. The stock property
     * @return StockRelatedRelationshipWrite|null
    */
    public function getStock(): ?StockRelatedRelationshipWrite {
        return $this->stock;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('nominalCode', $this->getNominalCode());
        $writer->writeObjectValue('project', $this->getProject());
        $writer->writeObjectValue('projectCostCode', $this->getProjectCostCode());
        $writer->writeObjectValue('stock', $this->getStock());
    }

    /**
     * Sets the nominalCode property value. The nominalCode property
     * @param NominalRelatedRelationshipWrite|null $value Value to set for the nominalCode property.
    */
    public function setNominalCode(?NominalRelatedRelationshipWrite $value): void {
        $this->nominalCode = $value;
    }

    /**
     * Sets the project property value. The project property
     * @param ProjectRelatedRelationshipWrite|null $value Value to set for the project property.
    */
    public function setProject(?ProjectRelatedRelationshipWrite $value): void {
        $this->project = $value;
    }

    /**
     * Sets the projectCostCode property value. The projectCostCode property
     * @param ProjectCostCodeRelatedRelationshipWrite|null $value Value to set for the projectCostCode property.
    */
    public function setProjectCostCode(?ProjectCostCodeRelatedRelationshipWrite $value): void {
        $this->projectCostCode = $value;
    }

    /**
     * Sets the stock property value. The stock property
     * @param StockRelatedRelationshipWrite|null $value Value to set for the stock property.
    */
    public function setStock(?StockRelatedRelationshipWrite $value): void {
        $this->stock = $value;
    }

}
