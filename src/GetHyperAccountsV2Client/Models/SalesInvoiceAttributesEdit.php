<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use DateTime;
use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class SalesInvoiceAttributesEdit implements Parsable
{
    /**
     * @var string|null $address1 The address1 property
    */
    private ?string $address1 = null;

    /**
     * @var string|null $address2 The address2 property
    */
    private ?string $address2 = null;

    /**
     * @var string|null $address3 The address3 property
    */
    private ?string $address3 = null;

    /**
     * @var string|null $address4 The address4 property
    */
    private ?string $address4 = null;

    /**
     * @var string|null $address5 The address5 property
    */
    private ?string $address5 = null;

    /**
     * @var float|null $amountPrepaid The amountPrepaid property
    */
    private ?float $amountPrepaid = null;

    /**
     * @var string|null $analysis1 The analysis1 property
    */
    private ?string $analysis1 = null;

    /**
     * @var string|null $analysis2 The analysis2 property
    */
    private ?string $analysis2 = null;

    /**
     * @var string|null $analysis3 The analysis3 property
    */
    private ?string $analysis3 = null;

    /**
     * @var int|null $carrDeptNumber The carrDeptNumber property
    */
    private ?int $carrDeptNumber = null;

    /**
     * @var float|null $carrNet The carrNet property
    */
    private ?float $carrNet = null;

    /**
     * @var string|null $carrNomCode The carrNomCode property
    */
    private ?string $carrNomCode = null;

    /**
     * @var float|null $carrTax The carrTax property
    */
    private ?float $carrTax = null;

    /**
     * @var SalesInvoiceAttributesEdit_carrTaxCode|null $carrTaxCode The carrTaxCode property
    */
    private ?SalesInvoiceAttributesEdit_carrTaxCode $carrTaxCode = null;

    /**
     * @var string|null $contactName The contactName property
    */
    private ?string $contactName = null;

    /**
     * @var int|null $currency The currency property
    */
    private ?int $currency = null;

    /**
     * @var float|null $custDiscRate The custDiscRate property
    */
    private ?float $custDiscRate = null;

    /**
     * @var string|null $custOrderNumber The custOrderNumber property
    */
    private ?string $custOrderNumber = null;

    /**
     * @var string|null $custTelNumber The custTelNumber property
    */
    private ?string $custTelNumber = null;

    /**
     * @var string|null $delAddress1 The delAddress1 property
    */
    private ?string $delAddress1 = null;

    /**
     * @var string|null $delAddress2 The delAddress2 property
    */
    private ?string $delAddress2 = null;

    /**
     * @var string|null $delAddress3 The delAddress3 property
    */
    private ?string $delAddress3 = null;

    /**
     * @var string|null $delAddress4 The delAddress4 property
    */
    private ?string $delAddress4 = null;

    /**
     * @var string|null $delAddress5 The delAddress5 property
    */
    private ?string $delAddress5 = null;

    /**
     * @var string|null $dunsNumber The dunsNumber property
    */
    private ?string $dunsNumber = null;

    /**
     * @var float|null $euroGross The euroGross property
    */
    private ?float $euroGross = null;

    /**
     * @var float|null $euroRate The euroRate property
    */
    private ?float $euroRate = null;

    /**
     * @var float|null $foreignRate The foreignRate property
    */
    private ?float $foreignRate = null;

    /**
     * @var int|null $gdnNumber The gdnNumber property
    */
    private ?int $gdnNumber = null;

    /**
     * @var int|null $globalDeptNumber The globalDeptNumber property
    */
    private ?int $globalDeptNumber = null;

    /**
     * @var string|null $globalDetails The globalDetails property
    */
    private ?string $globalDetails = null;

    /**
     * @var string|null $globalNomCode The globalNomCode property
    */
    private ?string $globalNomCode = null;

    /**
     * @var SalesInvoiceAttributesEdit_globalTaxCode|null $globalTaxCode The globalTaxCode property
    */
    private ?SalesInvoiceAttributesEdit_globalTaxCode $globalTaxCode = null;

    /**
     * @var DateTime|null $invoiceDate The invoiceDate property
    */
    private ?DateTime $invoiceDate = null;

    /**
     * @var int|null $invoiceTypeCode The invoiceTypeCode property
    */
    private ?int $invoiceTypeCode = null;

    /**
     * @var float|null $itemsNet The itemsNet property
    */
    private ?float $itemsNet = null;

    /**
     * @var float|null $itemsTax The itemsTax property
    */
    private ?float $itemsTax = null;

    /**
     * @var string|null $name The name property
    */
    private ?string $name = null;

    /**
     * @var string|null $notes1 The notes1 property
    */
    private ?string $notes1 = null;

    /**
     * @var string|null $notes2 The notes2 property
    */
    private ?string $notes2 = null;

    /**
     * @var string|null $notes3 The notes3 property
    */
    private ?string $notes3 = null;

    /**
     * @var string|null $orderNumber The orderNumber property
    */
    private ?string $orderNumber = null;

    /**
     * @var DateTime|null $paymentDueDate The paymentDueDate property
    */
    private ?DateTime $paymentDueDate = null;

    /**
     * @var string|null $paymentRef The paymentRef property
    */
    private ?string $paymentRef = null;

    /**
     * @var int|null $paymentType The paymentType property
    */
    private ?int $paymentType = null;

    /**
     * @var int|null $printedCode The printedCode property
    */
    private ?int $printedCode = null;

    /**
     * @var SalesInvoiceAttributesEdit_quoteStatus|null $quoteStatus The quoteStatus property
    */
    private ?SalesInvoiceAttributesEdit_quoteStatus $quoteStatus = null;

    /**
     * @var float|null $settlementDiscRate The settlementDiscRate property
    */
    private ?float $settlementDiscRate = null;

    /**
     * @var int|null $settlementDueDays The settlementDueDays property
    */
    private ?int $settlementDueDays = null;

    /**
     * @var string|null $takenBy The takenBy property
    */
    private ?string $takenBy = null;

    /**
     * @var bool|null $updateLedgers The updateLedgers property
    */
    private ?bool $updateLedgers = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return SalesInvoiceAttributesEdit
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): SalesInvoiceAttributesEdit {
        return new SalesInvoiceAttributesEdit();
    }

    /**
     * Gets the address1 property value. The address1 property
     * @return string|null
    */
    public function getAddress1(): ?string {
        return $this->address1;
    }

    /**
     * Gets the address2 property value. The address2 property
     * @return string|null
    */
    public function getAddress2(): ?string {
        return $this->address2;
    }

    /**
     * Gets the address3 property value. The address3 property
     * @return string|null
    */
    public function getAddress3(): ?string {
        return $this->address3;
    }

    /**
     * Gets the address4 property value. The address4 property
     * @return string|null
    */
    public function getAddress4(): ?string {
        return $this->address4;
    }

    /**
     * Gets the address5 property value. The address5 property
     * @return string|null
    */
    public function getAddress5(): ?string {
        return $this->address5;
    }

    /**
     * Gets the amountPrepaid property value. The amountPrepaid property
     * @return float|null
    */
    public function getAmountPrepaid(): ?float {
        return $this->amountPrepaid;
    }

    /**
     * Gets the analysis1 property value. The analysis1 property
     * @return string|null
    */
    public function getAnalysis1(): ?string {
        return $this->analysis1;
    }

    /**
     * Gets the analysis2 property value. The analysis2 property
     * @return string|null
    */
    public function getAnalysis2(): ?string {
        return $this->analysis2;
    }

    /**
     * Gets the analysis3 property value. The analysis3 property
     * @return string|null
    */
    public function getAnalysis3(): ?string {
        return $this->analysis3;
    }

    /**
     * Gets the carrDeptNumber property value. The carrDeptNumber property
     * @return int|null
    */
    public function getCarrDeptNumber(): ?int {
        return $this->carrDeptNumber;
    }

    /**
     * Gets the carrNet property value. The carrNet property
     * @return float|null
    */
    public function getCarrNet(): ?float {
        return $this->carrNet;
    }

    /**
     * Gets the carrNomCode property value. The carrNomCode property
     * @return string|null
    */
    public function getCarrNomCode(): ?string {
        return $this->carrNomCode;
    }

    /**
     * Gets the carrTax property value. The carrTax property
     * @return float|null
    */
    public function getCarrTax(): ?float {
        return $this->carrTax;
    }

    /**
     * Gets the carrTaxCode property value. The carrTaxCode property
     * @return SalesInvoiceAttributesEdit_carrTaxCode|null
    */
    public function getCarrTaxCode(): ?SalesInvoiceAttributesEdit_carrTaxCode {
        return $this->carrTaxCode;
    }

    /**
     * Gets the contactName property value. The contactName property
     * @return string|null
    */
    public function getContactName(): ?string {
        return $this->contactName;
    }

    /**
     * Gets the currency property value. The currency property
     * @return int|null
    */
    public function getCurrency(): ?int {
        return $this->currency;
    }

    /**
     * Gets the custDiscRate property value. The custDiscRate property
     * @return float|null
    */
    public function getCustDiscRate(): ?float {
        return $this->custDiscRate;
    }

    /**
     * Gets the custOrderNumber property value. The custOrderNumber property
     * @return string|null
    */
    public function getCustOrderNumber(): ?string {
        return $this->custOrderNumber;
    }

    /**
     * Gets the custTelNumber property value. The custTelNumber property
     * @return string|null
    */
    public function getCustTelNumber(): ?string {
        return $this->custTelNumber;
    }

    /**
     * Gets the delAddress1 property value. The delAddress1 property
     * @return string|null
    */
    public function getDelAddress1(): ?string {
        return $this->delAddress1;
    }

    /**
     * Gets the delAddress2 property value. The delAddress2 property
     * @return string|null
    */
    public function getDelAddress2(): ?string {
        return $this->delAddress2;
    }

    /**
     * Gets the delAddress3 property value. The delAddress3 property
     * @return string|null
    */
    public function getDelAddress3(): ?string {
        return $this->delAddress3;
    }

    /**
     * Gets the delAddress4 property value. The delAddress4 property
     * @return string|null
    */
    public function getDelAddress4(): ?string {
        return $this->delAddress4;
    }

    /**
     * Gets the delAddress5 property value. The delAddress5 property
     * @return string|null
    */
    public function getDelAddress5(): ?string {
        return $this->delAddress5;
    }

    /**
     * Gets the dunsNumber property value. The dunsNumber property
     * @return string|null
    */
    public function getDunsNumber(): ?string {
        return $this->dunsNumber;
    }

    /**
     * Gets the euroGross property value. The euroGross property
     * @return float|null
    */
    public function getEuroGross(): ?float {
        return $this->euroGross;
    }

    /**
     * Gets the euroRate property value. The euroRate property
     * @return float|null
    */
    public function getEuroRate(): ?float {
        return $this->euroRate;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'address1' => fn(ParseNode $n) => $o->setAddress1($n->getStringValue()),
            'address2' => fn(ParseNode $n) => $o->setAddress2($n->getStringValue()),
            'address3' => fn(ParseNode $n) => $o->setAddress3($n->getStringValue()),
            'address4' => fn(ParseNode $n) => $o->setAddress4($n->getStringValue()),
            'address5' => fn(ParseNode $n) => $o->setAddress5($n->getStringValue()),
            'amountPrepaid' => fn(ParseNode $n) => $o->setAmountPrepaid($n->getFloatValue()),
            'analysis1' => fn(ParseNode $n) => $o->setAnalysis1($n->getStringValue()),
            'analysis2' => fn(ParseNode $n) => $o->setAnalysis2($n->getStringValue()),
            'analysis3' => fn(ParseNode $n) => $o->setAnalysis3($n->getStringValue()),
            'carrDeptNumber' => fn(ParseNode $n) => $o->setCarrDeptNumber($n->getIntegerValue()),
            'carrNet' => fn(ParseNode $n) => $o->setCarrNet($n->getFloatValue()),
            'carrNomCode' => fn(ParseNode $n) => $o->setCarrNomCode($n->getStringValue()),
            'carrTax' => fn(ParseNode $n) => $o->setCarrTax($n->getFloatValue()),
            'carrTaxCode' => fn(ParseNode $n) => $o->setCarrTaxCode($n->getEnumValue(SalesInvoiceAttributesEdit_carrTaxCode::class)),
            'contactName' => fn(ParseNode $n) => $o->setContactName($n->getStringValue()),
            'currency' => fn(ParseNode $n) => $o->setCurrency($n->getIntegerValue()),
            'custDiscRate' => fn(ParseNode $n) => $o->setCustDiscRate($n->getFloatValue()),
            'custOrderNumber' => fn(ParseNode $n) => $o->setCustOrderNumber($n->getStringValue()),
            'custTelNumber' => fn(ParseNode $n) => $o->setCustTelNumber($n->getStringValue()),
            'delAddress1' => fn(ParseNode $n) => $o->setDelAddress1($n->getStringValue()),
            'delAddress2' => fn(ParseNode $n) => $o->setDelAddress2($n->getStringValue()),
            'delAddress3' => fn(ParseNode $n) => $o->setDelAddress3($n->getStringValue()),
            'delAddress4' => fn(ParseNode $n) => $o->setDelAddress4($n->getStringValue()),
            'delAddress5' => fn(ParseNode $n) => $o->setDelAddress5($n->getStringValue()),
            'dunsNumber' => fn(ParseNode $n) => $o->setDunsNumber($n->getStringValue()),
            'euroGross' => fn(ParseNode $n) => $o->setEuroGross($n->getFloatValue()),
            'euroRate' => fn(ParseNode $n) => $o->setEuroRate($n->getFloatValue()),
            'foreignRate' => fn(ParseNode $n) => $o->setForeignRate($n->getFloatValue()),
            'gdnNumber' => fn(ParseNode $n) => $o->setGdnNumber($n->getIntegerValue()),
            'globalDeptNumber' => fn(ParseNode $n) => $o->setGlobalDeptNumber($n->getIntegerValue()),
            'globalDetails' => fn(ParseNode $n) => $o->setGlobalDetails($n->getStringValue()),
            'globalNomCode' => fn(ParseNode $n) => $o->setGlobalNomCode($n->getStringValue()),
            'globalTaxCode' => fn(ParseNode $n) => $o->setGlobalTaxCode($n->getEnumValue(SalesInvoiceAttributesEdit_globalTaxCode::class)),
            'invoiceDate' => fn(ParseNode $n) => $o->setInvoiceDate($n->getDateTimeValue()),
            'invoiceTypeCode' => fn(ParseNode $n) => $o->setInvoiceTypeCode($n->getIntegerValue()),
            'itemsNet' => fn(ParseNode $n) => $o->setItemsNet($n->getFloatValue()),
            'itemsTax' => fn(ParseNode $n) => $o->setItemsTax($n->getFloatValue()),
            'name' => fn(ParseNode $n) => $o->setName($n->getStringValue()),
            'notes1' => fn(ParseNode $n) => $o->setNotes1($n->getStringValue()),
            'notes2' => fn(ParseNode $n) => $o->setNotes2($n->getStringValue()),
            'notes3' => fn(ParseNode $n) => $o->setNotes3($n->getStringValue()),
            'orderNumber' => fn(ParseNode $n) => $o->setOrderNumber($n->getStringValue()),
            'paymentDueDate' => fn(ParseNode $n) => $o->setPaymentDueDate($n->getDateTimeValue()),
            'paymentRef' => fn(ParseNode $n) => $o->setPaymentRef($n->getStringValue()),
            'paymentType' => fn(ParseNode $n) => $o->setPaymentType($n->getIntegerValue()),
            'printedCode' => fn(ParseNode $n) => $o->setPrintedCode($n->getIntegerValue()),
            'quoteStatus' => fn(ParseNode $n) => $o->setQuoteStatus($n->getEnumValue(SalesInvoiceAttributesEdit_quoteStatus::class)),
            'settlementDiscRate' => fn(ParseNode $n) => $o->setSettlementDiscRate($n->getFloatValue()),
            'settlementDueDays' => fn(ParseNode $n) => $o->setSettlementDueDays($n->getIntegerValue()),
            'takenBy' => fn(ParseNode $n) => $o->setTakenBy($n->getStringValue()),
            'updateLedgers' => fn(ParseNode $n) => $o->setUpdateLedgers($n->getBooleanValue()),
        ];
    }

    /**
     * Gets the foreignRate property value. The foreignRate property
     * @return float|null
    */
    public function getForeignRate(): ?float {
        return $this->foreignRate;
    }

    /**
     * Gets the gdnNumber property value. The gdnNumber property
     * @return int|null
    */
    public function getGdnNumber(): ?int {
        return $this->gdnNumber;
    }

    /**
     * Gets the globalDeptNumber property value. The globalDeptNumber property
     * @return int|null
    */
    public function getGlobalDeptNumber(): ?int {
        return $this->globalDeptNumber;
    }

    /**
     * Gets the globalDetails property value. The globalDetails property
     * @return string|null
    */
    public function getGlobalDetails(): ?string {
        return $this->globalDetails;
    }

    /**
     * Gets the globalNomCode property value. The globalNomCode property
     * @return string|null
    */
    public function getGlobalNomCode(): ?string {
        return $this->globalNomCode;
    }

    /**
     * Gets the globalTaxCode property value. The globalTaxCode property
     * @return SalesInvoiceAttributesEdit_globalTaxCode|null
    */
    public function getGlobalTaxCode(): ?SalesInvoiceAttributesEdit_globalTaxCode {
        return $this->globalTaxCode;
    }

    /**
     * Gets the invoiceDate property value. The invoiceDate property
     * @return DateTime|null
    */
    public function getInvoiceDate(): ?DateTime {
        return $this->invoiceDate;
    }

    /**
     * Gets the invoiceTypeCode property value. The invoiceTypeCode property
     * @return int|null
    */
    public function getInvoiceTypeCode(): ?int {
        return $this->invoiceTypeCode;
    }

    /**
     * Gets the itemsNet property value. The itemsNet property
     * @return float|null
    */
    public function getItemsNet(): ?float {
        return $this->itemsNet;
    }

    /**
     * Gets the itemsTax property value. The itemsTax property
     * @return float|null
    */
    public function getItemsTax(): ?float {
        return $this->itemsTax;
    }

    /**
     * Gets the name property value. The name property
     * @return string|null
    */
    public function getName(): ?string {
        return $this->name;
    }

    /**
     * Gets the notes1 property value. The notes1 property
     * @return string|null
    */
    public function getNotes1(): ?string {
        return $this->notes1;
    }

    /**
     * Gets the notes2 property value. The notes2 property
     * @return string|null
    */
    public function getNotes2(): ?string {
        return $this->notes2;
    }

    /**
     * Gets the notes3 property value. The notes3 property
     * @return string|null
    */
    public function getNotes3(): ?string {
        return $this->notes3;
    }

    /**
     * Gets the orderNumber property value. The orderNumber property
     * @return string|null
    */
    public function getOrderNumber(): ?string {
        return $this->orderNumber;
    }

    /**
     * Gets the paymentDueDate property value. The paymentDueDate property
     * @return DateTime|null
    */
    public function getPaymentDueDate(): ?DateTime {
        return $this->paymentDueDate;
    }

    /**
     * Gets the paymentRef property value. The paymentRef property
     * @return string|null
    */
    public function getPaymentRef(): ?string {
        return $this->paymentRef;
    }

    /**
     * Gets the paymentType property value. The paymentType property
     * @return int|null
    */
    public function getPaymentType(): ?int {
        return $this->paymentType;
    }

    /**
     * Gets the printedCode property value. The printedCode property
     * @return int|null
    */
    public function getPrintedCode(): ?int {
        return $this->printedCode;
    }

    /**
     * Gets the quoteStatus property value. The quoteStatus property
     * @return SalesInvoiceAttributesEdit_quoteStatus|null
    */
    public function getQuoteStatus(): ?SalesInvoiceAttributesEdit_quoteStatus {
        return $this->quoteStatus;
    }

    /**
     * Gets the settlementDiscRate property value. The settlementDiscRate property
     * @return float|null
    */
    public function getSettlementDiscRate(): ?float {
        return $this->settlementDiscRate;
    }

    /**
     * Gets the settlementDueDays property value. The settlementDueDays property
     * @return int|null
    */
    public function getSettlementDueDays(): ?int {
        return $this->settlementDueDays;
    }

    /**
     * Gets the takenBy property value. The takenBy property
     * @return string|null
    */
    public function getTakenBy(): ?string {
        return $this->takenBy;
    }

    /**
     * Gets the updateLedgers property value. The updateLedgers property
     * @return bool|null
    */
    public function getUpdateLedgers(): ?bool {
        return $this->updateLedgers;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeStringValue('address1', $this->getAddress1());
        $writer->writeStringValue('address2', $this->getAddress2());
        $writer->writeStringValue('address3', $this->getAddress3());
        $writer->writeStringValue('address4', $this->getAddress4());
        $writer->writeStringValue('address5', $this->getAddress5());
        $writer->writeFloatValue('amountPrepaid', $this->getAmountPrepaid());
        $writer->writeStringValue('analysis1', $this->getAnalysis1());
        $writer->writeStringValue('analysis2', $this->getAnalysis2());
        $writer->writeStringValue('analysis3', $this->getAnalysis3());
        $writer->writeIntegerValue('carrDeptNumber', $this->getCarrDeptNumber());
        $writer->writeFloatValue('carrNet', $this->getCarrNet());
        $writer->writeStringValue('carrNomCode', $this->getCarrNomCode());
        $writer->writeFloatValue('carrTax', $this->getCarrTax());
        $writer->writeEnumValue('carrTaxCode', $this->getCarrTaxCode());
        $writer->writeStringValue('contactName', $this->getContactName());
        $writer->writeIntegerValue('currency', $this->getCurrency());
        $writer->writeFloatValue('custDiscRate', $this->getCustDiscRate());
        $writer->writeStringValue('custOrderNumber', $this->getCustOrderNumber());
        $writer->writeStringValue('custTelNumber', $this->getCustTelNumber());
        $writer->writeStringValue('delAddress1', $this->getDelAddress1());
        $writer->writeStringValue('delAddress2', $this->getDelAddress2());
        $writer->writeStringValue('delAddress3', $this->getDelAddress3());
        $writer->writeStringValue('delAddress4', $this->getDelAddress4());
        $writer->writeStringValue('delAddress5', $this->getDelAddress5());
        $writer->writeStringValue('dunsNumber', $this->getDunsNumber());
        $writer->writeFloatValue('euroGross', $this->getEuroGross());
        $writer->writeFloatValue('euroRate', $this->getEuroRate());
        $writer->writeFloatValue('foreignRate', $this->getForeignRate());
        $writer->writeIntegerValue('gdnNumber', $this->getGdnNumber());
        $writer->writeIntegerValue('globalDeptNumber', $this->getGlobalDeptNumber());
        $writer->writeStringValue('globalDetails', $this->getGlobalDetails());
        $writer->writeStringValue('globalNomCode', $this->getGlobalNomCode());
        $writer->writeEnumValue('globalTaxCode', $this->getGlobalTaxCode());
        $writer->writeDateTimeValue('invoiceDate', $this->getInvoiceDate());
        $writer->writeIntegerValue('invoiceTypeCode', $this->getInvoiceTypeCode());
        $writer->writeFloatValue('itemsNet', $this->getItemsNet());
        $writer->writeFloatValue('itemsTax', $this->getItemsTax());
        $writer->writeStringValue('name', $this->getName());
        $writer->writeStringValue('notes1', $this->getNotes1());
        $writer->writeStringValue('notes2', $this->getNotes2());
        $writer->writeStringValue('notes3', $this->getNotes3());
        $writer->writeStringValue('orderNumber', $this->getOrderNumber());
        $writer->writeDateTimeValue('paymentDueDate', $this->getPaymentDueDate());
        $writer->writeStringValue('paymentRef', $this->getPaymentRef());
        $writer->writeIntegerValue('paymentType', $this->getPaymentType());
        $writer->writeIntegerValue('printedCode', $this->getPrintedCode());
        $writer->writeEnumValue('quoteStatus', $this->getQuoteStatus());
        $writer->writeFloatValue('settlementDiscRate', $this->getSettlementDiscRate());
        $writer->writeIntegerValue('settlementDueDays', $this->getSettlementDueDays());
        $writer->writeStringValue('takenBy', $this->getTakenBy());
        $writer->writeBooleanValue('updateLedgers', $this->getUpdateLedgers());
    }

    /**
     * Sets the address1 property value. The address1 property
     * @param string|null $value Value to set for the address1 property.
    */
    public function setAddress1(?string $value): void {
        $this->address1 = $value;
    }

    /**
     * Sets the address2 property value. The address2 property
     * @param string|null $value Value to set for the address2 property.
    */
    public function setAddress2(?string $value): void {
        $this->address2 = $value;
    }

    /**
     * Sets the address3 property value. The address3 property
     * @param string|null $value Value to set for the address3 property.
    */
    public function setAddress3(?string $value): void {
        $this->address3 = $value;
    }

    /**
     * Sets the address4 property value. The address4 property
     * @param string|null $value Value to set for the address4 property.
    */
    public function setAddress4(?string $value): void {
        $this->address4 = $value;
    }

    /**
     * Sets the address5 property value. The address5 property
     * @param string|null $value Value to set for the address5 property.
    */
    public function setAddress5(?string $value): void {
        $this->address5 = $value;
    }

    /**
     * Sets the amountPrepaid property value. The amountPrepaid property
     * @param float|null $value Value to set for the amountPrepaid property.
    */
    public function setAmountPrepaid(?float $value): void {
        $this->amountPrepaid = $value;
    }

    /**
     * Sets the analysis1 property value. The analysis1 property
     * @param string|null $value Value to set for the analysis1 property.
    */
    public function setAnalysis1(?string $value): void {
        $this->analysis1 = $value;
    }

    /**
     * Sets the analysis2 property value. The analysis2 property
     * @param string|null $value Value to set for the analysis2 property.
    */
    public function setAnalysis2(?string $value): void {
        $this->analysis2 = $value;
    }

    /**
     * Sets the analysis3 property value. The analysis3 property
     * @param string|null $value Value to set for the analysis3 property.
    */
    public function setAnalysis3(?string $value): void {
        $this->analysis3 = $value;
    }

    /**
     * Sets the carrDeptNumber property value. The carrDeptNumber property
     * @param int|null $value Value to set for the carrDeptNumber property.
    */
    public function setCarrDeptNumber(?int $value): void {
        $this->carrDeptNumber = $value;
    }

    /**
     * Sets the carrNet property value. The carrNet property
     * @param float|null $value Value to set for the carrNet property.
    */
    public function setCarrNet(?float $value): void {
        $this->carrNet = $value;
    }

    /**
     * Sets the carrNomCode property value. The carrNomCode property
     * @param string|null $value Value to set for the carrNomCode property.
    */
    public function setCarrNomCode(?string $value): void {
        $this->carrNomCode = $value;
    }

    /**
     * Sets the carrTax property value. The carrTax property
     * @param float|null $value Value to set for the carrTax property.
    */
    public function setCarrTax(?float $value): void {
        $this->carrTax = $value;
    }

    /**
     * Sets the carrTaxCode property value. The carrTaxCode property
     * @param SalesInvoiceAttributesEdit_carrTaxCode|null $value Value to set for the carrTaxCode property.
    */
    public function setCarrTaxCode(?SalesInvoiceAttributesEdit_carrTaxCode $value): void {
        $this->carrTaxCode = $value;
    }

    /**
     * Sets the contactName property value. The contactName property
     * @param string|null $value Value to set for the contactName property.
    */
    public function setContactName(?string $value): void {
        $this->contactName = $value;
    }

    /**
     * Sets the currency property value. The currency property
     * @param int|null $value Value to set for the currency property.
    */
    public function setCurrency(?int $value): void {
        $this->currency = $value;
    }

    /**
     * Sets the custDiscRate property value. The custDiscRate property
     * @param float|null $value Value to set for the custDiscRate property.
    */
    public function setCustDiscRate(?float $value): void {
        $this->custDiscRate = $value;
    }

    /**
     * Sets the custOrderNumber property value. The custOrderNumber property
     * @param string|null $value Value to set for the custOrderNumber property.
    */
    public function setCustOrderNumber(?string $value): void {
        $this->custOrderNumber = $value;
    }

    /**
     * Sets the custTelNumber property value. The custTelNumber property
     * @param string|null $value Value to set for the custTelNumber property.
    */
    public function setCustTelNumber(?string $value): void {
        $this->custTelNumber = $value;
    }

    /**
     * Sets the delAddress1 property value. The delAddress1 property
     * @param string|null $value Value to set for the delAddress1 property.
    */
    public function setDelAddress1(?string $value): void {
        $this->delAddress1 = $value;
    }

    /**
     * Sets the delAddress2 property value. The delAddress2 property
     * @param string|null $value Value to set for the delAddress2 property.
    */
    public function setDelAddress2(?string $value): void {
        $this->delAddress2 = $value;
    }

    /**
     * Sets the delAddress3 property value. The delAddress3 property
     * @param string|null $value Value to set for the delAddress3 property.
    */
    public function setDelAddress3(?string $value): void {
        $this->delAddress3 = $value;
    }

    /**
     * Sets the delAddress4 property value. The delAddress4 property
     * @param string|null $value Value to set for the delAddress4 property.
    */
    public function setDelAddress4(?string $value): void {
        $this->delAddress4 = $value;
    }

    /**
     * Sets the delAddress5 property value. The delAddress5 property
     * @param string|null $value Value to set for the delAddress5 property.
    */
    public function setDelAddress5(?string $value): void {
        $this->delAddress5 = $value;
    }

    /**
     * Sets the dunsNumber property value. The dunsNumber property
     * @param string|null $value Value to set for the dunsNumber property.
    */
    public function setDunsNumber(?string $value): void {
        $this->dunsNumber = $value;
    }

    /**
     * Sets the euroGross property value. The euroGross property
     * @param float|null $value Value to set for the euroGross property.
    */
    public function setEuroGross(?float $value): void {
        $this->euroGross = $value;
    }

    /**
     * Sets the euroRate property value. The euroRate property
     * @param float|null $value Value to set for the euroRate property.
    */
    public function setEuroRate(?float $value): void {
        $this->euroRate = $value;
    }

    /**
     * Sets the foreignRate property value. The foreignRate property
     * @param float|null $value Value to set for the foreignRate property.
    */
    public function setForeignRate(?float $value): void {
        $this->foreignRate = $value;
    }

    /**
     * Sets the gdnNumber property value. The gdnNumber property
     * @param int|null $value Value to set for the gdnNumber property.
    */
    public function setGdnNumber(?int $value): void {
        $this->gdnNumber = $value;
    }

    /**
     * Sets the globalDeptNumber property value. The globalDeptNumber property
     * @param int|null $value Value to set for the globalDeptNumber property.
    */
    public function setGlobalDeptNumber(?int $value): void {
        $this->globalDeptNumber = $value;
    }

    /**
     * Sets the globalDetails property value. The globalDetails property
     * @param string|null $value Value to set for the globalDetails property.
    */
    public function setGlobalDetails(?string $value): void {
        $this->globalDetails = $value;
    }

    /**
     * Sets the globalNomCode property value. The globalNomCode property
     * @param string|null $value Value to set for the globalNomCode property.
    */
    public function setGlobalNomCode(?string $value): void {
        $this->globalNomCode = $value;
    }

    /**
     * Sets the globalTaxCode property value. The globalTaxCode property
     * @param SalesInvoiceAttributesEdit_globalTaxCode|null $value Value to set for the globalTaxCode property.
    */
    public function setGlobalTaxCode(?SalesInvoiceAttributesEdit_globalTaxCode $value): void {
        $this->globalTaxCode = $value;
    }

    /**
     * Sets the invoiceDate property value. The invoiceDate property
     * @param DateTime|null $value Value to set for the invoiceDate property.
    */
    public function setInvoiceDate(?DateTime $value): void {
        $this->invoiceDate = $value;
    }

    /**
     * Sets the invoiceTypeCode property value. The invoiceTypeCode property
     * @param int|null $value Value to set for the invoiceTypeCode property.
    */
    public function setInvoiceTypeCode(?int $value): void {
        $this->invoiceTypeCode = $value;
    }

    /**
     * Sets the itemsNet property value. The itemsNet property
     * @param float|null $value Value to set for the itemsNet property.
    */
    public function setItemsNet(?float $value): void {
        $this->itemsNet = $value;
    }

    /**
     * Sets the itemsTax property value. The itemsTax property
     * @param float|null $value Value to set for the itemsTax property.
    */
    public function setItemsTax(?float $value): void {
        $this->itemsTax = $value;
    }

    /**
     * Sets the name property value. The name property
     * @param string|null $value Value to set for the name property.
    */
    public function setName(?string $value): void {
        $this->name = $value;
    }

    /**
     * Sets the notes1 property value. The notes1 property
     * @param string|null $value Value to set for the notes1 property.
    */
    public function setNotes1(?string $value): void {
        $this->notes1 = $value;
    }

    /**
     * Sets the notes2 property value. The notes2 property
     * @param string|null $value Value to set for the notes2 property.
    */
    public function setNotes2(?string $value): void {
        $this->notes2 = $value;
    }

    /**
     * Sets the notes3 property value. The notes3 property
     * @param string|null $value Value to set for the notes3 property.
    */
    public function setNotes3(?string $value): void {
        $this->notes3 = $value;
    }

    /**
     * Sets the orderNumber property value. The orderNumber property
     * @param string|null $value Value to set for the orderNumber property.
    */
    public function setOrderNumber(?string $value): void {
        $this->orderNumber = $value;
    }

    /**
     * Sets the paymentDueDate property value. The paymentDueDate property
     * @param DateTime|null $value Value to set for the paymentDueDate property.
    */
    public function setPaymentDueDate(?DateTime $value): void {
        $this->paymentDueDate = $value;
    }

    /**
     * Sets the paymentRef property value. The paymentRef property
     * @param string|null $value Value to set for the paymentRef property.
    */
    public function setPaymentRef(?string $value): void {
        $this->paymentRef = $value;
    }

    /**
     * Sets the paymentType property value. The paymentType property
     * @param int|null $value Value to set for the paymentType property.
    */
    public function setPaymentType(?int $value): void {
        $this->paymentType = $value;
    }

    /**
     * Sets the printedCode property value. The printedCode property
     * @param int|null $value Value to set for the printedCode property.
    */
    public function setPrintedCode(?int $value): void {
        $this->printedCode = $value;
    }

    /**
     * Sets the quoteStatus property value. The quoteStatus property
     * @param SalesInvoiceAttributesEdit_quoteStatus|null $value Value to set for the quoteStatus property.
    */
    public function setQuoteStatus(?SalesInvoiceAttributesEdit_quoteStatus $value): void {
        $this->quoteStatus = $value;
    }

    /**
     * Sets the settlementDiscRate property value. The settlementDiscRate property
     * @param float|null $value Value to set for the settlementDiscRate property.
    */
    public function setSettlementDiscRate(?float $value): void {
        $this->settlementDiscRate = $value;
    }

    /**
     * Sets the settlementDueDays property value. The settlementDueDays property
     * @param int|null $value Value to set for the settlementDueDays property.
    */
    public function setSettlementDueDays(?int $value): void {
        $this->settlementDueDays = $value;
    }

    /**
     * Sets the takenBy property value. The takenBy property
     * @param string|null $value Value to set for the takenBy property.
    */
    public function setTakenBy(?string $value): void {
        $this->takenBy = $value;
    }

    /**
     * Sets the updateLedgers property value. The updateLedgers property
     * @param bool|null $value Value to set for the updateLedgers property.
    */
    public function setUpdateLedgers(?bool $value): void {
        $this->updateLedgers = $value;
    }

}
