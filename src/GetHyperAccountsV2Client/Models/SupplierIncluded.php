<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class SupplierIncluded implements Parsable
{
    /**
     * @var GoodsReceivedNoteCollection|null $goodsReceivedNotes The goodsReceivedNotes property
    */
    private ?GoodsReceivedNoteCollection $goodsReceivedNotes = null;

    /**
     * @var PurchaseOrderCollection|null $purchaseOrders The purchaseOrders property
    */
    private ?PurchaseOrderCollection $purchaseOrders = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return SupplierIncluded
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): SupplierIncluded {
        return new SupplierIncluded();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'goodsReceivedNotes' => fn(ParseNode $n) => $o->setGoodsReceivedNotes($n->getObjectValue([GoodsReceivedNoteCollection::class, 'createFromDiscriminatorValue'])),
            'purchaseOrders' => fn(ParseNode $n) => $o->setPurchaseOrders($n->getObjectValue([PurchaseOrderCollection::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the goodsReceivedNotes property value. The goodsReceivedNotes property
     * @return GoodsReceivedNoteCollection|null
    */
    public function getGoodsReceivedNotes(): ?GoodsReceivedNoteCollection {
        return $this->goodsReceivedNotes;
    }

    /**
     * Gets the purchaseOrders property value. The purchaseOrders property
     * @return PurchaseOrderCollection|null
    */
    public function getPurchaseOrders(): ?PurchaseOrderCollection {
        return $this->purchaseOrders;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('goodsReceivedNotes', $this->getGoodsReceivedNotes());
        $writer->writeObjectValue('purchaseOrders', $this->getPurchaseOrders());
    }

    /**
     * Sets the goodsReceivedNotes property value. The goodsReceivedNotes property
     * @param GoodsReceivedNoteCollection|null $value Value to set for the goodsReceivedNotes property.
    */
    public function setGoodsReceivedNotes(?GoodsReceivedNoteCollection $value): void {
        $this->goodsReceivedNotes = $value;
    }

    /**
     * Sets the purchaseOrders property value. The purchaseOrders property
     * @param PurchaseOrderCollection|null $value Value to set for the purchaseOrders property.
    */
    public function setPurchaseOrders(?PurchaseOrderCollection $value): void {
        $this->purchaseOrders = $value;
    }

}
