<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class ProjectTranIncluded implements Parsable
{
    /**
     * @var ProjectGetDto|null $project The project property
    */
    private ?ProjectGetDto $project = null;

    /**
     * @var ProjectCostCodeGetDto|null $projectCostCode The projectCostCode property
    */
    private ?ProjectCostCodeGetDto $projectCostCode = null;

    /**
     * @var ProjectOnlyTransactionGetDto|null $projectOnlyTransaction The projectOnlyTransaction property
    */
    private ?ProjectOnlyTransactionGetDto $projectOnlyTransaction = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return ProjectTranIncluded
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): ProjectTranIncluded {
        return new ProjectTranIncluded();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'project' => fn(ParseNode $n) => $o->setProject($n->getObjectValue([ProjectGetDto::class, 'createFromDiscriminatorValue'])),
            'projectCostCode' => fn(ParseNode $n) => $o->setProjectCostCode($n->getObjectValue([ProjectCostCodeGetDto::class, 'createFromDiscriminatorValue'])),
            'projectOnlyTransaction' => fn(ParseNode $n) => $o->setProjectOnlyTransaction($n->getObjectValue([ProjectOnlyTransactionGetDto::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the project property value. The project property
     * @return ProjectGetDto|null
    */
    public function getProject(): ?ProjectGetDto {
        return $this->project;
    }

    /**
     * Gets the projectCostCode property value. The projectCostCode property
     * @return ProjectCostCodeGetDto|null
    */
    public function getProjectCostCode(): ?ProjectCostCodeGetDto {
        return $this->projectCostCode;
    }

    /**
     * Gets the projectOnlyTransaction property value. The projectOnlyTransaction property
     * @return ProjectOnlyTransactionGetDto|null
    */
    public function getProjectOnlyTransaction(): ?ProjectOnlyTransactionGetDto {
        return $this->projectOnlyTransaction;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('project', $this->getProject());
        $writer->writeObjectValue('projectCostCode', $this->getProjectCostCode());
        $writer->writeObjectValue('projectOnlyTransaction', $this->getProjectOnlyTransaction());
    }

    /**
     * Sets the project property value. The project property
     * @param ProjectGetDto|null $value Value to set for the project property.
    */
    public function setProject(?ProjectGetDto $value): void {
        $this->project = $value;
    }

    /**
     * Sets the projectCostCode property value. The projectCostCode property
     * @param ProjectCostCodeGetDto|null $value Value to set for the projectCostCode property.
    */
    public function setProjectCostCode(?ProjectCostCodeGetDto $value): void {
        $this->projectCostCode = $value;
    }

    /**
     * Sets the projectOnlyTransaction property value. The projectOnlyTransaction property
     * @param ProjectOnlyTransactionGetDto|null $value Value to set for the projectOnlyTransaction property.
    */
    public function setProjectOnlyTransaction(?ProjectOnlyTransactionGetDto $value): void {
        $this->projectOnlyTransaction = $value;
    }

}
