<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use DateTime;
use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class CompanyAttributesRead implements Parsable
{
    /**
     * @var string|null $address1 The address1 property
    */
    private ?string $address1 = null;

    /**
     * @var string|null $address2 The address2 property
    */
    private ?string $address2 = null;

    /**
     * @var string|null $address3 The address3 property
    */
    private ?string $address3 = null;

    /**
     * @var string|null $address4 The address4 property
    */
    private ?string $address4 = null;

    /**
     * @var string|null $address5 The address5 property
    */
    private ?string $address5 = null;

    /**
     * @var string|null $addressLabel1 The addressLabel1 property
    */
    private ?string $addressLabel1 = null;

    /**
     * @var string|null $addressLabel2 The addressLabel2 property
    */
    private ?string $addressLabel2 = null;

    /**
     * @var string|null $addressLabel3 The addressLabel3 property
    */
    private ?string $addressLabel3 = null;

    /**
     * @var string|null $addressLabel4 The addressLabel4 property
    */
    private ?string $addressLabel4 = null;

    /**
     * @var string|null $addressLabel5 The addressLabel5 property
    */
    private ?string $addressLabel5 = null;

    /**
     * @var int|null $allowNegativeStock The allowNegativeStock property
    */
    private ?int $allowNegativeStock = null;

    /**
     * @var int|null $alwaysCreateBatchEpayment The alwaysCreateBatchEpayment property
    */
    private ?int $alwaysCreateBatchEpayment = null;

    /**
     * @var int|null $budgetingMethod The budgetingMethod property
    */
    private ?int $budgetingMethod = null;

    /**
     * @var string|null $cAddress1 The cAddress1 property
    */
    private ?string $cAddress1 = null;

    /**
     * @var string|null $cAddress2 The cAddress2 property
    */
    private ?string $cAddress2 = null;

    /**
     * @var string|null $cAddress3 The cAddress3 property
    */
    private ?string $cAddress3 = null;

    /**
     * @var string|null $cAddress4 The cAddress4 property
    */
    private ?string $cAddress4 = null;

    /**
     * @var string|null $cAddress5 The cAddress5 property
    */
    private ?string $cAddress5 = null;

    /**
     * @var string|null $cashRegisterBank The cashRegisterBank property
    */
    private ?string $cashRegisterBank = null;

    /**
     * @var string|null $cashRegisterDiscrepenciesNom The cashRegisterDiscrepenciesNom property
    */
    private ?string $cashRegisterDiscrepenciesNom = null;

    /**
     * @var string|null $cashRegisterSalesNom The cashRegisterSalesNom property
    */
    private ?string $cashRegisterSalesNom = null;

    /**
     * @var int|null $cashRegisterTaxcode The cashRegisterTaxcode property
    */
    private ?int $cashRegisterTaxcode = null;

    /**
     * @var int|null $cashRegisterTaxInclusiveFlag The cashRegisterTaxInclusiveFlag property
    */
    private ?int $cashRegisterTaxInclusiveFlag = null;

    /**
     * @var string|null $ccLetter1 The ccLetter1 property
    */
    private ?string $ccLetter1 = null;

    /**
     * @var string|null $ccLetter2 The ccLetter2 property
    */
    private ?string $ccLetter2 = null;

    /**
     * @var string|null $ccLetter3 The ccLetter3 property
    */
    private ?string $ccLetter3 = null;

    /**
     * @var int|null $chargeDays The chargeDays property
    */
    private ?int $chargeDays = null;

    /**
     * @var int|null $charityState The charityState property
    */
    private ?int $charityState = null;

    /**
     * @var string|null $charitytaxref The charitytaxref property
    */
    private ?string $charitytaxref = null;

    /**
     * @var DateTime|null $clearAuditTrailDate The clearAuditTrailDate property
    */
    private ?DateTime $clearAuditTrailDate = null;

    /**
     * @var DateTime|null $closedLedgerCheckDate The closedLedgerCheckDate property
    */
    private ?DateTime $closedLedgerCheckDate = null;

    /**
     * @var int|null $closedLedgerDateEnabled The closedLedgerDateEnabled property
    */
    private ?int $closedLedgerDateEnabled = null;

    /**
     * @var string|null $countryCode The countryCode property
    */
    private ?string $countryCode = null;

    /**
     * @var string|null $countryCodeCombined The countryCodeCombined property
    */
    private ?string $countryCodeCombined = null;

    /**
     * @var string|null $countryCodeConverted The countryCodeConverted property
    */
    private ?string $countryCodeConverted = null;

    /**
     * @var string|null $creditRef The creditRef property
    */
    private ?string $creditRef = null;

    /**
     * @var int|null $customerPaymentDueImmediately The customerPaymentDueImmediately property
    */
    private ?int $customerPaymentDueImmediately = null;

    /**
     * @var int|null $dataManagementDisabled The dataManagementDisabled property
    */
    private ?int $dataManagementDisabled = null;

    /**
     * @var int|null $defaultBudgetingChart The defaultBudgetingChart property
    */
    private ?int $defaultBudgetingChart = null;

    /**
     * @var int|null $defaultChart The defaultChart property
    */
    private ?int $defaultChart = null;

    /**
     * @var int|null $defaultCreditBureau The defaultCreditBureau property
    */
    private ?int $defaultCreditBureau = null;

    /**
     * @var string|null $delAddress1 The delAddress1 property
    */
    private ?string $delAddress1 = null;

    /**
     * @var string|null $delAddress2 The delAddress2 property
    */
    private ?string $delAddress2 = null;

    /**
     * @var string|null $delAddress3 The delAddress3 property
    */
    private ?string $delAddress3 = null;

    /**
     * @var string|null $delAddress4 The delAddress4 property
    */
    private ?string $delAddress4 = null;

    /**
     * @var string|null $delAddress5 The delAddress5 property
    */
    private ?string $delAddress5 = null;

    /**
     * @var string|null $delEMail The delEMail property
    */
    private ?string $delEMail = null;

    /**
     * @var string|null $delFax The delFax property
    */
    private ?string $delFax = null;

    /**
     * @var string|null $delName The delName property
    */
    private ?string $delName = null;

    /**
     * @var string|null $delTelephone The delTelephone property
    */
    private ?string $delTelephone = null;

    /**
     * @var string|null $delWebAddress The delWebAddress property
    */
    private ?string $delWebAddress = null;

    /**
     * @var string|null $departmentsLabel The departmentsLabel property
    */
    private ?string $departmentsLabel = null;

    /**
     * @var int|null $directDebitsEnabled The directDebitsEnabled property
    */
    private ?int $directDebitsEnabled = null;

    /**
     * @var DateTime|null $directDebitsLastSyncedDate The directDebitsLastSyncedDate property
    */
    private ?DateTime $directDebitsLastSyncedDate = null;

    /**
     * @var string|null $dunsNumber The dunsNumber property
    */
    private ?string $dunsNumber = null;

    /**
     * @var int|null $ebankingState The ebankingState property
    */
    private ?int $ebankingState = null;

    /**
     * @var string|null $eMail The eMail property
    */
    private ?string $eMail = null;

    /**
     * @var string|null $endOfReport The endOfReport property
    */
    private ?string $endOfReport = null;

    /**
     * @var string|null $eoriNumber The eoriNumber property
    */
    private ?string $eoriNumber = null;

    /**
     * @var string|null $fax The fax property
    */
    private ?string $fax = null;

    /**
     * @var string|null $feesNominal The feesNominal property
    */
    private ?string $feesNominal = null;

    /**
     * @var int|null $financialYear The financialYear property
    */
    private ?int $financialYear = null;

    /**
     * @var string|null $fixedassetsLabel The fixedassetsLabel property
    */
    private ?string $fixedassetsLabel = null;

    /**
     * @var float|null $flatRateVatPercent The flatRateVatPercent property
    */
    private ?float $flatRateVatPercent = null;

    /**
     * @var int|null $hmrcPayeeEnabled The hmrcPayeeEnabled property
    */
    private ?int $hmrcPayeeEnabled = null;

    /**
     * @var int|null $hmrcPayeeStatus The hmrcPayeeStatus property
    */
    private ?int $hmrcPayeeStatus = null;

    /**
     * @var string|null $hmrcPayeeStatusDescription The hmrcPayeeStatusDescription property
    */
    private ?string $hmrcPayeeStatusDescription = null;

    /**
     * @var string|null $holdingAccount The holdingAccount property
    */
    private ?string $holdingAccount = null;

    /**
     * @var int|null $invoiceDiscUnitPrice The invoiceDiscUnitPrice property
    */
    private ?int $invoiceDiscUnitPrice = null;

    /**
     * @var string|null $invoicePaymentsState The invoicePaymentsState property
    */
    private ?string $invoicePaymentsState = null;

    /**
     * @var string|null $lAddress1 The lAddress1 property
    */
    private ?string $lAddress1 = null;

    /**
     * @var string|null $lAddress2 The lAddress2 property
    */
    private ?string $lAddress2 = null;

    /**
     * @var string|null $lAddress3 The lAddress3 property
    */
    private ?string $lAddress3 = null;

    /**
     * @var string|null $lAddress4 The lAddress4 property
    */
    private ?string $lAddress4 = null;

    /**
     * @var string|null $lAddress5 The lAddress5 property
    */
    private ?string $lAddress5 = null;

    /**
     * @var DateTime|null $lastBackupDate The lastBackupDate property
    */
    private ?DateTime $lastBackupDate = null;

    /**
     * @var int|null $lastCheckDataComments The lastCheckDataComments property
    */
    private ?int $lastCheckDataComments = null;

    /**
     * @var DateTime|null $lastCheckDataDate The lastCheckDataDate property
    */
    private ?DateTime $lastCheckDataDate = null;

    /**
     * @var int|null $lastCheckDataErrors The lastCheckDataErrors property
    */
    private ?int $lastCheckDataErrors = null;

    /**
     * @var int|null $lastCheckDataWarnings The lastCheckDataWarnings property
    */
    private ?int $lastCheckDataWarnings = null;

    /**
     * @var DateTime|null $lastMonthEnd The lastMonthEnd property
    */
    private ?DateTime $lastMonthEnd = null;

    /**
     * @var DateTime|null $lastRestoreDate The lastRestoreDate property
    */
    private ?DateTime $lastRestoreDate = null;

    /**
     * @var int|null $lastTransactionNumber The lastTransactionNumber property
    */
    private ?int $lastTransactionNumber = null;

    /**
     * @var string|null $lName The lName property
    */
    private ?string $lName = null;

    /**
     * @var int|null $mtdState The mtdState property
    */
    private ?int $mtdState = null;

    /**
     * @var string|null $name The name property
    */
    private ?string $name = null;

    /**
     * @var int|null $noDiscountPriceList The noDiscountPriceList property
    */
    private ?int $noDiscountPriceList = null;

    /**
     * @var string|null $nomAccruals The nomAccruals property
    */
    private ?string $nomAccruals = null;

    /**
     * @var string|null $nomBadDebts The nomBadDebts property
    */
    private ?string $nomBadDebts = null;

    /**
     * @var string|null $nomCreditCharges The nomCreditCharges property
    */
    private ?string $nomCreditCharges = null;

    /**
     * @var string|null $nomCreditors The nomCreditors property
    */
    private ?string $nomCreditors = null;

    /**
     * @var string|null $nomDebtors The nomDebtors property
    */
    private ?string $nomDebtors = null;

    /**
     * @var string|null $nomDefaultBank The nomDefaultBank property
    */
    private ?string $nomDefaultBank = null;

    /**
     * @var string|null $nomDefaultSales The nomDefaultSales property
    */
    private ?string $nomDefaultSales = null;

    /**
     * @var string|null $nomDefrevaluation The nomDefrevaluation property
    */
    private ?string $nomDefrevaluation = null;

    /**
     * @var string|null $nomDiscountPurchase The nomDiscountPurchase property
    */
    private ?string $nomDiscountPurchase = null;

    /**
     * @var string|null $nomDiscountSales The nomDiscountSales property
    */
    private ?string $nomDiscountSales = null;

    /**
     * @var string|null $nominatedBank The nominatedBank property
    */
    private ?string $nominatedBank = null;

    /**
     * @var string|null $nomIoss The nomIoss property
    */
    private ?string $nomIoss = null;

    /**
     * @var string|null $nomManualadjs The nomManualadjs property
    */
    private ?string $nomManualadjs = null;

    /**
     * @var string|null $nomMispostings The nomMispostings property
    */
    private ?string $nomMispostings = null;

    /**
     * @var string|null $nomNonunionoss The nomNonunionoss property
    */
    private ?string $nomNonunionoss = null;

    /**
     * @var string|null $nomPrepayments The nomPrepayments property
    */
    private ?string $nomPrepayments = null;

    /**
     * @var string|null $nomRetainedEarnings The nomRetainedEarnings property
    */
    private ?string $nomRetainedEarnings = null;

    /**
     * @var string|null $nomSuspense The nomSuspense property
    */
    private ?string $nomSuspense = null;

    /**
     * @var string|null $nomTaxPurchase The nomTaxPurchase property
    */
    private ?string $nomTaxPurchase = null;

    /**
     * @var string|null $nomTaxSales The nomTaxSales property
    */
    private ?string $nomTaxSales = null;

    /**
     * @var string|null $nomUnionoss The nomUnionoss property
    */
    private ?string $nomUnionoss = null;

    /**
     * @var string|null $nomVatliability The nomVatliability property
    */
    private ?string $nomVatliability = null;

    /**
     * @var int|null $nonVatTaxCode The nonVatTaxCode property
    */
    private ?int $nonVatTaxCode = null;

    /**
     * @var int|null $numberOfTransactions The numberOfTransactions property
    */
    private ?int $numberOfTransactions = null;

    /**
     * @var int|null $office365State The office365State property
    */
    private ?int $office365State = null;

    /**
     * @var int|null $ossiossEnabled The ossiossEnabled property
    */
    private ?int $ossiossEnabled = null;

    /**
     * @var int|null $paymentDueFromPurchase The paymentDueFromPurchase property
    */
    private ?int $paymentDueFromPurchase = null;

    /**
     * @var string|null $paymentDueFromPurchaseText The paymentDueFromPurchaseText property
    */
    private ?string $paymentDueFromPurchaseText = null;

    /**
     * @var int|null $paymentDueFromSales The paymentDueFromSales property
    */
    private ?int $paymentDueFromSales = null;

    /**
     * @var string|null $paymentDueFromSalesText The paymentDueFromSalesText property
    */
    private ?string $paymentDueFromSalesText = null;

    /**
     * @var int|null $paymentDuePurchase The paymentDuePurchase property
    */
    private ?int $paymentDuePurchase = null;

    /**
     * @var int|null $paymentDueSales The paymentDueSales property
    */
    private ?int $paymentDueSales = null;

    /**
     * @var int|null $popCarriageDept The popCarriageDept property
    */
    private ?int $popCarriageDept = null;

    /**
     * @var float|null $popCarriageNet The popCarriageNet property
    */
    private ?float $popCarriageNet = null;

    /**
     * @var string|null $popCarriageNomCode The popCarriageNomCode property
    */
    private ?string $popCarriageNomCode = null;

    /**
     * @var int|null $popCarriageTaxCode The popCarriageTaxCode property
    */
    private ?int $popCarriageTaxCode = null;

    /**
     * @var int|null $popGlobalDept The popGlobalDept property
    */
    private ?int $popGlobalDept = null;

    /**
     * @var string|null $popGlobalDetails The popGlobalDetails property
    */
    private ?string $popGlobalDetails = null;

    /**
     * @var string|null $popGlobalNomCode The popGlobalNomCode property
    */
    private ?string $popGlobalNomCode = null;

    /**
     * @var int|null $popGlobalTaxCode The popGlobalTaxCode property
    */
    private ?int $popGlobalTaxCode = null;

    /**
     * @var int|null $programBugfixVersion The programBugfixVersion property
    */
    private ?int $programBugfixVersion = null;

    /**
     * @var int|null $programBuildVersion The programBuildVersion property
    */
    private ?int $programBuildVersion = null;

    /**
     * @var int|null $programMajorVersion The programMajorVersion property
    */
    private ?int $programMajorVersion = null;

    /**
     * @var int|null $programMinorVersion The programMinorVersion property
    */
    private ?int $programMinorVersion = null;

    /**
     * @var string|null $programType The programType property
    */
    private ?string $programType = null;

    /**
     * @var int|null $protxEnabled The protxEnabled property
    */
    private ?int $protxEnabled = null;

    /**
     * @var string|null $protxLastSyncDate The protxLastSyncDate property
    */
    private ?string $protxLastSyncDate = null;

    /**
     * @var string|null $protxPassword The protxPassword property
    */
    private ?string $protxPassword = null;

    /**
     * @var string|null $protxUserid The protxUserid property
    */
    private ?string $protxUserid = null;

    /**
     * @var string|null $protxVendor The protxVendor property
    */
    private ?string $protxVendor = null;

    /**
     * @var int|null $purchaseAgeByMonth The purchaseAgeByMonth property
    */
    private ?int $purchaseAgeByMonth = null;

    /**
     * @var int|null $purchaseAgedPeriod1 The purchaseAgedPeriod1 property
    */
    private ?int $purchaseAgedPeriod1 = null;

    /**
     * @var int|null $purchaseAgedPeriod2 The purchaseAgedPeriod2 property
    */
    private ?int $purchaseAgedPeriod2 = null;

    /**
     * @var int|null $purchaseAgedPeriod3 The purchaseAgedPeriod3 property
    */
    private ?int $purchaseAgedPeriod3 = null;

    /**
     * @var int|null $purchaseAgedPeriod4 The purchaseAgedPeriod4 property
    */
    private ?int $purchaseAgedPeriod4 = null;

    /**
     * @var int|null $purchaseDefaultIncoterms The purchaseDefaultIncoterms property
    */
    private ?int $purchaseDefaultIncoterms = null;

    /**
     * @var string|null $purchaseDefaultIncotermsText The purchaseDefaultIncotermsText property
    */
    private ?string $purchaseDefaultIncotermsText = null;

    /**
     * @var string|null $purchaseLabel1 The purchaseLabel1 property
    */
    private ?string $purchaseLabel1 = null;

    /**
     * @var string|null $purchaseLabel2 The purchaseLabel2 property
    */
    private ?string $purchaseLabel2 = null;

    /**
     * @var string|null $purchaseLabel3 The purchaseLabel3 property
    */
    private ?string $purchaseLabel3 = null;

    /**
     * @var int|null $qtyDecimalPlace The qtyDecimalPlace property
    */
    private ?int $qtyDecimalPlace = null;

    /**
     * @var int|null $quotesToOrders The quotesToOrders property
    */
    private ?int $quotesToOrders = null;

    /**
     * @var DateTime|null $recordCreateDate The recordCreateDate property
    */
    private ?DateTime $recordCreateDate = null;

    /**
     * @var int|null $recordDeleted The recordDeleted property
    */
    private ?int $recordDeleted = null;

    /**
     * @var DateTime|null $recordModifyDate The recordModifyDate property
    */
    private ?DateTime $recordModifyDate = null;

    /**
     * @var float|null $salesAddDisc1 The salesAddDisc1 property
    */
    private ?float $salesAddDisc1 = null;

    /**
     * @var float|null $salesAddDisc10 The salesAddDisc10 property
    */
    private ?float $salesAddDisc10 = null;

    /**
     * @var float|null $salesAddDisc2 The salesAddDisc2 property
    */
    private ?float $salesAddDisc2 = null;

    /**
     * @var float|null $salesAddDisc3 The salesAddDisc3 property
    */
    private ?float $salesAddDisc3 = null;

    /**
     * @var float|null $salesAddDisc4 The salesAddDisc4 property
    */
    private ?float $salesAddDisc4 = null;

    /**
     * @var float|null $salesAddDisc5 The salesAddDisc5 property
    */
    private ?float $salesAddDisc5 = null;

    /**
     * @var float|null $salesAddDisc6 The salesAddDisc6 property
    */
    private ?float $salesAddDisc6 = null;

    /**
     * @var float|null $salesAddDisc7 The salesAddDisc7 property
    */
    private ?float $salesAddDisc7 = null;

    /**
     * @var float|null $salesAddDisc8 The salesAddDisc8 property
    */
    private ?float $salesAddDisc8 = null;

    /**
     * @var float|null $salesAddDisc9 The salesAddDisc9 property
    */
    private ?float $salesAddDisc9 = null;

    /**
     * @var int|null $salesAgeByMonth The salesAgeByMonth property
    */
    private ?int $salesAgeByMonth = null;

    /**
     * @var int|null $salesAgedPeriod1 The salesAgedPeriod1 property
    */
    private ?int $salesAgedPeriod1 = null;

    /**
     * @var int|null $salesAgedPeriod2 The salesAgedPeriod2 property
    */
    private ?int $salesAgedPeriod2 = null;

    /**
     * @var int|null $salesAgedPeriod3 The salesAgedPeriod3 property
    */
    private ?int $salesAgedPeriod3 = null;

    /**
     * @var int|null $salesAgedPeriod4 The salesAgedPeriod4 property
    */
    private ?int $salesAgedPeriod4 = null;

    /**
     * @var float|null $salesAmountSold1 The salesAmountSold1 property
    */
    private ?float $salesAmountSold1 = null;

    /**
     * @var float|null $salesAmountSold10 The salesAmountSold10 property
    */
    private ?float $salesAmountSold10 = null;

    /**
     * @var float|null $salesAmountSold2 The salesAmountSold2 property
    */
    private ?float $salesAmountSold2 = null;

    /**
     * @var float|null $salesAmountSold3 The salesAmountSold3 property
    */
    private ?float $salesAmountSold3 = null;

    /**
     * @var float|null $salesAmountSold4 The salesAmountSold4 property
    */
    private ?float $salesAmountSold4 = null;

    /**
     * @var float|null $salesAmountSold5 The salesAmountSold5 property
    */
    private ?float $salesAmountSold5 = null;

    /**
     * @var float|null $salesAmountSold6 The salesAmountSold6 property
    */
    private ?float $salesAmountSold6 = null;

    /**
     * @var float|null $salesAmountSold7 The salesAmountSold7 property
    */
    private ?float $salesAmountSold7 = null;

    /**
     * @var float|null $salesAmountSold8 The salesAmountSold8 property
    */
    private ?float $salesAmountSold8 = null;

    /**
     * @var float|null $salesAmountSold9 The salesAmountSold9 property
    */
    private ?float $salesAmountSold9 = null;

    /**
     * @var int|null $salesDefaultIncoterms The salesDefaultIncoterms property
    */
    private ?int $salesDefaultIncoterms = null;

    /**
     * @var string|null $salesDefaultIncotermsText The salesDefaultIncotermsText property
    */
    private ?string $salesDefaultIncotermsText = null;

    /**
     * @var string|null $salesLabel1 The salesLabel1 property
    */
    private ?string $salesLabel1 = null;

    /**
     * @var string|null $salesLabel2 The salesLabel2 property
    */
    private ?string $salesLabel2 = null;

    /**
     * @var string|null $salesLabel3 The salesLabel3 property
    */
    private ?string $salesLabel3 = null;

    /**
     * @var int|null $sipsState The sipsState property
    */
    private ?int $sipsState = null;

    /**
     * @var int|null $sopCarriageDept The sopCarriageDept property
    */
    private ?int $sopCarriageDept = null;

    /**
     * @var float|null $sopCarriageNet The sopCarriageNet property
    */
    private ?float $sopCarriageNet = null;

    /**
     * @var string|null $sopCarriageNomCode The sopCarriageNomCode property
    */
    private ?string $sopCarriageNomCode = null;

    /**
     * @var int|null $sopCarriageTaxCode The sopCarriageTaxCode property
    */
    private ?int $sopCarriageTaxCode = null;

    /**
     * @var int|null $sopGlobalDept The sopGlobalDept property
    */
    private ?int $sopGlobalDept = null;

    /**
     * @var string|null $sopGlobalDetails The sopGlobalDetails property
    */
    private ?string $sopGlobalDetails = null;

    /**
     * @var string|null $sopGlobalNomCode The sopGlobalNomCode property
    */
    private ?string $sopGlobalNomCode = null;

    /**
     * @var int|null $sopGlobalTaxCode The sopGlobalTaxCode property
    */
    private ?int $sopGlobalTaxCode = null;

    /**
     * @var int|null $startMonth The startMonth property
    */
    private ?int $startMonth = null;

    /**
     * @var string|null $stockcategoryLabel The stockcategoryLabel property
    */
    private ?string $stockcategoryLabel = null;

    /**
     * @var string|null $stockDiscountA The stockDiscountA property
    */
    private ?string $stockDiscountA = null;

    /**
     * @var string|null $stockDiscountB The stockDiscountB property
    */
    private ?string $stockDiscountB = null;

    /**
     * @var string|null $stockDiscountC The stockDiscountC property
    */
    private ?string $stockDiscountC = null;

    /**
     * @var string|null $stockDiscountD The stockDiscountD property
    */
    private ?string $stockDiscountD = null;

    /**
     * @var string|null $stockDiscountE The stockDiscountE property
    */
    private ?string $stockDiscountE = null;

    /**
     * @var string|null $stockLabel1 The stockLabel1 property
    */
    private ?string $stockLabel1 = null;

    /**
     * @var string|null $stockLabel2 The stockLabel2 property
    */
    private ?string $stockLabel2 = null;

    /**
     * @var string|null $stockLabel3 The stockLabel3 property
    */
    private ?string $stockLabel3 = null;

    /**
     * @var int|null $supplierPaymentDueImmediately The supplierPaymentDueImmediately property
    */
    private ?int $supplierPaymentDueImmediately = null;

    /**
     * @var string|null $telephone The telephone property
    */
    private ?string $telephone = null;

    /**
     * @var int|null $unitDecimalPlace The unitDecimalPlace property
    */
    private ?int $unitDecimalPlace = null;

    /**
     * @var int|null $vatCash The vatCash property
    */
    private ?int $vatCash = null;

    /**
     * @var string|null $vatRegNumber The vatRegNumber property
    */
    private ?string $vatRegNumber = null;

    /**
     * @var string|null $webAddress The webAddress property
    */
    private ?string $webAddress = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return CompanyAttributesRead
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): CompanyAttributesRead {
        return new CompanyAttributesRead();
    }

    /**
     * Gets the address1 property value. The address1 property
     * @return string|null
    */
    public function getAddress1(): ?string {
        return $this->address1;
    }

    /**
     * Gets the address2 property value. The address2 property
     * @return string|null
    */
    public function getAddress2(): ?string {
        return $this->address2;
    }

    /**
     * Gets the address3 property value. The address3 property
     * @return string|null
    */
    public function getAddress3(): ?string {
        return $this->address3;
    }

    /**
     * Gets the address4 property value. The address4 property
     * @return string|null
    */
    public function getAddress4(): ?string {
        return $this->address4;
    }

    /**
     * Gets the address5 property value. The address5 property
     * @return string|null
    */
    public function getAddress5(): ?string {
        return $this->address5;
    }

    /**
     * Gets the addressLabel1 property value. The addressLabel1 property
     * @return string|null
    */
    public function getAddressLabel1(): ?string {
        return $this->addressLabel1;
    }

    /**
     * Gets the addressLabel2 property value. The addressLabel2 property
     * @return string|null
    */
    public function getAddressLabel2(): ?string {
        return $this->addressLabel2;
    }

    /**
     * Gets the addressLabel3 property value. The addressLabel3 property
     * @return string|null
    */
    public function getAddressLabel3(): ?string {
        return $this->addressLabel3;
    }

    /**
     * Gets the addressLabel4 property value. The addressLabel4 property
     * @return string|null
    */
    public function getAddressLabel4(): ?string {
        return $this->addressLabel4;
    }

    /**
     * Gets the addressLabel5 property value. The addressLabel5 property
     * @return string|null
    */
    public function getAddressLabel5(): ?string {
        return $this->addressLabel5;
    }

    /**
     * Gets the allowNegativeStock property value. The allowNegativeStock property
     * @return int|null
    */
    public function getAllowNegativeStock(): ?int {
        return $this->allowNegativeStock;
    }

    /**
     * Gets the alwaysCreateBatchEpayment property value. The alwaysCreateBatchEpayment property
     * @return int|null
    */
    public function getAlwaysCreateBatchEpayment(): ?int {
        return $this->alwaysCreateBatchEpayment;
    }

    /**
     * Gets the budgetingMethod property value. The budgetingMethod property
     * @return int|null
    */
    public function getBudgetingMethod(): ?int {
        return $this->budgetingMethod;
    }

    /**
     * Gets the cAddress1 property value. The cAddress1 property
     * @return string|null
    */
    public function getCAddress1(): ?string {
        return $this->cAddress1;
    }

    /**
     * Gets the cAddress2 property value. The cAddress2 property
     * @return string|null
    */
    public function getCAddress2(): ?string {
        return $this->cAddress2;
    }

    /**
     * Gets the cAddress3 property value. The cAddress3 property
     * @return string|null
    */
    public function getCAddress3(): ?string {
        return $this->cAddress3;
    }

    /**
     * Gets the cAddress4 property value. The cAddress4 property
     * @return string|null
    */
    public function getCAddress4(): ?string {
        return $this->cAddress4;
    }

    /**
     * Gets the cAddress5 property value. The cAddress5 property
     * @return string|null
    */
    public function getCAddress5(): ?string {
        return $this->cAddress5;
    }

    /**
     * Gets the cashRegisterBank property value. The cashRegisterBank property
     * @return string|null
    */
    public function getCashRegisterBank(): ?string {
        return $this->cashRegisterBank;
    }

    /**
     * Gets the cashRegisterDiscrepenciesNom property value. The cashRegisterDiscrepenciesNom property
     * @return string|null
    */
    public function getCashRegisterDiscrepenciesNom(): ?string {
        return $this->cashRegisterDiscrepenciesNom;
    }

    /**
     * Gets the cashRegisterSalesNom property value. The cashRegisterSalesNom property
     * @return string|null
    */
    public function getCashRegisterSalesNom(): ?string {
        return $this->cashRegisterSalesNom;
    }

    /**
     * Gets the cashRegisterTaxcode property value. The cashRegisterTaxcode property
     * @return int|null
    */
    public function getCashRegisterTaxcode(): ?int {
        return $this->cashRegisterTaxcode;
    }

    /**
     * Gets the cashRegisterTaxInclusiveFlag property value. The cashRegisterTaxInclusiveFlag property
     * @return int|null
    */
    public function getCashRegisterTaxInclusiveFlag(): ?int {
        return $this->cashRegisterTaxInclusiveFlag;
    }

    /**
     * Gets the ccLetter1 property value. The ccLetter1 property
     * @return string|null
    */
    public function getCcLetter1(): ?string {
        return $this->ccLetter1;
    }

    /**
     * Gets the ccLetter2 property value. The ccLetter2 property
     * @return string|null
    */
    public function getCcLetter2(): ?string {
        return $this->ccLetter2;
    }

    /**
     * Gets the ccLetter3 property value. The ccLetter3 property
     * @return string|null
    */
    public function getCcLetter3(): ?string {
        return $this->ccLetter3;
    }

    /**
     * Gets the chargeDays property value. The chargeDays property
     * @return int|null
    */
    public function getChargeDays(): ?int {
        return $this->chargeDays;
    }

    /**
     * Gets the charityState property value. The charityState property
     * @return int|null
    */
    public function getCharityState(): ?int {
        return $this->charityState;
    }

    /**
     * Gets the charitytaxref property value. The charitytaxref property
     * @return string|null
    */
    public function getCharitytaxref(): ?string {
        return $this->charitytaxref;
    }

    /**
     * Gets the clearAuditTrailDate property value. The clearAuditTrailDate property
     * @return DateTime|null
    */
    public function getClearAuditTrailDate(): ?DateTime {
        return $this->clearAuditTrailDate;
    }

    /**
     * Gets the closedLedgerCheckDate property value. The closedLedgerCheckDate property
     * @return DateTime|null
    */
    public function getClosedLedgerCheckDate(): ?DateTime {
        return $this->closedLedgerCheckDate;
    }

    /**
     * Gets the closedLedgerDateEnabled property value. The closedLedgerDateEnabled property
     * @return int|null
    */
    public function getClosedLedgerDateEnabled(): ?int {
        return $this->closedLedgerDateEnabled;
    }

    /**
     * Gets the countryCode property value. The countryCode property
     * @return string|null
    */
    public function getCountryCode(): ?string {
        return $this->countryCode;
    }

    /**
     * Gets the countryCodeCombined property value. The countryCodeCombined property
     * @return string|null
    */
    public function getCountryCodeCombined(): ?string {
        return $this->countryCodeCombined;
    }

    /**
     * Gets the countryCodeConverted property value. The countryCodeConverted property
     * @return string|null
    */
    public function getCountryCodeConverted(): ?string {
        return $this->countryCodeConverted;
    }

    /**
     * Gets the creditRef property value. The creditRef property
     * @return string|null
    */
    public function getCreditRef(): ?string {
        return $this->creditRef;
    }

    /**
     * Gets the customerPaymentDueImmediately property value. The customerPaymentDueImmediately property
     * @return int|null
    */
    public function getCustomerPaymentDueImmediately(): ?int {
        return $this->customerPaymentDueImmediately;
    }

    /**
     * Gets the dataManagementDisabled property value. The dataManagementDisabled property
     * @return int|null
    */
    public function getDataManagementDisabled(): ?int {
        return $this->dataManagementDisabled;
    }

    /**
     * Gets the defaultBudgetingChart property value. The defaultBudgetingChart property
     * @return int|null
    */
    public function getDefaultBudgetingChart(): ?int {
        return $this->defaultBudgetingChart;
    }

    /**
     * Gets the defaultChart property value. The defaultChart property
     * @return int|null
    */
    public function getDefaultChart(): ?int {
        return $this->defaultChart;
    }

    /**
     * Gets the defaultCreditBureau property value. The defaultCreditBureau property
     * @return int|null
    */
    public function getDefaultCreditBureau(): ?int {
        return $this->defaultCreditBureau;
    }

    /**
     * Gets the delAddress1 property value. The delAddress1 property
     * @return string|null
    */
    public function getDelAddress1(): ?string {
        return $this->delAddress1;
    }

    /**
     * Gets the delAddress2 property value. The delAddress2 property
     * @return string|null
    */
    public function getDelAddress2(): ?string {
        return $this->delAddress2;
    }

    /**
     * Gets the delAddress3 property value. The delAddress3 property
     * @return string|null
    */
    public function getDelAddress3(): ?string {
        return $this->delAddress3;
    }

    /**
     * Gets the delAddress4 property value. The delAddress4 property
     * @return string|null
    */
    public function getDelAddress4(): ?string {
        return $this->delAddress4;
    }

    /**
     * Gets the delAddress5 property value. The delAddress5 property
     * @return string|null
    */
    public function getDelAddress5(): ?string {
        return $this->delAddress5;
    }

    /**
     * Gets the delEMail property value. The delEMail property
     * @return string|null
    */
    public function getDelEMail(): ?string {
        return $this->delEMail;
    }

    /**
     * Gets the delFax property value. The delFax property
     * @return string|null
    */
    public function getDelFax(): ?string {
        return $this->delFax;
    }

    /**
     * Gets the delName property value. The delName property
     * @return string|null
    */
    public function getDelName(): ?string {
        return $this->delName;
    }

    /**
     * Gets the delTelephone property value. The delTelephone property
     * @return string|null
    */
    public function getDelTelephone(): ?string {
        return $this->delTelephone;
    }

    /**
     * Gets the delWebAddress property value. The delWebAddress property
     * @return string|null
    */
    public function getDelWebAddress(): ?string {
        return $this->delWebAddress;
    }

    /**
     * Gets the departmentsLabel property value. The departmentsLabel property
     * @return string|null
    */
    public function getDepartmentsLabel(): ?string {
        return $this->departmentsLabel;
    }

    /**
     * Gets the directDebitsEnabled property value. The directDebitsEnabled property
     * @return int|null
    */
    public function getDirectDebitsEnabled(): ?int {
        return $this->directDebitsEnabled;
    }

    /**
     * Gets the directDebitsLastSyncedDate property value. The directDebitsLastSyncedDate property
     * @return DateTime|null
    */
    public function getDirectDebitsLastSyncedDate(): ?DateTime {
        return $this->directDebitsLastSyncedDate;
    }

    /**
     * Gets the dunsNumber property value. The dunsNumber property
     * @return string|null
    */
    public function getDunsNumber(): ?string {
        return $this->dunsNumber;
    }

    /**
     * Gets the ebankingState property value. The ebankingState property
     * @return int|null
    */
    public function getEbankingState(): ?int {
        return $this->ebankingState;
    }

    /**
     * Gets the eMail property value. The eMail property
     * @return string|null
    */
    public function getEMail(): ?string {
        return $this->eMail;
    }

    /**
     * Gets the endOfReport property value. The endOfReport property
     * @return string|null
    */
    public function getEndOfReport(): ?string {
        return $this->endOfReport;
    }

    /**
     * Gets the eoriNumber property value. The eoriNumber property
     * @return string|null
    */
    public function getEoriNumber(): ?string {
        return $this->eoriNumber;
    }

    /**
     * Gets the fax property value. The fax property
     * @return string|null
    */
    public function getFax(): ?string {
        return $this->fax;
    }

    /**
     * Gets the feesNominal property value. The feesNominal property
     * @return string|null
    */
    public function getFeesNominal(): ?string {
        return $this->feesNominal;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'address1' => fn(ParseNode $n) => $o->setAddress1($n->getStringValue()),
            'address2' => fn(ParseNode $n) => $o->setAddress2($n->getStringValue()),
            'address3' => fn(ParseNode $n) => $o->setAddress3($n->getStringValue()),
            'address4' => fn(ParseNode $n) => $o->setAddress4($n->getStringValue()),
            'address5' => fn(ParseNode $n) => $o->setAddress5($n->getStringValue()),
            'addressLabel1' => fn(ParseNode $n) => $o->setAddressLabel1($n->getStringValue()),
            'addressLabel2' => fn(ParseNode $n) => $o->setAddressLabel2($n->getStringValue()),
            'addressLabel3' => fn(ParseNode $n) => $o->setAddressLabel3($n->getStringValue()),
            'addressLabel4' => fn(ParseNode $n) => $o->setAddressLabel4($n->getStringValue()),
            'addressLabel5' => fn(ParseNode $n) => $o->setAddressLabel5($n->getStringValue()),
            'allowNegativeStock' => fn(ParseNode $n) => $o->setAllowNegativeStock($n->getIntegerValue()),
            'alwaysCreateBatchEpayment' => fn(ParseNode $n) => $o->setAlwaysCreateBatchEpayment($n->getIntegerValue()),
            'budgetingMethod' => fn(ParseNode $n) => $o->setBudgetingMethod($n->getIntegerValue()),
            'cAddress1' => fn(ParseNode $n) => $o->setCAddress1($n->getStringValue()),
            'cAddress2' => fn(ParseNode $n) => $o->setCAddress2($n->getStringValue()),
            'cAddress3' => fn(ParseNode $n) => $o->setCAddress3($n->getStringValue()),
            'cAddress4' => fn(ParseNode $n) => $o->setCAddress4($n->getStringValue()),
            'cAddress5' => fn(ParseNode $n) => $o->setCAddress5($n->getStringValue()),
            'cashRegisterBank' => fn(ParseNode $n) => $o->setCashRegisterBank($n->getStringValue()),
            'cashRegisterDiscrepenciesNom' => fn(ParseNode $n) => $o->setCashRegisterDiscrepenciesNom($n->getStringValue()),
            'cashRegisterSalesNom' => fn(ParseNode $n) => $o->setCashRegisterSalesNom($n->getStringValue()),
            'cashRegisterTaxcode' => fn(ParseNode $n) => $o->setCashRegisterTaxcode($n->getIntegerValue()),
            'cashRegisterTaxInclusiveFlag' => fn(ParseNode $n) => $o->setCashRegisterTaxInclusiveFlag($n->getIntegerValue()),
            'ccLetter1' => fn(ParseNode $n) => $o->setCcLetter1($n->getStringValue()),
            'ccLetter2' => fn(ParseNode $n) => $o->setCcLetter2($n->getStringValue()),
            'ccLetter3' => fn(ParseNode $n) => $o->setCcLetter3($n->getStringValue()),
            'chargeDays' => fn(ParseNode $n) => $o->setChargeDays($n->getIntegerValue()),
            'charityState' => fn(ParseNode $n) => $o->setCharityState($n->getIntegerValue()),
            'charitytaxref' => fn(ParseNode $n) => $o->setCharitytaxref($n->getStringValue()),
            'clearAuditTrailDate' => fn(ParseNode $n) => $o->setClearAuditTrailDate($n->getDateTimeValue()),
            'closedLedgerCheckDate' => fn(ParseNode $n) => $o->setClosedLedgerCheckDate($n->getDateTimeValue()),
            'closedLedgerDateEnabled' => fn(ParseNode $n) => $o->setClosedLedgerDateEnabled($n->getIntegerValue()),
            'countryCode' => fn(ParseNode $n) => $o->setCountryCode($n->getStringValue()),
            'countryCodeCombined' => fn(ParseNode $n) => $o->setCountryCodeCombined($n->getStringValue()),
            'countryCodeConverted' => fn(ParseNode $n) => $o->setCountryCodeConverted($n->getStringValue()),
            'creditRef' => fn(ParseNode $n) => $o->setCreditRef($n->getStringValue()),
            'customerPaymentDueImmediately' => fn(ParseNode $n) => $o->setCustomerPaymentDueImmediately($n->getIntegerValue()),
            'dataManagementDisabled' => fn(ParseNode $n) => $o->setDataManagementDisabled($n->getIntegerValue()),
            'defaultBudgetingChart' => fn(ParseNode $n) => $o->setDefaultBudgetingChart($n->getIntegerValue()),
            'defaultChart' => fn(ParseNode $n) => $o->setDefaultChart($n->getIntegerValue()),
            'defaultCreditBureau' => fn(ParseNode $n) => $o->setDefaultCreditBureau($n->getIntegerValue()),
            'delAddress1' => fn(ParseNode $n) => $o->setDelAddress1($n->getStringValue()),
            'delAddress2' => fn(ParseNode $n) => $o->setDelAddress2($n->getStringValue()),
            'delAddress3' => fn(ParseNode $n) => $o->setDelAddress3($n->getStringValue()),
            'delAddress4' => fn(ParseNode $n) => $o->setDelAddress4($n->getStringValue()),
            'delAddress5' => fn(ParseNode $n) => $o->setDelAddress5($n->getStringValue()),
            'delEMail' => fn(ParseNode $n) => $o->setDelEMail($n->getStringValue()),
            'delFax' => fn(ParseNode $n) => $o->setDelFax($n->getStringValue()),
            'delName' => fn(ParseNode $n) => $o->setDelName($n->getStringValue()),
            'delTelephone' => fn(ParseNode $n) => $o->setDelTelephone($n->getStringValue()),
            'delWebAddress' => fn(ParseNode $n) => $o->setDelWebAddress($n->getStringValue()),
            'departmentsLabel' => fn(ParseNode $n) => $o->setDepartmentsLabel($n->getStringValue()),
            'directDebitsEnabled' => fn(ParseNode $n) => $o->setDirectDebitsEnabled($n->getIntegerValue()),
            'directDebitsLastSyncedDate' => fn(ParseNode $n) => $o->setDirectDebitsLastSyncedDate($n->getDateTimeValue()),
            'dunsNumber' => fn(ParseNode $n) => $o->setDunsNumber($n->getStringValue()),
            'ebankingState' => fn(ParseNode $n) => $o->setEbankingState($n->getIntegerValue()),
            'eMail' => fn(ParseNode $n) => $o->setEMail($n->getStringValue()),
            'endOfReport' => fn(ParseNode $n) => $o->setEndOfReport($n->getStringValue()),
            'eoriNumber' => fn(ParseNode $n) => $o->setEoriNumber($n->getStringValue()),
            'fax' => fn(ParseNode $n) => $o->setFax($n->getStringValue()),
            'feesNominal' => fn(ParseNode $n) => $o->setFeesNominal($n->getStringValue()),
            'financialYear' => fn(ParseNode $n) => $o->setFinancialYear($n->getIntegerValue()),
            'fixedassetsLabel' => fn(ParseNode $n) => $o->setFixedassetsLabel($n->getStringValue()),
            'flatRateVatPercent' => fn(ParseNode $n) => $o->setFlatRateVatPercent($n->getFloatValue()),
            'hmrcPayeeEnabled' => fn(ParseNode $n) => $o->setHmrcPayeeEnabled($n->getIntegerValue()),
            'hmrcPayeeStatus' => fn(ParseNode $n) => $o->setHmrcPayeeStatus($n->getIntegerValue()),
            'hmrcPayeeStatusDescription' => fn(ParseNode $n) => $o->setHmrcPayeeStatusDescription($n->getStringValue()),
            'holdingAccount' => fn(ParseNode $n) => $o->setHoldingAccount($n->getStringValue()),
            'invoiceDiscUnitPrice' => fn(ParseNode $n) => $o->setInvoiceDiscUnitPrice($n->getIntegerValue()),
            'invoicePaymentsState' => fn(ParseNode $n) => $o->setInvoicePaymentsState($n->getStringValue()),
            'lAddress1' => fn(ParseNode $n) => $o->setLAddress1($n->getStringValue()),
            'lAddress2' => fn(ParseNode $n) => $o->setLAddress2($n->getStringValue()),
            'lAddress3' => fn(ParseNode $n) => $o->setLAddress3($n->getStringValue()),
            'lAddress4' => fn(ParseNode $n) => $o->setLAddress4($n->getStringValue()),
            'lAddress5' => fn(ParseNode $n) => $o->setLAddress5($n->getStringValue()),
            'lastBackupDate' => fn(ParseNode $n) => $o->setLastBackupDate($n->getDateTimeValue()),
            'lastCheckDataComments' => fn(ParseNode $n) => $o->setLastCheckDataComments($n->getIntegerValue()),
            'lastCheckDataDate' => fn(ParseNode $n) => $o->setLastCheckDataDate($n->getDateTimeValue()),
            'lastCheckDataErrors' => fn(ParseNode $n) => $o->setLastCheckDataErrors($n->getIntegerValue()),
            'lastCheckDataWarnings' => fn(ParseNode $n) => $o->setLastCheckDataWarnings($n->getIntegerValue()),
            'lastMonthEnd' => fn(ParseNode $n) => $o->setLastMonthEnd($n->getDateTimeValue()),
            'lastRestoreDate' => fn(ParseNode $n) => $o->setLastRestoreDate($n->getDateTimeValue()),
            'lastTransactionNumber' => fn(ParseNode $n) => $o->setLastTransactionNumber($n->getIntegerValue()),
            'lName' => fn(ParseNode $n) => $o->setLName($n->getStringValue()),
            'mtdState' => fn(ParseNode $n) => $o->setMtdState($n->getIntegerValue()),
            'name' => fn(ParseNode $n) => $o->setName($n->getStringValue()),
            'noDiscountPriceList' => fn(ParseNode $n) => $o->setNoDiscountPriceList($n->getIntegerValue()),
            'nomAccruals' => fn(ParseNode $n) => $o->setNomAccruals($n->getStringValue()),
            'nomBadDebts' => fn(ParseNode $n) => $o->setNomBadDebts($n->getStringValue()),
            'nomCreditCharges' => fn(ParseNode $n) => $o->setNomCreditCharges($n->getStringValue()),
            'nomCreditors' => fn(ParseNode $n) => $o->setNomCreditors($n->getStringValue()),
            'nomDebtors' => fn(ParseNode $n) => $o->setNomDebtors($n->getStringValue()),
            'nomDefaultBank' => fn(ParseNode $n) => $o->setNomDefaultBank($n->getStringValue()),
            'nomDefaultSales' => fn(ParseNode $n) => $o->setNomDefaultSales($n->getStringValue()),
            'nomDefrevaluation' => fn(ParseNode $n) => $o->setNomDefrevaluation($n->getStringValue()),
            'nomDiscountPurchase' => fn(ParseNode $n) => $o->setNomDiscountPurchase($n->getStringValue()),
            'nomDiscountSales' => fn(ParseNode $n) => $o->setNomDiscountSales($n->getStringValue()),
            'nominatedBank' => fn(ParseNode $n) => $o->setNominatedBank($n->getStringValue()),
            'nomIoss' => fn(ParseNode $n) => $o->setNomIoss($n->getStringValue()),
            'nomManualadjs' => fn(ParseNode $n) => $o->setNomManualadjs($n->getStringValue()),
            'nomMispostings' => fn(ParseNode $n) => $o->setNomMispostings($n->getStringValue()),
            'nomNonunionoss' => fn(ParseNode $n) => $o->setNomNonunionoss($n->getStringValue()),
            'nomPrepayments' => fn(ParseNode $n) => $o->setNomPrepayments($n->getStringValue()),
            'nomRetainedEarnings' => fn(ParseNode $n) => $o->setNomRetainedEarnings($n->getStringValue()),
            'nomSuspense' => fn(ParseNode $n) => $o->setNomSuspense($n->getStringValue()),
            'nomTaxPurchase' => fn(ParseNode $n) => $o->setNomTaxPurchase($n->getStringValue()),
            'nomTaxSales' => fn(ParseNode $n) => $o->setNomTaxSales($n->getStringValue()),
            'nomUnionoss' => fn(ParseNode $n) => $o->setNomUnionoss($n->getStringValue()),
            'nomVatliability' => fn(ParseNode $n) => $o->setNomVatliability($n->getStringValue()),
            'nonVatTaxCode' => fn(ParseNode $n) => $o->setNonVatTaxCode($n->getIntegerValue()),
            'numberOfTransactions' => fn(ParseNode $n) => $o->setNumberOfTransactions($n->getIntegerValue()),
            'office365State' => fn(ParseNode $n) => $o->setOffice365State($n->getIntegerValue()),
            'ossiossEnabled' => fn(ParseNode $n) => $o->setOssiossEnabled($n->getIntegerValue()),
            'paymentDueFromPurchase' => fn(ParseNode $n) => $o->setPaymentDueFromPurchase($n->getIntegerValue()),
            'paymentDueFromPurchaseText' => fn(ParseNode $n) => $o->setPaymentDueFromPurchaseText($n->getStringValue()),
            'paymentDueFromSales' => fn(ParseNode $n) => $o->setPaymentDueFromSales($n->getIntegerValue()),
            'paymentDueFromSalesText' => fn(ParseNode $n) => $o->setPaymentDueFromSalesText($n->getStringValue()),
            'paymentDuePurchase' => fn(ParseNode $n) => $o->setPaymentDuePurchase($n->getIntegerValue()),
            'paymentDueSales' => fn(ParseNode $n) => $o->setPaymentDueSales($n->getIntegerValue()),
            'popCarriageDept' => fn(ParseNode $n) => $o->setPopCarriageDept($n->getIntegerValue()),
            'popCarriageNet' => fn(ParseNode $n) => $o->setPopCarriageNet($n->getFloatValue()),
            'popCarriageNomCode' => fn(ParseNode $n) => $o->setPopCarriageNomCode($n->getStringValue()),
            'popCarriageTaxCode' => fn(ParseNode $n) => $o->setPopCarriageTaxCode($n->getIntegerValue()),
            'popGlobalDept' => fn(ParseNode $n) => $o->setPopGlobalDept($n->getIntegerValue()),
            'popGlobalDetails' => fn(ParseNode $n) => $o->setPopGlobalDetails($n->getStringValue()),
            'popGlobalNomCode' => fn(ParseNode $n) => $o->setPopGlobalNomCode($n->getStringValue()),
            'popGlobalTaxCode' => fn(ParseNode $n) => $o->setPopGlobalTaxCode($n->getIntegerValue()),
            'programBugfixVersion' => fn(ParseNode $n) => $o->setProgramBugfixVersion($n->getIntegerValue()),
            'programBuildVersion' => fn(ParseNode $n) => $o->setProgramBuildVersion($n->getIntegerValue()),
            'programMajorVersion' => fn(ParseNode $n) => $o->setProgramMajorVersion($n->getIntegerValue()),
            'programMinorVersion' => fn(ParseNode $n) => $o->setProgramMinorVersion($n->getIntegerValue()),
            'programType' => fn(ParseNode $n) => $o->setProgramType($n->getStringValue()),
            'protxEnabled' => fn(ParseNode $n) => $o->setProtxEnabled($n->getIntegerValue()),
            'protxLastSyncDate' => fn(ParseNode $n) => $o->setProtxLastSyncDate($n->getStringValue()),
            'protxPassword' => fn(ParseNode $n) => $o->setProtxPassword($n->getStringValue()),
            'protxUserid' => fn(ParseNode $n) => $o->setProtxUserid($n->getStringValue()),
            'protxVendor' => fn(ParseNode $n) => $o->setProtxVendor($n->getStringValue()),
            'purchaseAgeByMonth' => fn(ParseNode $n) => $o->setPurchaseAgeByMonth($n->getIntegerValue()),
            'purchaseAgedPeriod1' => fn(ParseNode $n) => $o->setPurchaseAgedPeriod1($n->getIntegerValue()),
            'purchaseAgedPeriod2' => fn(ParseNode $n) => $o->setPurchaseAgedPeriod2($n->getIntegerValue()),
            'purchaseAgedPeriod3' => fn(ParseNode $n) => $o->setPurchaseAgedPeriod3($n->getIntegerValue()),
            'purchaseAgedPeriod4' => fn(ParseNode $n) => $o->setPurchaseAgedPeriod4($n->getIntegerValue()),
            'purchaseDefaultIncoterms' => fn(ParseNode $n) => $o->setPurchaseDefaultIncoterms($n->getIntegerValue()),
            'purchaseDefaultIncotermsText' => fn(ParseNode $n) => $o->setPurchaseDefaultIncotermsText($n->getStringValue()),
            'purchaseLabel1' => fn(ParseNode $n) => $o->setPurchaseLabel1($n->getStringValue()),
            'purchaseLabel2' => fn(ParseNode $n) => $o->setPurchaseLabel2($n->getStringValue()),
            'purchaseLabel3' => fn(ParseNode $n) => $o->setPurchaseLabel3($n->getStringValue()),
            'qtyDecimalPlace' => fn(ParseNode $n) => $o->setQtyDecimalPlace($n->getIntegerValue()),
            'quotesToOrders' => fn(ParseNode $n) => $o->setQuotesToOrders($n->getIntegerValue()),
            'recordCreateDate' => fn(ParseNode $n) => $o->setRecordCreateDate($n->getDateTimeValue()),
            'recordDeleted' => fn(ParseNode $n) => $o->setRecordDeleted($n->getIntegerValue()),
            'recordModifyDate' => fn(ParseNode $n) => $o->setRecordModifyDate($n->getDateTimeValue()),
            'salesAddDisc1' => fn(ParseNode $n) => $o->setSalesAddDisc1($n->getFloatValue()),
            'salesAddDisc10' => fn(ParseNode $n) => $o->setSalesAddDisc10($n->getFloatValue()),
            'salesAddDisc2' => fn(ParseNode $n) => $o->setSalesAddDisc2($n->getFloatValue()),
            'salesAddDisc3' => fn(ParseNode $n) => $o->setSalesAddDisc3($n->getFloatValue()),
            'salesAddDisc4' => fn(ParseNode $n) => $o->setSalesAddDisc4($n->getFloatValue()),
            'salesAddDisc5' => fn(ParseNode $n) => $o->setSalesAddDisc5($n->getFloatValue()),
            'salesAddDisc6' => fn(ParseNode $n) => $o->setSalesAddDisc6($n->getFloatValue()),
            'salesAddDisc7' => fn(ParseNode $n) => $o->setSalesAddDisc7($n->getFloatValue()),
            'salesAddDisc8' => fn(ParseNode $n) => $o->setSalesAddDisc8($n->getFloatValue()),
            'salesAddDisc9' => fn(ParseNode $n) => $o->setSalesAddDisc9($n->getFloatValue()),
            'salesAgeByMonth' => fn(ParseNode $n) => $o->setSalesAgeByMonth($n->getIntegerValue()),
            'salesAgedPeriod1' => fn(ParseNode $n) => $o->setSalesAgedPeriod1($n->getIntegerValue()),
            'salesAgedPeriod2' => fn(ParseNode $n) => $o->setSalesAgedPeriod2($n->getIntegerValue()),
            'salesAgedPeriod3' => fn(ParseNode $n) => $o->setSalesAgedPeriod3($n->getIntegerValue()),
            'salesAgedPeriod4' => fn(ParseNode $n) => $o->setSalesAgedPeriod4($n->getIntegerValue()),
            'salesAmountSold1' => fn(ParseNode $n) => $o->setSalesAmountSold1($n->getFloatValue()),
            'salesAmountSold10' => fn(ParseNode $n) => $o->setSalesAmountSold10($n->getFloatValue()),
            'salesAmountSold2' => fn(ParseNode $n) => $o->setSalesAmountSold2($n->getFloatValue()),
            'salesAmountSold3' => fn(ParseNode $n) => $o->setSalesAmountSold3($n->getFloatValue()),
            'salesAmountSold4' => fn(ParseNode $n) => $o->setSalesAmountSold4($n->getFloatValue()),
            'salesAmountSold5' => fn(ParseNode $n) => $o->setSalesAmountSold5($n->getFloatValue()),
            'salesAmountSold6' => fn(ParseNode $n) => $o->setSalesAmountSold6($n->getFloatValue()),
            'salesAmountSold7' => fn(ParseNode $n) => $o->setSalesAmountSold7($n->getFloatValue()),
            'salesAmountSold8' => fn(ParseNode $n) => $o->setSalesAmountSold8($n->getFloatValue()),
            'salesAmountSold9' => fn(ParseNode $n) => $o->setSalesAmountSold9($n->getFloatValue()),
            'salesDefaultIncoterms' => fn(ParseNode $n) => $o->setSalesDefaultIncoterms($n->getIntegerValue()),
            'salesDefaultIncotermsText' => fn(ParseNode $n) => $o->setSalesDefaultIncotermsText($n->getStringValue()),
            'salesLabel1' => fn(ParseNode $n) => $o->setSalesLabel1($n->getStringValue()),
            'salesLabel2' => fn(ParseNode $n) => $o->setSalesLabel2($n->getStringValue()),
            'salesLabel3' => fn(ParseNode $n) => $o->setSalesLabel3($n->getStringValue()),
            'sipsState' => fn(ParseNode $n) => $o->setSipsState($n->getIntegerValue()),
            'sopCarriageDept' => fn(ParseNode $n) => $o->setSopCarriageDept($n->getIntegerValue()),
            'sopCarriageNet' => fn(ParseNode $n) => $o->setSopCarriageNet($n->getFloatValue()),
            'sopCarriageNomCode' => fn(ParseNode $n) => $o->setSopCarriageNomCode($n->getStringValue()),
            'sopCarriageTaxCode' => fn(ParseNode $n) => $o->setSopCarriageTaxCode($n->getIntegerValue()),
            'sopGlobalDept' => fn(ParseNode $n) => $o->setSopGlobalDept($n->getIntegerValue()),
            'sopGlobalDetails' => fn(ParseNode $n) => $o->setSopGlobalDetails($n->getStringValue()),
            'sopGlobalNomCode' => fn(ParseNode $n) => $o->setSopGlobalNomCode($n->getStringValue()),
            'sopGlobalTaxCode' => fn(ParseNode $n) => $o->setSopGlobalTaxCode($n->getIntegerValue()),
            'startMonth' => fn(ParseNode $n) => $o->setStartMonth($n->getIntegerValue()),
            'stockcategoryLabel' => fn(ParseNode $n) => $o->setStockcategoryLabel($n->getStringValue()),
            'stockDiscountA' => fn(ParseNode $n) => $o->setStockDiscountA($n->getStringValue()),
            'stockDiscountB' => fn(ParseNode $n) => $o->setStockDiscountB($n->getStringValue()),
            'stockDiscountC' => fn(ParseNode $n) => $o->setStockDiscountC($n->getStringValue()),
            'stockDiscountD' => fn(ParseNode $n) => $o->setStockDiscountD($n->getStringValue()),
            'stockDiscountE' => fn(ParseNode $n) => $o->setStockDiscountE($n->getStringValue()),
            'stockLabel1' => fn(ParseNode $n) => $o->setStockLabel1($n->getStringValue()),
            'stockLabel2' => fn(ParseNode $n) => $o->setStockLabel2($n->getStringValue()),
            'stockLabel3' => fn(ParseNode $n) => $o->setStockLabel3($n->getStringValue()),
            'supplierPaymentDueImmediately' => fn(ParseNode $n) => $o->setSupplierPaymentDueImmediately($n->getIntegerValue()),
            'telephone' => fn(ParseNode $n) => $o->setTelephone($n->getStringValue()),
            'unitDecimalPlace' => fn(ParseNode $n) => $o->setUnitDecimalPlace($n->getIntegerValue()),
            'vatCash' => fn(ParseNode $n) => $o->setVatCash($n->getIntegerValue()),
            'vatRegNumber' => fn(ParseNode $n) => $o->setVatRegNumber($n->getStringValue()),
            'webAddress' => fn(ParseNode $n) => $o->setWebAddress($n->getStringValue()),
        ];
    }

    /**
     * Gets the financialYear property value. The financialYear property
     * @return int|null
    */
    public function getFinancialYear(): ?int {
        return $this->financialYear;
    }

    /**
     * Gets the fixedassetsLabel property value. The fixedassetsLabel property
     * @return string|null
    */
    public function getFixedassetsLabel(): ?string {
        return $this->fixedassetsLabel;
    }

    /**
     * Gets the flatRateVatPercent property value. The flatRateVatPercent property
     * @return float|null
    */
    public function getFlatRateVatPercent(): ?float {
        return $this->flatRateVatPercent;
    }

    /**
     * Gets the hmrcPayeeEnabled property value. The hmrcPayeeEnabled property
     * @return int|null
    */
    public function getHmrcPayeeEnabled(): ?int {
        return $this->hmrcPayeeEnabled;
    }

    /**
     * Gets the hmrcPayeeStatus property value. The hmrcPayeeStatus property
     * @return int|null
    */
    public function getHmrcPayeeStatus(): ?int {
        return $this->hmrcPayeeStatus;
    }

    /**
     * Gets the hmrcPayeeStatusDescription property value. The hmrcPayeeStatusDescription property
     * @return string|null
    */
    public function getHmrcPayeeStatusDescription(): ?string {
        return $this->hmrcPayeeStatusDescription;
    }

    /**
     * Gets the holdingAccount property value. The holdingAccount property
     * @return string|null
    */
    public function getHoldingAccount(): ?string {
        return $this->holdingAccount;
    }

    /**
     * Gets the invoiceDiscUnitPrice property value. The invoiceDiscUnitPrice property
     * @return int|null
    */
    public function getInvoiceDiscUnitPrice(): ?int {
        return $this->invoiceDiscUnitPrice;
    }

    /**
     * Gets the invoicePaymentsState property value. The invoicePaymentsState property
     * @return string|null
    */
    public function getInvoicePaymentsState(): ?string {
        return $this->invoicePaymentsState;
    }

    /**
     * Gets the lAddress1 property value. The lAddress1 property
     * @return string|null
    */
    public function getLAddress1(): ?string {
        return $this->lAddress1;
    }

    /**
     * Gets the lAddress2 property value. The lAddress2 property
     * @return string|null
    */
    public function getLAddress2(): ?string {
        return $this->lAddress2;
    }

    /**
     * Gets the lAddress3 property value. The lAddress3 property
     * @return string|null
    */
    public function getLAddress3(): ?string {
        return $this->lAddress3;
    }

    /**
     * Gets the lAddress4 property value. The lAddress4 property
     * @return string|null
    */
    public function getLAddress4(): ?string {
        return $this->lAddress4;
    }

    /**
     * Gets the lAddress5 property value. The lAddress5 property
     * @return string|null
    */
    public function getLAddress5(): ?string {
        return $this->lAddress5;
    }

    /**
     * Gets the lastBackupDate property value. The lastBackupDate property
     * @return DateTime|null
    */
    public function getLastBackupDate(): ?DateTime {
        return $this->lastBackupDate;
    }

    /**
     * Gets the lastCheckDataComments property value. The lastCheckDataComments property
     * @return int|null
    */
    public function getLastCheckDataComments(): ?int {
        return $this->lastCheckDataComments;
    }

    /**
     * Gets the lastCheckDataDate property value. The lastCheckDataDate property
     * @return DateTime|null
    */
    public function getLastCheckDataDate(): ?DateTime {
        return $this->lastCheckDataDate;
    }

    /**
     * Gets the lastCheckDataErrors property value. The lastCheckDataErrors property
     * @return int|null
    */
    public function getLastCheckDataErrors(): ?int {
        return $this->lastCheckDataErrors;
    }

    /**
     * Gets the lastCheckDataWarnings property value. The lastCheckDataWarnings property
     * @return int|null
    */
    public function getLastCheckDataWarnings(): ?int {
        return $this->lastCheckDataWarnings;
    }

    /**
     * Gets the lastMonthEnd property value. The lastMonthEnd property
     * @return DateTime|null
    */
    public function getLastMonthEnd(): ?DateTime {
        return $this->lastMonthEnd;
    }

    /**
     * Gets the lastRestoreDate property value. The lastRestoreDate property
     * @return DateTime|null
    */
    public function getLastRestoreDate(): ?DateTime {
        return $this->lastRestoreDate;
    }

    /**
     * Gets the lastTransactionNumber property value. The lastTransactionNumber property
     * @return int|null
    */
    public function getLastTransactionNumber(): ?int {
        return $this->lastTransactionNumber;
    }

    /**
     * Gets the lName property value. The lName property
     * @return string|null
    */
    public function getLName(): ?string {
        return $this->lName;
    }

    /**
     * Gets the mtdState property value. The mtdState property
     * @return int|null
    */
    public function getMtdState(): ?int {
        return $this->mtdState;
    }

    /**
     * Gets the name property value. The name property
     * @return string|null
    */
    public function getName(): ?string {
        return $this->name;
    }

    /**
     * Gets the noDiscountPriceList property value. The noDiscountPriceList property
     * @return int|null
    */
    public function getNoDiscountPriceList(): ?int {
        return $this->noDiscountPriceList;
    }

    /**
     * Gets the nomAccruals property value. The nomAccruals property
     * @return string|null
    */
    public function getNomAccruals(): ?string {
        return $this->nomAccruals;
    }

    /**
     * Gets the nomBadDebts property value. The nomBadDebts property
     * @return string|null
    */
    public function getNomBadDebts(): ?string {
        return $this->nomBadDebts;
    }

    /**
     * Gets the nomCreditCharges property value. The nomCreditCharges property
     * @return string|null
    */
    public function getNomCreditCharges(): ?string {
        return $this->nomCreditCharges;
    }

    /**
     * Gets the nomCreditors property value. The nomCreditors property
     * @return string|null
    */
    public function getNomCreditors(): ?string {
        return $this->nomCreditors;
    }

    /**
     * Gets the nomDebtors property value. The nomDebtors property
     * @return string|null
    */
    public function getNomDebtors(): ?string {
        return $this->nomDebtors;
    }

    /**
     * Gets the nomDefaultBank property value. The nomDefaultBank property
     * @return string|null
    */
    public function getNomDefaultBank(): ?string {
        return $this->nomDefaultBank;
    }

    /**
     * Gets the nomDefaultSales property value. The nomDefaultSales property
     * @return string|null
    */
    public function getNomDefaultSales(): ?string {
        return $this->nomDefaultSales;
    }

    /**
     * Gets the nomDefrevaluation property value. The nomDefrevaluation property
     * @return string|null
    */
    public function getNomDefrevaluation(): ?string {
        return $this->nomDefrevaluation;
    }

    /**
     * Gets the nomDiscountPurchase property value. The nomDiscountPurchase property
     * @return string|null
    */
    public function getNomDiscountPurchase(): ?string {
        return $this->nomDiscountPurchase;
    }

    /**
     * Gets the nomDiscountSales property value. The nomDiscountSales property
     * @return string|null
    */
    public function getNomDiscountSales(): ?string {
        return $this->nomDiscountSales;
    }

    /**
     * Gets the nominatedBank property value. The nominatedBank property
     * @return string|null
    */
    public function getNominatedBank(): ?string {
        return $this->nominatedBank;
    }

    /**
     * Gets the nomIoss property value. The nomIoss property
     * @return string|null
    */
    public function getNomIoss(): ?string {
        return $this->nomIoss;
    }

    /**
     * Gets the nomManualadjs property value. The nomManualadjs property
     * @return string|null
    */
    public function getNomManualadjs(): ?string {
        return $this->nomManualadjs;
    }

    /**
     * Gets the nomMispostings property value. The nomMispostings property
     * @return string|null
    */
    public function getNomMispostings(): ?string {
        return $this->nomMispostings;
    }

    /**
     * Gets the nomNonunionoss property value. The nomNonunionoss property
     * @return string|null
    */
    public function getNomNonunionoss(): ?string {
        return $this->nomNonunionoss;
    }

    /**
     * Gets the nomPrepayments property value. The nomPrepayments property
     * @return string|null
    */
    public function getNomPrepayments(): ?string {
        return $this->nomPrepayments;
    }

    /**
     * Gets the nomRetainedEarnings property value. The nomRetainedEarnings property
     * @return string|null
    */
    public function getNomRetainedEarnings(): ?string {
        return $this->nomRetainedEarnings;
    }

    /**
     * Gets the nomSuspense property value. The nomSuspense property
     * @return string|null
    */
    public function getNomSuspense(): ?string {
        return $this->nomSuspense;
    }

    /**
     * Gets the nomTaxPurchase property value. The nomTaxPurchase property
     * @return string|null
    */
    public function getNomTaxPurchase(): ?string {
        return $this->nomTaxPurchase;
    }

    /**
     * Gets the nomTaxSales property value. The nomTaxSales property
     * @return string|null
    */
    public function getNomTaxSales(): ?string {
        return $this->nomTaxSales;
    }

    /**
     * Gets the nomUnionoss property value. The nomUnionoss property
     * @return string|null
    */
    public function getNomUnionoss(): ?string {
        return $this->nomUnionoss;
    }

    /**
     * Gets the nomVatliability property value. The nomVatliability property
     * @return string|null
    */
    public function getNomVatliability(): ?string {
        return $this->nomVatliability;
    }

    /**
     * Gets the nonVatTaxCode property value. The nonVatTaxCode property
     * @return int|null
    */
    public function getNonVatTaxCode(): ?int {
        return $this->nonVatTaxCode;
    }

    /**
     * Gets the numberOfTransactions property value. The numberOfTransactions property
     * @return int|null
    */
    public function getNumberOfTransactions(): ?int {
        return $this->numberOfTransactions;
    }

    /**
     * Gets the office365State property value. The office365State property
     * @return int|null
    */
    public function getOffice365State(): ?int {
        return $this->office365State;
    }

    /**
     * Gets the ossiossEnabled property value. The ossiossEnabled property
     * @return int|null
    */
    public function getOssiossEnabled(): ?int {
        return $this->ossiossEnabled;
    }

    /**
     * Gets the paymentDueFromPurchase property value. The paymentDueFromPurchase property
     * @return int|null
    */
    public function getPaymentDueFromPurchase(): ?int {
        return $this->paymentDueFromPurchase;
    }

    /**
     * Gets the paymentDueFromPurchaseText property value. The paymentDueFromPurchaseText property
     * @return string|null
    */
    public function getPaymentDueFromPurchaseText(): ?string {
        return $this->paymentDueFromPurchaseText;
    }

    /**
     * Gets the paymentDueFromSales property value. The paymentDueFromSales property
     * @return int|null
    */
    public function getPaymentDueFromSales(): ?int {
        return $this->paymentDueFromSales;
    }

    /**
     * Gets the paymentDueFromSalesText property value. The paymentDueFromSalesText property
     * @return string|null
    */
    public function getPaymentDueFromSalesText(): ?string {
        return $this->paymentDueFromSalesText;
    }

    /**
     * Gets the paymentDuePurchase property value. The paymentDuePurchase property
     * @return int|null
    */
    public function getPaymentDuePurchase(): ?int {
        return $this->paymentDuePurchase;
    }

    /**
     * Gets the paymentDueSales property value. The paymentDueSales property
     * @return int|null
    */
    public function getPaymentDueSales(): ?int {
        return $this->paymentDueSales;
    }

    /**
     * Gets the popCarriageDept property value. The popCarriageDept property
     * @return int|null
    */
    public function getPopCarriageDept(): ?int {
        return $this->popCarriageDept;
    }

    /**
     * Gets the popCarriageNet property value. The popCarriageNet property
     * @return float|null
    */
    public function getPopCarriageNet(): ?float {
        return $this->popCarriageNet;
    }

    /**
     * Gets the popCarriageNomCode property value. The popCarriageNomCode property
     * @return string|null
    */
    public function getPopCarriageNomCode(): ?string {
        return $this->popCarriageNomCode;
    }

    /**
     * Gets the popCarriageTaxCode property value. The popCarriageTaxCode property
     * @return int|null
    */
    public function getPopCarriageTaxCode(): ?int {
        return $this->popCarriageTaxCode;
    }

    /**
     * Gets the popGlobalDept property value. The popGlobalDept property
     * @return int|null
    */
    public function getPopGlobalDept(): ?int {
        return $this->popGlobalDept;
    }

    /**
     * Gets the popGlobalDetails property value. The popGlobalDetails property
     * @return string|null
    */
    public function getPopGlobalDetails(): ?string {
        return $this->popGlobalDetails;
    }

    /**
     * Gets the popGlobalNomCode property value. The popGlobalNomCode property
     * @return string|null
    */
    public function getPopGlobalNomCode(): ?string {
        return $this->popGlobalNomCode;
    }

    /**
     * Gets the popGlobalTaxCode property value. The popGlobalTaxCode property
     * @return int|null
    */
    public function getPopGlobalTaxCode(): ?int {
        return $this->popGlobalTaxCode;
    }

    /**
     * Gets the programBugfixVersion property value. The programBugfixVersion property
     * @return int|null
    */
    public function getProgramBugfixVersion(): ?int {
        return $this->programBugfixVersion;
    }

    /**
     * Gets the programBuildVersion property value. The programBuildVersion property
     * @return int|null
    */
    public function getProgramBuildVersion(): ?int {
        return $this->programBuildVersion;
    }

    /**
     * Gets the programMajorVersion property value. The programMajorVersion property
     * @return int|null
    */
    public function getProgramMajorVersion(): ?int {
        return $this->programMajorVersion;
    }

    /**
     * Gets the programMinorVersion property value. The programMinorVersion property
     * @return int|null
    */
    public function getProgramMinorVersion(): ?int {
        return $this->programMinorVersion;
    }

    /**
     * Gets the programType property value. The programType property
     * @return string|null
    */
    public function getProgramType(): ?string {
        return $this->programType;
    }

    /**
     * Gets the protxEnabled property value. The protxEnabled property
     * @return int|null
    */
    public function getProtxEnabled(): ?int {
        return $this->protxEnabled;
    }

    /**
     * Gets the protxLastSyncDate property value. The protxLastSyncDate property
     * @return string|null
    */
    public function getProtxLastSyncDate(): ?string {
        return $this->protxLastSyncDate;
    }

    /**
     * Gets the protxPassword property value. The protxPassword property
     * @return string|null
    */
    public function getProtxPassword(): ?string {
        return $this->protxPassword;
    }

    /**
     * Gets the protxUserid property value. The protxUserid property
     * @return string|null
    */
    public function getProtxUserid(): ?string {
        return $this->protxUserid;
    }

    /**
     * Gets the protxVendor property value. The protxVendor property
     * @return string|null
    */
    public function getProtxVendor(): ?string {
        return $this->protxVendor;
    }

    /**
     * Gets the purchaseAgeByMonth property value. The purchaseAgeByMonth property
     * @return int|null
    */
    public function getPurchaseAgeByMonth(): ?int {
        return $this->purchaseAgeByMonth;
    }

    /**
     * Gets the purchaseAgedPeriod1 property value. The purchaseAgedPeriod1 property
     * @return int|null
    */
    public function getPurchaseAgedPeriod1(): ?int {
        return $this->purchaseAgedPeriod1;
    }

    /**
     * Gets the purchaseAgedPeriod2 property value. The purchaseAgedPeriod2 property
     * @return int|null
    */
    public function getPurchaseAgedPeriod2(): ?int {
        return $this->purchaseAgedPeriod2;
    }

    /**
     * Gets the purchaseAgedPeriod3 property value. The purchaseAgedPeriod3 property
     * @return int|null
    */
    public function getPurchaseAgedPeriod3(): ?int {
        return $this->purchaseAgedPeriod3;
    }

    /**
     * Gets the purchaseAgedPeriod4 property value. The purchaseAgedPeriod4 property
     * @return int|null
    */
    public function getPurchaseAgedPeriod4(): ?int {
        return $this->purchaseAgedPeriod4;
    }

    /**
     * Gets the purchaseDefaultIncoterms property value. The purchaseDefaultIncoterms property
     * @return int|null
    */
    public function getPurchaseDefaultIncoterms(): ?int {
        return $this->purchaseDefaultIncoterms;
    }

    /**
     * Gets the purchaseDefaultIncotermsText property value. The purchaseDefaultIncotermsText property
     * @return string|null
    */
    public function getPurchaseDefaultIncotermsText(): ?string {
        return $this->purchaseDefaultIncotermsText;
    }

    /**
     * Gets the purchaseLabel1 property value. The purchaseLabel1 property
     * @return string|null
    */
    public function getPurchaseLabel1(): ?string {
        return $this->purchaseLabel1;
    }

    /**
     * Gets the purchaseLabel2 property value. The purchaseLabel2 property
     * @return string|null
    */
    public function getPurchaseLabel2(): ?string {
        return $this->purchaseLabel2;
    }

    /**
     * Gets the purchaseLabel3 property value. The purchaseLabel3 property
     * @return string|null
    */
    public function getPurchaseLabel3(): ?string {
        return $this->purchaseLabel3;
    }

    /**
     * Gets the qtyDecimalPlace property value. The qtyDecimalPlace property
     * @return int|null
    */
    public function getQtyDecimalPlace(): ?int {
        return $this->qtyDecimalPlace;
    }

    /**
     * Gets the quotesToOrders property value. The quotesToOrders property
     * @return int|null
    */
    public function getQuotesToOrders(): ?int {
        return $this->quotesToOrders;
    }

    /**
     * Gets the recordCreateDate property value. The recordCreateDate property
     * @return DateTime|null
    */
    public function getRecordCreateDate(): ?DateTime {
        return $this->recordCreateDate;
    }

    /**
     * Gets the recordDeleted property value. The recordDeleted property
     * @return int|null
    */
    public function getRecordDeleted(): ?int {
        return $this->recordDeleted;
    }

    /**
     * Gets the recordModifyDate property value. The recordModifyDate property
     * @return DateTime|null
    */
    public function getRecordModifyDate(): ?DateTime {
        return $this->recordModifyDate;
    }

    /**
     * Gets the salesAddDisc1 property value. The salesAddDisc1 property
     * @return float|null
    */
    public function getSalesAddDisc1(): ?float {
        return $this->salesAddDisc1;
    }

    /**
     * Gets the salesAddDisc10 property value. The salesAddDisc10 property
     * @return float|null
    */
    public function getSalesAddDisc10(): ?float {
        return $this->salesAddDisc10;
    }

    /**
     * Gets the salesAddDisc2 property value. The salesAddDisc2 property
     * @return float|null
    */
    public function getSalesAddDisc2(): ?float {
        return $this->salesAddDisc2;
    }

    /**
     * Gets the salesAddDisc3 property value. The salesAddDisc3 property
     * @return float|null
    */
    public function getSalesAddDisc3(): ?float {
        return $this->salesAddDisc3;
    }

    /**
     * Gets the salesAddDisc4 property value. The salesAddDisc4 property
     * @return float|null
    */
    public function getSalesAddDisc4(): ?float {
        return $this->salesAddDisc4;
    }

    /**
     * Gets the salesAddDisc5 property value. The salesAddDisc5 property
     * @return float|null
    */
    public function getSalesAddDisc5(): ?float {
        return $this->salesAddDisc5;
    }

    /**
     * Gets the salesAddDisc6 property value. The salesAddDisc6 property
     * @return float|null
    */
    public function getSalesAddDisc6(): ?float {
        return $this->salesAddDisc6;
    }

    /**
     * Gets the salesAddDisc7 property value. The salesAddDisc7 property
     * @return float|null
    */
    public function getSalesAddDisc7(): ?float {
        return $this->salesAddDisc7;
    }

    /**
     * Gets the salesAddDisc8 property value. The salesAddDisc8 property
     * @return float|null
    */
    public function getSalesAddDisc8(): ?float {
        return $this->salesAddDisc8;
    }

    /**
     * Gets the salesAddDisc9 property value. The salesAddDisc9 property
     * @return float|null
    */
    public function getSalesAddDisc9(): ?float {
        return $this->salesAddDisc9;
    }

    /**
     * Gets the salesAgeByMonth property value. The salesAgeByMonth property
     * @return int|null
    */
    public function getSalesAgeByMonth(): ?int {
        return $this->salesAgeByMonth;
    }

    /**
     * Gets the salesAgedPeriod1 property value. The salesAgedPeriod1 property
     * @return int|null
    */
    public function getSalesAgedPeriod1(): ?int {
        return $this->salesAgedPeriod1;
    }

    /**
     * Gets the salesAgedPeriod2 property value. The salesAgedPeriod2 property
     * @return int|null
    */
    public function getSalesAgedPeriod2(): ?int {
        return $this->salesAgedPeriod2;
    }

    /**
     * Gets the salesAgedPeriod3 property value. The salesAgedPeriod3 property
     * @return int|null
    */
    public function getSalesAgedPeriod3(): ?int {
        return $this->salesAgedPeriod3;
    }

    /**
     * Gets the salesAgedPeriod4 property value. The salesAgedPeriod4 property
     * @return int|null
    */
    public function getSalesAgedPeriod4(): ?int {
        return $this->salesAgedPeriod4;
    }

    /**
     * Gets the salesAmountSold1 property value. The salesAmountSold1 property
     * @return float|null
    */
    public function getSalesAmountSold1(): ?float {
        return $this->salesAmountSold1;
    }

    /**
     * Gets the salesAmountSold10 property value. The salesAmountSold10 property
     * @return float|null
    */
    public function getSalesAmountSold10(): ?float {
        return $this->salesAmountSold10;
    }

    /**
     * Gets the salesAmountSold2 property value. The salesAmountSold2 property
     * @return float|null
    */
    public function getSalesAmountSold2(): ?float {
        return $this->salesAmountSold2;
    }

    /**
     * Gets the salesAmountSold3 property value. The salesAmountSold3 property
     * @return float|null
    */
    public function getSalesAmountSold3(): ?float {
        return $this->salesAmountSold3;
    }

    /**
     * Gets the salesAmountSold4 property value. The salesAmountSold4 property
     * @return float|null
    */
    public function getSalesAmountSold4(): ?float {
        return $this->salesAmountSold4;
    }

    /**
     * Gets the salesAmountSold5 property value. The salesAmountSold5 property
     * @return float|null
    */
    public function getSalesAmountSold5(): ?float {
        return $this->salesAmountSold5;
    }

    /**
     * Gets the salesAmountSold6 property value. The salesAmountSold6 property
     * @return float|null
    */
    public function getSalesAmountSold6(): ?float {
        return $this->salesAmountSold6;
    }

    /**
     * Gets the salesAmountSold7 property value. The salesAmountSold7 property
     * @return float|null
    */
    public function getSalesAmountSold7(): ?float {
        return $this->salesAmountSold7;
    }

    /**
     * Gets the salesAmountSold8 property value. The salesAmountSold8 property
     * @return float|null
    */
    public function getSalesAmountSold8(): ?float {
        return $this->salesAmountSold8;
    }

    /**
     * Gets the salesAmountSold9 property value. The salesAmountSold9 property
     * @return float|null
    */
    public function getSalesAmountSold9(): ?float {
        return $this->salesAmountSold9;
    }

    /**
     * Gets the salesDefaultIncoterms property value. The salesDefaultIncoterms property
     * @return int|null
    */
    public function getSalesDefaultIncoterms(): ?int {
        return $this->salesDefaultIncoterms;
    }

    /**
     * Gets the salesDefaultIncotermsText property value. The salesDefaultIncotermsText property
     * @return string|null
    */
    public function getSalesDefaultIncotermsText(): ?string {
        return $this->salesDefaultIncotermsText;
    }

    /**
     * Gets the salesLabel1 property value. The salesLabel1 property
     * @return string|null
    */
    public function getSalesLabel1(): ?string {
        return $this->salesLabel1;
    }

    /**
     * Gets the salesLabel2 property value. The salesLabel2 property
     * @return string|null
    */
    public function getSalesLabel2(): ?string {
        return $this->salesLabel2;
    }

    /**
     * Gets the salesLabel3 property value. The salesLabel3 property
     * @return string|null
    */
    public function getSalesLabel3(): ?string {
        return $this->salesLabel3;
    }

    /**
     * Gets the sipsState property value. The sipsState property
     * @return int|null
    */
    public function getSipsState(): ?int {
        return $this->sipsState;
    }

    /**
     * Gets the sopCarriageDept property value. The sopCarriageDept property
     * @return int|null
    */
    public function getSopCarriageDept(): ?int {
        return $this->sopCarriageDept;
    }

    /**
     * Gets the sopCarriageNet property value. The sopCarriageNet property
     * @return float|null
    */
    public function getSopCarriageNet(): ?float {
        return $this->sopCarriageNet;
    }

    /**
     * Gets the sopCarriageNomCode property value. The sopCarriageNomCode property
     * @return string|null
    */
    public function getSopCarriageNomCode(): ?string {
        return $this->sopCarriageNomCode;
    }

    /**
     * Gets the sopCarriageTaxCode property value. The sopCarriageTaxCode property
     * @return int|null
    */
    public function getSopCarriageTaxCode(): ?int {
        return $this->sopCarriageTaxCode;
    }

    /**
     * Gets the sopGlobalDept property value. The sopGlobalDept property
     * @return int|null
    */
    public function getSopGlobalDept(): ?int {
        return $this->sopGlobalDept;
    }

    /**
     * Gets the sopGlobalDetails property value. The sopGlobalDetails property
     * @return string|null
    */
    public function getSopGlobalDetails(): ?string {
        return $this->sopGlobalDetails;
    }

    /**
     * Gets the sopGlobalNomCode property value. The sopGlobalNomCode property
     * @return string|null
    */
    public function getSopGlobalNomCode(): ?string {
        return $this->sopGlobalNomCode;
    }

    /**
     * Gets the sopGlobalTaxCode property value. The sopGlobalTaxCode property
     * @return int|null
    */
    public function getSopGlobalTaxCode(): ?int {
        return $this->sopGlobalTaxCode;
    }

    /**
     * Gets the startMonth property value. The startMonth property
     * @return int|null
    */
    public function getStartMonth(): ?int {
        return $this->startMonth;
    }

    /**
     * Gets the stockcategoryLabel property value. The stockcategoryLabel property
     * @return string|null
    */
    public function getStockcategoryLabel(): ?string {
        return $this->stockcategoryLabel;
    }

    /**
     * Gets the stockDiscountA property value. The stockDiscountA property
     * @return string|null
    */
    public function getStockDiscountA(): ?string {
        return $this->stockDiscountA;
    }

    /**
     * Gets the stockDiscountB property value. The stockDiscountB property
     * @return string|null
    */
    public function getStockDiscountB(): ?string {
        return $this->stockDiscountB;
    }

    /**
     * Gets the stockDiscountC property value. The stockDiscountC property
     * @return string|null
    */
    public function getStockDiscountC(): ?string {
        return $this->stockDiscountC;
    }

    /**
     * Gets the stockDiscountD property value. The stockDiscountD property
     * @return string|null
    */
    public function getStockDiscountD(): ?string {
        return $this->stockDiscountD;
    }

    /**
     * Gets the stockDiscountE property value. The stockDiscountE property
     * @return string|null
    */
    public function getStockDiscountE(): ?string {
        return $this->stockDiscountE;
    }

    /**
     * Gets the stockLabel1 property value. The stockLabel1 property
     * @return string|null
    */
    public function getStockLabel1(): ?string {
        return $this->stockLabel1;
    }

    /**
     * Gets the stockLabel2 property value. The stockLabel2 property
     * @return string|null
    */
    public function getStockLabel2(): ?string {
        return $this->stockLabel2;
    }

    /**
     * Gets the stockLabel3 property value. The stockLabel3 property
     * @return string|null
    */
    public function getStockLabel3(): ?string {
        return $this->stockLabel3;
    }

    /**
     * Gets the supplierPaymentDueImmediately property value. The supplierPaymentDueImmediately property
     * @return int|null
    */
    public function getSupplierPaymentDueImmediately(): ?int {
        return $this->supplierPaymentDueImmediately;
    }

    /**
     * Gets the telephone property value. The telephone property
     * @return string|null
    */
    public function getTelephone(): ?string {
        return $this->telephone;
    }

    /**
     * Gets the unitDecimalPlace property value. The unitDecimalPlace property
     * @return int|null
    */
    public function getUnitDecimalPlace(): ?int {
        return $this->unitDecimalPlace;
    }

    /**
     * Gets the vatCash property value. The vatCash property
     * @return int|null
    */
    public function getVatCash(): ?int {
        return $this->vatCash;
    }

    /**
     * Gets the vatRegNumber property value. The vatRegNumber property
     * @return string|null
    */
    public function getVatRegNumber(): ?string {
        return $this->vatRegNumber;
    }

    /**
     * Gets the webAddress property value. The webAddress property
     * @return string|null
    */
    public function getWebAddress(): ?string {
        return $this->webAddress;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeStringValue('address1', $this->getAddress1());
        $writer->writeStringValue('address2', $this->getAddress2());
        $writer->writeStringValue('address3', $this->getAddress3());
        $writer->writeStringValue('address4', $this->getAddress4());
        $writer->writeStringValue('address5', $this->getAddress5());
        $writer->writeStringValue('addressLabel1', $this->getAddressLabel1());
        $writer->writeStringValue('addressLabel2', $this->getAddressLabel2());
        $writer->writeStringValue('addressLabel3', $this->getAddressLabel3());
        $writer->writeStringValue('addressLabel4', $this->getAddressLabel4());
        $writer->writeStringValue('addressLabel5', $this->getAddressLabel5());
        $writer->writeIntegerValue('allowNegativeStock', $this->getAllowNegativeStock());
        $writer->writeIntegerValue('alwaysCreateBatchEpayment', $this->getAlwaysCreateBatchEpayment());
        $writer->writeIntegerValue('budgetingMethod', $this->getBudgetingMethod());
        $writer->writeStringValue('cAddress1', $this->getCAddress1());
        $writer->writeStringValue('cAddress2', $this->getCAddress2());
        $writer->writeStringValue('cAddress3', $this->getCAddress3());
        $writer->writeStringValue('cAddress4', $this->getCAddress4());
        $writer->writeStringValue('cAddress5', $this->getCAddress5());
        $writer->writeStringValue('cashRegisterBank', $this->getCashRegisterBank());
        $writer->writeStringValue('cashRegisterDiscrepenciesNom', $this->getCashRegisterDiscrepenciesNom());
        $writer->writeStringValue('cashRegisterSalesNom', $this->getCashRegisterSalesNom());
        $writer->writeIntegerValue('cashRegisterTaxcode', $this->getCashRegisterTaxcode());
        $writer->writeIntegerValue('cashRegisterTaxInclusiveFlag', $this->getCashRegisterTaxInclusiveFlag());
        $writer->writeStringValue('ccLetter1', $this->getCcLetter1());
        $writer->writeStringValue('ccLetter2', $this->getCcLetter2());
        $writer->writeStringValue('ccLetter3', $this->getCcLetter3());
        $writer->writeIntegerValue('chargeDays', $this->getChargeDays());
        $writer->writeIntegerValue('charityState', $this->getCharityState());
        $writer->writeStringValue('charitytaxref', $this->getCharitytaxref());
        $writer->writeDateTimeValue('clearAuditTrailDate', $this->getClearAuditTrailDate());
        $writer->writeDateTimeValue('closedLedgerCheckDate', $this->getClosedLedgerCheckDate());
        $writer->writeIntegerValue('closedLedgerDateEnabled', $this->getClosedLedgerDateEnabled());
        $writer->writeStringValue('countryCode', $this->getCountryCode());
        $writer->writeStringValue('countryCodeCombined', $this->getCountryCodeCombined());
        $writer->writeStringValue('countryCodeConverted', $this->getCountryCodeConverted());
        $writer->writeStringValue('creditRef', $this->getCreditRef());
        $writer->writeIntegerValue('customerPaymentDueImmediately', $this->getCustomerPaymentDueImmediately());
        $writer->writeIntegerValue('dataManagementDisabled', $this->getDataManagementDisabled());
        $writer->writeIntegerValue('defaultBudgetingChart', $this->getDefaultBudgetingChart());
        $writer->writeIntegerValue('defaultChart', $this->getDefaultChart());
        $writer->writeIntegerValue('defaultCreditBureau', $this->getDefaultCreditBureau());
        $writer->writeStringValue('delAddress1', $this->getDelAddress1());
        $writer->writeStringValue('delAddress2', $this->getDelAddress2());
        $writer->writeStringValue('delAddress3', $this->getDelAddress3());
        $writer->writeStringValue('delAddress4', $this->getDelAddress4());
        $writer->writeStringValue('delAddress5', $this->getDelAddress5());
        $writer->writeStringValue('delEMail', $this->getDelEMail());
        $writer->writeStringValue('delFax', $this->getDelFax());
        $writer->writeStringValue('delName', $this->getDelName());
        $writer->writeStringValue('delTelephone', $this->getDelTelephone());
        $writer->writeStringValue('delWebAddress', $this->getDelWebAddress());
        $writer->writeStringValue('departmentsLabel', $this->getDepartmentsLabel());
        $writer->writeIntegerValue('directDebitsEnabled', $this->getDirectDebitsEnabled());
        $writer->writeDateTimeValue('directDebitsLastSyncedDate', $this->getDirectDebitsLastSyncedDate());
        $writer->writeStringValue('dunsNumber', $this->getDunsNumber());
        $writer->writeIntegerValue('ebankingState', $this->getEbankingState());
        $writer->writeStringValue('eMail', $this->getEMail());
        $writer->writeStringValue('endOfReport', $this->getEndOfReport());
        $writer->writeStringValue('eoriNumber', $this->getEoriNumber());
        $writer->writeStringValue('fax', $this->getFax());
        $writer->writeStringValue('feesNominal', $this->getFeesNominal());
        $writer->writeIntegerValue('financialYear', $this->getFinancialYear());
        $writer->writeStringValue('fixedassetsLabel', $this->getFixedassetsLabel());
        $writer->writeFloatValue('flatRateVatPercent', $this->getFlatRateVatPercent());
        $writer->writeIntegerValue('hmrcPayeeEnabled', $this->getHmrcPayeeEnabled());
        $writer->writeIntegerValue('hmrcPayeeStatus', $this->getHmrcPayeeStatus());
        $writer->writeStringValue('hmrcPayeeStatusDescription', $this->getHmrcPayeeStatusDescription());
        $writer->writeStringValue('holdingAccount', $this->getHoldingAccount());
        $writer->writeIntegerValue('invoiceDiscUnitPrice', $this->getInvoiceDiscUnitPrice());
        $writer->writeStringValue('invoicePaymentsState', $this->getInvoicePaymentsState());
        $writer->writeStringValue('lAddress1', $this->getLAddress1());
        $writer->writeStringValue('lAddress2', $this->getLAddress2());
        $writer->writeStringValue('lAddress3', $this->getLAddress3());
        $writer->writeStringValue('lAddress4', $this->getLAddress4());
        $writer->writeStringValue('lAddress5', $this->getLAddress5());
        $writer->writeDateTimeValue('lastBackupDate', $this->getLastBackupDate());
        $writer->writeIntegerValue('lastCheckDataComments', $this->getLastCheckDataComments());
        $writer->writeDateTimeValue('lastCheckDataDate', $this->getLastCheckDataDate());
        $writer->writeIntegerValue('lastCheckDataErrors', $this->getLastCheckDataErrors());
        $writer->writeIntegerValue('lastCheckDataWarnings', $this->getLastCheckDataWarnings());
        $writer->writeDateTimeValue('lastMonthEnd', $this->getLastMonthEnd());
        $writer->writeDateTimeValue('lastRestoreDate', $this->getLastRestoreDate());
        $writer->writeIntegerValue('lastTransactionNumber', $this->getLastTransactionNumber());
        $writer->writeStringValue('lName', $this->getLName());
        $writer->writeIntegerValue('mtdState', $this->getMtdState());
        $writer->writeStringValue('name', $this->getName());
        $writer->writeIntegerValue('noDiscountPriceList', $this->getNoDiscountPriceList());
        $writer->writeStringValue('nomAccruals', $this->getNomAccruals());
        $writer->writeStringValue('nomBadDebts', $this->getNomBadDebts());
        $writer->writeStringValue('nomCreditCharges', $this->getNomCreditCharges());
        $writer->writeStringValue('nomCreditors', $this->getNomCreditors());
        $writer->writeStringValue('nomDebtors', $this->getNomDebtors());
        $writer->writeStringValue('nomDefaultBank', $this->getNomDefaultBank());
        $writer->writeStringValue('nomDefaultSales', $this->getNomDefaultSales());
        $writer->writeStringValue('nomDefrevaluation', $this->getNomDefrevaluation());
        $writer->writeStringValue('nomDiscountPurchase', $this->getNomDiscountPurchase());
        $writer->writeStringValue('nomDiscountSales', $this->getNomDiscountSales());
        $writer->writeStringValue('nominatedBank', $this->getNominatedBank());
        $writer->writeStringValue('nomIoss', $this->getNomIoss());
        $writer->writeStringValue('nomManualadjs', $this->getNomManualadjs());
        $writer->writeStringValue('nomMispostings', $this->getNomMispostings());
        $writer->writeStringValue('nomNonunionoss', $this->getNomNonunionoss());
        $writer->writeStringValue('nomPrepayments', $this->getNomPrepayments());
        $writer->writeStringValue('nomRetainedEarnings', $this->getNomRetainedEarnings());
        $writer->writeStringValue('nomSuspense', $this->getNomSuspense());
        $writer->writeStringValue('nomTaxPurchase', $this->getNomTaxPurchase());
        $writer->writeStringValue('nomTaxSales', $this->getNomTaxSales());
        $writer->writeStringValue('nomUnionoss', $this->getNomUnionoss());
        $writer->writeStringValue('nomVatliability', $this->getNomVatliability());
        $writer->writeIntegerValue('nonVatTaxCode', $this->getNonVatTaxCode());
        $writer->writeIntegerValue('numberOfTransactions', $this->getNumberOfTransactions());
        $writer->writeIntegerValue('office365State', $this->getOffice365State());
        $writer->writeIntegerValue('ossiossEnabled', $this->getOssiossEnabled());
        $writer->writeIntegerValue('paymentDueFromPurchase', $this->getPaymentDueFromPurchase());
        $writer->writeStringValue('paymentDueFromPurchaseText', $this->getPaymentDueFromPurchaseText());
        $writer->writeIntegerValue('paymentDueFromSales', $this->getPaymentDueFromSales());
        $writer->writeStringValue('paymentDueFromSalesText', $this->getPaymentDueFromSalesText());
        $writer->writeIntegerValue('paymentDuePurchase', $this->getPaymentDuePurchase());
        $writer->writeIntegerValue('paymentDueSales', $this->getPaymentDueSales());
        $writer->writeIntegerValue('popCarriageDept', $this->getPopCarriageDept());
        $writer->writeFloatValue('popCarriageNet', $this->getPopCarriageNet());
        $writer->writeStringValue('popCarriageNomCode', $this->getPopCarriageNomCode());
        $writer->writeIntegerValue('popCarriageTaxCode', $this->getPopCarriageTaxCode());
        $writer->writeIntegerValue('popGlobalDept', $this->getPopGlobalDept());
        $writer->writeStringValue('popGlobalDetails', $this->getPopGlobalDetails());
        $writer->writeStringValue('popGlobalNomCode', $this->getPopGlobalNomCode());
        $writer->writeIntegerValue('popGlobalTaxCode', $this->getPopGlobalTaxCode());
        $writer->writeIntegerValue('programBugfixVersion', $this->getProgramBugfixVersion());
        $writer->writeIntegerValue('programBuildVersion', $this->getProgramBuildVersion());
        $writer->writeIntegerValue('programMajorVersion', $this->getProgramMajorVersion());
        $writer->writeIntegerValue('programMinorVersion', $this->getProgramMinorVersion());
        $writer->writeStringValue('programType', $this->getProgramType());
        $writer->writeIntegerValue('protxEnabled', $this->getProtxEnabled());
        $writer->writeStringValue('protxLastSyncDate', $this->getProtxLastSyncDate());
        $writer->writeStringValue('protxPassword', $this->getProtxPassword());
        $writer->writeStringValue('protxUserid', $this->getProtxUserid());
        $writer->writeStringValue('protxVendor', $this->getProtxVendor());
        $writer->writeIntegerValue('purchaseAgeByMonth', $this->getPurchaseAgeByMonth());
        $writer->writeIntegerValue('purchaseAgedPeriod1', $this->getPurchaseAgedPeriod1());
        $writer->writeIntegerValue('purchaseAgedPeriod2', $this->getPurchaseAgedPeriod2());
        $writer->writeIntegerValue('purchaseAgedPeriod3', $this->getPurchaseAgedPeriod3());
        $writer->writeIntegerValue('purchaseAgedPeriod4', $this->getPurchaseAgedPeriod4());
        $writer->writeIntegerValue('purchaseDefaultIncoterms', $this->getPurchaseDefaultIncoterms());
        $writer->writeStringValue('purchaseDefaultIncotermsText', $this->getPurchaseDefaultIncotermsText());
        $writer->writeStringValue('purchaseLabel1', $this->getPurchaseLabel1());
        $writer->writeStringValue('purchaseLabel2', $this->getPurchaseLabel2());
        $writer->writeStringValue('purchaseLabel3', $this->getPurchaseLabel3());
        $writer->writeIntegerValue('qtyDecimalPlace', $this->getQtyDecimalPlace());
        $writer->writeIntegerValue('quotesToOrders', $this->getQuotesToOrders());
        $writer->writeDateTimeValue('recordCreateDate', $this->getRecordCreateDate());
        $writer->writeIntegerValue('recordDeleted', $this->getRecordDeleted());
        $writer->writeDateTimeValue('recordModifyDate', $this->getRecordModifyDate());
        $writer->writeFloatValue('salesAddDisc1', $this->getSalesAddDisc1());
        $writer->writeFloatValue('salesAddDisc10', $this->getSalesAddDisc10());
        $writer->writeFloatValue('salesAddDisc2', $this->getSalesAddDisc2());
        $writer->writeFloatValue('salesAddDisc3', $this->getSalesAddDisc3());
        $writer->writeFloatValue('salesAddDisc4', $this->getSalesAddDisc4());
        $writer->writeFloatValue('salesAddDisc5', $this->getSalesAddDisc5());
        $writer->writeFloatValue('salesAddDisc6', $this->getSalesAddDisc6());
        $writer->writeFloatValue('salesAddDisc7', $this->getSalesAddDisc7());
        $writer->writeFloatValue('salesAddDisc8', $this->getSalesAddDisc8());
        $writer->writeFloatValue('salesAddDisc9', $this->getSalesAddDisc9());
        $writer->writeIntegerValue('salesAgeByMonth', $this->getSalesAgeByMonth());
        $writer->writeIntegerValue('salesAgedPeriod1', $this->getSalesAgedPeriod1());
        $writer->writeIntegerValue('salesAgedPeriod2', $this->getSalesAgedPeriod2());
        $writer->writeIntegerValue('salesAgedPeriod3', $this->getSalesAgedPeriod3());
        $writer->writeIntegerValue('salesAgedPeriod4', $this->getSalesAgedPeriod4());
        $writer->writeFloatValue('salesAmountSold1', $this->getSalesAmountSold1());
        $writer->writeFloatValue('salesAmountSold10', $this->getSalesAmountSold10());
        $writer->writeFloatValue('salesAmountSold2', $this->getSalesAmountSold2());
        $writer->writeFloatValue('salesAmountSold3', $this->getSalesAmountSold3());
        $writer->writeFloatValue('salesAmountSold4', $this->getSalesAmountSold4());
        $writer->writeFloatValue('salesAmountSold5', $this->getSalesAmountSold5());
        $writer->writeFloatValue('salesAmountSold6', $this->getSalesAmountSold6());
        $writer->writeFloatValue('salesAmountSold7', $this->getSalesAmountSold7());
        $writer->writeFloatValue('salesAmountSold8', $this->getSalesAmountSold8());
        $writer->writeFloatValue('salesAmountSold9', $this->getSalesAmountSold9());
        $writer->writeIntegerValue('salesDefaultIncoterms', $this->getSalesDefaultIncoterms());
        $writer->writeStringValue('salesDefaultIncotermsText', $this->getSalesDefaultIncotermsText());
        $writer->writeStringValue('salesLabel1', $this->getSalesLabel1());
        $writer->writeStringValue('salesLabel2', $this->getSalesLabel2());
        $writer->writeStringValue('salesLabel3', $this->getSalesLabel3());
        $writer->writeIntegerValue('sipsState', $this->getSipsState());
        $writer->writeIntegerValue('sopCarriageDept', $this->getSopCarriageDept());
        $writer->writeFloatValue('sopCarriageNet', $this->getSopCarriageNet());
        $writer->writeStringValue('sopCarriageNomCode', $this->getSopCarriageNomCode());
        $writer->writeIntegerValue('sopCarriageTaxCode', $this->getSopCarriageTaxCode());
        $writer->writeIntegerValue('sopGlobalDept', $this->getSopGlobalDept());
        $writer->writeStringValue('sopGlobalDetails', $this->getSopGlobalDetails());
        $writer->writeStringValue('sopGlobalNomCode', $this->getSopGlobalNomCode());
        $writer->writeIntegerValue('sopGlobalTaxCode', $this->getSopGlobalTaxCode());
        $writer->writeIntegerValue('startMonth', $this->getStartMonth());
        $writer->writeStringValue('stockcategoryLabel', $this->getStockcategoryLabel());
        $writer->writeStringValue('stockDiscountA', $this->getStockDiscountA());
        $writer->writeStringValue('stockDiscountB', $this->getStockDiscountB());
        $writer->writeStringValue('stockDiscountC', $this->getStockDiscountC());
        $writer->writeStringValue('stockDiscountD', $this->getStockDiscountD());
        $writer->writeStringValue('stockDiscountE', $this->getStockDiscountE());
        $writer->writeStringValue('stockLabel1', $this->getStockLabel1());
        $writer->writeStringValue('stockLabel2', $this->getStockLabel2());
        $writer->writeStringValue('stockLabel3', $this->getStockLabel3());
        $writer->writeIntegerValue('supplierPaymentDueImmediately', $this->getSupplierPaymentDueImmediately());
        $writer->writeStringValue('telephone', $this->getTelephone());
        $writer->writeIntegerValue('unitDecimalPlace', $this->getUnitDecimalPlace());
        $writer->writeIntegerValue('vatCash', $this->getVatCash());
        $writer->writeStringValue('vatRegNumber', $this->getVatRegNumber());
        $writer->writeStringValue('webAddress', $this->getWebAddress());
    }

    /**
     * Sets the address1 property value. The address1 property
     * @param string|null $value Value to set for the address1 property.
    */
    public function setAddress1(?string $value): void {
        $this->address1 = $value;
    }

    /**
     * Sets the address2 property value. The address2 property
     * @param string|null $value Value to set for the address2 property.
    */
    public function setAddress2(?string $value): void {
        $this->address2 = $value;
    }

    /**
     * Sets the address3 property value. The address3 property
     * @param string|null $value Value to set for the address3 property.
    */
    public function setAddress3(?string $value): void {
        $this->address3 = $value;
    }

    /**
     * Sets the address4 property value. The address4 property
     * @param string|null $value Value to set for the address4 property.
    */
    public function setAddress4(?string $value): void {
        $this->address4 = $value;
    }

    /**
     * Sets the address5 property value. The address5 property
     * @param string|null $value Value to set for the address5 property.
    */
    public function setAddress5(?string $value): void {
        $this->address5 = $value;
    }

    /**
     * Sets the addressLabel1 property value. The addressLabel1 property
     * @param string|null $value Value to set for the addressLabel1 property.
    */
    public function setAddressLabel1(?string $value): void {
        $this->addressLabel1 = $value;
    }

    /**
     * Sets the addressLabel2 property value. The addressLabel2 property
     * @param string|null $value Value to set for the addressLabel2 property.
    */
    public function setAddressLabel2(?string $value): void {
        $this->addressLabel2 = $value;
    }

    /**
     * Sets the addressLabel3 property value. The addressLabel3 property
     * @param string|null $value Value to set for the addressLabel3 property.
    */
    public function setAddressLabel3(?string $value): void {
        $this->addressLabel3 = $value;
    }

    /**
     * Sets the addressLabel4 property value. The addressLabel4 property
     * @param string|null $value Value to set for the addressLabel4 property.
    */
    public function setAddressLabel4(?string $value): void {
        $this->addressLabel4 = $value;
    }

    /**
     * Sets the addressLabel5 property value. The addressLabel5 property
     * @param string|null $value Value to set for the addressLabel5 property.
    */
    public function setAddressLabel5(?string $value): void {
        $this->addressLabel5 = $value;
    }

    /**
     * Sets the allowNegativeStock property value. The allowNegativeStock property
     * @param int|null $value Value to set for the allowNegativeStock property.
    */
    public function setAllowNegativeStock(?int $value): void {
        $this->allowNegativeStock = $value;
    }

    /**
     * Sets the alwaysCreateBatchEpayment property value. The alwaysCreateBatchEpayment property
     * @param int|null $value Value to set for the alwaysCreateBatchEpayment property.
    */
    public function setAlwaysCreateBatchEpayment(?int $value): void {
        $this->alwaysCreateBatchEpayment = $value;
    }

    /**
     * Sets the budgetingMethod property value. The budgetingMethod property
     * @param int|null $value Value to set for the budgetingMethod property.
    */
    public function setBudgetingMethod(?int $value): void {
        $this->budgetingMethod = $value;
    }

    /**
     * Sets the cAddress1 property value. The cAddress1 property
     * @param string|null $value Value to set for the cAddress1 property.
    */
    public function setCAddress1(?string $value): void {
        $this->cAddress1 = $value;
    }

    /**
     * Sets the cAddress2 property value. The cAddress2 property
     * @param string|null $value Value to set for the cAddress2 property.
    */
    public function setCAddress2(?string $value): void {
        $this->cAddress2 = $value;
    }

    /**
     * Sets the cAddress3 property value. The cAddress3 property
     * @param string|null $value Value to set for the cAddress3 property.
    */
    public function setCAddress3(?string $value): void {
        $this->cAddress3 = $value;
    }

    /**
     * Sets the cAddress4 property value. The cAddress4 property
     * @param string|null $value Value to set for the cAddress4 property.
    */
    public function setCAddress4(?string $value): void {
        $this->cAddress4 = $value;
    }

    /**
     * Sets the cAddress5 property value. The cAddress5 property
     * @param string|null $value Value to set for the cAddress5 property.
    */
    public function setCAddress5(?string $value): void {
        $this->cAddress5 = $value;
    }

    /**
     * Sets the cashRegisterBank property value. The cashRegisterBank property
     * @param string|null $value Value to set for the cashRegisterBank property.
    */
    public function setCashRegisterBank(?string $value): void {
        $this->cashRegisterBank = $value;
    }

    /**
     * Sets the cashRegisterDiscrepenciesNom property value. The cashRegisterDiscrepenciesNom property
     * @param string|null $value Value to set for the cashRegisterDiscrepenciesNom property.
    */
    public function setCashRegisterDiscrepenciesNom(?string $value): void {
        $this->cashRegisterDiscrepenciesNom = $value;
    }

    /**
     * Sets the cashRegisterSalesNom property value. The cashRegisterSalesNom property
     * @param string|null $value Value to set for the cashRegisterSalesNom property.
    */
    public function setCashRegisterSalesNom(?string $value): void {
        $this->cashRegisterSalesNom = $value;
    }

    /**
     * Sets the cashRegisterTaxcode property value. The cashRegisterTaxcode property
     * @param int|null $value Value to set for the cashRegisterTaxcode property.
    */
    public function setCashRegisterTaxcode(?int $value): void {
        $this->cashRegisterTaxcode = $value;
    }

    /**
     * Sets the cashRegisterTaxInclusiveFlag property value. The cashRegisterTaxInclusiveFlag property
     * @param int|null $value Value to set for the cashRegisterTaxInclusiveFlag property.
    */
    public function setCashRegisterTaxInclusiveFlag(?int $value): void {
        $this->cashRegisterTaxInclusiveFlag = $value;
    }

    /**
     * Sets the ccLetter1 property value. The ccLetter1 property
     * @param string|null $value Value to set for the ccLetter1 property.
    */
    public function setCcLetter1(?string $value): void {
        $this->ccLetter1 = $value;
    }

    /**
     * Sets the ccLetter2 property value. The ccLetter2 property
     * @param string|null $value Value to set for the ccLetter2 property.
    */
    public function setCcLetter2(?string $value): void {
        $this->ccLetter2 = $value;
    }

    /**
     * Sets the ccLetter3 property value. The ccLetter3 property
     * @param string|null $value Value to set for the ccLetter3 property.
    */
    public function setCcLetter3(?string $value): void {
        $this->ccLetter3 = $value;
    }

    /**
     * Sets the chargeDays property value. The chargeDays property
     * @param int|null $value Value to set for the chargeDays property.
    */
    public function setChargeDays(?int $value): void {
        $this->chargeDays = $value;
    }

    /**
     * Sets the charityState property value. The charityState property
     * @param int|null $value Value to set for the charityState property.
    */
    public function setCharityState(?int $value): void {
        $this->charityState = $value;
    }

    /**
     * Sets the charitytaxref property value. The charitytaxref property
     * @param string|null $value Value to set for the charitytaxref property.
    */
    public function setCharitytaxref(?string $value): void {
        $this->charitytaxref = $value;
    }

    /**
     * Sets the clearAuditTrailDate property value. The clearAuditTrailDate property
     * @param DateTime|null $value Value to set for the clearAuditTrailDate property.
    */
    public function setClearAuditTrailDate(?DateTime $value): void {
        $this->clearAuditTrailDate = $value;
    }

    /**
     * Sets the closedLedgerCheckDate property value. The closedLedgerCheckDate property
     * @param DateTime|null $value Value to set for the closedLedgerCheckDate property.
    */
    public function setClosedLedgerCheckDate(?DateTime $value): void {
        $this->closedLedgerCheckDate = $value;
    }

    /**
     * Sets the closedLedgerDateEnabled property value. The closedLedgerDateEnabled property
     * @param int|null $value Value to set for the closedLedgerDateEnabled property.
    */
    public function setClosedLedgerDateEnabled(?int $value): void {
        $this->closedLedgerDateEnabled = $value;
    }

    /**
     * Sets the countryCode property value. The countryCode property
     * @param string|null $value Value to set for the countryCode property.
    */
    public function setCountryCode(?string $value): void {
        $this->countryCode = $value;
    }

    /**
     * Sets the countryCodeCombined property value. The countryCodeCombined property
     * @param string|null $value Value to set for the countryCodeCombined property.
    */
    public function setCountryCodeCombined(?string $value): void {
        $this->countryCodeCombined = $value;
    }

    /**
     * Sets the countryCodeConverted property value. The countryCodeConverted property
     * @param string|null $value Value to set for the countryCodeConverted property.
    */
    public function setCountryCodeConverted(?string $value): void {
        $this->countryCodeConverted = $value;
    }

    /**
     * Sets the creditRef property value. The creditRef property
     * @param string|null $value Value to set for the creditRef property.
    */
    public function setCreditRef(?string $value): void {
        $this->creditRef = $value;
    }

    /**
     * Sets the customerPaymentDueImmediately property value. The customerPaymentDueImmediately property
     * @param int|null $value Value to set for the customerPaymentDueImmediately property.
    */
    public function setCustomerPaymentDueImmediately(?int $value): void {
        $this->customerPaymentDueImmediately = $value;
    }

    /**
     * Sets the dataManagementDisabled property value. The dataManagementDisabled property
     * @param int|null $value Value to set for the dataManagementDisabled property.
    */
    public function setDataManagementDisabled(?int $value): void {
        $this->dataManagementDisabled = $value;
    }

    /**
     * Sets the defaultBudgetingChart property value. The defaultBudgetingChart property
     * @param int|null $value Value to set for the defaultBudgetingChart property.
    */
    public function setDefaultBudgetingChart(?int $value): void {
        $this->defaultBudgetingChart = $value;
    }

    /**
     * Sets the defaultChart property value. The defaultChart property
     * @param int|null $value Value to set for the defaultChart property.
    */
    public function setDefaultChart(?int $value): void {
        $this->defaultChart = $value;
    }

    /**
     * Sets the defaultCreditBureau property value. The defaultCreditBureau property
     * @param int|null $value Value to set for the defaultCreditBureau property.
    */
    public function setDefaultCreditBureau(?int $value): void {
        $this->defaultCreditBureau = $value;
    }

    /**
     * Sets the delAddress1 property value. The delAddress1 property
     * @param string|null $value Value to set for the delAddress1 property.
    */
    public function setDelAddress1(?string $value): void {
        $this->delAddress1 = $value;
    }

    /**
     * Sets the delAddress2 property value. The delAddress2 property
     * @param string|null $value Value to set for the delAddress2 property.
    */
    public function setDelAddress2(?string $value): void {
        $this->delAddress2 = $value;
    }

    /**
     * Sets the delAddress3 property value. The delAddress3 property
     * @param string|null $value Value to set for the delAddress3 property.
    */
    public function setDelAddress3(?string $value): void {
        $this->delAddress3 = $value;
    }

    /**
     * Sets the delAddress4 property value. The delAddress4 property
     * @param string|null $value Value to set for the delAddress4 property.
    */
    public function setDelAddress4(?string $value): void {
        $this->delAddress4 = $value;
    }

    /**
     * Sets the delAddress5 property value. The delAddress5 property
     * @param string|null $value Value to set for the delAddress5 property.
    */
    public function setDelAddress5(?string $value): void {
        $this->delAddress5 = $value;
    }

    /**
     * Sets the delEMail property value. The delEMail property
     * @param string|null $value Value to set for the delEMail property.
    */
    public function setDelEMail(?string $value): void {
        $this->delEMail = $value;
    }

    /**
     * Sets the delFax property value. The delFax property
     * @param string|null $value Value to set for the delFax property.
    */
    public function setDelFax(?string $value): void {
        $this->delFax = $value;
    }

    /**
     * Sets the delName property value. The delName property
     * @param string|null $value Value to set for the delName property.
    */
    public function setDelName(?string $value): void {
        $this->delName = $value;
    }

    /**
     * Sets the delTelephone property value. The delTelephone property
     * @param string|null $value Value to set for the delTelephone property.
    */
    public function setDelTelephone(?string $value): void {
        $this->delTelephone = $value;
    }

    /**
     * Sets the delWebAddress property value. The delWebAddress property
     * @param string|null $value Value to set for the delWebAddress property.
    */
    public function setDelWebAddress(?string $value): void {
        $this->delWebAddress = $value;
    }

    /**
     * Sets the departmentsLabel property value. The departmentsLabel property
     * @param string|null $value Value to set for the departmentsLabel property.
    */
    public function setDepartmentsLabel(?string $value): void {
        $this->departmentsLabel = $value;
    }

    /**
     * Sets the directDebitsEnabled property value. The directDebitsEnabled property
     * @param int|null $value Value to set for the directDebitsEnabled property.
    */
    public function setDirectDebitsEnabled(?int $value): void {
        $this->directDebitsEnabled = $value;
    }

    /**
     * Sets the directDebitsLastSyncedDate property value. The directDebitsLastSyncedDate property
     * @param DateTime|null $value Value to set for the directDebitsLastSyncedDate property.
    */
    public function setDirectDebitsLastSyncedDate(?DateTime $value): void {
        $this->directDebitsLastSyncedDate = $value;
    }

    /**
     * Sets the dunsNumber property value. The dunsNumber property
     * @param string|null $value Value to set for the dunsNumber property.
    */
    public function setDunsNumber(?string $value): void {
        $this->dunsNumber = $value;
    }

    /**
     * Sets the ebankingState property value. The ebankingState property
     * @param int|null $value Value to set for the ebankingState property.
    */
    public function setEbankingState(?int $value): void {
        $this->ebankingState = $value;
    }

    /**
     * Sets the eMail property value. The eMail property
     * @param string|null $value Value to set for the eMail property.
    */
    public function setEMail(?string $value): void {
        $this->eMail = $value;
    }

    /**
     * Sets the endOfReport property value. The endOfReport property
     * @param string|null $value Value to set for the endOfReport property.
    */
    public function setEndOfReport(?string $value): void {
        $this->endOfReport = $value;
    }

    /**
     * Sets the eoriNumber property value. The eoriNumber property
     * @param string|null $value Value to set for the eoriNumber property.
    */
    public function setEoriNumber(?string $value): void {
        $this->eoriNumber = $value;
    }

    /**
     * Sets the fax property value. The fax property
     * @param string|null $value Value to set for the fax property.
    */
    public function setFax(?string $value): void {
        $this->fax = $value;
    }

    /**
     * Sets the feesNominal property value. The feesNominal property
     * @param string|null $value Value to set for the feesNominal property.
    */
    public function setFeesNominal(?string $value): void {
        $this->feesNominal = $value;
    }

    /**
     * Sets the financialYear property value. The financialYear property
     * @param int|null $value Value to set for the financialYear property.
    */
    public function setFinancialYear(?int $value): void {
        $this->financialYear = $value;
    }

    /**
     * Sets the fixedassetsLabel property value. The fixedassetsLabel property
     * @param string|null $value Value to set for the fixedassetsLabel property.
    */
    public function setFixedassetsLabel(?string $value): void {
        $this->fixedassetsLabel = $value;
    }

    /**
     * Sets the flatRateVatPercent property value. The flatRateVatPercent property
     * @param float|null $value Value to set for the flatRateVatPercent property.
    */
    public function setFlatRateVatPercent(?float $value): void {
        $this->flatRateVatPercent = $value;
    }

    /**
     * Sets the hmrcPayeeEnabled property value. The hmrcPayeeEnabled property
     * @param int|null $value Value to set for the hmrcPayeeEnabled property.
    */
    public function setHmrcPayeeEnabled(?int $value): void {
        $this->hmrcPayeeEnabled = $value;
    }

    /**
     * Sets the hmrcPayeeStatus property value. The hmrcPayeeStatus property
     * @param int|null $value Value to set for the hmrcPayeeStatus property.
    */
    public function setHmrcPayeeStatus(?int $value): void {
        $this->hmrcPayeeStatus = $value;
    }

    /**
     * Sets the hmrcPayeeStatusDescription property value. The hmrcPayeeStatusDescription property
     * @param string|null $value Value to set for the hmrcPayeeStatusDescription property.
    */
    public function setHmrcPayeeStatusDescription(?string $value): void {
        $this->hmrcPayeeStatusDescription = $value;
    }

    /**
     * Sets the holdingAccount property value. The holdingAccount property
     * @param string|null $value Value to set for the holdingAccount property.
    */
    public function setHoldingAccount(?string $value): void {
        $this->holdingAccount = $value;
    }

    /**
     * Sets the invoiceDiscUnitPrice property value. The invoiceDiscUnitPrice property
     * @param int|null $value Value to set for the invoiceDiscUnitPrice property.
    */
    public function setInvoiceDiscUnitPrice(?int $value): void {
        $this->invoiceDiscUnitPrice = $value;
    }

    /**
     * Sets the invoicePaymentsState property value. The invoicePaymentsState property
     * @param string|null $value Value to set for the invoicePaymentsState property.
    */
    public function setInvoicePaymentsState(?string $value): void {
        $this->invoicePaymentsState = $value;
    }

    /**
     * Sets the lAddress1 property value. The lAddress1 property
     * @param string|null $value Value to set for the lAddress1 property.
    */
    public function setLAddress1(?string $value): void {
        $this->lAddress1 = $value;
    }

    /**
     * Sets the lAddress2 property value. The lAddress2 property
     * @param string|null $value Value to set for the lAddress2 property.
    */
    public function setLAddress2(?string $value): void {
        $this->lAddress2 = $value;
    }

    /**
     * Sets the lAddress3 property value. The lAddress3 property
     * @param string|null $value Value to set for the lAddress3 property.
    */
    public function setLAddress3(?string $value): void {
        $this->lAddress3 = $value;
    }

    /**
     * Sets the lAddress4 property value. The lAddress4 property
     * @param string|null $value Value to set for the lAddress4 property.
    */
    public function setLAddress4(?string $value): void {
        $this->lAddress4 = $value;
    }

    /**
     * Sets the lAddress5 property value. The lAddress5 property
     * @param string|null $value Value to set for the lAddress5 property.
    */
    public function setLAddress5(?string $value): void {
        $this->lAddress5 = $value;
    }

    /**
     * Sets the lastBackupDate property value. The lastBackupDate property
     * @param DateTime|null $value Value to set for the lastBackupDate property.
    */
    public function setLastBackupDate(?DateTime $value): void {
        $this->lastBackupDate = $value;
    }

    /**
     * Sets the lastCheckDataComments property value. The lastCheckDataComments property
     * @param int|null $value Value to set for the lastCheckDataComments property.
    */
    public function setLastCheckDataComments(?int $value): void {
        $this->lastCheckDataComments = $value;
    }

    /**
     * Sets the lastCheckDataDate property value. The lastCheckDataDate property
     * @param DateTime|null $value Value to set for the lastCheckDataDate property.
    */
    public function setLastCheckDataDate(?DateTime $value): void {
        $this->lastCheckDataDate = $value;
    }

    /**
     * Sets the lastCheckDataErrors property value. The lastCheckDataErrors property
     * @param int|null $value Value to set for the lastCheckDataErrors property.
    */
    public function setLastCheckDataErrors(?int $value): void {
        $this->lastCheckDataErrors = $value;
    }

    /**
     * Sets the lastCheckDataWarnings property value. The lastCheckDataWarnings property
     * @param int|null $value Value to set for the lastCheckDataWarnings property.
    */
    public function setLastCheckDataWarnings(?int $value): void {
        $this->lastCheckDataWarnings = $value;
    }

    /**
     * Sets the lastMonthEnd property value. The lastMonthEnd property
     * @param DateTime|null $value Value to set for the lastMonthEnd property.
    */
    public function setLastMonthEnd(?DateTime $value): void {
        $this->lastMonthEnd = $value;
    }

    /**
     * Sets the lastRestoreDate property value. The lastRestoreDate property
     * @param DateTime|null $value Value to set for the lastRestoreDate property.
    */
    public function setLastRestoreDate(?DateTime $value): void {
        $this->lastRestoreDate = $value;
    }

    /**
     * Sets the lastTransactionNumber property value. The lastTransactionNumber property
     * @param int|null $value Value to set for the lastTransactionNumber property.
    */
    public function setLastTransactionNumber(?int $value): void {
        $this->lastTransactionNumber = $value;
    }

    /**
     * Sets the lName property value. The lName property
     * @param string|null $value Value to set for the lName property.
    */
    public function setLName(?string $value): void {
        $this->lName = $value;
    }

    /**
     * Sets the mtdState property value. The mtdState property
     * @param int|null $value Value to set for the mtdState property.
    */
    public function setMtdState(?int $value): void {
        $this->mtdState = $value;
    }

    /**
     * Sets the name property value. The name property
     * @param string|null $value Value to set for the name property.
    */
    public function setName(?string $value): void {
        $this->name = $value;
    }

    /**
     * Sets the noDiscountPriceList property value. The noDiscountPriceList property
     * @param int|null $value Value to set for the noDiscountPriceList property.
    */
    public function setNoDiscountPriceList(?int $value): void {
        $this->noDiscountPriceList = $value;
    }

    /**
     * Sets the nomAccruals property value. The nomAccruals property
     * @param string|null $value Value to set for the nomAccruals property.
    */
    public function setNomAccruals(?string $value): void {
        $this->nomAccruals = $value;
    }

    /**
     * Sets the nomBadDebts property value. The nomBadDebts property
     * @param string|null $value Value to set for the nomBadDebts property.
    */
    public function setNomBadDebts(?string $value): void {
        $this->nomBadDebts = $value;
    }

    /**
     * Sets the nomCreditCharges property value. The nomCreditCharges property
     * @param string|null $value Value to set for the nomCreditCharges property.
    */
    public function setNomCreditCharges(?string $value): void {
        $this->nomCreditCharges = $value;
    }

    /**
     * Sets the nomCreditors property value. The nomCreditors property
     * @param string|null $value Value to set for the nomCreditors property.
    */
    public function setNomCreditors(?string $value): void {
        $this->nomCreditors = $value;
    }

    /**
     * Sets the nomDebtors property value. The nomDebtors property
     * @param string|null $value Value to set for the nomDebtors property.
    */
    public function setNomDebtors(?string $value): void {
        $this->nomDebtors = $value;
    }

    /**
     * Sets the nomDefaultBank property value. The nomDefaultBank property
     * @param string|null $value Value to set for the nomDefaultBank property.
    */
    public function setNomDefaultBank(?string $value): void {
        $this->nomDefaultBank = $value;
    }

    /**
     * Sets the nomDefaultSales property value. The nomDefaultSales property
     * @param string|null $value Value to set for the nomDefaultSales property.
    */
    public function setNomDefaultSales(?string $value): void {
        $this->nomDefaultSales = $value;
    }

    /**
     * Sets the nomDefrevaluation property value. The nomDefrevaluation property
     * @param string|null $value Value to set for the nomDefrevaluation property.
    */
    public function setNomDefrevaluation(?string $value): void {
        $this->nomDefrevaluation = $value;
    }

    /**
     * Sets the nomDiscountPurchase property value. The nomDiscountPurchase property
     * @param string|null $value Value to set for the nomDiscountPurchase property.
    */
    public function setNomDiscountPurchase(?string $value): void {
        $this->nomDiscountPurchase = $value;
    }

    /**
     * Sets the nomDiscountSales property value. The nomDiscountSales property
     * @param string|null $value Value to set for the nomDiscountSales property.
    */
    public function setNomDiscountSales(?string $value): void {
        $this->nomDiscountSales = $value;
    }

    /**
     * Sets the nominatedBank property value. The nominatedBank property
     * @param string|null $value Value to set for the nominatedBank property.
    */
    public function setNominatedBank(?string $value): void {
        $this->nominatedBank = $value;
    }

    /**
     * Sets the nomIoss property value. The nomIoss property
     * @param string|null $value Value to set for the nomIoss property.
    */
    public function setNomIoss(?string $value): void {
        $this->nomIoss = $value;
    }

    /**
     * Sets the nomManualadjs property value. The nomManualadjs property
     * @param string|null $value Value to set for the nomManualadjs property.
    */
    public function setNomManualadjs(?string $value): void {
        $this->nomManualadjs = $value;
    }

    /**
     * Sets the nomMispostings property value. The nomMispostings property
     * @param string|null $value Value to set for the nomMispostings property.
    */
    public function setNomMispostings(?string $value): void {
        $this->nomMispostings = $value;
    }

    /**
     * Sets the nomNonunionoss property value. The nomNonunionoss property
     * @param string|null $value Value to set for the nomNonunionoss property.
    */
    public function setNomNonunionoss(?string $value): void {
        $this->nomNonunionoss = $value;
    }

    /**
     * Sets the nomPrepayments property value. The nomPrepayments property
     * @param string|null $value Value to set for the nomPrepayments property.
    */
    public function setNomPrepayments(?string $value): void {
        $this->nomPrepayments = $value;
    }

    /**
     * Sets the nomRetainedEarnings property value. The nomRetainedEarnings property
     * @param string|null $value Value to set for the nomRetainedEarnings property.
    */
    public function setNomRetainedEarnings(?string $value): void {
        $this->nomRetainedEarnings = $value;
    }

    /**
     * Sets the nomSuspense property value. The nomSuspense property
     * @param string|null $value Value to set for the nomSuspense property.
    */
    public function setNomSuspense(?string $value): void {
        $this->nomSuspense = $value;
    }

    /**
     * Sets the nomTaxPurchase property value. The nomTaxPurchase property
     * @param string|null $value Value to set for the nomTaxPurchase property.
    */
    public function setNomTaxPurchase(?string $value): void {
        $this->nomTaxPurchase = $value;
    }

    /**
     * Sets the nomTaxSales property value. The nomTaxSales property
     * @param string|null $value Value to set for the nomTaxSales property.
    */
    public function setNomTaxSales(?string $value): void {
        $this->nomTaxSales = $value;
    }

    /**
     * Sets the nomUnionoss property value. The nomUnionoss property
     * @param string|null $value Value to set for the nomUnionoss property.
    */
    public function setNomUnionoss(?string $value): void {
        $this->nomUnionoss = $value;
    }

    /**
     * Sets the nomVatliability property value. The nomVatliability property
     * @param string|null $value Value to set for the nomVatliability property.
    */
    public function setNomVatliability(?string $value): void {
        $this->nomVatliability = $value;
    }

    /**
     * Sets the nonVatTaxCode property value. The nonVatTaxCode property
     * @param int|null $value Value to set for the nonVatTaxCode property.
    */
    public function setNonVatTaxCode(?int $value): void {
        $this->nonVatTaxCode = $value;
    }

    /**
     * Sets the numberOfTransactions property value. The numberOfTransactions property
     * @param int|null $value Value to set for the numberOfTransactions property.
    */
    public function setNumberOfTransactions(?int $value): void {
        $this->numberOfTransactions = $value;
    }

    /**
     * Sets the office365State property value. The office365State property
     * @param int|null $value Value to set for the office365State property.
    */
    public function setOffice365State(?int $value): void {
        $this->office365State = $value;
    }

    /**
     * Sets the ossiossEnabled property value. The ossiossEnabled property
     * @param int|null $value Value to set for the ossiossEnabled property.
    */
    public function setOssiossEnabled(?int $value): void {
        $this->ossiossEnabled = $value;
    }

    /**
     * Sets the paymentDueFromPurchase property value. The paymentDueFromPurchase property
     * @param int|null $value Value to set for the paymentDueFromPurchase property.
    */
    public function setPaymentDueFromPurchase(?int $value): void {
        $this->paymentDueFromPurchase = $value;
    }

    /**
     * Sets the paymentDueFromPurchaseText property value. The paymentDueFromPurchaseText property
     * @param string|null $value Value to set for the paymentDueFromPurchaseText property.
    */
    public function setPaymentDueFromPurchaseText(?string $value): void {
        $this->paymentDueFromPurchaseText = $value;
    }

    /**
     * Sets the paymentDueFromSales property value. The paymentDueFromSales property
     * @param int|null $value Value to set for the paymentDueFromSales property.
    */
    public function setPaymentDueFromSales(?int $value): void {
        $this->paymentDueFromSales = $value;
    }

    /**
     * Sets the paymentDueFromSalesText property value. The paymentDueFromSalesText property
     * @param string|null $value Value to set for the paymentDueFromSalesText property.
    */
    public function setPaymentDueFromSalesText(?string $value): void {
        $this->paymentDueFromSalesText = $value;
    }

    /**
     * Sets the paymentDuePurchase property value. The paymentDuePurchase property
     * @param int|null $value Value to set for the paymentDuePurchase property.
    */
    public function setPaymentDuePurchase(?int $value): void {
        $this->paymentDuePurchase = $value;
    }

    /**
     * Sets the paymentDueSales property value. The paymentDueSales property
     * @param int|null $value Value to set for the paymentDueSales property.
    */
    public function setPaymentDueSales(?int $value): void {
        $this->paymentDueSales = $value;
    }

    /**
     * Sets the popCarriageDept property value. The popCarriageDept property
     * @param int|null $value Value to set for the popCarriageDept property.
    */
    public function setPopCarriageDept(?int $value): void {
        $this->popCarriageDept = $value;
    }

    /**
     * Sets the popCarriageNet property value. The popCarriageNet property
     * @param float|null $value Value to set for the popCarriageNet property.
    */
    public function setPopCarriageNet(?float $value): void {
        $this->popCarriageNet = $value;
    }

    /**
     * Sets the popCarriageNomCode property value. The popCarriageNomCode property
     * @param string|null $value Value to set for the popCarriageNomCode property.
    */
    public function setPopCarriageNomCode(?string $value): void {
        $this->popCarriageNomCode = $value;
    }

    /**
     * Sets the popCarriageTaxCode property value. The popCarriageTaxCode property
     * @param int|null $value Value to set for the popCarriageTaxCode property.
    */
    public function setPopCarriageTaxCode(?int $value): void {
        $this->popCarriageTaxCode = $value;
    }

    /**
     * Sets the popGlobalDept property value. The popGlobalDept property
     * @param int|null $value Value to set for the popGlobalDept property.
    */
    public function setPopGlobalDept(?int $value): void {
        $this->popGlobalDept = $value;
    }

    /**
     * Sets the popGlobalDetails property value. The popGlobalDetails property
     * @param string|null $value Value to set for the popGlobalDetails property.
    */
    public function setPopGlobalDetails(?string $value): void {
        $this->popGlobalDetails = $value;
    }

    /**
     * Sets the popGlobalNomCode property value. The popGlobalNomCode property
     * @param string|null $value Value to set for the popGlobalNomCode property.
    */
    public function setPopGlobalNomCode(?string $value): void {
        $this->popGlobalNomCode = $value;
    }

    /**
     * Sets the popGlobalTaxCode property value. The popGlobalTaxCode property
     * @param int|null $value Value to set for the popGlobalTaxCode property.
    */
    public function setPopGlobalTaxCode(?int $value): void {
        $this->popGlobalTaxCode = $value;
    }

    /**
     * Sets the programBugfixVersion property value. The programBugfixVersion property
     * @param int|null $value Value to set for the programBugfixVersion property.
    */
    public function setProgramBugfixVersion(?int $value): void {
        $this->programBugfixVersion = $value;
    }

    /**
     * Sets the programBuildVersion property value. The programBuildVersion property
     * @param int|null $value Value to set for the programBuildVersion property.
    */
    public function setProgramBuildVersion(?int $value): void {
        $this->programBuildVersion = $value;
    }

    /**
     * Sets the programMajorVersion property value. The programMajorVersion property
     * @param int|null $value Value to set for the programMajorVersion property.
    */
    public function setProgramMajorVersion(?int $value): void {
        $this->programMajorVersion = $value;
    }

    /**
     * Sets the programMinorVersion property value. The programMinorVersion property
     * @param int|null $value Value to set for the programMinorVersion property.
    */
    public function setProgramMinorVersion(?int $value): void {
        $this->programMinorVersion = $value;
    }

    /**
     * Sets the programType property value. The programType property
     * @param string|null $value Value to set for the programType property.
    */
    public function setProgramType(?string $value): void {
        $this->programType = $value;
    }

    /**
     * Sets the protxEnabled property value. The protxEnabled property
     * @param int|null $value Value to set for the protxEnabled property.
    */
    public function setProtxEnabled(?int $value): void {
        $this->protxEnabled = $value;
    }

    /**
     * Sets the protxLastSyncDate property value. The protxLastSyncDate property
     * @param string|null $value Value to set for the protxLastSyncDate property.
    */
    public function setProtxLastSyncDate(?string $value): void {
        $this->protxLastSyncDate = $value;
    }

    /**
     * Sets the protxPassword property value. The protxPassword property
     * @param string|null $value Value to set for the protxPassword property.
    */
    public function setProtxPassword(?string $value): void {
        $this->protxPassword = $value;
    }

    /**
     * Sets the protxUserid property value. The protxUserid property
     * @param string|null $value Value to set for the protxUserid property.
    */
    public function setProtxUserid(?string $value): void {
        $this->protxUserid = $value;
    }

    /**
     * Sets the protxVendor property value. The protxVendor property
     * @param string|null $value Value to set for the protxVendor property.
    */
    public function setProtxVendor(?string $value): void {
        $this->protxVendor = $value;
    }

    /**
     * Sets the purchaseAgeByMonth property value. The purchaseAgeByMonth property
     * @param int|null $value Value to set for the purchaseAgeByMonth property.
    */
    public function setPurchaseAgeByMonth(?int $value): void {
        $this->purchaseAgeByMonth = $value;
    }

    /**
     * Sets the purchaseAgedPeriod1 property value. The purchaseAgedPeriod1 property
     * @param int|null $value Value to set for the purchaseAgedPeriod1 property.
    */
    public function setPurchaseAgedPeriod1(?int $value): void {
        $this->purchaseAgedPeriod1 = $value;
    }

    /**
     * Sets the purchaseAgedPeriod2 property value. The purchaseAgedPeriod2 property
     * @param int|null $value Value to set for the purchaseAgedPeriod2 property.
    */
    public function setPurchaseAgedPeriod2(?int $value): void {
        $this->purchaseAgedPeriod2 = $value;
    }

    /**
     * Sets the purchaseAgedPeriod3 property value. The purchaseAgedPeriod3 property
     * @param int|null $value Value to set for the purchaseAgedPeriod3 property.
    */
    public function setPurchaseAgedPeriod3(?int $value): void {
        $this->purchaseAgedPeriod3 = $value;
    }

    /**
     * Sets the purchaseAgedPeriod4 property value. The purchaseAgedPeriod4 property
     * @param int|null $value Value to set for the purchaseAgedPeriod4 property.
    */
    public function setPurchaseAgedPeriod4(?int $value): void {
        $this->purchaseAgedPeriod4 = $value;
    }

    /**
     * Sets the purchaseDefaultIncoterms property value. The purchaseDefaultIncoterms property
     * @param int|null $value Value to set for the purchaseDefaultIncoterms property.
    */
    public function setPurchaseDefaultIncoterms(?int $value): void {
        $this->purchaseDefaultIncoterms = $value;
    }

    /**
     * Sets the purchaseDefaultIncotermsText property value. The purchaseDefaultIncotermsText property
     * @param string|null $value Value to set for the purchaseDefaultIncotermsText property.
    */
    public function setPurchaseDefaultIncotermsText(?string $value): void {
        $this->purchaseDefaultIncotermsText = $value;
    }

    /**
     * Sets the purchaseLabel1 property value. The purchaseLabel1 property
     * @param string|null $value Value to set for the purchaseLabel1 property.
    */
    public function setPurchaseLabel1(?string $value): void {
        $this->purchaseLabel1 = $value;
    }

    /**
     * Sets the purchaseLabel2 property value. The purchaseLabel2 property
     * @param string|null $value Value to set for the purchaseLabel2 property.
    */
    public function setPurchaseLabel2(?string $value): void {
        $this->purchaseLabel2 = $value;
    }

    /**
     * Sets the purchaseLabel3 property value. The purchaseLabel3 property
     * @param string|null $value Value to set for the purchaseLabel3 property.
    */
    public function setPurchaseLabel3(?string $value): void {
        $this->purchaseLabel3 = $value;
    }

    /**
     * Sets the qtyDecimalPlace property value. The qtyDecimalPlace property
     * @param int|null $value Value to set for the qtyDecimalPlace property.
    */
    public function setQtyDecimalPlace(?int $value): void {
        $this->qtyDecimalPlace = $value;
    }

    /**
     * Sets the quotesToOrders property value. The quotesToOrders property
     * @param int|null $value Value to set for the quotesToOrders property.
    */
    public function setQuotesToOrders(?int $value): void {
        $this->quotesToOrders = $value;
    }

    /**
     * Sets the recordCreateDate property value. The recordCreateDate property
     * @param DateTime|null $value Value to set for the recordCreateDate property.
    */
    public function setRecordCreateDate(?DateTime $value): void {
        $this->recordCreateDate = $value;
    }

    /**
     * Sets the recordDeleted property value. The recordDeleted property
     * @param int|null $value Value to set for the recordDeleted property.
    */
    public function setRecordDeleted(?int $value): void {
        $this->recordDeleted = $value;
    }

    /**
     * Sets the recordModifyDate property value. The recordModifyDate property
     * @param DateTime|null $value Value to set for the recordModifyDate property.
    */
    public function setRecordModifyDate(?DateTime $value): void {
        $this->recordModifyDate = $value;
    }

    /**
     * Sets the salesAddDisc1 property value. The salesAddDisc1 property
     * @param float|null $value Value to set for the salesAddDisc1 property.
    */
    public function setSalesAddDisc1(?float $value): void {
        $this->salesAddDisc1 = $value;
    }

    /**
     * Sets the salesAddDisc10 property value. The salesAddDisc10 property
     * @param float|null $value Value to set for the salesAddDisc10 property.
    */
    public function setSalesAddDisc10(?float $value): void {
        $this->salesAddDisc10 = $value;
    }

    /**
     * Sets the salesAddDisc2 property value. The salesAddDisc2 property
     * @param float|null $value Value to set for the salesAddDisc2 property.
    */
    public function setSalesAddDisc2(?float $value): void {
        $this->salesAddDisc2 = $value;
    }

    /**
     * Sets the salesAddDisc3 property value. The salesAddDisc3 property
     * @param float|null $value Value to set for the salesAddDisc3 property.
    */
    public function setSalesAddDisc3(?float $value): void {
        $this->salesAddDisc3 = $value;
    }

    /**
     * Sets the salesAddDisc4 property value. The salesAddDisc4 property
     * @param float|null $value Value to set for the salesAddDisc4 property.
    */
    public function setSalesAddDisc4(?float $value): void {
        $this->salesAddDisc4 = $value;
    }

    /**
     * Sets the salesAddDisc5 property value. The salesAddDisc5 property
     * @param float|null $value Value to set for the salesAddDisc5 property.
    */
    public function setSalesAddDisc5(?float $value): void {
        $this->salesAddDisc5 = $value;
    }

    /**
     * Sets the salesAddDisc6 property value. The salesAddDisc6 property
     * @param float|null $value Value to set for the salesAddDisc6 property.
    */
    public function setSalesAddDisc6(?float $value): void {
        $this->salesAddDisc6 = $value;
    }

    /**
     * Sets the salesAddDisc7 property value. The salesAddDisc7 property
     * @param float|null $value Value to set for the salesAddDisc7 property.
    */
    public function setSalesAddDisc7(?float $value): void {
        $this->salesAddDisc7 = $value;
    }

    /**
     * Sets the salesAddDisc8 property value. The salesAddDisc8 property
     * @param float|null $value Value to set for the salesAddDisc8 property.
    */
    public function setSalesAddDisc8(?float $value): void {
        $this->salesAddDisc8 = $value;
    }

    /**
     * Sets the salesAddDisc9 property value. The salesAddDisc9 property
     * @param float|null $value Value to set for the salesAddDisc9 property.
    */
    public function setSalesAddDisc9(?float $value): void {
        $this->salesAddDisc9 = $value;
    }

    /**
     * Sets the salesAgeByMonth property value. The salesAgeByMonth property
     * @param int|null $value Value to set for the salesAgeByMonth property.
    */
    public function setSalesAgeByMonth(?int $value): void {
        $this->salesAgeByMonth = $value;
    }

    /**
     * Sets the salesAgedPeriod1 property value. The salesAgedPeriod1 property
     * @param int|null $value Value to set for the salesAgedPeriod1 property.
    */
    public function setSalesAgedPeriod1(?int $value): void {
        $this->salesAgedPeriod1 = $value;
    }

    /**
     * Sets the salesAgedPeriod2 property value. The salesAgedPeriod2 property
     * @param int|null $value Value to set for the salesAgedPeriod2 property.
    */
    public function setSalesAgedPeriod2(?int $value): void {
        $this->salesAgedPeriod2 = $value;
    }

    /**
     * Sets the salesAgedPeriod3 property value. The salesAgedPeriod3 property
     * @param int|null $value Value to set for the salesAgedPeriod3 property.
    */
    public function setSalesAgedPeriod3(?int $value): void {
        $this->salesAgedPeriod3 = $value;
    }

    /**
     * Sets the salesAgedPeriod4 property value. The salesAgedPeriod4 property
     * @param int|null $value Value to set for the salesAgedPeriod4 property.
    */
    public function setSalesAgedPeriod4(?int $value): void {
        $this->salesAgedPeriod4 = $value;
    }

    /**
     * Sets the salesAmountSold1 property value. The salesAmountSold1 property
     * @param float|null $value Value to set for the salesAmountSold1 property.
    */
    public function setSalesAmountSold1(?float $value): void {
        $this->salesAmountSold1 = $value;
    }

    /**
     * Sets the salesAmountSold10 property value. The salesAmountSold10 property
     * @param float|null $value Value to set for the salesAmountSold10 property.
    */
    public function setSalesAmountSold10(?float $value): void {
        $this->salesAmountSold10 = $value;
    }

    /**
     * Sets the salesAmountSold2 property value. The salesAmountSold2 property
     * @param float|null $value Value to set for the salesAmountSold2 property.
    */
    public function setSalesAmountSold2(?float $value): void {
        $this->salesAmountSold2 = $value;
    }

    /**
     * Sets the salesAmountSold3 property value. The salesAmountSold3 property
     * @param float|null $value Value to set for the salesAmountSold3 property.
    */
    public function setSalesAmountSold3(?float $value): void {
        $this->salesAmountSold3 = $value;
    }

    /**
     * Sets the salesAmountSold4 property value. The salesAmountSold4 property
     * @param float|null $value Value to set for the salesAmountSold4 property.
    */
    public function setSalesAmountSold4(?float $value): void {
        $this->salesAmountSold4 = $value;
    }

    /**
     * Sets the salesAmountSold5 property value. The salesAmountSold5 property
     * @param float|null $value Value to set for the salesAmountSold5 property.
    */
    public function setSalesAmountSold5(?float $value): void {
        $this->salesAmountSold5 = $value;
    }

    /**
     * Sets the salesAmountSold6 property value. The salesAmountSold6 property
     * @param float|null $value Value to set for the salesAmountSold6 property.
    */
    public function setSalesAmountSold6(?float $value): void {
        $this->salesAmountSold6 = $value;
    }

    /**
     * Sets the salesAmountSold7 property value. The salesAmountSold7 property
     * @param float|null $value Value to set for the salesAmountSold7 property.
    */
    public function setSalesAmountSold7(?float $value): void {
        $this->salesAmountSold7 = $value;
    }

    /**
     * Sets the salesAmountSold8 property value. The salesAmountSold8 property
     * @param float|null $value Value to set for the salesAmountSold8 property.
    */
    public function setSalesAmountSold8(?float $value): void {
        $this->salesAmountSold8 = $value;
    }

    /**
     * Sets the salesAmountSold9 property value. The salesAmountSold9 property
     * @param float|null $value Value to set for the salesAmountSold9 property.
    */
    public function setSalesAmountSold9(?float $value): void {
        $this->salesAmountSold9 = $value;
    }

    /**
     * Sets the salesDefaultIncoterms property value. The salesDefaultIncoterms property
     * @param int|null $value Value to set for the salesDefaultIncoterms property.
    */
    public function setSalesDefaultIncoterms(?int $value): void {
        $this->salesDefaultIncoterms = $value;
    }

    /**
     * Sets the salesDefaultIncotermsText property value. The salesDefaultIncotermsText property
     * @param string|null $value Value to set for the salesDefaultIncotermsText property.
    */
    public function setSalesDefaultIncotermsText(?string $value): void {
        $this->salesDefaultIncotermsText = $value;
    }

    /**
     * Sets the salesLabel1 property value. The salesLabel1 property
     * @param string|null $value Value to set for the salesLabel1 property.
    */
    public function setSalesLabel1(?string $value): void {
        $this->salesLabel1 = $value;
    }

    /**
     * Sets the salesLabel2 property value. The salesLabel2 property
     * @param string|null $value Value to set for the salesLabel2 property.
    */
    public function setSalesLabel2(?string $value): void {
        $this->salesLabel2 = $value;
    }

    /**
     * Sets the salesLabel3 property value. The salesLabel3 property
     * @param string|null $value Value to set for the salesLabel3 property.
    */
    public function setSalesLabel3(?string $value): void {
        $this->salesLabel3 = $value;
    }

    /**
     * Sets the sipsState property value. The sipsState property
     * @param int|null $value Value to set for the sipsState property.
    */
    public function setSipsState(?int $value): void {
        $this->sipsState = $value;
    }

    /**
     * Sets the sopCarriageDept property value. The sopCarriageDept property
     * @param int|null $value Value to set for the sopCarriageDept property.
    */
    public function setSopCarriageDept(?int $value): void {
        $this->sopCarriageDept = $value;
    }

    /**
     * Sets the sopCarriageNet property value. The sopCarriageNet property
     * @param float|null $value Value to set for the sopCarriageNet property.
    */
    public function setSopCarriageNet(?float $value): void {
        $this->sopCarriageNet = $value;
    }

    /**
     * Sets the sopCarriageNomCode property value. The sopCarriageNomCode property
     * @param string|null $value Value to set for the sopCarriageNomCode property.
    */
    public function setSopCarriageNomCode(?string $value): void {
        $this->sopCarriageNomCode = $value;
    }

    /**
     * Sets the sopCarriageTaxCode property value. The sopCarriageTaxCode property
     * @param int|null $value Value to set for the sopCarriageTaxCode property.
    */
    public function setSopCarriageTaxCode(?int $value): void {
        $this->sopCarriageTaxCode = $value;
    }

    /**
     * Sets the sopGlobalDept property value. The sopGlobalDept property
     * @param int|null $value Value to set for the sopGlobalDept property.
    */
    public function setSopGlobalDept(?int $value): void {
        $this->sopGlobalDept = $value;
    }

    /**
     * Sets the sopGlobalDetails property value. The sopGlobalDetails property
     * @param string|null $value Value to set for the sopGlobalDetails property.
    */
    public function setSopGlobalDetails(?string $value): void {
        $this->sopGlobalDetails = $value;
    }

    /**
     * Sets the sopGlobalNomCode property value. The sopGlobalNomCode property
     * @param string|null $value Value to set for the sopGlobalNomCode property.
    */
    public function setSopGlobalNomCode(?string $value): void {
        $this->sopGlobalNomCode = $value;
    }

    /**
     * Sets the sopGlobalTaxCode property value. The sopGlobalTaxCode property
     * @param int|null $value Value to set for the sopGlobalTaxCode property.
    */
    public function setSopGlobalTaxCode(?int $value): void {
        $this->sopGlobalTaxCode = $value;
    }

    /**
     * Sets the startMonth property value. The startMonth property
     * @param int|null $value Value to set for the startMonth property.
    */
    public function setStartMonth(?int $value): void {
        $this->startMonth = $value;
    }

    /**
     * Sets the stockcategoryLabel property value. The stockcategoryLabel property
     * @param string|null $value Value to set for the stockcategoryLabel property.
    */
    public function setStockcategoryLabel(?string $value): void {
        $this->stockcategoryLabel = $value;
    }

    /**
     * Sets the stockDiscountA property value. The stockDiscountA property
     * @param string|null $value Value to set for the stockDiscountA property.
    */
    public function setStockDiscountA(?string $value): void {
        $this->stockDiscountA = $value;
    }

    /**
     * Sets the stockDiscountB property value. The stockDiscountB property
     * @param string|null $value Value to set for the stockDiscountB property.
    */
    public function setStockDiscountB(?string $value): void {
        $this->stockDiscountB = $value;
    }

    /**
     * Sets the stockDiscountC property value. The stockDiscountC property
     * @param string|null $value Value to set for the stockDiscountC property.
    */
    public function setStockDiscountC(?string $value): void {
        $this->stockDiscountC = $value;
    }

    /**
     * Sets the stockDiscountD property value. The stockDiscountD property
     * @param string|null $value Value to set for the stockDiscountD property.
    */
    public function setStockDiscountD(?string $value): void {
        $this->stockDiscountD = $value;
    }

    /**
     * Sets the stockDiscountE property value. The stockDiscountE property
     * @param string|null $value Value to set for the stockDiscountE property.
    */
    public function setStockDiscountE(?string $value): void {
        $this->stockDiscountE = $value;
    }

    /**
     * Sets the stockLabel1 property value. The stockLabel1 property
     * @param string|null $value Value to set for the stockLabel1 property.
    */
    public function setStockLabel1(?string $value): void {
        $this->stockLabel1 = $value;
    }

    /**
     * Sets the stockLabel2 property value. The stockLabel2 property
     * @param string|null $value Value to set for the stockLabel2 property.
    */
    public function setStockLabel2(?string $value): void {
        $this->stockLabel2 = $value;
    }

    /**
     * Sets the stockLabel3 property value. The stockLabel3 property
     * @param string|null $value Value to set for the stockLabel3 property.
    */
    public function setStockLabel3(?string $value): void {
        $this->stockLabel3 = $value;
    }

    /**
     * Sets the supplierPaymentDueImmediately property value. The supplierPaymentDueImmediately property
     * @param int|null $value Value to set for the supplierPaymentDueImmediately property.
    */
    public function setSupplierPaymentDueImmediately(?int $value): void {
        $this->supplierPaymentDueImmediately = $value;
    }

    /**
     * Sets the telephone property value. The telephone property
     * @param string|null $value Value to set for the telephone property.
    */
    public function setTelephone(?string $value): void {
        $this->telephone = $value;
    }

    /**
     * Sets the unitDecimalPlace property value. The unitDecimalPlace property
     * @param int|null $value Value to set for the unitDecimalPlace property.
    */
    public function setUnitDecimalPlace(?int $value): void {
        $this->unitDecimalPlace = $value;
    }

    /**
     * Sets the vatCash property value. The vatCash property
     * @param int|null $value Value to set for the vatCash property.
    */
    public function setVatCash(?int $value): void {
        $this->vatCash = $value;
    }

    /**
     * Sets the vatRegNumber property value. The vatRegNumber property
     * @param string|null $value Value to set for the vatRegNumber property.
    */
    public function setVatRegNumber(?string $value): void {
        $this->vatRegNumber = $value;
    }

    /**
     * Sets the webAddress property value. The webAddress property
     * @param string|null $value Value to set for the webAddress property.
    */
    public function setWebAddress(?string $value): void {
        $this->webAddress = $value;
    }

}
