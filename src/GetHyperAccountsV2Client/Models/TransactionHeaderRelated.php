<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class TransactionHeaderRelated implements Parsable
{
    /**
     * @var int|null $headerNumber The headerNumber property
    */
    private ?int $headerNumber = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return TransactionHeaderRelated
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): TransactionHeaderRelated {
        return new TransactionHeaderRelated();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'headerNumber' => fn(ParseNode $n) => $o->setHeaderNumber($n->getIntegerValue()),
        ];
    }

    /**
     * Gets the headerNumber property value. The headerNumber property
     * @return int|null
    */
    public function getHeaderNumber(): ?int {
        return $this->headerNumber;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeIntegerValue('headerNumber', $this->getHeaderNumber());
    }

    /**
     * Sets the headerNumber property value. The headerNumber property
     * @param int|null $value Value to set for the headerNumber property.
    */
    public function setHeaderNumber(?int $value): void {
        $this->headerNumber = $value;
    }

}
