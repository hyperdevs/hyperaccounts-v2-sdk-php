<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class SalesInvoicePostDto implements Parsable
{
    /**
     * @var SalesInvoiceAttributesWrite|null $attributes The attributes property
    */
    private ?SalesInvoiceAttributesWrite $attributes = null;

    /**
     * @var int|null $invoiceNumber The invoiceNumber property
    */
    private ?int $invoiceNumber = null;

    /**
     * @var SalesInvoiceRelationshipsWrite|null $relationships The relationships property
    */
    private ?SalesInvoiceRelationshipsWrite $relationships = null;

    /**
     * @var SalesInvoiceReliantChilds|null $reliantChilds The reliantChilds property
    */
    private ?SalesInvoiceReliantChilds $reliantChilds = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return SalesInvoicePostDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): SalesInvoicePostDto {
        return new SalesInvoicePostDto();
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return SalesInvoiceAttributesWrite|null
    */
    public function getAttributes(): ?SalesInvoiceAttributesWrite {
        return $this->attributes;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([SalesInvoiceAttributesWrite::class, 'createFromDiscriminatorValue'])),
            'invoiceNumber' => fn(ParseNode $n) => $o->setInvoiceNumber($n->getIntegerValue()),
            'relationships' => fn(ParseNode $n) => $o->setRelationships($n->getObjectValue([SalesInvoiceRelationshipsWrite::class, 'createFromDiscriminatorValue'])),
            'reliantChilds' => fn(ParseNode $n) => $o->setReliantChilds($n->getObjectValue([SalesInvoiceReliantChilds::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the invoiceNumber property value. The invoiceNumber property
     * @return int|null
    */
    public function getInvoiceNumber(): ?int {
        return $this->invoiceNumber;
    }

    /**
     * Gets the relationships property value. The relationships property
     * @return SalesInvoiceRelationshipsWrite|null
    */
    public function getRelationships(): ?SalesInvoiceRelationshipsWrite {
        return $this->relationships;
    }

    /**
     * Gets the reliantChilds property value. The reliantChilds property
     * @return SalesInvoiceReliantChilds|null
    */
    public function getReliantChilds(): ?SalesInvoiceReliantChilds {
        return $this->reliantChilds;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('attributes', $this->getAttributes());
        $writer->writeIntegerValue('invoiceNumber', $this->getInvoiceNumber());
        $writer->writeObjectValue('relationships', $this->getRelationships());
        $writer->writeObjectValue('reliantChilds', $this->getReliantChilds());
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param SalesInvoiceAttributesWrite|null $value Value to set for the attributes property.
    */
    public function setAttributes(?SalesInvoiceAttributesWrite $value): void {
        $this->attributes = $value;
    }

    /**
     * Sets the invoiceNumber property value. The invoiceNumber property
     * @param int|null $value Value to set for the invoiceNumber property.
    */
    public function setInvoiceNumber(?int $value): void {
        $this->invoiceNumber = $value;
    }

    /**
     * Sets the relationships property value. The relationships property
     * @param SalesInvoiceRelationshipsWrite|null $value Value to set for the relationships property.
    */
    public function setRelationships(?SalesInvoiceRelationshipsWrite $value): void {
        $this->relationships = $value;
    }

    /**
     * Sets the reliantChilds property value. The reliantChilds property
     * @param SalesInvoiceReliantChilds|null $value Value to set for the reliantChilds property.
    */
    public function setReliantChilds(?SalesInvoiceReliantChilds $value): void {
        $this->reliantChilds = $value;
    }

}
