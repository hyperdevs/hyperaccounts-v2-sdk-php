<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class TransactionSplitRelationships implements Parsable
{
    /**
     * @var TransactionHeaderRelatedRelationship|null $header The header property
    */
    private ?TransactionHeaderRelatedRelationship $header = null;

    /**
     * @var NominalRelatedRelationship|null $nominalCode The nominalCode property
    */
    private ?NominalRelatedRelationship $nominalCode = null;

    /**
     * @var ProjectRelatedRelationship|null $project The project property
    */
    private ?ProjectRelatedRelationship $project = null;

    /**
     * @var ProjectCostCodeRelatedRelationship|null $projectCostCode The projectCostCode property
    */
    private ?ProjectCostCodeRelatedRelationship $projectCostCode = null;

    /**
     * @var TransactionUsageRelatedListRelationship|null $usages The usages property
    */
    private ?TransactionUsageRelatedListRelationship $usages = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return TransactionSplitRelationships
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): TransactionSplitRelationships {
        return new TransactionSplitRelationships();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'header' => fn(ParseNode $n) => $o->setHeader($n->getObjectValue([TransactionHeaderRelatedRelationship::class, 'createFromDiscriminatorValue'])),
            'nominalCode' => fn(ParseNode $n) => $o->setNominalCode($n->getObjectValue([NominalRelatedRelationship::class, 'createFromDiscriminatorValue'])),
            'project' => fn(ParseNode $n) => $o->setProject($n->getObjectValue([ProjectRelatedRelationship::class, 'createFromDiscriminatorValue'])),
            'projectCostCode' => fn(ParseNode $n) => $o->setProjectCostCode($n->getObjectValue([ProjectCostCodeRelatedRelationship::class, 'createFromDiscriminatorValue'])),
            'usages' => fn(ParseNode $n) => $o->setUsages($n->getObjectValue([TransactionUsageRelatedListRelationship::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the header property value. The header property
     * @return TransactionHeaderRelatedRelationship|null
    */
    public function getHeader(): ?TransactionHeaderRelatedRelationship {
        return $this->header;
    }

    /**
     * Gets the nominalCode property value. The nominalCode property
     * @return NominalRelatedRelationship|null
    */
    public function getNominalCode(): ?NominalRelatedRelationship {
        return $this->nominalCode;
    }

    /**
     * Gets the project property value. The project property
     * @return ProjectRelatedRelationship|null
    */
    public function getProject(): ?ProjectRelatedRelationship {
        return $this->project;
    }

    /**
     * Gets the projectCostCode property value. The projectCostCode property
     * @return ProjectCostCodeRelatedRelationship|null
    */
    public function getProjectCostCode(): ?ProjectCostCodeRelatedRelationship {
        return $this->projectCostCode;
    }

    /**
     * Gets the usages property value. The usages property
     * @return TransactionUsageRelatedListRelationship|null
    */
    public function getUsages(): ?TransactionUsageRelatedListRelationship {
        return $this->usages;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('header', $this->getHeader());
        $writer->writeObjectValue('nominalCode', $this->getNominalCode());
        $writer->writeObjectValue('project', $this->getProject());
        $writer->writeObjectValue('projectCostCode', $this->getProjectCostCode());
        $writer->writeObjectValue('usages', $this->getUsages());
    }

    /**
     * Sets the header property value. The header property
     * @param TransactionHeaderRelatedRelationship|null $value Value to set for the header property.
    */
    public function setHeader(?TransactionHeaderRelatedRelationship $value): void {
        $this->header = $value;
    }

    /**
     * Sets the nominalCode property value. The nominalCode property
     * @param NominalRelatedRelationship|null $value Value to set for the nominalCode property.
    */
    public function setNominalCode(?NominalRelatedRelationship $value): void {
        $this->nominalCode = $value;
    }

    /**
     * Sets the project property value. The project property
     * @param ProjectRelatedRelationship|null $value Value to set for the project property.
    */
    public function setProject(?ProjectRelatedRelationship $value): void {
        $this->project = $value;
    }

    /**
     * Sets the projectCostCode property value. The projectCostCode property
     * @param ProjectCostCodeRelatedRelationship|null $value Value to set for the projectCostCode property.
    */
    public function setProjectCostCode(?ProjectCostCodeRelatedRelationship $value): void {
        $this->projectCostCode = $value;
    }

    /**
     * Sets the usages property value. The usages property
     * @param TransactionUsageRelatedListRelationship|null $value Value to set for the usages property.
    */
    public function setUsages(?TransactionUsageRelatedListRelationship $value): void {
        $this->usages = $value;
    }

}
