<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class TransactionSplitIncluded implements Parsable
{
    /**
     * @var TransactionHeaderGetDto|null $header The header property
    */
    private ?TransactionHeaderGetDto $header = null;

    /**
     * @var NominalGetDto|null $nominalCode The nominalCode property
    */
    private ?NominalGetDto $nominalCode = null;

    /**
     * @var ProjectGetDto|null $project The project property
    */
    private ?ProjectGetDto $project = null;

    /**
     * @var ProjectCostCodeGetDto|null $projectCostCode The projectCostCode property
    */
    private ?ProjectCostCodeGetDto $projectCostCode = null;

    /**
     * @var TransactionUsageCollection|null $usages The usages property
    */
    private ?TransactionUsageCollection $usages = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return TransactionSplitIncluded
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): TransactionSplitIncluded {
        return new TransactionSplitIncluded();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'header' => fn(ParseNode $n) => $o->setHeader($n->getObjectValue([TransactionHeaderGetDto::class, 'createFromDiscriminatorValue'])),
            'nominalCode' => fn(ParseNode $n) => $o->setNominalCode($n->getObjectValue([NominalGetDto::class, 'createFromDiscriminatorValue'])),
            'project' => fn(ParseNode $n) => $o->setProject($n->getObjectValue([ProjectGetDto::class, 'createFromDiscriminatorValue'])),
            'projectCostCode' => fn(ParseNode $n) => $o->setProjectCostCode($n->getObjectValue([ProjectCostCodeGetDto::class, 'createFromDiscriminatorValue'])),
            'usages' => fn(ParseNode $n) => $o->setUsages($n->getObjectValue([TransactionUsageCollection::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the header property value. The header property
     * @return TransactionHeaderGetDto|null
    */
    public function getHeader(): ?TransactionHeaderGetDto {
        return $this->header;
    }

    /**
     * Gets the nominalCode property value. The nominalCode property
     * @return NominalGetDto|null
    */
    public function getNominalCode(): ?NominalGetDto {
        return $this->nominalCode;
    }

    /**
     * Gets the project property value. The project property
     * @return ProjectGetDto|null
    */
    public function getProject(): ?ProjectGetDto {
        return $this->project;
    }

    /**
     * Gets the projectCostCode property value. The projectCostCode property
     * @return ProjectCostCodeGetDto|null
    */
    public function getProjectCostCode(): ?ProjectCostCodeGetDto {
        return $this->projectCostCode;
    }

    /**
     * Gets the usages property value. The usages property
     * @return TransactionUsageCollection|null
    */
    public function getUsages(): ?TransactionUsageCollection {
        return $this->usages;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('header', $this->getHeader());
        $writer->writeObjectValue('nominalCode', $this->getNominalCode());
        $writer->writeObjectValue('project', $this->getProject());
        $writer->writeObjectValue('projectCostCode', $this->getProjectCostCode());
        $writer->writeObjectValue('usages', $this->getUsages());
    }

    /**
     * Sets the header property value. The header property
     * @param TransactionHeaderGetDto|null $value Value to set for the header property.
    */
    public function setHeader(?TransactionHeaderGetDto $value): void {
        $this->header = $value;
    }

    /**
     * Sets the nominalCode property value. The nominalCode property
     * @param NominalGetDto|null $value Value to set for the nominalCode property.
    */
    public function setNominalCode(?NominalGetDto $value): void {
        $this->nominalCode = $value;
    }

    /**
     * Sets the project property value. The project property
     * @param ProjectGetDto|null $value Value to set for the project property.
    */
    public function setProject(?ProjectGetDto $value): void {
        $this->project = $value;
    }

    /**
     * Sets the projectCostCode property value. The projectCostCode property
     * @param ProjectCostCodeGetDto|null $value Value to set for the projectCostCode property.
    */
    public function setProjectCostCode(?ProjectCostCodeGetDto $value): void {
        $this->projectCostCode = $value;
    }

    /**
     * Sets the usages property value. The usages property
     * @param TransactionUsageCollection|null $value Value to set for the usages property.
    */
    public function setUsages(?TransactionUsageCollection $value): void {
        $this->usages = $value;
    }

}
