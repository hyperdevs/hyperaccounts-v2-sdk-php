<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class SageApplicationConfigOutcome implements Parsable
{
    /**
     * @var string|null $readError The readError property
    */
    private ?string $readError = null;

    /**
     * @var bool|null $readSuccess The readSuccess property
    */
    private ?bool $readSuccess = null;

    /**
     * @var bool|null $success The success property
    */
    private ?bool $success = null;

    /**
     * @var string|null $writeError The writeError property
    */
    private ?string $writeError = null;

    /**
     * @var bool|null $writeSuccess The writeSuccess property
    */
    private ?bool $writeSuccess = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return SageApplicationConfigOutcome
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): SageApplicationConfigOutcome {
        return new SageApplicationConfigOutcome();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'readError' => fn(ParseNode $n) => $o->setReadError($n->getStringValue()),
            'readSuccess' => fn(ParseNode $n) => $o->setReadSuccess($n->getBooleanValue()),
            'success' => fn(ParseNode $n) => $o->setSuccess($n->getBooleanValue()),
            'writeError' => fn(ParseNode $n) => $o->setWriteError($n->getStringValue()),
            'writeSuccess' => fn(ParseNode $n) => $o->setWriteSuccess($n->getBooleanValue()),
        ];
    }

    /**
     * Gets the readError property value. The readError property
     * @return string|null
    */
    public function getReadError(): ?string {
        return $this->readError;
    }

    /**
     * Gets the readSuccess property value. The readSuccess property
     * @return bool|null
    */
    public function getReadSuccess(): ?bool {
        return $this->readSuccess;
    }

    /**
     * Gets the success property value. The success property
     * @return bool|null
    */
    public function getSuccess(): ?bool {
        return $this->success;
    }

    /**
     * Gets the writeError property value. The writeError property
     * @return string|null
    */
    public function getWriteError(): ?string {
        return $this->writeError;
    }

    /**
     * Gets the writeSuccess property value. The writeSuccess property
     * @return bool|null
    */
    public function getWriteSuccess(): ?bool {
        return $this->writeSuccess;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
    }

    /**
     * Sets the readError property value. The readError property
     * @param string|null $value Value to set for the readError property.
    */
    public function setReadError(?string $value): void {
        $this->readError = $value;
    }

    /**
     * Sets the readSuccess property value. The readSuccess property
     * @param bool|null $value Value to set for the readSuccess property.
    */
    public function setReadSuccess(?bool $value): void {
        $this->readSuccess = $value;
    }

    /**
     * Sets the success property value. The success property
     * @param bool|null $value Value to set for the success property.
    */
    public function setSuccess(?bool $value): void {
        $this->success = $value;
    }

    /**
     * Sets the writeError property value. The writeError property
     * @param string|null $value Value to set for the writeError property.
    */
    public function setWriteError(?string $value): void {
        $this->writeError = $value;
    }

    /**
     * Sets the writeSuccess property value. The writeSuccess property
     * @param bool|null $value Value to set for the writeSuccess property.
    */
    public function setWriteSuccess(?bool $value): void {
        $this->writeSuccess = $value;
    }

}
