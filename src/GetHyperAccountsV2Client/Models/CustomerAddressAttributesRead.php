<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use DateTime;
use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class CustomerAddressAttributesRead implements Parsable
{
    /**
     * @var string|null $address1 The address1 property
    */
    private ?string $address1 = null;

    /**
     * @var string|null $address2 The address2 property
    */
    private ?string $address2 = null;

    /**
     * @var string|null $address3 The address3 property
    */
    private ?string $address3 = null;

    /**
     * @var string|null $address4 The address4 property
    */
    private ?string $address4 = null;

    /**
     * @var string|null $address5 The address5 property
    */
    private ?string $address5 = null;

    /**
     * @var int|null $addressRef The addressRef property
    */
    private ?int $addressRef = null;

    /**
     * @var int|null $addressType The addressType property
    */
    private ?int $addressType = null;

    /**
     * @var string|null $addressTypeName The addressTypeName property
    */
    private ?string $addressTypeName = null;

    /**
     * @var string|null $contactName The contactName property
    */
    private ?string $contactName = null;

    /**
     * @var string|null $contactRole The contactRole property
    */
    private ?string $contactRole = null;

    /**
     * @var string|null $countryCode The countryCode property
    */
    private ?string $countryCode = null;

    /**
     * @var string|null $description The description property
    */
    private ?string $description = null;

    /**
     * @var string|null $eMail The eMail property
    */
    private ?string $eMail = null;

    /**
     * @var string|null $fax The fax property
    */
    private ?string $fax = null;

    /**
     * @var string|null $name The name property
    */
    private ?string $name = null;

    /**
     * @var string|null $notes The notes property
    */
    private ?string $notes = null;

    /**
     * @var DateTime|null $recordCreateDate The recordCreateDate property
    */
    private ?DateTime $recordCreateDate = null;

    /**
     * @var int|null $recordDeleted The recordDeleted property
    */
    private ?int $recordDeleted = null;

    /**
     * @var DateTime|null $recordModifyDate The recordModifyDate property
    */
    private ?DateTime $recordModifyDate = null;

    /**
     * @var int|null $taxCode The taxCode property
    */
    private ?int $taxCode = null;

    /**
     * @var string|null $telephone The telephone property
    */
    private ?string $telephone = null;

    /**
     * @var string|null $telephone2 The telephone2 property
    */
    private ?string $telephone2 = null;

    /**
     * @var string|null $vatRegNumber The vatRegNumber property
    */
    private ?string $vatRegNumber = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return CustomerAddressAttributesRead
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): CustomerAddressAttributesRead {
        return new CustomerAddressAttributesRead();
    }

    /**
     * Gets the address1 property value. The address1 property
     * @return string|null
    */
    public function getAddress1(): ?string {
        return $this->address1;
    }

    /**
     * Gets the address2 property value. The address2 property
     * @return string|null
    */
    public function getAddress2(): ?string {
        return $this->address2;
    }

    /**
     * Gets the address3 property value. The address3 property
     * @return string|null
    */
    public function getAddress3(): ?string {
        return $this->address3;
    }

    /**
     * Gets the address4 property value. The address4 property
     * @return string|null
    */
    public function getAddress4(): ?string {
        return $this->address4;
    }

    /**
     * Gets the address5 property value. The address5 property
     * @return string|null
    */
    public function getAddress5(): ?string {
        return $this->address5;
    }

    /**
     * Gets the addressRef property value. The addressRef property
     * @return int|null
    */
    public function getAddressRef(): ?int {
        return $this->addressRef;
    }

    /**
     * Gets the addressType property value. The addressType property
     * @return int|null
    */
    public function getAddressType(): ?int {
        return $this->addressType;
    }

    /**
     * Gets the addressTypeName property value. The addressTypeName property
     * @return string|null
    */
    public function getAddressTypeName(): ?string {
        return $this->addressTypeName;
    }

    /**
     * Gets the contactName property value. The contactName property
     * @return string|null
    */
    public function getContactName(): ?string {
        return $this->contactName;
    }

    /**
     * Gets the contactRole property value. The contactRole property
     * @return string|null
    */
    public function getContactRole(): ?string {
        return $this->contactRole;
    }

    /**
     * Gets the countryCode property value. The countryCode property
     * @return string|null
    */
    public function getCountryCode(): ?string {
        return $this->countryCode;
    }

    /**
     * Gets the description property value. The description property
     * @return string|null
    */
    public function getDescription(): ?string {
        return $this->description;
    }

    /**
     * Gets the eMail property value. The eMail property
     * @return string|null
    */
    public function getEMail(): ?string {
        return $this->eMail;
    }

    /**
     * Gets the fax property value. The fax property
     * @return string|null
    */
    public function getFax(): ?string {
        return $this->fax;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'address1' => fn(ParseNode $n) => $o->setAddress1($n->getStringValue()),
            'address2' => fn(ParseNode $n) => $o->setAddress2($n->getStringValue()),
            'address3' => fn(ParseNode $n) => $o->setAddress3($n->getStringValue()),
            'address4' => fn(ParseNode $n) => $o->setAddress4($n->getStringValue()),
            'address5' => fn(ParseNode $n) => $o->setAddress5($n->getStringValue()),
            'addressRef' => fn(ParseNode $n) => $o->setAddressRef($n->getIntegerValue()),
            'addressType' => fn(ParseNode $n) => $o->setAddressType($n->getIntegerValue()),
            'addressTypeName' => fn(ParseNode $n) => $o->setAddressTypeName($n->getStringValue()),
            'contactName' => fn(ParseNode $n) => $o->setContactName($n->getStringValue()),
            'contactRole' => fn(ParseNode $n) => $o->setContactRole($n->getStringValue()),
            'countryCode' => fn(ParseNode $n) => $o->setCountryCode($n->getStringValue()),
            'description' => fn(ParseNode $n) => $o->setDescription($n->getStringValue()),
            'eMail' => fn(ParseNode $n) => $o->setEMail($n->getStringValue()),
            'fax' => fn(ParseNode $n) => $o->setFax($n->getStringValue()),
            'name' => fn(ParseNode $n) => $o->setName($n->getStringValue()),
            'notes' => fn(ParseNode $n) => $o->setNotes($n->getStringValue()),
            'recordCreateDate' => fn(ParseNode $n) => $o->setRecordCreateDate($n->getDateTimeValue()),
            'recordDeleted' => fn(ParseNode $n) => $o->setRecordDeleted($n->getIntegerValue()),
            'recordModifyDate' => fn(ParseNode $n) => $o->setRecordModifyDate($n->getDateTimeValue()),
            'taxCode' => fn(ParseNode $n) => $o->setTaxCode($n->getIntegerValue()),
            'telephone' => fn(ParseNode $n) => $o->setTelephone($n->getStringValue()),
            'telephone2' => fn(ParseNode $n) => $o->setTelephone2($n->getStringValue()),
            'vatRegNumber' => fn(ParseNode $n) => $o->setVatRegNumber($n->getStringValue()),
        ];
    }

    /**
     * Gets the name property value. The name property
     * @return string|null
    */
    public function getName(): ?string {
        return $this->name;
    }

    /**
     * Gets the notes property value. The notes property
     * @return string|null
    */
    public function getNotes(): ?string {
        return $this->notes;
    }

    /**
     * Gets the recordCreateDate property value. The recordCreateDate property
     * @return DateTime|null
    */
    public function getRecordCreateDate(): ?DateTime {
        return $this->recordCreateDate;
    }

    /**
     * Gets the recordDeleted property value. The recordDeleted property
     * @return int|null
    */
    public function getRecordDeleted(): ?int {
        return $this->recordDeleted;
    }

    /**
     * Gets the recordModifyDate property value. The recordModifyDate property
     * @return DateTime|null
    */
    public function getRecordModifyDate(): ?DateTime {
        return $this->recordModifyDate;
    }

    /**
     * Gets the taxCode property value. The taxCode property
     * @return int|null
    */
    public function getTaxCode(): ?int {
        return $this->taxCode;
    }

    /**
     * Gets the telephone property value. The telephone property
     * @return string|null
    */
    public function getTelephone(): ?string {
        return $this->telephone;
    }

    /**
     * Gets the telephone2 property value. The telephone2 property
     * @return string|null
    */
    public function getTelephone2(): ?string {
        return $this->telephone2;
    }

    /**
     * Gets the vatRegNumber property value. The vatRegNumber property
     * @return string|null
    */
    public function getVatRegNumber(): ?string {
        return $this->vatRegNumber;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeStringValue('address1', $this->getAddress1());
        $writer->writeStringValue('address2', $this->getAddress2());
        $writer->writeStringValue('address3', $this->getAddress3());
        $writer->writeStringValue('address4', $this->getAddress4());
        $writer->writeStringValue('address5', $this->getAddress5());
        $writer->writeIntegerValue('addressRef', $this->getAddressRef());
        $writer->writeIntegerValue('addressType', $this->getAddressType());
        $writer->writeStringValue('addressTypeName', $this->getAddressTypeName());
        $writer->writeStringValue('contactName', $this->getContactName());
        $writer->writeStringValue('contactRole', $this->getContactRole());
        $writer->writeStringValue('countryCode', $this->getCountryCode());
        $writer->writeStringValue('description', $this->getDescription());
        $writer->writeStringValue('eMail', $this->getEMail());
        $writer->writeStringValue('fax', $this->getFax());
        $writer->writeStringValue('name', $this->getName());
        $writer->writeStringValue('notes', $this->getNotes());
        $writer->writeDateTimeValue('recordCreateDate', $this->getRecordCreateDate());
        $writer->writeIntegerValue('recordDeleted', $this->getRecordDeleted());
        $writer->writeDateTimeValue('recordModifyDate', $this->getRecordModifyDate());
        $writer->writeIntegerValue('taxCode', $this->getTaxCode());
        $writer->writeStringValue('telephone', $this->getTelephone());
        $writer->writeStringValue('telephone2', $this->getTelephone2());
        $writer->writeStringValue('vatRegNumber', $this->getVatRegNumber());
    }

    /**
     * Sets the address1 property value. The address1 property
     * @param string|null $value Value to set for the address1 property.
    */
    public function setAddress1(?string $value): void {
        $this->address1 = $value;
    }

    /**
     * Sets the address2 property value. The address2 property
     * @param string|null $value Value to set for the address2 property.
    */
    public function setAddress2(?string $value): void {
        $this->address2 = $value;
    }

    /**
     * Sets the address3 property value. The address3 property
     * @param string|null $value Value to set for the address3 property.
    */
    public function setAddress3(?string $value): void {
        $this->address3 = $value;
    }

    /**
     * Sets the address4 property value. The address4 property
     * @param string|null $value Value to set for the address4 property.
    */
    public function setAddress4(?string $value): void {
        $this->address4 = $value;
    }

    /**
     * Sets the address5 property value. The address5 property
     * @param string|null $value Value to set for the address5 property.
    */
    public function setAddress5(?string $value): void {
        $this->address5 = $value;
    }

    /**
     * Sets the addressRef property value. The addressRef property
     * @param int|null $value Value to set for the addressRef property.
    */
    public function setAddressRef(?int $value): void {
        $this->addressRef = $value;
    }

    /**
     * Sets the addressType property value. The addressType property
     * @param int|null $value Value to set for the addressType property.
    */
    public function setAddressType(?int $value): void {
        $this->addressType = $value;
    }

    /**
     * Sets the addressTypeName property value. The addressTypeName property
     * @param string|null $value Value to set for the addressTypeName property.
    */
    public function setAddressTypeName(?string $value): void {
        $this->addressTypeName = $value;
    }

    /**
     * Sets the contactName property value. The contactName property
     * @param string|null $value Value to set for the contactName property.
    */
    public function setContactName(?string $value): void {
        $this->contactName = $value;
    }

    /**
     * Sets the contactRole property value. The contactRole property
     * @param string|null $value Value to set for the contactRole property.
    */
    public function setContactRole(?string $value): void {
        $this->contactRole = $value;
    }

    /**
     * Sets the countryCode property value. The countryCode property
     * @param string|null $value Value to set for the countryCode property.
    */
    public function setCountryCode(?string $value): void {
        $this->countryCode = $value;
    }

    /**
     * Sets the description property value. The description property
     * @param string|null $value Value to set for the description property.
    */
    public function setDescription(?string $value): void {
        $this->description = $value;
    }

    /**
     * Sets the eMail property value. The eMail property
     * @param string|null $value Value to set for the eMail property.
    */
    public function setEMail(?string $value): void {
        $this->eMail = $value;
    }

    /**
     * Sets the fax property value. The fax property
     * @param string|null $value Value to set for the fax property.
    */
    public function setFax(?string $value): void {
        $this->fax = $value;
    }

    /**
     * Sets the name property value. The name property
     * @param string|null $value Value to set for the name property.
    */
    public function setName(?string $value): void {
        $this->name = $value;
    }

    /**
     * Sets the notes property value. The notes property
     * @param string|null $value Value to set for the notes property.
    */
    public function setNotes(?string $value): void {
        $this->notes = $value;
    }

    /**
     * Sets the recordCreateDate property value. The recordCreateDate property
     * @param DateTime|null $value Value to set for the recordCreateDate property.
    */
    public function setRecordCreateDate(?DateTime $value): void {
        $this->recordCreateDate = $value;
    }

    /**
     * Sets the recordDeleted property value. The recordDeleted property
     * @param int|null $value Value to set for the recordDeleted property.
    */
    public function setRecordDeleted(?int $value): void {
        $this->recordDeleted = $value;
    }

    /**
     * Sets the recordModifyDate property value. The recordModifyDate property
     * @param DateTime|null $value Value to set for the recordModifyDate property.
    */
    public function setRecordModifyDate(?DateTime $value): void {
        $this->recordModifyDate = $value;
    }

    /**
     * Sets the taxCode property value. The taxCode property
     * @param int|null $value Value to set for the taxCode property.
    */
    public function setTaxCode(?int $value): void {
        $this->taxCode = $value;
    }

    /**
     * Sets the telephone property value. The telephone property
     * @param string|null $value Value to set for the telephone property.
    */
    public function setTelephone(?string $value): void {
        $this->telephone = $value;
    }

    /**
     * Sets the telephone2 property value. The telephone2 property
     * @param string|null $value Value to set for the telephone2 property.
    */
    public function setTelephone2(?string $value): void {
        $this->telephone2 = $value;
    }

    /**
     * Sets the vatRegNumber property value. The vatRegNumber property
     * @param string|null $value Value to set for the vatRegNumber property.
    */
    public function setVatRegNumber(?string $value): void {
        $this->vatRegNumber = $value;
    }

}
