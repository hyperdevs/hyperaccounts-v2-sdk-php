<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class StockIncluded implements Parsable
{
    /**
     * @var GoodsReceivedNoteCollection|null $goodsReceivedNotes The goodsReceivedNotes property
    */
    private ?GoodsReceivedNoteCollection $goodsReceivedNotes = null;

    /**
     * @var SalesInvoiceItemCollection|null $invoiceItems The invoiceItems property
    */
    private ?SalesInvoiceItemCollection $invoiceItems = null;

    /**
     * @var NominalGetDto|null $nominalCode The nominalCode property
    */
    private ?NominalGetDto $nominalCode = null;

    /**
     * @var PriceCollection|null $prices The prices property
    */
    private ?PriceCollection $prices = null;

    /**
     * @var PurchaseOrderItemCollection|null $purchaseOrderItems The purchaseOrderItems property
    */
    private ?PurchaseOrderItemCollection $purchaseOrderItems = null;

    /**
     * @var SalesOrderItemCollection|null $salesOrderItems The salesOrderItems property
    */
    private ?SalesOrderItemCollection $salesOrderItems = null;

    /**
     * @var StockTransactionCollection|null $transactions The transactions property
    */
    private ?StockTransactionCollection $transactions = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return StockIncluded
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): StockIncluded {
        return new StockIncluded();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'goodsReceivedNotes' => fn(ParseNode $n) => $o->setGoodsReceivedNotes($n->getObjectValue([GoodsReceivedNoteCollection::class, 'createFromDiscriminatorValue'])),
            'invoiceItems' => fn(ParseNode $n) => $o->setInvoiceItems($n->getObjectValue([SalesInvoiceItemCollection::class, 'createFromDiscriminatorValue'])),
            'nominalCode' => fn(ParseNode $n) => $o->setNominalCode($n->getObjectValue([NominalGetDto::class, 'createFromDiscriminatorValue'])),
            'prices' => fn(ParseNode $n) => $o->setPrices($n->getObjectValue([PriceCollection::class, 'createFromDiscriminatorValue'])),
            'purchaseOrderItems' => fn(ParseNode $n) => $o->setPurchaseOrderItems($n->getObjectValue([PurchaseOrderItemCollection::class, 'createFromDiscriminatorValue'])),
            'salesOrderItems' => fn(ParseNode $n) => $o->setSalesOrderItems($n->getObjectValue([SalesOrderItemCollection::class, 'createFromDiscriminatorValue'])),
            'transactions' => fn(ParseNode $n) => $o->setTransactions($n->getObjectValue([StockTransactionCollection::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the goodsReceivedNotes property value. The goodsReceivedNotes property
     * @return GoodsReceivedNoteCollection|null
    */
    public function getGoodsReceivedNotes(): ?GoodsReceivedNoteCollection {
        return $this->goodsReceivedNotes;
    }

    /**
     * Gets the invoiceItems property value. The invoiceItems property
     * @return SalesInvoiceItemCollection|null
    */
    public function getInvoiceItems(): ?SalesInvoiceItemCollection {
        return $this->invoiceItems;
    }

    /**
     * Gets the nominalCode property value. The nominalCode property
     * @return NominalGetDto|null
    */
    public function getNominalCode(): ?NominalGetDto {
        return $this->nominalCode;
    }

    /**
     * Gets the prices property value. The prices property
     * @return PriceCollection|null
    */
    public function getPrices(): ?PriceCollection {
        return $this->prices;
    }

    /**
     * Gets the purchaseOrderItems property value. The purchaseOrderItems property
     * @return PurchaseOrderItemCollection|null
    */
    public function getPurchaseOrderItems(): ?PurchaseOrderItemCollection {
        return $this->purchaseOrderItems;
    }

    /**
     * Gets the salesOrderItems property value. The salesOrderItems property
     * @return SalesOrderItemCollection|null
    */
    public function getSalesOrderItems(): ?SalesOrderItemCollection {
        return $this->salesOrderItems;
    }

    /**
     * Gets the transactions property value. The transactions property
     * @return StockTransactionCollection|null
    */
    public function getTransactions(): ?StockTransactionCollection {
        return $this->transactions;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('goodsReceivedNotes', $this->getGoodsReceivedNotes());
        $writer->writeObjectValue('invoiceItems', $this->getInvoiceItems());
        $writer->writeObjectValue('nominalCode', $this->getNominalCode());
        $writer->writeObjectValue('prices', $this->getPrices());
        $writer->writeObjectValue('purchaseOrderItems', $this->getPurchaseOrderItems());
        $writer->writeObjectValue('salesOrderItems', $this->getSalesOrderItems());
        $writer->writeObjectValue('transactions', $this->getTransactions());
    }

    /**
     * Sets the goodsReceivedNotes property value. The goodsReceivedNotes property
     * @param GoodsReceivedNoteCollection|null $value Value to set for the goodsReceivedNotes property.
    */
    public function setGoodsReceivedNotes(?GoodsReceivedNoteCollection $value): void {
        $this->goodsReceivedNotes = $value;
    }

    /**
     * Sets the invoiceItems property value. The invoiceItems property
     * @param SalesInvoiceItemCollection|null $value Value to set for the invoiceItems property.
    */
    public function setInvoiceItems(?SalesInvoiceItemCollection $value): void {
        $this->invoiceItems = $value;
    }

    /**
     * Sets the nominalCode property value. The nominalCode property
     * @param NominalGetDto|null $value Value to set for the nominalCode property.
    */
    public function setNominalCode(?NominalGetDto $value): void {
        $this->nominalCode = $value;
    }

    /**
     * Sets the prices property value. The prices property
     * @param PriceCollection|null $value Value to set for the prices property.
    */
    public function setPrices(?PriceCollection $value): void {
        $this->prices = $value;
    }

    /**
     * Sets the purchaseOrderItems property value. The purchaseOrderItems property
     * @param PurchaseOrderItemCollection|null $value Value to set for the purchaseOrderItems property.
    */
    public function setPurchaseOrderItems(?PurchaseOrderItemCollection $value): void {
        $this->purchaseOrderItems = $value;
    }

    /**
     * Sets the salesOrderItems property value. The salesOrderItems property
     * @param SalesOrderItemCollection|null $value Value to set for the salesOrderItems property.
    */
    public function setSalesOrderItems(?SalesOrderItemCollection $value): void {
        $this->salesOrderItems = $value;
    }

    /**
     * Sets the transactions property value. The transactions property
     * @param StockTransactionCollection|null $value Value to set for the transactions property.
    */
    public function setTransactions(?StockTransactionCollection $value): void {
        $this->transactions = $value;
    }

}
