<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class SalesOrderItemIncluded implements Parsable
{
    /**
     * @var NominalGetDto|null $nominalCode The nominalCode property
    */
    private ?NominalGetDto $nominalCode = null;

    /**
     * @var ProjectGetDto|null $project The project property
    */
    private ?ProjectGetDto $project = null;

    /**
     * @var SalesOrderGetDto|null $salesOrder The salesOrder property
    */
    private ?SalesOrderGetDto $salesOrder = null;

    /**
     * @var StockGetDto|null $stock The stock property
    */
    private ?StockGetDto $stock = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return SalesOrderItemIncluded
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): SalesOrderItemIncluded {
        return new SalesOrderItemIncluded();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'nominalCode' => fn(ParseNode $n) => $o->setNominalCode($n->getObjectValue([NominalGetDto::class, 'createFromDiscriminatorValue'])),
            'project' => fn(ParseNode $n) => $o->setProject($n->getObjectValue([ProjectGetDto::class, 'createFromDiscriminatorValue'])),
            'salesOrder' => fn(ParseNode $n) => $o->setSalesOrder($n->getObjectValue([SalesOrderGetDto::class, 'createFromDiscriminatorValue'])),
            'stock' => fn(ParseNode $n) => $o->setStock($n->getObjectValue([StockGetDto::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the nominalCode property value. The nominalCode property
     * @return NominalGetDto|null
    */
    public function getNominalCode(): ?NominalGetDto {
        return $this->nominalCode;
    }

    /**
     * Gets the project property value. The project property
     * @return ProjectGetDto|null
    */
    public function getProject(): ?ProjectGetDto {
        return $this->project;
    }

    /**
     * Gets the salesOrder property value. The salesOrder property
     * @return SalesOrderGetDto|null
    */
    public function getSalesOrder(): ?SalesOrderGetDto {
        return $this->salesOrder;
    }

    /**
     * Gets the stock property value. The stock property
     * @return StockGetDto|null
    */
    public function getStock(): ?StockGetDto {
        return $this->stock;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('nominalCode', $this->getNominalCode());
        $writer->writeObjectValue('project', $this->getProject());
        $writer->writeObjectValue('salesOrder', $this->getSalesOrder());
        $writer->writeObjectValue('stock', $this->getStock());
    }

    /**
     * Sets the nominalCode property value. The nominalCode property
     * @param NominalGetDto|null $value Value to set for the nominalCode property.
    */
    public function setNominalCode(?NominalGetDto $value): void {
        $this->nominalCode = $value;
    }

    /**
     * Sets the project property value. The project property
     * @param ProjectGetDto|null $value Value to set for the project property.
    */
    public function setProject(?ProjectGetDto $value): void {
        $this->project = $value;
    }

    /**
     * Sets the salesOrder property value. The salesOrder property
     * @param SalesOrderGetDto|null $value Value to set for the salesOrder property.
    */
    public function setSalesOrder(?SalesOrderGetDto $value): void {
        $this->salesOrder = $value;
    }

    /**
     * Sets the stock property value. The stock property
     * @param StockGetDto|null $value Value to set for the stock property.
    */
    public function setStock(?StockGetDto $value): void {
        $this->stock = $value;
    }

}
