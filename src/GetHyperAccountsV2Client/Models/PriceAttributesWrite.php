<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class PriceAttributesWrite implements Parsable
{
    /**
     * @var string|null $pricingRef The pricingRef property
    */
    private ?string $pricingRef = null;

    /**
     * @var int|null $rounderDirection The rounderDirection property
    */
    private ?int $rounderDirection = null;

    /**
     * @var int|null $rounderMethod The rounderMethod property
    */
    private ?int $rounderMethod = null;

    /**
     * @var float|null $rounderMultipleOf The rounderMultipleOf property
    */
    private ?float $rounderMultipleOf = null;

    /**
     * @var float|null $storedPrice The storedPrice property
    */
    private ?float $storedPrice = null;

    /**
     * @var PriceAttributesWrite_type|null $type The type property
    */
    private ?PriceAttributesWrite_type $type = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return PriceAttributesWrite
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): PriceAttributesWrite {
        return new PriceAttributesWrite();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'pricingRef' => fn(ParseNode $n) => $o->setPricingRef($n->getStringValue()),
            'rounderDirection' => fn(ParseNode $n) => $o->setRounderDirection($n->getIntegerValue()),
            'rounderMethod' => fn(ParseNode $n) => $o->setRounderMethod($n->getIntegerValue()),
            'rounderMultipleOf' => fn(ParseNode $n) => $o->setRounderMultipleOf($n->getFloatValue()),
            'storedPrice' => fn(ParseNode $n) => $o->setStoredPrice($n->getFloatValue()),
            'type' => fn(ParseNode $n) => $o->setType($n->getEnumValue(PriceAttributesWrite_type::class)),
        ];
    }

    /**
     * Gets the pricingRef property value. The pricingRef property
     * @return string|null
    */
    public function getPricingRef(): ?string {
        return $this->pricingRef;
    }

    /**
     * Gets the rounderDirection property value. The rounderDirection property
     * @return int|null
    */
    public function getRounderDirection(): ?int {
        return $this->rounderDirection;
    }

    /**
     * Gets the rounderMethod property value. The rounderMethod property
     * @return int|null
    */
    public function getRounderMethod(): ?int {
        return $this->rounderMethod;
    }

    /**
     * Gets the rounderMultipleOf property value. The rounderMultipleOf property
     * @return float|null
    */
    public function getRounderMultipleOf(): ?float {
        return $this->rounderMultipleOf;
    }

    /**
     * Gets the storedPrice property value. The storedPrice property
     * @return float|null
    */
    public function getStoredPrice(): ?float {
        return $this->storedPrice;
    }

    /**
     * Gets the type property value. The type property
     * @return PriceAttributesWrite_type|null
    */
    public function getType(): ?PriceAttributesWrite_type {
        return $this->type;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeStringValue('pricingRef', $this->getPricingRef());
        $writer->writeIntegerValue('rounderDirection', $this->getRounderDirection());
        $writer->writeIntegerValue('rounderMethod', $this->getRounderMethod());
        $writer->writeFloatValue('rounderMultipleOf', $this->getRounderMultipleOf());
        $writer->writeFloatValue('storedPrice', $this->getStoredPrice());
        $writer->writeEnumValue('type', $this->getType());
    }

    /**
     * Sets the pricingRef property value. The pricingRef property
     * @param string|null $value Value to set for the pricingRef property.
    */
    public function setPricingRef(?string $value): void {
        $this->pricingRef = $value;
    }

    /**
     * Sets the rounderDirection property value. The rounderDirection property
     * @param int|null $value Value to set for the rounderDirection property.
    */
    public function setRounderDirection(?int $value): void {
        $this->rounderDirection = $value;
    }

    /**
     * Sets the rounderMethod property value. The rounderMethod property
     * @param int|null $value Value to set for the rounderMethod property.
    */
    public function setRounderMethod(?int $value): void {
        $this->rounderMethod = $value;
    }

    /**
     * Sets the rounderMultipleOf property value. The rounderMultipleOf property
     * @param float|null $value Value to set for the rounderMultipleOf property.
    */
    public function setRounderMultipleOf(?float $value): void {
        $this->rounderMultipleOf = $value;
    }

    /**
     * Sets the storedPrice property value. The storedPrice property
     * @param float|null $value Value to set for the storedPrice property.
    */
    public function setStoredPrice(?float $value): void {
        $this->storedPrice = $value;
    }

    /**
     * Sets the type property value. The type property
     * @param PriceAttributesWrite_type|null $value Value to set for the type property.
    */
    public function setType(?PriceAttributesWrite_type $value): void {
        $this->type = $value;
    }

}
