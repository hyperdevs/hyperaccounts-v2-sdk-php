<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class ChartOfAccountsRelationships implements Parsable
{
    /**
     * @var ChartOfAccountsCategoryRelatedListRelationship|null $categories The categories property
    */
    private ?ChartOfAccountsCategoryRelatedListRelationship $categories = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return ChartOfAccountsRelationships
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): ChartOfAccountsRelationships {
        return new ChartOfAccountsRelationships();
    }

    /**
     * Gets the categories property value. The categories property
     * @return ChartOfAccountsCategoryRelatedListRelationship|null
    */
    public function getCategories(): ?ChartOfAccountsCategoryRelatedListRelationship {
        return $this->categories;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'categories' => fn(ParseNode $n) => $o->setCategories($n->getObjectValue([ChartOfAccountsCategoryRelatedListRelationship::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('categories', $this->getCategories());
    }

    /**
     * Sets the categories property value. The categories property
     * @param ChartOfAccountsCategoryRelatedListRelationship|null $value Value to set for the categories property.
    */
    public function setCategories(?ChartOfAccountsCategoryRelatedListRelationship $value): void {
        $this->categories = $value;
    }

}
