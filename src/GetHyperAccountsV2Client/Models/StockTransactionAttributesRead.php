<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use DateTime;
use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class StockTransactionAttributesRead implements Parsable
{
    /**
     * @var int|null $artefactType The artefactType property
    */
    private ?int $artefactType = null;

    /**
     * @var float|null $costPrice The costPrice property
    */
    private ?float $costPrice = null;

    /**
     * @var DateTime|null $date The date property
    */
    private ?DateTime $date = null;

    /**
     * @var string|null $details The details property
    */
    private ?string $details = null;

    /**
     * @var int|null $gdnNumber The gdnNumber property
    */
    private ?int $gdnNumber = null;

    /**
     * @var int|null $grnNumber The grnNumber property
    */
    private ?int $grnNumber = null;

    /**
     * @var int|null $ispReference The ispReference property
    */
    private ?int $ispReference = null;

    /**
     * @var float|null $qtyUsed The qtyUsed property
    */
    private ?float $qtyUsed = null;

    /**
     * @var float|null $quantity The quantity property
    */
    private ?float $quantity = null;

    /**
     * @var DateTime|null $recordCreateDate The recordCreateDate property
    */
    private ?DateTime $recordCreateDate = null;

    /**
     * @var int|null $recordDeleted The recordDeleted property
    */
    private ?int $recordDeleted = null;

    /**
     * @var DateTime|null $recordModifyDate The recordModifyDate property
    */
    private ?DateTime $recordModifyDate = null;

    /**
     * @var string|null $reference The reference property
    */
    private ?string $reference = null;

    /**
     * @var int|null $referenceNumeric The referenceNumeric property
    */
    private ?int $referenceNumeric = null;

    /**
     * @var float|null $salesPrice The salesPrice property
    */
    private ?float $salesPrice = null;

    /**
     * @var StockTransactionAttributesRead_type|null $type The type property
    */
    private ?StockTransactionAttributesRead_type $type = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return StockTransactionAttributesRead
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): StockTransactionAttributesRead {
        return new StockTransactionAttributesRead();
    }

    /**
     * Gets the artefactType property value. The artefactType property
     * @return int|null
    */
    public function getArtefactType(): ?int {
        return $this->artefactType;
    }

    /**
     * Gets the costPrice property value. The costPrice property
     * @return float|null
    */
    public function getCostPrice(): ?float {
        return $this->costPrice;
    }

    /**
     * Gets the date property value. The date property
     * @return DateTime|null
    */
    public function getDate(): ?DateTime {
        return $this->date;
    }

    /**
     * Gets the details property value. The details property
     * @return string|null
    */
    public function getDetails(): ?string {
        return $this->details;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'artefactType' => fn(ParseNode $n) => $o->setArtefactType($n->getIntegerValue()),
            'costPrice' => fn(ParseNode $n) => $o->setCostPrice($n->getFloatValue()),
            'date' => fn(ParseNode $n) => $o->setDate($n->getDateTimeValue()),
            'details' => fn(ParseNode $n) => $o->setDetails($n->getStringValue()),
            'gdnNumber' => fn(ParseNode $n) => $o->setGdnNumber($n->getIntegerValue()),
            'grnNumber' => fn(ParseNode $n) => $o->setGrnNumber($n->getIntegerValue()),
            'ispReference' => fn(ParseNode $n) => $o->setIspReference($n->getIntegerValue()),
            'qtyUsed' => fn(ParseNode $n) => $o->setQtyUsed($n->getFloatValue()),
            'quantity' => fn(ParseNode $n) => $o->setQuantity($n->getFloatValue()),
            'recordCreateDate' => fn(ParseNode $n) => $o->setRecordCreateDate($n->getDateTimeValue()),
            'recordDeleted' => fn(ParseNode $n) => $o->setRecordDeleted($n->getIntegerValue()),
            'recordModifyDate' => fn(ParseNode $n) => $o->setRecordModifyDate($n->getDateTimeValue()),
            'reference' => fn(ParseNode $n) => $o->setReference($n->getStringValue()),
            'referenceNumeric' => fn(ParseNode $n) => $o->setReferenceNumeric($n->getIntegerValue()),
            'salesPrice' => fn(ParseNode $n) => $o->setSalesPrice($n->getFloatValue()),
            'type' => fn(ParseNode $n) => $o->setType($n->getEnumValue(StockTransactionAttributesRead_type::class)),
        ];
    }

    /**
     * Gets the gdnNumber property value. The gdnNumber property
     * @return int|null
    */
    public function getGdnNumber(): ?int {
        return $this->gdnNumber;
    }

    /**
     * Gets the grnNumber property value. The grnNumber property
     * @return int|null
    */
    public function getGrnNumber(): ?int {
        return $this->grnNumber;
    }

    /**
     * Gets the ispReference property value. The ispReference property
     * @return int|null
    */
    public function getIspReference(): ?int {
        return $this->ispReference;
    }

    /**
     * Gets the qtyUsed property value. The qtyUsed property
     * @return float|null
    */
    public function getQtyUsed(): ?float {
        return $this->qtyUsed;
    }

    /**
     * Gets the quantity property value. The quantity property
     * @return float|null
    */
    public function getQuantity(): ?float {
        return $this->quantity;
    }

    /**
     * Gets the recordCreateDate property value. The recordCreateDate property
     * @return DateTime|null
    */
    public function getRecordCreateDate(): ?DateTime {
        return $this->recordCreateDate;
    }

    /**
     * Gets the recordDeleted property value. The recordDeleted property
     * @return int|null
    */
    public function getRecordDeleted(): ?int {
        return $this->recordDeleted;
    }

    /**
     * Gets the recordModifyDate property value. The recordModifyDate property
     * @return DateTime|null
    */
    public function getRecordModifyDate(): ?DateTime {
        return $this->recordModifyDate;
    }

    /**
     * Gets the reference property value. The reference property
     * @return string|null
    */
    public function getReference(): ?string {
        return $this->reference;
    }

    /**
     * Gets the referenceNumeric property value. The referenceNumeric property
     * @return int|null
    */
    public function getReferenceNumeric(): ?int {
        return $this->referenceNumeric;
    }

    /**
     * Gets the salesPrice property value. The salesPrice property
     * @return float|null
    */
    public function getSalesPrice(): ?float {
        return $this->salesPrice;
    }

    /**
     * Gets the type property value. The type property
     * @return StockTransactionAttributesRead_type|null
    */
    public function getType(): ?StockTransactionAttributesRead_type {
        return $this->type;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeIntegerValue('artefactType', $this->getArtefactType());
        $writer->writeFloatValue('costPrice', $this->getCostPrice());
        $writer->writeDateTimeValue('date', $this->getDate());
        $writer->writeStringValue('details', $this->getDetails());
        $writer->writeIntegerValue('gdnNumber', $this->getGdnNumber());
        $writer->writeIntegerValue('grnNumber', $this->getGrnNumber());
        $writer->writeIntegerValue('ispReference', $this->getIspReference());
        $writer->writeFloatValue('qtyUsed', $this->getQtyUsed());
        $writer->writeFloatValue('quantity', $this->getQuantity());
        $writer->writeDateTimeValue('recordCreateDate', $this->getRecordCreateDate());
        $writer->writeIntegerValue('recordDeleted', $this->getRecordDeleted());
        $writer->writeDateTimeValue('recordModifyDate', $this->getRecordModifyDate());
        $writer->writeStringValue('reference', $this->getReference());
        $writer->writeIntegerValue('referenceNumeric', $this->getReferenceNumeric());
        $writer->writeFloatValue('salesPrice', $this->getSalesPrice());
        $writer->writeEnumValue('type', $this->getType());
    }

    /**
     * Sets the artefactType property value. The artefactType property
     * @param int|null $value Value to set for the artefactType property.
    */
    public function setArtefactType(?int $value): void {
        $this->artefactType = $value;
    }

    /**
     * Sets the costPrice property value. The costPrice property
     * @param float|null $value Value to set for the costPrice property.
    */
    public function setCostPrice(?float $value): void {
        $this->costPrice = $value;
    }

    /**
     * Sets the date property value. The date property
     * @param DateTime|null $value Value to set for the date property.
    */
    public function setDate(?DateTime $value): void {
        $this->date = $value;
    }

    /**
     * Sets the details property value. The details property
     * @param string|null $value Value to set for the details property.
    */
    public function setDetails(?string $value): void {
        $this->details = $value;
    }

    /**
     * Sets the gdnNumber property value. The gdnNumber property
     * @param int|null $value Value to set for the gdnNumber property.
    */
    public function setGdnNumber(?int $value): void {
        $this->gdnNumber = $value;
    }

    /**
     * Sets the grnNumber property value. The grnNumber property
     * @param int|null $value Value to set for the grnNumber property.
    */
    public function setGrnNumber(?int $value): void {
        $this->grnNumber = $value;
    }

    /**
     * Sets the ispReference property value. The ispReference property
     * @param int|null $value Value to set for the ispReference property.
    */
    public function setIspReference(?int $value): void {
        $this->ispReference = $value;
    }

    /**
     * Sets the qtyUsed property value. The qtyUsed property
     * @param float|null $value Value to set for the qtyUsed property.
    */
    public function setQtyUsed(?float $value): void {
        $this->qtyUsed = $value;
    }

    /**
     * Sets the quantity property value. The quantity property
     * @param float|null $value Value to set for the quantity property.
    */
    public function setQuantity(?float $value): void {
        $this->quantity = $value;
    }

    /**
     * Sets the recordCreateDate property value. The recordCreateDate property
     * @param DateTime|null $value Value to set for the recordCreateDate property.
    */
    public function setRecordCreateDate(?DateTime $value): void {
        $this->recordCreateDate = $value;
    }

    /**
     * Sets the recordDeleted property value. The recordDeleted property
     * @param int|null $value Value to set for the recordDeleted property.
    */
    public function setRecordDeleted(?int $value): void {
        $this->recordDeleted = $value;
    }

    /**
     * Sets the recordModifyDate property value. The recordModifyDate property
     * @param DateTime|null $value Value to set for the recordModifyDate property.
    */
    public function setRecordModifyDate(?DateTime $value): void {
        $this->recordModifyDate = $value;
    }

    /**
     * Sets the reference property value. The reference property
     * @param string|null $value Value to set for the reference property.
    */
    public function setReference(?string $value): void {
        $this->reference = $value;
    }

    /**
     * Sets the referenceNumeric property value. The referenceNumeric property
     * @param int|null $value Value to set for the referenceNumeric property.
    */
    public function setReferenceNumeric(?int $value): void {
        $this->referenceNumeric = $value;
    }

    /**
     * Sets the salesPrice property value. The salesPrice property
     * @param float|null $value Value to set for the salesPrice property.
    */
    public function setSalesPrice(?float $value): void {
        $this->salesPrice = $value;
    }

    /**
     * Sets the type property value. The type property
     * @param StockTransactionAttributesRead_type|null $value Value to set for the type property.
    */
    public function setType(?StockTransactionAttributesRead_type $value): void {
        $this->type = $value;
    }

}
