<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class PriceRelated implements Parsable
{
    /**
     * @var string|null $priceId The priceId property
    */
    private ?string $priceId = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return PriceRelated
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): PriceRelated {
        return new PriceRelated();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'priceId' => fn(ParseNode $n) => $o->setPriceId($n->getStringValue()),
        ];
    }

    /**
     * Gets the priceId property value. The priceId property
     * @return string|null
    */
    public function getPriceId(): ?string {
        return $this->priceId;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeStringValue('priceId', $this->getPriceId());
    }

    /**
     * Sets the priceId property value. The priceId property
     * @param string|null $value Value to set for the priceId property.
    */
    public function setPriceId(?string $value): void {
        $this->priceId = $value;
    }

}
