<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use DateTime;
use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class PurchaseOrderItemAttributesEdit implements Parsable
{
    /**
     * @var float|null $addDiscRate The addDiscRate property
    */
    private ?float $addDiscRate = null;

    /**
     * @var string|null $comment1 The comment1 property
    */
    private ?string $comment1 = null;

    /**
     * @var string|null $comment2 The comment2 property
    */
    private ?string $comment2 = null;

    /**
     * @var string|null $costCodeRef The costCodeRef property
    */
    private ?string $costCodeRef = null;

    /**
     * @var DateTime|null $deliveryDate The deliveryDate property
    */
    private ?DateTime $deliveryDate = null;

    /**
     * @var int|null $deptNumber The deptNumber property
    */
    private ?int $deptNumber = null;

    /**
     * @var string|null $description The description property
    */
    private ?string $description = null;

    /**
     * @var float|null $discountAmount The discountAmount property
    */
    private ?float $discountAmount = null;

    /**
     * @var float|null $discountRate The discountRate property
    */
    private ?float $discountRate = null;

    /**
     * @var DateTime|null $dueDate The dueDate property
    */
    private ?DateTime $dueDate = null;

    /**
     * @var float|null $fullNetAmount The fullNetAmount property
    */
    private ?float $fullNetAmount = null;

    /**
     * @var int|null $itemNumber The itemNumber property
    */
    private ?int $itemNumber = null;

    /**
     * @var float|null $netAmount The netAmount property
    */
    private ?float $netAmount = null;

    /**
     * @var string|null $projectRef The projectRef property
    */
    private ?string $projectRef = null;

    /**
     * @var float|null $qtyAllocated The qtyAllocated property
    */
    private ?float $qtyAllocated = null;

    /**
     * @var float|null $qtyAllocatedToProject The qtyAllocatedToProject property
    */
    private ?float $qtyAllocatedToProject = null;

    /**
     * @var float|null $qtyDelivered The qtyDelivered property
    */
    private ?float $qtyDelivered = null;

    /**
     * @var float|null $qtyDespatch The qtyDespatch property
    */
    private ?float $qtyDespatch = null;

    /**
     * @var float|null $qtyInvoiced The qtyInvoiced property
    */
    private ?float $qtyInvoiced = null;

    /**
     * @var float|null $qtyIssuedToProject The qtyIssuedToProject property
    */
    private ?float $qtyIssuedToProject = null;

    /**
     * @var float|null $qtyOrder The qtyOrder property
    */
    private ?float $qtyOrder = null;

    /**
     * @var int|null $serviceFlag The serviceFlag property
    */
    private ?int $serviceFlag = null;

    /**
     * @var float|null $taxAmount The taxAmount property
    */
    private ?float $taxAmount = null;

    /**
     * @var PurchaseOrderItemAttributesEdit_taxCode|null $taxCode The taxCode property
    */
    private ?PurchaseOrderItemAttributesEdit_taxCode $taxCode = null;

    /**
     * @var float|null $taxRate The taxRate property
    */
    private ?float $taxRate = null;

    /**
     * @var string|null $text The text property
    */
    private ?string $text = null;

    /**
     * @var string|null $unitOfSale The unitOfSale property
    */
    private ?string $unitOfSale = null;

    /**
     * @var float|null $unitPrice The unitPrice property
    */
    private ?float $unitPrice = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return PurchaseOrderItemAttributesEdit
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): PurchaseOrderItemAttributesEdit {
        return new PurchaseOrderItemAttributesEdit();
    }

    /**
     * Gets the addDiscRate property value. The addDiscRate property
     * @return float|null
    */
    public function getAddDiscRate(): ?float {
        return $this->addDiscRate;
    }

    /**
     * Gets the comment1 property value. The comment1 property
     * @return string|null
    */
    public function getComment1(): ?string {
        return $this->comment1;
    }

    /**
     * Gets the comment2 property value. The comment2 property
     * @return string|null
    */
    public function getComment2(): ?string {
        return $this->comment2;
    }

    /**
     * Gets the costCodeRef property value. The costCodeRef property
     * @return string|null
    */
    public function getCostCodeRef(): ?string {
        return $this->costCodeRef;
    }

    /**
     * Gets the deliveryDate property value. The deliveryDate property
     * @return DateTime|null
    */
    public function getDeliveryDate(): ?DateTime {
        return $this->deliveryDate;
    }

    /**
     * Gets the deptNumber property value. The deptNumber property
     * @return int|null
    */
    public function getDeptNumber(): ?int {
        return $this->deptNumber;
    }

    /**
     * Gets the description property value. The description property
     * @return string|null
    */
    public function getDescription(): ?string {
        return $this->description;
    }

    /**
     * Gets the discountAmount property value. The discountAmount property
     * @return float|null
    */
    public function getDiscountAmount(): ?float {
        return $this->discountAmount;
    }

    /**
     * Gets the discountRate property value. The discountRate property
     * @return float|null
    */
    public function getDiscountRate(): ?float {
        return $this->discountRate;
    }

    /**
     * Gets the dueDate property value. The dueDate property
     * @return DateTime|null
    */
    public function getDueDate(): ?DateTime {
        return $this->dueDate;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'addDiscRate' => fn(ParseNode $n) => $o->setAddDiscRate($n->getFloatValue()),
            'comment1' => fn(ParseNode $n) => $o->setComment1($n->getStringValue()),
            'comment2' => fn(ParseNode $n) => $o->setComment2($n->getStringValue()),
            'costCodeRef' => fn(ParseNode $n) => $o->setCostCodeRef($n->getStringValue()),
            'deliveryDate' => fn(ParseNode $n) => $o->setDeliveryDate($n->getDateTimeValue()),
            'deptNumber' => fn(ParseNode $n) => $o->setDeptNumber($n->getIntegerValue()),
            'description' => fn(ParseNode $n) => $o->setDescription($n->getStringValue()),
            'discountAmount' => fn(ParseNode $n) => $o->setDiscountAmount($n->getFloatValue()),
            'discountRate' => fn(ParseNode $n) => $o->setDiscountRate($n->getFloatValue()),
            'dueDate' => fn(ParseNode $n) => $o->setDueDate($n->getDateTimeValue()),
            'fullNetAmount' => fn(ParseNode $n) => $o->setFullNetAmount($n->getFloatValue()),
            'itemNumber' => fn(ParseNode $n) => $o->setItemNumber($n->getIntegerValue()),
            'netAmount' => fn(ParseNode $n) => $o->setNetAmount($n->getFloatValue()),
            'projectRef' => fn(ParseNode $n) => $o->setProjectRef($n->getStringValue()),
            'qtyAllocated' => fn(ParseNode $n) => $o->setQtyAllocated($n->getFloatValue()),
            'qtyAllocatedToProject' => fn(ParseNode $n) => $o->setQtyAllocatedToProject($n->getFloatValue()),
            'qtyDelivered' => fn(ParseNode $n) => $o->setQtyDelivered($n->getFloatValue()),
            'qtyDespatch' => fn(ParseNode $n) => $o->setQtyDespatch($n->getFloatValue()),
            'qtyInvoiced' => fn(ParseNode $n) => $o->setQtyInvoiced($n->getFloatValue()),
            'qtyIssuedToProject' => fn(ParseNode $n) => $o->setQtyIssuedToProject($n->getFloatValue()),
            'qtyOrder' => fn(ParseNode $n) => $o->setQtyOrder($n->getFloatValue()),
            'serviceFlag' => fn(ParseNode $n) => $o->setServiceFlag($n->getIntegerValue()),
            'taxAmount' => fn(ParseNode $n) => $o->setTaxAmount($n->getFloatValue()),
            'taxCode' => fn(ParseNode $n) => $o->setTaxCode($n->getEnumValue(PurchaseOrderItemAttributesEdit_taxCode::class)),
            'taxRate' => fn(ParseNode $n) => $o->setTaxRate($n->getFloatValue()),
            'text' => fn(ParseNode $n) => $o->setText($n->getStringValue()),
            'unitOfSale' => fn(ParseNode $n) => $o->setUnitOfSale($n->getStringValue()),
            'unitPrice' => fn(ParseNode $n) => $o->setUnitPrice($n->getFloatValue()),
        ];
    }

    /**
     * Gets the fullNetAmount property value. The fullNetAmount property
     * @return float|null
    */
    public function getFullNetAmount(): ?float {
        return $this->fullNetAmount;
    }

    /**
     * Gets the itemNumber property value. The itemNumber property
     * @return int|null
    */
    public function getItemNumber(): ?int {
        return $this->itemNumber;
    }

    /**
     * Gets the netAmount property value. The netAmount property
     * @return float|null
    */
    public function getNetAmount(): ?float {
        return $this->netAmount;
    }

    /**
     * Gets the projectRef property value. The projectRef property
     * @return string|null
    */
    public function getProjectRef(): ?string {
        return $this->projectRef;
    }

    /**
     * Gets the qtyAllocated property value. The qtyAllocated property
     * @return float|null
    */
    public function getQtyAllocated(): ?float {
        return $this->qtyAllocated;
    }

    /**
     * Gets the qtyAllocatedToProject property value. The qtyAllocatedToProject property
     * @return float|null
    */
    public function getQtyAllocatedToProject(): ?float {
        return $this->qtyAllocatedToProject;
    }

    /**
     * Gets the qtyDelivered property value. The qtyDelivered property
     * @return float|null
    */
    public function getQtyDelivered(): ?float {
        return $this->qtyDelivered;
    }

    /**
     * Gets the qtyDespatch property value. The qtyDespatch property
     * @return float|null
    */
    public function getQtyDespatch(): ?float {
        return $this->qtyDespatch;
    }

    /**
     * Gets the qtyInvoiced property value. The qtyInvoiced property
     * @return float|null
    */
    public function getQtyInvoiced(): ?float {
        return $this->qtyInvoiced;
    }

    /**
     * Gets the qtyIssuedToProject property value. The qtyIssuedToProject property
     * @return float|null
    */
    public function getQtyIssuedToProject(): ?float {
        return $this->qtyIssuedToProject;
    }

    /**
     * Gets the qtyOrder property value. The qtyOrder property
     * @return float|null
    */
    public function getQtyOrder(): ?float {
        return $this->qtyOrder;
    }

    /**
     * Gets the serviceFlag property value. The serviceFlag property
     * @return int|null
    */
    public function getServiceFlag(): ?int {
        return $this->serviceFlag;
    }

    /**
     * Gets the taxAmount property value. The taxAmount property
     * @return float|null
    */
    public function getTaxAmount(): ?float {
        return $this->taxAmount;
    }

    /**
     * Gets the taxCode property value. The taxCode property
     * @return PurchaseOrderItemAttributesEdit_taxCode|null
    */
    public function getTaxCode(): ?PurchaseOrderItemAttributesEdit_taxCode {
        return $this->taxCode;
    }

    /**
     * Gets the taxRate property value. The taxRate property
     * @return float|null
    */
    public function getTaxRate(): ?float {
        return $this->taxRate;
    }

    /**
     * Gets the text property value. The text property
     * @return string|null
    */
    public function getText(): ?string {
        return $this->text;
    }

    /**
     * Gets the unitOfSale property value. The unitOfSale property
     * @return string|null
    */
    public function getUnitOfSale(): ?string {
        return $this->unitOfSale;
    }

    /**
     * Gets the unitPrice property value. The unitPrice property
     * @return float|null
    */
    public function getUnitPrice(): ?float {
        return $this->unitPrice;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeFloatValue('addDiscRate', $this->getAddDiscRate());
        $writer->writeStringValue('comment1', $this->getComment1());
        $writer->writeStringValue('comment2', $this->getComment2());
        $writer->writeStringValue('costCodeRef', $this->getCostCodeRef());
        $writer->writeDateTimeValue('deliveryDate', $this->getDeliveryDate());
        $writer->writeIntegerValue('deptNumber', $this->getDeptNumber());
        $writer->writeStringValue('description', $this->getDescription());
        $writer->writeFloatValue('discountAmount', $this->getDiscountAmount());
        $writer->writeFloatValue('discountRate', $this->getDiscountRate());
        $writer->writeDateTimeValue('dueDate', $this->getDueDate());
        $writer->writeFloatValue('fullNetAmount', $this->getFullNetAmount());
        $writer->writeIntegerValue('itemNumber', $this->getItemNumber());
        $writer->writeFloatValue('netAmount', $this->getNetAmount());
        $writer->writeStringValue('projectRef', $this->getProjectRef());
        $writer->writeFloatValue('qtyAllocated', $this->getQtyAllocated());
        $writer->writeFloatValue('qtyAllocatedToProject', $this->getQtyAllocatedToProject());
        $writer->writeFloatValue('qtyDelivered', $this->getQtyDelivered());
        $writer->writeFloatValue('qtyDespatch', $this->getQtyDespatch());
        $writer->writeFloatValue('qtyInvoiced', $this->getQtyInvoiced());
        $writer->writeFloatValue('qtyIssuedToProject', $this->getQtyIssuedToProject());
        $writer->writeFloatValue('qtyOrder', $this->getQtyOrder());
        $writer->writeIntegerValue('serviceFlag', $this->getServiceFlag());
        $writer->writeFloatValue('taxAmount', $this->getTaxAmount());
        $writer->writeEnumValue('taxCode', $this->getTaxCode());
        $writer->writeFloatValue('taxRate', $this->getTaxRate());
        $writer->writeStringValue('text', $this->getText());
        $writer->writeStringValue('unitOfSale', $this->getUnitOfSale());
        $writer->writeFloatValue('unitPrice', $this->getUnitPrice());
    }

    /**
     * Sets the addDiscRate property value. The addDiscRate property
     * @param float|null $value Value to set for the addDiscRate property.
    */
    public function setAddDiscRate(?float $value): void {
        $this->addDiscRate = $value;
    }

    /**
     * Sets the comment1 property value. The comment1 property
     * @param string|null $value Value to set for the comment1 property.
    */
    public function setComment1(?string $value): void {
        $this->comment1 = $value;
    }

    /**
     * Sets the comment2 property value. The comment2 property
     * @param string|null $value Value to set for the comment2 property.
    */
    public function setComment2(?string $value): void {
        $this->comment2 = $value;
    }

    /**
     * Sets the costCodeRef property value. The costCodeRef property
     * @param string|null $value Value to set for the costCodeRef property.
    */
    public function setCostCodeRef(?string $value): void {
        $this->costCodeRef = $value;
    }

    /**
     * Sets the deliveryDate property value. The deliveryDate property
     * @param DateTime|null $value Value to set for the deliveryDate property.
    */
    public function setDeliveryDate(?DateTime $value): void {
        $this->deliveryDate = $value;
    }

    /**
     * Sets the deptNumber property value. The deptNumber property
     * @param int|null $value Value to set for the deptNumber property.
    */
    public function setDeptNumber(?int $value): void {
        $this->deptNumber = $value;
    }

    /**
     * Sets the description property value. The description property
     * @param string|null $value Value to set for the description property.
    */
    public function setDescription(?string $value): void {
        $this->description = $value;
    }

    /**
     * Sets the discountAmount property value. The discountAmount property
     * @param float|null $value Value to set for the discountAmount property.
    */
    public function setDiscountAmount(?float $value): void {
        $this->discountAmount = $value;
    }

    /**
     * Sets the discountRate property value. The discountRate property
     * @param float|null $value Value to set for the discountRate property.
    */
    public function setDiscountRate(?float $value): void {
        $this->discountRate = $value;
    }

    /**
     * Sets the dueDate property value. The dueDate property
     * @param DateTime|null $value Value to set for the dueDate property.
    */
    public function setDueDate(?DateTime $value): void {
        $this->dueDate = $value;
    }

    /**
     * Sets the fullNetAmount property value. The fullNetAmount property
     * @param float|null $value Value to set for the fullNetAmount property.
    */
    public function setFullNetAmount(?float $value): void {
        $this->fullNetAmount = $value;
    }

    /**
     * Sets the itemNumber property value. The itemNumber property
     * @param int|null $value Value to set for the itemNumber property.
    */
    public function setItemNumber(?int $value): void {
        $this->itemNumber = $value;
    }

    /**
     * Sets the netAmount property value. The netAmount property
     * @param float|null $value Value to set for the netAmount property.
    */
    public function setNetAmount(?float $value): void {
        $this->netAmount = $value;
    }

    /**
     * Sets the projectRef property value. The projectRef property
     * @param string|null $value Value to set for the projectRef property.
    */
    public function setProjectRef(?string $value): void {
        $this->projectRef = $value;
    }

    /**
     * Sets the qtyAllocated property value. The qtyAllocated property
     * @param float|null $value Value to set for the qtyAllocated property.
    */
    public function setQtyAllocated(?float $value): void {
        $this->qtyAllocated = $value;
    }

    /**
     * Sets the qtyAllocatedToProject property value. The qtyAllocatedToProject property
     * @param float|null $value Value to set for the qtyAllocatedToProject property.
    */
    public function setQtyAllocatedToProject(?float $value): void {
        $this->qtyAllocatedToProject = $value;
    }

    /**
     * Sets the qtyDelivered property value. The qtyDelivered property
     * @param float|null $value Value to set for the qtyDelivered property.
    */
    public function setQtyDelivered(?float $value): void {
        $this->qtyDelivered = $value;
    }

    /**
     * Sets the qtyDespatch property value. The qtyDespatch property
     * @param float|null $value Value to set for the qtyDespatch property.
    */
    public function setQtyDespatch(?float $value): void {
        $this->qtyDespatch = $value;
    }

    /**
     * Sets the qtyInvoiced property value. The qtyInvoiced property
     * @param float|null $value Value to set for the qtyInvoiced property.
    */
    public function setQtyInvoiced(?float $value): void {
        $this->qtyInvoiced = $value;
    }

    /**
     * Sets the qtyIssuedToProject property value. The qtyIssuedToProject property
     * @param float|null $value Value to set for the qtyIssuedToProject property.
    */
    public function setQtyIssuedToProject(?float $value): void {
        $this->qtyIssuedToProject = $value;
    }

    /**
     * Sets the qtyOrder property value. The qtyOrder property
     * @param float|null $value Value to set for the qtyOrder property.
    */
    public function setQtyOrder(?float $value): void {
        $this->qtyOrder = $value;
    }

    /**
     * Sets the serviceFlag property value. The serviceFlag property
     * @param int|null $value Value to set for the serviceFlag property.
    */
    public function setServiceFlag(?int $value): void {
        $this->serviceFlag = $value;
    }

    /**
     * Sets the taxAmount property value. The taxAmount property
     * @param float|null $value Value to set for the taxAmount property.
    */
    public function setTaxAmount(?float $value): void {
        $this->taxAmount = $value;
    }

    /**
     * Sets the taxCode property value. The taxCode property
     * @param PurchaseOrderItemAttributesEdit_taxCode|null $value Value to set for the taxCode property.
    */
    public function setTaxCode(?PurchaseOrderItemAttributesEdit_taxCode $value): void {
        $this->taxCode = $value;
    }

    /**
     * Sets the taxRate property value. The taxRate property
     * @param float|null $value Value to set for the taxRate property.
    */
    public function setTaxRate(?float $value): void {
        $this->taxRate = $value;
    }

    /**
     * Sets the text property value. The text property
     * @param string|null $value Value to set for the text property.
    */
    public function setText(?string $value): void {
        $this->text = $value;
    }

    /**
     * Sets the unitOfSale property value. The unitOfSale property
     * @param string|null $value Value to set for the unitOfSale property.
    */
    public function setUnitOfSale(?string $value): void {
        $this->unitOfSale = $value;
    }

    /**
     * Sets the unitPrice property value. The unitPrice property
     * @param float|null $value Value to set for the unitPrice property.
    */
    public function setUnitPrice(?float $value): void {
        $this->unitPrice = $value;
    }

}
