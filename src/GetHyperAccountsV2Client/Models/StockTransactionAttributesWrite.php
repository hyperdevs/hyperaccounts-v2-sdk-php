<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use DateTime;
use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class StockTransactionAttributesWrite implements Parsable
{
    /**
     * @var float|null $costPrice The costPrice property
    */
    private ?float $costPrice = null;

    /**
     * @var DateTime|null $date The date property
    */
    private ?DateTime $date = null;

    /**
     * @var string|null $details The details property
    */
    private ?string $details = null;

    /**
     * @var int|null $ispReference The ispReference property
    */
    private ?int $ispReference = null;

    /**
     * @var float|null $qtyUsed The qtyUsed property
    */
    private ?float $qtyUsed = null;

    /**
     * @var float|null $quantity The quantity property
    */
    private ?float $quantity = null;

    /**
     * @var string|null $reference The reference property
    */
    private ?string $reference = null;

    /**
     * @var float|null $salesPrice The salesPrice property
    */
    private ?float $salesPrice = null;

    /**
     * @var StockTransactionAttributesWrite_type|null $type The type property
    */
    private ?StockTransactionAttributesWrite_type $type = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return StockTransactionAttributesWrite
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): StockTransactionAttributesWrite {
        return new StockTransactionAttributesWrite();
    }

    /**
     * Gets the costPrice property value. The costPrice property
     * @return float|null
    */
    public function getCostPrice(): ?float {
        return $this->costPrice;
    }

    /**
     * Gets the date property value. The date property
     * @return DateTime|null
    */
    public function getDate(): ?DateTime {
        return $this->date;
    }

    /**
     * Gets the details property value. The details property
     * @return string|null
    */
    public function getDetails(): ?string {
        return $this->details;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'costPrice' => fn(ParseNode $n) => $o->setCostPrice($n->getFloatValue()),
            'date' => fn(ParseNode $n) => $o->setDate($n->getDateTimeValue()),
            'details' => fn(ParseNode $n) => $o->setDetails($n->getStringValue()),
            'ispReference' => fn(ParseNode $n) => $o->setIspReference($n->getIntegerValue()),
            'qtyUsed' => fn(ParseNode $n) => $o->setQtyUsed($n->getFloatValue()),
            'quantity' => fn(ParseNode $n) => $o->setQuantity($n->getFloatValue()),
            'reference' => fn(ParseNode $n) => $o->setReference($n->getStringValue()),
            'salesPrice' => fn(ParseNode $n) => $o->setSalesPrice($n->getFloatValue()),
            'type' => fn(ParseNode $n) => $o->setType($n->getEnumValue(StockTransactionAttributesWrite_type::class)),
        ];
    }

    /**
     * Gets the ispReference property value. The ispReference property
     * @return int|null
    */
    public function getIspReference(): ?int {
        return $this->ispReference;
    }

    /**
     * Gets the qtyUsed property value. The qtyUsed property
     * @return float|null
    */
    public function getQtyUsed(): ?float {
        return $this->qtyUsed;
    }

    /**
     * Gets the quantity property value. The quantity property
     * @return float|null
    */
    public function getQuantity(): ?float {
        return $this->quantity;
    }

    /**
     * Gets the reference property value. The reference property
     * @return string|null
    */
    public function getReference(): ?string {
        return $this->reference;
    }

    /**
     * Gets the salesPrice property value. The salesPrice property
     * @return float|null
    */
    public function getSalesPrice(): ?float {
        return $this->salesPrice;
    }

    /**
     * Gets the type property value. The type property
     * @return StockTransactionAttributesWrite_type|null
    */
    public function getType(): ?StockTransactionAttributesWrite_type {
        return $this->type;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeFloatValue('costPrice', $this->getCostPrice());
        $writer->writeDateTimeValue('date', $this->getDate());
        $writer->writeStringValue('details', $this->getDetails());
        $writer->writeIntegerValue('ispReference', $this->getIspReference());
        $writer->writeFloatValue('qtyUsed', $this->getQtyUsed());
        $writer->writeFloatValue('quantity', $this->getQuantity());
        $writer->writeStringValue('reference', $this->getReference());
        $writer->writeFloatValue('salesPrice', $this->getSalesPrice());
        $writer->writeEnumValue('type', $this->getType());
    }

    /**
     * Sets the costPrice property value. The costPrice property
     * @param float|null $value Value to set for the costPrice property.
    */
    public function setCostPrice(?float $value): void {
        $this->costPrice = $value;
    }

    /**
     * Sets the date property value. The date property
     * @param DateTime|null $value Value to set for the date property.
    */
    public function setDate(?DateTime $value): void {
        $this->date = $value;
    }

    /**
     * Sets the details property value. The details property
     * @param string|null $value Value to set for the details property.
    */
    public function setDetails(?string $value): void {
        $this->details = $value;
    }

    /**
     * Sets the ispReference property value. The ispReference property
     * @param int|null $value Value to set for the ispReference property.
    */
    public function setIspReference(?int $value): void {
        $this->ispReference = $value;
    }

    /**
     * Sets the qtyUsed property value. The qtyUsed property
     * @param float|null $value Value to set for the qtyUsed property.
    */
    public function setQtyUsed(?float $value): void {
        $this->qtyUsed = $value;
    }

    /**
     * Sets the quantity property value. The quantity property
     * @param float|null $value Value to set for the quantity property.
    */
    public function setQuantity(?float $value): void {
        $this->quantity = $value;
    }

    /**
     * Sets the reference property value. The reference property
     * @param string|null $value Value to set for the reference property.
    */
    public function setReference(?string $value): void {
        $this->reference = $value;
    }

    /**
     * Sets the salesPrice property value. The salesPrice property
     * @param float|null $value Value to set for the salesPrice property.
    */
    public function setSalesPrice(?float $value): void {
        $this->salesPrice = $value;
    }

    /**
     * Sets the type property value. The type property
     * @param StockTransactionAttributesWrite_type|null $value Value to set for the type property.
    */
    public function setType(?StockTransactionAttributesWrite_type $value): void {
        $this->type = $value;
    }

}
