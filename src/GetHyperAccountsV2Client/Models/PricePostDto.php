<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class PricePostDto implements Parsable
{
    /**
     * @var PriceAttributesWrite|null $attributes The attributes property
    */
    private ?PriceAttributesWrite $attributes = null;

    /**
     * @var string|null $priceId The priceId property
    */
    private ?string $priceId = null;

    /**
     * @var PriceRelationshipsWrite|null $relationships The relationships property
    */
    private ?PriceRelationshipsWrite $relationships = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return PricePostDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): PricePostDto {
        return new PricePostDto();
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return PriceAttributesWrite|null
    */
    public function getAttributes(): ?PriceAttributesWrite {
        return $this->attributes;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([PriceAttributesWrite::class, 'createFromDiscriminatorValue'])),
            'priceId' => fn(ParseNode $n) => $o->setPriceId($n->getStringValue()),
            'relationships' => fn(ParseNode $n) => $o->setRelationships($n->getObjectValue([PriceRelationshipsWrite::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the priceId property value. The priceId property
     * @return string|null
    */
    public function getPriceId(): ?string {
        return $this->priceId;
    }

    /**
     * Gets the relationships property value. The relationships property
     * @return PriceRelationshipsWrite|null
    */
    public function getRelationships(): ?PriceRelationshipsWrite {
        return $this->relationships;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('attributes', $this->getAttributes());
        $writer->writeStringValue('priceId', $this->getPriceId());
        $writer->writeObjectValue('relationships', $this->getRelationships());
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param PriceAttributesWrite|null $value Value to set for the attributes property.
    */
    public function setAttributes(?PriceAttributesWrite $value): void {
        $this->attributes = $value;
    }

    /**
     * Sets the priceId property value. The priceId property
     * @param string|null $value Value to set for the priceId property.
    */
    public function setPriceId(?string $value): void {
        $this->priceId = $value;
    }

    /**
     * Sets the relationships property value. The relationships property
     * @param PriceRelationshipsWrite|null $value Value to set for the relationships property.
    */
    public function setRelationships(?PriceRelationshipsWrite $value): void {
        $this->relationships = $value;
    }

}
