<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class ProjectCostCodeRelatedRequired implements Parsable
{
    /**
     * @var int|null $costCodeId The costCodeId property
    */
    private ?int $costCodeId = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return ProjectCostCodeRelatedRequired
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): ProjectCostCodeRelatedRequired {
        return new ProjectCostCodeRelatedRequired();
    }

    /**
     * Gets the costCodeId property value. The costCodeId property
     * @return int|null
    */
    public function getCostCodeId(): ?int {
        return $this->costCodeId;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'costCodeId' => fn(ParseNode $n) => $o->setCostCodeId($n->getIntegerValue()),
        ];
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeIntegerValue('costCodeId', $this->getCostCodeId());
    }

    /**
     * Sets the costCodeId property value. The costCodeId property
     * @param int|null $value Value to set for the costCodeId property.
    */
    public function setCostCodeId(?int $value): void {
        $this->costCodeId = $value;
    }

}
