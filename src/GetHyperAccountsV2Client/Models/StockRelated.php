<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class StockRelated implements Parsable
{
    /**
     * @var string|null $stockCode The stockCode property
    */
    private ?string $stockCode = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return StockRelated
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): StockRelated {
        return new StockRelated();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'stockCode' => fn(ParseNode $n) => $o->setStockCode($n->getStringValue()),
        ];
    }

    /**
     * Gets the stockCode property value. The stockCode property
     * @return string|null
    */
    public function getStockCode(): ?string {
        return $this->stockCode;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeStringValue('stockCode', $this->getStockCode());
    }

    /**
     * Sets the stockCode property value. The stockCode property
     * @param string|null $value Value to set for the stockCode property.
    */
    public function setStockCode(?string $value): void {
        $this->stockCode = $value;
    }

}
