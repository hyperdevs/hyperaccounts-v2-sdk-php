<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class DepartmentGetDto implements Parsable
{
    /**
     * @var DepartmentAttributesRead|null $attributes The attributes property
    */
    private ?DepartmentAttributesRead $attributes = null;

    /**
     * @var ResourceLinks|null $links The links property
    */
    private ?ResourceLinks $links = null;

    /**
     * @var string|null $reference The reference property
    */
    private ?string $reference = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return DepartmentGetDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): DepartmentGetDto {
        return new DepartmentGetDto();
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return DepartmentAttributesRead|null
    */
    public function getAttributes(): ?DepartmentAttributesRead {
        return $this->attributes;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([DepartmentAttributesRead::class, 'createFromDiscriminatorValue'])),
            'links' => fn(ParseNode $n) => $o->setLinks($n->getObjectValue([ResourceLinks::class, 'createFromDiscriminatorValue'])),
            'reference' => fn(ParseNode $n) => $o->setReference($n->getStringValue()),
        ];
    }

    /**
     * Gets the links property value. The links property
     * @return ResourceLinks|null
    */
    public function getLinks(): ?ResourceLinks {
        return $this->links;
    }

    /**
     * Gets the reference property value. The reference property
     * @return string|null
    */
    public function getReference(): ?string {
        return $this->reference;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('attributes', $this->getAttributes());
        $writer->writeObjectValue('links', $this->getLinks());
        $writer->writeStringValue('reference', $this->getReference());
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param DepartmentAttributesRead|null $value Value to set for the attributes property.
    */
    public function setAttributes(?DepartmentAttributesRead $value): void {
        $this->attributes = $value;
    }

    /**
     * Sets the links property value. The links property
     * @param ResourceLinks|null $value Value to set for the links property.
    */
    public function setLinks(?ResourceLinks $value): void {
        $this->links = $value;
    }

    /**
     * Sets the reference property value. The reference property
     * @param string|null $value Value to set for the reference property.
    */
    public function setReference(?string $value): void {
        $this->reference = $value;
    }

}
