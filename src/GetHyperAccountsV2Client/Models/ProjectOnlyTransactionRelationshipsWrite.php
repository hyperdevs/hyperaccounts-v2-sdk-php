<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class ProjectOnlyTransactionRelationshipsWrite implements Parsable
{
    /**
     * @var ProjectRelatedRequiredRelationshipWriteRequired|null $project The project property
    */
    private ?ProjectRelatedRequiredRelationshipWriteRequired $project = null;

    /**
     * @var ProjectCostCodeRelatedRequiredRelationshipWriteRequired|null $projectCostCode The projectCostCode property
    */
    private ?ProjectCostCodeRelatedRequiredRelationshipWriteRequired $projectCostCode = null;

    /**
     * @var ProjectResourceRelatedRelationshipWrite|null $projectResource The projectResource property
    */
    private ?ProjectResourceRelatedRelationshipWrite $projectResource = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return ProjectOnlyTransactionRelationshipsWrite
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): ProjectOnlyTransactionRelationshipsWrite {
        return new ProjectOnlyTransactionRelationshipsWrite();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'project' => fn(ParseNode $n) => $o->setProject($n->getObjectValue([ProjectRelatedRequiredRelationshipWriteRequired::class, 'createFromDiscriminatorValue'])),
            'projectCostCode' => fn(ParseNode $n) => $o->setProjectCostCode($n->getObjectValue([ProjectCostCodeRelatedRequiredRelationshipWriteRequired::class, 'createFromDiscriminatorValue'])),
            'projectResource' => fn(ParseNode $n) => $o->setProjectResource($n->getObjectValue([ProjectResourceRelatedRelationshipWrite::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the project property value. The project property
     * @return ProjectRelatedRequiredRelationshipWriteRequired|null
    */
    public function getProject(): ?ProjectRelatedRequiredRelationshipWriteRequired {
        return $this->project;
    }

    /**
     * Gets the projectCostCode property value. The projectCostCode property
     * @return ProjectCostCodeRelatedRequiredRelationshipWriteRequired|null
    */
    public function getProjectCostCode(): ?ProjectCostCodeRelatedRequiredRelationshipWriteRequired {
        return $this->projectCostCode;
    }

    /**
     * Gets the projectResource property value. The projectResource property
     * @return ProjectResourceRelatedRelationshipWrite|null
    */
    public function getProjectResource(): ?ProjectResourceRelatedRelationshipWrite {
        return $this->projectResource;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('project', $this->getProject());
        $writer->writeObjectValue('projectCostCode', $this->getProjectCostCode());
        $writer->writeObjectValue('projectResource', $this->getProjectResource());
    }

    /**
     * Sets the project property value. The project property
     * @param ProjectRelatedRequiredRelationshipWriteRequired|null $value Value to set for the project property.
    */
    public function setProject(?ProjectRelatedRequiredRelationshipWriteRequired $value): void {
        $this->project = $value;
    }

    /**
     * Sets the projectCostCode property value. The projectCostCode property
     * @param ProjectCostCodeRelatedRequiredRelationshipWriteRequired|null $value Value to set for the projectCostCode property.
    */
    public function setProjectCostCode(?ProjectCostCodeRelatedRequiredRelationshipWriteRequired $value): void {
        $this->projectCostCode = $value;
    }

    /**
     * Sets the projectResource property value. The projectResource property
     * @param ProjectResourceRelatedRelationshipWrite|null $value Value to set for the projectResource property.
    */
    public function setProjectResource(?ProjectResourceRelatedRelationshipWrite $value): void {
        $this->projectResource = $value;
    }

}
