<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class SalesOrderRelationshipsEdit implements Parsable
{
    /**
     * @var CustomerRelatedRelationshipWrite|null $customer The customer property
    */
    private ?CustomerRelatedRelationshipWrite $customer = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return SalesOrderRelationshipsEdit
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): SalesOrderRelationshipsEdit {
        return new SalesOrderRelationshipsEdit();
    }

    /**
     * Gets the customer property value. The customer property
     * @return CustomerRelatedRelationshipWrite|null
    */
    public function getCustomer(): ?CustomerRelatedRelationshipWrite {
        return $this->customer;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'customer' => fn(ParseNode $n) => $o->setCustomer($n->getObjectValue([CustomerRelatedRelationshipWrite::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('customer', $this->getCustomer());
    }

    /**
     * Sets the customer property value. The customer property
     * @param CustomerRelatedRelationshipWrite|null $value Value to set for the customer property.
    */
    public function setCustomer(?CustomerRelatedRelationshipWrite $value): void {
        $this->customer = $value;
    }

}
