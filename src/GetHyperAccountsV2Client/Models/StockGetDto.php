<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class StockGetDto implements Parsable
{
    /**
     * @var StockAttributesRead|null $attributes The attributes property
    */
    private ?StockAttributesRead $attributes = null;

    /**
     * @var StockIncluded|null $included The included property
    */
    private ?StockIncluded $included = null;

    /**
     * @var ResourceLinks|null $links The links property
    */
    private ?ResourceLinks $links = null;

    /**
     * @var StockRelationships|null $relationships The relationships property
    */
    private ?StockRelationships $relationships = null;

    /**
     * @var string|null $stockCode The stockCode property
    */
    private ?string $stockCode = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return StockGetDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): StockGetDto {
        return new StockGetDto();
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return StockAttributesRead|null
    */
    public function getAttributes(): ?StockAttributesRead {
        return $this->attributes;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([StockAttributesRead::class, 'createFromDiscriminatorValue'])),
            'included' => fn(ParseNode $n) => $o->setIncluded($n->getObjectValue([StockIncluded::class, 'createFromDiscriminatorValue'])),
            'links' => fn(ParseNode $n) => $o->setLinks($n->getObjectValue([ResourceLinks::class, 'createFromDiscriminatorValue'])),
            'relationships' => fn(ParseNode $n) => $o->setRelationships($n->getObjectValue([StockRelationships::class, 'createFromDiscriminatorValue'])),
            'stockCode' => fn(ParseNode $n) => $o->setStockCode($n->getStringValue()),
        ];
    }

    /**
     * Gets the included property value. The included property
     * @return StockIncluded|null
    */
    public function getIncluded(): ?StockIncluded {
        return $this->included;
    }

    /**
     * Gets the links property value. The links property
     * @return ResourceLinks|null
    */
    public function getLinks(): ?ResourceLinks {
        return $this->links;
    }

    /**
     * Gets the relationships property value. The relationships property
     * @return StockRelationships|null
    */
    public function getRelationships(): ?StockRelationships {
        return $this->relationships;
    }

    /**
     * Gets the stockCode property value. The stockCode property
     * @return string|null
    */
    public function getStockCode(): ?string {
        return $this->stockCode;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('attributes', $this->getAttributes());
        $writer->writeObjectValue('included', $this->getIncluded());
        $writer->writeObjectValue('links', $this->getLinks());
        $writer->writeObjectValue('relationships', $this->getRelationships());
        $writer->writeStringValue('stockCode', $this->getStockCode());
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param StockAttributesRead|null $value Value to set for the attributes property.
    */
    public function setAttributes(?StockAttributesRead $value): void {
        $this->attributes = $value;
    }

    /**
     * Sets the included property value. The included property
     * @param StockIncluded|null $value Value to set for the included property.
    */
    public function setIncluded(?StockIncluded $value): void {
        $this->included = $value;
    }

    /**
     * Sets the links property value. The links property
     * @param ResourceLinks|null $value Value to set for the links property.
    */
    public function setLinks(?ResourceLinks $value): void {
        $this->links = $value;
    }

    /**
     * Sets the relationships property value. The relationships property
     * @param StockRelationships|null $value Value to set for the relationships property.
    */
    public function setRelationships(?StockRelationships $value): void {
        $this->relationships = $value;
    }

    /**
     * Sets the stockCode property value. The stockCode property
     * @param string|null $value Value to set for the stockCode property.
    */
    public function setStockCode(?string $value): void {
        $this->stockCode = $value;
    }

}
