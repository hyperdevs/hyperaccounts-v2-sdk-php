<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class ProjectResourceIncluded implements Parsable
{
    /**
     * @var ProjectOnlyTransactionCollection|null $projectOnlyTransactions The projectOnlyTransactions property
    */
    private ?ProjectOnlyTransactionCollection $projectOnlyTransactions = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return ProjectResourceIncluded
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): ProjectResourceIncluded {
        return new ProjectResourceIncluded();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'projectOnlyTransactions' => fn(ParseNode $n) => $o->setProjectOnlyTransactions($n->getObjectValue([ProjectOnlyTransactionCollection::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the projectOnlyTransactions property value. The projectOnlyTransactions property
     * @return ProjectOnlyTransactionCollection|null
    */
    public function getProjectOnlyTransactions(): ?ProjectOnlyTransactionCollection {
        return $this->projectOnlyTransactions;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('projectOnlyTransactions', $this->getProjectOnlyTransactions());
    }

    /**
     * Sets the projectOnlyTransactions property value. The projectOnlyTransactions property
     * @param ProjectOnlyTransactionCollection|null $value Value to set for the projectOnlyTransactions property.
    */
    public function setProjectOnlyTransactions(?ProjectOnlyTransactionCollection $value): void {
        $this->projectOnlyTransactions = $value;
    }

}
