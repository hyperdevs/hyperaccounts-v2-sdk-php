<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class GoodsDispatchNoteGetDto implements Parsable
{
    /**
     * @var GoodsDispatchNoteAttributesRead|null $attributes The attributes property
    */
    private ?GoodsDispatchNoteAttributesRead $attributes = null;

    /**
     * @var string|null $compositeKeyGdnNumberAndItemNumber The compositeKeyGdnNumberAndItemNumber property
    */
    private ?string $compositeKeyGdnNumberAndItemNumber = null;

    /**
     * @var GoodsDispatchNoteIncluded|null $included The included property
    */
    private ?GoodsDispatchNoteIncluded $included = null;

    /**
     * @var ResourceLinks|null $links The links property
    */
    private ?ResourceLinks $links = null;

    /**
     * @var GoodsDispatchNoteRelationships|null $relationships The relationships property
    */
    private ?GoodsDispatchNoteRelationships $relationships = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return GoodsDispatchNoteGetDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): GoodsDispatchNoteGetDto {
        return new GoodsDispatchNoteGetDto();
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return GoodsDispatchNoteAttributesRead|null
    */
    public function getAttributes(): ?GoodsDispatchNoteAttributesRead {
        return $this->attributes;
    }

    /**
     * Gets the compositeKeyGdnNumberAndItemNumber property value. The compositeKeyGdnNumberAndItemNumber property
     * @return string|null
    */
    public function getCompositeKeyGdnNumberAndItemNumber(): ?string {
        return $this->compositeKeyGdnNumberAndItemNumber;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([GoodsDispatchNoteAttributesRead::class, 'createFromDiscriminatorValue'])),
            'compositeKeyGdnNumberAndItemNumber' => fn(ParseNode $n) => $o->setCompositeKeyGdnNumberAndItemNumber($n->getStringValue()),
            'included' => fn(ParseNode $n) => $o->setIncluded($n->getObjectValue([GoodsDispatchNoteIncluded::class, 'createFromDiscriminatorValue'])),
            'links' => fn(ParseNode $n) => $o->setLinks($n->getObjectValue([ResourceLinks::class, 'createFromDiscriminatorValue'])),
            'relationships' => fn(ParseNode $n) => $o->setRelationships($n->getObjectValue([GoodsDispatchNoteRelationships::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the included property value. The included property
     * @return GoodsDispatchNoteIncluded|null
    */
    public function getIncluded(): ?GoodsDispatchNoteIncluded {
        return $this->included;
    }

    /**
     * Gets the links property value. The links property
     * @return ResourceLinks|null
    */
    public function getLinks(): ?ResourceLinks {
        return $this->links;
    }

    /**
     * Gets the relationships property value. The relationships property
     * @return GoodsDispatchNoteRelationships|null
    */
    public function getRelationships(): ?GoodsDispatchNoteRelationships {
        return $this->relationships;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('attributes', $this->getAttributes());
        $writer->writeObjectValue('included', $this->getIncluded());
        $writer->writeObjectValue('links', $this->getLinks());
        $writer->writeObjectValue('relationships', $this->getRelationships());
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param GoodsDispatchNoteAttributesRead|null $value Value to set for the attributes property.
    */
    public function setAttributes(?GoodsDispatchNoteAttributesRead $value): void {
        $this->attributes = $value;
    }

    /**
     * Sets the compositeKeyGdnNumberAndItemNumber property value. The compositeKeyGdnNumberAndItemNumber property
     * @param string|null $value Value to set for the compositeKeyGdnNumberAndItemNumber property.
    */
    public function setCompositeKeyGdnNumberAndItemNumber(?string $value): void {
        $this->compositeKeyGdnNumberAndItemNumber = $value;
    }

    /**
     * Sets the included property value. The included property
     * @param GoodsDispatchNoteIncluded|null $value Value to set for the included property.
    */
    public function setIncluded(?GoodsDispatchNoteIncluded $value): void {
        $this->included = $value;
    }

    /**
     * Sets the links property value. The links property
     * @param ResourceLinks|null $value Value to set for the links property.
    */
    public function setLinks(?ResourceLinks $value): void {
        $this->links = $value;
    }

    /**
     * Sets the relationships property value. The relationships property
     * @param GoodsDispatchNoteRelationships|null $value Value to set for the relationships property.
    */
    public function setRelationships(?GoodsDispatchNoteRelationships $value): void {
        $this->relationships = $value;
    }

}
