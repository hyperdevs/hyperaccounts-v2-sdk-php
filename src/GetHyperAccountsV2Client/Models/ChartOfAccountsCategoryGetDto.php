<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class ChartOfAccountsCategoryGetDto implements Parsable
{
    /**
     * @var ChartOfAccountsCategoryAttributesRead|null $attributes The attributes property
    */
    private ?ChartOfAccountsCategoryAttributesRead $attributes = null;

    /**
     * @var ChartOfAccountsCategoryIncluded|null $included The included property
    */
    private ?ChartOfAccountsCategoryIncluded $included = null;

    /**
     * @var ResourceLinks|null $links The links property
    */
    private ?ResourceLinks $links = null;

    /**
     * @var ChartOfAccountsCategoryRelationships|null $relationships The relationships property
    */
    private ?ChartOfAccountsCategoryRelationships $relationships = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return ChartOfAccountsCategoryGetDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): ChartOfAccountsCategoryGetDto {
        return new ChartOfAccountsCategoryGetDto();
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return ChartOfAccountsCategoryAttributesRead|null
    */
    public function getAttributes(): ?ChartOfAccountsCategoryAttributesRead {
        return $this->attributes;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([ChartOfAccountsCategoryAttributesRead::class, 'createFromDiscriminatorValue'])),
            'included' => fn(ParseNode $n) => $o->setIncluded($n->getObjectValue([ChartOfAccountsCategoryIncluded::class, 'createFromDiscriminatorValue'])),
            'links' => fn(ParseNode $n) => $o->setLinks($n->getObjectValue([ResourceLinks::class, 'createFromDiscriminatorValue'])),
            'relationships' => fn(ParseNode $n) => $o->setRelationships($n->getObjectValue([ChartOfAccountsCategoryRelationships::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the included property value. The included property
     * @return ChartOfAccountsCategoryIncluded|null
    */
    public function getIncluded(): ?ChartOfAccountsCategoryIncluded {
        return $this->included;
    }

    /**
     * Gets the links property value. The links property
     * @return ResourceLinks|null
    */
    public function getLinks(): ?ResourceLinks {
        return $this->links;
    }

    /**
     * Gets the relationships property value. The relationships property
     * @return ChartOfAccountsCategoryRelationships|null
    */
    public function getRelationships(): ?ChartOfAccountsCategoryRelationships {
        return $this->relationships;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('attributes', $this->getAttributes());
        $writer->writeObjectValue('included', $this->getIncluded());
        $writer->writeObjectValue('links', $this->getLinks());
        $writer->writeObjectValue('relationships', $this->getRelationships());
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param ChartOfAccountsCategoryAttributesRead|null $value Value to set for the attributes property.
    */
    public function setAttributes(?ChartOfAccountsCategoryAttributesRead $value): void {
        $this->attributes = $value;
    }

    /**
     * Sets the included property value. The included property
     * @param ChartOfAccountsCategoryIncluded|null $value Value to set for the included property.
    */
    public function setIncluded(?ChartOfAccountsCategoryIncluded $value): void {
        $this->included = $value;
    }

    /**
     * Sets the links property value. The links property
     * @param ResourceLinks|null $value Value to set for the links property.
    */
    public function setLinks(?ResourceLinks $value): void {
        $this->links = $value;
    }

    /**
     * Sets the relationships property value. The relationships property
     * @param ChartOfAccountsCategoryRelationships|null $value Value to set for the relationships property.
    */
    public function setRelationships(?ChartOfAccountsCategoryRelationships $value): void {
        $this->relationships = $value;
    }

}
