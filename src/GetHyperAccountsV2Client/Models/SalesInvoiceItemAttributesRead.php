<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use DateTime;
use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class SalesInvoiceItemAttributesRead implements Parsable
{
    /**
     * @var float|null $addDiscRate The addDiscRate property
    */
    private ?float $addDiscRate = null;

    /**
     * @var string|null $comment1 The comment1 property
    */
    private ?string $comment1 = null;

    /**
     * @var string|null $comment2 The comment2 property
    */
    private ?string $comment2 = null;

    /**
     * @var DateTime|null $deliveryDate The deliveryDate property
    */
    private ?DateTime $deliveryDate = null;

    /**
     * @var string|null $deptName The deptName property
    */
    private ?string $deptName = null;

    /**
     * @var int|null $deptNumber The deptNumber property
    */
    private ?int $deptNumber = null;

    /**
     * @var string|null $description The description property
    */
    private ?string $description = null;

    /**
     * @var float|null $discountAmount The discountAmount property
    */
    private ?float $discountAmount = null;

    /**
     * @var float|null $discountRate The discountRate property
    */
    private ?float $discountRate = null;

    /**
     * @var int|null $ecVatDescriptionId The ecVatDescriptionId property
    */
    private ?int $ecVatDescriptionId = null;

    /**
     * @var int|null $extOrderLineRef The extOrderLineRef property
    */
    private ?int $extOrderLineRef = null;

    /**
     * @var string|null $extOrderRef The extOrderRef property
    */
    private ?string $extOrderRef = null;

    /**
     * @var float|null $foreignDiscountAmount The foreignDiscountAmount property
    */
    private ?float $foreignDiscountAmount = null;

    /**
     * @var float|null $foreignFullNetAmount The foreignFullNetAmount property
    */
    private ?float $foreignFullNetAmount = null;

    /**
     * @var float|null $foreignGrossAmount The foreignGrossAmount property
    */
    private ?float $foreignGrossAmount = null;

    /**
     * @var float|null $foreignNetAmount The foreignNetAmount property
    */
    private ?float $foreignNetAmount = null;

    /**
     * @var float|null $foreignTaxAmount The foreignTaxAmount property
    */
    private ?float $foreignTaxAmount = null;

    /**
     * @var float|null $foreignUnitPrice The foreignUnitPrice property
    */
    private ?float $foreignUnitPrice = null;

    /**
     * @var float|null $fullNetAmount The fullNetAmount property
    */
    private ?float $fullNetAmount = null;

    /**
     * @var int|null $generatedMessage The generatedMessage property
    */
    private ?int $generatedMessage = null;

    /**
     * @var float|null $grossAmount The grossAmount property
    */
    private ?float $grossAmount = null;

    /**
     * @var int|null $itemNumber The itemNumber property
    */
    private ?int $itemNumber = null;

    /**
     * @var string|null $jobNumber The jobNumber property
    */
    private ?string $jobNumber = null;

    /**
     * @var float|null $negotiationDiscNet The negotiationDiscNet property
    */
    private ?float $negotiationDiscNet = null;

    /**
     * @var float|null $negotiationDiscNetBase The negotiationDiscNetBase property
    */
    private ?float $negotiationDiscNetBase = null;

    /**
     * @var float|null $netAmount The netAmount property
    */
    private ?float $netAmount = null;

    /**
     * @var float|null $quantity The quantity property
    */
    private ?float $quantity = null;

    /**
     * @var DateTime|null $recordCreateDate The recordCreateDate property
    */
    private ?DateTime $recordCreateDate = null;

    /**
     * @var int|null $recordDeleted The recordDeleted property
    */
    private ?int $recordDeleted = null;

    /**
     * @var DateTime|null $recordModifyDate The recordModifyDate property
    */
    private ?DateTime $recordModifyDate = null;

    /**
     * @var int|null $serviceFlag The serviceFlag property
    */
    private ?int $serviceFlag = null;

    /**
     * @var float|null $taxAmount The taxAmount property
    */
    private ?float $taxAmount = null;

    /**
     * @var SalesInvoiceItemAttributesRead_taxCode|null $taxCode The taxCode property
    */
    private ?SalesInvoiceItemAttributesRead_taxCode $taxCode = null;

    /**
     * @var int|null $taxCodeId The taxCodeId property
    */
    private ?int $taxCodeId = null;

    /**
     * @var float|null $taxRate The taxRate property
    */
    private ?float $taxRate = null;

    /**
     * @var string|null $text The text property
    */
    private ?string $text = null;

    /**
     * @var string|null $unitOfSale The unitOfSale property
    */
    private ?string $unitOfSale = null;

    /**
     * @var float|null $unitPrice The unitPrice property
    */
    private ?float $unitPrice = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return SalesInvoiceItemAttributesRead
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): SalesInvoiceItemAttributesRead {
        return new SalesInvoiceItemAttributesRead();
    }

    /**
     * Gets the addDiscRate property value. The addDiscRate property
     * @return float|null
    */
    public function getAddDiscRate(): ?float {
        return $this->addDiscRate;
    }

    /**
     * Gets the comment1 property value. The comment1 property
     * @return string|null
    */
    public function getComment1(): ?string {
        return $this->comment1;
    }

    /**
     * Gets the comment2 property value. The comment2 property
     * @return string|null
    */
    public function getComment2(): ?string {
        return $this->comment2;
    }

    /**
     * Gets the deliveryDate property value. The deliveryDate property
     * @return DateTime|null
    */
    public function getDeliveryDate(): ?DateTime {
        return $this->deliveryDate;
    }

    /**
     * Gets the deptName property value. The deptName property
     * @return string|null
    */
    public function getDeptName(): ?string {
        return $this->deptName;
    }

    /**
     * Gets the deptNumber property value. The deptNumber property
     * @return int|null
    */
    public function getDeptNumber(): ?int {
        return $this->deptNumber;
    }

    /**
     * Gets the description property value. The description property
     * @return string|null
    */
    public function getDescription(): ?string {
        return $this->description;
    }

    /**
     * Gets the discountAmount property value. The discountAmount property
     * @return float|null
    */
    public function getDiscountAmount(): ?float {
        return $this->discountAmount;
    }

    /**
     * Gets the discountRate property value. The discountRate property
     * @return float|null
    */
    public function getDiscountRate(): ?float {
        return $this->discountRate;
    }

    /**
     * Gets the ecVatDescriptionId property value. The ecVatDescriptionId property
     * @return int|null
    */
    public function getEcVatDescriptionId(): ?int {
        return $this->ecVatDescriptionId;
    }

    /**
     * Gets the extOrderLineRef property value. The extOrderLineRef property
     * @return int|null
    */
    public function getExtOrderLineRef(): ?int {
        return $this->extOrderLineRef;
    }

    /**
     * Gets the extOrderRef property value. The extOrderRef property
     * @return string|null
    */
    public function getExtOrderRef(): ?string {
        return $this->extOrderRef;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'addDiscRate' => fn(ParseNode $n) => $o->setAddDiscRate($n->getFloatValue()),
            'comment1' => fn(ParseNode $n) => $o->setComment1($n->getStringValue()),
            'comment2' => fn(ParseNode $n) => $o->setComment2($n->getStringValue()),
            'deliveryDate' => fn(ParseNode $n) => $o->setDeliveryDate($n->getDateTimeValue()),
            'deptName' => fn(ParseNode $n) => $o->setDeptName($n->getStringValue()),
            'deptNumber' => fn(ParseNode $n) => $o->setDeptNumber($n->getIntegerValue()),
            'description' => fn(ParseNode $n) => $o->setDescription($n->getStringValue()),
            'discountAmount' => fn(ParseNode $n) => $o->setDiscountAmount($n->getFloatValue()),
            'discountRate' => fn(ParseNode $n) => $o->setDiscountRate($n->getFloatValue()),
            'ecVatDescriptionId' => fn(ParseNode $n) => $o->setEcVatDescriptionId($n->getIntegerValue()),
            'extOrderLineRef' => fn(ParseNode $n) => $o->setExtOrderLineRef($n->getIntegerValue()),
            'extOrderRef' => fn(ParseNode $n) => $o->setExtOrderRef($n->getStringValue()),
            'foreignDiscountAmount' => fn(ParseNode $n) => $o->setForeignDiscountAmount($n->getFloatValue()),
            'foreignFullNetAmount' => fn(ParseNode $n) => $o->setForeignFullNetAmount($n->getFloatValue()),
            'foreignGrossAmount' => fn(ParseNode $n) => $o->setForeignGrossAmount($n->getFloatValue()),
            'foreignNetAmount' => fn(ParseNode $n) => $o->setForeignNetAmount($n->getFloatValue()),
            'foreignTaxAmount' => fn(ParseNode $n) => $o->setForeignTaxAmount($n->getFloatValue()),
            'foreignUnitPrice' => fn(ParseNode $n) => $o->setForeignUnitPrice($n->getFloatValue()),
            'fullNetAmount' => fn(ParseNode $n) => $o->setFullNetAmount($n->getFloatValue()),
            'generatedMessage' => fn(ParseNode $n) => $o->setGeneratedMessage($n->getIntegerValue()),
            'grossAmount' => fn(ParseNode $n) => $o->setGrossAmount($n->getFloatValue()),
            'itemNumber' => fn(ParseNode $n) => $o->setItemNumber($n->getIntegerValue()),
            'jobNumber' => fn(ParseNode $n) => $o->setJobNumber($n->getStringValue()),
            'negotiationDiscNet' => fn(ParseNode $n) => $o->setNegotiationDiscNet($n->getFloatValue()),
            'negotiationDiscNetBase' => fn(ParseNode $n) => $o->setNegotiationDiscNetBase($n->getFloatValue()),
            'netAmount' => fn(ParseNode $n) => $o->setNetAmount($n->getFloatValue()),
            'quantity' => fn(ParseNode $n) => $o->setQuantity($n->getFloatValue()),
            'recordCreateDate' => fn(ParseNode $n) => $o->setRecordCreateDate($n->getDateTimeValue()),
            'recordDeleted' => fn(ParseNode $n) => $o->setRecordDeleted($n->getIntegerValue()),
            'recordModifyDate' => fn(ParseNode $n) => $o->setRecordModifyDate($n->getDateTimeValue()),
            'serviceFlag' => fn(ParseNode $n) => $o->setServiceFlag($n->getIntegerValue()),
            'taxAmount' => fn(ParseNode $n) => $o->setTaxAmount($n->getFloatValue()),
            'taxCode' => fn(ParseNode $n) => $o->setTaxCode($n->getEnumValue(SalesInvoiceItemAttributesRead_taxCode::class)),
            'taxCodeId' => fn(ParseNode $n) => $o->setTaxCodeId($n->getIntegerValue()),
            'taxRate' => fn(ParseNode $n) => $o->setTaxRate($n->getFloatValue()),
            'text' => fn(ParseNode $n) => $o->setText($n->getStringValue()),
            'unitOfSale' => fn(ParseNode $n) => $o->setUnitOfSale($n->getStringValue()),
            'unitPrice' => fn(ParseNode $n) => $o->setUnitPrice($n->getFloatValue()),
        ];
    }

    /**
     * Gets the foreignDiscountAmount property value. The foreignDiscountAmount property
     * @return float|null
    */
    public function getForeignDiscountAmount(): ?float {
        return $this->foreignDiscountAmount;
    }

    /**
     * Gets the foreignFullNetAmount property value. The foreignFullNetAmount property
     * @return float|null
    */
    public function getForeignFullNetAmount(): ?float {
        return $this->foreignFullNetAmount;
    }

    /**
     * Gets the foreignGrossAmount property value. The foreignGrossAmount property
     * @return float|null
    */
    public function getForeignGrossAmount(): ?float {
        return $this->foreignGrossAmount;
    }

    /**
     * Gets the foreignNetAmount property value. The foreignNetAmount property
     * @return float|null
    */
    public function getForeignNetAmount(): ?float {
        return $this->foreignNetAmount;
    }

    /**
     * Gets the foreignTaxAmount property value. The foreignTaxAmount property
     * @return float|null
    */
    public function getForeignTaxAmount(): ?float {
        return $this->foreignTaxAmount;
    }

    /**
     * Gets the foreignUnitPrice property value. The foreignUnitPrice property
     * @return float|null
    */
    public function getForeignUnitPrice(): ?float {
        return $this->foreignUnitPrice;
    }

    /**
     * Gets the fullNetAmount property value. The fullNetAmount property
     * @return float|null
    */
    public function getFullNetAmount(): ?float {
        return $this->fullNetAmount;
    }

    /**
     * Gets the generatedMessage property value. The generatedMessage property
     * @return int|null
    */
    public function getGeneratedMessage(): ?int {
        return $this->generatedMessage;
    }

    /**
     * Gets the grossAmount property value. The grossAmount property
     * @return float|null
    */
    public function getGrossAmount(): ?float {
        return $this->grossAmount;
    }

    /**
     * Gets the itemNumber property value. The itemNumber property
     * @return int|null
    */
    public function getItemNumber(): ?int {
        return $this->itemNumber;
    }

    /**
     * Gets the jobNumber property value. The jobNumber property
     * @return string|null
    */
    public function getJobNumber(): ?string {
        return $this->jobNumber;
    }

    /**
     * Gets the negotiationDiscNet property value. The negotiationDiscNet property
     * @return float|null
    */
    public function getNegotiationDiscNet(): ?float {
        return $this->negotiationDiscNet;
    }

    /**
     * Gets the negotiationDiscNetBase property value. The negotiationDiscNetBase property
     * @return float|null
    */
    public function getNegotiationDiscNetBase(): ?float {
        return $this->negotiationDiscNetBase;
    }

    /**
     * Gets the netAmount property value. The netAmount property
     * @return float|null
    */
    public function getNetAmount(): ?float {
        return $this->netAmount;
    }

    /**
     * Gets the quantity property value. The quantity property
     * @return float|null
    */
    public function getQuantity(): ?float {
        return $this->quantity;
    }

    /**
     * Gets the recordCreateDate property value. The recordCreateDate property
     * @return DateTime|null
    */
    public function getRecordCreateDate(): ?DateTime {
        return $this->recordCreateDate;
    }

    /**
     * Gets the recordDeleted property value. The recordDeleted property
     * @return int|null
    */
    public function getRecordDeleted(): ?int {
        return $this->recordDeleted;
    }

    /**
     * Gets the recordModifyDate property value. The recordModifyDate property
     * @return DateTime|null
    */
    public function getRecordModifyDate(): ?DateTime {
        return $this->recordModifyDate;
    }

    /**
     * Gets the serviceFlag property value. The serviceFlag property
     * @return int|null
    */
    public function getServiceFlag(): ?int {
        return $this->serviceFlag;
    }

    /**
     * Gets the taxAmount property value. The taxAmount property
     * @return float|null
    */
    public function getTaxAmount(): ?float {
        return $this->taxAmount;
    }

    /**
     * Gets the taxCode property value. The taxCode property
     * @return SalesInvoiceItemAttributesRead_taxCode|null
    */
    public function getTaxCode(): ?SalesInvoiceItemAttributesRead_taxCode {
        return $this->taxCode;
    }

    /**
     * Gets the taxCodeId property value. The taxCodeId property
     * @return int|null
    */
    public function getTaxCodeId(): ?int {
        return $this->taxCodeId;
    }

    /**
     * Gets the taxRate property value. The taxRate property
     * @return float|null
    */
    public function getTaxRate(): ?float {
        return $this->taxRate;
    }

    /**
     * Gets the text property value. The text property
     * @return string|null
    */
    public function getText(): ?string {
        return $this->text;
    }

    /**
     * Gets the unitOfSale property value. The unitOfSale property
     * @return string|null
    */
    public function getUnitOfSale(): ?string {
        return $this->unitOfSale;
    }

    /**
     * Gets the unitPrice property value. The unitPrice property
     * @return float|null
    */
    public function getUnitPrice(): ?float {
        return $this->unitPrice;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeFloatValue('addDiscRate', $this->getAddDiscRate());
        $writer->writeStringValue('comment1', $this->getComment1());
        $writer->writeStringValue('comment2', $this->getComment2());
        $writer->writeDateTimeValue('deliveryDate', $this->getDeliveryDate());
        $writer->writeStringValue('deptName', $this->getDeptName());
        $writer->writeIntegerValue('deptNumber', $this->getDeptNumber());
        $writer->writeStringValue('description', $this->getDescription());
        $writer->writeFloatValue('discountAmount', $this->getDiscountAmount());
        $writer->writeFloatValue('discountRate', $this->getDiscountRate());
        $writer->writeIntegerValue('ecVatDescriptionId', $this->getEcVatDescriptionId());
        $writer->writeIntegerValue('extOrderLineRef', $this->getExtOrderLineRef());
        $writer->writeStringValue('extOrderRef', $this->getExtOrderRef());
        $writer->writeFloatValue('foreignDiscountAmount', $this->getForeignDiscountAmount());
        $writer->writeFloatValue('foreignFullNetAmount', $this->getForeignFullNetAmount());
        $writer->writeFloatValue('foreignGrossAmount', $this->getForeignGrossAmount());
        $writer->writeFloatValue('foreignNetAmount', $this->getForeignNetAmount());
        $writer->writeFloatValue('foreignTaxAmount', $this->getForeignTaxAmount());
        $writer->writeFloatValue('foreignUnitPrice', $this->getForeignUnitPrice());
        $writer->writeFloatValue('fullNetAmount', $this->getFullNetAmount());
        $writer->writeIntegerValue('generatedMessage', $this->getGeneratedMessage());
        $writer->writeFloatValue('grossAmount', $this->getGrossAmount());
        $writer->writeIntegerValue('itemNumber', $this->getItemNumber());
        $writer->writeStringValue('jobNumber', $this->getJobNumber());
        $writer->writeFloatValue('negotiationDiscNet', $this->getNegotiationDiscNet());
        $writer->writeFloatValue('negotiationDiscNetBase', $this->getNegotiationDiscNetBase());
        $writer->writeFloatValue('netAmount', $this->getNetAmount());
        $writer->writeFloatValue('quantity', $this->getQuantity());
        $writer->writeDateTimeValue('recordCreateDate', $this->getRecordCreateDate());
        $writer->writeIntegerValue('recordDeleted', $this->getRecordDeleted());
        $writer->writeDateTimeValue('recordModifyDate', $this->getRecordModifyDate());
        $writer->writeIntegerValue('serviceFlag', $this->getServiceFlag());
        $writer->writeFloatValue('taxAmount', $this->getTaxAmount());
        $writer->writeEnumValue('taxCode', $this->getTaxCode());
        $writer->writeIntegerValue('taxCodeId', $this->getTaxCodeId());
        $writer->writeFloatValue('taxRate', $this->getTaxRate());
        $writer->writeStringValue('text', $this->getText());
        $writer->writeStringValue('unitOfSale', $this->getUnitOfSale());
        $writer->writeFloatValue('unitPrice', $this->getUnitPrice());
    }

    /**
     * Sets the addDiscRate property value. The addDiscRate property
     * @param float|null $value Value to set for the addDiscRate property.
    */
    public function setAddDiscRate(?float $value): void {
        $this->addDiscRate = $value;
    }

    /**
     * Sets the comment1 property value. The comment1 property
     * @param string|null $value Value to set for the comment1 property.
    */
    public function setComment1(?string $value): void {
        $this->comment1 = $value;
    }

    /**
     * Sets the comment2 property value. The comment2 property
     * @param string|null $value Value to set for the comment2 property.
    */
    public function setComment2(?string $value): void {
        $this->comment2 = $value;
    }

    /**
     * Sets the deliveryDate property value. The deliveryDate property
     * @param DateTime|null $value Value to set for the deliveryDate property.
    */
    public function setDeliveryDate(?DateTime $value): void {
        $this->deliveryDate = $value;
    }

    /**
     * Sets the deptName property value. The deptName property
     * @param string|null $value Value to set for the deptName property.
    */
    public function setDeptName(?string $value): void {
        $this->deptName = $value;
    }

    /**
     * Sets the deptNumber property value. The deptNumber property
     * @param int|null $value Value to set for the deptNumber property.
    */
    public function setDeptNumber(?int $value): void {
        $this->deptNumber = $value;
    }

    /**
     * Sets the description property value. The description property
     * @param string|null $value Value to set for the description property.
    */
    public function setDescription(?string $value): void {
        $this->description = $value;
    }

    /**
     * Sets the discountAmount property value. The discountAmount property
     * @param float|null $value Value to set for the discountAmount property.
    */
    public function setDiscountAmount(?float $value): void {
        $this->discountAmount = $value;
    }

    /**
     * Sets the discountRate property value. The discountRate property
     * @param float|null $value Value to set for the discountRate property.
    */
    public function setDiscountRate(?float $value): void {
        $this->discountRate = $value;
    }

    /**
     * Sets the ecVatDescriptionId property value. The ecVatDescriptionId property
     * @param int|null $value Value to set for the ecVatDescriptionId property.
    */
    public function setEcVatDescriptionId(?int $value): void {
        $this->ecVatDescriptionId = $value;
    }

    /**
     * Sets the extOrderLineRef property value. The extOrderLineRef property
     * @param int|null $value Value to set for the extOrderLineRef property.
    */
    public function setExtOrderLineRef(?int $value): void {
        $this->extOrderLineRef = $value;
    }

    /**
     * Sets the extOrderRef property value. The extOrderRef property
     * @param string|null $value Value to set for the extOrderRef property.
    */
    public function setExtOrderRef(?string $value): void {
        $this->extOrderRef = $value;
    }

    /**
     * Sets the foreignDiscountAmount property value. The foreignDiscountAmount property
     * @param float|null $value Value to set for the foreignDiscountAmount property.
    */
    public function setForeignDiscountAmount(?float $value): void {
        $this->foreignDiscountAmount = $value;
    }

    /**
     * Sets the foreignFullNetAmount property value. The foreignFullNetAmount property
     * @param float|null $value Value to set for the foreignFullNetAmount property.
    */
    public function setForeignFullNetAmount(?float $value): void {
        $this->foreignFullNetAmount = $value;
    }

    /**
     * Sets the foreignGrossAmount property value. The foreignGrossAmount property
     * @param float|null $value Value to set for the foreignGrossAmount property.
    */
    public function setForeignGrossAmount(?float $value): void {
        $this->foreignGrossAmount = $value;
    }

    /**
     * Sets the foreignNetAmount property value. The foreignNetAmount property
     * @param float|null $value Value to set for the foreignNetAmount property.
    */
    public function setForeignNetAmount(?float $value): void {
        $this->foreignNetAmount = $value;
    }

    /**
     * Sets the foreignTaxAmount property value. The foreignTaxAmount property
     * @param float|null $value Value to set for the foreignTaxAmount property.
    */
    public function setForeignTaxAmount(?float $value): void {
        $this->foreignTaxAmount = $value;
    }

    /**
     * Sets the foreignUnitPrice property value. The foreignUnitPrice property
     * @param float|null $value Value to set for the foreignUnitPrice property.
    */
    public function setForeignUnitPrice(?float $value): void {
        $this->foreignUnitPrice = $value;
    }

    /**
     * Sets the fullNetAmount property value. The fullNetAmount property
     * @param float|null $value Value to set for the fullNetAmount property.
    */
    public function setFullNetAmount(?float $value): void {
        $this->fullNetAmount = $value;
    }

    /**
     * Sets the generatedMessage property value. The generatedMessage property
     * @param int|null $value Value to set for the generatedMessage property.
    */
    public function setGeneratedMessage(?int $value): void {
        $this->generatedMessage = $value;
    }

    /**
     * Sets the grossAmount property value. The grossAmount property
     * @param float|null $value Value to set for the grossAmount property.
    */
    public function setGrossAmount(?float $value): void {
        $this->grossAmount = $value;
    }

    /**
     * Sets the itemNumber property value. The itemNumber property
     * @param int|null $value Value to set for the itemNumber property.
    */
    public function setItemNumber(?int $value): void {
        $this->itemNumber = $value;
    }

    /**
     * Sets the jobNumber property value. The jobNumber property
     * @param string|null $value Value to set for the jobNumber property.
    */
    public function setJobNumber(?string $value): void {
        $this->jobNumber = $value;
    }

    /**
     * Sets the negotiationDiscNet property value. The negotiationDiscNet property
     * @param float|null $value Value to set for the negotiationDiscNet property.
    */
    public function setNegotiationDiscNet(?float $value): void {
        $this->negotiationDiscNet = $value;
    }

    /**
     * Sets the negotiationDiscNetBase property value. The negotiationDiscNetBase property
     * @param float|null $value Value to set for the negotiationDiscNetBase property.
    */
    public function setNegotiationDiscNetBase(?float $value): void {
        $this->negotiationDiscNetBase = $value;
    }

    /**
     * Sets the netAmount property value. The netAmount property
     * @param float|null $value Value to set for the netAmount property.
    */
    public function setNetAmount(?float $value): void {
        $this->netAmount = $value;
    }

    /**
     * Sets the quantity property value. The quantity property
     * @param float|null $value Value to set for the quantity property.
    */
    public function setQuantity(?float $value): void {
        $this->quantity = $value;
    }

    /**
     * Sets the recordCreateDate property value. The recordCreateDate property
     * @param DateTime|null $value Value to set for the recordCreateDate property.
    */
    public function setRecordCreateDate(?DateTime $value): void {
        $this->recordCreateDate = $value;
    }

    /**
     * Sets the recordDeleted property value. The recordDeleted property
     * @param int|null $value Value to set for the recordDeleted property.
    */
    public function setRecordDeleted(?int $value): void {
        $this->recordDeleted = $value;
    }

    /**
     * Sets the recordModifyDate property value. The recordModifyDate property
     * @param DateTime|null $value Value to set for the recordModifyDate property.
    */
    public function setRecordModifyDate(?DateTime $value): void {
        $this->recordModifyDate = $value;
    }

    /**
     * Sets the serviceFlag property value. The serviceFlag property
     * @param int|null $value Value to set for the serviceFlag property.
    */
    public function setServiceFlag(?int $value): void {
        $this->serviceFlag = $value;
    }

    /**
     * Sets the taxAmount property value. The taxAmount property
     * @param float|null $value Value to set for the taxAmount property.
    */
    public function setTaxAmount(?float $value): void {
        $this->taxAmount = $value;
    }

    /**
     * Sets the taxCode property value. The taxCode property
     * @param SalesInvoiceItemAttributesRead_taxCode|null $value Value to set for the taxCode property.
    */
    public function setTaxCode(?SalesInvoiceItemAttributesRead_taxCode $value): void {
        $this->taxCode = $value;
    }

    /**
     * Sets the taxCodeId property value. The taxCodeId property
     * @param int|null $value Value to set for the taxCodeId property.
    */
    public function setTaxCodeId(?int $value): void {
        $this->taxCodeId = $value;
    }

    /**
     * Sets the taxRate property value. The taxRate property
     * @param float|null $value Value to set for the taxRate property.
    */
    public function setTaxRate(?float $value): void {
        $this->taxRate = $value;
    }

    /**
     * Sets the text property value. The text property
     * @param string|null $value Value to set for the text property.
    */
    public function setText(?string $value): void {
        $this->text = $value;
    }

    /**
     * Sets the unitOfSale property value. The unitOfSale property
     * @param string|null $value Value to set for the unitOfSale property.
    */
    public function setUnitOfSale(?string $value): void {
        $this->unitOfSale = $value;
    }

    /**
     * Sets the unitPrice property value. The unitPrice property
     * @param float|null $value Value to set for the unitPrice property.
    */
    public function setUnitPrice(?float $value): void {
        $this->unitPrice = $value;
    }

}
