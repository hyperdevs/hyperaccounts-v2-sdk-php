<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class ProjectStatusGetDto implements Parsable
{
    /**
     * @var ProjectStatusAttributesRead|null $attributes The attributes property
    */
    private ?ProjectStatusAttributesRead $attributes = null;

    /**
     * @var ProjectStatusIncluded|null $included The included property
    */
    private ?ProjectStatusIncluded $included = null;

    /**
     * @var ResourceLinks|null $links The links property
    */
    private ?ResourceLinks $links = null;

    /**
     * @var ProjectStatusRelationships|null $relationships The relationships property
    */
    private ?ProjectStatusRelationships $relationships = null;

    /**
     * @var int|null $statusId The statusId property
    */
    private ?int $statusId = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return ProjectStatusGetDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): ProjectStatusGetDto {
        return new ProjectStatusGetDto();
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return ProjectStatusAttributesRead|null
    */
    public function getAttributes(): ?ProjectStatusAttributesRead {
        return $this->attributes;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([ProjectStatusAttributesRead::class, 'createFromDiscriminatorValue'])),
            'included' => fn(ParseNode $n) => $o->setIncluded($n->getObjectValue([ProjectStatusIncluded::class, 'createFromDiscriminatorValue'])),
            'links' => fn(ParseNode $n) => $o->setLinks($n->getObjectValue([ResourceLinks::class, 'createFromDiscriminatorValue'])),
            'relationships' => fn(ParseNode $n) => $o->setRelationships($n->getObjectValue([ProjectStatusRelationships::class, 'createFromDiscriminatorValue'])),
            'statusId' => fn(ParseNode $n) => $o->setStatusId($n->getIntegerValue()),
        ];
    }

    /**
     * Gets the included property value. The included property
     * @return ProjectStatusIncluded|null
    */
    public function getIncluded(): ?ProjectStatusIncluded {
        return $this->included;
    }

    /**
     * Gets the links property value. The links property
     * @return ResourceLinks|null
    */
    public function getLinks(): ?ResourceLinks {
        return $this->links;
    }

    /**
     * Gets the relationships property value. The relationships property
     * @return ProjectStatusRelationships|null
    */
    public function getRelationships(): ?ProjectStatusRelationships {
        return $this->relationships;
    }

    /**
     * Gets the statusId property value. The statusId property
     * @return int|null
    */
    public function getStatusId(): ?int {
        return $this->statusId;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('attributes', $this->getAttributes());
        $writer->writeObjectValue('included', $this->getIncluded());
        $writer->writeObjectValue('links', $this->getLinks());
        $writer->writeObjectValue('relationships', $this->getRelationships());
        $writer->writeIntegerValue('statusId', $this->getStatusId());
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param ProjectStatusAttributesRead|null $value Value to set for the attributes property.
    */
    public function setAttributes(?ProjectStatusAttributesRead $value): void {
        $this->attributes = $value;
    }

    /**
     * Sets the included property value. The included property
     * @param ProjectStatusIncluded|null $value Value to set for the included property.
    */
    public function setIncluded(?ProjectStatusIncluded $value): void {
        $this->included = $value;
    }

    /**
     * Sets the links property value. The links property
     * @param ResourceLinks|null $value Value to set for the links property.
    */
    public function setLinks(?ResourceLinks $value): void {
        $this->links = $value;
    }

    /**
     * Sets the relationships property value. The relationships property
     * @param ProjectStatusRelationships|null $value Value to set for the relationships property.
    */
    public function setRelationships(?ProjectStatusRelationships $value): void {
        $this->relationships = $value;
    }

    /**
     * Sets the statusId property value. The statusId property
     * @param int|null $value Value to set for the statusId property.
    */
    public function setStatusId(?int $value): void {
        $this->statusId = $value;
    }

}
