<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class ChartOfAccountsCategoryTitleIncluded implements Parsable
{
    /**
     * @var ChartOfAccountsCategoryCollection|null $categories The categories property
    */
    private ?ChartOfAccountsCategoryCollection $categories = null;

    /**
     * @var ChartOfAccountsGetDto|null $chart The chart property
    */
    private ?ChartOfAccountsGetDto $chart = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return ChartOfAccountsCategoryTitleIncluded
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): ChartOfAccountsCategoryTitleIncluded {
        return new ChartOfAccountsCategoryTitleIncluded();
    }

    /**
     * Gets the categories property value. The categories property
     * @return ChartOfAccountsCategoryCollection|null
    */
    public function getCategories(): ?ChartOfAccountsCategoryCollection {
        return $this->categories;
    }

    /**
     * Gets the chart property value. The chart property
     * @return ChartOfAccountsGetDto|null
    */
    public function getChart(): ?ChartOfAccountsGetDto {
        return $this->chart;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'categories' => fn(ParseNode $n) => $o->setCategories($n->getObjectValue([ChartOfAccountsCategoryCollection::class, 'createFromDiscriminatorValue'])),
            'chart' => fn(ParseNode $n) => $o->setChart($n->getObjectValue([ChartOfAccountsGetDto::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('categories', $this->getCategories());
        $writer->writeObjectValue('chart', $this->getChart());
    }

    /**
     * Sets the categories property value. The categories property
     * @param ChartOfAccountsCategoryCollection|null $value Value to set for the categories property.
    */
    public function setCategories(?ChartOfAccountsCategoryCollection $value): void {
        $this->categories = $value;
    }

    /**
     * Sets the chart property value. The chart property
     * @param ChartOfAccountsGetDto|null $value Value to set for the chart property.
    */
    public function setChart(?ChartOfAccountsGetDto $value): void {
        $this->chart = $value;
    }

}
