<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use DateTime;
use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class FixedAssetAttributesRead implements Parsable
{
    /**
     * @var int|null $assetCat The assetCat property
    */
    private ?int $assetCat = null;

    /**
     * @var string|null $balSheetNomCode The balSheetNomCode property
    */
    private ?string $balSheetNomCode = null;

    /**
     * @var float|null $costPrice The costPrice property
    */
    private ?float $costPrice = null;

    /**
     * @var float|null $depLast The depLast property
    */
    private ?float $depLast = null;

    /**
     * @var string|null $depMethod The depMethod property
    */
    private ?string $depMethod = null;

    /**
     * @var int|null $depMethodCode The depMethodCode property
    */
    private ?int $depMethodCode = null;

    /**
     * @var float|null $depNext The depNext property
    */
    private ?float $depNext = null;

    /**
     * @var float|null $depRate The depRate property
    */
    private ?float $depRate = null;

    /**
     * @var string|null $deptName The deptName property
    */
    private ?string $deptName = null;

    /**
     * @var int|null $deptNumber The deptNumber property
    */
    private ?int $deptNumber = null;

    /**
     * @var float|null $depToDate The depToDate property
    */
    private ?float $depToDate = null;

    /**
     * @var string|null $details1 The details1 property
    */
    private ?string $details1 = null;

    /**
     * @var string|null $details2 The details2 property
    */
    private ?string $details2 = null;

    /**
     * @var string|null $details3 The details3 property
    */
    private ?string $details3 = null;

    /**
     * @var string|null $employee The employee property
    */
    private ?string $employee = null;

    /**
     * @var float|null $netBook The netBook property
    */
    private ?float $netBook = null;

    /**
     * @var string|null $profitLossNomCode The profitLossNomCode property
    */
    private ?string $profitLossNomCode = null;

    /**
     * @var DateTime|null $purchaseDate The purchaseDate property
    */
    private ?DateTime $purchaseDate = null;

    /**
     * @var string|null $purchaseRef The purchaseRef property
    */
    private ?string $purchaseRef = null;

    /**
     * @var DateTime|null $recordCreateDate The recordCreateDate property
    */
    private ?DateTime $recordCreateDate = null;

    /**
     * @var int|null $recordDeleted The recordDeleted property
    */
    private ?int $recordDeleted = null;

    /**
     * @var DateTime|null $recordModifyDate The recordModifyDate property
    */
    private ?DateTime $recordModifyDate = null;

    /**
     * @var string|null $serialNumber The serialNumber property
    */
    private ?string $serialNumber = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return FixedAssetAttributesRead
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): FixedAssetAttributesRead {
        return new FixedAssetAttributesRead();
    }

    /**
     * Gets the assetCat property value. The assetCat property
     * @return int|null
    */
    public function getAssetCat(): ?int {
        return $this->assetCat;
    }

    /**
     * Gets the balSheetNomCode property value. The balSheetNomCode property
     * @return string|null
    */
    public function getBalSheetNomCode(): ?string {
        return $this->balSheetNomCode;
    }

    /**
     * Gets the costPrice property value. The costPrice property
     * @return float|null
    */
    public function getCostPrice(): ?float {
        return $this->costPrice;
    }

    /**
     * Gets the depLast property value. The depLast property
     * @return float|null
    */
    public function getDepLast(): ?float {
        return $this->depLast;
    }

    /**
     * Gets the depMethod property value. The depMethod property
     * @return string|null
    */
    public function getDepMethod(): ?string {
        return $this->depMethod;
    }

    /**
     * Gets the depMethodCode property value. The depMethodCode property
     * @return int|null
    */
    public function getDepMethodCode(): ?int {
        return $this->depMethodCode;
    }

    /**
     * Gets the depNext property value. The depNext property
     * @return float|null
    */
    public function getDepNext(): ?float {
        return $this->depNext;
    }

    /**
     * Gets the depRate property value. The depRate property
     * @return float|null
    */
    public function getDepRate(): ?float {
        return $this->depRate;
    }

    /**
     * Gets the deptName property value. The deptName property
     * @return string|null
    */
    public function getDeptName(): ?string {
        return $this->deptName;
    }

    /**
     * Gets the deptNumber property value. The deptNumber property
     * @return int|null
    */
    public function getDeptNumber(): ?int {
        return $this->deptNumber;
    }

    /**
     * Gets the depToDate property value. The depToDate property
     * @return float|null
    */
    public function getDepToDate(): ?float {
        return $this->depToDate;
    }

    /**
     * Gets the details1 property value. The details1 property
     * @return string|null
    */
    public function getDetails1(): ?string {
        return $this->details1;
    }

    /**
     * Gets the details2 property value. The details2 property
     * @return string|null
    */
    public function getDetails2(): ?string {
        return $this->details2;
    }

    /**
     * Gets the details3 property value. The details3 property
     * @return string|null
    */
    public function getDetails3(): ?string {
        return $this->details3;
    }

    /**
     * Gets the employee property value. The employee property
     * @return string|null
    */
    public function getEmployee(): ?string {
        return $this->employee;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'assetCat' => fn(ParseNode $n) => $o->setAssetCat($n->getIntegerValue()),
            'balSheetNomCode' => fn(ParseNode $n) => $o->setBalSheetNomCode($n->getStringValue()),
            'costPrice' => fn(ParseNode $n) => $o->setCostPrice($n->getFloatValue()),
            'depLast' => fn(ParseNode $n) => $o->setDepLast($n->getFloatValue()),
            'depMethod' => fn(ParseNode $n) => $o->setDepMethod($n->getStringValue()),
            'depMethodCode' => fn(ParseNode $n) => $o->setDepMethodCode($n->getIntegerValue()),
            'depNext' => fn(ParseNode $n) => $o->setDepNext($n->getFloatValue()),
            'depRate' => fn(ParseNode $n) => $o->setDepRate($n->getFloatValue()),
            'deptName' => fn(ParseNode $n) => $o->setDeptName($n->getStringValue()),
            'deptNumber' => fn(ParseNode $n) => $o->setDeptNumber($n->getIntegerValue()),
            'depToDate' => fn(ParseNode $n) => $o->setDepToDate($n->getFloatValue()),
            'details1' => fn(ParseNode $n) => $o->setDetails1($n->getStringValue()),
            'details2' => fn(ParseNode $n) => $o->setDetails2($n->getStringValue()),
            'details3' => fn(ParseNode $n) => $o->setDetails3($n->getStringValue()),
            'employee' => fn(ParseNode $n) => $o->setEmployee($n->getStringValue()),
            'netBook' => fn(ParseNode $n) => $o->setNetBook($n->getFloatValue()),
            'profitLossNomCode' => fn(ParseNode $n) => $o->setProfitLossNomCode($n->getStringValue()),
            'purchaseDate' => fn(ParseNode $n) => $o->setPurchaseDate($n->getDateTimeValue()),
            'purchaseRef' => fn(ParseNode $n) => $o->setPurchaseRef($n->getStringValue()),
            'recordCreateDate' => fn(ParseNode $n) => $o->setRecordCreateDate($n->getDateTimeValue()),
            'recordDeleted' => fn(ParseNode $n) => $o->setRecordDeleted($n->getIntegerValue()),
            'recordModifyDate' => fn(ParseNode $n) => $o->setRecordModifyDate($n->getDateTimeValue()),
            'serialNumber' => fn(ParseNode $n) => $o->setSerialNumber($n->getStringValue()),
        ];
    }

    /**
     * Gets the netBook property value. The netBook property
     * @return float|null
    */
    public function getNetBook(): ?float {
        return $this->netBook;
    }

    /**
     * Gets the profitLossNomCode property value. The profitLossNomCode property
     * @return string|null
    */
    public function getProfitLossNomCode(): ?string {
        return $this->profitLossNomCode;
    }

    /**
     * Gets the purchaseDate property value. The purchaseDate property
     * @return DateTime|null
    */
    public function getPurchaseDate(): ?DateTime {
        return $this->purchaseDate;
    }

    /**
     * Gets the purchaseRef property value. The purchaseRef property
     * @return string|null
    */
    public function getPurchaseRef(): ?string {
        return $this->purchaseRef;
    }

    /**
     * Gets the recordCreateDate property value. The recordCreateDate property
     * @return DateTime|null
    */
    public function getRecordCreateDate(): ?DateTime {
        return $this->recordCreateDate;
    }

    /**
     * Gets the recordDeleted property value. The recordDeleted property
     * @return int|null
    */
    public function getRecordDeleted(): ?int {
        return $this->recordDeleted;
    }

    /**
     * Gets the recordModifyDate property value. The recordModifyDate property
     * @return DateTime|null
    */
    public function getRecordModifyDate(): ?DateTime {
        return $this->recordModifyDate;
    }

    /**
     * Gets the serialNumber property value. The serialNumber property
     * @return string|null
    */
    public function getSerialNumber(): ?string {
        return $this->serialNumber;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeIntegerValue('assetCat', $this->getAssetCat());
        $writer->writeStringValue('balSheetNomCode', $this->getBalSheetNomCode());
        $writer->writeFloatValue('costPrice', $this->getCostPrice());
        $writer->writeFloatValue('depLast', $this->getDepLast());
        $writer->writeStringValue('depMethod', $this->getDepMethod());
        $writer->writeIntegerValue('depMethodCode', $this->getDepMethodCode());
        $writer->writeFloatValue('depNext', $this->getDepNext());
        $writer->writeFloatValue('depRate', $this->getDepRate());
        $writer->writeStringValue('deptName', $this->getDeptName());
        $writer->writeIntegerValue('deptNumber', $this->getDeptNumber());
        $writer->writeFloatValue('depToDate', $this->getDepToDate());
        $writer->writeStringValue('details1', $this->getDetails1());
        $writer->writeStringValue('details2', $this->getDetails2());
        $writer->writeStringValue('details3', $this->getDetails3());
        $writer->writeStringValue('employee', $this->getEmployee());
        $writer->writeFloatValue('netBook', $this->getNetBook());
        $writer->writeStringValue('profitLossNomCode', $this->getProfitLossNomCode());
        $writer->writeDateTimeValue('purchaseDate', $this->getPurchaseDate());
        $writer->writeStringValue('purchaseRef', $this->getPurchaseRef());
        $writer->writeDateTimeValue('recordCreateDate', $this->getRecordCreateDate());
        $writer->writeIntegerValue('recordDeleted', $this->getRecordDeleted());
        $writer->writeDateTimeValue('recordModifyDate', $this->getRecordModifyDate());
        $writer->writeStringValue('serialNumber', $this->getSerialNumber());
    }

    /**
     * Sets the assetCat property value. The assetCat property
     * @param int|null $value Value to set for the assetCat property.
    */
    public function setAssetCat(?int $value): void {
        $this->assetCat = $value;
    }

    /**
     * Sets the balSheetNomCode property value. The balSheetNomCode property
     * @param string|null $value Value to set for the balSheetNomCode property.
    */
    public function setBalSheetNomCode(?string $value): void {
        $this->balSheetNomCode = $value;
    }

    /**
     * Sets the costPrice property value. The costPrice property
     * @param float|null $value Value to set for the costPrice property.
    */
    public function setCostPrice(?float $value): void {
        $this->costPrice = $value;
    }

    /**
     * Sets the depLast property value. The depLast property
     * @param float|null $value Value to set for the depLast property.
    */
    public function setDepLast(?float $value): void {
        $this->depLast = $value;
    }

    /**
     * Sets the depMethod property value. The depMethod property
     * @param string|null $value Value to set for the depMethod property.
    */
    public function setDepMethod(?string $value): void {
        $this->depMethod = $value;
    }

    /**
     * Sets the depMethodCode property value. The depMethodCode property
     * @param int|null $value Value to set for the depMethodCode property.
    */
    public function setDepMethodCode(?int $value): void {
        $this->depMethodCode = $value;
    }

    /**
     * Sets the depNext property value. The depNext property
     * @param float|null $value Value to set for the depNext property.
    */
    public function setDepNext(?float $value): void {
        $this->depNext = $value;
    }

    /**
     * Sets the depRate property value. The depRate property
     * @param float|null $value Value to set for the depRate property.
    */
    public function setDepRate(?float $value): void {
        $this->depRate = $value;
    }

    /**
     * Sets the deptName property value. The deptName property
     * @param string|null $value Value to set for the deptName property.
    */
    public function setDeptName(?string $value): void {
        $this->deptName = $value;
    }

    /**
     * Sets the deptNumber property value. The deptNumber property
     * @param int|null $value Value to set for the deptNumber property.
    */
    public function setDeptNumber(?int $value): void {
        $this->deptNumber = $value;
    }

    /**
     * Sets the depToDate property value. The depToDate property
     * @param float|null $value Value to set for the depToDate property.
    */
    public function setDepToDate(?float $value): void {
        $this->depToDate = $value;
    }

    /**
     * Sets the details1 property value. The details1 property
     * @param string|null $value Value to set for the details1 property.
    */
    public function setDetails1(?string $value): void {
        $this->details1 = $value;
    }

    /**
     * Sets the details2 property value. The details2 property
     * @param string|null $value Value to set for the details2 property.
    */
    public function setDetails2(?string $value): void {
        $this->details2 = $value;
    }

    /**
     * Sets the details3 property value. The details3 property
     * @param string|null $value Value to set for the details3 property.
    */
    public function setDetails3(?string $value): void {
        $this->details3 = $value;
    }

    /**
     * Sets the employee property value. The employee property
     * @param string|null $value Value to set for the employee property.
    */
    public function setEmployee(?string $value): void {
        $this->employee = $value;
    }

    /**
     * Sets the netBook property value. The netBook property
     * @param float|null $value Value to set for the netBook property.
    */
    public function setNetBook(?float $value): void {
        $this->netBook = $value;
    }

    /**
     * Sets the profitLossNomCode property value. The profitLossNomCode property
     * @param string|null $value Value to set for the profitLossNomCode property.
    */
    public function setProfitLossNomCode(?string $value): void {
        $this->profitLossNomCode = $value;
    }

    /**
     * Sets the purchaseDate property value. The purchaseDate property
     * @param DateTime|null $value Value to set for the purchaseDate property.
    */
    public function setPurchaseDate(?DateTime $value): void {
        $this->purchaseDate = $value;
    }

    /**
     * Sets the purchaseRef property value. The purchaseRef property
     * @param string|null $value Value to set for the purchaseRef property.
    */
    public function setPurchaseRef(?string $value): void {
        $this->purchaseRef = $value;
    }

    /**
     * Sets the recordCreateDate property value. The recordCreateDate property
     * @param DateTime|null $value Value to set for the recordCreateDate property.
    */
    public function setRecordCreateDate(?DateTime $value): void {
        $this->recordCreateDate = $value;
    }

    /**
     * Sets the recordDeleted property value. The recordDeleted property
     * @param int|null $value Value to set for the recordDeleted property.
    */
    public function setRecordDeleted(?int $value): void {
        $this->recordDeleted = $value;
    }

    /**
     * Sets the recordModifyDate property value. The recordModifyDate property
     * @param DateTime|null $value Value to set for the recordModifyDate property.
    */
    public function setRecordModifyDate(?DateTime $value): void {
        $this->recordModifyDate = $value;
    }

    /**
     * Sets the serialNumber property value. The serialNumber property
     * @param string|null $value Value to set for the serialNumber property.
    */
    public function setSerialNumber(?string $value): void {
        $this->serialNumber = $value;
    }

}
