<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class ProjectPostDto implements Parsable
{
    /**
     * @var ProjectAttributesWrite|null $attributes The attributes property
    */
    private ?ProjectAttributesWrite $attributes = null;

    /**
     * @var ProjectRelationshipsWrite|null $relationships The relationships property
    */
    private ?ProjectRelationshipsWrite $relationships = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return ProjectPostDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): ProjectPostDto {
        return new ProjectPostDto();
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return ProjectAttributesWrite|null
    */
    public function getAttributes(): ?ProjectAttributesWrite {
        return $this->attributes;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([ProjectAttributesWrite::class, 'createFromDiscriminatorValue'])),
            'relationships' => fn(ParseNode $n) => $o->setRelationships($n->getObjectValue([ProjectRelationshipsWrite::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the relationships property value. The relationships property
     * @return ProjectRelationshipsWrite|null
    */
    public function getRelationships(): ?ProjectRelationshipsWrite {
        return $this->relationships;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('attributes', $this->getAttributes());
        $writer->writeObjectValue('relationships', $this->getRelationships());
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param ProjectAttributesWrite|null $value Value to set for the attributes property.
    */
    public function setAttributes(?ProjectAttributesWrite $value): void {
        $this->attributes = $value;
    }

    /**
     * Sets the relationships property value. The relationships property
     * @param ProjectRelationshipsWrite|null $value Value to set for the relationships property.
    */
    public function setRelationships(?ProjectRelationshipsWrite $value): void {
        $this->relationships = $value;
    }

}
