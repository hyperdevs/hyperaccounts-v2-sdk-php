<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class TransactionHeaderCreateDocumentAttributesWrite implements Parsable
{
    /**
     * @var string|null $documentLink The documentLink property
    */
    private ?string $documentLink = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return TransactionHeaderCreateDocumentAttributesWrite
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): TransactionHeaderCreateDocumentAttributesWrite {
        return new TransactionHeaderCreateDocumentAttributesWrite();
    }

    /**
     * Gets the documentLink property value. The documentLink property
     * @return string|null
    */
    public function getDocumentLink(): ?string {
        return $this->documentLink;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'documentLink' => fn(ParseNode $n) => $o->setDocumentLink($n->getStringValue()),
        ];
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeStringValue('documentLink', $this->getDocumentLink());
    }

    /**
     * Sets the documentLink property value. The documentLink property
     * @param string|null $value Value to set for the documentLink property.
    */
    public function setDocumentLink(?string $value): void {
        $this->documentLink = $value;
    }

}
