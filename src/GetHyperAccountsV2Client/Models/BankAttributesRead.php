<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use DateTime;
use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class BankAttributesRead implements Parsable
{
    /**
     * @var string|null $accountName The accountName property
    */
    private ?string $accountName = null;

    /**
     * @var string|null $accountNumber The accountNumber property
    */
    private ?string $accountNumber = null;

    /**
     * @var string|null $additionalRef1 The additionalRef1 property
    */
    private ?string $additionalRef1 = null;

    /**
     * @var string|null $additionalRef2 The additionalRef2 property
    */
    private ?string $additionalRef2 = null;

    /**
     * @var string|null $additionalRef3 The additionalRef3 property
    */
    private ?string $additionalRef3 = null;

    /**
     * @var string|null $address1 The address1 property
    */
    private ?string $address1 = null;

    /**
     * @var string|null $address2 The address2 property
    */
    private ?string $address2 = null;

    /**
     * @var string|null $address3 The address3 property
    */
    private ?string $address3 = null;

    /**
     * @var string|null $address4 The address4 property
    */
    private ?string $address4 = null;

    /**
     * @var string|null $address5 The address5 property
    */
    private ?string $address5 = null;

    /**
     * @var string|null $bankChargesDefaultBankRef The bankChargesDefaultBankRef property
    */
    private ?string $bankChargesDefaultBankRef = null;

    /**
     * @var int|null $bankChargesDefaultDepartment The bankChargesDefaultDepartment property
    */
    private ?int $bankChargesDefaultDepartment = null;

    /**
     * @var string|null $bankChargesDefaultNominalCode The bankChargesDefaultNominalCode property
    */
    private ?string $bankChargesDefaultNominalCode = null;

    /**
     * @var int|null $bankChargesDefaultTaxCode The bankChargesDefaultTaxCode property
    */
    private ?int $bankChargesDefaultTaxCode = null;

    /**
     * @var int|null $bankFeedsState The bankFeedsState property
    */
    private ?int $bankFeedsState = null;

    /**
     * @var string|null $bicswift The bicswift property
    */
    private ?string $bicswift = null;

    /**
     * @var string|null $cAddress1 The cAddress1 property
    */
    private ?string $cAddress1 = null;

    /**
     * @var string|null $cAddress2 The cAddress2 property
    */
    private ?string $cAddress2 = null;

    /**
     * @var string|null $cAddress3 The cAddress3 property
    */
    private ?string $cAddress3 = null;

    /**
     * @var string|null $cAddress4 The cAddress4 property
    */
    private ?string $cAddress4 = null;

    /**
     * @var string|null $cAddress5 The cAddress5 property
    */
    private ?string $cAddress5 = null;

    /**
     * @var string|null $contactName The contactName property
    */
    private ?string $contactName = null;

    /**
     * @var int|null $currency The currency property
    */
    private ?int $currency = null;

    /**
     * @var string|null $eMail The eMail property
    */
    private ?string $eMail = null;

    /**
     * @var string|null $fax The fax property
    */
    private ?string $fax = null;

    /**
     * @var float|null $foreignBalance The foreignBalance property
    */
    private ?float $foreignBalance = null;

    /**
     * @var string|null $iban The iban property
    */
    private ?string $iban = null;

    /**
     * @var int|null $inactiveFlag The inactiveFlag property
    */
    private ?int $inactiveFlag = null;

    /**
     * @var float|null $lastRecBalance The lastRecBalance property
    */
    private ?float $lastRecBalance = null;

    /**
     * @var DateTime|null $lastRecDate The lastRecDate property
    */
    private ?DateTime $lastRecDate = null;

    /**
     * @var string|null $name The name property
    */
    private ?string $name = null;

    /**
     * @var int|null $number The number property
    */
    private ?int $number = null;

    /**
     * @var float|null $overdraftLimit The overdraftLimit property
    */
    private ?float $overdraftLimit = null;

    /**
     * @var DateTime|null $recordCreateDate The recordCreateDate property
    */
    private ?DateTime $recordCreateDate = null;

    /**
     * @var int|null $recordDeleted The recordDeleted property
    */
    private ?int $recordDeleted = null;

    /**
     * @var DateTime|null $recordModifyDate The recordModifyDate property
    */
    private ?DateTime $recordModifyDate = null;

    /**
     * @var string|null $rollNumber The rollNumber property
    */
    private ?string $rollNumber = null;

    /**
     * @var string|null $sortCode The sortCode property
    */
    private ?string $sortCode = null;

    /**
     * @var string|null $telephone The telephone property
    */
    private ?string $telephone = null;

    /**
     * @var string|null $type The type property
    */
    private ?string $type = null;

    /**
     * @var string|null $webAddress The webAddress property
    */
    private ?string $webAddress = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return BankAttributesRead
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): BankAttributesRead {
        return new BankAttributesRead();
    }

    /**
     * Gets the accountName property value. The accountName property
     * @return string|null
    */
    public function getAccountName(): ?string {
        return $this->accountName;
    }

    /**
     * Gets the accountNumber property value. The accountNumber property
     * @return string|null
    */
    public function getAccountNumber(): ?string {
        return $this->accountNumber;
    }

    /**
     * Gets the additionalRef1 property value. The additionalRef1 property
     * @return string|null
    */
    public function getAdditionalRef1(): ?string {
        return $this->additionalRef1;
    }

    /**
     * Gets the additionalRef2 property value. The additionalRef2 property
     * @return string|null
    */
    public function getAdditionalRef2(): ?string {
        return $this->additionalRef2;
    }

    /**
     * Gets the additionalRef3 property value. The additionalRef3 property
     * @return string|null
    */
    public function getAdditionalRef3(): ?string {
        return $this->additionalRef3;
    }

    /**
     * Gets the address1 property value. The address1 property
     * @return string|null
    */
    public function getAddress1(): ?string {
        return $this->address1;
    }

    /**
     * Gets the address2 property value. The address2 property
     * @return string|null
    */
    public function getAddress2(): ?string {
        return $this->address2;
    }

    /**
     * Gets the address3 property value. The address3 property
     * @return string|null
    */
    public function getAddress3(): ?string {
        return $this->address3;
    }

    /**
     * Gets the address4 property value. The address4 property
     * @return string|null
    */
    public function getAddress4(): ?string {
        return $this->address4;
    }

    /**
     * Gets the address5 property value. The address5 property
     * @return string|null
    */
    public function getAddress5(): ?string {
        return $this->address5;
    }

    /**
     * Gets the bankChargesDefaultBankRef property value. The bankChargesDefaultBankRef property
     * @return string|null
    */
    public function getBankChargesDefaultBankRef(): ?string {
        return $this->bankChargesDefaultBankRef;
    }

    /**
     * Gets the bankChargesDefaultDepartment property value. The bankChargesDefaultDepartment property
     * @return int|null
    */
    public function getBankChargesDefaultDepartment(): ?int {
        return $this->bankChargesDefaultDepartment;
    }

    /**
     * Gets the bankChargesDefaultNominalCode property value. The bankChargesDefaultNominalCode property
     * @return string|null
    */
    public function getBankChargesDefaultNominalCode(): ?string {
        return $this->bankChargesDefaultNominalCode;
    }

    /**
     * Gets the bankChargesDefaultTaxCode property value. The bankChargesDefaultTaxCode property
     * @return int|null
    */
    public function getBankChargesDefaultTaxCode(): ?int {
        return $this->bankChargesDefaultTaxCode;
    }

    /**
     * Gets the bankFeedsState property value. The bankFeedsState property
     * @return int|null
    */
    public function getBankFeedsState(): ?int {
        return $this->bankFeedsState;
    }

    /**
     * Gets the bicswift property value. The bicswift property
     * @return string|null
    */
    public function getBicswift(): ?string {
        return $this->bicswift;
    }

    /**
     * Gets the cAddress1 property value. The cAddress1 property
     * @return string|null
    */
    public function getCAddress1(): ?string {
        return $this->cAddress1;
    }

    /**
     * Gets the cAddress2 property value. The cAddress2 property
     * @return string|null
    */
    public function getCAddress2(): ?string {
        return $this->cAddress2;
    }

    /**
     * Gets the cAddress3 property value. The cAddress3 property
     * @return string|null
    */
    public function getCAddress3(): ?string {
        return $this->cAddress3;
    }

    /**
     * Gets the cAddress4 property value. The cAddress4 property
     * @return string|null
    */
    public function getCAddress4(): ?string {
        return $this->cAddress4;
    }

    /**
     * Gets the cAddress5 property value. The cAddress5 property
     * @return string|null
    */
    public function getCAddress5(): ?string {
        return $this->cAddress5;
    }

    /**
     * Gets the contactName property value. The contactName property
     * @return string|null
    */
    public function getContactName(): ?string {
        return $this->contactName;
    }

    /**
     * Gets the currency property value. The currency property
     * @return int|null
    */
    public function getCurrency(): ?int {
        return $this->currency;
    }

    /**
     * Gets the eMail property value. The eMail property
     * @return string|null
    */
    public function getEMail(): ?string {
        return $this->eMail;
    }

    /**
     * Gets the fax property value. The fax property
     * @return string|null
    */
    public function getFax(): ?string {
        return $this->fax;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'accountName' => fn(ParseNode $n) => $o->setAccountName($n->getStringValue()),
            'accountNumber' => fn(ParseNode $n) => $o->setAccountNumber($n->getStringValue()),
            'additionalRef1' => fn(ParseNode $n) => $o->setAdditionalRef1($n->getStringValue()),
            'additionalRef2' => fn(ParseNode $n) => $o->setAdditionalRef2($n->getStringValue()),
            'additionalRef3' => fn(ParseNode $n) => $o->setAdditionalRef3($n->getStringValue()),
            'address1' => fn(ParseNode $n) => $o->setAddress1($n->getStringValue()),
            'address2' => fn(ParseNode $n) => $o->setAddress2($n->getStringValue()),
            'address3' => fn(ParseNode $n) => $o->setAddress3($n->getStringValue()),
            'address4' => fn(ParseNode $n) => $o->setAddress4($n->getStringValue()),
            'address5' => fn(ParseNode $n) => $o->setAddress5($n->getStringValue()),
            'bankChargesDefaultBankRef' => fn(ParseNode $n) => $o->setBankChargesDefaultBankRef($n->getStringValue()),
            'bankChargesDefaultDepartment' => fn(ParseNode $n) => $o->setBankChargesDefaultDepartment($n->getIntegerValue()),
            'bankChargesDefaultNominalCode' => fn(ParseNode $n) => $o->setBankChargesDefaultNominalCode($n->getStringValue()),
            'bankChargesDefaultTaxCode' => fn(ParseNode $n) => $o->setBankChargesDefaultTaxCode($n->getIntegerValue()),
            'bankFeedsState' => fn(ParseNode $n) => $o->setBankFeedsState($n->getIntegerValue()),
            'bicswift' => fn(ParseNode $n) => $o->setBicswift($n->getStringValue()),
            'cAddress1' => fn(ParseNode $n) => $o->setCAddress1($n->getStringValue()),
            'cAddress2' => fn(ParseNode $n) => $o->setCAddress2($n->getStringValue()),
            'cAddress3' => fn(ParseNode $n) => $o->setCAddress3($n->getStringValue()),
            'cAddress4' => fn(ParseNode $n) => $o->setCAddress4($n->getStringValue()),
            'cAddress5' => fn(ParseNode $n) => $o->setCAddress5($n->getStringValue()),
            'contactName' => fn(ParseNode $n) => $o->setContactName($n->getStringValue()),
            'currency' => fn(ParseNode $n) => $o->setCurrency($n->getIntegerValue()),
            'eMail' => fn(ParseNode $n) => $o->setEMail($n->getStringValue()),
            'fax' => fn(ParseNode $n) => $o->setFax($n->getStringValue()),
            'foreignBalance' => fn(ParseNode $n) => $o->setForeignBalance($n->getFloatValue()),
            'iban' => fn(ParseNode $n) => $o->setIban($n->getStringValue()),
            'inactiveFlag' => fn(ParseNode $n) => $o->setInactiveFlag($n->getIntegerValue()),
            'lastRecBalance' => fn(ParseNode $n) => $o->setLastRecBalance($n->getFloatValue()),
            'lastRecDate' => fn(ParseNode $n) => $o->setLastRecDate($n->getDateTimeValue()),
            'name' => fn(ParseNode $n) => $o->setName($n->getStringValue()),
            'number' => fn(ParseNode $n) => $o->setNumber($n->getIntegerValue()),
            'overdraftLimit' => fn(ParseNode $n) => $o->setOverdraftLimit($n->getFloatValue()),
            'recordCreateDate' => fn(ParseNode $n) => $o->setRecordCreateDate($n->getDateTimeValue()),
            'recordDeleted' => fn(ParseNode $n) => $o->setRecordDeleted($n->getIntegerValue()),
            'recordModifyDate' => fn(ParseNode $n) => $o->setRecordModifyDate($n->getDateTimeValue()),
            'rollNumber' => fn(ParseNode $n) => $o->setRollNumber($n->getStringValue()),
            'sortCode' => fn(ParseNode $n) => $o->setSortCode($n->getStringValue()),
            'telephone' => fn(ParseNode $n) => $o->setTelephone($n->getStringValue()),
            'type' => fn(ParseNode $n) => $o->setType($n->getStringValue()),
            'webAddress' => fn(ParseNode $n) => $o->setWebAddress($n->getStringValue()),
        ];
    }

    /**
     * Gets the foreignBalance property value. The foreignBalance property
     * @return float|null
    */
    public function getForeignBalance(): ?float {
        return $this->foreignBalance;
    }

    /**
     * Gets the iban property value. The iban property
     * @return string|null
    */
    public function getIban(): ?string {
        return $this->iban;
    }

    /**
     * Gets the inactiveFlag property value. The inactiveFlag property
     * @return int|null
    */
    public function getInactiveFlag(): ?int {
        return $this->inactiveFlag;
    }

    /**
     * Gets the lastRecBalance property value. The lastRecBalance property
     * @return float|null
    */
    public function getLastRecBalance(): ?float {
        return $this->lastRecBalance;
    }

    /**
     * Gets the lastRecDate property value. The lastRecDate property
     * @return DateTime|null
    */
    public function getLastRecDate(): ?DateTime {
        return $this->lastRecDate;
    }

    /**
     * Gets the name property value. The name property
     * @return string|null
    */
    public function getName(): ?string {
        return $this->name;
    }

    /**
     * Gets the number property value. The number property
     * @return int|null
    */
    public function getNumber(): ?int {
        return $this->number;
    }

    /**
     * Gets the overdraftLimit property value. The overdraftLimit property
     * @return float|null
    */
    public function getOverdraftLimit(): ?float {
        return $this->overdraftLimit;
    }

    /**
     * Gets the recordCreateDate property value. The recordCreateDate property
     * @return DateTime|null
    */
    public function getRecordCreateDate(): ?DateTime {
        return $this->recordCreateDate;
    }

    /**
     * Gets the recordDeleted property value. The recordDeleted property
     * @return int|null
    */
    public function getRecordDeleted(): ?int {
        return $this->recordDeleted;
    }

    /**
     * Gets the recordModifyDate property value. The recordModifyDate property
     * @return DateTime|null
    */
    public function getRecordModifyDate(): ?DateTime {
        return $this->recordModifyDate;
    }

    /**
     * Gets the rollNumber property value. The rollNumber property
     * @return string|null
    */
    public function getRollNumber(): ?string {
        return $this->rollNumber;
    }

    /**
     * Gets the sortCode property value. The sortCode property
     * @return string|null
    */
    public function getSortCode(): ?string {
        return $this->sortCode;
    }

    /**
     * Gets the telephone property value. The telephone property
     * @return string|null
    */
    public function getTelephone(): ?string {
        return $this->telephone;
    }

    /**
     * Gets the type property value. The type property
     * @return string|null
    */
    public function getType(): ?string {
        return $this->type;
    }

    /**
     * Gets the webAddress property value. The webAddress property
     * @return string|null
    */
    public function getWebAddress(): ?string {
        return $this->webAddress;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeStringValue('accountName', $this->getAccountName());
        $writer->writeStringValue('accountNumber', $this->getAccountNumber());
        $writer->writeStringValue('additionalRef1', $this->getAdditionalRef1());
        $writer->writeStringValue('additionalRef2', $this->getAdditionalRef2());
        $writer->writeStringValue('additionalRef3', $this->getAdditionalRef3());
        $writer->writeStringValue('address1', $this->getAddress1());
        $writer->writeStringValue('address2', $this->getAddress2());
        $writer->writeStringValue('address3', $this->getAddress3());
        $writer->writeStringValue('address4', $this->getAddress4());
        $writer->writeStringValue('address5', $this->getAddress5());
        $writer->writeStringValue('bankChargesDefaultBankRef', $this->getBankChargesDefaultBankRef());
        $writer->writeIntegerValue('bankChargesDefaultDepartment', $this->getBankChargesDefaultDepartment());
        $writer->writeStringValue('bankChargesDefaultNominalCode', $this->getBankChargesDefaultNominalCode());
        $writer->writeIntegerValue('bankChargesDefaultTaxCode', $this->getBankChargesDefaultTaxCode());
        $writer->writeIntegerValue('bankFeedsState', $this->getBankFeedsState());
        $writer->writeStringValue('bicswift', $this->getBicswift());
        $writer->writeStringValue('cAddress1', $this->getCAddress1());
        $writer->writeStringValue('cAddress2', $this->getCAddress2());
        $writer->writeStringValue('cAddress3', $this->getCAddress3());
        $writer->writeStringValue('cAddress4', $this->getCAddress4());
        $writer->writeStringValue('cAddress5', $this->getCAddress5());
        $writer->writeStringValue('contactName', $this->getContactName());
        $writer->writeIntegerValue('currency', $this->getCurrency());
        $writer->writeStringValue('eMail', $this->getEMail());
        $writer->writeStringValue('fax', $this->getFax());
        $writer->writeFloatValue('foreignBalance', $this->getForeignBalance());
        $writer->writeStringValue('iban', $this->getIban());
        $writer->writeIntegerValue('inactiveFlag', $this->getInactiveFlag());
        $writer->writeFloatValue('lastRecBalance', $this->getLastRecBalance());
        $writer->writeDateTimeValue('lastRecDate', $this->getLastRecDate());
        $writer->writeStringValue('name', $this->getName());
        $writer->writeIntegerValue('number', $this->getNumber());
        $writer->writeFloatValue('overdraftLimit', $this->getOverdraftLimit());
        $writer->writeDateTimeValue('recordCreateDate', $this->getRecordCreateDate());
        $writer->writeIntegerValue('recordDeleted', $this->getRecordDeleted());
        $writer->writeDateTimeValue('recordModifyDate', $this->getRecordModifyDate());
        $writer->writeStringValue('rollNumber', $this->getRollNumber());
        $writer->writeStringValue('sortCode', $this->getSortCode());
        $writer->writeStringValue('telephone', $this->getTelephone());
        $writer->writeStringValue('type', $this->getType());
        $writer->writeStringValue('webAddress', $this->getWebAddress());
    }

    /**
     * Sets the accountName property value. The accountName property
     * @param string|null $value Value to set for the accountName property.
    */
    public function setAccountName(?string $value): void {
        $this->accountName = $value;
    }

    /**
     * Sets the accountNumber property value. The accountNumber property
     * @param string|null $value Value to set for the accountNumber property.
    */
    public function setAccountNumber(?string $value): void {
        $this->accountNumber = $value;
    }

    /**
     * Sets the additionalRef1 property value. The additionalRef1 property
     * @param string|null $value Value to set for the additionalRef1 property.
    */
    public function setAdditionalRef1(?string $value): void {
        $this->additionalRef1 = $value;
    }

    /**
     * Sets the additionalRef2 property value. The additionalRef2 property
     * @param string|null $value Value to set for the additionalRef2 property.
    */
    public function setAdditionalRef2(?string $value): void {
        $this->additionalRef2 = $value;
    }

    /**
     * Sets the additionalRef3 property value. The additionalRef3 property
     * @param string|null $value Value to set for the additionalRef3 property.
    */
    public function setAdditionalRef3(?string $value): void {
        $this->additionalRef3 = $value;
    }

    /**
     * Sets the address1 property value. The address1 property
     * @param string|null $value Value to set for the address1 property.
    */
    public function setAddress1(?string $value): void {
        $this->address1 = $value;
    }

    /**
     * Sets the address2 property value. The address2 property
     * @param string|null $value Value to set for the address2 property.
    */
    public function setAddress2(?string $value): void {
        $this->address2 = $value;
    }

    /**
     * Sets the address3 property value. The address3 property
     * @param string|null $value Value to set for the address3 property.
    */
    public function setAddress3(?string $value): void {
        $this->address3 = $value;
    }

    /**
     * Sets the address4 property value. The address4 property
     * @param string|null $value Value to set for the address4 property.
    */
    public function setAddress4(?string $value): void {
        $this->address4 = $value;
    }

    /**
     * Sets the address5 property value. The address5 property
     * @param string|null $value Value to set for the address5 property.
    */
    public function setAddress5(?string $value): void {
        $this->address5 = $value;
    }

    /**
     * Sets the bankChargesDefaultBankRef property value. The bankChargesDefaultBankRef property
     * @param string|null $value Value to set for the bankChargesDefaultBankRef property.
    */
    public function setBankChargesDefaultBankRef(?string $value): void {
        $this->bankChargesDefaultBankRef = $value;
    }

    /**
     * Sets the bankChargesDefaultDepartment property value. The bankChargesDefaultDepartment property
     * @param int|null $value Value to set for the bankChargesDefaultDepartment property.
    */
    public function setBankChargesDefaultDepartment(?int $value): void {
        $this->bankChargesDefaultDepartment = $value;
    }

    /**
     * Sets the bankChargesDefaultNominalCode property value. The bankChargesDefaultNominalCode property
     * @param string|null $value Value to set for the bankChargesDefaultNominalCode property.
    */
    public function setBankChargesDefaultNominalCode(?string $value): void {
        $this->bankChargesDefaultNominalCode = $value;
    }

    /**
     * Sets the bankChargesDefaultTaxCode property value. The bankChargesDefaultTaxCode property
     * @param int|null $value Value to set for the bankChargesDefaultTaxCode property.
    */
    public function setBankChargesDefaultTaxCode(?int $value): void {
        $this->bankChargesDefaultTaxCode = $value;
    }

    /**
     * Sets the bankFeedsState property value. The bankFeedsState property
     * @param int|null $value Value to set for the bankFeedsState property.
    */
    public function setBankFeedsState(?int $value): void {
        $this->bankFeedsState = $value;
    }

    /**
     * Sets the bicswift property value. The bicswift property
     * @param string|null $value Value to set for the bicswift property.
    */
    public function setBicswift(?string $value): void {
        $this->bicswift = $value;
    }

    /**
     * Sets the cAddress1 property value. The cAddress1 property
     * @param string|null $value Value to set for the cAddress1 property.
    */
    public function setCAddress1(?string $value): void {
        $this->cAddress1 = $value;
    }

    /**
     * Sets the cAddress2 property value. The cAddress2 property
     * @param string|null $value Value to set for the cAddress2 property.
    */
    public function setCAddress2(?string $value): void {
        $this->cAddress2 = $value;
    }

    /**
     * Sets the cAddress3 property value. The cAddress3 property
     * @param string|null $value Value to set for the cAddress3 property.
    */
    public function setCAddress3(?string $value): void {
        $this->cAddress3 = $value;
    }

    /**
     * Sets the cAddress4 property value. The cAddress4 property
     * @param string|null $value Value to set for the cAddress4 property.
    */
    public function setCAddress4(?string $value): void {
        $this->cAddress4 = $value;
    }

    /**
     * Sets the cAddress5 property value. The cAddress5 property
     * @param string|null $value Value to set for the cAddress5 property.
    */
    public function setCAddress5(?string $value): void {
        $this->cAddress5 = $value;
    }

    /**
     * Sets the contactName property value. The contactName property
     * @param string|null $value Value to set for the contactName property.
    */
    public function setContactName(?string $value): void {
        $this->contactName = $value;
    }

    /**
     * Sets the currency property value. The currency property
     * @param int|null $value Value to set for the currency property.
    */
    public function setCurrency(?int $value): void {
        $this->currency = $value;
    }

    /**
     * Sets the eMail property value. The eMail property
     * @param string|null $value Value to set for the eMail property.
    */
    public function setEMail(?string $value): void {
        $this->eMail = $value;
    }

    /**
     * Sets the fax property value. The fax property
     * @param string|null $value Value to set for the fax property.
    */
    public function setFax(?string $value): void {
        $this->fax = $value;
    }

    /**
     * Sets the foreignBalance property value. The foreignBalance property
     * @param float|null $value Value to set for the foreignBalance property.
    */
    public function setForeignBalance(?float $value): void {
        $this->foreignBalance = $value;
    }

    /**
     * Sets the iban property value. The iban property
     * @param string|null $value Value to set for the iban property.
    */
    public function setIban(?string $value): void {
        $this->iban = $value;
    }

    /**
     * Sets the inactiveFlag property value. The inactiveFlag property
     * @param int|null $value Value to set for the inactiveFlag property.
    */
    public function setInactiveFlag(?int $value): void {
        $this->inactiveFlag = $value;
    }

    /**
     * Sets the lastRecBalance property value. The lastRecBalance property
     * @param float|null $value Value to set for the lastRecBalance property.
    */
    public function setLastRecBalance(?float $value): void {
        $this->lastRecBalance = $value;
    }

    /**
     * Sets the lastRecDate property value. The lastRecDate property
     * @param DateTime|null $value Value to set for the lastRecDate property.
    */
    public function setLastRecDate(?DateTime $value): void {
        $this->lastRecDate = $value;
    }

    /**
     * Sets the name property value. The name property
     * @param string|null $value Value to set for the name property.
    */
    public function setName(?string $value): void {
        $this->name = $value;
    }

    /**
     * Sets the number property value. The number property
     * @param int|null $value Value to set for the number property.
    */
    public function setNumber(?int $value): void {
        $this->number = $value;
    }

    /**
     * Sets the overdraftLimit property value. The overdraftLimit property
     * @param float|null $value Value to set for the overdraftLimit property.
    */
    public function setOverdraftLimit(?float $value): void {
        $this->overdraftLimit = $value;
    }

    /**
     * Sets the recordCreateDate property value. The recordCreateDate property
     * @param DateTime|null $value Value to set for the recordCreateDate property.
    */
    public function setRecordCreateDate(?DateTime $value): void {
        $this->recordCreateDate = $value;
    }

    /**
     * Sets the recordDeleted property value. The recordDeleted property
     * @param int|null $value Value to set for the recordDeleted property.
    */
    public function setRecordDeleted(?int $value): void {
        $this->recordDeleted = $value;
    }

    /**
     * Sets the recordModifyDate property value. The recordModifyDate property
     * @param DateTime|null $value Value to set for the recordModifyDate property.
    */
    public function setRecordModifyDate(?DateTime $value): void {
        $this->recordModifyDate = $value;
    }

    /**
     * Sets the rollNumber property value. The rollNumber property
     * @param string|null $value Value to set for the rollNumber property.
    */
    public function setRollNumber(?string $value): void {
        $this->rollNumber = $value;
    }

    /**
     * Sets the sortCode property value. The sortCode property
     * @param string|null $value Value to set for the sortCode property.
    */
    public function setSortCode(?string $value): void {
        $this->sortCode = $value;
    }

    /**
     * Sets the telephone property value. The telephone property
     * @param string|null $value Value to set for the telephone property.
    */
    public function setTelephone(?string $value): void {
        $this->telephone = $value;
    }

    /**
     * Sets the type property value. The type property
     * @param string|null $value Value to set for the type property.
    */
    public function setType(?string $value): void {
        $this->type = $value;
    }

    /**
     * Sets the webAddress property value. The webAddress property
     * @param string|null $value Value to set for the webAddress property.
    */
    public function setWebAddress(?string $value): void {
        $this->webAddress = $value;
    }

}
