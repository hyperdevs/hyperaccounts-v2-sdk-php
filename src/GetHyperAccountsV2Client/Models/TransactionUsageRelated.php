<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class TransactionUsageRelated implements Parsable
{
    /**
     * @var int|null $usageNumber The usageNumber property
    */
    private ?int $usageNumber = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return TransactionUsageRelated
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): TransactionUsageRelated {
        return new TransactionUsageRelated();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'usageNumber' => fn(ParseNode $n) => $o->setUsageNumber($n->getIntegerValue()),
        ];
    }

    /**
     * Gets the usageNumber property value. The usageNumber property
     * @return int|null
    */
    public function getUsageNumber(): ?int {
        return $this->usageNumber;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeIntegerValue('usageNumber', $this->getUsageNumber());
    }

    /**
     * Sets the usageNumber property value. The usageNumber property
     * @param int|null $value Value to set for the usageNumber property.
    */
    public function setUsageNumber(?int $value): void {
        $this->usageNumber = $value;
    }

}
