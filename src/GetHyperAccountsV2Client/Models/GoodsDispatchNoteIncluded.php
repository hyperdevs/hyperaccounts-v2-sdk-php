<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class GoodsDispatchNoteIncluded implements Parsable
{
    /**
     * @var CustomerGetDto|null $customer The customer property
    */
    private ?CustomerGetDto $customer = null;

    /**
     * @var SalesOrderGetDto|null $salesOrder The salesOrder property
    */
    private ?SalesOrderGetDto $salesOrder = null;

    /**
     * @var StockGetDto|null $stock The stock property
    */
    private ?StockGetDto $stock = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return GoodsDispatchNoteIncluded
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): GoodsDispatchNoteIncluded {
        return new GoodsDispatchNoteIncluded();
    }

    /**
     * Gets the customer property value. The customer property
     * @return CustomerGetDto|null
    */
    public function getCustomer(): ?CustomerGetDto {
        return $this->customer;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'customer' => fn(ParseNode $n) => $o->setCustomer($n->getObjectValue([CustomerGetDto::class, 'createFromDiscriminatorValue'])),
            'salesOrder' => fn(ParseNode $n) => $o->setSalesOrder($n->getObjectValue([SalesOrderGetDto::class, 'createFromDiscriminatorValue'])),
            'stock' => fn(ParseNode $n) => $o->setStock($n->getObjectValue([StockGetDto::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the salesOrder property value. The salesOrder property
     * @return SalesOrderGetDto|null
    */
    public function getSalesOrder(): ?SalesOrderGetDto {
        return $this->salesOrder;
    }

    /**
     * Gets the stock property value. The stock property
     * @return StockGetDto|null
    */
    public function getStock(): ?StockGetDto {
        return $this->stock;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('customer', $this->getCustomer());
        $writer->writeObjectValue('salesOrder', $this->getSalesOrder());
        $writer->writeObjectValue('stock', $this->getStock());
    }

    /**
     * Sets the customer property value. The customer property
     * @param CustomerGetDto|null $value Value to set for the customer property.
    */
    public function setCustomer(?CustomerGetDto $value): void {
        $this->customer = $value;
    }

    /**
     * Sets the salesOrder property value. The salesOrder property
     * @param SalesOrderGetDto|null $value Value to set for the salesOrder property.
    */
    public function setSalesOrder(?SalesOrderGetDto $value): void {
        $this->salesOrder = $value;
    }

    /**
     * Sets the stock property value. The stock property
     * @param StockGetDto|null $value Value to set for the stock property.
    */
    public function setStock(?StockGetDto $value): void {
        $this->stock = $value;
    }

}
