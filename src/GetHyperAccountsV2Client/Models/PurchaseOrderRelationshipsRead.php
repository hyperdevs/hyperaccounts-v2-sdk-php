<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class PurchaseOrderRelationshipsRead implements Parsable
{
    /**
     * @var GoodsReceivedNoteRelatedListRelationship|null $goodsReceivedNotes The goodsReceivedNotes property
    */
    private ?GoodsReceivedNoteRelatedListRelationship $goodsReceivedNotes = null;

    /**
     * @var PurchaseOrderItemRelatedListRelationship|null $items The items property
    */
    private ?PurchaseOrderItemRelatedListRelationship $items = null;

    /**
     * @var ProjectRelatedRelationship|null $project The project property
    */
    private ?ProjectRelatedRelationship $project = null;

    /**
     * @var SupplierRelatedRelationship|null $supplier The supplier property
    */
    private ?SupplierRelatedRelationship $supplier = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return PurchaseOrderRelationshipsRead
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): PurchaseOrderRelationshipsRead {
        return new PurchaseOrderRelationshipsRead();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'goodsReceivedNotes' => fn(ParseNode $n) => $o->setGoodsReceivedNotes($n->getObjectValue([GoodsReceivedNoteRelatedListRelationship::class, 'createFromDiscriminatorValue'])),
            'items' => fn(ParseNode $n) => $o->setItems($n->getObjectValue([PurchaseOrderItemRelatedListRelationship::class, 'createFromDiscriminatorValue'])),
            'project' => fn(ParseNode $n) => $o->setProject($n->getObjectValue([ProjectRelatedRelationship::class, 'createFromDiscriminatorValue'])),
            'supplier' => fn(ParseNode $n) => $o->setSupplier($n->getObjectValue([SupplierRelatedRelationship::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the goodsReceivedNotes property value. The goodsReceivedNotes property
     * @return GoodsReceivedNoteRelatedListRelationship|null
    */
    public function getGoodsReceivedNotes(): ?GoodsReceivedNoteRelatedListRelationship {
        return $this->goodsReceivedNotes;
    }

    /**
     * Gets the items property value. The items property
     * @return PurchaseOrderItemRelatedListRelationship|null
    */
    public function getItems(): ?PurchaseOrderItemRelatedListRelationship {
        return $this->items;
    }

    /**
     * Gets the project property value. The project property
     * @return ProjectRelatedRelationship|null
    */
    public function getProject(): ?ProjectRelatedRelationship {
        return $this->project;
    }

    /**
     * Gets the supplier property value. The supplier property
     * @return SupplierRelatedRelationship|null
    */
    public function getSupplier(): ?SupplierRelatedRelationship {
        return $this->supplier;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('goodsReceivedNotes', $this->getGoodsReceivedNotes());
        $writer->writeObjectValue('items', $this->getItems());
        $writer->writeObjectValue('project', $this->getProject());
        $writer->writeObjectValue('supplier', $this->getSupplier());
    }

    /**
     * Sets the goodsReceivedNotes property value. The goodsReceivedNotes property
     * @param GoodsReceivedNoteRelatedListRelationship|null $value Value to set for the goodsReceivedNotes property.
    */
    public function setGoodsReceivedNotes(?GoodsReceivedNoteRelatedListRelationship $value): void {
        $this->goodsReceivedNotes = $value;
    }

    /**
     * Sets the items property value. The items property
     * @param PurchaseOrderItemRelatedListRelationship|null $value Value to set for the items property.
    */
    public function setItems(?PurchaseOrderItemRelatedListRelationship $value): void {
        $this->items = $value;
    }

    /**
     * Sets the project property value. The project property
     * @param ProjectRelatedRelationship|null $value Value to set for the project property.
    */
    public function setProject(?ProjectRelatedRelationship $value): void {
        $this->project = $value;
    }

    /**
     * Sets the supplier property value. The supplier property
     * @param SupplierRelatedRelationship|null $value Value to set for the supplier property.
    */
    public function setSupplier(?SupplierRelatedRelationship $value): void {
        $this->supplier = $value;
    }

}
