<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class StockRelationshipsWrite implements Parsable
{
    /**
     * @var NominalRelatedRelationshipWrite|null $nominalCode The nominalCode property
    */
    private ?NominalRelatedRelationshipWrite $nominalCode = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return StockRelationshipsWrite
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): StockRelationshipsWrite {
        return new StockRelationshipsWrite();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'nominalCode' => fn(ParseNode $n) => $o->setNominalCode($n->getObjectValue([NominalRelatedRelationshipWrite::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the nominalCode property value. The nominalCode property
     * @return NominalRelatedRelationshipWrite|null
    */
    public function getNominalCode(): ?NominalRelatedRelationshipWrite {
        return $this->nominalCode;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('nominalCode', $this->getNominalCode());
    }

    /**
     * Sets the nominalCode property value. The nominalCode property
     * @param NominalRelatedRelationshipWrite|null $value Value to set for the nominalCode property.
    */
    public function setNominalCode(?NominalRelatedRelationshipWrite $value): void {
        $this->nominalCode = $value;
    }

}
