<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use DateTime;
use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class GoodsDispatchNoteAttributesRead implements Parsable
{
    /**
     * @var string|null $customerGdnNumber The customerGdnNumber property
    */
    private ?string $customerGdnNumber = null;

    /**
     * @var DateTime|null $date The date property
    */
    private ?DateTime $date = null;

    /**
     * @var string|null $description The description property
    */
    private ?string $description = null;

    /**
     * @var int|null $gdnNumber The gdnNumber property
    */
    private ?int $gdnNumber = null;

    /**
     * @var int|null $itemNumber The itemNumber property
    */
    private ?int $itemNumber = null;

    /**
     * @var int|null $orderItem The orderItem property
    */
    private ?int $orderItem = null;

    /**
     * @var int|null $printedFlag The printedFlag property
    */
    private ?int $printedFlag = null;

    /**
     * @var float|null $qtyDespatched The qtyDespatched property
    */
    private ?float $qtyDespatched = null;

    /**
     * @var float|null $qtyOnOrder The qtyOnOrder property
    */
    private ?float $qtyOnOrder = null;

    /**
     * @var DateTime|null $recordCreateDate The recordCreateDate property
    */
    private ?DateTime $recordCreateDate = null;

    /**
     * @var int|null $recordDeleted The recordDeleted property
    */
    private ?int $recordDeleted = null;

    /**
     * @var DateTime|null $recordModifyDate The recordModifyDate property
    */
    private ?DateTime $recordModifyDate = null;

    /**
     * @var int|null $uniqueId The uniqueId property
    */
    private ?int $uniqueId = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return GoodsDispatchNoteAttributesRead
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): GoodsDispatchNoteAttributesRead {
        return new GoodsDispatchNoteAttributesRead();
    }

    /**
     * Gets the customerGdnNumber property value. The customerGdnNumber property
     * @return string|null
    */
    public function getCustomerGdnNumber(): ?string {
        return $this->customerGdnNumber;
    }

    /**
     * Gets the date property value. The date property
     * @return DateTime|null
    */
    public function getDate(): ?DateTime {
        return $this->date;
    }

    /**
     * Gets the description property value. The description property
     * @return string|null
    */
    public function getDescription(): ?string {
        return $this->description;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'customerGdnNumber' => fn(ParseNode $n) => $o->setCustomerGdnNumber($n->getStringValue()),
            'date' => fn(ParseNode $n) => $o->setDate($n->getDateTimeValue()),
            'description' => fn(ParseNode $n) => $o->setDescription($n->getStringValue()),
            'gdnNumber' => fn(ParseNode $n) => $o->setGdnNumber($n->getIntegerValue()),
            'itemNumber' => fn(ParseNode $n) => $o->setItemNumber($n->getIntegerValue()),
            'orderItem' => fn(ParseNode $n) => $o->setOrderItem($n->getIntegerValue()),
            'printedFlag' => fn(ParseNode $n) => $o->setPrintedFlag($n->getIntegerValue()),
            'qtyDespatched' => fn(ParseNode $n) => $o->setQtyDespatched($n->getFloatValue()),
            'qtyOnOrder' => fn(ParseNode $n) => $o->setQtyOnOrder($n->getFloatValue()),
            'recordCreateDate' => fn(ParseNode $n) => $o->setRecordCreateDate($n->getDateTimeValue()),
            'recordDeleted' => fn(ParseNode $n) => $o->setRecordDeleted($n->getIntegerValue()),
            'recordModifyDate' => fn(ParseNode $n) => $o->setRecordModifyDate($n->getDateTimeValue()),
            'uniqueId' => fn(ParseNode $n) => $o->setUniqueId($n->getIntegerValue()),
        ];
    }

    /**
     * Gets the gdnNumber property value. The gdnNumber property
     * @return int|null
    */
    public function getGdnNumber(): ?int {
        return $this->gdnNumber;
    }

    /**
     * Gets the itemNumber property value. The itemNumber property
     * @return int|null
    */
    public function getItemNumber(): ?int {
        return $this->itemNumber;
    }

    /**
     * Gets the orderItem property value. The orderItem property
     * @return int|null
    */
    public function getOrderItem(): ?int {
        return $this->orderItem;
    }

    /**
     * Gets the printedFlag property value. The printedFlag property
     * @return int|null
    */
    public function getPrintedFlag(): ?int {
        return $this->printedFlag;
    }

    /**
     * Gets the qtyDespatched property value. The qtyDespatched property
     * @return float|null
    */
    public function getQtyDespatched(): ?float {
        return $this->qtyDespatched;
    }

    /**
     * Gets the qtyOnOrder property value. The qtyOnOrder property
     * @return float|null
    */
    public function getQtyOnOrder(): ?float {
        return $this->qtyOnOrder;
    }

    /**
     * Gets the recordCreateDate property value. The recordCreateDate property
     * @return DateTime|null
    */
    public function getRecordCreateDate(): ?DateTime {
        return $this->recordCreateDate;
    }

    /**
     * Gets the recordDeleted property value. The recordDeleted property
     * @return int|null
    */
    public function getRecordDeleted(): ?int {
        return $this->recordDeleted;
    }

    /**
     * Gets the recordModifyDate property value. The recordModifyDate property
     * @return DateTime|null
    */
    public function getRecordModifyDate(): ?DateTime {
        return $this->recordModifyDate;
    }

    /**
     * Gets the uniqueId property value. The uniqueId property
     * @return int|null
    */
    public function getUniqueId(): ?int {
        return $this->uniqueId;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeStringValue('customerGdnNumber', $this->getCustomerGdnNumber());
        $writer->writeDateTimeValue('date', $this->getDate());
        $writer->writeStringValue('description', $this->getDescription());
        $writer->writeIntegerValue('gdnNumber', $this->getGdnNumber());
        $writer->writeIntegerValue('itemNumber', $this->getItemNumber());
        $writer->writeIntegerValue('orderItem', $this->getOrderItem());
        $writer->writeIntegerValue('printedFlag', $this->getPrintedFlag());
        $writer->writeFloatValue('qtyDespatched', $this->getQtyDespatched());
        $writer->writeFloatValue('qtyOnOrder', $this->getQtyOnOrder());
        $writer->writeDateTimeValue('recordCreateDate', $this->getRecordCreateDate());
        $writer->writeIntegerValue('recordDeleted', $this->getRecordDeleted());
        $writer->writeDateTimeValue('recordModifyDate', $this->getRecordModifyDate());
        $writer->writeIntegerValue('uniqueId', $this->getUniqueId());
    }

    /**
     * Sets the customerGdnNumber property value. The customerGdnNumber property
     * @param string|null $value Value to set for the customerGdnNumber property.
    */
    public function setCustomerGdnNumber(?string $value): void {
        $this->customerGdnNumber = $value;
    }

    /**
     * Sets the date property value. The date property
     * @param DateTime|null $value Value to set for the date property.
    */
    public function setDate(?DateTime $value): void {
        $this->date = $value;
    }

    /**
     * Sets the description property value. The description property
     * @param string|null $value Value to set for the description property.
    */
    public function setDescription(?string $value): void {
        $this->description = $value;
    }

    /**
     * Sets the gdnNumber property value. The gdnNumber property
     * @param int|null $value Value to set for the gdnNumber property.
    */
    public function setGdnNumber(?int $value): void {
        $this->gdnNumber = $value;
    }

    /**
     * Sets the itemNumber property value. The itemNumber property
     * @param int|null $value Value to set for the itemNumber property.
    */
    public function setItemNumber(?int $value): void {
        $this->itemNumber = $value;
    }

    /**
     * Sets the orderItem property value. The orderItem property
     * @param int|null $value Value to set for the orderItem property.
    */
    public function setOrderItem(?int $value): void {
        $this->orderItem = $value;
    }

    /**
     * Sets the printedFlag property value. The printedFlag property
     * @param int|null $value Value to set for the printedFlag property.
    */
    public function setPrintedFlag(?int $value): void {
        $this->printedFlag = $value;
    }

    /**
     * Sets the qtyDespatched property value. The qtyDespatched property
     * @param float|null $value Value to set for the qtyDespatched property.
    */
    public function setQtyDespatched(?float $value): void {
        $this->qtyDespatched = $value;
    }

    /**
     * Sets the qtyOnOrder property value. The qtyOnOrder property
     * @param float|null $value Value to set for the qtyOnOrder property.
    */
    public function setQtyOnOrder(?float $value): void {
        $this->qtyOnOrder = $value;
    }

    /**
     * Sets the recordCreateDate property value. The recordCreateDate property
     * @param DateTime|null $value Value to set for the recordCreateDate property.
    */
    public function setRecordCreateDate(?DateTime $value): void {
        $this->recordCreateDate = $value;
    }

    /**
     * Sets the recordDeleted property value. The recordDeleted property
     * @param int|null $value Value to set for the recordDeleted property.
    */
    public function setRecordDeleted(?int $value): void {
        $this->recordDeleted = $value;
    }

    /**
     * Sets the recordModifyDate property value. The recordModifyDate property
     * @param DateTime|null $value Value to set for the recordModifyDate property.
    */
    public function setRecordModifyDate(?DateTime $value): void {
        $this->recordModifyDate = $value;
    }

    /**
     * Sets the uniqueId property value. The uniqueId property
     * @param int|null $value Value to set for the uniqueId property.
    */
    public function setUniqueId(?int $value): void {
        $this->uniqueId = $value;
    }

}
