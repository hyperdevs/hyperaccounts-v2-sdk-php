<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class SalesOrderItemRelationships implements Parsable
{
    /**
     * @var NominalRelatedRelationship|null $nominalCode The nominalCode property
    */
    private ?NominalRelatedRelationship $nominalCode = null;

    /**
     * @var ProjectRelatedRelationship|null $project The project property
    */
    private ?ProjectRelatedRelationship $project = null;

    /**
     * @var SalesOrderRelatedRelationship|null $salesOrder The salesOrder property
    */
    private ?SalesOrderRelatedRelationship $salesOrder = null;

    /**
     * @var StockRelatedRelationship|null $stock The stock property
    */
    private ?StockRelatedRelationship $stock = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return SalesOrderItemRelationships
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): SalesOrderItemRelationships {
        return new SalesOrderItemRelationships();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'nominalCode' => fn(ParseNode $n) => $o->setNominalCode($n->getObjectValue([NominalRelatedRelationship::class, 'createFromDiscriminatorValue'])),
            'project' => fn(ParseNode $n) => $o->setProject($n->getObjectValue([ProjectRelatedRelationship::class, 'createFromDiscriminatorValue'])),
            'salesOrder' => fn(ParseNode $n) => $o->setSalesOrder($n->getObjectValue([SalesOrderRelatedRelationship::class, 'createFromDiscriminatorValue'])),
            'stock' => fn(ParseNode $n) => $o->setStock($n->getObjectValue([StockRelatedRelationship::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the nominalCode property value. The nominalCode property
     * @return NominalRelatedRelationship|null
    */
    public function getNominalCode(): ?NominalRelatedRelationship {
        return $this->nominalCode;
    }

    /**
     * Gets the project property value. The project property
     * @return ProjectRelatedRelationship|null
    */
    public function getProject(): ?ProjectRelatedRelationship {
        return $this->project;
    }

    /**
     * Gets the salesOrder property value. The salesOrder property
     * @return SalesOrderRelatedRelationship|null
    */
    public function getSalesOrder(): ?SalesOrderRelatedRelationship {
        return $this->salesOrder;
    }

    /**
     * Gets the stock property value. The stock property
     * @return StockRelatedRelationship|null
    */
    public function getStock(): ?StockRelatedRelationship {
        return $this->stock;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('nominalCode', $this->getNominalCode());
        $writer->writeObjectValue('project', $this->getProject());
        $writer->writeObjectValue('salesOrder', $this->getSalesOrder());
        $writer->writeObjectValue('stock', $this->getStock());
    }

    /**
     * Sets the nominalCode property value. The nominalCode property
     * @param NominalRelatedRelationship|null $value Value to set for the nominalCode property.
    */
    public function setNominalCode(?NominalRelatedRelationship $value): void {
        $this->nominalCode = $value;
    }

    /**
     * Sets the project property value. The project property
     * @param ProjectRelatedRelationship|null $value Value to set for the project property.
    */
    public function setProject(?ProjectRelatedRelationship $value): void {
        $this->project = $value;
    }

    /**
     * Sets the salesOrder property value. The salesOrder property
     * @param SalesOrderRelatedRelationship|null $value Value to set for the salesOrder property.
    */
    public function setSalesOrder(?SalesOrderRelatedRelationship $value): void {
        $this->salesOrder = $value;
    }

    /**
     * Sets the stock property value. The stock property
     * @param StockRelatedRelationship|null $value Value to set for the stock property.
    */
    public function setStock(?StockRelatedRelationship $value): void {
        $this->stock = $value;
    }

}
