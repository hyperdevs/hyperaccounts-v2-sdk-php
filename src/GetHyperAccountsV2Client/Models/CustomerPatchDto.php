<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class CustomerPatchDto implements Parsable
{
    /**
     * @var CustomerAttributesEdit|null $attributes The attributes property
    */
    private ?CustomerAttributesEdit $attributes = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return CustomerPatchDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): CustomerPatchDto {
        return new CustomerPatchDto();
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return CustomerAttributesEdit|null
    */
    public function getAttributes(): ?CustomerAttributesEdit {
        return $this->attributes;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([CustomerAttributesEdit::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('attributes', $this->getAttributes());
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param CustomerAttributesEdit|null $value Value to set for the attributes property.
    */
    public function setAttributes(?CustomerAttributesEdit $value): void {
        $this->attributes = $value;
    }

}
