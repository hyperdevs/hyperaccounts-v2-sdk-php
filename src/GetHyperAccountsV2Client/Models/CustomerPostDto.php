<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class CustomerPostDto implements Parsable
{
    /**
     * @var string|null $accountRef The accountRef property
    */
    private ?string $accountRef = null;

    /**
     * @var CustomerAttributesWrite|null $attributes The attributes property
    */
    private ?CustomerAttributesWrite $attributes = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return CustomerPostDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): CustomerPostDto {
        return new CustomerPostDto();
    }

    /**
     * Gets the accountRef property value. The accountRef property
     * @return string|null
    */
    public function getAccountRef(): ?string {
        return $this->accountRef;
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return CustomerAttributesWrite|null
    */
    public function getAttributes(): ?CustomerAttributesWrite {
        return $this->attributes;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'accountRef' => fn(ParseNode $n) => $o->setAccountRef($n->getStringValue()),
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([CustomerAttributesWrite::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeStringValue('accountRef', $this->getAccountRef());
        $writer->writeObjectValue('attributes', $this->getAttributes());
    }

    /**
     * Sets the accountRef property value. The accountRef property
     * @param string|null $value Value to set for the accountRef property.
    */
    public function setAccountRef(?string $value): void {
        $this->accountRef = $value;
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param CustomerAttributesWrite|null $value Value to set for the attributes property.
    */
    public function setAttributes(?CustomerAttributesWrite $value): void {
        $this->attributes = $value;
    }

}
