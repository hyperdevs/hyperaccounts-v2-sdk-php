<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class ProjectOnlyTransactionRelated implements Parsable
{
    /**
     * @var int|null $projectTranId The projectTranId property
    */
    private ?int $projectTranId = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return ProjectOnlyTransactionRelated
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): ProjectOnlyTransactionRelated {
        return new ProjectOnlyTransactionRelated();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'projectTranId' => fn(ParseNode $n) => $o->setProjectTranId($n->getIntegerValue()),
        ];
    }

    /**
     * Gets the projectTranId property value. The projectTranId property
     * @return int|null
    */
    public function getProjectTranId(): ?int {
        return $this->projectTranId;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeIntegerValue('projectTranId', $this->getProjectTranId());
    }

    /**
     * Sets the projectTranId property value. The projectTranId property
     * @param int|null $value Value to set for the projectTranId property.
    */
    public function setProjectTranId(?int $value): void {
        $this->projectTranId = $value;
    }

}
