<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class SalesOrderItemPatchDto implements Parsable
{
    /**
     * @var SalesOrderItemAttributesEdit|null $attributes The attributes property
    */
    private ?SalesOrderItemAttributesEdit $attributes = null;

    /**
     * @var SalesOrderItemRelationshipsEdit|null $relationships The relationships property
    */
    private ?SalesOrderItemRelationshipsEdit $relationships = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return SalesOrderItemPatchDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): SalesOrderItemPatchDto {
        return new SalesOrderItemPatchDto();
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return SalesOrderItemAttributesEdit|null
    */
    public function getAttributes(): ?SalesOrderItemAttributesEdit {
        return $this->attributes;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([SalesOrderItemAttributesEdit::class, 'createFromDiscriminatorValue'])),
            'relationships' => fn(ParseNode $n) => $o->setRelationships($n->getObjectValue([SalesOrderItemRelationshipsEdit::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the relationships property value. The relationships property
     * @return SalesOrderItemRelationshipsEdit|null
    */
    public function getRelationships(): ?SalesOrderItemRelationshipsEdit {
        return $this->relationships;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('attributes', $this->getAttributes());
        $writer->writeObjectValue('relationships', $this->getRelationships());
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param SalesOrderItemAttributesEdit|null $value Value to set for the attributes property.
    */
    public function setAttributes(?SalesOrderItemAttributesEdit $value): void {
        $this->attributes = $value;
    }

    /**
     * Sets the relationships property value. The relationships property
     * @param SalesOrderItemRelationshipsEdit|null $value Value to set for the relationships property.
    */
    public function setRelationships(?SalesOrderItemRelationshipsEdit $value): void {
        $this->relationships = $value;
    }

}
