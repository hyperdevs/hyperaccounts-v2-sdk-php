<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class ProjectRelationshipsWrite implements Parsable
{
    /**
     * @var CustomerRelatedRelationshipWrite|null $customer The customer property
    */
    private ?CustomerRelatedRelationshipWrite $customer = null;

    /**
     * @var ProjectStatusRelatedRelationshipWrite|null $status The status property
    */
    private ?ProjectStatusRelatedRelationshipWrite $status = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return ProjectRelationshipsWrite
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): ProjectRelationshipsWrite {
        return new ProjectRelationshipsWrite();
    }

    /**
     * Gets the customer property value. The customer property
     * @return CustomerRelatedRelationshipWrite|null
    */
    public function getCustomer(): ?CustomerRelatedRelationshipWrite {
        return $this->customer;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'customer' => fn(ParseNode $n) => $o->setCustomer($n->getObjectValue([CustomerRelatedRelationshipWrite::class, 'createFromDiscriminatorValue'])),
            'status' => fn(ParseNode $n) => $o->setStatus($n->getObjectValue([ProjectStatusRelatedRelationshipWrite::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the status property value. The status property
     * @return ProjectStatusRelatedRelationshipWrite|null
    */
    public function getStatus(): ?ProjectStatusRelatedRelationshipWrite {
        return $this->status;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('customer', $this->getCustomer());
        $writer->writeObjectValue('status', $this->getStatus());
    }

    /**
     * Sets the customer property value. The customer property
     * @param CustomerRelatedRelationshipWrite|null $value Value to set for the customer property.
    */
    public function setCustomer(?CustomerRelatedRelationshipWrite $value): void {
        $this->customer = $value;
    }

    /**
     * Sets the status property value. The status property
     * @param ProjectStatusRelatedRelationshipWrite|null $value Value to set for the status property.
    */
    public function setStatus(?ProjectStatusRelatedRelationshipWrite $value): void {
        $this->status = $value;
    }

}
