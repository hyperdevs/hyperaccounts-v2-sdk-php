<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class TransactionUsageGetDto implements Parsable
{
    /**
     * @var TransactionUsageAttributesRead|null $attributes The attributes property
    */
    private ?TransactionUsageAttributesRead $attributes = null;

    /**
     * @var TransactionUsageIncluded|null $included The included property
    */
    private ?TransactionUsageIncluded $included = null;

    /**
     * @var ResourceLinks|null $links The links property
    */
    private ?ResourceLinks $links = null;

    /**
     * @var TransactionUsageRelationships|null $relationships The relationships property
    */
    private ?TransactionUsageRelationships $relationships = null;

    /**
     * @var int|null $usageNumber The usageNumber property
    */
    private ?int $usageNumber = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return TransactionUsageGetDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): TransactionUsageGetDto {
        return new TransactionUsageGetDto();
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return TransactionUsageAttributesRead|null
    */
    public function getAttributes(): ?TransactionUsageAttributesRead {
        return $this->attributes;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([TransactionUsageAttributesRead::class, 'createFromDiscriminatorValue'])),
            'included' => fn(ParseNode $n) => $o->setIncluded($n->getObjectValue([TransactionUsageIncluded::class, 'createFromDiscriminatorValue'])),
            'links' => fn(ParseNode $n) => $o->setLinks($n->getObjectValue([ResourceLinks::class, 'createFromDiscriminatorValue'])),
            'relationships' => fn(ParseNode $n) => $o->setRelationships($n->getObjectValue([TransactionUsageRelationships::class, 'createFromDiscriminatorValue'])),
            'usageNumber' => fn(ParseNode $n) => $o->setUsageNumber($n->getIntegerValue()),
        ];
    }

    /**
     * Gets the included property value. The included property
     * @return TransactionUsageIncluded|null
    */
    public function getIncluded(): ?TransactionUsageIncluded {
        return $this->included;
    }

    /**
     * Gets the links property value. The links property
     * @return ResourceLinks|null
    */
    public function getLinks(): ?ResourceLinks {
        return $this->links;
    }

    /**
     * Gets the relationships property value. The relationships property
     * @return TransactionUsageRelationships|null
    */
    public function getRelationships(): ?TransactionUsageRelationships {
        return $this->relationships;
    }

    /**
     * Gets the usageNumber property value. The usageNumber property
     * @return int|null
    */
    public function getUsageNumber(): ?int {
        return $this->usageNumber;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('attributes', $this->getAttributes());
        $writer->writeObjectValue('included', $this->getIncluded());
        $writer->writeObjectValue('links', $this->getLinks());
        $writer->writeObjectValue('relationships', $this->getRelationships());
        $writer->writeIntegerValue('usageNumber', $this->getUsageNumber());
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param TransactionUsageAttributesRead|null $value Value to set for the attributes property.
    */
    public function setAttributes(?TransactionUsageAttributesRead $value): void {
        $this->attributes = $value;
    }

    /**
     * Sets the included property value. The included property
     * @param TransactionUsageIncluded|null $value Value to set for the included property.
    */
    public function setIncluded(?TransactionUsageIncluded $value): void {
        $this->included = $value;
    }

    /**
     * Sets the links property value. The links property
     * @param ResourceLinks|null $value Value to set for the links property.
    */
    public function setLinks(?ResourceLinks $value): void {
        $this->links = $value;
    }

    /**
     * Sets the relationships property value. The relationships property
     * @param TransactionUsageRelationships|null $value Value to set for the relationships property.
    */
    public function setRelationships(?TransactionUsageRelationships $value): void {
        $this->relationships = $value;
    }

    /**
     * Sets the usageNumber property value. The usageNumber property
     * @param int|null $value Value to set for the usageNumber property.
    */
    public function setUsageNumber(?int $value): void {
        $this->usageNumber = $value;
    }

}
