<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class ProjectBudgetGetDto implements Parsable
{
    /**
     * @var ProjectBudgetAttributesRead|null $attributes The attributes property
    */
    private ?ProjectBudgetAttributesRead $attributes = null;

    /**
     * @var string|null $compositeKeyProjectIdAndCostCodeId The compositeKeyProjectIdAndCostCodeId property
    */
    private ?string $compositeKeyProjectIdAndCostCodeId = null;

    /**
     * @var ProjectBudgetIncluded|null $included The included property
    */
    private ?ProjectBudgetIncluded $included = null;

    /**
     * @var ResourceLinks|null $links The links property
    */
    private ?ResourceLinks $links = null;

    /**
     * @var ProjectBudgetRelationships|null $relationships The relationships property
    */
    private ?ProjectBudgetRelationships $relationships = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return ProjectBudgetGetDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): ProjectBudgetGetDto {
        return new ProjectBudgetGetDto();
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return ProjectBudgetAttributesRead|null
    */
    public function getAttributes(): ?ProjectBudgetAttributesRead {
        return $this->attributes;
    }

    /**
     * Gets the compositeKeyProjectIdAndCostCodeId property value. The compositeKeyProjectIdAndCostCodeId property
     * @return string|null
    */
    public function getCompositeKeyProjectIdAndCostCodeId(): ?string {
        return $this->compositeKeyProjectIdAndCostCodeId;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([ProjectBudgetAttributesRead::class, 'createFromDiscriminatorValue'])),
            'compositeKeyProjectIdAndCostCodeId' => fn(ParseNode $n) => $o->setCompositeKeyProjectIdAndCostCodeId($n->getStringValue()),
            'included' => fn(ParseNode $n) => $o->setIncluded($n->getObjectValue([ProjectBudgetIncluded::class, 'createFromDiscriminatorValue'])),
            'links' => fn(ParseNode $n) => $o->setLinks($n->getObjectValue([ResourceLinks::class, 'createFromDiscriminatorValue'])),
            'relationships' => fn(ParseNode $n) => $o->setRelationships($n->getObjectValue([ProjectBudgetRelationships::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the included property value. The included property
     * @return ProjectBudgetIncluded|null
    */
    public function getIncluded(): ?ProjectBudgetIncluded {
        return $this->included;
    }

    /**
     * Gets the links property value. The links property
     * @return ResourceLinks|null
    */
    public function getLinks(): ?ResourceLinks {
        return $this->links;
    }

    /**
     * Gets the relationships property value. The relationships property
     * @return ProjectBudgetRelationships|null
    */
    public function getRelationships(): ?ProjectBudgetRelationships {
        return $this->relationships;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('attributes', $this->getAttributes());
        $writer->writeObjectValue('included', $this->getIncluded());
        $writer->writeObjectValue('links', $this->getLinks());
        $writer->writeObjectValue('relationships', $this->getRelationships());
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param ProjectBudgetAttributesRead|null $value Value to set for the attributes property.
    */
    public function setAttributes(?ProjectBudgetAttributesRead $value): void {
        $this->attributes = $value;
    }

    /**
     * Sets the compositeKeyProjectIdAndCostCodeId property value. The compositeKeyProjectIdAndCostCodeId property
     * @param string|null $value Value to set for the compositeKeyProjectIdAndCostCodeId property.
    */
    public function setCompositeKeyProjectIdAndCostCodeId(?string $value): void {
        $this->compositeKeyProjectIdAndCostCodeId = $value;
    }

    /**
     * Sets the included property value. The included property
     * @param ProjectBudgetIncluded|null $value Value to set for the included property.
    */
    public function setIncluded(?ProjectBudgetIncluded $value): void {
        $this->included = $value;
    }

    /**
     * Sets the links property value. The links property
     * @param ResourceLinks|null $value Value to set for the links property.
    */
    public function setLinks(?ResourceLinks $value): void {
        $this->links = $value;
    }

    /**
     * Sets the relationships property value. The relationships property
     * @param ProjectBudgetRelationships|null $value Value to set for the relationships property.
    */
    public function setRelationships(?ProjectBudgetRelationships $value): void {
        $this->relationships = $value;
    }

}
