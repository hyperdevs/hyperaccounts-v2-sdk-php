<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class PurchaseOrderItemRelationshipsRead implements Parsable
{
    /**
     * @var NominalRelatedRelationship|null $nominalCode The nominalCode property
    */
    private ?NominalRelatedRelationship $nominalCode = null;

    /**
     * @var PurchaseOrderRelatedRelationship|null $purchaseOrder The purchaseOrder property
    */
    private ?PurchaseOrderRelatedRelationship $purchaseOrder = null;

    /**
     * @var StockRelatedRelationship|null $stock The stock property
    */
    private ?StockRelatedRelationship $stock = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return PurchaseOrderItemRelationshipsRead
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): PurchaseOrderItemRelationshipsRead {
        return new PurchaseOrderItemRelationshipsRead();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'nominalCode' => fn(ParseNode $n) => $o->setNominalCode($n->getObjectValue([NominalRelatedRelationship::class, 'createFromDiscriminatorValue'])),
            'purchaseOrder' => fn(ParseNode $n) => $o->setPurchaseOrder($n->getObjectValue([PurchaseOrderRelatedRelationship::class, 'createFromDiscriminatorValue'])),
            'stock' => fn(ParseNode $n) => $o->setStock($n->getObjectValue([StockRelatedRelationship::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the nominalCode property value. The nominalCode property
     * @return NominalRelatedRelationship|null
    */
    public function getNominalCode(): ?NominalRelatedRelationship {
        return $this->nominalCode;
    }

    /**
     * Gets the purchaseOrder property value. The purchaseOrder property
     * @return PurchaseOrderRelatedRelationship|null
    */
    public function getPurchaseOrder(): ?PurchaseOrderRelatedRelationship {
        return $this->purchaseOrder;
    }

    /**
     * Gets the stock property value. The stock property
     * @return StockRelatedRelationship|null
    */
    public function getStock(): ?StockRelatedRelationship {
        return $this->stock;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('nominalCode', $this->getNominalCode());
        $writer->writeObjectValue('purchaseOrder', $this->getPurchaseOrder());
        $writer->writeObjectValue('stock', $this->getStock());
    }

    /**
     * Sets the nominalCode property value. The nominalCode property
     * @param NominalRelatedRelationship|null $value Value to set for the nominalCode property.
    */
    public function setNominalCode(?NominalRelatedRelationship $value): void {
        $this->nominalCode = $value;
    }

    /**
     * Sets the purchaseOrder property value. The purchaseOrder property
     * @param PurchaseOrderRelatedRelationship|null $value Value to set for the purchaseOrder property.
    */
    public function setPurchaseOrder(?PurchaseOrderRelatedRelationship $value): void {
        $this->purchaseOrder = $value;
    }

    /**
     * Sets the stock property value. The stock property
     * @param StockRelatedRelationship|null $value Value to set for the stock property.
    */
    public function setStock(?StockRelatedRelationship $value): void {
        $this->stock = $value;
    }

}
