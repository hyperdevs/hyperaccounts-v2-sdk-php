<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class TransactionSplitGetDto implements Parsable
{
    /**
     * @var TransactionSplitAttributesRead|null $attributes The attributes property
    */
    private ?TransactionSplitAttributesRead $attributes = null;

    /**
     * @var TransactionSplitIncluded|null $included The included property
    */
    private ?TransactionSplitIncluded $included = null;

    /**
     * @var ResourceLinks|null $links The links property
    */
    private ?ResourceLinks $links = null;

    /**
     * @var TransactionSplitRelationships|null $relationships The relationships property
    */
    private ?TransactionSplitRelationships $relationships = null;

    /**
     * @var int|null $splitNumber The splitNumber property
    */
    private ?int $splitNumber = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return TransactionSplitGetDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): TransactionSplitGetDto {
        return new TransactionSplitGetDto();
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return TransactionSplitAttributesRead|null
    */
    public function getAttributes(): ?TransactionSplitAttributesRead {
        return $this->attributes;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([TransactionSplitAttributesRead::class, 'createFromDiscriminatorValue'])),
            'included' => fn(ParseNode $n) => $o->setIncluded($n->getObjectValue([TransactionSplitIncluded::class, 'createFromDiscriminatorValue'])),
            'links' => fn(ParseNode $n) => $o->setLinks($n->getObjectValue([ResourceLinks::class, 'createFromDiscriminatorValue'])),
            'relationships' => fn(ParseNode $n) => $o->setRelationships($n->getObjectValue([TransactionSplitRelationships::class, 'createFromDiscriminatorValue'])),
            'splitNumber' => fn(ParseNode $n) => $o->setSplitNumber($n->getIntegerValue()),
        ];
    }

    /**
     * Gets the included property value. The included property
     * @return TransactionSplitIncluded|null
    */
    public function getIncluded(): ?TransactionSplitIncluded {
        return $this->included;
    }

    /**
     * Gets the links property value. The links property
     * @return ResourceLinks|null
    */
    public function getLinks(): ?ResourceLinks {
        return $this->links;
    }

    /**
     * Gets the relationships property value. The relationships property
     * @return TransactionSplitRelationships|null
    */
    public function getRelationships(): ?TransactionSplitRelationships {
        return $this->relationships;
    }

    /**
     * Gets the splitNumber property value. The splitNumber property
     * @return int|null
    */
    public function getSplitNumber(): ?int {
        return $this->splitNumber;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('attributes', $this->getAttributes());
        $writer->writeObjectValue('included', $this->getIncluded());
        $writer->writeObjectValue('links', $this->getLinks());
        $writer->writeObjectValue('relationships', $this->getRelationships());
        $writer->writeIntegerValue('splitNumber', $this->getSplitNumber());
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param TransactionSplitAttributesRead|null $value Value to set for the attributes property.
    */
    public function setAttributes(?TransactionSplitAttributesRead $value): void {
        $this->attributes = $value;
    }

    /**
     * Sets the included property value. The included property
     * @param TransactionSplitIncluded|null $value Value to set for the included property.
    */
    public function setIncluded(?TransactionSplitIncluded $value): void {
        $this->included = $value;
    }

    /**
     * Sets the links property value. The links property
     * @param ResourceLinks|null $value Value to set for the links property.
    */
    public function setLinks(?ResourceLinks $value): void {
        $this->links = $value;
    }

    /**
     * Sets the relationships property value. The relationships property
     * @param TransactionSplitRelationships|null $value Value to set for the relationships property.
    */
    public function setRelationships(?TransactionSplitRelationships $value): void {
        $this->relationships = $value;
    }

    /**
     * Sets the splitNumber property value. The splitNumber property
     * @param int|null $value Value to set for the splitNumber property.
    */
    public function setSplitNumber(?int $value): void {
        $this->splitNumber = $value;
    }

}
