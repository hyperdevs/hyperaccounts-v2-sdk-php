<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Enum;

class TransactionSplitAttributesRead_vatFlag extends Enum {
    public const NOT_APPLICABLE = 'NotApplicable';
    public const NO = 'No';
    public const YES = 'Yes';
}
