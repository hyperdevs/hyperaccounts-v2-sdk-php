<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class StockTransactionPostDto implements Parsable
{
    /**
     * @var StockTransactionAttributesWrite|null $attributes The attributes property
    */
    private ?StockTransactionAttributesWrite $attributes = null;

    /**
     * @var StockTransactionRelationshipsWrite|null $relationships The relationships property
    */
    private ?StockTransactionRelationshipsWrite $relationships = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return StockTransactionPostDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): StockTransactionPostDto {
        return new StockTransactionPostDto();
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return StockTransactionAttributesWrite|null
    */
    public function getAttributes(): ?StockTransactionAttributesWrite {
        return $this->attributes;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([StockTransactionAttributesWrite::class, 'createFromDiscriminatorValue'])),
            'relationships' => fn(ParseNode $n) => $o->setRelationships($n->getObjectValue([StockTransactionRelationshipsWrite::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the relationships property value. The relationships property
     * @return StockTransactionRelationshipsWrite|null
    */
    public function getRelationships(): ?StockTransactionRelationshipsWrite {
        return $this->relationships;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('attributes', $this->getAttributes());
        $writer->writeObjectValue('relationships', $this->getRelationships());
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param StockTransactionAttributesWrite|null $value Value to set for the attributes property.
    */
    public function setAttributes(?StockTransactionAttributesWrite $value): void {
        $this->attributes = $value;
    }

    /**
     * Sets the relationships property value. The relationships property
     * @param StockTransactionRelationshipsWrite|null $value Value to set for the relationships property.
    */
    public function setRelationships(?StockTransactionRelationshipsWrite $value): void {
        $this->relationships = $value;
    }

}
