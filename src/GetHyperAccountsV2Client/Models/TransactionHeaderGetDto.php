<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class TransactionHeaderGetDto implements Parsable
{
    /**
     * @var TransactionHeaderAttributesRead|null $attributes The attributes property
    */
    private ?TransactionHeaderAttributesRead $attributes = null;

    /**
     * @var int|null $headerNumber The headerNumber property
    */
    private ?int $headerNumber = null;

    /**
     * @var TransactionHeaderIncluded|null $included The included property
    */
    private ?TransactionHeaderIncluded $included = null;

    /**
     * @var ResourceLinks|null $links The links property
    */
    private ?ResourceLinks $links = null;

    /**
     * @var TransactionHeaderRelationships|null $relationships The relationships property
    */
    private ?TransactionHeaderRelationships $relationships = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return TransactionHeaderGetDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): TransactionHeaderGetDto {
        return new TransactionHeaderGetDto();
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return TransactionHeaderAttributesRead|null
    */
    public function getAttributes(): ?TransactionHeaderAttributesRead {
        return $this->attributes;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([TransactionHeaderAttributesRead::class, 'createFromDiscriminatorValue'])),
            'headerNumber' => fn(ParseNode $n) => $o->setHeaderNumber($n->getIntegerValue()),
            'included' => fn(ParseNode $n) => $o->setIncluded($n->getObjectValue([TransactionHeaderIncluded::class, 'createFromDiscriminatorValue'])),
            'links' => fn(ParseNode $n) => $o->setLinks($n->getObjectValue([ResourceLinks::class, 'createFromDiscriminatorValue'])),
            'relationships' => fn(ParseNode $n) => $o->setRelationships($n->getObjectValue([TransactionHeaderRelationships::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the headerNumber property value. The headerNumber property
     * @return int|null
    */
    public function getHeaderNumber(): ?int {
        return $this->headerNumber;
    }

    /**
     * Gets the included property value. The included property
     * @return TransactionHeaderIncluded|null
    */
    public function getIncluded(): ?TransactionHeaderIncluded {
        return $this->included;
    }

    /**
     * Gets the links property value. The links property
     * @return ResourceLinks|null
    */
    public function getLinks(): ?ResourceLinks {
        return $this->links;
    }

    /**
     * Gets the relationships property value. The relationships property
     * @return TransactionHeaderRelationships|null
    */
    public function getRelationships(): ?TransactionHeaderRelationships {
        return $this->relationships;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('attributes', $this->getAttributes());
        $writer->writeIntegerValue('headerNumber', $this->getHeaderNumber());
        $writer->writeObjectValue('included', $this->getIncluded());
        $writer->writeObjectValue('links', $this->getLinks());
        $writer->writeObjectValue('relationships', $this->getRelationships());
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param TransactionHeaderAttributesRead|null $value Value to set for the attributes property.
    */
    public function setAttributes(?TransactionHeaderAttributesRead $value): void {
        $this->attributes = $value;
    }

    /**
     * Sets the headerNumber property value. The headerNumber property
     * @param int|null $value Value to set for the headerNumber property.
    */
    public function setHeaderNumber(?int $value): void {
        $this->headerNumber = $value;
    }

    /**
     * Sets the included property value. The included property
     * @param TransactionHeaderIncluded|null $value Value to set for the included property.
    */
    public function setIncluded(?TransactionHeaderIncluded $value): void {
        $this->included = $value;
    }

    /**
     * Sets the links property value. The links property
     * @param ResourceLinks|null $value Value to set for the links property.
    */
    public function setLinks(?ResourceLinks $value): void {
        $this->links = $value;
    }

    /**
     * Sets the relationships property value. The relationships property
     * @param TransactionHeaderRelationships|null $value Value to set for the relationships property.
    */
    public function setRelationships(?TransactionHeaderRelationships $value): void {
        $this->relationships = $value;
    }

}
