<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class TaxCodeAttributesRead implements Parsable
{
    /**
     * @var string|null $description The description property
    */
    private ?string $description = null;

    /**
     * @var int|null $euroPurchase The euroPurchase property
    */
    private ?int $euroPurchase = null;

    /**
     * @var int|null $euroSales The euroSales property
    */
    private ?int $euroSales = null;

    /**
     * @var int|null $isReversecharge The isReversecharge property
    */
    private ?int $isReversecharge = null;

    /**
     * @var int|null $ossiossCode The ossiossCode property
    */
    private ?int $ossiossCode = null;

    /**
     * @var int|null $overrideRelatedRate The overrideRelatedRate property
    */
    private ?int $overrideRelatedRate = null;

    /**
     * @var int|null $postponed The postponed property
    */
    private ?int $postponed = null;

    /**
     * @var int|null $relatedCode The relatedCode property
    */
    private ?int $relatedCode = null;

    /**
     * @var float|null $relatedRate The relatedRate property
    */
    private ?float $relatedRate = null;

    /**
     * @var string|null $taxCode The taxCode property
    */
    private ?string $taxCode = null;

    /**
     * @var float|null $taxRate The taxRate property
    */
    private ?float $taxRate = null;

    /**
     * @var int|null $ukNonCustomsUnionImportExport The ukNonCustomsUnionImportExport property
    */
    private ?int $ukNonCustomsUnionImportExport = null;

    /**
     * @var int|null $vatInclude The vatInclude property
    */
    private ?int $vatInclude = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return TaxCodeAttributesRead
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): TaxCodeAttributesRead {
        return new TaxCodeAttributesRead();
    }

    /**
     * Gets the description property value. The description property
     * @return string|null
    */
    public function getDescription(): ?string {
        return $this->description;
    }

    /**
     * Gets the euroPurchase property value. The euroPurchase property
     * @return int|null
    */
    public function getEuroPurchase(): ?int {
        return $this->euroPurchase;
    }

    /**
     * Gets the euroSales property value. The euroSales property
     * @return int|null
    */
    public function getEuroSales(): ?int {
        return $this->euroSales;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'description' => fn(ParseNode $n) => $o->setDescription($n->getStringValue()),
            'euroPurchase' => fn(ParseNode $n) => $o->setEuroPurchase($n->getIntegerValue()),
            'euroSales' => fn(ParseNode $n) => $o->setEuroSales($n->getIntegerValue()),
            'isReversecharge' => fn(ParseNode $n) => $o->setIsReversecharge($n->getIntegerValue()),
            'ossiossCode' => fn(ParseNode $n) => $o->setOssiossCode($n->getIntegerValue()),
            'overrideRelatedRate' => fn(ParseNode $n) => $o->setOverrideRelatedRate($n->getIntegerValue()),
            'postponed' => fn(ParseNode $n) => $o->setPostponed($n->getIntegerValue()),
            'relatedCode' => fn(ParseNode $n) => $o->setRelatedCode($n->getIntegerValue()),
            'relatedRate' => fn(ParseNode $n) => $o->setRelatedRate($n->getFloatValue()),
            'taxCode' => fn(ParseNode $n) => $o->setTaxCode($n->getStringValue()),
            'taxRate' => fn(ParseNode $n) => $o->setTaxRate($n->getFloatValue()),
            'ukNonCustomsUnionImportExport' => fn(ParseNode $n) => $o->setUkNonCustomsUnionImportExport($n->getIntegerValue()),
            'vatInclude' => fn(ParseNode $n) => $o->setVatInclude($n->getIntegerValue()),
        ];
    }

    /**
     * Gets the isReversecharge property value. The isReversecharge property
     * @return int|null
    */
    public function getIsReversecharge(): ?int {
        return $this->isReversecharge;
    }

    /**
     * Gets the ossiossCode property value. The ossiossCode property
     * @return int|null
    */
    public function getOssiossCode(): ?int {
        return $this->ossiossCode;
    }

    /**
     * Gets the overrideRelatedRate property value. The overrideRelatedRate property
     * @return int|null
    */
    public function getOverrideRelatedRate(): ?int {
        return $this->overrideRelatedRate;
    }

    /**
     * Gets the postponed property value. The postponed property
     * @return int|null
    */
    public function getPostponed(): ?int {
        return $this->postponed;
    }

    /**
     * Gets the relatedCode property value. The relatedCode property
     * @return int|null
    */
    public function getRelatedCode(): ?int {
        return $this->relatedCode;
    }

    /**
     * Gets the relatedRate property value. The relatedRate property
     * @return float|null
    */
    public function getRelatedRate(): ?float {
        return $this->relatedRate;
    }

    /**
     * Gets the taxCode property value. The taxCode property
     * @return string|null
    */
    public function getTaxCode(): ?string {
        return $this->taxCode;
    }

    /**
     * Gets the taxRate property value. The taxRate property
     * @return float|null
    */
    public function getTaxRate(): ?float {
        return $this->taxRate;
    }

    /**
     * Gets the ukNonCustomsUnionImportExport property value. The ukNonCustomsUnionImportExport property
     * @return int|null
    */
    public function getUkNonCustomsUnionImportExport(): ?int {
        return $this->ukNonCustomsUnionImportExport;
    }

    /**
     * Gets the vatInclude property value. The vatInclude property
     * @return int|null
    */
    public function getVatInclude(): ?int {
        return $this->vatInclude;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeStringValue('description', $this->getDescription());
        $writer->writeIntegerValue('euroPurchase', $this->getEuroPurchase());
        $writer->writeIntegerValue('euroSales', $this->getEuroSales());
        $writer->writeIntegerValue('isReversecharge', $this->getIsReversecharge());
        $writer->writeIntegerValue('ossiossCode', $this->getOssiossCode());
        $writer->writeIntegerValue('overrideRelatedRate', $this->getOverrideRelatedRate());
        $writer->writeIntegerValue('postponed', $this->getPostponed());
        $writer->writeIntegerValue('relatedCode', $this->getRelatedCode());
        $writer->writeFloatValue('relatedRate', $this->getRelatedRate());
        $writer->writeStringValue('taxCode', $this->getTaxCode());
        $writer->writeFloatValue('taxRate', $this->getTaxRate());
        $writer->writeIntegerValue('ukNonCustomsUnionImportExport', $this->getUkNonCustomsUnionImportExport());
        $writer->writeIntegerValue('vatInclude', $this->getVatInclude());
    }

    /**
     * Sets the description property value. The description property
     * @param string|null $value Value to set for the description property.
    */
    public function setDescription(?string $value): void {
        $this->description = $value;
    }

    /**
     * Sets the euroPurchase property value. The euroPurchase property
     * @param int|null $value Value to set for the euroPurchase property.
    */
    public function setEuroPurchase(?int $value): void {
        $this->euroPurchase = $value;
    }

    /**
     * Sets the euroSales property value. The euroSales property
     * @param int|null $value Value to set for the euroSales property.
    */
    public function setEuroSales(?int $value): void {
        $this->euroSales = $value;
    }

    /**
     * Sets the isReversecharge property value. The isReversecharge property
     * @param int|null $value Value to set for the isReversecharge property.
    */
    public function setIsReversecharge(?int $value): void {
        $this->isReversecharge = $value;
    }

    /**
     * Sets the ossiossCode property value. The ossiossCode property
     * @param int|null $value Value to set for the ossiossCode property.
    */
    public function setOssiossCode(?int $value): void {
        $this->ossiossCode = $value;
    }

    /**
     * Sets the overrideRelatedRate property value. The overrideRelatedRate property
     * @param int|null $value Value to set for the overrideRelatedRate property.
    */
    public function setOverrideRelatedRate(?int $value): void {
        $this->overrideRelatedRate = $value;
    }

    /**
     * Sets the postponed property value. The postponed property
     * @param int|null $value Value to set for the postponed property.
    */
    public function setPostponed(?int $value): void {
        $this->postponed = $value;
    }

    /**
     * Sets the relatedCode property value. The relatedCode property
     * @param int|null $value Value to set for the relatedCode property.
    */
    public function setRelatedCode(?int $value): void {
        $this->relatedCode = $value;
    }

    /**
     * Sets the relatedRate property value. The relatedRate property
     * @param float|null $value Value to set for the relatedRate property.
    */
    public function setRelatedRate(?float $value): void {
        $this->relatedRate = $value;
    }

    /**
     * Sets the taxCode property value. The taxCode property
     * @param string|null $value Value to set for the taxCode property.
    */
    public function setTaxCode(?string $value): void {
        $this->taxCode = $value;
    }

    /**
     * Sets the taxRate property value. The taxRate property
     * @param float|null $value Value to set for the taxRate property.
    */
    public function setTaxRate(?float $value): void {
        $this->taxRate = $value;
    }

    /**
     * Sets the ukNonCustomsUnionImportExport property value. The ukNonCustomsUnionImportExport property
     * @param int|null $value Value to set for the ukNonCustomsUnionImportExport property.
    */
    public function setUkNonCustomsUnionImportExport(?int $value): void {
        $this->ukNonCustomsUnionImportExport = $value;
    }

    /**
     * Sets the vatInclude property value. The vatInclude property
     * @param int|null $value Value to set for the vatInclude property.
    */
    public function setVatInclude(?int $value): void {
        $this->vatInclude = $value;
    }

}
