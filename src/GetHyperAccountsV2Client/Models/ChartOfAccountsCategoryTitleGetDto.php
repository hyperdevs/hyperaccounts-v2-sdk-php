<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class ChartOfAccountsCategoryTitleGetDto implements Parsable
{
    /**
     * @var ChartOfAccountsCategoryTitleAttributesRead|null $attributes The attributes property
    */
    private ?ChartOfAccountsCategoryTitleAttributesRead $attributes = null;

    /**
     * @var string|null $compositeKeyChartAndCategory The compositeKeyChartAndCategory property
    */
    private ?string $compositeKeyChartAndCategory = null;

    /**
     * @var ChartOfAccountsCategoryTitleIncluded|null $included The included property
    */
    private ?ChartOfAccountsCategoryTitleIncluded $included = null;

    /**
     * @var ResourceLinks|null $links The links property
    */
    private ?ResourceLinks $links = null;

    /**
     * @var ChartOfAccountsCategoryTitleRelationships|null $relationships The relationships property
    */
    private ?ChartOfAccountsCategoryTitleRelationships $relationships = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return ChartOfAccountsCategoryTitleGetDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): ChartOfAccountsCategoryTitleGetDto {
        return new ChartOfAccountsCategoryTitleGetDto();
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return ChartOfAccountsCategoryTitleAttributesRead|null
    */
    public function getAttributes(): ?ChartOfAccountsCategoryTitleAttributesRead {
        return $this->attributes;
    }

    /**
     * Gets the compositeKeyChartAndCategory property value. The compositeKeyChartAndCategory property
     * @return string|null
    */
    public function getCompositeKeyChartAndCategory(): ?string {
        return $this->compositeKeyChartAndCategory;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([ChartOfAccountsCategoryTitleAttributesRead::class, 'createFromDiscriminatorValue'])),
            'compositeKeyChartAndCategory' => fn(ParseNode $n) => $o->setCompositeKeyChartAndCategory($n->getStringValue()),
            'included' => fn(ParseNode $n) => $o->setIncluded($n->getObjectValue([ChartOfAccountsCategoryTitleIncluded::class, 'createFromDiscriminatorValue'])),
            'links' => fn(ParseNode $n) => $o->setLinks($n->getObjectValue([ResourceLinks::class, 'createFromDiscriminatorValue'])),
            'relationships' => fn(ParseNode $n) => $o->setRelationships($n->getObjectValue([ChartOfAccountsCategoryTitleRelationships::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the included property value. The included property
     * @return ChartOfAccountsCategoryTitleIncluded|null
    */
    public function getIncluded(): ?ChartOfAccountsCategoryTitleIncluded {
        return $this->included;
    }

    /**
     * Gets the links property value. The links property
     * @return ResourceLinks|null
    */
    public function getLinks(): ?ResourceLinks {
        return $this->links;
    }

    /**
     * Gets the relationships property value. The relationships property
     * @return ChartOfAccountsCategoryTitleRelationships|null
    */
    public function getRelationships(): ?ChartOfAccountsCategoryTitleRelationships {
        return $this->relationships;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('attributes', $this->getAttributes());
        $writer->writeObjectValue('included', $this->getIncluded());
        $writer->writeObjectValue('links', $this->getLinks());
        $writer->writeObjectValue('relationships', $this->getRelationships());
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param ChartOfAccountsCategoryTitleAttributesRead|null $value Value to set for the attributes property.
    */
    public function setAttributes(?ChartOfAccountsCategoryTitleAttributesRead $value): void {
        $this->attributes = $value;
    }

    /**
     * Sets the compositeKeyChartAndCategory property value. The compositeKeyChartAndCategory property
     * @param string|null $value Value to set for the compositeKeyChartAndCategory property.
    */
    public function setCompositeKeyChartAndCategory(?string $value): void {
        $this->compositeKeyChartAndCategory = $value;
    }

    /**
     * Sets the included property value. The included property
     * @param ChartOfAccountsCategoryTitleIncluded|null $value Value to set for the included property.
    */
    public function setIncluded(?ChartOfAccountsCategoryTitleIncluded $value): void {
        $this->included = $value;
    }

    /**
     * Sets the links property value. The links property
     * @param ResourceLinks|null $value Value to set for the links property.
    */
    public function setLinks(?ResourceLinks $value): void {
        $this->links = $value;
    }

    /**
     * Sets the relationships property value. The relationships property
     * @param ChartOfAccountsCategoryTitleRelationships|null $value Value to set for the relationships property.
    */
    public function setRelationships(?ChartOfAccountsCategoryTitleRelationships $value): void {
        $this->relationships = $value;
    }

}
