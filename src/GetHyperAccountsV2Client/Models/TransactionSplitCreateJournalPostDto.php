<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class TransactionSplitCreateJournalPostDto implements Parsable
{
    /**
     * @var TransactionSplitExceptTypeAttributesWrite|null $attributes The attributes property
    */
    private ?TransactionSplitExceptTypeAttributesWrite $attributes = null;

    /**
     * @var TransactionSplitRelationshipsWrite|null $relationships The relationships property
    */
    private ?TransactionSplitRelationshipsWrite $relationships = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return TransactionSplitCreateJournalPostDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): TransactionSplitCreateJournalPostDto {
        return new TransactionSplitCreateJournalPostDto();
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return TransactionSplitExceptTypeAttributesWrite|null
    */
    public function getAttributes(): ?TransactionSplitExceptTypeAttributesWrite {
        return $this->attributes;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([TransactionSplitExceptTypeAttributesWrite::class, 'createFromDiscriminatorValue'])),
            'relationships' => fn(ParseNode $n) => $o->setRelationships($n->getObjectValue([TransactionSplitRelationshipsWrite::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the relationships property value. The relationships property
     * @return TransactionSplitRelationshipsWrite|null
    */
    public function getRelationships(): ?TransactionSplitRelationshipsWrite {
        return $this->relationships;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('attributes', $this->getAttributes());
        $writer->writeObjectValue('relationships', $this->getRelationships());
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param TransactionSplitExceptTypeAttributesWrite|null $value Value to set for the attributes property.
    */
    public function setAttributes(?TransactionSplitExceptTypeAttributesWrite $value): void {
        $this->attributes = $value;
    }

    /**
     * Sets the relationships property value. The relationships property
     * @param TransactionSplitRelationshipsWrite|null $value Value to set for the relationships property.
    */
    public function setRelationships(?TransactionSplitRelationshipsWrite $value): void {
        $this->relationships = $value;
    }

}
