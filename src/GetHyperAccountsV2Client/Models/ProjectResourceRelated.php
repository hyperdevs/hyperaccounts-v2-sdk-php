<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class ProjectResourceRelated implements Parsable
{
    /**
     * @var int|null $resourceId The resourceId property
    */
    private ?int $resourceId = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return ProjectResourceRelated
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): ProjectResourceRelated {
        return new ProjectResourceRelated();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'resourceId' => fn(ParseNode $n) => $o->setResourceId($n->getIntegerValue()),
        ];
    }

    /**
     * Gets the resourceId property value. The resourceId property
     * @return int|null
    */
    public function getResourceId(): ?int {
        return $this->resourceId;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeIntegerValue('resourceId', $this->getResourceId());
    }

    /**
     * Sets the resourceId property value. The resourceId property
     * @param int|null $value Value to set for the resourceId property.
    */
    public function setResourceId(?int $value): void {
        $this->resourceId = $value;
    }

}
