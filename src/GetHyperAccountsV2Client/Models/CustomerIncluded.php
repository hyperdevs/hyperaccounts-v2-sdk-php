<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class CustomerIncluded implements Parsable
{
    /**
     * @var CustomerAddressCollection|null $addresses The addresses property
    */
    private ?CustomerAddressCollection $addresses = null;

    /**
     * @var SalesInvoiceCollection|null $invoices The invoices property
    */
    private ?SalesInvoiceCollection $invoices = null;

    /**
     * @var ProjectCollection|null $projects The projects property
    */
    private ?ProjectCollection $projects = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return CustomerIncluded
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): CustomerIncluded {
        return new CustomerIncluded();
    }

    /**
     * Gets the addresses property value. The addresses property
     * @return CustomerAddressCollection|null
    */
    public function getAddresses(): ?CustomerAddressCollection {
        return $this->addresses;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'addresses' => fn(ParseNode $n) => $o->setAddresses($n->getObjectValue([CustomerAddressCollection::class, 'createFromDiscriminatorValue'])),
            'invoices' => fn(ParseNode $n) => $o->setInvoices($n->getObjectValue([SalesInvoiceCollection::class, 'createFromDiscriminatorValue'])),
            'projects' => fn(ParseNode $n) => $o->setProjects($n->getObjectValue([ProjectCollection::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the invoices property value. The invoices property
     * @return SalesInvoiceCollection|null
    */
    public function getInvoices(): ?SalesInvoiceCollection {
        return $this->invoices;
    }

    /**
     * Gets the projects property value. The projects property
     * @return ProjectCollection|null
    */
    public function getProjects(): ?ProjectCollection {
        return $this->projects;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('addresses', $this->getAddresses());
        $writer->writeObjectValue('invoices', $this->getInvoices());
        $writer->writeObjectValue('projects', $this->getProjects());
    }

    /**
     * Sets the addresses property value. The addresses property
     * @param CustomerAddressCollection|null $value Value to set for the addresses property.
    */
    public function setAddresses(?CustomerAddressCollection $value): void {
        $this->addresses = $value;
    }

    /**
     * Sets the invoices property value. The invoices property
     * @param SalesInvoiceCollection|null $value Value to set for the invoices property.
    */
    public function setInvoices(?SalesInvoiceCollection $value): void {
        $this->invoices = $value;
    }

    /**
     * Sets the projects property value. The projects property
     * @param ProjectCollection|null $value Value to set for the projects property.
    */
    public function setProjects(?ProjectCollection $value): void {
        $this->projects = $value;
    }

}
