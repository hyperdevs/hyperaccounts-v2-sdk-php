<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class ProjectIncluded implements Parsable
{
    /**
     * @var CustomerGetDto|null $customer The customer property
    */
    private ?CustomerGetDto $customer = null;

    /**
     * @var ProjectBudgetCollection|null $projectBudgets The projectBudgets property
    */
    private ?ProjectBudgetCollection $projectBudgets = null;

    /**
     * @var ProjectTranCollection|null $projectTrans The projectTrans property
    */
    private ?ProjectTranCollection $projectTrans = null;

    /**
     * @var PurchaseOrderCollection|null $purchaseOrders The purchaseOrders property
    */
    private ?PurchaseOrderCollection $purchaseOrders = null;

    /**
     * @var SalesInvoiceItemCollection|null $salesInvoiceItems The salesInvoiceItems property
    */
    private ?SalesInvoiceItemCollection $salesInvoiceItems = null;

    /**
     * @var SalesInvoiceCollection|null $salesInvoices The salesInvoices property
    */
    private ?SalesInvoiceCollection $salesInvoices = null;

    /**
     * @var SalesOrderItemCollection|null $salesOrderItems The salesOrderItems property
    */
    private ?SalesOrderItemCollection $salesOrderItems = null;

    /**
     * @var SalesOrderCollection|null $salesOrders The salesOrders property
    */
    private ?SalesOrderCollection $salesOrders = null;

    /**
     * @var ProjectStatusGetDto|null $status The status property
    */
    private ?ProjectStatusGetDto $status = null;

    /**
     * @var TransactionSplitCollection|null $transactionSplits The transactionSplits property
    */
    private ?TransactionSplitCollection $transactionSplits = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return ProjectIncluded
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): ProjectIncluded {
        return new ProjectIncluded();
    }

    /**
     * Gets the customer property value. The customer property
     * @return CustomerGetDto|null
    */
    public function getCustomer(): ?CustomerGetDto {
        return $this->customer;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'customer' => fn(ParseNode $n) => $o->setCustomer($n->getObjectValue([CustomerGetDto::class, 'createFromDiscriminatorValue'])),
            'projectBudgets' => fn(ParseNode $n) => $o->setProjectBudgets($n->getObjectValue([ProjectBudgetCollection::class, 'createFromDiscriminatorValue'])),
            'projectTrans' => fn(ParseNode $n) => $o->setProjectTrans($n->getObjectValue([ProjectTranCollection::class, 'createFromDiscriminatorValue'])),
            'purchaseOrders' => fn(ParseNode $n) => $o->setPurchaseOrders($n->getObjectValue([PurchaseOrderCollection::class, 'createFromDiscriminatorValue'])),
            'salesInvoiceItems' => fn(ParseNode $n) => $o->setSalesInvoiceItems($n->getObjectValue([SalesInvoiceItemCollection::class, 'createFromDiscriminatorValue'])),
            'salesInvoices' => fn(ParseNode $n) => $o->setSalesInvoices($n->getObjectValue([SalesInvoiceCollection::class, 'createFromDiscriminatorValue'])),
            'salesOrderItems' => fn(ParseNode $n) => $o->setSalesOrderItems($n->getObjectValue([SalesOrderItemCollection::class, 'createFromDiscriminatorValue'])),
            'salesOrders' => fn(ParseNode $n) => $o->setSalesOrders($n->getObjectValue([SalesOrderCollection::class, 'createFromDiscriminatorValue'])),
            'status' => fn(ParseNode $n) => $o->setStatus($n->getObjectValue([ProjectStatusGetDto::class, 'createFromDiscriminatorValue'])),
            'transactionSplits' => fn(ParseNode $n) => $o->setTransactionSplits($n->getObjectValue([TransactionSplitCollection::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the projectBudgets property value. The projectBudgets property
     * @return ProjectBudgetCollection|null
    */
    public function getProjectBudgets(): ?ProjectBudgetCollection {
        return $this->projectBudgets;
    }

    /**
     * Gets the projectTrans property value. The projectTrans property
     * @return ProjectTranCollection|null
    */
    public function getProjectTrans(): ?ProjectTranCollection {
        return $this->projectTrans;
    }

    /**
     * Gets the purchaseOrders property value. The purchaseOrders property
     * @return PurchaseOrderCollection|null
    */
    public function getPurchaseOrders(): ?PurchaseOrderCollection {
        return $this->purchaseOrders;
    }

    /**
     * Gets the salesInvoiceItems property value. The salesInvoiceItems property
     * @return SalesInvoiceItemCollection|null
    */
    public function getSalesInvoiceItems(): ?SalesInvoiceItemCollection {
        return $this->salesInvoiceItems;
    }

    /**
     * Gets the salesInvoices property value. The salesInvoices property
     * @return SalesInvoiceCollection|null
    */
    public function getSalesInvoices(): ?SalesInvoiceCollection {
        return $this->salesInvoices;
    }

    /**
     * Gets the salesOrderItems property value. The salesOrderItems property
     * @return SalesOrderItemCollection|null
    */
    public function getSalesOrderItems(): ?SalesOrderItemCollection {
        return $this->salesOrderItems;
    }

    /**
     * Gets the salesOrders property value. The salesOrders property
     * @return SalesOrderCollection|null
    */
    public function getSalesOrders(): ?SalesOrderCollection {
        return $this->salesOrders;
    }

    /**
     * Gets the status property value. The status property
     * @return ProjectStatusGetDto|null
    */
    public function getStatus(): ?ProjectStatusGetDto {
        return $this->status;
    }

    /**
     * Gets the transactionSplits property value. The transactionSplits property
     * @return TransactionSplitCollection|null
    */
    public function getTransactionSplits(): ?TransactionSplitCollection {
        return $this->transactionSplits;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('customer', $this->getCustomer());
        $writer->writeObjectValue('projectBudgets', $this->getProjectBudgets());
        $writer->writeObjectValue('projectTrans', $this->getProjectTrans());
        $writer->writeObjectValue('purchaseOrders', $this->getPurchaseOrders());
        $writer->writeObjectValue('salesInvoiceItems', $this->getSalesInvoiceItems());
        $writer->writeObjectValue('salesInvoices', $this->getSalesInvoices());
        $writer->writeObjectValue('salesOrderItems', $this->getSalesOrderItems());
        $writer->writeObjectValue('salesOrders', $this->getSalesOrders());
        $writer->writeObjectValue('status', $this->getStatus());
        $writer->writeObjectValue('transactionSplits', $this->getTransactionSplits());
    }

    /**
     * Sets the customer property value. The customer property
     * @param CustomerGetDto|null $value Value to set for the customer property.
    */
    public function setCustomer(?CustomerGetDto $value): void {
        $this->customer = $value;
    }

    /**
     * Sets the projectBudgets property value. The projectBudgets property
     * @param ProjectBudgetCollection|null $value Value to set for the projectBudgets property.
    */
    public function setProjectBudgets(?ProjectBudgetCollection $value): void {
        $this->projectBudgets = $value;
    }

    /**
     * Sets the projectTrans property value. The projectTrans property
     * @param ProjectTranCollection|null $value Value to set for the projectTrans property.
    */
    public function setProjectTrans(?ProjectTranCollection $value): void {
        $this->projectTrans = $value;
    }

    /**
     * Sets the purchaseOrders property value. The purchaseOrders property
     * @param PurchaseOrderCollection|null $value Value to set for the purchaseOrders property.
    */
    public function setPurchaseOrders(?PurchaseOrderCollection $value): void {
        $this->purchaseOrders = $value;
    }

    /**
     * Sets the salesInvoiceItems property value. The salesInvoiceItems property
     * @param SalesInvoiceItemCollection|null $value Value to set for the salesInvoiceItems property.
    */
    public function setSalesInvoiceItems(?SalesInvoiceItemCollection $value): void {
        $this->salesInvoiceItems = $value;
    }

    /**
     * Sets the salesInvoices property value. The salesInvoices property
     * @param SalesInvoiceCollection|null $value Value to set for the salesInvoices property.
    */
    public function setSalesInvoices(?SalesInvoiceCollection $value): void {
        $this->salesInvoices = $value;
    }

    /**
     * Sets the salesOrderItems property value. The salesOrderItems property
     * @param SalesOrderItemCollection|null $value Value to set for the salesOrderItems property.
    */
    public function setSalesOrderItems(?SalesOrderItemCollection $value): void {
        $this->salesOrderItems = $value;
    }

    /**
     * Sets the salesOrders property value. The salesOrders property
     * @param SalesOrderCollection|null $value Value to set for the salesOrders property.
    */
    public function setSalesOrders(?SalesOrderCollection $value): void {
        $this->salesOrders = $value;
    }

    /**
     * Sets the status property value. The status property
     * @param ProjectStatusGetDto|null $value Value to set for the status property.
    */
    public function setStatus(?ProjectStatusGetDto $value): void {
        $this->status = $value;
    }

    /**
     * Sets the transactionSplits property value. The transactionSplits property
     * @param TransactionSplitCollection|null $value Value to set for the transactionSplits property.
    */
    public function setTransactionSplits(?TransactionSplitCollection $value): void {
        $this->transactionSplits = $value;
    }

}
