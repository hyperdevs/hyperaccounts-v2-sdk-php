<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class NominalRelationships implements Parsable
{
    /**
     * @var PurchaseOrderItemRelatedListRelationship|null $purchaseOrderItems The purchaseOrderItems property
    */
    private ?PurchaseOrderItemRelatedListRelationship $purchaseOrderItems = null;

    /**
     * @var SalesInvoiceItemRelatedListRelationship|null $salesInvoiceItems The salesInvoiceItems property
    */
    private ?SalesInvoiceItemRelatedListRelationship $salesInvoiceItems = null;

    /**
     * @var SalesOrderItemRelatedListRelationship|null $salesOrderItems The salesOrderItems property
    */
    private ?SalesOrderItemRelatedListRelationship $salesOrderItems = null;

    /**
     * @var StockRelatedListRelationship|null $stocks The stocks property
    */
    private ?StockRelatedListRelationship $stocks = null;

    /**
     * @var TransactionSplitRelatedListRelationship|null $transactionSplits The transactionSplits property
    */
    private ?TransactionSplitRelatedListRelationship $transactionSplits = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return NominalRelationships
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): NominalRelationships {
        return new NominalRelationships();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'purchaseOrderItems' => fn(ParseNode $n) => $o->setPurchaseOrderItems($n->getObjectValue([PurchaseOrderItemRelatedListRelationship::class, 'createFromDiscriminatorValue'])),
            'salesInvoiceItems' => fn(ParseNode $n) => $o->setSalesInvoiceItems($n->getObjectValue([SalesInvoiceItemRelatedListRelationship::class, 'createFromDiscriminatorValue'])),
            'salesOrderItems' => fn(ParseNode $n) => $o->setSalesOrderItems($n->getObjectValue([SalesOrderItemRelatedListRelationship::class, 'createFromDiscriminatorValue'])),
            'stocks' => fn(ParseNode $n) => $o->setStocks($n->getObjectValue([StockRelatedListRelationship::class, 'createFromDiscriminatorValue'])),
            'transactionSplits' => fn(ParseNode $n) => $o->setTransactionSplits($n->getObjectValue([TransactionSplitRelatedListRelationship::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the purchaseOrderItems property value. The purchaseOrderItems property
     * @return PurchaseOrderItemRelatedListRelationship|null
    */
    public function getPurchaseOrderItems(): ?PurchaseOrderItemRelatedListRelationship {
        return $this->purchaseOrderItems;
    }

    /**
     * Gets the salesInvoiceItems property value. The salesInvoiceItems property
     * @return SalesInvoiceItemRelatedListRelationship|null
    */
    public function getSalesInvoiceItems(): ?SalesInvoiceItemRelatedListRelationship {
        return $this->salesInvoiceItems;
    }

    /**
     * Gets the salesOrderItems property value. The salesOrderItems property
     * @return SalesOrderItemRelatedListRelationship|null
    */
    public function getSalesOrderItems(): ?SalesOrderItemRelatedListRelationship {
        return $this->salesOrderItems;
    }

    /**
     * Gets the stocks property value. The stocks property
     * @return StockRelatedListRelationship|null
    */
    public function getStocks(): ?StockRelatedListRelationship {
        return $this->stocks;
    }

    /**
     * Gets the transactionSplits property value. The transactionSplits property
     * @return TransactionSplitRelatedListRelationship|null
    */
    public function getTransactionSplits(): ?TransactionSplitRelatedListRelationship {
        return $this->transactionSplits;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('purchaseOrderItems', $this->getPurchaseOrderItems());
        $writer->writeObjectValue('salesInvoiceItems', $this->getSalesInvoiceItems());
        $writer->writeObjectValue('salesOrderItems', $this->getSalesOrderItems());
        $writer->writeObjectValue('stocks', $this->getStocks());
        $writer->writeObjectValue('transactionSplits', $this->getTransactionSplits());
    }

    /**
     * Sets the purchaseOrderItems property value. The purchaseOrderItems property
     * @param PurchaseOrderItemRelatedListRelationship|null $value Value to set for the purchaseOrderItems property.
    */
    public function setPurchaseOrderItems(?PurchaseOrderItemRelatedListRelationship $value): void {
        $this->purchaseOrderItems = $value;
    }

    /**
     * Sets the salesInvoiceItems property value. The salesInvoiceItems property
     * @param SalesInvoiceItemRelatedListRelationship|null $value Value to set for the salesInvoiceItems property.
    */
    public function setSalesInvoiceItems(?SalesInvoiceItemRelatedListRelationship $value): void {
        $this->salesInvoiceItems = $value;
    }

    /**
     * Sets the salesOrderItems property value. The salesOrderItems property
     * @param SalesOrderItemRelatedListRelationship|null $value Value to set for the salesOrderItems property.
    */
    public function setSalesOrderItems(?SalesOrderItemRelatedListRelationship $value): void {
        $this->salesOrderItems = $value;
    }

    /**
     * Sets the stocks property value. The stocks property
     * @param StockRelatedListRelationship|null $value Value to set for the stocks property.
    */
    public function setStocks(?StockRelatedListRelationship $value): void {
        $this->stocks = $value;
    }

    /**
     * Sets the transactionSplits property value. The transactionSplits property
     * @param TransactionSplitRelatedListRelationship|null $value Value to set for the transactionSplits property.
    */
    public function setTransactionSplits(?TransactionSplitRelatedListRelationship $value): void {
        $this->transactionSplits = $value;
    }

}
