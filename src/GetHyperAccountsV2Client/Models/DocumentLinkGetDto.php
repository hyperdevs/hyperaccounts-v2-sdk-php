<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class DocumentLinkGetDto implements Parsable
{
    /**
     * @var DocumentLinkAttributesRead|null $attributes The attributes property
    */
    private ?DocumentLinkAttributesRead $attributes = null;

    /**
     * @var ResourceLinks|null $links The links property
    */
    private ?ResourceLinks $links = null;

    /**
     * @var int|null $transactionId The transactionId property
    */
    private ?int $transactionId = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return DocumentLinkGetDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): DocumentLinkGetDto {
        return new DocumentLinkGetDto();
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return DocumentLinkAttributesRead|null
    */
    public function getAttributes(): ?DocumentLinkAttributesRead {
        return $this->attributes;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([DocumentLinkAttributesRead::class, 'createFromDiscriminatorValue'])),
            'links' => fn(ParseNode $n) => $o->setLinks($n->getObjectValue([ResourceLinks::class, 'createFromDiscriminatorValue'])),
            'transactionId' => fn(ParseNode $n) => $o->setTransactionId($n->getIntegerValue()),
        ];
    }

    /**
     * Gets the links property value. The links property
     * @return ResourceLinks|null
    */
    public function getLinks(): ?ResourceLinks {
        return $this->links;
    }

    /**
     * Gets the transactionId property value. The transactionId property
     * @return int|null
    */
    public function getTransactionId(): ?int {
        return $this->transactionId;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('attributes', $this->getAttributes());
        $writer->writeObjectValue('links', $this->getLinks());
        $writer->writeIntegerValue('transactionId', $this->getTransactionId());
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param DocumentLinkAttributesRead|null $value Value to set for the attributes property.
    */
    public function setAttributes(?DocumentLinkAttributesRead $value): void {
        $this->attributes = $value;
    }

    /**
     * Sets the links property value. The links property
     * @param ResourceLinks|null $value Value to set for the links property.
    */
    public function setLinks(?ResourceLinks $value): void {
        $this->links = $value;
    }

    /**
     * Sets the transactionId property value. The transactionId property
     * @param int|null $value Value to set for the transactionId property.
    */
    public function setTransactionId(?int $value): void {
        $this->transactionId = $value;
    }

}
