<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class CustomerRelationshipsRead implements Parsable
{
    /**
     * @var CustomerAddressRelatedListRelationship|null $addresses The addresses property
    */
    private ?CustomerAddressRelatedListRelationship $addresses = null;

    /**
     * @var SalesInvoiceRelatedListRelationship|null $invoices The invoices property
    */
    private ?SalesInvoiceRelatedListRelationship $invoices = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return CustomerRelationshipsRead
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): CustomerRelationshipsRead {
        return new CustomerRelationshipsRead();
    }

    /**
     * Gets the addresses property value. The addresses property
     * @return CustomerAddressRelatedListRelationship|null
    */
    public function getAddresses(): ?CustomerAddressRelatedListRelationship {
        return $this->addresses;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'addresses' => fn(ParseNode $n) => $o->setAddresses($n->getObjectValue([CustomerAddressRelatedListRelationship::class, 'createFromDiscriminatorValue'])),
            'invoices' => fn(ParseNode $n) => $o->setInvoices($n->getObjectValue([SalesInvoiceRelatedListRelationship::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the invoices property value. The invoices property
     * @return SalesInvoiceRelatedListRelationship|null
    */
    public function getInvoices(): ?SalesInvoiceRelatedListRelationship {
        return $this->invoices;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('addresses', $this->getAddresses());
        $writer->writeObjectValue('invoices', $this->getInvoices());
    }

    /**
     * Sets the addresses property value. The addresses property
     * @param CustomerAddressRelatedListRelationship|null $value Value to set for the addresses property.
    */
    public function setAddresses(?CustomerAddressRelatedListRelationship $value): void {
        $this->addresses = $value;
    }

    /**
     * Sets the invoices property value. The invoices property
     * @param SalesInvoiceRelatedListRelationship|null $value Value to set for the invoices property.
    */
    public function setInvoices(?SalesInvoiceRelatedListRelationship $value): void {
        $this->invoices = $value;
    }

}
