<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class ProjectGetDto implements Parsable
{
    /**
     * @var ProjectAttributesRead|null $attributes The attributes property
    */
    private ?ProjectAttributesRead $attributes = null;

    /**
     * @var ProjectIncluded|null $included The included property
    */
    private ?ProjectIncluded $included = null;

    /**
     * @var ResourceLinks|null $links The links property
    */
    private ?ResourceLinks $links = null;

    /**
     * @var int|null $projectId The projectId property
    */
    private ?int $projectId = null;

    /**
     * @var ProjectRelationships|null $relationships The relationships property
    */
    private ?ProjectRelationships $relationships = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return ProjectGetDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): ProjectGetDto {
        return new ProjectGetDto();
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return ProjectAttributesRead|null
    */
    public function getAttributes(): ?ProjectAttributesRead {
        return $this->attributes;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([ProjectAttributesRead::class, 'createFromDiscriminatorValue'])),
            'included' => fn(ParseNode $n) => $o->setIncluded($n->getObjectValue([ProjectIncluded::class, 'createFromDiscriminatorValue'])),
            'links' => fn(ParseNode $n) => $o->setLinks($n->getObjectValue([ResourceLinks::class, 'createFromDiscriminatorValue'])),
            'projectId' => fn(ParseNode $n) => $o->setProjectId($n->getIntegerValue()),
            'relationships' => fn(ParseNode $n) => $o->setRelationships($n->getObjectValue([ProjectRelationships::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the included property value. The included property
     * @return ProjectIncluded|null
    */
    public function getIncluded(): ?ProjectIncluded {
        return $this->included;
    }

    /**
     * Gets the links property value. The links property
     * @return ResourceLinks|null
    */
    public function getLinks(): ?ResourceLinks {
        return $this->links;
    }

    /**
     * Gets the projectId property value. The projectId property
     * @return int|null
    */
    public function getProjectId(): ?int {
        return $this->projectId;
    }

    /**
     * Gets the relationships property value. The relationships property
     * @return ProjectRelationships|null
    */
    public function getRelationships(): ?ProjectRelationships {
        return $this->relationships;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('attributes', $this->getAttributes());
        $writer->writeObjectValue('included', $this->getIncluded());
        $writer->writeObjectValue('links', $this->getLinks());
        $writer->writeIntegerValue('projectId', $this->getProjectId());
        $writer->writeObjectValue('relationships', $this->getRelationships());
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param ProjectAttributesRead|null $value Value to set for the attributes property.
    */
    public function setAttributes(?ProjectAttributesRead $value): void {
        $this->attributes = $value;
    }

    /**
     * Sets the included property value. The included property
     * @param ProjectIncluded|null $value Value to set for the included property.
    */
    public function setIncluded(?ProjectIncluded $value): void {
        $this->included = $value;
    }

    /**
     * Sets the links property value. The links property
     * @param ResourceLinks|null $value Value to set for the links property.
    */
    public function setLinks(?ResourceLinks $value): void {
        $this->links = $value;
    }

    /**
     * Sets the projectId property value. The projectId property
     * @param int|null $value Value to set for the projectId property.
    */
    public function setProjectId(?int $value): void {
        $this->projectId = $value;
    }

    /**
     * Sets the relationships property value. The relationships property
     * @param ProjectRelationships|null $value Value to set for the relationships property.
    */
    public function setRelationships(?ProjectRelationships $value): void {
        $this->relationships = $value;
    }

}
