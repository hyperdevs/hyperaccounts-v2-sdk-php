<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Enum;

class StockTransactionAttributesWrite_type extends Enum {
    public const ADJUSTMENT_IN = 'AdjustmentIn';
    public const ADJUSTMENT_OUT = 'AdjustmentOut';
    public const GOODS_IN = 'GoodsIn';
    public const GOODS_OUT = 'GoodsOut';
    public const MOVEMENT_IN = 'MovementIn';
    public const MOVEMENT_OUT = 'MovementOut';
    public const GOODS_RETURNED = 'GoodsReturned';
    public const DAMAGES_IN = 'DamagesIn';
    public const DAMAGES_OUT = 'DamagesOut';
    public const WRITE_OFF = 'WriteOff';
}
