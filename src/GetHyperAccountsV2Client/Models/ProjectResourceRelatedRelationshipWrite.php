<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class ProjectResourceRelatedRelationshipWrite implements Parsable
{
    /**
     * @var ProjectResourceRelated|null $data The data property
    */
    private ?ProjectResourceRelated $data = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return ProjectResourceRelatedRelationshipWrite
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): ProjectResourceRelatedRelationshipWrite {
        return new ProjectResourceRelatedRelationshipWrite();
    }

    /**
     * Gets the data property value. The data property
     * @return ProjectResourceRelated|null
    */
    public function getData(): ?ProjectResourceRelated {
        return $this->data;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'data' => fn(ParseNode $n) => $o->setData($n->getObjectValue([ProjectResourceRelated::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('data', $this->getData());
    }

    /**
     * Sets the data property value. The data property
     * @param ProjectResourceRelated|null $value Value to set for the data property.
    */
    public function setData(?ProjectResourceRelated $value): void {
        $this->data = $value;
    }

}
