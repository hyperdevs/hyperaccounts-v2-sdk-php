<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class StockTransactionRelationships implements Parsable
{
    /**
     * @var StockRelatedRelationship|null $stock The stock property
    */
    private ?StockRelatedRelationship $stock = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return StockTransactionRelationships
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): StockTransactionRelationships {
        return new StockTransactionRelationships();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'stock' => fn(ParseNode $n) => $o->setStock($n->getObjectValue([StockRelatedRelationship::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the stock property value. The stock property
     * @return StockRelatedRelationship|null
    */
    public function getStock(): ?StockRelatedRelationship {
        return $this->stock;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('stock', $this->getStock());
    }

    /**
     * Sets the stock property value. The stock property
     * @param StockRelatedRelationship|null $value Value to set for the stock property.
    */
    public function setStock(?StockRelatedRelationship $value): void {
        $this->stock = $value;
    }

}
