<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class ProjectCostCodeRelationships implements Parsable
{
    /**
     * @var ProjectBudgetRelatedListRelationship|null $projectBudgets The projectBudgets property
    */
    private ?ProjectBudgetRelatedListRelationship $projectBudgets = null;

    /**
     * @var TransactionSplitRelatedListRelationship|null $splits The splits property
    */
    private ?TransactionSplitRelatedListRelationship $splits = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return ProjectCostCodeRelationships
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): ProjectCostCodeRelationships {
        return new ProjectCostCodeRelationships();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'projectBudgets' => fn(ParseNode $n) => $o->setProjectBudgets($n->getObjectValue([ProjectBudgetRelatedListRelationship::class, 'createFromDiscriminatorValue'])),
            'splits' => fn(ParseNode $n) => $o->setSplits($n->getObjectValue([TransactionSplitRelatedListRelationship::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the projectBudgets property value. The projectBudgets property
     * @return ProjectBudgetRelatedListRelationship|null
    */
    public function getProjectBudgets(): ?ProjectBudgetRelatedListRelationship {
        return $this->projectBudgets;
    }

    /**
     * Gets the splits property value. The splits property
     * @return TransactionSplitRelatedListRelationship|null
    */
    public function getSplits(): ?TransactionSplitRelatedListRelationship {
        return $this->splits;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('projectBudgets', $this->getProjectBudgets());
        $writer->writeObjectValue('splits', $this->getSplits());
    }

    /**
     * Sets the projectBudgets property value. The projectBudgets property
     * @param ProjectBudgetRelatedListRelationship|null $value Value to set for the projectBudgets property.
    */
    public function setProjectBudgets(?ProjectBudgetRelatedListRelationship $value): void {
        $this->projectBudgets = $value;
    }

    /**
     * Sets the splits property value. The splits property
     * @param TransactionSplitRelatedListRelationship|null $value Value to set for the splits property.
    */
    public function setSplits(?TransactionSplitRelatedListRelationship $value): void {
        $this->splits = $value;
    }

}
