<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class TransactionSplitPostAllocateRelationshipsWrite implements Parsable
{
    /**
     * @var TransactionSplitRelatedRequiredRelationshipWrite|null $allocateTo The allocateTo property
    */
    private ?TransactionSplitRelatedRequiredRelationshipWrite $allocateTo = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return TransactionSplitPostAllocateRelationshipsWrite
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): TransactionSplitPostAllocateRelationshipsWrite {
        return new TransactionSplitPostAllocateRelationshipsWrite();
    }

    /**
     * Gets the allocateTo property value. The allocateTo property
     * @return TransactionSplitRelatedRequiredRelationshipWrite|null
    */
    public function getAllocateTo(): ?TransactionSplitRelatedRequiredRelationshipWrite {
        return $this->allocateTo;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'allocateTo' => fn(ParseNode $n) => $o->setAllocateTo($n->getObjectValue([TransactionSplitRelatedRequiredRelationshipWrite::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('allocateTo', $this->getAllocateTo());
    }

    /**
     * Sets the allocateTo property value. The allocateTo property
     * @param TransactionSplitRelatedRequiredRelationshipWrite|null $value Value to set for the allocateTo property.
    */
    public function setAllocateTo(?TransactionSplitRelatedRequiredRelationshipWrite $value): void {
        $this->allocateTo = $value;
    }

}
