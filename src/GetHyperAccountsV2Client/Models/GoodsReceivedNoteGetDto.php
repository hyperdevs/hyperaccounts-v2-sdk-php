<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class GoodsReceivedNoteGetDto implements Parsable
{
    /**
     * @var GoodsReceivedNoteAttributesRead|null $attributes The attributes property
    */
    private ?GoodsReceivedNoteAttributesRead $attributes = null;

    /**
     * @var string|null $compositeKeyGrnNumberAndItemNumber The compositeKeyGrnNumberAndItemNumber property
    */
    private ?string $compositeKeyGrnNumberAndItemNumber = null;

    /**
     * @var GoodsReceivedNoteIncluded|null $included The included property
    */
    private ?GoodsReceivedNoteIncluded $included = null;

    /**
     * @var ResourceLinks|null $links The links property
    */
    private ?ResourceLinks $links = null;

    /**
     * @var GoodsReceivedNoteRelationships|null $relationships The relationships property
    */
    private ?GoodsReceivedNoteRelationships $relationships = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return GoodsReceivedNoteGetDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): GoodsReceivedNoteGetDto {
        return new GoodsReceivedNoteGetDto();
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return GoodsReceivedNoteAttributesRead|null
    */
    public function getAttributes(): ?GoodsReceivedNoteAttributesRead {
        return $this->attributes;
    }

    /**
     * Gets the compositeKeyGrnNumberAndItemNumber property value. The compositeKeyGrnNumberAndItemNumber property
     * @return string|null
    */
    public function getCompositeKeyGrnNumberAndItemNumber(): ?string {
        return $this->compositeKeyGrnNumberAndItemNumber;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([GoodsReceivedNoteAttributesRead::class, 'createFromDiscriminatorValue'])),
            'compositeKeyGrnNumberAndItemNumber' => fn(ParseNode $n) => $o->setCompositeKeyGrnNumberAndItemNumber($n->getStringValue()),
            'included' => fn(ParseNode $n) => $o->setIncluded($n->getObjectValue([GoodsReceivedNoteIncluded::class, 'createFromDiscriminatorValue'])),
            'links' => fn(ParseNode $n) => $o->setLinks($n->getObjectValue([ResourceLinks::class, 'createFromDiscriminatorValue'])),
            'relationships' => fn(ParseNode $n) => $o->setRelationships($n->getObjectValue([GoodsReceivedNoteRelationships::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the included property value. The included property
     * @return GoodsReceivedNoteIncluded|null
    */
    public function getIncluded(): ?GoodsReceivedNoteIncluded {
        return $this->included;
    }

    /**
     * Gets the links property value. The links property
     * @return ResourceLinks|null
    */
    public function getLinks(): ?ResourceLinks {
        return $this->links;
    }

    /**
     * Gets the relationships property value. The relationships property
     * @return GoodsReceivedNoteRelationships|null
    */
    public function getRelationships(): ?GoodsReceivedNoteRelationships {
        return $this->relationships;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('attributes', $this->getAttributes());
        $writer->writeObjectValue('included', $this->getIncluded());
        $writer->writeObjectValue('links', $this->getLinks());
        $writer->writeObjectValue('relationships', $this->getRelationships());
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param GoodsReceivedNoteAttributesRead|null $value Value to set for the attributes property.
    */
    public function setAttributes(?GoodsReceivedNoteAttributesRead $value): void {
        $this->attributes = $value;
    }

    /**
     * Sets the compositeKeyGrnNumberAndItemNumber property value. The compositeKeyGrnNumberAndItemNumber property
     * @param string|null $value Value to set for the compositeKeyGrnNumberAndItemNumber property.
    */
    public function setCompositeKeyGrnNumberAndItemNumber(?string $value): void {
        $this->compositeKeyGrnNumberAndItemNumber = $value;
    }

    /**
     * Sets the included property value. The included property
     * @param GoodsReceivedNoteIncluded|null $value Value to set for the included property.
    */
    public function setIncluded(?GoodsReceivedNoteIncluded $value): void {
        $this->included = $value;
    }

    /**
     * Sets the links property value. The links property
     * @param ResourceLinks|null $value Value to set for the links property.
    */
    public function setLinks(?ResourceLinks $value): void {
        $this->links = $value;
    }

    /**
     * Sets the relationships property value. The relationships property
     * @param GoodsReceivedNoteRelationships|null $value Value to set for the relationships property.
    */
    public function setRelationships(?GoodsReceivedNoteRelationships $value): void {
        $this->relationships = $value;
    }

}
