<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class GoodsReceivedNoteIncluded implements Parsable
{
    /**
     * @var PurchaseOrderGetDto|null $purchaseOrder The purchaseOrder property
    */
    private ?PurchaseOrderGetDto $purchaseOrder = null;

    /**
     * @var StockGetDto|null $stock The stock property
    */
    private ?StockGetDto $stock = null;

    /**
     * @var SupplierGetDto|null $supplier The supplier property
    */
    private ?SupplierGetDto $supplier = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return GoodsReceivedNoteIncluded
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): GoodsReceivedNoteIncluded {
        return new GoodsReceivedNoteIncluded();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'purchaseOrder' => fn(ParseNode $n) => $o->setPurchaseOrder($n->getObjectValue([PurchaseOrderGetDto::class, 'createFromDiscriminatorValue'])),
            'stock' => fn(ParseNode $n) => $o->setStock($n->getObjectValue([StockGetDto::class, 'createFromDiscriminatorValue'])),
            'supplier' => fn(ParseNode $n) => $o->setSupplier($n->getObjectValue([SupplierGetDto::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the purchaseOrder property value. The purchaseOrder property
     * @return PurchaseOrderGetDto|null
    */
    public function getPurchaseOrder(): ?PurchaseOrderGetDto {
        return $this->purchaseOrder;
    }

    /**
     * Gets the stock property value. The stock property
     * @return StockGetDto|null
    */
    public function getStock(): ?StockGetDto {
        return $this->stock;
    }

    /**
     * Gets the supplier property value. The supplier property
     * @return SupplierGetDto|null
    */
    public function getSupplier(): ?SupplierGetDto {
        return $this->supplier;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('purchaseOrder', $this->getPurchaseOrder());
        $writer->writeObjectValue('stock', $this->getStock());
        $writer->writeObjectValue('supplier', $this->getSupplier());
    }

    /**
     * Sets the purchaseOrder property value. The purchaseOrder property
     * @param PurchaseOrderGetDto|null $value Value to set for the purchaseOrder property.
    */
    public function setPurchaseOrder(?PurchaseOrderGetDto $value): void {
        $this->purchaseOrder = $value;
    }

    /**
     * Sets the stock property value. The stock property
     * @param StockGetDto|null $value Value to set for the stock property.
    */
    public function setStock(?StockGetDto $value): void {
        $this->stock = $value;
    }

    /**
     * Sets the supplier property value. The supplier property
     * @param SupplierGetDto|null $value Value to set for the supplier property.
    */
    public function setSupplier(?SupplierGetDto $value): void {
        $this->supplier = $value;
    }

}
