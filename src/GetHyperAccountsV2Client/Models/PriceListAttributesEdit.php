<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class PriceListAttributesEdit implements Parsable
{
    /**
     * @var int|null $currency The currency property
    */
    private ?int $currency = null;

    /**
     * @var int|null $hasStaticPrices The hasStaticPrices property
    */
    private ?int $hasStaticPrices = null;

    /**
     * @var string|null $name The name property
    */
    private ?string $name = null;

    /**
     * @var PriceListAttributesEdit_type|null $type The type property
    */
    private ?PriceListAttributesEdit_type $type = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return PriceListAttributesEdit
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): PriceListAttributesEdit {
        return new PriceListAttributesEdit();
    }

    /**
     * Gets the currency property value. The currency property
     * @return int|null
    */
    public function getCurrency(): ?int {
        return $this->currency;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'currency' => fn(ParseNode $n) => $o->setCurrency($n->getIntegerValue()),
            'hasStaticPrices' => fn(ParseNode $n) => $o->setHasStaticPrices($n->getIntegerValue()),
            'name' => fn(ParseNode $n) => $o->setName($n->getStringValue()),
            'type' => fn(ParseNode $n) => $o->setType($n->getEnumValue(PriceListAttributesEdit_type::class)),
        ];
    }

    /**
     * Gets the hasStaticPrices property value. The hasStaticPrices property
     * @return int|null
    */
    public function getHasStaticPrices(): ?int {
        return $this->hasStaticPrices;
    }

    /**
     * Gets the name property value. The name property
     * @return string|null
    */
    public function getName(): ?string {
        return $this->name;
    }

    /**
     * Gets the type property value. The type property
     * @return PriceListAttributesEdit_type|null
    */
    public function getType(): ?PriceListAttributesEdit_type {
        return $this->type;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeIntegerValue('currency', $this->getCurrency());
        $writer->writeIntegerValue('hasStaticPrices', $this->getHasStaticPrices());
        $writer->writeStringValue('name', $this->getName());
        $writer->writeEnumValue('type', $this->getType());
    }

    /**
     * Sets the currency property value. The currency property
     * @param int|null $value Value to set for the currency property.
    */
    public function setCurrency(?int $value): void {
        $this->currency = $value;
    }

    /**
     * Sets the hasStaticPrices property value. The hasStaticPrices property
     * @param int|null $value Value to set for the hasStaticPrices property.
    */
    public function setHasStaticPrices(?int $value): void {
        $this->hasStaticPrices = $value;
    }

    /**
     * Sets the name property value. The name property
     * @param string|null $value Value to set for the name property.
    */
    public function setName(?string $value): void {
        $this->name = $value;
    }

    /**
     * Sets the type property value. The type property
     * @param PriceListAttributesEdit_type|null $value Value to set for the type property.
    */
    public function setType(?PriceListAttributesEdit_type $value): void {
        $this->type = $value;
    }

}
