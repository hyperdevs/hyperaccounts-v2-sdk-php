<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class PurchaseOrderRelationshipsWrite implements Parsable
{
    /**
     * @var SupplierRelatedRequiredRelationshipWrite|null $supplier The supplier property
    */
    private ?SupplierRelatedRequiredRelationshipWrite $supplier = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return PurchaseOrderRelationshipsWrite
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): PurchaseOrderRelationshipsWrite {
        return new PurchaseOrderRelationshipsWrite();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'supplier' => fn(ParseNode $n) => $o->setSupplier($n->getObjectValue([SupplierRelatedRequiredRelationshipWrite::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the supplier property value. The supplier property
     * @return SupplierRelatedRequiredRelationshipWrite|null
    */
    public function getSupplier(): ?SupplierRelatedRequiredRelationshipWrite {
        return $this->supplier;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('supplier', $this->getSupplier());
    }

    /**
     * Sets the supplier property value. The supplier property
     * @param SupplierRelatedRequiredRelationshipWrite|null $value Value to set for the supplier property.
    */
    public function setSupplier(?SupplierRelatedRequiredRelationshipWrite $value): void {
        $this->supplier = $value;
    }

}
