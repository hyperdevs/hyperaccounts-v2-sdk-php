<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Enum;

class TransactionSplitExceptTypeAttributesWrite_bankFlag extends Enum {
    public const BANK_NOT_APPLICABLE = 'BankNotApplicable';
    public const BANK_RECONCILED1 = 'BankReconciled1';
    public const BANK_UNRECONCILED1 = 'BankUnreconciled1';
    public const BANK_ADJUSTMENT1 = 'BankAdjustment1';
    public const BANK_CLEARED = 'BankCleared';
    public const BANK_ERECONCILED = 'BankEreconciled';
    public const BANK_UNRECONCILED2 = 'BankUnreconciled2';
    public const BANK_NOT_APPLICABLE2 = 'BankNotApplicable2';
    public const BANK_RECONCILED2 = 'BankReconciled2';
}
