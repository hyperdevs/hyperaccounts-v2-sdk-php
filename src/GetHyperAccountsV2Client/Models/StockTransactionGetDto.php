<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class StockTransactionGetDto implements Parsable
{
    /**
     * @var StockTransactionAttributesRead|null $attributes The attributes property
    */
    private ?StockTransactionAttributesRead $attributes = null;

    /**
     * @var StockTransactionIncluded|null $included The included property
    */
    private ?StockTransactionIncluded $included = null;

    /**
     * @var ResourceLinks|null $links The links property
    */
    private ?ResourceLinks $links = null;

    /**
     * @var StockTransactionRelationships|null $relationships The relationships property
    */
    private ?StockTransactionRelationships $relationships = null;

    /**
     * @var int|null $tranNumber The tranNumber property
    */
    private ?int $tranNumber = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return StockTransactionGetDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): StockTransactionGetDto {
        return new StockTransactionGetDto();
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return StockTransactionAttributesRead|null
    */
    public function getAttributes(): ?StockTransactionAttributesRead {
        return $this->attributes;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([StockTransactionAttributesRead::class, 'createFromDiscriminatorValue'])),
            'included' => fn(ParseNode $n) => $o->setIncluded($n->getObjectValue([StockTransactionIncluded::class, 'createFromDiscriminatorValue'])),
            'links' => fn(ParseNode $n) => $o->setLinks($n->getObjectValue([ResourceLinks::class, 'createFromDiscriminatorValue'])),
            'relationships' => fn(ParseNode $n) => $o->setRelationships($n->getObjectValue([StockTransactionRelationships::class, 'createFromDiscriminatorValue'])),
            'tranNumber' => fn(ParseNode $n) => $o->setTranNumber($n->getIntegerValue()),
        ];
    }

    /**
     * Gets the included property value. The included property
     * @return StockTransactionIncluded|null
    */
    public function getIncluded(): ?StockTransactionIncluded {
        return $this->included;
    }

    /**
     * Gets the links property value. The links property
     * @return ResourceLinks|null
    */
    public function getLinks(): ?ResourceLinks {
        return $this->links;
    }

    /**
     * Gets the relationships property value. The relationships property
     * @return StockTransactionRelationships|null
    */
    public function getRelationships(): ?StockTransactionRelationships {
        return $this->relationships;
    }

    /**
     * Gets the tranNumber property value. The tranNumber property
     * @return int|null
    */
    public function getTranNumber(): ?int {
        return $this->tranNumber;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('attributes', $this->getAttributes());
        $writer->writeObjectValue('included', $this->getIncluded());
        $writer->writeObjectValue('links', $this->getLinks());
        $writer->writeObjectValue('relationships', $this->getRelationships());
        $writer->writeIntegerValue('tranNumber', $this->getTranNumber());
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param StockTransactionAttributesRead|null $value Value to set for the attributes property.
    */
    public function setAttributes(?StockTransactionAttributesRead $value): void {
        $this->attributes = $value;
    }

    /**
     * Sets the included property value. The included property
     * @param StockTransactionIncluded|null $value Value to set for the included property.
    */
    public function setIncluded(?StockTransactionIncluded $value): void {
        $this->included = $value;
    }

    /**
     * Sets the links property value. The links property
     * @param ResourceLinks|null $value Value to set for the links property.
    */
    public function setLinks(?ResourceLinks $value): void {
        $this->links = $value;
    }

    /**
     * Sets the relationships property value. The relationships property
     * @param StockTransactionRelationships|null $value Value to set for the relationships property.
    */
    public function setRelationships(?StockTransactionRelationships $value): void {
        $this->relationships = $value;
    }

    /**
     * Sets the tranNumber property value. The tranNumber property
     * @param int|null $value Value to set for the tranNumber property.
    */
    public function setTranNumber(?int $value): void {
        $this->tranNumber = $value;
    }

}
