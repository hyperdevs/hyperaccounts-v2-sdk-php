<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class SalesOrderItemPostDto implements Parsable
{
    /**
     * @var SalesOrderItemAttributesWrite|null $attributes The attributes property
    */
    private ?SalesOrderItemAttributesWrite $attributes = null;

    /**
     * @var SalesOrderItemRelationshipsWrite|null $relationships The relationships property
    */
    private ?SalesOrderItemRelationshipsWrite $relationships = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return SalesOrderItemPostDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): SalesOrderItemPostDto {
        return new SalesOrderItemPostDto();
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return SalesOrderItemAttributesWrite|null
    */
    public function getAttributes(): ?SalesOrderItemAttributesWrite {
        return $this->attributes;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([SalesOrderItemAttributesWrite::class, 'createFromDiscriminatorValue'])),
            'relationships' => fn(ParseNode $n) => $o->setRelationships($n->getObjectValue([SalesOrderItemRelationshipsWrite::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the relationships property value. The relationships property
     * @return SalesOrderItemRelationshipsWrite|null
    */
    public function getRelationships(): ?SalesOrderItemRelationshipsWrite {
        return $this->relationships;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('attributes', $this->getAttributes());
        $writer->writeObjectValue('relationships', $this->getRelationships());
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param SalesOrderItemAttributesWrite|null $value Value to set for the attributes property.
    */
    public function setAttributes(?SalesOrderItemAttributesWrite $value): void {
        $this->attributes = $value;
    }

    /**
     * Sets the relationships property value. The relationships property
     * @param SalesOrderItemRelationshipsWrite|null $value Value to set for the relationships property.
    */
    public function setRelationships(?SalesOrderItemRelationshipsWrite $value): void {
        $this->relationships = $value;
    }

}
