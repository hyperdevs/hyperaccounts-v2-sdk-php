<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class SalesInvoiceRelationships implements Parsable
{
    /**
     * @var CustomerRelatedRelationship|null $customer The customer property
    */
    private ?CustomerRelatedRelationship $customer = null;

    /**
     * @var SalesInvoiceItemRelatedListRelationship|null $items The items property
    */
    private ?SalesInvoiceItemRelatedListRelationship $items = null;

    /**
     * @var ProjectRelatedRelationship|null $project The project property
    */
    private ?ProjectRelatedRelationship $project = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return SalesInvoiceRelationships
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): SalesInvoiceRelationships {
        return new SalesInvoiceRelationships();
    }

    /**
     * Gets the customer property value. The customer property
     * @return CustomerRelatedRelationship|null
    */
    public function getCustomer(): ?CustomerRelatedRelationship {
        return $this->customer;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'customer' => fn(ParseNode $n) => $o->setCustomer($n->getObjectValue([CustomerRelatedRelationship::class, 'createFromDiscriminatorValue'])),
            'items' => fn(ParseNode $n) => $o->setItems($n->getObjectValue([SalesInvoiceItemRelatedListRelationship::class, 'createFromDiscriminatorValue'])),
            'project' => fn(ParseNode $n) => $o->setProject($n->getObjectValue([ProjectRelatedRelationship::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the items property value. The items property
     * @return SalesInvoiceItemRelatedListRelationship|null
    */
    public function getItems(): ?SalesInvoiceItemRelatedListRelationship {
        return $this->items;
    }

    /**
     * Gets the project property value. The project property
     * @return ProjectRelatedRelationship|null
    */
    public function getProject(): ?ProjectRelatedRelationship {
        return $this->project;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('customer', $this->getCustomer());
        $writer->writeObjectValue('items', $this->getItems());
        $writer->writeObjectValue('project', $this->getProject());
    }

    /**
     * Sets the customer property value. The customer property
     * @param CustomerRelatedRelationship|null $value Value to set for the customer property.
    */
    public function setCustomer(?CustomerRelatedRelationship $value): void {
        $this->customer = $value;
    }

    /**
     * Sets the items property value. The items property
     * @param SalesInvoiceItemRelatedListRelationship|null $value Value to set for the items property.
    */
    public function setItems(?SalesInvoiceItemRelatedListRelationship $value): void {
        $this->items = $value;
    }

    /**
     * Sets the project property value. The project property
     * @param ProjectRelatedRelationship|null $value Value to set for the project property.
    */
    public function setProject(?ProjectRelatedRelationship $value): void {
        $this->project = $value;
    }

}
