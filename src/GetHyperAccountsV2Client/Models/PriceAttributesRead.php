<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use DateTime;
use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class PriceAttributesRead implements Parsable
{
    /**
     * @var int|null $calcMethod The calcMethod property
    */
    private ?int $calcMethod = null;

    /**
     * @var float|null $calcValue The calcValue property
    */
    private ?float $calcValue = null;

    /**
     * @var string|null $pricingRef The pricingRef property
    */
    private ?string $pricingRef = null;

    /**
     * @var DateTime|null $recordCreateDate The recordCreateDate property
    */
    private ?DateTime $recordCreateDate = null;

    /**
     * @var int|null $recordDeleted The recordDeleted property
    */
    private ?int $recordDeleted = null;

    /**
     * @var DateTime|null $recordModifyDate The recordModifyDate property
    */
    private ?DateTime $recordModifyDate = null;

    /**
     * @var float|null $rounderAdjustment The rounderAdjustment property
    */
    private ?float $rounderAdjustment = null;

    /**
     * @var int|null $rounderDirection The rounderDirection property
    */
    private ?int $rounderDirection = null;

    /**
     * @var int|null $rounderMethod The rounderMethod property
    */
    private ?int $rounderMethod = null;

    /**
     * @var float|null $rounderMultipleOf The rounderMultipleOf property
    */
    private ?float $rounderMultipleOf = null;

    /**
     * @var float|null $storedPrice The storedPrice property
    */
    private ?float $storedPrice = null;

    /**
     * @var PriceAttributesRead_type|null $type The type property
    */
    private ?PriceAttributesRead_type $type = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return PriceAttributesRead
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): PriceAttributesRead {
        return new PriceAttributesRead();
    }

    /**
     * Gets the calcMethod property value. The calcMethod property
     * @return int|null
    */
    public function getCalcMethod(): ?int {
        return $this->calcMethod;
    }

    /**
     * Gets the calcValue property value. The calcValue property
     * @return float|null
    */
    public function getCalcValue(): ?float {
        return $this->calcValue;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'calcMethod' => fn(ParseNode $n) => $o->setCalcMethod($n->getIntegerValue()),
            'calcValue' => fn(ParseNode $n) => $o->setCalcValue($n->getFloatValue()),
            'pricingRef' => fn(ParseNode $n) => $o->setPricingRef($n->getStringValue()),
            'recordCreateDate' => fn(ParseNode $n) => $o->setRecordCreateDate($n->getDateTimeValue()),
            'recordDeleted' => fn(ParseNode $n) => $o->setRecordDeleted($n->getIntegerValue()),
            'recordModifyDate' => fn(ParseNode $n) => $o->setRecordModifyDate($n->getDateTimeValue()),
            'rounderAdjustment' => fn(ParseNode $n) => $o->setRounderAdjustment($n->getFloatValue()),
            'rounderDirection' => fn(ParseNode $n) => $o->setRounderDirection($n->getIntegerValue()),
            'rounderMethod' => fn(ParseNode $n) => $o->setRounderMethod($n->getIntegerValue()),
            'rounderMultipleOf' => fn(ParseNode $n) => $o->setRounderMultipleOf($n->getFloatValue()),
            'storedPrice' => fn(ParseNode $n) => $o->setStoredPrice($n->getFloatValue()),
            'type' => fn(ParseNode $n) => $o->setType($n->getEnumValue(PriceAttributesRead_type::class)),
        ];
    }

    /**
     * Gets the pricingRef property value. The pricingRef property
     * @return string|null
    */
    public function getPricingRef(): ?string {
        return $this->pricingRef;
    }

    /**
     * Gets the recordCreateDate property value. The recordCreateDate property
     * @return DateTime|null
    */
    public function getRecordCreateDate(): ?DateTime {
        return $this->recordCreateDate;
    }

    /**
     * Gets the recordDeleted property value. The recordDeleted property
     * @return int|null
    */
    public function getRecordDeleted(): ?int {
        return $this->recordDeleted;
    }

    /**
     * Gets the recordModifyDate property value. The recordModifyDate property
     * @return DateTime|null
    */
    public function getRecordModifyDate(): ?DateTime {
        return $this->recordModifyDate;
    }

    /**
     * Gets the rounderAdjustment property value. The rounderAdjustment property
     * @return float|null
    */
    public function getRounderAdjustment(): ?float {
        return $this->rounderAdjustment;
    }

    /**
     * Gets the rounderDirection property value. The rounderDirection property
     * @return int|null
    */
    public function getRounderDirection(): ?int {
        return $this->rounderDirection;
    }

    /**
     * Gets the rounderMethod property value. The rounderMethod property
     * @return int|null
    */
    public function getRounderMethod(): ?int {
        return $this->rounderMethod;
    }

    /**
     * Gets the rounderMultipleOf property value. The rounderMultipleOf property
     * @return float|null
    */
    public function getRounderMultipleOf(): ?float {
        return $this->rounderMultipleOf;
    }

    /**
     * Gets the storedPrice property value. The storedPrice property
     * @return float|null
    */
    public function getStoredPrice(): ?float {
        return $this->storedPrice;
    }

    /**
     * Gets the type property value. The type property
     * @return PriceAttributesRead_type|null
    */
    public function getType(): ?PriceAttributesRead_type {
        return $this->type;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeIntegerValue('calcMethod', $this->getCalcMethod());
        $writer->writeFloatValue('calcValue', $this->getCalcValue());
        $writer->writeStringValue('pricingRef', $this->getPricingRef());
        $writer->writeDateTimeValue('recordCreateDate', $this->getRecordCreateDate());
        $writer->writeIntegerValue('recordDeleted', $this->getRecordDeleted());
        $writer->writeDateTimeValue('recordModifyDate', $this->getRecordModifyDate());
        $writer->writeFloatValue('rounderAdjustment', $this->getRounderAdjustment());
        $writer->writeIntegerValue('rounderDirection', $this->getRounderDirection());
        $writer->writeIntegerValue('rounderMethod', $this->getRounderMethod());
        $writer->writeFloatValue('rounderMultipleOf', $this->getRounderMultipleOf());
        $writer->writeFloatValue('storedPrice', $this->getStoredPrice());
        $writer->writeEnumValue('type', $this->getType());
    }

    /**
     * Sets the calcMethod property value. The calcMethod property
     * @param int|null $value Value to set for the calcMethod property.
    */
    public function setCalcMethod(?int $value): void {
        $this->calcMethod = $value;
    }

    /**
     * Sets the calcValue property value. The calcValue property
     * @param float|null $value Value to set for the calcValue property.
    */
    public function setCalcValue(?float $value): void {
        $this->calcValue = $value;
    }

    /**
     * Sets the pricingRef property value. The pricingRef property
     * @param string|null $value Value to set for the pricingRef property.
    */
    public function setPricingRef(?string $value): void {
        $this->pricingRef = $value;
    }

    /**
     * Sets the recordCreateDate property value. The recordCreateDate property
     * @param DateTime|null $value Value to set for the recordCreateDate property.
    */
    public function setRecordCreateDate(?DateTime $value): void {
        $this->recordCreateDate = $value;
    }

    /**
     * Sets the recordDeleted property value. The recordDeleted property
     * @param int|null $value Value to set for the recordDeleted property.
    */
    public function setRecordDeleted(?int $value): void {
        $this->recordDeleted = $value;
    }

    /**
     * Sets the recordModifyDate property value. The recordModifyDate property
     * @param DateTime|null $value Value to set for the recordModifyDate property.
    */
    public function setRecordModifyDate(?DateTime $value): void {
        $this->recordModifyDate = $value;
    }

    /**
     * Sets the rounderAdjustment property value. The rounderAdjustment property
     * @param float|null $value Value to set for the rounderAdjustment property.
    */
    public function setRounderAdjustment(?float $value): void {
        $this->rounderAdjustment = $value;
    }

    /**
     * Sets the rounderDirection property value. The rounderDirection property
     * @param int|null $value Value to set for the rounderDirection property.
    */
    public function setRounderDirection(?int $value): void {
        $this->rounderDirection = $value;
    }

    /**
     * Sets the rounderMethod property value. The rounderMethod property
     * @param int|null $value Value to set for the rounderMethod property.
    */
    public function setRounderMethod(?int $value): void {
        $this->rounderMethod = $value;
    }

    /**
     * Sets the rounderMultipleOf property value. The rounderMultipleOf property
     * @param float|null $value Value to set for the rounderMultipleOf property.
    */
    public function setRounderMultipleOf(?float $value): void {
        $this->rounderMultipleOf = $value;
    }

    /**
     * Sets the storedPrice property value. The storedPrice property
     * @param float|null $value Value to set for the storedPrice property.
    */
    public function setStoredPrice(?float $value): void {
        $this->storedPrice = $value;
    }

    /**
     * Sets the type property value. The type property
     * @param PriceAttributesRead_type|null $value Value to set for the type property.
    */
    public function setType(?PriceAttributesRead_type $value): void {
        $this->type = $value;
    }

}
