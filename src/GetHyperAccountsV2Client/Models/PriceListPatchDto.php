<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class PriceListPatchDto implements Parsable
{
    /**
     * @var PriceListAttributesEdit|null $attributes The attributes property
    */
    private ?PriceListAttributesEdit $attributes = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return PriceListPatchDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): PriceListPatchDto {
        return new PriceListPatchDto();
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return PriceListAttributesEdit|null
    */
    public function getAttributes(): ?PriceListAttributesEdit {
        return $this->attributes;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([PriceListAttributesEdit::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('attributes', $this->getAttributes());
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param PriceListAttributesEdit|null $value Value to set for the attributes property.
    */
    public function setAttributes(?PriceListAttributesEdit $value): void {
        $this->attributes = $value;
    }

}
