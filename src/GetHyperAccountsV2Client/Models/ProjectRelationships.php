<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class ProjectRelationships implements Parsable
{
    /**
     * @var CustomerRelatedRelationship|null $customer The customer property
    */
    private ?CustomerRelatedRelationship $customer = null;

    /**
     * @var ProjectBudgetRelatedListRelationship|null $projectBudgets The projectBudgets property
    */
    private ?ProjectBudgetRelatedListRelationship $projectBudgets = null;

    /**
     * @var ProjectTranRelatedListRelationship|null $projectTrans The projectTrans property
    */
    private ?ProjectTranRelatedListRelationship $projectTrans = null;

    /**
     * @var PurchaseOrderRelatedListRelationship|null $purchaseOrders The purchaseOrders property
    */
    private ?PurchaseOrderRelatedListRelationship $purchaseOrders = null;

    /**
     * @var SalesInvoiceItemRelatedListRelationship|null $salesInvoiceItems The salesInvoiceItems property
    */
    private ?SalesInvoiceItemRelatedListRelationship $salesInvoiceItems = null;

    /**
     * @var SalesInvoiceRelatedListRelationship|null $salesInvoices The salesInvoices property
    */
    private ?SalesInvoiceRelatedListRelationship $salesInvoices = null;

    /**
     * @var SalesOrderItemRelatedListRelationship|null $salesOrderItems The salesOrderItems property
    */
    private ?SalesOrderItemRelatedListRelationship $salesOrderItems = null;

    /**
     * @var SalesOrderRelatedListRelationship|null $salesOrders The salesOrders property
    */
    private ?SalesOrderRelatedListRelationship $salesOrders = null;

    /**
     * @var ProjectStatusRelatedRelationship|null $status The status property
    */
    private ?ProjectStatusRelatedRelationship $status = null;

    /**
     * @var TransactionSplitRelatedListRelationship|null $transactionSplits The transactionSplits property
    */
    private ?TransactionSplitRelatedListRelationship $transactionSplits = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return ProjectRelationships
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): ProjectRelationships {
        return new ProjectRelationships();
    }

    /**
     * Gets the customer property value. The customer property
     * @return CustomerRelatedRelationship|null
    */
    public function getCustomer(): ?CustomerRelatedRelationship {
        return $this->customer;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'customer' => fn(ParseNode $n) => $o->setCustomer($n->getObjectValue([CustomerRelatedRelationship::class, 'createFromDiscriminatorValue'])),
            'projectBudgets' => fn(ParseNode $n) => $o->setProjectBudgets($n->getObjectValue([ProjectBudgetRelatedListRelationship::class, 'createFromDiscriminatorValue'])),
            'projectTrans' => fn(ParseNode $n) => $o->setProjectTrans($n->getObjectValue([ProjectTranRelatedListRelationship::class, 'createFromDiscriminatorValue'])),
            'purchaseOrders' => fn(ParseNode $n) => $o->setPurchaseOrders($n->getObjectValue([PurchaseOrderRelatedListRelationship::class, 'createFromDiscriminatorValue'])),
            'salesInvoiceItems' => fn(ParseNode $n) => $o->setSalesInvoiceItems($n->getObjectValue([SalesInvoiceItemRelatedListRelationship::class, 'createFromDiscriminatorValue'])),
            'salesInvoices' => fn(ParseNode $n) => $o->setSalesInvoices($n->getObjectValue([SalesInvoiceRelatedListRelationship::class, 'createFromDiscriminatorValue'])),
            'salesOrderItems' => fn(ParseNode $n) => $o->setSalesOrderItems($n->getObjectValue([SalesOrderItemRelatedListRelationship::class, 'createFromDiscriminatorValue'])),
            'salesOrders' => fn(ParseNode $n) => $o->setSalesOrders($n->getObjectValue([SalesOrderRelatedListRelationship::class, 'createFromDiscriminatorValue'])),
            'status' => fn(ParseNode $n) => $o->setStatus($n->getObjectValue([ProjectStatusRelatedRelationship::class, 'createFromDiscriminatorValue'])),
            'transactionSplits' => fn(ParseNode $n) => $o->setTransactionSplits($n->getObjectValue([TransactionSplitRelatedListRelationship::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the projectBudgets property value. The projectBudgets property
     * @return ProjectBudgetRelatedListRelationship|null
    */
    public function getProjectBudgets(): ?ProjectBudgetRelatedListRelationship {
        return $this->projectBudgets;
    }

    /**
     * Gets the projectTrans property value. The projectTrans property
     * @return ProjectTranRelatedListRelationship|null
    */
    public function getProjectTrans(): ?ProjectTranRelatedListRelationship {
        return $this->projectTrans;
    }

    /**
     * Gets the purchaseOrders property value. The purchaseOrders property
     * @return PurchaseOrderRelatedListRelationship|null
    */
    public function getPurchaseOrders(): ?PurchaseOrderRelatedListRelationship {
        return $this->purchaseOrders;
    }

    /**
     * Gets the salesInvoiceItems property value. The salesInvoiceItems property
     * @return SalesInvoiceItemRelatedListRelationship|null
    */
    public function getSalesInvoiceItems(): ?SalesInvoiceItemRelatedListRelationship {
        return $this->salesInvoiceItems;
    }

    /**
     * Gets the salesInvoices property value. The salesInvoices property
     * @return SalesInvoiceRelatedListRelationship|null
    */
    public function getSalesInvoices(): ?SalesInvoiceRelatedListRelationship {
        return $this->salesInvoices;
    }

    /**
     * Gets the salesOrderItems property value. The salesOrderItems property
     * @return SalesOrderItemRelatedListRelationship|null
    */
    public function getSalesOrderItems(): ?SalesOrderItemRelatedListRelationship {
        return $this->salesOrderItems;
    }

    /**
     * Gets the salesOrders property value. The salesOrders property
     * @return SalesOrderRelatedListRelationship|null
    */
    public function getSalesOrders(): ?SalesOrderRelatedListRelationship {
        return $this->salesOrders;
    }

    /**
     * Gets the status property value. The status property
     * @return ProjectStatusRelatedRelationship|null
    */
    public function getStatus(): ?ProjectStatusRelatedRelationship {
        return $this->status;
    }

    /**
     * Gets the transactionSplits property value. The transactionSplits property
     * @return TransactionSplitRelatedListRelationship|null
    */
    public function getTransactionSplits(): ?TransactionSplitRelatedListRelationship {
        return $this->transactionSplits;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('customer', $this->getCustomer());
        $writer->writeObjectValue('projectBudgets', $this->getProjectBudgets());
        $writer->writeObjectValue('projectTrans', $this->getProjectTrans());
        $writer->writeObjectValue('purchaseOrders', $this->getPurchaseOrders());
        $writer->writeObjectValue('salesInvoiceItems', $this->getSalesInvoiceItems());
        $writer->writeObjectValue('salesInvoices', $this->getSalesInvoices());
        $writer->writeObjectValue('salesOrderItems', $this->getSalesOrderItems());
        $writer->writeObjectValue('salesOrders', $this->getSalesOrders());
        $writer->writeObjectValue('status', $this->getStatus());
        $writer->writeObjectValue('transactionSplits', $this->getTransactionSplits());
    }

    /**
     * Sets the customer property value. The customer property
     * @param CustomerRelatedRelationship|null $value Value to set for the customer property.
    */
    public function setCustomer(?CustomerRelatedRelationship $value): void {
        $this->customer = $value;
    }

    /**
     * Sets the projectBudgets property value. The projectBudgets property
     * @param ProjectBudgetRelatedListRelationship|null $value Value to set for the projectBudgets property.
    */
    public function setProjectBudgets(?ProjectBudgetRelatedListRelationship $value): void {
        $this->projectBudgets = $value;
    }

    /**
     * Sets the projectTrans property value. The projectTrans property
     * @param ProjectTranRelatedListRelationship|null $value Value to set for the projectTrans property.
    */
    public function setProjectTrans(?ProjectTranRelatedListRelationship $value): void {
        $this->projectTrans = $value;
    }

    /**
     * Sets the purchaseOrders property value. The purchaseOrders property
     * @param PurchaseOrderRelatedListRelationship|null $value Value to set for the purchaseOrders property.
    */
    public function setPurchaseOrders(?PurchaseOrderRelatedListRelationship $value): void {
        $this->purchaseOrders = $value;
    }

    /**
     * Sets the salesInvoiceItems property value. The salesInvoiceItems property
     * @param SalesInvoiceItemRelatedListRelationship|null $value Value to set for the salesInvoiceItems property.
    */
    public function setSalesInvoiceItems(?SalesInvoiceItemRelatedListRelationship $value): void {
        $this->salesInvoiceItems = $value;
    }

    /**
     * Sets the salesInvoices property value. The salesInvoices property
     * @param SalesInvoiceRelatedListRelationship|null $value Value to set for the salesInvoices property.
    */
    public function setSalesInvoices(?SalesInvoiceRelatedListRelationship $value): void {
        $this->salesInvoices = $value;
    }

    /**
     * Sets the salesOrderItems property value. The salesOrderItems property
     * @param SalesOrderItemRelatedListRelationship|null $value Value to set for the salesOrderItems property.
    */
    public function setSalesOrderItems(?SalesOrderItemRelatedListRelationship $value): void {
        $this->salesOrderItems = $value;
    }

    /**
     * Sets the salesOrders property value. The salesOrders property
     * @param SalesOrderRelatedListRelationship|null $value Value to set for the salesOrders property.
    */
    public function setSalesOrders(?SalesOrderRelatedListRelationship $value): void {
        $this->salesOrders = $value;
    }

    /**
     * Sets the status property value. The status property
     * @param ProjectStatusRelatedRelationship|null $value Value to set for the status property.
    */
    public function setStatus(?ProjectStatusRelatedRelationship $value): void {
        $this->status = $value;
    }

    /**
     * Sets the transactionSplits property value. The transactionSplits property
     * @param TransactionSplitRelatedListRelationship|null $value Value to set for the transactionSplits property.
    */
    public function setTransactionSplits(?TransactionSplitRelatedListRelationship $value): void {
        $this->transactionSplits = $value;
    }

}
