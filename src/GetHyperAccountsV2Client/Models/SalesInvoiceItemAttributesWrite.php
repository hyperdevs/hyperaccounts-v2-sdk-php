<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use DateTime;
use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class SalesInvoiceItemAttributesWrite implements Parsable
{
    /**
     * @var float|null $addDiscRate The addDiscRate property
    */
    private ?float $addDiscRate = null;

    /**
     * @var string|null $comment1 The comment1 property
    */
    private ?string $comment1 = null;

    /**
     * @var string|null $comment2 The comment2 property
    */
    private ?string $comment2 = null;

    /**
     * @var DateTime|null $deliveryDate The deliveryDate property
    */
    private ?DateTime $deliveryDate = null;

    /**
     * @var int|null $deptNumber The deptNumber property
    */
    private ?int $deptNumber = null;

    /**
     * @var string|null $description The description property
    */
    private ?string $description = null;

    /**
     * @var float|null $discountAmount The discountAmount property
    */
    private ?float $discountAmount = null;

    /**
     * @var float|null $discountRate The discountRate property
    */
    private ?float $discountRate = null;

    /**
     * @var int|null $extOrderLineRef The extOrderLineRef property
    */
    private ?int $extOrderLineRef = null;

    /**
     * @var string|null $extOrderRef The extOrderRef property
    */
    private ?string $extOrderRef = null;

    /**
     * @var float|null $fullNetAmount The fullNetAmount property
    */
    private ?float $fullNetAmount = null;

    /**
     * @var int|null $itemNumber The itemNumber property
    */
    private ?int $itemNumber = null;

    /**
     * @var float|null $netAmount The netAmount property
    */
    private ?float $netAmount = null;

    /**
     * @var int|null $serviceFlag The serviceFlag property
    */
    private ?int $serviceFlag = null;

    /**
     * @var float|null $taxAmount The taxAmount property
    */
    private ?float $taxAmount = null;

    /**
     * @var SalesInvoiceItemAttributesWrite_taxCode|null $taxCode The taxCode property
    */
    private ?SalesInvoiceItemAttributesWrite_taxCode $taxCode = null;

    /**
     * @var float|null $taxRate The taxRate property
    */
    private ?float $taxRate = null;

    /**
     * @var string|null $text The text property
    */
    private ?string $text = null;

    /**
     * @var string|null $unitOfSale The unitOfSale property
    */
    private ?string $unitOfSale = null;

    /**
     * @var float|null $unitPrice The unitPrice property
    */
    private ?float $unitPrice = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return SalesInvoiceItemAttributesWrite
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): SalesInvoiceItemAttributesWrite {
        return new SalesInvoiceItemAttributesWrite();
    }

    /**
     * Gets the addDiscRate property value. The addDiscRate property
     * @return float|null
    */
    public function getAddDiscRate(): ?float {
        return $this->addDiscRate;
    }

    /**
     * Gets the comment1 property value. The comment1 property
     * @return string|null
    */
    public function getComment1(): ?string {
        return $this->comment1;
    }

    /**
     * Gets the comment2 property value. The comment2 property
     * @return string|null
    */
    public function getComment2(): ?string {
        return $this->comment2;
    }

    /**
     * Gets the deliveryDate property value. The deliveryDate property
     * @return DateTime|null
    */
    public function getDeliveryDate(): ?DateTime {
        return $this->deliveryDate;
    }

    /**
     * Gets the deptNumber property value. The deptNumber property
     * @return int|null
    */
    public function getDeptNumber(): ?int {
        return $this->deptNumber;
    }

    /**
     * Gets the description property value. The description property
     * @return string|null
    */
    public function getDescription(): ?string {
        return $this->description;
    }

    /**
     * Gets the discountAmount property value. The discountAmount property
     * @return float|null
    */
    public function getDiscountAmount(): ?float {
        return $this->discountAmount;
    }

    /**
     * Gets the discountRate property value. The discountRate property
     * @return float|null
    */
    public function getDiscountRate(): ?float {
        return $this->discountRate;
    }

    /**
     * Gets the extOrderLineRef property value. The extOrderLineRef property
     * @return int|null
    */
    public function getExtOrderLineRef(): ?int {
        return $this->extOrderLineRef;
    }

    /**
     * Gets the extOrderRef property value. The extOrderRef property
     * @return string|null
    */
    public function getExtOrderRef(): ?string {
        return $this->extOrderRef;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'addDiscRate' => fn(ParseNode $n) => $o->setAddDiscRate($n->getFloatValue()),
            'comment1' => fn(ParseNode $n) => $o->setComment1($n->getStringValue()),
            'comment2' => fn(ParseNode $n) => $o->setComment2($n->getStringValue()),
            'deliveryDate' => fn(ParseNode $n) => $o->setDeliveryDate($n->getDateTimeValue()),
            'deptNumber' => fn(ParseNode $n) => $o->setDeptNumber($n->getIntegerValue()),
            'description' => fn(ParseNode $n) => $o->setDescription($n->getStringValue()),
            'discountAmount' => fn(ParseNode $n) => $o->setDiscountAmount($n->getFloatValue()),
            'discountRate' => fn(ParseNode $n) => $o->setDiscountRate($n->getFloatValue()),
            'extOrderLineRef' => fn(ParseNode $n) => $o->setExtOrderLineRef($n->getIntegerValue()),
            'extOrderRef' => fn(ParseNode $n) => $o->setExtOrderRef($n->getStringValue()),
            'fullNetAmount' => fn(ParseNode $n) => $o->setFullNetAmount($n->getFloatValue()),
            'itemNumber' => fn(ParseNode $n) => $o->setItemNumber($n->getIntegerValue()),
            'netAmount' => fn(ParseNode $n) => $o->setNetAmount($n->getFloatValue()),
            'serviceFlag' => fn(ParseNode $n) => $o->setServiceFlag($n->getIntegerValue()),
            'taxAmount' => fn(ParseNode $n) => $o->setTaxAmount($n->getFloatValue()),
            'taxCode' => fn(ParseNode $n) => $o->setTaxCode($n->getEnumValue(SalesInvoiceItemAttributesWrite_taxCode::class)),
            'taxRate' => fn(ParseNode $n) => $o->setTaxRate($n->getFloatValue()),
            'text' => fn(ParseNode $n) => $o->setText($n->getStringValue()),
            'unitOfSale' => fn(ParseNode $n) => $o->setUnitOfSale($n->getStringValue()),
            'unitPrice' => fn(ParseNode $n) => $o->setUnitPrice($n->getFloatValue()),
        ];
    }

    /**
     * Gets the fullNetAmount property value. The fullNetAmount property
     * @return float|null
    */
    public function getFullNetAmount(): ?float {
        return $this->fullNetAmount;
    }

    /**
     * Gets the itemNumber property value. The itemNumber property
     * @return int|null
    */
    public function getItemNumber(): ?int {
        return $this->itemNumber;
    }

    /**
     * Gets the netAmount property value. The netAmount property
     * @return float|null
    */
    public function getNetAmount(): ?float {
        return $this->netAmount;
    }

    /**
     * Gets the serviceFlag property value. The serviceFlag property
     * @return int|null
    */
    public function getServiceFlag(): ?int {
        return $this->serviceFlag;
    }

    /**
     * Gets the taxAmount property value. The taxAmount property
     * @return float|null
    */
    public function getTaxAmount(): ?float {
        return $this->taxAmount;
    }

    /**
     * Gets the taxCode property value. The taxCode property
     * @return SalesInvoiceItemAttributesWrite_taxCode|null
    */
    public function getTaxCode(): ?SalesInvoiceItemAttributesWrite_taxCode {
        return $this->taxCode;
    }

    /**
     * Gets the taxRate property value. The taxRate property
     * @return float|null
    */
    public function getTaxRate(): ?float {
        return $this->taxRate;
    }

    /**
     * Gets the text property value. The text property
     * @return string|null
    */
    public function getText(): ?string {
        return $this->text;
    }

    /**
     * Gets the unitOfSale property value. The unitOfSale property
     * @return string|null
    */
    public function getUnitOfSale(): ?string {
        return $this->unitOfSale;
    }

    /**
     * Gets the unitPrice property value. The unitPrice property
     * @return float|null
    */
    public function getUnitPrice(): ?float {
        return $this->unitPrice;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeFloatValue('addDiscRate', $this->getAddDiscRate());
        $writer->writeStringValue('comment1', $this->getComment1());
        $writer->writeStringValue('comment2', $this->getComment2());
        $writer->writeDateTimeValue('deliveryDate', $this->getDeliveryDate());
        $writer->writeIntegerValue('deptNumber', $this->getDeptNumber());
        $writer->writeStringValue('description', $this->getDescription());
        $writer->writeFloatValue('discountAmount', $this->getDiscountAmount());
        $writer->writeFloatValue('discountRate', $this->getDiscountRate());
        $writer->writeIntegerValue('extOrderLineRef', $this->getExtOrderLineRef());
        $writer->writeStringValue('extOrderRef', $this->getExtOrderRef());
        $writer->writeFloatValue('fullNetAmount', $this->getFullNetAmount());
        $writer->writeIntegerValue('itemNumber', $this->getItemNumber());
        $writer->writeFloatValue('netAmount', $this->getNetAmount());
        $writer->writeIntegerValue('serviceFlag', $this->getServiceFlag());
        $writer->writeFloatValue('taxAmount', $this->getTaxAmount());
        $writer->writeEnumValue('taxCode', $this->getTaxCode());
        $writer->writeFloatValue('taxRate', $this->getTaxRate());
        $writer->writeStringValue('text', $this->getText());
        $writer->writeStringValue('unitOfSale', $this->getUnitOfSale());
        $writer->writeFloatValue('unitPrice', $this->getUnitPrice());
    }

    /**
     * Sets the addDiscRate property value. The addDiscRate property
     * @param float|null $value Value to set for the addDiscRate property.
    */
    public function setAddDiscRate(?float $value): void {
        $this->addDiscRate = $value;
    }

    /**
     * Sets the comment1 property value. The comment1 property
     * @param string|null $value Value to set for the comment1 property.
    */
    public function setComment1(?string $value): void {
        $this->comment1 = $value;
    }

    /**
     * Sets the comment2 property value. The comment2 property
     * @param string|null $value Value to set for the comment2 property.
    */
    public function setComment2(?string $value): void {
        $this->comment2 = $value;
    }

    /**
     * Sets the deliveryDate property value. The deliveryDate property
     * @param DateTime|null $value Value to set for the deliveryDate property.
    */
    public function setDeliveryDate(?DateTime $value): void {
        $this->deliveryDate = $value;
    }

    /**
     * Sets the deptNumber property value. The deptNumber property
     * @param int|null $value Value to set for the deptNumber property.
    */
    public function setDeptNumber(?int $value): void {
        $this->deptNumber = $value;
    }

    /**
     * Sets the description property value. The description property
     * @param string|null $value Value to set for the description property.
    */
    public function setDescription(?string $value): void {
        $this->description = $value;
    }

    /**
     * Sets the discountAmount property value. The discountAmount property
     * @param float|null $value Value to set for the discountAmount property.
    */
    public function setDiscountAmount(?float $value): void {
        $this->discountAmount = $value;
    }

    /**
     * Sets the discountRate property value. The discountRate property
     * @param float|null $value Value to set for the discountRate property.
    */
    public function setDiscountRate(?float $value): void {
        $this->discountRate = $value;
    }

    /**
     * Sets the extOrderLineRef property value. The extOrderLineRef property
     * @param int|null $value Value to set for the extOrderLineRef property.
    */
    public function setExtOrderLineRef(?int $value): void {
        $this->extOrderLineRef = $value;
    }

    /**
     * Sets the extOrderRef property value. The extOrderRef property
     * @param string|null $value Value to set for the extOrderRef property.
    */
    public function setExtOrderRef(?string $value): void {
        $this->extOrderRef = $value;
    }

    /**
     * Sets the fullNetAmount property value. The fullNetAmount property
     * @param float|null $value Value to set for the fullNetAmount property.
    */
    public function setFullNetAmount(?float $value): void {
        $this->fullNetAmount = $value;
    }

    /**
     * Sets the itemNumber property value. The itemNumber property
     * @param int|null $value Value to set for the itemNumber property.
    */
    public function setItemNumber(?int $value): void {
        $this->itemNumber = $value;
    }

    /**
     * Sets the netAmount property value. The netAmount property
     * @param float|null $value Value to set for the netAmount property.
    */
    public function setNetAmount(?float $value): void {
        $this->netAmount = $value;
    }

    /**
     * Sets the serviceFlag property value. The serviceFlag property
     * @param int|null $value Value to set for the serviceFlag property.
    */
    public function setServiceFlag(?int $value): void {
        $this->serviceFlag = $value;
    }

    /**
     * Sets the taxAmount property value. The taxAmount property
     * @param float|null $value Value to set for the taxAmount property.
    */
    public function setTaxAmount(?float $value): void {
        $this->taxAmount = $value;
    }

    /**
     * Sets the taxCode property value. The taxCode property
     * @param SalesInvoiceItemAttributesWrite_taxCode|null $value Value to set for the taxCode property.
    */
    public function setTaxCode(?SalesInvoiceItemAttributesWrite_taxCode $value): void {
        $this->taxCode = $value;
    }

    /**
     * Sets the taxRate property value. The taxRate property
     * @param float|null $value Value to set for the taxRate property.
    */
    public function setTaxRate(?float $value): void {
        $this->taxRate = $value;
    }

    /**
     * Sets the text property value. The text property
     * @param string|null $value Value to set for the text property.
    */
    public function setText(?string $value): void {
        $this->text = $value;
    }

    /**
     * Sets the unitOfSale property value. The unitOfSale property
     * @param string|null $value Value to set for the unitOfSale property.
    */
    public function setUnitOfSale(?string $value): void {
        $this->unitOfSale = $value;
    }

    /**
     * Sets the unitPrice property value. The unitPrice property
     * @param float|null $value Value to set for the unitPrice property.
    */
    public function setUnitPrice(?float $value): void {
        $this->unitPrice = $value;
    }

}
