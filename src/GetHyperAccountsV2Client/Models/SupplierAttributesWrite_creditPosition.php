<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Enum;

class SupplierAttributesWrite_creditPosition extends Enum {
    public const GOOD = 'Good';
    public const REMINDER = 'Reminder';
    public const WARNING = 'Warning';
    public const LEGAL = 'Legal';
    public const I_N_V_A_L_I_D__E_R_R_O_R = 'INVALID_ERROR';
}
