<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Enum;

class TransactionSplitAttributesWrite_paidFlag extends Enum {
    public const NOT_APPLICABLE = 'NotApplicable';
    public const NO = 'No';
    public const YES = 'Yes';
}
