<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class CustomerAddressGetDto implements Parsable
{
    /**
     * @var CustomerAddressAttributesRead|null $attributes The attributes property
    */
    private ?CustomerAddressAttributesRead $attributes = null;

    /**
     * @var CustomerAddressIncluded|null $included The included property
    */
    private ?CustomerAddressIncluded $included = null;

    /**
     * @var ResourceLinks|null $links The links property
    */
    private ?ResourceLinks $links = null;

    /**
     * @var string|null $reference The reference property
    */
    private ?string $reference = null;

    /**
     * @var CustomerAddressRelationshipsRead|null $relationships The relationships property
    */
    private ?CustomerAddressRelationshipsRead $relationships = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return CustomerAddressGetDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): CustomerAddressGetDto {
        return new CustomerAddressGetDto();
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return CustomerAddressAttributesRead|null
    */
    public function getAttributes(): ?CustomerAddressAttributesRead {
        return $this->attributes;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([CustomerAddressAttributesRead::class, 'createFromDiscriminatorValue'])),
            'included' => fn(ParseNode $n) => $o->setIncluded($n->getObjectValue([CustomerAddressIncluded::class, 'createFromDiscriminatorValue'])),
            'links' => fn(ParseNode $n) => $o->setLinks($n->getObjectValue([ResourceLinks::class, 'createFromDiscriminatorValue'])),
            'reference' => fn(ParseNode $n) => $o->setReference($n->getStringValue()),
            'relationships' => fn(ParseNode $n) => $o->setRelationships($n->getObjectValue([CustomerAddressRelationshipsRead::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the included property value. The included property
     * @return CustomerAddressIncluded|null
    */
    public function getIncluded(): ?CustomerAddressIncluded {
        return $this->included;
    }

    /**
     * Gets the links property value. The links property
     * @return ResourceLinks|null
    */
    public function getLinks(): ?ResourceLinks {
        return $this->links;
    }

    /**
     * Gets the reference property value. The reference property
     * @return string|null
    */
    public function getReference(): ?string {
        return $this->reference;
    }

    /**
     * Gets the relationships property value. The relationships property
     * @return CustomerAddressRelationshipsRead|null
    */
    public function getRelationships(): ?CustomerAddressRelationshipsRead {
        return $this->relationships;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('attributes', $this->getAttributes());
        $writer->writeObjectValue('included', $this->getIncluded());
        $writer->writeObjectValue('links', $this->getLinks());
        $writer->writeStringValue('reference', $this->getReference());
        $writer->writeObjectValue('relationships', $this->getRelationships());
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param CustomerAddressAttributesRead|null $value Value to set for the attributes property.
    */
    public function setAttributes(?CustomerAddressAttributesRead $value): void {
        $this->attributes = $value;
    }

    /**
     * Sets the included property value. The included property
     * @param CustomerAddressIncluded|null $value Value to set for the included property.
    */
    public function setIncluded(?CustomerAddressIncluded $value): void {
        $this->included = $value;
    }

    /**
     * Sets the links property value. The links property
     * @param ResourceLinks|null $value Value to set for the links property.
    */
    public function setLinks(?ResourceLinks $value): void {
        $this->links = $value;
    }

    /**
     * Sets the reference property value. The reference property
     * @param string|null $value Value to set for the reference property.
    */
    public function setReference(?string $value): void {
        $this->reference = $value;
    }

    /**
     * Sets the relationships property value. The relationships property
     * @param CustomerAddressRelationshipsRead|null $value Value to set for the relationships property.
    */
    public function setRelationships(?CustomerAddressRelationshipsRead $value): void {
        $this->relationships = $value;
    }

}
