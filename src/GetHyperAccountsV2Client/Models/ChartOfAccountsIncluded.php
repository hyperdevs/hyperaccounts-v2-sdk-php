<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class ChartOfAccountsIncluded implements Parsable
{
    /**
     * @var ChartOfAccountsCategoryCollection|null $categories The categories property
    */
    private ?ChartOfAccountsCategoryCollection $categories = null;

    /**
     * @var ChartOfAccountsCategoryTitleCollection|null $categoryTitles The categoryTitles property
    */
    private ?ChartOfAccountsCategoryTitleCollection $categoryTitles = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return ChartOfAccountsIncluded
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): ChartOfAccountsIncluded {
        return new ChartOfAccountsIncluded();
    }

    /**
     * Gets the categories property value. The categories property
     * @return ChartOfAccountsCategoryCollection|null
    */
    public function getCategories(): ?ChartOfAccountsCategoryCollection {
        return $this->categories;
    }

    /**
     * Gets the categoryTitles property value. The categoryTitles property
     * @return ChartOfAccountsCategoryTitleCollection|null
    */
    public function getCategoryTitles(): ?ChartOfAccountsCategoryTitleCollection {
        return $this->categoryTitles;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'categories' => fn(ParseNode $n) => $o->setCategories($n->getObjectValue([ChartOfAccountsCategoryCollection::class, 'createFromDiscriminatorValue'])),
            'categoryTitles' => fn(ParseNode $n) => $o->setCategoryTitles($n->getObjectValue([ChartOfAccountsCategoryTitleCollection::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('categories', $this->getCategories());
        $writer->writeObjectValue('categoryTitles', $this->getCategoryTitles());
    }

    /**
     * Sets the categories property value. The categories property
     * @param ChartOfAccountsCategoryCollection|null $value Value to set for the categories property.
    */
    public function setCategories(?ChartOfAccountsCategoryCollection $value): void {
        $this->categories = $value;
    }

    /**
     * Sets the categoryTitles property value. The categoryTitles property
     * @param ChartOfAccountsCategoryTitleCollection|null $value Value to set for the categoryTitles property.
    */
    public function setCategoryTitles(?ChartOfAccountsCategoryTitleCollection $value): void {
        $this->categoryTitles = $value;
    }

}
