<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use DateTime;
use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class TransactionHeaderAttributesWrite implements Parsable
{
    /**
     * @var string|null $accountRef The accountRef property
    */
    private ?string $accountRef = null;

    /**
     * @var string|null $bankChargeAccount The bankChargeAccount property
    */
    private ?string $bankChargeAccount = null;

    /**
     * @var int|null $bankChargeIsCustomer The bankChargeIsCustomer property
    */
    private ?int $bankChargeIsCustomer = null;

    /**
     * @var string|null $bankCode The bankCode property
    */
    private ?string $bankCode = null;

    /**
     * @var TransactionHeaderAttributesWrite_bankFlag|null $bankFlag The bankFlag property
    */
    private ?TransactionHeaderAttributesWrite_bankFlag $bankFlag = null;

    /**
     * @var int|null $currency The currency property
    */
    private ?int $currency = null;

    /**
     * @var DateTime|null $date The date property
    */
    private ?DateTime $date = null;

    /**
     * @var DateTime|null $dateAmended The dateAmended property
    */
    private ?DateTime $dateAmended = null;

    /**
     * @var int|null $deletedFlag The deletedFlag property
    */
    private ?int $deletedFlag = null;

    /**
     * @var DateTime|null $depositDate The depositDate property
    */
    private ?DateTime $depositDate = null;

    /**
     * @var TransactionHeaderAttributesWrite_depositFlag|null $depositFlag The depositFlag property
    */
    private ?TransactionHeaderAttributesWrite_depositFlag $depositFlag = null;

    /**
     * @var string|null $details The details property
    */
    private ?string $details = null;

    /**
     * @var int|null $disputed The disputed property
    */
    private ?int $disputed = null;

    /**
     * @var float|null $euroGross The euroGross property
    */
    private ?float $euroGross = null;

    /**
     * @var float|null $euroRate The euroRate property
    */
    private ?float $euroRate = null;

    /**
     * @var int|null $financeCharge The financeCharge property
    */
    private ?int $financeCharge = null;

    /**
     * @var float|null $foreignAmountPaid The foreignAmountPaid property
    */
    private ?float $foreignAmountPaid = null;

    /**
     * @var float|null $foreignNetAmount The foreignNetAmount property
    */
    private ?float $foreignNetAmount = null;

    /**
     * @var float|null $foreignRate The foreignRate property
    */
    private ?float $foreignRate = null;

    /**
     * @var float|null $foreignTaxAmount The foreignTaxAmount property
    */
    private ?float $foreignTaxAmount = null;

    /**
     * @var float|null $interestRate The interestRate property
    */
    private ?float $interestRate = null;

    /**
     * @var string|null $invRef The invRef property
    */
    private ?string $invRef = null;

    /**
     * @var float|null $netAmount The netAmount property
    */
    private ?float $netAmount = null;

    /**
     * @var DateTime|null $overriddenClosedLedgerDate The overriddenClosedLedgerDate property
    */
    private ?DateTime $overriddenClosedLedgerDate = null;

    /**
     * @var TransactionHeaderAttributesWrite_paidFlag|null $paidFlag The paidFlag property
    */
    private ?TransactionHeaderAttributesWrite_paidFlag $paidFlag = null;

    /**
     * @var float|null $taxAmount The taxAmount property
    */
    private ?float $taxAmount = null;

    /**
     * @var TransactionHeaderAttributesWrite_type|null $type The type property
    */
    private ?TransactionHeaderAttributesWrite_type $type = null;

    /**
     * @var string|null $userName The userName property
    */
    private ?string $userName = null;

    /**
     * @var string|null $userNameAmended The userNameAmended property
    */
    private ?string $userNameAmended = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return TransactionHeaderAttributesWrite
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): TransactionHeaderAttributesWrite {
        return new TransactionHeaderAttributesWrite();
    }

    /**
     * Gets the accountRef property value. The accountRef property
     * @return string|null
    */
    public function getAccountRef(): ?string {
        return $this->accountRef;
    }

    /**
     * Gets the bankChargeAccount property value. The bankChargeAccount property
     * @return string|null
    */
    public function getBankChargeAccount(): ?string {
        return $this->bankChargeAccount;
    }

    /**
     * Gets the bankChargeIsCustomer property value. The bankChargeIsCustomer property
     * @return int|null
    */
    public function getBankChargeIsCustomer(): ?int {
        return $this->bankChargeIsCustomer;
    }

    /**
     * Gets the bankCode property value. The bankCode property
     * @return string|null
    */
    public function getBankCode(): ?string {
        return $this->bankCode;
    }

    /**
     * Gets the bankFlag property value. The bankFlag property
     * @return TransactionHeaderAttributesWrite_bankFlag|null
    */
    public function getBankFlag(): ?TransactionHeaderAttributesWrite_bankFlag {
        return $this->bankFlag;
    }

    /**
     * Gets the currency property value. The currency property
     * @return int|null
    */
    public function getCurrency(): ?int {
        return $this->currency;
    }

    /**
     * Gets the date property value. The date property
     * @return DateTime|null
    */
    public function getDate(): ?DateTime {
        return $this->date;
    }

    /**
     * Gets the dateAmended property value. The dateAmended property
     * @return DateTime|null
    */
    public function getDateAmended(): ?DateTime {
        return $this->dateAmended;
    }

    /**
     * Gets the deletedFlag property value. The deletedFlag property
     * @return int|null
    */
    public function getDeletedFlag(): ?int {
        return $this->deletedFlag;
    }

    /**
     * Gets the depositDate property value. The depositDate property
     * @return DateTime|null
    */
    public function getDepositDate(): ?DateTime {
        return $this->depositDate;
    }

    /**
     * Gets the depositFlag property value. The depositFlag property
     * @return TransactionHeaderAttributesWrite_depositFlag|null
    */
    public function getDepositFlag(): ?TransactionHeaderAttributesWrite_depositFlag {
        return $this->depositFlag;
    }

    /**
     * Gets the details property value. The details property
     * @return string|null
    */
    public function getDetails(): ?string {
        return $this->details;
    }

    /**
     * Gets the disputed property value. The disputed property
     * @return int|null
    */
    public function getDisputed(): ?int {
        return $this->disputed;
    }

    /**
     * Gets the euroGross property value. The euroGross property
     * @return float|null
    */
    public function getEuroGross(): ?float {
        return $this->euroGross;
    }

    /**
     * Gets the euroRate property value. The euroRate property
     * @return float|null
    */
    public function getEuroRate(): ?float {
        return $this->euroRate;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'accountRef' => fn(ParseNode $n) => $o->setAccountRef($n->getStringValue()),
            'bankChargeAccount' => fn(ParseNode $n) => $o->setBankChargeAccount($n->getStringValue()),
            'bankChargeIsCustomer' => fn(ParseNode $n) => $o->setBankChargeIsCustomer($n->getIntegerValue()),
            'bankCode' => fn(ParseNode $n) => $o->setBankCode($n->getStringValue()),
            'bankFlag' => fn(ParseNode $n) => $o->setBankFlag($n->getEnumValue(TransactionHeaderAttributesWrite_bankFlag::class)),
            'currency' => fn(ParseNode $n) => $o->setCurrency($n->getIntegerValue()),
            'date' => fn(ParseNode $n) => $o->setDate($n->getDateTimeValue()),
            'dateAmended' => fn(ParseNode $n) => $o->setDateAmended($n->getDateTimeValue()),
            'deletedFlag' => fn(ParseNode $n) => $o->setDeletedFlag($n->getIntegerValue()),
            'depositDate' => fn(ParseNode $n) => $o->setDepositDate($n->getDateTimeValue()),
            'depositFlag' => fn(ParseNode $n) => $o->setDepositFlag($n->getEnumValue(TransactionHeaderAttributesWrite_depositFlag::class)),
            'details' => fn(ParseNode $n) => $o->setDetails($n->getStringValue()),
            'disputed' => fn(ParseNode $n) => $o->setDisputed($n->getIntegerValue()),
            'euroGross' => fn(ParseNode $n) => $o->setEuroGross($n->getFloatValue()),
            'euroRate' => fn(ParseNode $n) => $o->setEuroRate($n->getFloatValue()),
            'financeCharge' => fn(ParseNode $n) => $o->setFinanceCharge($n->getIntegerValue()),
            'foreignAmountPaid' => fn(ParseNode $n) => $o->setForeignAmountPaid($n->getFloatValue()),
            'foreignNetAmount' => fn(ParseNode $n) => $o->setForeignNetAmount($n->getFloatValue()),
            'foreignRate' => fn(ParseNode $n) => $o->setForeignRate($n->getFloatValue()),
            'foreignTaxAmount' => fn(ParseNode $n) => $o->setForeignTaxAmount($n->getFloatValue()),
            'interestRate' => fn(ParseNode $n) => $o->setInterestRate($n->getFloatValue()),
            'invRef' => fn(ParseNode $n) => $o->setInvRef($n->getStringValue()),
            'netAmount' => fn(ParseNode $n) => $o->setNetAmount($n->getFloatValue()),
            'overriddenClosedLedgerDate' => fn(ParseNode $n) => $o->setOverriddenClosedLedgerDate($n->getDateTimeValue()),
            'paidFlag' => fn(ParseNode $n) => $o->setPaidFlag($n->getEnumValue(TransactionHeaderAttributesWrite_paidFlag::class)),
            'taxAmount' => fn(ParseNode $n) => $o->setTaxAmount($n->getFloatValue()),
            'type' => fn(ParseNode $n) => $o->setType($n->getEnumValue(TransactionHeaderAttributesWrite_type::class)),
            'userName' => fn(ParseNode $n) => $o->setUserName($n->getStringValue()),
            'userNameAmended' => fn(ParseNode $n) => $o->setUserNameAmended($n->getStringValue()),
        ];
    }

    /**
     * Gets the financeCharge property value. The financeCharge property
     * @return int|null
    */
    public function getFinanceCharge(): ?int {
        return $this->financeCharge;
    }

    /**
     * Gets the foreignAmountPaid property value. The foreignAmountPaid property
     * @return float|null
    */
    public function getForeignAmountPaid(): ?float {
        return $this->foreignAmountPaid;
    }

    /**
     * Gets the foreignNetAmount property value. The foreignNetAmount property
     * @return float|null
    */
    public function getForeignNetAmount(): ?float {
        return $this->foreignNetAmount;
    }

    /**
     * Gets the foreignRate property value. The foreignRate property
     * @return float|null
    */
    public function getForeignRate(): ?float {
        return $this->foreignRate;
    }

    /**
     * Gets the foreignTaxAmount property value. The foreignTaxAmount property
     * @return float|null
    */
    public function getForeignTaxAmount(): ?float {
        return $this->foreignTaxAmount;
    }

    /**
     * Gets the interestRate property value. The interestRate property
     * @return float|null
    */
    public function getInterestRate(): ?float {
        return $this->interestRate;
    }

    /**
     * Gets the invRef property value. The invRef property
     * @return string|null
    */
    public function getInvRef(): ?string {
        return $this->invRef;
    }

    /**
     * Gets the netAmount property value. The netAmount property
     * @return float|null
    */
    public function getNetAmount(): ?float {
        return $this->netAmount;
    }

    /**
     * Gets the overriddenClosedLedgerDate property value. The overriddenClosedLedgerDate property
     * @return DateTime|null
    */
    public function getOverriddenClosedLedgerDate(): ?DateTime {
        return $this->overriddenClosedLedgerDate;
    }

    /**
     * Gets the paidFlag property value. The paidFlag property
     * @return TransactionHeaderAttributesWrite_paidFlag|null
    */
    public function getPaidFlag(): ?TransactionHeaderAttributesWrite_paidFlag {
        return $this->paidFlag;
    }

    /**
     * Gets the taxAmount property value. The taxAmount property
     * @return float|null
    */
    public function getTaxAmount(): ?float {
        return $this->taxAmount;
    }

    /**
     * Gets the type property value. The type property
     * @return TransactionHeaderAttributesWrite_type|null
    */
    public function getType(): ?TransactionHeaderAttributesWrite_type {
        return $this->type;
    }

    /**
     * Gets the userName property value. The userName property
     * @return string|null
    */
    public function getUserName(): ?string {
        return $this->userName;
    }

    /**
     * Gets the userNameAmended property value. The userNameAmended property
     * @return string|null
    */
    public function getUserNameAmended(): ?string {
        return $this->userNameAmended;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeStringValue('accountRef', $this->getAccountRef());
        $writer->writeStringValue('bankChargeAccount', $this->getBankChargeAccount());
        $writer->writeIntegerValue('bankChargeIsCustomer', $this->getBankChargeIsCustomer());
        $writer->writeStringValue('bankCode', $this->getBankCode());
        $writer->writeEnumValue('bankFlag', $this->getBankFlag());
        $writer->writeIntegerValue('currency', $this->getCurrency());
        $writer->writeDateTimeValue('date', $this->getDate());
        $writer->writeDateTimeValue('dateAmended', $this->getDateAmended());
        $writer->writeIntegerValue('deletedFlag', $this->getDeletedFlag());
        $writer->writeDateTimeValue('depositDate', $this->getDepositDate());
        $writer->writeEnumValue('depositFlag', $this->getDepositFlag());
        $writer->writeStringValue('details', $this->getDetails());
        $writer->writeIntegerValue('disputed', $this->getDisputed());
        $writer->writeFloatValue('euroGross', $this->getEuroGross());
        $writer->writeFloatValue('euroRate', $this->getEuroRate());
        $writer->writeIntegerValue('financeCharge', $this->getFinanceCharge());
        $writer->writeFloatValue('foreignAmountPaid', $this->getForeignAmountPaid());
        $writer->writeFloatValue('foreignNetAmount', $this->getForeignNetAmount());
        $writer->writeFloatValue('foreignRate', $this->getForeignRate());
        $writer->writeFloatValue('foreignTaxAmount', $this->getForeignTaxAmount());
        $writer->writeFloatValue('interestRate', $this->getInterestRate());
        $writer->writeStringValue('invRef', $this->getInvRef());
        $writer->writeFloatValue('netAmount', $this->getNetAmount());
        $writer->writeDateTimeValue('overriddenClosedLedgerDate', $this->getOverriddenClosedLedgerDate());
        $writer->writeEnumValue('paidFlag', $this->getPaidFlag());
        $writer->writeFloatValue('taxAmount', $this->getTaxAmount());
        $writer->writeEnumValue('type', $this->getType());
        $writer->writeStringValue('userName', $this->getUserName());
        $writer->writeStringValue('userNameAmended', $this->getUserNameAmended());
    }

    /**
     * Sets the accountRef property value. The accountRef property
     * @param string|null $value Value to set for the accountRef property.
    */
    public function setAccountRef(?string $value): void {
        $this->accountRef = $value;
    }

    /**
     * Sets the bankChargeAccount property value. The bankChargeAccount property
     * @param string|null $value Value to set for the bankChargeAccount property.
    */
    public function setBankChargeAccount(?string $value): void {
        $this->bankChargeAccount = $value;
    }

    /**
     * Sets the bankChargeIsCustomer property value. The bankChargeIsCustomer property
     * @param int|null $value Value to set for the bankChargeIsCustomer property.
    */
    public function setBankChargeIsCustomer(?int $value): void {
        $this->bankChargeIsCustomer = $value;
    }

    /**
     * Sets the bankCode property value. The bankCode property
     * @param string|null $value Value to set for the bankCode property.
    */
    public function setBankCode(?string $value): void {
        $this->bankCode = $value;
    }

    /**
     * Sets the bankFlag property value. The bankFlag property
     * @param TransactionHeaderAttributesWrite_bankFlag|null $value Value to set for the bankFlag property.
    */
    public function setBankFlag(?TransactionHeaderAttributesWrite_bankFlag $value): void {
        $this->bankFlag = $value;
    }

    /**
     * Sets the currency property value. The currency property
     * @param int|null $value Value to set for the currency property.
    */
    public function setCurrency(?int $value): void {
        $this->currency = $value;
    }

    /**
     * Sets the date property value. The date property
     * @param DateTime|null $value Value to set for the date property.
    */
    public function setDate(?DateTime $value): void {
        $this->date = $value;
    }

    /**
     * Sets the dateAmended property value. The dateAmended property
     * @param DateTime|null $value Value to set for the dateAmended property.
    */
    public function setDateAmended(?DateTime $value): void {
        $this->dateAmended = $value;
    }

    /**
     * Sets the deletedFlag property value. The deletedFlag property
     * @param int|null $value Value to set for the deletedFlag property.
    */
    public function setDeletedFlag(?int $value): void {
        $this->deletedFlag = $value;
    }

    /**
     * Sets the depositDate property value. The depositDate property
     * @param DateTime|null $value Value to set for the depositDate property.
    */
    public function setDepositDate(?DateTime $value): void {
        $this->depositDate = $value;
    }

    /**
     * Sets the depositFlag property value. The depositFlag property
     * @param TransactionHeaderAttributesWrite_depositFlag|null $value Value to set for the depositFlag property.
    */
    public function setDepositFlag(?TransactionHeaderAttributesWrite_depositFlag $value): void {
        $this->depositFlag = $value;
    }

    /**
     * Sets the details property value. The details property
     * @param string|null $value Value to set for the details property.
    */
    public function setDetails(?string $value): void {
        $this->details = $value;
    }

    /**
     * Sets the disputed property value. The disputed property
     * @param int|null $value Value to set for the disputed property.
    */
    public function setDisputed(?int $value): void {
        $this->disputed = $value;
    }

    /**
     * Sets the euroGross property value. The euroGross property
     * @param float|null $value Value to set for the euroGross property.
    */
    public function setEuroGross(?float $value): void {
        $this->euroGross = $value;
    }

    /**
     * Sets the euroRate property value. The euroRate property
     * @param float|null $value Value to set for the euroRate property.
    */
    public function setEuroRate(?float $value): void {
        $this->euroRate = $value;
    }

    /**
     * Sets the financeCharge property value. The financeCharge property
     * @param int|null $value Value to set for the financeCharge property.
    */
    public function setFinanceCharge(?int $value): void {
        $this->financeCharge = $value;
    }

    /**
     * Sets the foreignAmountPaid property value. The foreignAmountPaid property
     * @param float|null $value Value to set for the foreignAmountPaid property.
    */
    public function setForeignAmountPaid(?float $value): void {
        $this->foreignAmountPaid = $value;
    }

    /**
     * Sets the foreignNetAmount property value. The foreignNetAmount property
     * @param float|null $value Value to set for the foreignNetAmount property.
    */
    public function setForeignNetAmount(?float $value): void {
        $this->foreignNetAmount = $value;
    }

    /**
     * Sets the foreignRate property value. The foreignRate property
     * @param float|null $value Value to set for the foreignRate property.
    */
    public function setForeignRate(?float $value): void {
        $this->foreignRate = $value;
    }

    /**
     * Sets the foreignTaxAmount property value. The foreignTaxAmount property
     * @param float|null $value Value to set for the foreignTaxAmount property.
    */
    public function setForeignTaxAmount(?float $value): void {
        $this->foreignTaxAmount = $value;
    }

    /**
     * Sets the interestRate property value. The interestRate property
     * @param float|null $value Value to set for the interestRate property.
    */
    public function setInterestRate(?float $value): void {
        $this->interestRate = $value;
    }

    /**
     * Sets the invRef property value. The invRef property
     * @param string|null $value Value to set for the invRef property.
    */
    public function setInvRef(?string $value): void {
        $this->invRef = $value;
    }

    /**
     * Sets the netAmount property value. The netAmount property
     * @param float|null $value Value to set for the netAmount property.
    */
    public function setNetAmount(?float $value): void {
        $this->netAmount = $value;
    }

    /**
     * Sets the overriddenClosedLedgerDate property value. The overriddenClosedLedgerDate property
     * @param DateTime|null $value Value to set for the overriddenClosedLedgerDate property.
    */
    public function setOverriddenClosedLedgerDate(?DateTime $value): void {
        $this->overriddenClosedLedgerDate = $value;
    }

    /**
     * Sets the paidFlag property value. The paidFlag property
     * @param TransactionHeaderAttributesWrite_paidFlag|null $value Value to set for the paidFlag property.
    */
    public function setPaidFlag(?TransactionHeaderAttributesWrite_paidFlag $value): void {
        $this->paidFlag = $value;
    }

    /**
     * Sets the taxAmount property value. The taxAmount property
     * @param float|null $value Value to set for the taxAmount property.
    */
    public function setTaxAmount(?float $value): void {
        $this->taxAmount = $value;
    }

    /**
     * Sets the type property value. The type property
     * @param TransactionHeaderAttributesWrite_type|null $value Value to set for the type property.
    */
    public function setType(?TransactionHeaderAttributesWrite_type $value): void {
        $this->type = $value;
    }

    /**
     * Sets the userName property value. The userName property
     * @param string|null $value Value to set for the userName property.
    */
    public function setUserName(?string $value): void {
        $this->userName = $value;
    }

    /**
     * Sets the userNameAmended property value. The userNameAmended property
     * @param string|null $value Value to set for the userNameAmended property.
    */
    public function setUserNameAmended(?string $value): void {
        $this->userNameAmended = $value;
    }

}
