<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use DateTime;
use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class SupplierAttributesEdit implements Parsable
{
    /**
     * @var int|null $accountOnHold The accountOnHold property
    */
    private ?int $accountOnHold = null;

    /**
     * @var string|null $analysis1 The analysis1 property
    */
    private ?string $analysis1 = null;

    /**
     * @var string|null $analysis2 The analysis2 property
    */
    private ?string $analysis2 = null;

    /**
     * @var string|null $analysis3 The analysis3 property
    */
    private ?string $analysis3 = null;

    /**
     * @var float|null $balance The balance property
    */
    private ?float $balance = null;

    /**
     * @var string|null $bankAccountName The bankAccountName property
    */
    private ?string $bankAccountName = null;

    /**
     * @var string|null $bankAccountNumber The bankAccountNumber property
    */
    private ?string $bankAccountNumber = null;

    /**
     * @var string|null $bankAddress1 The bankAddress1 property
    */
    private ?string $bankAddress1 = null;

    /**
     * @var string|null $bankAddress2 The bankAddress2 property
    */
    private ?string $bankAddress2 = null;

    /**
     * @var string|null $bankAddress3 The bankAddress3 property
    */
    private ?string $bankAddress3 = null;

    /**
     * @var string|null $bankAddress4 The bankAddress4 property
    */
    private ?string $bankAddress4 = null;

    /**
     * @var string|null $bankAddress5 The bankAddress5 property
    */
    private ?string $bankAddress5 = null;

    /**
     * @var string|null $bankIban The bankIban property
    */
    private ?string $bankIban = null;

    /**
     * @var string|null $bankName The bankName property
    */
    private ?string $bankName = null;

    /**
     * @var string|null $bankSortCode The bankSortCode property
    */
    private ?string $bankSortCode = null;

    /**
     * @var string|null $contactName The contactName property
    */
    private ?string $contactName = null;

    /**
     * @var string|null $countryCode The countryCode property
    */
    private ?string $countryCode = null;

    /**
     * @var float|null $creditBf The creditBf property
    */
    private ?float $creditBf = null;

    /**
     * @var float|null $creditCf The creditCf property
    */
    private ?float $creditCf = null;

    /**
     * @var float|null $creditLimit The creditLimit property
    */
    private ?float $creditLimit = null;

    /**
     * @var float|null $creditMth1 The creditMth1 property
    */
    private ?float $creditMth1 = null;

    /**
     * @var float|null $creditMth10 The creditMth10 property
    */
    private ?float $creditMth10 = null;

    /**
     * @var float|null $creditMth11 The creditMth11 property
    */
    private ?float $creditMth11 = null;

    /**
     * @var float|null $creditMth12 The creditMth12 property
    */
    private ?float $creditMth12 = null;

    /**
     * @var float|null $creditMth2 The creditMth2 property
    */
    private ?float $creditMth2 = null;

    /**
     * @var float|null $creditMth3 The creditMth3 property
    */
    private ?float $creditMth3 = null;

    /**
     * @var float|null $creditMth4 The creditMth4 property
    */
    private ?float $creditMth4 = null;

    /**
     * @var float|null $creditMth5 The creditMth5 property
    */
    private ?float $creditMth5 = null;

    /**
     * @var float|null $creditMth6 The creditMth6 property
    */
    private ?float $creditMth6 = null;

    /**
     * @var float|null $creditMth7 The creditMth7 property
    */
    private ?float $creditMth7 = null;

    /**
     * @var float|null $creditMth8 The creditMth8 property
    */
    private ?float $creditMth8 = null;

    /**
     * @var float|null $creditMth9 The creditMth9 property
    */
    private ?float $creditMth9 = null;

    /**
     * @var SupplierAttributesEdit_creditPosition|null $creditPosition The creditPosition property
    */
    private ?SupplierAttributesEdit_creditPosition $creditPosition = null;

    /**
     * @var int|null $currency The currency property
    */
    private ?int $currency = null;

    /**
     * @var DateTime|null $declarationValidFrom The declarationValidFrom property
    */
    private ?DateTime $declarationValidFrom = null;

    /**
     * @var string|null $defNomCode The defNomCode property
    */
    private ?string $defNomCode = null;

    /**
     * @var SupplierAttributesEdit_defTaxCode|null $defTaxCode The defTaxCode property
    */
    private ?SupplierAttributesEdit_defTaxCode $defTaxCode = null;

    /**
     * @var int|null $deptNumber The deptNumber property
    */
    private ?int $deptNumber = null;

    /**
     * @var float|null $discountRate The discountRate property
    */
    private ?float $discountRate = null;

    /**
     * @var string|null $donorForename The donorForename property
    */
    private ?string $donorForename = null;

    /**
     * @var string|null $donorSurname The donorSurname property
    */
    private ?string $donorSurname = null;

    /**
     * @var string|null $donorTitle The donorTitle property
    */
    private ?string $donorTitle = null;

    /**
     * @var string|null $dunsNumber The dunsNumber property
    */
    private ?string $dunsNumber = null;

    /**
     * @var string|null $eMail The eMail property
    */
    private ?string $eMail = null;

    /**
     * @var string|null $eMail2 The eMail2 property
    */
    private ?string $eMail2 = null;

    /**
     * @var string|null $eMail3 The eMail3 property
    */
    private ?string $eMail3 = null;

    /**
     * @var string|null $eoriNumber The eoriNumber property
    */
    private ?string $eoriNumber = null;

    /**
     * @var string|null $fax The fax property
    */
    private ?string $fax = null;

    /**
     * @var int|null $inactiveFlag The inactiveFlag property
    */
    private ?int $inactiveFlag = null;

    /**
     * @var float|null $invoiceBf The invoiceBf property
    */
    private ?float $invoiceBf = null;

    /**
     * @var float|null $invoiceCf The invoiceCf property
    */
    private ?float $invoiceCf = null;

    /**
     * @var float|null $invoiceMth1 The invoiceMth1 property
    */
    private ?float $invoiceMth1 = null;

    /**
     * @var float|null $invoiceMth10 The invoiceMth10 property
    */
    private ?float $invoiceMth10 = null;

    /**
     * @var float|null $invoiceMth11 The invoiceMth11 property
    */
    private ?float $invoiceMth11 = null;

    /**
     * @var float|null $invoiceMth12 The invoiceMth12 property
    */
    private ?float $invoiceMth12 = null;

    /**
     * @var float|null $invoiceMth2 The invoiceMth2 property
    */
    private ?float $invoiceMth2 = null;

    /**
     * @var float|null $invoiceMth3 The invoiceMth3 property
    */
    private ?float $invoiceMth3 = null;

    /**
     * @var float|null $invoiceMth4 The invoiceMth4 property
    */
    private ?float $invoiceMth4 = null;

    /**
     * @var float|null $invoiceMth5 The invoiceMth5 property
    */
    private ?float $invoiceMth5 = null;

    /**
     * @var float|null $invoiceMth6 The invoiceMth6 property
    */
    private ?float $invoiceMth6 = null;

    /**
     * @var float|null $invoiceMth7 The invoiceMth7 property
    */
    private ?float $invoiceMth7 = null;

    /**
     * @var float|null $invoiceMth8 The invoiceMth8 property
    */
    private ?float $invoiceMth8 = null;

    /**
     * @var float|null $invoiceMth9 The invoiceMth9 property
    */
    private ?float $invoiceMth9 = null;

    /**
     * @var DateTime|null $lastInvDate The lastInvDate property
    */
    private ?DateTime $lastInvDate = null;

    /**
     * @var string|null $name The name property
    */
    private ?string $name = null;

    /**
     * @var float|null $paymentBf The paymentBf property
    */
    private ?float $paymentBf = null;

    /**
     * @var float|null $paymentCf The paymentCf property
    */
    private ?float $paymentCf = null;

    /**
     * @var int|null $paymentDueDays The paymentDueDays property
    */
    private ?int $paymentDueDays = null;

    /**
     * @var int|null $paymentDueFrom The paymentDueFrom property
    */
    private ?int $paymentDueFrom = null;

    /**
     * @var float|null $paymentMth1 The paymentMth1 property
    */
    private ?float $paymentMth1 = null;

    /**
     * @var float|null $paymentMth10 The paymentMth10 property
    */
    private ?float $paymentMth10 = null;

    /**
     * @var float|null $paymentMth11 The paymentMth11 property
    */
    private ?float $paymentMth11 = null;

    /**
     * @var float|null $paymentMth12 The paymentMth12 property
    */
    private ?float $paymentMth12 = null;

    /**
     * @var float|null $paymentMth2 The paymentMth2 property
    */
    private ?float $paymentMth2 = null;

    /**
     * @var float|null $paymentMth3 The paymentMth3 property
    */
    private ?float $paymentMth3 = null;

    /**
     * @var float|null $paymentMth4 The paymentMth4 property
    */
    private ?float $paymentMth4 = null;

    /**
     * @var float|null $paymentMth5 The paymentMth5 property
    */
    private ?float $paymentMth5 = null;

    /**
     * @var float|null $paymentMth6 The paymentMth6 property
    */
    private ?float $paymentMth6 = null;

    /**
     * @var float|null $paymentMth7 The paymentMth7 property
    */
    private ?float $paymentMth7 = null;

    /**
     * @var float|null $paymentMth8 The paymentMth8 property
    */
    private ?float $paymentMth8 = null;

    /**
     * @var float|null $paymentMth9 The paymentMth9 property
    */
    private ?float $paymentMth9 = null;

    /**
     * @var float|null $priorYear The priorYear property
    */
    private ?float $priorYear = null;

    /**
     * @var float|null $settlementDiscRate The settlementDiscRate property
    */
    private ?float $settlementDiscRate = null;

    /**
     * @var int|null $settlementDueDays The settlementDueDays property
    */
    private ?int $settlementDueDays = null;

    /**
     * @var string|null $telephone The telephone property
    */
    private ?string $telephone = null;

    /**
     * @var string|null $telephone2 The telephone2 property
    */
    private ?string $telephone2 = null;

    /**
     * @var string|null $terms The terms property
    */
    private ?string $terms = null;

    /**
     * @var string|null $tradeContact The tradeContact property
    */
    private ?string $tradeContact = null;

    /**
     * @var float|null $turnoverMtd The turnoverMtd property
    */
    private ?float $turnoverMtd = null;

    /**
     * @var float|null $turnoverYtd The turnoverYtd property
    */
    private ?float $turnoverYtd = null;

    /**
     * @var string|null $vatRegNumber The vatRegNumber property
    */
    private ?string $vatRegNumber = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return SupplierAttributesEdit
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): SupplierAttributesEdit {
        return new SupplierAttributesEdit();
    }

    /**
     * Gets the accountOnHold property value. The accountOnHold property
     * @return int|null
    */
    public function getAccountOnHold(): ?int {
        return $this->accountOnHold;
    }

    /**
     * Gets the analysis1 property value. The analysis1 property
     * @return string|null
    */
    public function getAnalysis1(): ?string {
        return $this->analysis1;
    }

    /**
     * Gets the analysis2 property value. The analysis2 property
     * @return string|null
    */
    public function getAnalysis2(): ?string {
        return $this->analysis2;
    }

    /**
     * Gets the analysis3 property value. The analysis3 property
     * @return string|null
    */
    public function getAnalysis3(): ?string {
        return $this->analysis3;
    }

    /**
     * Gets the balance property value. The balance property
     * @return float|null
    */
    public function getBalance(): ?float {
        return $this->balance;
    }

    /**
     * Gets the bankAccountName property value. The bankAccountName property
     * @return string|null
    */
    public function getBankAccountName(): ?string {
        return $this->bankAccountName;
    }

    /**
     * Gets the bankAccountNumber property value. The bankAccountNumber property
     * @return string|null
    */
    public function getBankAccountNumber(): ?string {
        return $this->bankAccountNumber;
    }

    /**
     * Gets the bankAddress1 property value. The bankAddress1 property
     * @return string|null
    */
    public function getBankAddress1(): ?string {
        return $this->bankAddress1;
    }

    /**
     * Gets the bankAddress2 property value. The bankAddress2 property
     * @return string|null
    */
    public function getBankAddress2(): ?string {
        return $this->bankAddress2;
    }

    /**
     * Gets the bankAddress3 property value. The bankAddress3 property
     * @return string|null
    */
    public function getBankAddress3(): ?string {
        return $this->bankAddress3;
    }

    /**
     * Gets the bankAddress4 property value. The bankAddress4 property
     * @return string|null
    */
    public function getBankAddress4(): ?string {
        return $this->bankAddress4;
    }

    /**
     * Gets the bankAddress5 property value. The bankAddress5 property
     * @return string|null
    */
    public function getBankAddress5(): ?string {
        return $this->bankAddress5;
    }

    /**
     * Gets the bankIban property value. The bankIban property
     * @return string|null
    */
    public function getBankIban(): ?string {
        return $this->bankIban;
    }

    /**
     * Gets the bankName property value. The bankName property
     * @return string|null
    */
    public function getBankName(): ?string {
        return $this->bankName;
    }

    /**
     * Gets the bankSortCode property value. The bankSortCode property
     * @return string|null
    */
    public function getBankSortCode(): ?string {
        return $this->bankSortCode;
    }

    /**
     * Gets the contactName property value. The contactName property
     * @return string|null
    */
    public function getContactName(): ?string {
        return $this->contactName;
    }

    /**
     * Gets the countryCode property value. The countryCode property
     * @return string|null
    */
    public function getCountryCode(): ?string {
        return $this->countryCode;
    }

    /**
     * Gets the creditBf property value. The creditBf property
     * @return float|null
    */
    public function getCreditBf(): ?float {
        return $this->creditBf;
    }

    /**
     * Gets the creditCf property value. The creditCf property
     * @return float|null
    */
    public function getCreditCf(): ?float {
        return $this->creditCf;
    }

    /**
     * Gets the creditLimit property value. The creditLimit property
     * @return float|null
    */
    public function getCreditLimit(): ?float {
        return $this->creditLimit;
    }

    /**
     * Gets the creditMth1 property value. The creditMth1 property
     * @return float|null
    */
    public function getCreditMth1(): ?float {
        return $this->creditMth1;
    }

    /**
     * Gets the creditMth10 property value. The creditMth10 property
     * @return float|null
    */
    public function getCreditMth10(): ?float {
        return $this->creditMth10;
    }

    /**
     * Gets the creditMth11 property value. The creditMth11 property
     * @return float|null
    */
    public function getCreditMth11(): ?float {
        return $this->creditMth11;
    }

    /**
     * Gets the creditMth12 property value. The creditMth12 property
     * @return float|null
    */
    public function getCreditMth12(): ?float {
        return $this->creditMth12;
    }

    /**
     * Gets the creditMth2 property value. The creditMth2 property
     * @return float|null
    */
    public function getCreditMth2(): ?float {
        return $this->creditMth2;
    }

    /**
     * Gets the creditMth3 property value. The creditMth3 property
     * @return float|null
    */
    public function getCreditMth3(): ?float {
        return $this->creditMth3;
    }

    /**
     * Gets the creditMth4 property value. The creditMth4 property
     * @return float|null
    */
    public function getCreditMth4(): ?float {
        return $this->creditMth4;
    }

    /**
     * Gets the creditMth5 property value. The creditMth5 property
     * @return float|null
    */
    public function getCreditMth5(): ?float {
        return $this->creditMth5;
    }

    /**
     * Gets the creditMth6 property value. The creditMth6 property
     * @return float|null
    */
    public function getCreditMth6(): ?float {
        return $this->creditMth6;
    }

    /**
     * Gets the creditMth7 property value. The creditMth7 property
     * @return float|null
    */
    public function getCreditMth7(): ?float {
        return $this->creditMth7;
    }

    /**
     * Gets the creditMth8 property value. The creditMth8 property
     * @return float|null
    */
    public function getCreditMth8(): ?float {
        return $this->creditMth8;
    }

    /**
     * Gets the creditMth9 property value. The creditMth9 property
     * @return float|null
    */
    public function getCreditMth9(): ?float {
        return $this->creditMth9;
    }

    /**
     * Gets the creditPosition property value. The creditPosition property
     * @return SupplierAttributesEdit_creditPosition|null
    */
    public function getCreditPosition(): ?SupplierAttributesEdit_creditPosition {
        return $this->creditPosition;
    }

    /**
     * Gets the currency property value. The currency property
     * @return int|null
    */
    public function getCurrency(): ?int {
        return $this->currency;
    }

    /**
     * Gets the declarationValidFrom property value. The declarationValidFrom property
     * @return DateTime|null
    */
    public function getDeclarationValidFrom(): ?DateTime {
        return $this->declarationValidFrom;
    }

    /**
     * Gets the defNomCode property value. The defNomCode property
     * @return string|null
    */
    public function getDefNomCode(): ?string {
        return $this->defNomCode;
    }

    /**
     * Gets the defTaxCode property value. The defTaxCode property
     * @return SupplierAttributesEdit_defTaxCode|null
    */
    public function getDefTaxCode(): ?SupplierAttributesEdit_defTaxCode {
        return $this->defTaxCode;
    }

    /**
     * Gets the deptNumber property value. The deptNumber property
     * @return int|null
    */
    public function getDeptNumber(): ?int {
        return $this->deptNumber;
    }

    /**
     * Gets the discountRate property value. The discountRate property
     * @return float|null
    */
    public function getDiscountRate(): ?float {
        return $this->discountRate;
    }

    /**
     * Gets the donorForename property value. The donorForename property
     * @return string|null
    */
    public function getDonorForename(): ?string {
        return $this->donorForename;
    }

    /**
     * Gets the donorSurname property value. The donorSurname property
     * @return string|null
    */
    public function getDonorSurname(): ?string {
        return $this->donorSurname;
    }

    /**
     * Gets the donorTitle property value. The donorTitle property
     * @return string|null
    */
    public function getDonorTitle(): ?string {
        return $this->donorTitle;
    }

    /**
     * Gets the dunsNumber property value. The dunsNumber property
     * @return string|null
    */
    public function getDunsNumber(): ?string {
        return $this->dunsNumber;
    }

    /**
     * Gets the eMail property value. The eMail property
     * @return string|null
    */
    public function getEMail(): ?string {
        return $this->eMail;
    }

    /**
     * Gets the eMail2 property value. The eMail2 property
     * @return string|null
    */
    public function getEMail2(): ?string {
        return $this->eMail2;
    }

    /**
     * Gets the eMail3 property value. The eMail3 property
     * @return string|null
    */
    public function getEMail3(): ?string {
        return $this->eMail3;
    }

    /**
     * Gets the eoriNumber property value. The eoriNumber property
     * @return string|null
    */
    public function getEoriNumber(): ?string {
        return $this->eoriNumber;
    }

    /**
     * Gets the fax property value. The fax property
     * @return string|null
    */
    public function getFax(): ?string {
        return $this->fax;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'accountOnHold' => fn(ParseNode $n) => $o->setAccountOnHold($n->getIntegerValue()),
            'analysis1' => fn(ParseNode $n) => $o->setAnalysis1($n->getStringValue()),
            'analysis2' => fn(ParseNode $n) => $o->setAnalysis2($n->getStringValue()),
            'analysis3' => fn(ParseNode $n) => $o->setAnalysis3($n->getStringValue()),
            'balance' => fn(ParseNode $n) => $o->setBalance($n->getFloatValue()),
            'bankAccountName' => fn(ParseNode $n) => $o->setBankAccountName($n->getStringValue()),
            'bankAccountNumber' => fn(ParseNode $n) => $o->setBankAccountNumber($n->getStringValue()),
            'bankAddress1' => fn(ParseNode $n) => $o->setBankAddress1($n->getStringValue()),
            'bankAddress2' => fn(ParseNode $n) => $o->setBankAddress2($n->getStringValue()),
            'bankAddress3' => fn(ParseNode $n) => $o->setBankAddress3($n->getStringValue()),
            'bankAddress4' => fn(ParseNode $n) => $o->setBankAddress4($n->getStringValue()),
            'bankAddress5' => fn(ParseNode $n) => $o->setBankAddress5($n->getStringValue()),
            'bankIban' => fn(ParseNode $n) => $o->setBankIban($n->getStringValue()),
            'bankName' => fn(ParseNode $n) => $o->setBankName($n->getStringValue()),
            'bankSortCode' => fn(ParseNode $n) => $o->setBankSortCode($n->getStringValue()),
            'contactName' => fn(ParseNode $n) => $o->setContactName($n->getStringValue()),
            'countryCode' => fn(ParseNode $n) => $o->setCountryCode($n->getStringValue()),
            'creditBf' => fn(ParseNode $n) => $o->setCreditBf($n->getFloatValue()),
            'creditCf' => fn(ParseNode $n) => $o->setCreditCf($n->getFloatValue()),
            'creditLimit' => fn(ParseNode $n) => $o->setCreditLimit($n->getFloatValue()),
            'creditMth1' => fn(ParseNode $n) => $o->setCreditMth1($n->getFloatValue()),
            'creditMth10' => fn(ParseNode $n) => $o->setCreditMth10($n->getFloatValue()),
            'creditMth11' => fn(ParseNode $n) => $o->setCreditMth11($n->getFloatValue()),
            'creditMth12' => fn(ParseNode $n) => $o->setCreditMth12($n->getFloatValue()),
            'creditMth2' => fn(ParseNode $n) => $o->setCreditMth2($n->getFloatValue()),
            'creditMth3' => fn(ParseNode $n) => $o->setCreditMth3($n->getFloatValue()),
            'creditMth4' => fn(ParseNode $n) => $o->setCreditMth4($n->getFloatValue()),
            'creditMth5' => fn(ParseNode $n) => $o->setCreditMth5($n->getFloatValue()),
            'creditMth6' => fn(ParseNode $n) => $o->setCreditMth6($n->getFloatValue()),
            'creditMth7' => fn(ParseNode $n) => $o->setCreditMth7($n->getFloatValue()),
            'creditMth8' => fn(ParseNode $n) => $o->setCreditMth8($n->getFloatValue()),
            'creditMth9' => fn(ParseNode $n) => $o->setCreditMth9($n->getFloatValue()),
            'creditPosition' => fn(ParseNode $n) => $o->setCreditPosition($n->getEnumValue(SupplierAttributesEdit_creditPosition::class)),
            'currency' => fn(ParseNode $n) => $o->setCurrency($n->getIntegerValue()),
            'declarationValidFrom' => fn(ParseNode $n) => $o->setDeclarationValidFrom($n->getDateTimeValue()),
            'defNomCode' => fn(ParseNode $n) => $o->setDefNomCode($n->getStringValue()),
            'defTaxCode' => fn(ParseNode $n) => $o->setDefTaxCode($n->getEnumValue(SupplierAttributesEdit_defTaxCode::class)),
            'deptNumber' => fn(ParseNode $n) => $o->setDeptNumber($n->getIntegerValue()),
            'discountRate' => fn(ParseNode $n) => $o->setDiscountRate($n->getFloatValue()),
            'donorForename' => fn(ParseNode $n) => $o->setDonorForename($n->getStringValue()),
            'donorSurname' => fn(ParseNode $n) => $o->setDonorSurname($n->getStringValue()),
            'donorTitle' => fn(ParseNode $n) => $o->setDonorTitle($n->getStringValue()),
            'dunsNumber' => fn(ParseNode $n) => $o->setDunsNumber($n->getStringValue()),
            'eMail' => fn(ParseNode $n) => $o->setEMail($n->getStringValue()),
            'eMail2' => fn(ParseNode $n) => $o->setEMail2($n->getStringValue()),
            'eMail3' => fn(ParseNode $n) => $o->setEMail3($n->getStringValue()),
            'eoriNumber' => fn(ParseNode $n) => $o->setEoriNumber($n->getStringValue()),
            'fax' => fn(ParseNode $n) => $o->setFax($n->getStringValue()),
            'inactiveFlag' => fn(ParseNode $n) => $o->setInactiveFlag($n->getIntegerValue()),
            'invoiceBf' => fn(ParseNode $n) => $o->setInvoiceBf($n->getFloatValue()),
            'invoiceCf' => fn(ParseNode $n) => $o->setInvoiceCf($n->getFloatValue()),
            'invoiceMth1' => fn(ParseNode $n) => $o->setInvoiceMth1($n->getFloatValue()),
            'invoiceMth10' => fn(ParseNode $n) => $o->setInvoiceMth10($n->getFloatValue()),
            'invoiceMth11' => fn(ParseNode $n) => $o->setInvoiceMth11($n->getFloatValue()),
            'invoiceMth12' => fn(ParseNode $n) => $o->setInvoiceMth12($n->getFloatValue()),
            'invoiceMth2' => fn(ParseNode $n) => $o->setInvoiceMth2($n->getFloatValue()),
            'invoiceMth3' => fn(ParseNode $n) => $o->setInvoiceMth3($n->getFloatValue()),
            'invoiceMth4' => fn(ParseNode $n) => $o->setInvoiceMth4($n->getFloatValue()),
            'invoiceMth5' => fn(ParseNode $n) => $o->setInvoiceMth5($n->getFloatValue()),
            'invoiceMth6' => fn(ParseNode $n) => $o->setInvoiceMth6($n->getFloatValue()),
            'invoiceMth7' => fn(ParseNode $n) => $o->setInvoiceMth7($n->getFloatValue()),
            'invoiceMth8' => fn(ParseNode $n) => $o->setInvoiceMth8($n->getFloatValue()),
            'invoiceMth9' => fn(ParseNode $n) => $o->setInvoiceMth9($n->getFloatValue()),
            'lastInvDate' => fn(ParseNode $n) => $o->setLastInvDate($n->getDateTimeValue()),
            'name' => fn(ParseNode $n) => $o->setName($n->getStringValue()),
            'paymentBf' => fn(ParseNode $n) => $o->setPaymentBf($n->getFloatValue()),
            'paymentCf' => fn(ParseNode $n) => $o->setPaymentCf($n->getFloatValue()),
            'paymentDueDays' => fn(ParseNode $n) => $o->setPaymentDueDays($n->getIntegerValue()),
            'paymentDueFrom' => fn(ParseNode $n) => $o->setPaymentDueFrom($n->getIntegerValue()),
            'paymentMth1' => fn(ParseNode $n) => $o->setPaymentMth1($n->getFloatValue()),
            'paymentMth10' => fn(ParseNode $n) => $o->setPaymentMth10($n->getFloatValue()),
            'paymentMth11' => fn(ParseNode $n) => $o->setPaymentMth11($n->getFloatValue()),
            'paymentMth12' => fn(ParseNode $n) => $o->setPaymentMth12($n->getFloatValue()),
            'paymentMth2' => fn(ParseNode $n) => $o->setPaymentMth2($n->getFloatValue()),
            'paymentMth3' => fn(ParseNode $n) => $o->setPaymentMth3($n->getFloatValue()),
            'paymentMth4' => fn(ParseNode $n) => $o->setPaymentMth4($n->getFloatValue()),
            'paymentMth5' => fn(ParseNode $n) => $o->setPaymentMth5($n->getFloatValue()),
            'paymentMth6' => fn(ParseNode $n) => $o->setPaymentMth6($n->getFloatValue()),
            'paymentMth7' => fn(ParseNode $n) => $o->setPaymentMth7($n->getFloatValue()),
            'paymentMth8' => fn(ParseNode $n) => $o->setPaymentMth8($n->getFloatValue()),
            'paymentMth9' => fn(ParseNode $n) => $o->setPaymentMth9($n->getFloatValue()),
            'priorYear' => fn(ParseNode $n) => $o->setPriorYear($n->getFloatValue()),
            'settlementDiscRate' => fn(ParseNode $n) => $o->setSettlementDiscRate($n->getFloatValue()),
            'settlementDueDays' => fn(ParseNode $n) => $o->setSettlementDueDays($n->getIntegerValue()),
            'telephone' => fn(ParseNode $n) => $o->setTelephone($n->getStringValue()),
            'telephone2' => fn(ParseNode $n) => $o->setTelephone2($n->getStringValue()),
            'terms' => fn(ParseNode $n) => $o->setTerms($n->getStringValue()),
            'tradeContact' => fn(ParseNode $n) => $o->setTradeContact($n->getStringValue()),
            'turnoverMtd' => fn(ParseNode $n) => $o->setTurnoverMtd($n->getFloatValue()),
            'turnoverYtd' => fn(ParseNode $n) => $o->setTurnoverYtd($n->getFloatValue()),
            'vatRegNumber' => fn(ParseNode $n) => $o->setVatRegNumber($n->getStringValue()),
        ];
    }

    /**
     * Gets the inactiveFlag property value. The inactiveFlag property
     * @return int|null
    */
    public function getInactiveFlag(): ?int {
        return $this->inactiveFlag;
    }

    /**
     * Gets the invoiceBf property value. The invoiceBf property
     * @return float|null
    */
    public function getInvoiceBf(): ?float {
        return $this->invoiceBf;
    }

    /**
     * Gets the invoiceCf property value. The invoiceCf property
     * @return float|null
    */
    public function getInvoiceCf(): ?float {
        return $this->invoiceCf;
    }

    /**
     * Gets the invoiceMth1 property value. The invoiceMth1 property
     * @return float|null
    */
    public function getInvoiceMth1(): ?float {
        return $this->invoiceMth1;
    }

    /**
     * Gets the invoiceMth10 property value. The invoiceMth10 property
     * @return float|null
    */
    public function getInvoiceMth10(): ?float {
        return $this->invoiceMth10;
    }

    /**
     * Gets the invoiceMth11 property value. The invoiceMth11 property
     * @return float|null
    */
    public function getInvoiceMth11(): ?float {
        return $this->invoiceMth11;
    }

    /**
     * Gets the invoiceMth12 property value. The invoiceMth12 property
     * @return float|null
    */
    public function getInvoiceMth12(): ?float {
        return $this->invoiceMth12;
    }

    /**
     * Gets the invoiceMth2 property value. The invoiceMth2 property
     * @return float|null
    */
    public function getInvoiceMth2(): ?float {
        return $this->invoiceMth2;
    }

    /**
     * Gets the invoiceMth3 property value. The invoiceMth3 property
     * @return float|null
    */
    public function getInvoiceMth3(): ?float {
        return $this->invoiceMth3;
    }

    /**
     * Gets the invoiceMth4 property value. The invoiceMth4 property
     * @return float|null
    */
    public function getInvoiceMth4(): ?float {
        return $this->invoiceMth4;
    }

    /**
     * Gets the invoiceMth5 property value. The invoiceMth5 property
     * @return float|null
    */
    public function getInvoiceMth5(): ?float {
        return $this->invoiceMth5;
    }

    /**
     * Gets the invoiceMth6 property value. The invoiceMth6 property
     * @return float|null
    */
    public function getInvoiceMth6(): ?float {
        return $this->invoiceMth6;
    }

    /**
     * Gets the invoiceMth7 property value. The invoiceMth7 property
     * @return float|null
    */
    public function getInvoiceMth7(): ?float {
        return $this->invoiceMth7;
    }

    /**
     * Gets the invoiceMth8 property value. The invoiceMth8 property
     * @return float|null
    */
    public function getInvoiceMth8(): ?float {
        return $this->invoiceMth8;
    }

    /**
     * Gets the invoiceMth9 property value. The invoiceMth9 property
     * @return float|null
    */
    public function getInvoiceMth9(): ?float {
        return $this->invoiceMth9;
    }

    /**
     * Gets the lastInvDate property value. The lastInvDate property
     * @return DateTime|null
    */
    public function getLastInvDate(): ?DateTime {
        return $this->lastInvDate;
    }

    /**
     * Gets the name property value. The name property
     * @return string|null
    */
    public function getName(): ?string {
        return $this->name;
    }

    /**
     * Gets the paymentBf property value. The paymentBf property
     * @return float|null
    */
    public function getPaymentBf(): ?float {
        return $this->paymentBf;
    }

    /**
     * Gets the paymentCf property value. The paymentCf property
     * @return float|null
    */
    public function getPaymentCf(): ?float {
        return $this->paymentCf;
    }

    /**
     * Gets the paymentDueDays property value. The paymentDueDays property
     * @return int|null
    */
    public function getPaymentDueDays(): ?int {
        return $this->paymentDueDays;
    }

    /**
     * Gets the paymentDueFrom property value. The paymentDueFrom property
     * @return int|null
    */
    public function getPaymentDueFrom(): ?int {
        return $this->paymentDueFrom;
    }

    /**
     * Gets the paymentMth1 property value. The paymentMth1 property
     * @return float|null
    */
    public function getPaymentMth1(): ?float {
        return $this->paymentMth1;
    }

    /**
     * Gets the paymentMth10 property value. The paymentMth10 property
     * @return float|null
    */
    public function getPaymentMth10(): ?float {
        return $this->paymentMth10;
    }

    /**
     * Gets the paymentMth11 property value. The paymentMth11 property
     * @return float|null
    */
    public function getPaymentMth11(): ?float {
        return $this->paymentMth11;
    }

    /**
     * Gets the paymentMth12 property value. The paymentMth12 property
     * @return float|null
    */
    public function getPaymentMth12(): ?float {
        return $this->paymentMth12;
    }

    /**
     * Gets the paymentMth2 property value. The paymentMth2 property
     * @return float|null
    */
    public function getPaymentMth2(): ?float {
        return $this->paymentMth2;
    }

    /**
     * Gets the paymentMth3 property value. The paymentMth3 property
     * @return float|null
    */
    public function getPaymentMth3(): ?float {
        return $this->paymentMth3;
    }

    /**
     * Gets the paymentMth4 property value. The paymentMth4 property
     * @return float|null
    */
    public function getPaymentMth4(): ?float {
        return $this->paymentMth4;
    }

    /**
     * Gets the paymentMth5 property value. The paymentMth5 property
     * @return float|null
    */
    public function getPaymentMth5(): ?float {
        return $this->paymentMth5;
    }

    /**
     * Gets the paymentMth6 property value. The paymentMth6 property
     * @return float|null
    */
    public function getPaymentMth6(): ?float {
        return $this->paymentMth6;
    }

    /**
     * Gets the paymentMth7 property value. The paymentMth7 property
     * @return float|null
    */
    public function getPaymentMth7(): ?float {
        return $this->paymentMth7;
    }

    /**
     * Gets the paymentMth8 property value. The paymentMth8 property
     * @return float|null
    */
    public function getPaymentMth8(): ?float {
        return $this->paymentMth8;
    }

    /**
     * Gets the paymentMth9 property value. The paymentMth9 property
     * @return float|null
    */
    public function getPaymentMth9(): ?float {
        return $this->paymentMth9;
    }

    /**
     * Gets the priorYear property value. The priorYear property
     * @return float|null
    */
    public function getPriorYear(): ?float {
        return $this->priorYear;
    }

    /**
     * Gets the settlementDiscRate property value. The settlementDiscRate property
     * @return float|null
    */
    public function getSettlementDiscRate(): ?float {
        return $this->settlementDiscRate;
    }

    /**
     * Gets the settlementDueDays property value. The settlementDueDays property
     * @return int|null
    */
    public function getSettlementDueDays(): ?int {
        return $this->settlementDueDays;
    }

    /**
     * Gets the telephone property value. The telephone property
     * @return string|null
    */
    public function getTelephone(): ?string {
        return $this->telephone;
    }

    /**
     * Gets the telephone2 property value. The telephone2 property
     * @return string|null
    */
    public function getTelephone2(): ?string {
        return $this->telephone2;
    }

    /**
     * Gets the terms property value. The terms property
     * @return string|null
    */
    public function getTerms(): ?string {
        return $this->terms;
    }

    /**
     * Gets the tradeContact property value. The tradeContact property
     * @return string|null
    */
    public function getTradeContact(): ?string {
        return $this->tradeContact;
    }

    /**
     * Gets the turnoverMtd property value. The turnoverMtd property
     * @return float|null
    */
    public function getTurnoverMtd(): ?float {
        return $this->turnoverMtd;
    }

    /**
     * Gets the turnoverYtd property value. The turnoverYtd property
     * @return float|null
    */
    public function getTurnoverYtd(): ?float {
        return $this->turnoverYtd;
    }

    /**
     * Gets the vatRegNumber property value. The vatRegNumber property
     * @return string|null
    */
    public function getVatRegNumber(): ?string {
        return $this->vatRegNumber;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeIntegerValue('accountOnHold', $this->getAccountOnHold());
        $writer->writeStringValue('analysis1', $this->getAnalysis1());
        $writer->writeStringValue('analysis2', $this->getAnalysis2());
        $writer->writeStringValue('analysis3', $this->getAnalysis3());
        $writer->writeFloatValue('balance', $this->getBalance());
        $writer->writeStringValue('bankAccountName', $this->getBankAccountName());
        $writer->writeStringValue('bankAccountNumber', $this->getBankAccountNumber());
        $writer->writeStringValue('bankAddress1', $this->getBankAddress1());
        $writer->writeStringValue('bankAddress2', $this->getBankAddress2());
        $writer->writeStringValue('bankAddress3', $this->getBankAddress3());
        $writer->writeStringValue('bankAddress4', $this->getBankAddress4());
        $writer->writeStringValue('bankAddress5', $this->getBankAddress5());
        $writer->writeStringValue('bankIban', $this->getBankIban());
        $writer->writeStringValue('bankName', $this->getBankName());
        $writer->writeStringValue('bankSortCode', $this->getBankSortCode());
        $writer->writeStringValue('contactName', $this->getContactName());
        $writer->writeStringValue('countryCode', $this->getCountryCode());
        $writer->writeFloatValue('creditBf', $this->getCreditBf());
        $writer->writeFloatValue('creditCf', $this->getCreditCf());
        $writer->writeFloatValue('creditLimit', $this->getCreditLimit());
        $writer->writeFloatValue('creditMth1', $this->getCreditMth1());
        $writer->writeFloatValue('creditMth10', $this->getCreditMth10());
        $writer->writeFloatValue('creditMth11', $this->getCreditMth11());
        $writer->writeFloatValue('creditMth12', $this->getCreditMth12());
        $writer->writeFloatValue('creditMth2', $this->getCreditMth2());
        $writer->writeFloatValue('creditMth3', $this->getCreditMth3());
        $writer->writeFloatValue('creditMth4', $this->getCreditMth4());
        $writer->writeFloatValue('creditMth5', $this->getCreditMth5());
        $writer->writeFloatValue('creditMth6', $this->getCreditMth6());
        $writer->writeFloatValue('creditMth7', $this->getCreditMth7());
        $writer->writeFloatValue('creditMth8', $this->getCreditMth8());
        $writer->writeFloatValue('creditMth9', $this->getCreditMth9());
        $writer->writeEnumValue('creditPosition', $this->getCreditPosition());
        $writer->writeIntegerValue('currency', $this->getCurrency());
        $writer->writeDateTimeValue('declarationValidFrom', $this->getDeclarationValidFrom());
        $writer->writeStringValue('defNomCode', $this->getDefNomCode());
        $writer->writeEnumValue('defTaxCode', $this->getDefTaxCode());
        $writer->writeIntegerValue('deptNumber', $this->getDeptNumber());
        $writer->writeFloatValue('discountRate', $this->getDiscountRate());
        $writer->writeStringValue('donorForename', $this->getDonorForename());
        $writer->writeStringValue('donorSurname', $this->getDonorSurname());
        $writer->writeStringValue('donorTitle', $this->getDonorTitle());
        $writer->writeStringValue('dunsNumber', $this->getDunsNumber());
        $writer->writeStringValue('eMail', $this->getEMail());
        $writer->writeStringValue('eMail2', $this->getEMail2());
        $writer->writeStringValue('eMail3', $this->getEMail3());
        $writer->writeStringValue('eoriNumber', $this->getEoriNumber());
        $writer->writeStringValue('fax', $this->getFax());
        $writer->writeIntegerValue('inactiveFlag', $this->getInactiveFlag());
        $writer->writeFloatValue('invoiceBf', $this->getInvoiceBf());
        $writer->writeFloatValue('invoiceCf', $this->getInvoiceCf());
        $writer->writeFloatValue('invoiceMth1', $this->getInvoiceMth1());
        $writer->writeFloatValue('invoiceMth10', $this->getInvoiceMth10());
        $writer->writeFloatValue('invoiceMth11', $this->getInvoiceMth11());
        $writer->writeFloatValue('invoiceMth12', $this->getInvoiceMth12());
        $writer->writeFloatValue('invoiceMth2', $this->getInvoiceMth2());
        $writer->writeFloatValue('invoiceMth3', $this->getInvoiceMth3());
        $writer->writeFloatValue('invoiceMth4', $this->getInvoiceMth4());
        $writer->writeFloatValue('invoiceMth5', $this->getInvoiceMth5());
        $writer->writeFloatValue('invoiceMth6', $this->getInvoiceMth6());
        $writer->writeFloatValue('invoiceMth7', $this->getInvoiceMth7());
        $writer->writeFloatValue('invoiceMth8', $this->getInvoiceMth8());
        $writer->writeFloatValue('invoiceMth9', $this->getInvoiceMth9());
        $writer->writeDateTimeValue('lastInvDate', $this->getLastInvDate());
        $writer->writeStringValue('name', $this->getName());
        $writer->writeFloatValue('paymentBf', $this->getPaymentBf());
        $writer->writeFloatValue('paymentCf', $this->getPaymentCf());
        $writer->writeIntegerValue('paymentDueDays', $this->getPaymentDueDays());
        $writer->writeIntegerValue('paymentDueFrom', $this->getPaymentDueFrom());
        $writer->writeFloatValue('paymentMth1', $this->getPaymentMth1());
        $writer->writeFloatValue('paymentMth10', $this->getPaymentMth10());
        $writer->writeFloatValue('paymentMth11', $this->getPaymentMth11());
        $writer->writeFloatValue('paymentMth12', $this->getPaymentMth12());
        $writer->writeFloatValue('paymentMth2', $this->getPaymentMth2());
        $writer->writeFloatValue('paymentMth3', $this->getPaymentMth3());
        $writer->writeFloatValue('paymentMth4', $this->getPaymentMth4());
        $writer->writeFloatValue('paymentMth5', $this->getPaymentMth5());
        $writer->writeFloatValue('paymentMth6', $this->getPaymentMth6());
        $writer->writeFloatValue('paymentMth7', $this->getPaymentMth7());
        $writer->writeFloatValue('paymentMth8', $this->getPaymentMth8());
        $writer->writeFloatValue('paymentMth9', $this->getPaymentMth9());
        $writer->writeFloatValue('priorYear', $this->getPriorYear());
        $writer->writeFloatValue('settlementDiscRate', $this->getSettlementDiscRate());
        $writer->writeIntegerValue('settlementDueDays', $this->getSettlementDueDays());
        $writer->writeStringValue('telephone', $this->getTelephone());
        $writer->writeStringValue('telephone2', $this->getTelephone2());
        $writer->writeStringValue('terms', $this->getTerms());
        $writer->writeStringValue('tradeContact', $this->getTradeContact());
        $writer->writeFloatValue('turnoverMtd', $this->getTurnoverMtd());
        $writer->writeFloatValue('turnoverYtd', $this->getTurnoverYtd());
        $writer->writeStringValue('vatRegNumber', $this->getVatRegNumber());
    }

    /**
     * Sets the accountOnHold property value. The accountOnHold property
     * @param int|null $value Value to set for the accountOnHold property.
    */
    public function setAccountOnHold(?int $value): void {
        $this->accountOnHold = $value;
    }

    /**
     * Sets the analysis1 property value. The analysis1 property
     * @param string|null $value Value to set for the analysis1 property.
    */
    public function setAnalysis1(?string $value): void {
        $this->analysis1 = $value;
    }

    /**
     * Sets the analysis2 property value. The analysis2 property
     * @param string|null $value Value to set for the analysis2 property.
    */
    public function setAnalysis2(?string $value): void {
        $this->analysis2 = $value;
    }

    /**
     * Sets the analysis3 property value. The analysis3 property
     * @param string|null $value Value to set for the analysis3 property.
    */
    public function setAnalysis3(?string $value): void {
        $this->analysis3 = $value;
    }

    /**
     * Sets the balance property value. The balance property
     * @param float|null $value Value to set for the balance property.
    */
    public function setBalance(?float $value): void {
        $this->balance = $value;
    }

    /**
     * Sets the bankAccountName property value. The bankAccountName property
     * @param string|null $value Value to set for the bankAccountName property.
    */
    public function setBankAccountName(?string $value): void {
        $this->bankAccountName = $value;
    }

    /**
     * Sets the bankAccountNumber property value. The bankAccountNumber property
     * @param string|null $value Value to set for the bankAccountNumber property.
    */
    public function setBankAccountNumber(?string $value): void {
        $this->bankAccountNumber = $value;
    }

    /**
     * Sets the bankAddress1 property value. The bankAddress1 property
     * @param string|null $value Value to set for the bankAddress1 property.
    */
    public function setBankAddress1(?string $value): void {
        $this->bankAddress1 = $value;
    }

    /**
     * Sets the bankAddress2 property value. The bankAddress2 property
     * @param string|null $value Value to set for the bankAddress2 property.
    */
    public function setBankAddress2(?string $value): void {
        $this->bankAddress2 = $value;
    }

    /**
     * Sets the bankAddress3 property value. The bankAddress3 property
     * @param string|null $value Value to set for the bankAddress3 property.
    */
    public function setBankAddress3(?string $value): void {
        $this->bankAddress3 = $value;
    }

    /**
     * Sets the bankAddress4 property value. The bankAddress4 property
     * @param string|null $value Value to set for the bankAddress4 property.
    */
    public function setBankAddress4(?string $value): void {
        $this->bankAddress4 = $value;
    }

    /**
     * Sets the bankAddress5 property value. The bankAddress5 property
     * @param string|null $value Value to set for the bankAddress5 property.
    */
    public function setBankAddress5(?string $value): void {
        $this->bankAddress5 = $value;
    }

    /**
     * Sets the bankIban property value. The bankIban property
     * @param string|null $value Value to set for the bankIban property.
    */
    public function setBankIban(?string $value): void {
        $this->bankIban = $value;
    }

    /**
     * Sets the bankName property value. The bankName property
     * @param string|null $value Value to set for the bankName property.
    */
    public function setBankName(?string $value): void {
        $this->bankName = $value;
    }

    /**
     * Sets the bankSortCode property value. The bankSortCode property
     * @param string|null $value Value to set for the bankSortCode property.
    */
    public function setBankSortCode(?string $value): void {
        $this->bankSortCode = $value;
    }

    /**
     * Sets the contactName property value. The contactName property
     * @param string|null $value Value to set for the contactName property.
    */
    public function setContactName(?string $value): void {
        $this->contactName = $value;
    }

    /**
     * Sets the countryCode property value. The countryCode property
     * @param string|null $value Value to set for the countryCode property.
    */
    public function setCountryCode(?string $value): void {
        $this->countryCode = $value;
    }

    /**
     * Sets the creditBf property value. The creditBf property
     * @param float|null $value Value to set for the creditBf property.
    */
    public function setCreditBf(?float $value): void {
        $this->creditBf = $value;
    }

    /**
     * Sets the creditCf property value. The creditCf property
     * @param float|null $value Value to set for the creditCf property.
    */
    public function setCreditCf(?float $value): void {
        $this->creditCf = $value;
    }

    /**
     * Sets the creditLimit property value. The creditLimit property
     * @param float|null $value Value to set for the creditLimit property.
    */
    public function setCreditLimit(?float $value): void {
        $this->creditLimit = $value;
    }

    /**
     * Sets the creditMth1 property value. The creditMth1 property
     * @param float|null $value Value to set for the creditMth1 property.
    */
    public function setCreditMth1(?float $value): void {
        $this->creditMth1 = $value;
    }

    /**
     * Sets the creditMth10 property value. The creditMth10 property
     * @param float|null $value Value to set for the creditMth10 property.
    */
    public function setCreditMth10(?float $value): void {
        $this->creditMth10 = $value;
    }

    /**
     * Sets the creditMth11 property value. The creditMth11 property
     * @param float|null $value Value to set for the creditMth11 property.
    */
    public function setCreditMth11(?float $value): void {
        $this->creditMth11 = $value;
    }

    /**
     * Sets the creditMth12 property value. The creditMth12 property
     * @param float|null $value Value to set for the creditMth12 property.
    */
    public function setCreditMth12(?float $value): void {
        $this->creditMth12 = $value;
    }

    /**
     * Sets the creditMth2 property value. The creditMth2 property
     * @param float|null $value Value to set for the creditMth2 property.
    */
    public function setCreditMth2(?float $value): void {
        $this->creditMth2 = $value;
    }

    /**
     * Sets the creditMth3 property value. The creditMth3 property
     * @param float|null $value Value to set for the creditMth3 property.
    */
    public function setCreditMth3(?float $value): void {
        $this->creditMth3 = $value;
    }

    /**
     * Sets the creditMth4 property value. The creditMth4 property
     * @param float|null $value Value to set for the creditMth4 property.
    */
    public function setCreditMth4(?float $value): void {
        $this->creditMth4 = $value;
    }

    /**
     * Sets the creditMth5 property value. The creditMth5 property
     * @param float|null $value Value to set for the creditMth5 property.
    */
    public function setCreditMth5(?float $value): void {
        $this->creditMth5 = $value;
    }

    /**
     * Sets the creditMth6 property value. The creditMth6 property
     * @param float|null $value Value to set for the creditMth6 property.
    */
    public function setCreditMth6(?float $value): void {
        $this->creditMth6 = $value;
    }

    /**
     * Sets the creditMth7 property value. The creditMth7 property
     * @param float|null $value Value to set for the creditMth7 property.
    */
    public function setCreditMth7(?float $value): void {
        $this->creditMth7 = $value;
    }

    /**
     * Sets the creditMth8 property value. The creditMth8 property
     * @param float|null $value Value to set for the creditMth8 property.
    */
    public function setCreditMth8(?float $value): void {
        $this->creditMth8 = $value;
    }

    /**
     * Sets the creditMth9 property value. The creditMth9 property
     * @param float|null $value Value to set for the creditMth9 property.
    */
    public function setCreditMth9(?float $value): void {
        $this->creditMth9 = $value;
    }

    /**
     * Sets the creditPosition property value. The creditPosition property
     * @param SupplierAttributesEdit_creditPosition|null $value Value to set for the creditPosition property.
    */
    public function setCreditPosition(?SupplierAttributesEdit_creditPosition $value): void {
        $this->creditPosition = $value;
    }

    /**
     * Sets the currency property value. The currency property
     * @param int|null $value Value to set for the currency property.
    */
    public function setCurrency(?int $value): void {
        $this->currency = $value;
    }

    /**
     * Sets the declarationValidFrom property value. The declarationValidFrom property
     * @param DateTime|null $value Value to set for the declarationValidFrom property.
    */
    public function setDeclarationValidFrom(?DateTime $value): void {
        $this->declarationValidFrom = $value;
    }

    /**
     * Sets the defNomCode property value. The defNomCode property
     * @param string|null $value Value to set for the defNomCode property.
    */
    public function setDefNomCode(?string $value): void {
        $this->defNomCode = $value;
    }

    /**
     * Sets the defTaxCode property value. The defTaxCode property
     * @param SupplierAttributesEdit_defTaxCode|null $value Value to set for the defTaxCode property.
    */
    public function setDefTaxCode(?SupplierAttributesEdit_defTaxCode $value): void {
        $this->defTaxCode = $value;
    }

    /**
     * Sets the deptNumber property value. The deptNumber property
     * @param int|null $value Value to set for the deptNumber property.
    */
    public function setDeptNumber(?int $value): void {
        $this->deptNumber = $value;
    }

    /**
     * Sets the discountRate property value. The discountRate property
     * @param float|null $value Value to set for the discountRate property.
    */
    public function setDiscountRate(?float $value): void {
        $this->discountRate = $value;
    }

    /**
     * Sets the donorForename property value. The donorForename property
     * @param string|null $value Value to set for the donorForename property.
    */
    public function setDonorForename(?string $value): void {
        $this->donorForename = $value;
    }

    /**
     * Sets the donorSurname property value. The donorSurname property
     * @param string|null $value Value to set for the donorSurname property.
    */
    public function setDonorSurname(?string $value): void {
        $this->donorSurname = $value;
    }

    /**
     * Sets the donorTitle property value. The donorTitle property
     * @param string|null $value Value to set for the donorTitle property.
    */
    public function setDonorTitle(?string $value): void {
        $this->donorTitle = $value;
    }

    /**
     * Sets the dunsNumber property value. The dunsNumber property
     * @param string|null $value Value to set for the dunsNumber property.
    */
    public function setDunsNumber(?string $value): void {
        $this->dunsNumber = $value;
    }

    /**
     * Sets the eMail property value. The eMail property
     * @param string|null $value Value to set for the eMail property.
    */
    public function setEMail(?string $value): void {
        $this->eMail = $value;
    }

    /**
     * Sets the eMail2 property value. The eMail2 property
     * @param string|null $value Value to set for the eMail2 property.
    */
    public function setEMail2(?string $value): void {
        $this->eMail2 = $value;
    }

    /**
     * Sets the eMail3 property value. The eMail3 property
     * @param string|null $value Value to set for the eMail3 property.
    */
    public function setEMail3(?string $value): void {
        $this->eMail3 = $value;
    }

    /**
     * Sets the eoriNumber property value. The eoriNumber property
     * @param string|null $value Value to set for the eoriNumber property.
    */
    public function setEoriNumber(?string $value): void {
        $this->eoriNumber = $value;
    }

    /**
     * Sets the fax property value. The fax property
     * @param string|null $value Value to set for the fax property.
    */
    public function setFax(?string $value): void {
        $this->fax = $value;
    }

    /**
     * Sets the inactiveFlag property value. The inactiveFlag property
     * @param int|null $value Value to set for the inactiveFlag property.
    */
    public function setInactiveFlag(?int $value): void {
        $this->inactiveFlag = $value;
    }

    /**
     * Sets the invoiceBf property value. The invoiceBf property
     * @param float|null $value Value to set for the invoiceBf property.
    */
    public function setInvoiceBf(?float $value): void {
        $this->invoiceBf = $value;
    }

    /**
     * Sets the invoiceCf property value. The invoiceCf property
     * @param float|null $value Value to set for the invoiceCf property.
    */
    public function setInvoiceCf(?float $value): void {
        $this->invoiceCf = $value;
    }

    /**
     * Sets the invoiceMth1 property value. The invoiceMth1 property
     * @param float|null $value Value to set for the invoiceMth1 property.
    */
    public function setInvoiceMth1(?float $value): void {
        $this->invoiceMth1 = $value;
    }

    /**
     * Sets the invoiceMth10 property value. The invoiceMth10 property
     * @param float|null $value Value to set for the invoiceMth10 property.
    */
    public function setInvoiceMth10(?float $value): void {
        $this->invoiceMth10 = $value;
    }

    /**
     * Sets the invoiceMth11 property value. The invoiceMth11 property
     * @param float|null $value Value to set for the invoiceMth11 property.
    */
    public function setInvoiceMth11(?float $value): void {
        $this->invoiceMth11 = $value;
    }

    /**
     * Sets the invoiceMth12 property value. The invoiceMth12 property
     * @param float|null $value Value to set for the invoiceMth12 property.
    */
    public function setInvoiceMth12(?float $value): void {
        $this->invoiceMth12 = $value;
    }

    /**
     * Sets the invoiceMth2 property value. The invoiceMth2 property
     * @param float|null $value Value to set for the invoiceMth2 property.
    */
    public function setInvoiceMth2(?float $value): void {
        $this->invoiceMth2 = $value;
    }

    /**
     * Sets the invoiceMth3 property value. The invoiceMth3 property
     * @param float|null $value Value to set for the invoiceMth3 property.
    */
    public function setInvoiceMth3(?float $value): void {
        $this->invoiceMth3 = $value;
    }

    /**
     * Sets the invoiceMth4 property value. The invoiceMth4 property
     * @param float|null $value Value to set for the invoiceMth4 property.
    */
    public function setInvoiceMth4(?float $value): void {
        $this->invoiceMth4 = $value;
    }

    /**
     * Sets the invoiceMth5 property value. The invoiceMth5 property
     * @param float|null $value Value to set for the invoiceMth5 property.
    */
    public function setInvoiceMth5(?float $value): void {
        $this->invoiceMth5 = $value;
    }

    /**
     * Sets the invoiceMth6 property value. The invoiceMth6 property
     * @param float|null $value Value to set for the invoiceMth6 property.
    */
    public function setInvoiceMth6(?float $value): void {
        $this->invoiceMth6 = $value;
    }

    /**
     * Sets the invoiceMth7 property value. The invoiceMth7 property
     * @param float|null $value Value to set for the invoiceMth7 property.
    */
    public function setInvoiceMth7(?float $value): void {
        $this->invoiceMth7 = $value;
    }

    /**
     * Sets the invoiceMth8 property value. The invoiceMth8 property
     * @param float|null $value Value to set for the invoiceMth8 property.
    */
    public function setInvoiceMth8(?float $value): void {
        $this->invoiceMth8 = $value;
    }

    /**
     * Sets the invoiceMth9 property value. The invoiceMth9 property
     * @param float|null $value Value to set for the invoiceMth9 property.
    */
    public function setInvoiceMth9(?float $value): void {
        $this->invoiceMth9 = $value;
    }

    /**
     * Sets the lastInvDate property value. The lastInvDate property
     * @param DateTime|null $value Value to set for the lastInvDate property.
    */
    public function setLastInvDate(?DateTime $value): void {
        $this->lastInvDate = $value;
    }

    /**
     * Sets the name property value. The name property
     * @param string|null $value Value to set for the name property.
    */
    public function setName(?string $value): void {
        $this->name = $value;
    }

    /**
     * Sets the paymentBf property value. The paymentBf property
     * @param float|null $value Value to set for the paymentBf property.
    */
    public function setPaymentBf(?float $value): void {
        $this->paymentBf = $value;
    }

    /**
     * Sets the paymentCf property value. The paymentCf property
     * @param float|null $value Value to set for the paymentCf property.
    */
    public function setPaymentCf(?float $value): void {
        $this->paymentCf = $value;
    }

    /**
     * Sets the paymentDueDays property value. The paymentDueDays property
     * @param int|null $value Value to set for the paymentDueDays property.
    */
    public function setPaymentDueDays(?int $value): void {
        $this->paymentDueDays = $value;
    }

    /**
     * Sets the paymentDueFrom property value. The paymentDueFrom property
     * @param int|null $value Value to set for the paymentDueFrom property.
    */
    public function setPaymentDueFrom(?int $value): void {
        $this->paymentDueFrom = $value;
    }

    /**
     * Sets the paymentMth1 property value. The paymentMth1 property
     * @param float|null $value Value to set for the paymentMth1 property.
    */
    public function setPaymentMth1(?float $value): void {
        $this->paymentMth1 = $value;
    }

    /**
     * Sets the paymentMth10 property value. The paymentMth10 property
     * @param float|null $value Value to set for the paymentMth10 property.
    */
    public function setPaymentMth10(?float $value): void {
        $this->paymentMth10 = $value;
    }

    /**
     * Sets the paymentMth11 property value. The paymentMth11 property
     * @param float|null $value Value to set for the paymentMth11 property.
    */
    public function setPaymentMth11(?float $value): void {
        $this->paymentMth11 = $value;
    }

    /**
     * Sets the paymentMth12 property value. The paymentMth12 property
     * @param float|null $value Value to set for the paymentMth12 property.
    */
    public function setPaymentMth12(?float $value): void {
        $this->paymentMth12 = $value;
    }

    /**
     * Sets the paymentMth2 property value. The paymentMth2 property
     * @param float|null $value Value to set for the paymentMth2 property.
    */
    public function setPaymentMth2(?float $value): void {
        $this->paymentMth2 = $value;
    }

    /**
     * Sets the paymentMth3 property value. The paymentMth3 property
     * @param float|null $value Value to set for the paymentMth3 property.
    */
    public function setPaymentMth3(?float $value): void {
        $this->paymentMth3 = $value;
    }

    /**
     * Sets the paymentMth4 property value. The paymentMth4 property
     * @param float|null $value Value to set for the paymentMth4 property.
    */
    public function setPaymentMth4(?float $value): void {
        $this->paymentMth4 = $value;
    }

    /**
     * Sets the paymentMth5 property value. The paymentMth5 property
     * @param float|null $value Value to set for the paymentMth5 property.
    */
    public function setPaymentMth5(?float $value): void {
        $this->paymentMth5 = $value;
    }

    /**
     * Sets the paymentMth6 property value. The paymentMth6 property
     * @param float|null $value Value to set for the paymentMth6 property.
    */
    public function setPaymentMth6(?float $value): void {
        $this->paymentMth6 = $value;
    }

    /**
     * Sets the paymentMth7 property value. The paymentMth7 property
     * @param float|null $value Value to set for the paymentMth7 property.
    */
    public function setPaymentMth7(?float $value): void {
        $this->paymentMth7 = $value;
    }

    /**
     * Sets the paymentMth8 property value. The paymentMth8 property
     * @param float|null $value Value to set for the paymentMth8 property.
    */
    public function setPaymentMth8(?float $value): void {
        $this->paymentMth8 = $value;
    }

    /**
     * Sets the paymentMth9 property value. The paymentMth9 property
     * @param float|null $value Value to set for the paymentMth9 property.
    */
    public function setPaymentMth9(?float $value): void {
        $this->paymentMth9 = $value;
    }

    /**
     * Sets the priorYear property value. The priorYear property
     * @param float|null $value Value to set for the priorYear property.
    */
    public function setPriorYear(?float $value): void {
        $this->priorYear = $value;
    }

    /**
     * Sets the settlementDiscRate property value. The settlementDiscRate property
     * @param float|null $value Value to set for the settlementDiscRate property.
    */
    public function setSettlementDiscRate(?float $value): void {
        $this->settlementDiscRate = $value;
    }

    /**
     * Sets the settlementDueDays property value. The settlementDueDays property
     * @param int|null $value Value to set for the settlementDueDays property.
    */
    public function setSettlementDueDays(?int $value): void {
        $this->settlementDueDays = $value;
    }

    /**
     * Sets the telephone property value. The telephone property
     * @param string|null $value Value to set for the telephone property.
    */
    public function setTelephone(?string $value): void {
        $this->telephone = $value;
    }

    /**
     * Sets the telephone2 property value. The telephone2 property
     * @param string|null $value Value to set for the telephone2 property.
    */
    public function setTelephone2(?string $value): void {
        $this->telephone2 = $value;
    }

    /**
     * Sets the terms property value. The terms property
     * @param string|null $value Value to set for the terms property.
    */
    public function setTerms(?string $value): void {
        $this->terms = $value;
    }

    /**
     * Sets the tradeContact property value. The tradeContact property
     * @param string|null $value Value to set for the tradeContact property.
    */
    public function setTradeContact(?string $value): void {
        $this->tradeContact = $value;
    }

    /**
     * Sets the turnoverMtd property value. The turnoverMtd property
     * @param float|null $value Value to set for the turnoverMtd property.
    */
    public function setTurnoverMtd(?float $value): void {
        $this->turnoverMtd = $value;
    }

    /**
     * Sets the turnoverYtd property value. The turnoverYtd property
     * @param float|null $value Value to set for the turnoverYtd property.
    */
    public function setTurnoverYtd(?float $value): void {
        $this->turnoverYtd = $value;
    }

    /**
     * Sets the vatRegNumber property value. The vatRegNumber property
     * @param string|null $value Value to set for the vatRegNumber property.
    */
    public function setVatRegNumber(?string $value): void {
        $this->vatRegNumber = $value;
    }

}
