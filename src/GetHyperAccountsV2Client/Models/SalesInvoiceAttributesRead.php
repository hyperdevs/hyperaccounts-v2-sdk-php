<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use DateTime;
use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class SalesInvoiceAttributesRead implements Parsable
{
    /**
     * @var string|null $address1 The address1 property
    */
    private ?string $address1 = null;

    /**
     * @var string|null $address2 The address2 property
    */
    private ?string $address2 = null;

    /**
     * @var string|null $address3 The address3 property
    */
    private ?string $address3 = null;

    /**
     * @var string|null $address4 The address4 property
    */
    private ?string $address4 = null;

    /**
     * @var string|null $address5 The address5 property
    */
    private ?string $address5 = null;

    /**
     * @var float|null $amountPrepaid The amountPrepaid property
    */
    private ?float $amountPrepaid = null;

    /**
     * @var string|null $analysis1 The analysis1 property
    */
    private ?string $analysis1 = null;

    /**
     * @var string|null $analysis2 The analysis2 property
    */
    private ?string $analysis2 = null;

    /**
     * @var string|null $analysis3 The analysis3 property
    */
    private ?string $analysis3 = null;

    /**
     * @var string|null $bankRef The bankRef property
    */
    private ?string $bankRef = null;

    /**
     * @var string|null $cAddress1 The cAddress1 property
    */
    private ?string $cAddress1 = null;

    /**
     * @var string|null $cAddress2 The cAddress2 property
    */
    private ?string $cAddress2 = null;

    /**
     * @var string|null $cAddress3 The cAddress3 property
    */
    private ?string $cAddress3 = null;

    /**
     * @var string|null $cAddress4 The cAddress4 property
    */
    private ?string $cAddress4 = null;

    /**
     * @var string|null $cAddress5 The cAddress5 property
    */
    private ?string $cAddress5 = null;

    /**
     * @var string|null $carrDeptName The carrDeptName property
    */
    private ?string $carrDeptName = null;

    /**
     * @var int|null $carrDeptNumber The carrDeptNumber property
    */
    private ?int $carrDeptNumber = null;

    /**
     * @var float|null $carrGross The carrGross property
    */
    private ?float $carrGross = null;

    /**
     * @var float|null $carrNet The carrNet property
    */
    private ?float $carrNet = null;

    /**
     * @var string|null $carrNomCode The carrNomCode property
    */
    private ?string $carrNomCode = null;

    /**
     * @var float|null $carrTax The carrTax property
    */
    private ?float $carrTax = null;

    /**
     * @var SalesInvoiceAttributesRead_carrTaxCode|null $carrTaxCode The carrTaxCode property
    */
    private ?SalesInvoiceAttributesRead_carrTaxCode $carrTaxCode = null;

    /**
     * @var string|null $consignment The consignment property
    */
    private ?string $consignment = null;

    /**
     * @var string|null $contactName The contactName property
    */
    private ?string $contactName = null;

    /**
     * @var int|null $containsCisReverseChargeItems The containsCisReverseChargeItems property
    */
    private ?int $containsCisReverseChargeItems = null;

    /**
     * @var string|null $courierName The courierName property
    */
    private ?string $courierName = null;

    /**
     * @var int|null $courierNumber The courierNumber property
    */
    private ?int $courierNumber = null;

    /**
     * @var int|null $currency The currency property
    */
    private ?int $currency = null;

    /**
     * @var int|null $currencyType The currencyType property
    */
    private ?int $currencyType = null;

    /**
     * @var float|null $custDiscRate The custDiscRate property
    */
    private ?float $custDiscRate = null;

    /**
     * @var string|null $custOrderNumber The custOrderNumber property
    */
    private ?string $custOrderNumber = null;

    /**
     * @var string|null $custTelNumber The custTelNumber property
    */
    private ?string $custTelNumber = null;

    /**
     * @var string|null $delAddress1 The delAddress1 property
    */
    private ?string $delAddress1 = null;

    /**
     * @var string|null $delAddress2 The delAddress2 property
    */
    private ?string $delAddress2 = null;

    /**
     * @var string|null $delAddress3 The delAddress3 property
    */
    private ?string $delAddress3 = null;

    /**
     * @var string|null $delAddress4 The delAddress4 property
    */
    private ?string $delAddress4 = null;

    /**
     * @var string|null $delAddress5 The delAddress5 property
    */
    private ?string $delAddress5 = null;

    /**
     * @var string|null $delName The delName property
    */
    private ?string $delName = null;

    /**
     * @var string|null $dunsNumber The dunsNumber property
    */
    private ?string $dunsNumber = null;

    /**
     * @var float|null $euroGross The euroGross property
    */
    private ?float $euroGross = null;

    /**
     * @var float|null $euroRate The euroRate property
    */
    private ?float $euroRate = null;

    /**
     * @var float|null $foreignAmountPrepaid The foreignAmountPrepaid property
    */
    private ?float $foreignAmountPrepaid = null;

    /**
     * @var float|null $foreignCarrGross The foreignCarrGross property
    */
    private ?float $foreignCarrGross = null;

    /**
     * @var float|null $foreignCarrNet The foreignCarrNet property
    */
    private ?float $foreignCarrNet = null;

    /**
     * @var float|null $foreignCarrTax The foreignCarrTax property
    */
    private ?float $foreignCarrTax = null;

    /**
     * @var float|null $foreignInvoiceGross The foreignInvoiceGross property
    */
    private ?float $foreignInvoiceGross = null;

    /**
     * @var float|null $foreignInvoiceNet The foreignInvoiceNet property
    */
    private ?float $foreignInvoiceNet = null;

    /**
     * @var float|null $foreignInvoiceTax The foreignInvoiceTax property
    */
    private ?float $foreignInvoiceTax = null;

    /**
     * @var float|null $foreignItemsGross The foreignItemsGross property
    */
    private ?float $foreignItemsGross = null;

    /**
     * @var float|null $foreignItemsNet The foreignItemsNet property
    */
    private ?float $foreignItemsNet = null;

    /**
     * @var float|null $foreignItemsTax The foreignItemsTax property
    */
    private ?float $foreignItemsTax = null;

    /**
     * @var float|null $foreignRate The foreignRate property
    */
    private ?float $foreignRate = null;

    /**
     * @var float|null $foreignSettlementDiscAmount The foreignSettlementDiscAmount property
    */
    private ?float $foreignSettlementDiscAmount = null;

    /**
     * @var float|null $foreignSettlementTotal The foreignSettlementTotal property
    */
    private ?float $foreignSettlementTotal = null;

    /**
     * @var int|null $gdnNumber The gdnNumber property
    */
    private ?int $gdnNumber = null;

    /**
     * @var string|null $globalDeptName The globalDeptName property
    */
    private ?string $globalDeptName = null;

    /**
     * @var int|null $globalDeptNumber The globalDeptNumber property
    */
    private ?int $globalDeptNumber = null;

    /**
     * @var string|null $globalDetails The globalDetails property
    */
    private ?string $globalDetails = null;

    /**
     * @var string|null $globalNomCode The globalNomCode property
    */
    private ?string $globalNomCode = null;

    /**
     * @var SalesInvoiceAttributesRead_globalTaxCode|null $globalTaxCode The globalTaxCode property
    */
    private ?SalesInvoiceAttributesRead_globalTaxCode $globalTaxCode = null;

    /**
     * @var int|null $incoterms The incoterms property
    */
    private ?int $incoterms = null;

    /**
     * @var string|null $incotermsText The incotermsText property
    */
    private ?string $incotermsText = null;

    /**
     * @var DateTime|null $invoiceDate The invoiceDate property
    */
    private ?DateTime $invoiceDate = null;

    /**
     * @var float|null $invoiceGross The invoiceGross property
    */
    private ?float $invoiceGross = null;

    /**
     * @var float|null $invoiceNet The invoiceNet property
    */
    private ?float $invoiceNet = null;

    /**
     * @var string|null $invoiceOrCredit The invoiceOrCredit property
    */
    private ?string $invoiceOrCredit = null;

    /**
     * @var string|null $invoicePaymentId The invoicePaymentId property
    */
    private ?string $invoicePaymentId = null;

    /**
     * @var float|null $invoiceTax The invoiceTax property
    */
    private ?float $invoiceTax = null;

    /**
     * @var string|null $invoiceType The invoiceType property
    */
    private ?string $invoiceType = null;

    /**
     * @var int|null $invoiceTypeCode The invoiceTypeCode property
    */
    private ?int $invoiceTypeCode = null;

    /**
     * @var float|null $itemsGross The itemsGross property
    */
    private ?float $itemsGross = null;

    /**
     * @var float|null $itemsNet The itemsNet property
    */
    private ?float $itemsNet = null;

    /**
     * @var float|null $itemsTax The itemsTax property
    */
    private ?float $itemsTax = null;

    /**
     * @var string|null $name The name property
    */
    private ?string $name = null;

    /**
     * @var float|null $netAmount1 The netAmount1 property
    */
    private ?float $netAmount1 = null;

    /**
     * @var float|null $netAmount2 The netAmount2 property
    */
    private ?float $netAmount2 = null;

    /**
     * @var float|null $netAmount3 The netAmount3 property
    */
    private ?float $netAmount3 = null;

    /**
     * @var float|null $netAmount4 The netAmount4 property
    */
    private ?float $netAmount4 = null;

    /**
     * @var float|null $netAmount5 The netAmount5 property
    */
    private ?float $netAmount5 = null;

    /**
     * @var string|null $netValueDiscountComment1 The netValueDiscountComment1 property
    */
    private ?string $netValueDiscountComment1 = null;

    /**
     * @var string|null $netValueDiscountComment2 The netValueDiscountComment2 property
    */
    private ?string $netValueDiscountComment2 = null;

    /**
     * @var string|null $netValueDiscountDesc The netValueDiscountDesc property
    */
    private ?string $netValueDiscountDesc = null;

    /**
     * @var float|null $netValueDiscountNet The netValueDiscountNet property
    */
    private ?float $netValueDiscountNet = null;

    /**
     * @var float|null $netValueDiscountNetBase The netValueDiscountNetBase property
    */
    private ?float $netValueDiscountNetBase = null;

    /**
     * @var float|null $netValueDiscountRate The netValueDiscountRate property
    */
    private ?float $netValueDiscountRate = null;

    /**
     * @var string|null $notes1 The notes1 property
    */
    private ?string $notes1 = null;

    /**
     * @var string|null $notes2 The notes2 property
    */
    private ?string $notes2 = null;

    /**
     * @var string|null $notes3 The notes3 property
    */
    private ?string $notes3 = null;

    /**
     * @var string|null $orderNumber The orderNumber property
    */
    private ?string $orderNumber = null;

    /**
     * @var int|null $orderNumberNumeric The orderNumberNumeric property
    */
    private ?int $orderNumberNumeric = null;

    /**
     * @var string|null $ossCountryOfVat The ossCountryOfVat property
    */
    private ?string $ossCountryOfVat = null;

    /**
     * @var int|null $ossReportingType The ossReportingType property
    */
    private ?int $ossReportingType = null;

    /**
     * @var string|null $ossReportingTypeName The ossReportingTypeName property
    */
    private ?string $ossReportingTypeName = null;

    /**
     * @var DateTime|null $paymentDueDate The paymentDueDate property
    */
    private ?DateTime $paymentDueDate = null;

    /**
     * @var string|null $paymentRef The paymentRef property
    */
    private ?string $paymentRef = null;

    /**
     * @var int|null $paymentType The paymentType property
    */
    private ?int $paymentType = null;

    /**
     * @var string|null $posted The posted property
    */
    private ?string $posted = null;

    /**
     * @var int|null $postedCode The postedCode property
    */
    private ?int $postedCode = null;

    /**
     * @var string|null $printed The printed property
    */
    private ?string $printed = null;

    /**
     * @var int|null $printedCode The printedCode property
    */
    private ?int $printedCode = null;

    /**
     * @var DateTime|null $quoteExpiryDate The quoteExpiryDate property
    */
    private ?DateTime $quoteExpiryDate = null;

    /**
     * @var SalesInvoiceAttributesRead_quoteStatus|null $quoteStatus The quoteStatus property
    */
    private ?SalesInvoiceAttributesRead_quoteStatus $quoteStatus = null;

    /**
     * @var int|null $quoteStatusId The quoteStatusId property
    */
    private ?int $quoteStatusId = null;

    /**
     * @var DateTime|null $recordCreateDate The recordCreateDate property
    */
    private ?DateTime $recordCreateDate = null;

    /**
     * @var int|null $recordDeleted The recordDeleted property
    */
    private ?int $recordDeleted = null;

    /**
     * @var DateTime|null $recordModifyDate The recordModifyDate property
    */
    private ?DateTime $recordModifyDate = null;

    /**
     * @var string|null $recurringRef The recurringRef property
    */
    private ?string $recurringRef = null;

    /**
     * @var int|null $resubmitInvoicePaymentRequired The resubmitInvoicePaymentRequired property
    */
    private ?int $resubmitInvoicePaymentRequired = null;

    /**
     * @var float|null $settlementDiscAmount The settlementDiscAmount property
    */
    private ?float $settlementDiscAmount = null;

    /**
     * @var float|null $settlementDiscRate The settlementDiscRate property
    */
    private ?float $settlementDiscRate = null;

    /**
     * @var int|null $settlementDueDays The settlementDueDays property
    */
    private ?int $settlementDueDays = null;

    /**
     * @var float|null $settlementTotal The settlementTotal property
    */
    private ?float $settlementTotal = null;

    /**
     * @var string|null $takenBy The takenBy property
    */
    private ?string $takenBy = null;

    /**
     * @var float|null $taxAmount1 The taxAmount1 property
    */
    private ?float $taxAmount1 = null;

    /**
     * @var float|null $taxAmount2 The taxAmount2 property
    */
    private ?float $taxAmount2 = null;

    /**
     * @var float|null $taxAmount3 The taxAmount3 property
    */
    private ?float $taxAmount3 = null;

    /**
     * @var float|null $taxAmount4 The taxAmount4 property
    */
    private ?float $taxAmount4 = null;

    /**
     * @var float|null $taxAmount5 The taxAmount5 property
    */
    private ?float $taxAmount5 = null;

    /**
     * @var float|null $taxRate1 The taxRate1 property
    */
    private ?float $taxRate1 = null;

    /**
     * @var float|null $taxRate2 The taxRate2 property
    */
    private ?float $taxRate2 = null;

    /**
     * @var float|null $taxRate3 The taxRate3 property
    */
    private ?float $taxRate3 = null;

    /**
     * @var float|null $taxRate4 The taxRate4 property
    */
    private ?float $taxRate4 = null;

    /**
     * @var float|null $taxRate5 The taxRate5 property
    */
    private ?float $taxRate5 = null;

    /**
     * @var string|null $vatRegNumber The vatRegNumber property
    */
    private ?string $vatRegNumber = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return SalesInvoiceAttributesRead
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): SalesInvoiceAttributesRead {
        return new SalesInvoiceAttributesRead();
    }

    /**
     * Gets the address1 property value. The address1 property
     * @return string|null
    */
    public function getAddress1(): ?string {
        return $this->address1;
    }

    /**
     * Gets the address2 property value. The address2 property
     * @return string|null
    */
    public function getAddress2(): ?string {
        return $this->address2;
    }

    /**
     * Gets the address3 property value. The address3 property
     * @return string|null
    */
    public function getAddress3(): ?string {
        return $this->address3;
    }

    /**
     * Gets the address4 property value. The address4 property
     * @return string|null
    */
    public function getAddress4(): ?string {
        return $this->address4;
    }

    /**
     * Gets the address5 property value. The address5 property
     * @return string|null
    */
    public function getAddress5(): ?string {
        return $this->address5;
    }

    /**
     * Gets the amountPrepaid property value. The amountPrepaid property
     * @return float|null
    */
    public function getAmountPrepaid(): ?float {
        return $this->amountPrepaid;
    }

    /**
     * Gets the analysis1 property value. The analysis1 property
     * @return string|null
    */
    public function getAnalysis1(): ?string {
        return $this->analysis1;
    }

    /**
     * Gets the analysis2 property value. The analysis2 property
     * @return string|null
    */
    public function getAnalysis2(): ?string {
        return $this->analysis2;
    }

    /**
     * Gets the analysis3 property value. The analysis3 property
     * @return string|null
    */
    public function getAnalysis3(): ?string {
        return $this->analysis3;
    }

    /**
     * Gets the bankRef property value. The bankRef property
     * @return string|null
    */
    public function getBankRef(): ?string {
        return $this->bankRef;
    }

    /**
     * Gets the cAddress1 property value. The cAddress1 property
     * @return string|null
    */
    public function getCAddress1(): ?string {
        return $this->cAddress1;
    }

    /**
     * Gets the cAddress2 property value. The cAddress2 property
     * @return string|null
    */
    public function getCAddress2(): ?string {
        return $this->cAddress2;
    }

    /**
     * Gets the cAddress3 property value. The cAddress3 property
     * @return string|null
    */
    public function getCAddress3(): ?string {
        return $this->cAddress3;
    }

    /**
     * Gets the cAddress4 property value. The cAddress4 property
     * @return string|null
    */
    public function getCAddress4(): ?string {
        return $this->cAddress4;
    }

    /**
     * Gets the cAddress5 property value. The cAddress5 property
     * @return string|null
    */
    public function getCAddress5(): ?string {
        return $this->cAddress5;
    }

    /**
     * Gets the carrDeptName property value. The carrDeptName property
     * @return string|null
    */
    public function getCarrDeptName(): ?string {
        return $this->carrDeptName;
    }

    /**
     * Gets the carrDeptNumber property value. The carrDeptNumber property
     * @return int|null
    */
    public function getCarrDeptNumber(): ?int {
        return $this->carrDeptNumber;
    }

    /**
     * Gets the carrGross property value. The carrGross property
     * @return float|null
    */
    public function getCarrGross(): ?float {
        return $this->carrGross;
    }

    /**
     * Gets the carrNet property value. The carrNet property
     * @return float|null
    */
    public function getCarrNet(): ?float {
        return $this->carrNet;
    }

    /**
     * Gets the carrNomCode property value. The carrNomCode property
     * @return string|null
    */
    public function getCarrNomCode(): ?string {
        return $this->carrNomCode;
    }

    /**
     * Gets the carrTax property value. The carrTax property
     * @return float|null
    */
    public function getCarrTax(): ?float {
        return $this->carrTax;
    }

    /**
     * Gets the carrTaxCode property value. The carrTaxCode property
     * @return SalesInvoiceAttributesRead_carrTaxCode|null
    */
    public function getCarrTaxCode(): ?SalesInvoiceAttributesRead_carrTaxCode {
        return $this->carrTaxCode;
    }

    /**
     * Gets the consignment property value. The consignment property
     * @return string|null
    */
    public function getConsignment(): ?string {
        return $this->consignment;
    }

    /**
     * Gets the contactName property value. The contactName property
     * @return string|null
    */
    public function getContactName(): ?string {
        return $this->contactName;
    }

    /**
     * Gets the containsCisReverseChargeItems property value. The containsCisReverseChargeItems property
     * @return int|null
    */
    public function getContainsCisReverseChargeItems(): ?int {
        return $this->containsCisReverseChargeItems;
    }

    /**
     * Gets the courierName property value. The courierName property
     * @return string|null
    */
    public function getCourierName(): ?string {
        return $this->courierName;
    }

    /**
     * Gets the courierNumber property value. The courierNumber property
     * @return int|null
    */
    public function getCourierNumber(): ?int {
        return $this->courierNumber;
    }

    /**
     * Gets the currency property value. The currency property
     * @return int|null
    */
    public function getCurrency(): ?int {
        return $this->currency;
    }

    /**
     * Gets the currencyType property value. The currencyType property
     * @return int|null
    */
    public function getCurrencyType(): ?int {
        return $this->currencyType;
    }

    /**
     * Gets the custDiscRate property value. The custDiscRate property
     * @return float|null
    */
    public function getCustDiscRate(): ?float {
        return $this->custDiscRate;
    }

    /**
     * Gets the custOrderNumber property value. The custOrderNumber property
     * @return string|null
    */
    public function getCustOrderNumber(): ?string {
        return $this->custOrderNumber;
    }

    /**
     * Gets the custTelNumber property value. The custTelNumber property
     * @return string|null
    */
    public function getCustTelNumber(): ?string {
        return $this->custTelNumber;
    }

    /**
     * Gets the delAddress1 property value. The delAddress1 property
     * @return string|null
    */
    public function getDelAddress1(): ?string {
        return $this->delAddress1;
    }

    /**
     * Gets the delAddress2 property value. The delAddress2 property
     * @return string|null
    */
    public function getDelAddress2(): ?string {
        return $this->delAddress2;
    }

    /**
     * Gets the delAddress3 property value. The delAddress3 property
     * @return string|null
    */
    public function getDelAddress3(): ?string {
        return $this->delAddress3;
    }

    /**
     * Gets the delAddress4 property value. The delAddress4 property
     * @return string|null
    */
    public function getDelAddress4(): ?string {
        return $this->delAddress4;
    }

    /**
     * Gets the delAddress5 property value. The delAddress5 property
     * @return string|null
    */
    public function getDelAddress5(): ?string {
        return $this->delAddress5;
    }

    /**
     * Gets the delName property value. The delName property
     * @return string|null
    */
    public function getDelName(): ?string {
        return $this->delName;
    }

    /**
     * Gets the dunsNumber property value. The dunsNumber property
     * @return string|null
    */
    public function getDunsNumber(): ?string {
        return $this->dunsNumber;
    }

    /**
     * Gets the euroGross property value. The euroGross property
     * @return float|null
    */
    public function getEuroGross(): ?float {
        return $this->euroGross;
    }

    /**
     * Gets the euroRate property value. The euroRate property
     * @return float|null
    */
    public function getEuroRate(): ?float {
        return $this->euroRate;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'address1' => fn(ParseNode $n) => $o->setAddress1($n->getStringValue()),
            'address2' => fn(ParseNode $n) => $o->setAddress2($n->getStringValue()),
            'address3' => fn(ParseNode $n) => $o->setAddress3($n->getStringValue()),
            'address4' => fn(ParseNode $n) => $o->setAddress4($n->getStringValue()),
            'address5' => fn(ParseNode $n) => $o->setAddress5($n->getStringValue()),
            'amountPrepaid' => fn(ParseNode $n) => $o->setAmountPrepaid($n->getFloatValue()),
            'analysis1' => fn(ParseNode $n) => $o->setAnalysis1($n->getStringValue()),
            'analysis2' => fn(ParseNode $n) => $o->setAnalysis2($n->getStringValue()),
            'analysis3' => fn(ParseNode $n) => $o->setAnalysis3($n->getStringValue()),
            'bankRef' => fn(ParseNode $n) => $o->setBankRef($n->getStringValue()),
            'cAddress1' => fn(ParseNode $n) => $o->setCAddress1($n->getStringValue()),
            'cAddress2' => fn(ParseNode $n) => $o->setCAddress2($n->getStringValue()),
            'cAddress3' => fn(ParseNode $n) => $o->setCAddress3($n->getStringValue()),
            'cAddress4' => fn(ParseNode $n) => $o->setCAddress4($n->getStringValue()),
            'cAddress5' => fn(ParseNode $n) => $o->setCAddress5($n->getStringValue()),
            'carrDeptName' => fn(ParseNode $n) => $o->setCarrDeptName($n->getStringValue()),
            'carrDeptNumber' => fn(ParseNode $n) => $o->setCarrDeptNumber($n->getIntegerValue()),
            'carrGross' => fn(ParseNode $n) => $o->setCarrGross($n->getFloatValue()),
            'carrNet' => fn(ParseNode $n) => $o->setCarrNet($n->getFloatValue()),
            'carrNomCode' => fn(ParseNode $n) => $o->setCarrNomCode($n->getStringValue()),
            'carrTax' => fn(ParseNode $n) => $o->setCarrTax($n->getFloatValue()),
            'carrTaxCode' => fn(ParseNode $n) => $o->setCarrTaxCode($n->getEnumValue(SalesInvoiceAttributesRead_carrTaxCode::class)),
            'consignment' => fn(ParseNode $n) => $o->setConsignment($n->getStringValue()),
            'contactName' => fn(ParseNode $n) => $o->setContactName($n->getStringValue()),
            'containsCisReverseChargeItems' => fn(ParseNode $n) => $o->setContainsCisReverseChargeItems($n->getIntegerValue()),
            'courierName' => fn(ParseNode $n) => $o->setCourierName($n->getStringValue()),
            'courierNumber' => fn(ParseNode $n) => $o->setCourierNumber($n->getIntegerValue()),
            'currency' => fn(ParseNode $n) => $o->setCurrency($n->getIntegerValue()),
            'currencyType' => fn(ParseNode $n) => $o->setCurrencyType($n->getIntegerValue()),
            'custDiscRate' => fn(ParseNode $n) => $o->setCustDiscRate($n->getFloatValue()),
            'custOrderNumber' => fn(ParseNode $n) => $o->setCustOrderNumber($n->getStringValue()),
            'custTelNumber' => fn(ParseNode $n) => $o->setCustTelNumber($n->getStringValue()),
            'delAddress1' => fn(ParseNode $n) => $o->setDelAddress1($n->getStringValue()),
            'delAddress2' => fn(ParseNode $n) => $o->setDelAddress2($n->getStringValue()),
            'delAddress3' => fn(ParseNode $n) => $o->setDelAddress3($n->getStringValue()),
            'delAddress4' => fn(ParseNode $n) => $o->setDelAddress4($n->getStringValue()),
            'delAddress5' => fn(ParseNode $n) => $o->setDelAddress5($n->getStringValue()),
            'delName' => fn(ParseNode $n) => $o->setDelName($n->getStringValue()),
            'dunsNumber' => fn(ParseNode $n) => $o->setDunsNumber($n->getStringValue()),
            'euroGross' => fn(ParseNode $n) => $o->setEuroGross($n->getFloatValue()),
            'euroRate' => fn(ParseNode $n) => $o->setEuroRate($n->getFloatValue()),
            'foreignAmountPrepaid' => fn(ParseNode $n) => $o->setForeignAmountPrepaid($n->getFloatValue()),
            'foreignCarrGross' => fn(ParseNode $n) => $o->setForeignCarrGross($n->getFloatValue()),
            'foreignCarrNet' => fn(ParseNode $n) => $o->setForeignCarrNet($n->getFloatValue()),
            'foreignCarrTax' => fn(ParseNode $n) => $o->setForeignCarrTax($n->getFloatValue()),
            'foreignInvoiceGross' => fn(ParseNode $n) => $o->setForeignInvoiceGross($n->getFloatValue()),
            'foreignInvoiceNet' => fn(ParseNode $n) => $o->setForeignInvoiceNet($n->getFloatValue()),
            'foreignInvoiceTax' => fn(ParseNode $n) => $o->setForeignInvoiceTax($n->getFloatValue()),
            'foreignItemsGross' => fn(ParseNode $n) => $o->setForeignItemsGross($n->getFloatValue()),
            'foreignItemsNet' => fn(ParseNode $n) => $o->setForeignItemsNet($n->getFloatValue()),
            'foreignItemsTax' => fn(ParseNode $n) => $o->setForeignItemsTax($n->getFloatValue()),
            'foreignRate' => fn(ParseNode $n) => $o->setForeignRate($n->getFloatValue()),
            'foreignSettlementDiscAmount' => fn(ParseNode $n) => $o->setForeignSettlementDiscAmount($n->getFloatValue()),
            'foreignSettlementTotal' => fn(ParseNode $n) => $o->setForeignSettlementTotal($n->getFloatValue()),
            'gdnNumber' => fn(ParseNode $n) => $o->setGdnNumber($n->getIntegerValue()),
            'globalDeptName' => fn(ParseNode $n) => $o->setGlobalDeptName($n->getStringValue()),
            'globalDeptNumber' => fn(ParseNode $n) => $o->setGlobalDeptNumber($n->getIntegerValue()),
            'globalDetails' => fn(ParseNode $n) => $o->setGlobalDetails($n->getStringValue()),
            'globalNomCode' => fn(ParseNode $n) => $o->setGlobalNomCode($n->getStringValue()),
            'globalTaxCode' => fn(ParseNode $n) => $o->setGlobalTaxCode($n->getEnumValue(SalesInvoiceAttributesRead_globalTaxCode::class)),
            'incoterms' => fn(ParseNode $n) => $o->setIncoterms($n->getIntegerValue()),
            'incotermsText' => fn(ParseNode $n) => $o->setIncotermsText($n->getStringValue()),
            'invoiceDate' => fn(ParseNode $n) => $o->setInvoiceDate($n->getDateTimeValue()),
            'invoiceGross' => fn(ParseNode $n) => $o->setInvoiceGross($n->getFloatValue()),
            'invoiceNet' => fn(ParseNode $n) => $o->setInvoiceNet($n->getFloatValue()),
            'invoiceOrCredit' => fn(ParseNode $n) => $o->setInvoiceOrCredit($n->getStringValue()),
            'invoicePaymentId' => fn(ParseNode $n) => $o->setInvoicePaymentId($n->getStringValue()),
            'invoiceTax' => fn(ParseNode $n) => $o->setInvoiceTax($n->getFloatValue()),
            'invoiceType' => fn(ParseNode $n) => $o->setInvoiceType($n->getStringValue()),
            'invoiceTypeCode' => fn(ParseNode $n) => $o->setInvoiceTypeCode($n->getIntegerValue()),
            'itemsGross' => fn(ParseNode $n) => $o->setItemsGross($n->getFloatValue()),
            'itemsNet' => fn(ParseNode $n) => $o->setItemsNet($n->getFloatValue()),
            'itemsTax' => fn(ParseNode $n) => $o->setItemsTax($n->getFloatValue()),
            'name' => fn(ParseNode $n) => $o->setName($n->getStringValue()),
            'netAmount1' => fn(ParseNode $n) => $o->setNetAmount1($n->getFloatValue()),
            'netAmount2' => fn(ParseNode $n) => $o->setNetAmount2($n->getFloatValue()),
            'netAmount3' => fn(ParseNode $n) => $o->setNetAmount3($n->getFloatValue()),
            'netAmount4' => fn(ParseNode $n) => $o->setNetAmount4($n->getFloatValue()),
            'netAmount5' => fn(ParseNode $n) => $o->setNetAmount5($n->getFloatValue()),
            'netValueDiscountComment1' => fn(ParseNode $n) => $o->setNetValueDiscountComment1($n->getStringValue()),
            'netValueDiscountComment2' => fn(ParseNode $n) => $o->setNetValueDiscountComment2($n->getStringValue()),
            'netValueDiscountDesc' => fn(ParseNode $n) => $o->setNetValueDiscountDesc($n->getStringValue()),
            'netValueDiscountNet' => fn(ParseNode $n) => $o->setNetValueDiscountNet($n->getFloatValue()),
            'netValueDiscountNetBase' => fn(ParseNode $n) => $o->setNetValueDiscountNetBase($n->getFloatValue()),
            'netValueDiscountRate' => fn(ParseNode $n) => $o->setNetValueDiscountRate($n->getFloatValue()),
            'notes1' => fn(ParseNode $n) => $o->setNotes1($n->getStringValue()),
            'notes2' => fn(ParseNode $n) => $o->setNotes2($n->getStringValue()),
            'notes3' => fn(ParseNode $n) => $o->setNotes3($n->getStringValue()),
            'orderNumber' => fn(ParseNode $n) => $o->setOrderNumber($n->getStringValue()),
            'orderNumberNumeric' => fn(ParseNode $n) => $o->setOrderNumberNumeric($n->getIntegerValue()),
            'ossCountryOfVat' => fn(ParseNode $n) => $o->setOssCountryOfVat($n->getStringValue()),
            'ossReportingType' => fn(ParseNode $n) => $o->setOssReportingType($n->getIntegerValue()),
            'ossReportingTypeName' => fn(ParseNode $n) => $o->setOssReportingTypeName($n->getStringValue()),
            'paymentDueDate' => fn(ParseNode $n) => $o->setPaymentDueDate($n->getDateTimeValue()),
            'paymentRef' => fn(ParseNode $n) => $o->setPaymentRef($n->getStringValue()),
            'paymentType' => fn(ParseNode $n) => $o->setPaymentType($n->getIntegerValue()),
            'posted' => fn(ParseNode $n) => $o->setPosted($n->getStringValue()),
            'postedCode' => fn(ParseNode $n) => $o->setPostedCode($n->getIntegerValue()),
            'printed' => fn(ParseNode $n) => $o->setPrinted($n->getStringValue()),
            'printedCode' => fn(ParseNode $n) => $o->setPrintedCode($n->getIntegerValue()),
            'quoteExpiryDate' => fn(ParseNode $n) => $o->setQuoteExpiryDate($n->getDateTimeValue()),
            'quoteStatus' => fn(ParseNode $n) => $o->setQuoteStatus($n->getEnumValue(SalesInvoiceAttributesRead_quoteStatus::class)),
            'quoteStatusId' => fn(ParseNode $n) => $o->setQuoteStatusId($n->getIntegerValue()),
            'recordCreateDate' => fn(ParseNode $n) => $o->setRecordCreateDate($n->getDateTimeValue()),
            'recordDeleted' => fn(ParseNode $n) => $o->setRecordDeleted($n->getIntegerValue()),
            'recordModifyDate' => fn(ParseNode $n) => $o->setRecordModifyDate($n->getDateTimeValue()),
            'recurringRef' => fn(ParseNode $n) => $o->setRecurringRef($n->getStringValue()),
            'resubmitInvoicePaymentRequired' => fn(ParseNode $n) => $o->setResubmitInvoicePaymentRequired($n->getIntegerValue()),
            'settlementDiscAmount' => fn(ParseNode $n) => $o->setSettlementDiscAmount($n->getFloatValue()),
            'settlementDiscRate' => fn(ParseNode $n) => $o->setSettlementDiscRate($n->getFloatValue()),
            'settlementDueDays' => fn(ParseNode $n) => $o->setSettlementDueDays($n->getIntegerValue()),
            'settlementTotal' => fn(ParseNode $n) => $o->setSettlementTotal($n->getFloatValue()),
            'takenBy' => fn(ParseNode $n) => $o->setTakenBy($n->getStringValue()),
            'taxAmount1' => fn(ParseNode $n) => $o->setTaxAmount1($n->getFloatValue()),
            'taxAmount2' => fn(ParseNode $n) => $o->setTaxAmount2($n->getFloatValue()),
            'taxAmount3' => fn(ParseNode $n) => $o->setTaxAmount3($n->getFloatValue()),
            'taxAmount4' => fn(ParseNode $n) => $o->setTaxAmount4($n->getFloatValue()),
            'taxAmount5' => fn(ParseNode $n) => $o->setTaxAmount5($n->getFloatValue()),
            'taxRate1' => fn(ParseNode $n) => $o->setTaxRate1($n->getFloatValue()),
            'taxRate2' => fn(ParseNode $n) => $o->setTaxRate2($n->getFloatValue()),
            'taxRate3' => fn(ParseNode $n) => $o->setTaxRate3($n->getFloatValue()),
            'taxRate4' => fn(ParseNode $n) => $o->setTaxRate4($n->getFloatValue()),
            'taxRate5' => fn(ParseNode $n) => $o->setTaxRate5($n->getFloatValue()),
            'vatRegNumber' => fn(ParseNode $n) => $o->setVatRegNumber($n->getStringValue()),
        ];
    }

    /**
     * Gets the foreignAmountPrepaid property value. The foreignAmountPrepaid property
     * @return float|null
    */
    public function getForeignAmountPrepaid(): ?float {
        return $this->foreignAmountPrepaid;
    }

    /**
     * Gets the foreignCarrGross property value. The foreignCarrGross property
     * @return float|null
    */
    public function getForeignCarrGross(): ?float {
        return $this->foreignCarrGross;
    }

    /**
     * Gets the foreignCarrNet property value. The foreignCarrNet property
     * @return float|null
    */
    public function getForeignCarrNet(): ?float {
        return $this->foreignCarrNet;
    }

    /**
     * Gets the foreignCarrTax property value. The foreignCarrTax property
     * @return float|null
    */
    public function getForeignCarrTax(): ?float {
        return $this->foreignCarrTax;
    }

    /**
     * Gets the foreignInvoiceGross property value. The foreignInvoiceGross property
     * @return float|null
    */
    public function getForeignInvoiceGross(): ?float {
        return $this->foreignInvoiceGross;
    }

    /**
     * Gets the foreignInvoiceNet property value. The foreignInvoiceNet property
     * @return float|null
    */
    public function getForeignInvoiceNet(): ?float {
        return $this->foreignInvoiceNet;
    }

    /**
     * Gets the foreignInvoiceTax property value. The foreignInvoiceTax property
     * @return float|null
    */
    public function getForeignInvoiceTax(): ?float {
        return $this->foreignInvoiceTax;
    }

    /**
     * Gets the foreignItemsGross property value. The foreignItemsGross property
     * @return float|null
    */
    public function getForeignItemsGross(): ?float {
        return $this->foreignItemsGross;
    }

    /**
     * Gets the foreignItemsNet property value. The foreignItemsNet property
     * @return float|null
    */
    public function getForeignItemsNet(): ?float {
        return $this->foreignItemsNet;
    }

    /**
     * Gets the foreignItemsTax property value. The foreignItemsTax property
     * @return float|null
    */
    public function getForeignItemsTax(): ?float {
        return $this->foreignItemsTax;
    }

    /**
     * Gets the foreignRate property value. The foreignRate property
     * @return float|null
    */
    public function getForeignRate(): ?float {
        return $this->foreignRate;
    }

    /**
     * Gets the foreignSettlementDiscAmount property value. The foreignSettlementDiscAmount property
     * @return float|null
    */
    public function getForeignSettlementDiscAmount(): ?float {
        return $this->foreignSettlementDiscAmount;
    }

    /**
     * Gets the foreignSettlementTotal property value. The foreignSettlementTotal property
     * @return float|null
    */
    public function getForeignSettlementTotal(): ?float {
        return $this->foreignSettlementTotal;
    }

    /**
     * Gets the gdnNumber property value. The gdnNumber property
     * @return int|null
    */
    public function getGdnNumber(): ?int {
        return $this->gdnNumber;
    }

    /**
     * Gets the globalDeptName property value. The globalDeptName property
     * @return string|null
    */
    public function getGlobalDeptName(): ?string {
        return $this->globalDeptName;
    }

    /**
     * Gets the globalDeptNumber property value. The globalDeptNumber property
     * @return int|null
    */
    public function getGlobalDeptNumber(): ?int {
        return $this->globalDeptNumber;
    }

    /**
     * Gets the globalDetails property value. The globalDetails property
     * @return string|null
    */
    public function getGlobalDetails(): ?string {
        return $this->globalDetails;
    }

    /**
     * Gets the globalNomCode property value. The globalNomCode property
     * @return string|null
    */
    public function getGlobalNomCode(): ?string {
        return $this->globalNomCode;
    }

    /**
     * Gets the globalTaxCode property value. The globalTaxCode property
     * @return SalesInvoiceAttributesRead_globalTaxCode|null
    */
    public function getGlobalTaxCode(): ?SalesInvoiceAttributesRead_globalTaxCode {
        return $this->globalTaxCode;
    }

    /**
     * Gets the incoterms property value. The incoterms property
     * @return int|null
    */
    public function getIncoterms(): ?int {
        return $this->incoterms;
    }

    /**
     * Gets the incotermsText property value. The incotermsText property
     * @return string|null
    */
    public function getIncotermsText(): ?string {
        return $this->incotermsText;
    }

    /**
     * Gets the invoiceDate property value. The invoiceDate property
     * @return DateTime|null
    */
    public function getInvoiceDate(): ?DateTime {
        return $this->invoiceDate;
    }

    /**
     * Gets the invoiceGross property value. The invoiceGross property
     * @return float|null
    */
    public function getInvoiceGross(): ?float {
        return $this->invoiceGross;
    }

    /**
     * Gets the invoiceNet property value. The invoiceNet property
     * @return float|null
    */
    public function getInvoiceNet(): ?float {
        return $this->invoiceNet;
    }

    /**
     * Gets the invoiceOrCredit property value. The invoiceOrCredit property
     * @return string|null
    */
    public function getInvoiceOrCredit(): ?string {
        return $this->invoiceOrCredit;
    }

    /**
     * Gets the invoicePaymentId property value. The invoicePaymentId property
     * @return string|null
    */
    public function getInvoicePaymentId(): ?string {
        return $this->invoicePaymentId;
    }

    /**
     * Gets the invoiceTax property value. The invoiceTax property
     * @return float|null
    */
    public function getInvoiceTax(): ?float {
        return $this->invoiceTax;
    }

    /**
     * Gets the invoiceType property value. The invoiceType property
     * @return string|null
    */
    public function getInvoiceType(): ?string {
        return $this->invoiceType;
    }

    /**
     * Gets the invoiceTypeCode property value. The invoiceTypeCode property
     * @return int|null
    */
    public function getInvoiceTypeCode(): ?int {
        return $this->invoiceTypeCode;
    }

    /**
     * Gets the itemsGross property value. The itemsGross property
     * @return float|null
    */
    public function getItemsGross(): ?float {
        return $this->itemsGross;
    }

    /**
     * Gets the itemsNet property value. The itemsNet property
     * @return float|null
    */
    public function getItemsNet(): ?float {
        return $this->itemsNet;
    }

    /**
     * Gets the itemsTax property value. The itemsTax property
     * @return float|null
    */
    public function getItemsTax(): ?float {
        return $this->itemsTax;
    }

    /**
     * Gets the name property value. The name property
     * @return string|null
    */
    public function getName(): ?string {
        return $this->name;
    }

    /**
     * Gets the netAmount1 property value. The netAmount1 property
     * @return float|null
    */
    public function getNetAmount1(): ?float {
        return $this->netAmount1;
    }

    /**
     * Gets the netAmount2 property value. The netAmount2 property
     * @return float|null
    */
    public function getNetAmount2(): ?float {
        return $this->netAmount2;
    }

    /**
     * Gets the netAmount3 property value. The netAmount3 property
     * @return float|null
    */
    public function getNetAmount3(): ?float {
        return $this->netAmount3;
    }

    /**
     * Gets the netAmount4 property value. The netAmount4 property
     * @return float|null
    */
    public function getNetAmount4(): ?float {
        return $this->netAmount4;
    }

    /**
     * Gets the netAmount5 property value. The netAmount5 property
     * @return float|null
    */
    public function getNetAmount5(): ?float {
        return $this->netAmount5;
    }

    /**
     * Gets the netValueDiscountComment1 property value. The netValueDiscountComment1 property
     * @return string|null
    */
    public function getNetValueDiscountComment1(): ?string {
        return $this->netValueDiscountComment1;
    }

    /**
     * Gets the netValueDiscountComment2 property value. The netValueDiscountComment2 property
     * @return string|null
    */
    public function getNetValueDiscountComment2(): ?string {
        return $this->netValueDiscountComment2;
    }

    /**
     * Gets the netValueDiscountDesc property value. The netValueDiscountDesc property
     * @return string|null
    */
    public function getNetValueDiscountDesc(): ?string {
        return $this->netValueDiscountDesc;
    }

    /**
     * Gets the netValueDiscountNet property value. The netValueDiscountNet property
     * @return float|null
    */
    public function getNetValueDiscountNet(): ?float {
        return $this->netValueDiscountNet;
    }

    /**
     * Gets the netValueDiscountNetBase property value. The netValueDiscountNetBase property
     * @return float|null
    */
    public function getNetValueDiscountNetBase(): ?float {
        return $this->netValueDiscountNetBase;
    }

    /**
     * Gets the netValueDiscountRate property value. The netValueDiscountRate property
     * @return float|null
    */
    public function getNetValueDiscountRate(): ?float {
        return $this->netValueDiscountRate;
    }

    /**
     * Gets the notes1 property value. The notes1 property
     * @return string|null
    */
    public function getNotes1(): ?string {
        return $this->notes1;
    }

    /**
     * Gets the notes2 property value. The notes2 property
     * @return string|null
    */
    public function getNotes2(): ?string {
        return $this->notes2;
    }

    /**
     * Gets the notes3 property value. The notes3 property
     * @return string|null
    */
    public function getNotes3(): ?string {
        return $this->notes3;
    }

    /**
     * Gets the orderNumber property value. The orderNumber property
     * @return string|null
    */
    public function getOrderNumber(): ?string {
        return $this->orderNumber;
    }

    /**
     * Gets the orderNumberNumeric property value. The orderNumberNumeric property
     * @return int|null
    */
    public function getOrderNumberNumeric(): ?int {
        return $this->orderNumberNumeric;
    }

    /**
     * Gets the ossCountryOfVat property value. The ossCountryOfVat property
     * @return string|null
    */
    public function getOssCountryOfVat(): ?string {
        return $this->ossCountryOfVat;
    }

    /**
     * Gets the ossReportingType property value. The ossReportingType property
     * @return int|null
    */
    public function getOssReportingType(): ?int {
        return $this->ossReportingType;
    }

    /**
     * Gets the ossReportingTypeName property value. The ossReportingTypeName property
     * @return string|null
    */
    public function getOssReportingTypeName(): ?string {
        return $this->ossReportingTypeName;
    }

    /**
     * Gets the paymentDueDate property value. The paymentDueDate property
     * @return DateTime|null
    */
    public function getPaymentDueDate(): ?DateTime {
        return $this->paymentDueDate;
    }

    /**
     * Gets the paymentRef property value. The paymentRef property
     * @return string|null
    */
    public function getPaymentRef(): ?string {
        return $this->paymentRef;
    }

    /**
     * Gets the paymentType property value. The paymentType property
     * @return int|null
    */
    public function getPaymentType(): ?int {
        return $this->paymentType;
    }

    /**
     * Gets the posted property value. The posted property
     * @return string|null
    */
    public function getPosted(): ?string {
        return $this->posted;
    }

    /**
     * Gets the postedCode property value. The postedCode property
     * @return int|null
    */
    public function getPostedCode(): ?int {
        return $this->postedCode;
    }

    /**
     * Gets the printed property value. The printed property
     * @return string|null
    */
    public function getPrinted(): ?string {
        return $this->printed;
    }

    /**
     * Gets the printedCode property value. The printedCode property
     * @return int|null
    */
    public function getPrintedCode(): ?int {
        return $this->printedCode;
    }

    /**
     * Gets the quoteExpiryDate property value. The quoteExpiryDate property
     * @return DateTime|null
    */
    public function getQuoteExpiryDate(): ?DateTime {
        return $this->quoteExpiryDate;
    }

    /**
     * Gets the quoteStatus property value. The quoteStatus property
     * @return SalesInvoiceAttributesRead_quoteStatus|null
    */
    public function getQuoteStatus(): ?SalesInvoiceAttributesRead_quoteStatus {
        return $this->quoteStatus;
    }

    /**
     * Gets the quoteStatusId property value. The quoteStatusId property
     * @return int|null
    */
    public function getQuoteStatusId(): ?int {
        return $this->quoteStatusId;
    }

    /**
     * Gets the recordCreateDate property value. The recordCreateDate property
     * @return DateTime|null
    */
    public function getRecordCreateDate(): ?DateTime {
        return $this->recordCreateDate;
    }

    /**
     * Gets the recordDeleted property value. The recordDeleted property
     * @return int|null
    */
    public function getRecordDeleted(): ?int {
        return $this->recordDeleted;
    }

    /**
     * Gets the recordModifyDate property value. The recordModifyDate property
     * @return DateTime|null
    */
    public function getRecordModifyDate(): ?DateTime {
        return $this->recordModifyDate;
    }

    /**
     * Gets the recurringRef property value. The recurringRef property
     * @return string|null
    */
    public function getRecurringRef(): ?string {
        return $this->recurringRef;
    }

    /**
     * Gets the resubmitInvoicePaymentRequired property value. The resubmitInvoicePaymentRequired property
     * @return int|null
    */
    public function getResubmitInvoicePaymentRequired(): ?int {
        return $this->resubmitInvoicePaymentRequired;
    }

    /**
     * Gets the settlementDiscAmount property value. The settlementDiscAmount property
     * @return float|null
    */
    public function getSettlementDiscAmount(): ?float {
        return $this->settlementDiscAmount;
    }

    /**
     * Gets the settlementDiscRate property value. The settlementDiscRate property
     * @return float|null
    */
    public function getSettlementDiscRate(): ?float {
        return $this->settlementDiscRate;
    }

    /**
     * Gets the settlementDueDays property value. The settlementDueDays property
     * @return int|null
    */
    public function getSettlementDueDays(): ?int {
        return $this->settlementDueDays;
    }

    /**
     * Gets the settlementTotal property value. The settlementTotal property
     * @return float|null
    */
    public function getSettlementTotal(): ?float {
        return $this->settlementTotal;
    }

    /**
     * Gets the takenBy property value. The takenBy property
     * @return string|null
    */
    public function getTakenBy(): ?string {
        return $this->takenBy;
    }

    /**
     * Gets the taxAmount1 property value. The taxAmount1 property
     * @return float|null
    */
    public function getTaxAmount1(): ?float {
        return $this->taxAmount1;
    }

    /**
     * Gets the taxAmount2 property value. The taxAmount2 property
     * @return float|null
    */
    public function getTaxAmount2(): ?float {
        return $this->taxAmount2;
    }

    /**
     * Gets the taxAmount3 property value. The taxAmount3 property
     * @return float|null
    */
    public function getTaxAmount3(): ?float {
        return $this->taxAmount3;
    }

    /**
     * Gets the taxAmount4 property value. The taxAmount4 property
     * @return float|null
    */
    public function getTaxAmount4(): ?float {
        return $this->taxAmount4;
    }

    /**
     * Gets the taxAmount5 property value. The taxAmount5 property
     * @return float|null
    */
    public function getTaxAmount5(): ?float {
        return $this->taxAmount5;
    }

    /**
     * Gets the taxRate1 property value. The taxRate1 property
     * @return float|null
    */
    public function getTaxRate1(): ?float {
        return $this->taxRate1;
    }

    /**
     * Gets the taxRate2 property value. The taxRate2 property
     * @return float|null
    */
    public function getTaxRate2(): ?float {
        return $this->taxRate2;
    }

    /**
     * Gets the taxRate3 property value. The taxRate3 property
     * @return float|null
    */
    public function getTaxRate3(): ?float {
        return $this->taxRate3;
    }

    /**
     * Gets the taxRate4 property value. The taxRate4 property
     * @return float|null
    */
    public function getTaxRate4(): ?float {
        return $this->taxRate4;
    }

    /**
     * Gets the taxRate5 property value. The taxRate5 property
     * @return float|null
    */
    public function getTaxRate5(): ?float {
        return $this->taxRate5;
    }

    /**
     * Gets the vatRegNumber property value. The vatRegNumber property
     * @return string|null
    */
    public function getVatRegNumber(): ?string {
        return $this->vatRegNumber;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeStringValue('address1', $this->getAddress1());
        $writer->writeStringValue('address2', $this->getAddress2());
        $writer->writeStringValue('address3', $this->getAddress3());
        $writer->writeStringValue('address4', $this->getAddress4());
        $writer->writeStringValue('address5', $this->getAddress5());
        $writer->writeFloatValue('amountPrepaid', $this->getAmountPrepaid());
        $writer->writeStringValue('analysis1', $this->getAnalysis1());
        $writer->writeStringValue('analysis2', $this->getAnalysis2());
        $writer->writeStringValue('analysis3', $this->getAnalysis3());
        $writer->writeStringValue('bankRef', $this->getBankRef());
        $writer->writeStringValue('cAddress1', $this->getCAddress1());
        $writer->writeStringValue('cAddress2', $this->getCAddress2());
        $writer->writeStringValue('cAddress3', $this->getCAddress3());
        $writer->writeStringValue('cAddress4', $this->getCAddress4());
        $writer->writeStringValue('cAddress5', $this->getCAddress5());
        $writer->writeStringValue('carrDeptName', $this->getCarrDeptName());
        $writer->writeIntegerValue('carrDeptNumber', $this->getCarrDeptNumber());
        $writer->writeFloatValue('carrGross', $this->getCarrGross());
        $writer->writeFloatValue('carrNet', $this->getCarrNet());
        $writer->writeStringValue('carrNomCode', $this->getCarrNomCode());
        $writer->writeFloatValue('carrTax', $this->getCarrTax());
        $writer->writeEnumValue('carrTaxCode', $this->getCarrTaxCode());
        $writer->writeStringValue('consignment', $this->getConsignment());
        $writer->writeStringValue('contactName', $this->getContactName());
        $writer->writeIntegerValue('containsCisReverseChargeItems', $this->getContainsCisReverseChargeItems());
        $writer->writeStringValue('courierName', $this->getCourierName());
        $writer->writeIntegerValue('courierNumber', $this->getCourierNumber());
        $writer->writeIntegerValue('currency', $this->getCurrency());
        $writer->writeIntegerValue('currencyType', $this->getCurrencyType());
        $writer->writeFloatValue('custDiscRate', $this->getCustDiscRate());
        $writer->writeStringValue('custOrderNumber', $this->getCustOrderNumber());
        $writer->writeStringValue('custTelNumber', $this->getCustTelNumber());
        $writer->writeStringValue('delAddress1', $this->getDelAddress1());
        $writer->writeStringValue('delAddress2', $this->getDelAddress2());
        $writer->writeStringValue('delAddress3', $this->getDelAddress3());
        $writer->writeStringValue('delAddress4', $this->getDelAddress4());
        $writer->writeStringValue('delAddress5', $this->getDelAddress5());
        $writer->writeStringValue('delName', $this->getDelName());
        $writer->writeStringValue('dunsNumber', $this->getDunsNumber());
        $writer->writeFloatValue('euroGross', $this->getEuroGross());
        $writer->writeFloatValue('euroRate', $this->getEuroRate());
        $writer->writeFloatValue('foreignAmountPrepaid', $this->getForeignAmountPrepaid());
        $writer->writeFloatValue('foreignCarrGross', $this->getForeignCarrGross());
        $writer->writeFloatValue('foreignCarrNet', $this->getForeignCarrNet());
        $writer->writeFloatValue('foreignCarrTax', $this->getForeignCarrTax());
        $writer->writeFloatValue('foreignInvoiceGross', $this->getForeignInvoiceGross());
        $writer->writeFloatValue('foreignInvoiceNet', $this->getForeignInvoiceNet());
        $writer->writeFloatValue('foreignInvoiceTax', $this->getForeignInvoiceTax());
        $writer->writeFloatValue('foreignItemsGross', $this->getForeignItemsGross());
        $writer->writeFloatValue('foreignItemsNet', $this->getForeignItemsNet());
        $writer->writeFloatValue('foreignItemsTax', $this->getForeignItemsTax());
        $writer->writeFloatValue('foreignRate', $this->getForeignRate());
        $writer->writeFloatValue('foreignSettlementDiscAmount', $this->getForeignSettlementDiscAmount());
        $writer->writeFloatValue('foreignSettlementTotal', $this->getForeignSettlementTotal());
        $writer->writeIntegerValue('gdnNumber', $this->getGdnNumber());
        $writer->writeStringValue('globalDeptName', $this->getGlobalDeptName());
        $writer->writeIntegerValue('globalDeptNumber', $this->getGlobalDeptNumber());
        $writer->writeStringValue('globalDetails', $this->getGlobalDetails());
        $writer->writeStringValue('globalNomCode', $this->getGlobalNomCode());
        $writer->writeEnumValue('globalTaxCode', $this->getGlobalTaxCode());
        $writer->writeIntegerValue('incoterms', $this->getIncoterms());
        $writer->writeStringValue('incotermsText', $this->getIncotermsText());
        $writer->writeDateTimeValue('invoiceDate', $this->getInvoiceDate());
        $writer->writeFloatValue('invoiceGross', $this->getInvoiceGross());
        $writer->writeFloatValue('invoiceNet', $this->getInvoiceNet());
        $writer->writeStringValue('invoiceOrCredit', $this->getInvoiceOrCredit());
        $writer->writeStringValue('invoicePaymentId', $this->getInvoicePaymentId());
        $writer->writeFloatValue('invoiceTax', $this->getInvoiceTax());
        $writer->writeStringValue('invoiceType', $this->getInvoiceType());
        $writer->writeIntegerValue('invoiceTypeCode', $this->getInvoiceTypeCode());
        $writer->writeFloatValue('itemsGross', $this->getItemsGross());
        $writer->writeFloatValue('itemsNet', $this->getItemsNet());
        $writer->writeFloatValue('itemsTax', $this->getItemsTax());
        $writer->writeStringValue('name', $this->getName());
        $writer->writeFloatValue('netAmount1', $this->getNetAmount1());
        $writer->writeFloatValue('netAmount2', $this->getNetAmount2());
        $writer->writeFloatValue('netAmount3', $this->getNetAmount3());
        $writer->writeFloatValue('netAmount4', $this->getNetAmount4());
        $writer->writeFloatValue('netAmount5', $this->getNetAmount5());
        $writer->writeStringValue('netValueDiscountComment1', $this->getNetValueDiscountComment1());
        $writer->writeStringValue('netValueDiscountComment2', $this->getNetValueDiscountComment2());
        $writer->writeStringValue('netValueDiscountDesc', $this->getNetValueDiscountDesc());
        $writer->writeFloatValue('netValueDiscountNet', $this->getNetValueDiscountNet());
        $writer->writeFloatValue('netValueDiscountNetBase', $this->getNetValueDiscountNetBase());
        $writer->writeFloatValue('netValueDiscountRate', $this->getNetValueDiscountRate());
        $writer->writeStringValue('notes1', $this->getNotes1());
        $writer->writeStringValue('notes2', $this->getNotes2());
        $writer->writeStringValue('notes3', $this->getNotes3());
        $writer->writeStringValue('orderNumber', $this->getOrderNumber());
        $writer->writeIntegerValue('orderNumberNumeric', $this->getOrderNumberNumeric());
        $writer->writeStringValue('ossCountryOfVat', $this->getOssCountryOfVat());
        $writer->writeIntegerValue('ossReportingType', $this->getOssReportingType());
        $writer->writeStringValue('ossReportingTypeName', $this->getOssReportingTypeName());
        $writer->writeDateTimeValue('paymentDueDate', $this->getPaymentDueDate());
        $writer->writeStringValue('paymentRef', $this->getPaymentRef());
        $writer->writeIntegerValue('paymentType', $this->getPaymentType());
        $writer->writeStringValue('posted', $this->getPosted());
        $writer->writeIntegerValue('postedCode', $this->getPostedCode());
        $writer->writeStringValue('printed', $this->getPrinted());
        $writer->writeIntegerValue('printedCode', $this->getPrintedCode());
        $writer->writeDateTimeValue('quoteExpiryDate', $this->getQuoteExpiryDate());
        $writer->writeEnumValue('quoteStatus', $this->getQuoteStatus());
        $writer->writeIntegerValue('quoteStatusId', $this->getQuoteStatusId());
        $writer->writeDateTimeValue('recordCreateDate', $this->getRecordCreateDate());
        $writer->writeIntegerValue('recordDeleted', $this->getRecordDeleted());
        $writer->writeDateTimeValue('recordModifyDate', $this->getRecordModifyDate());
        $writer->writeStringValue('recurringRef', $this->getRecurringRef());
        $writer->writeIntegerValue('resubmitInvoicePaymentRequired', $this->getResubmitInvoicePaymentRequired());
        $writer->writeFloatValue('settlementDiscAmount', $this->getSettlementDiscAmount());
        $writer->writeFloatValue('settlementDiscRate', $this->getSettlementDiscRate());
        $writer->writeIntegerValue('settlementDueDays', $this->getSettlementDueDays());
        $writer->writeFloatValue('settlementTotal', $this->getSettlementTotal());
        $writer->writeStringValue('takenBy', $this->getTakenBy());
        $writer->writeFloatValue('taxAmount1', $this->getTaxAmount1());
        $writer->writeFloatValue('taxAmount2', $this->getTaxAmount2());
        $writer->writeFloatValue('taxAmount3', $this->getTaxAmount3());
        $writer->writeFloatValue('taxAmount4', $this->getTaxAmount4());
        $writer->writeFloatValue('taxAmount5', $this->getTaxAmount5());
        $writer->writeFloatValue('taxRate1', $this->getTaxRate1());
        $writer->writeFloatValue('taxRate2', $this->getTaxRate2());
        $writer->writeFloatValue('taxRate3', $this->getTaxRate3());
        $writer->writeFloatValue('taxRate4', $this->getTaxRate4());
        $writer->writeFloatValue('taxRate5', $this->getTaxRate5());
        $writer->writeStringValue('vatRegNumber', $this->getVatRegNumber());
    }

    /**
     * Sets the address1 property value. The address1 property
     * @param string|null $value Value to set for the address1 property.
    */
    public function setAddress1(?string $value): void {
        $this->address1 = $value;
    }

    /**
     * Sets the address2 property value. The address2 property
     * @param string|null $value Value to set for the address2 property.
    */
    public function setAddress2(?string $value): void {
        $this->address2 = $value;
    }

    /**
     * Sets the address3 property value. The address3 property
     * @param string|null $value Value to set for the address3 property.
    */
    public function setAddress3(?string $value): void {
        $this->address3 = $value;
    }

    /**
     * Sets the address4 property value. The address4 property
     * @param string|null $value Value to set for the address4 property.
    */
    public function setAddress4(?string $value): void {
        $this->address4 = $value;
    }

    /**
     * Sets the address5 property value. The address5 property
     * @param string|null $value Value to set for the address5 property.
    */
    public function setAddress5(?string $value): void {
        $this->address5 = $value;
    }

    /**
     * Sets the amountPrepaid property value. The amountPrepaid property
     * @param float|null $value Value to set for the amountPrepaid property.
    */
    public function setAmountPrepaid(?float $value): void {
        $this->amountPrepaid = $value;
    }

    /**
     * Sets the analysis1 property value. The analysis1 property
     * @param string|null $value Value to set for the analysis1 property.
    */
    public function setAnalysis1(?string $value): void {
        $this->analysis1 = $value;
    }

    /**
     * Sets the analysis2 property value. The analysis2 property
     * @param string|null $value Value to set for the analysis2 property.
    */
    public function setAnalysis2(?string $value): void {
        $this->analysis2 = $value;
    }

    /**
     * Sets the analysis3 property value. The analysis3 property
     * @param string|null $value Value to set for the analysis3 property.
    */
    public function setAnalysis3(?string $value): void {
        $this->analysis3 = $value;
    }

    /**
     * Sets the bankRef property value. The bankRef property
     * @param string|null $value Value to set for the bankRef property.
    */
    public function setBankRef(?string $value): void {
        $this->bankRef = $value;
    }

    /**
     * Sets the cAddress1 property value. The cAddress1 property
     * @param string|null $value Value to set for the cAddress1 property.
    */
    public function setCAddress1(?string $value): void {
        $this->cAddress1 = $value;
    }

    /**
     * Sets the cAddress2 property value. The cAddress2 property
     * @param string|null $value Value to set for the cAddress2 property.
    */
    public function setCAddress2(?string $value): void {
        $this->cAddress2 = $value;
    }

    /**
     * Sets the cAddress3 property value. The cAddress3 property
     * @param string|null $value Value to set for the cAddress3 property.
    */
    public function setCAddress3(?string $value): void {
        $this->cAddress3 = $value;
    }

    /**
     * Sets the cAddress4 property value. The cAddress4 property
     * @param string|null $value Value to set for the cAddress4 property.
    */
    public function setCAddress4(?string $value): void {
        $this->cAddress4 = $value;
    }

    /**
     * Sets the cAddress5 property value. The cAddress5 property
     * @param string|null $value Value to set for the cAddress5 property.
    */
    public function setCAddress5(?string $value): void {
        $this->cAddress5 = $value;
    }

    /**
     * Sets the carrDeptName property value. The carrDeptName property
     * @param string|null $value Value to set for the carrDeptName property.
    */
    public function setCarrDeptName(?string $value): void {
        $this->carrDeptName = $value;
    }

    /**
     * Sets the carrDeptNumber property value. The carrDeptNumber property
     * @param int|null $value Value to set for the carrDeptNumber property.
    */
    public function setCarrDeptNumber(?int $value): void {
        $this->carrDeptNumber = $value;
    }

    /**
     * Sets the carrGross property value. The carrGross property
     * @param float|null $value Value to set for the carrGross property.
    */
    public function setCarrGross(?float $value): void {
        $this->carrGross = $value;
    }

    /**
     * Sets the carrNet property value. The carrNet property
     * @param float|null $value Value to set for the carrNet property.
    */
    public function setCarrNet(?float $value): void {
        $this->carrNet = $value;
    }

    /**
     * Sets the carrNomCode property value. The carrNomCode property
     * @param string|null $value Value to set for the carrNomCode property.
    */
    public function setCarrNomCode(?string $value): void {
        $this->carrNomCode = $value;
    }

    /**
     * Sets the carrTax property value. The carrTax property
     * @param float|null $value Value to set for the carrTax property.
    */
    public function setCarrTax(?float $value): void {
        $this->carrTax = $value;
    }

    /**
     * Sets the carrTaxCode property value. The carrTaxCode property
     * @param SalesInvoiceAttributesRead_carrTaxCode|null $value Value to set for the carrTaxCode property.
    */
    public function setCarrTaxCode(?SalesInvoiceAttributesRead_carrTaxCode $value): void {
        $this->carrTaxCode = $value;
    }

    /**
     * Sets the consignment property value. The consignment property
     * @param string|null $value Value to set for the consignment property.
    */
    public function setConsignment(?string $value): void {
        $this->consignment = $value;
    }

    /**
     * Sets the contactName property value. The contactName property
     * @param string|null $value Value to set for the contactName property.
    */
    public function setContactName(?string $value): void {
        $this->contactName = $value;
    }

    /**
     * Sets the containsCisReverseChargeItems property value. The containsCisReverseChargeItems property
     * @param int|null $value Value to set for the containsCisReverseChargeItems property.
    */
    public function setContainsCisReverseChargeItems(?int $value): void {
        $this->containsCisReverseChargeItems = $value;
    }

    /**
     * Sets the courierName property value. The courierName property
     * @param string|null $value Value to set for the courierName property.
    */
    public function setCourierName(?string $value): void {
        $this->courierName = $value;
    }

    /**
     * Sets the courierNumber property value. The courierNumber property
     * @param int|null $value Value to set for the courierNumber property.
    */
    public function setCourierNumber(?int $value): void {
        $this->courierNumber = $value;
    }

    /**
     * Sets the currency property value. The currency property
     * @param int|null $value Value to set for the currency property.
    */
    public function setCurrency(?int $value): void {
        $this->currency = $value;
    }

    /**
     * Sets the currencyType property value. The currencyType property
     * @param int|null $value Value to set for the currencyType property.
    */
    public function setCurrencyType(?int $value): void {
        $this->currencyType = $value;
    }

    /**
     * Sets the custDiscRate property value. The custDiscRate property
     * @param float|null $value Value to set for the custDiscRate property.
    */
    public function setCustDiscRate(?float $value): void {
        $this->custDiscRate = $value;
    }

    /**
     * Sets the custOrderNumber property value. The custOrderNumber property
     * @param string|null $value Value to set for the custOrderNumber property.
    */
    public function setCustOrderNumber(?string $value): void {
        $this->custOrderNumber = $value;
    }

    /**
     * Sets the custTelNumber property value. The custTelNumber property
     * @param string|null $value Value to set for the custTelNumber property.
    */
    public function setCustTelNumber(?string $value): void {
        $this->custTelNumber = $value;
    }

    /**
     * Sets the delAddress1 property value. The delAddress1 property
     * @param string|null $value Value to set for the delAddress1 property.
    */
    public function setDelAddress1(?string $value): void {
        $this->delAddress1 = $value;
    }

    /**
     * Sets the delAddress2 property value. The delAddress2 property
     * @param string|null $value Value to set for the delAddress2 property.
    */
    public function setDelAddress2(?string $value): void {
        $this->delAddress2 = $value;
    }

    /**
     * Sets the delAddress3 property value. The delAddress3 property
     * @param string|null $value Value to set for the delAddress3 property.
    */
    public function setDelAddress3(?string $value): void {
        $this->delAddress3 = $value;
    }

    /**
     * Sets the delAddress4 property value. The delAddress4 property
     * @param string|null $value Value to set for the delAddress4 property.
    */
    public function setDelAddress4(?string $value): void {
        $this->delAddress4 = $value;
    }

    /**
     * Sets the delAddress5 property value. The delAddress5 property
     * @param string|null $value Value to set for the delAddress5 property.
    */
    public function setDelAddress5(?string $value): void {
        $this->delAddress5 = $value;
    }

    /**
     * Sets the delName property value. The delName property
     * @param string|null $value Value to set for the delName property.
    */
    public function setDelName(?string $value): void {
        $this->delName = $value;
    }

    /**
     * Sets the dunsNumber property value. The dunsNumber property
     * @param string|null $value Value to set for the dunsNumber property.
    */
    public function setDunsNumber(?string $value): void {
        $this->dunsNumber = $value;
    }

    /**
     * Sets the euroGross property value. The euroGross property
     * @param float|null $value Value to set for the euroGross property.
    */
    public function setEuroGross(?float $value): void {
        $this->euroGross = $value;
    }

    /**
     * Sets the euroRate property value. The euroRate property
     * @param float|null $value Value to set for the euroRate property.
    */
    public function setEuroRate(?float $value): void {
        $this->euroRate = $value;
    }

    /**
     * Sets the foreignAmountPrepaid property value. The foreignAmountPrepaid property
     * @param float|null $value Value to set for the foreignAmountPrepaid property.
    */
    public function setForeignAmountPrepaid(?float $value): void {
        $this->foreignAmountPrepaid = $value;
    }

    /**
     * Sets the foreignCarrGross property value. The foreignCarrGross property
     * @param float|null $value Value to set for the foreignCarrGross property.
    */
    public function setForeignCarrGross(?float $value): void {
        $this->foreignCarrGross = $value;
    }

    /**
     * Sets the foreignCarrNet property value. The foreignCarrNet property
     * @param float|null $value Value to set for the foreignCarrNet property.
    */
    public function setForeignCarrNet(?float $value): void {
        $this->foreignCarrNet = $value;
    }

    /**
     * Sets the foreignCarrTax property value. The foreignCarrTax property
     * @param float|null $value Value to set for the foreignCarrTax property.
    */
    public function setForeignCarrTax(?float $value): void {
        $this->foreignCarrTax = $value;
    }

    /**
     * Sets the foreignInvoiceGross property value. The foreignInvoiceGross property
     * @param float|null $value Value to set for the foreignInvoiceGross property.
    */
    public function setForeignInvoiceGross(?float $value): void {
        $this->foreignInvoiceGross = $value;
    }

    /**
     * Sets the foreignInvoiceNet property value. The foreignInvoiceNet property
     * @param float|null $value Value to set for the foreignInvoiceNet property.
    */
    public function setForeignInvoiceNet(?float $value): void {
        $this->foreignInvoiceNet = $value;
    }

    /**
     * Sets the foreignInvoiceTax property value. The foreignInvoiceTax property
     * @param float|null $value Value to set for the foreignInvoiceTax property.
    */
    public function setForeignInvoiceTax(?float $value): void {
        $this->foreignInvoiceTax = $value;
    }

    /**
     * Sets the foreignItemsGross property value. The foreignItemsGross property
     * @param float|null $value Value to set for the foreignItemsGross property.
    */
    public function setForeignItemsGross(?float $value): void {
        $this->foreignItemsGross = $value;
    }

    /**
     * Sets the foreignItemsNet property value. The foreignItemsNet property
     * @param float|null $value Value to set for the foreignItemsNet property.
    */
    public function setForeignItemsNet(?float $value): void {
        $this->foreignItemsNet = $value;
    }

    /**
     * Sets the foreignItemsTax property value. The foreignItemsTax property
     * @param float|null $value Value to set for the foreignItemsTax property.
    */
    public function setForeignItemsTax(?float $value): void {
        $this->foreignItemsTax = $value;
    }

    /**
     * Sets the foreignRate property value. The foreignRate property
     * @param float|null $value Value to set for the foreignRate property.
    */
    public function setForeignRate(?float $value): void {
        $this->foreignRate = $value;
    }

    /**
     * Sets the foreignSettlementDiscAmount property value. The foreignSettlementDiscAmount property
     * @param float|null $value Value to set for the foreignSettlementDiscAmount property.
    */
    public function setForeignSettlementDiscAmount(?float $value): void {
        $this->foreignSettlementDiscAmount = $value;
    }

    /**
     * Sets the foreignSettlementTotal property value. The foreignSettlementTotal property
     * @param float|null $value Value to set for the foreignSettlementTotal property.
    */
    public function setForeignSettlementTotal(?float $value): void {
        $this->foreignSettlementTotal = $value;
    }

    /**
     * Sets the gdnNumber property value. The gdnNumber property
     * @param int|null $value Value to set for the gdnNumber property.
    */
    public function setGdnNumber(?int $value): void {
        $this->gdnNumber = $value;
    }

    /**
     * Sets the globalDeptName property value. The globalDeptName property
     * @param string|null $value Value to set for the globalDeptName property.
    */
    public function setGlobalDeptName(?string $value): void {
        $this->globalDeptName = $value;
    }

    /**
     * Sets the globalDeptNumber property value. The globalDeptNumber property
     * @param int|null $value Value to set for the globalDeptNumber property.
    */
    public function setGlobalDeptNumber(?int $value): void {
        $this->globalDeptNumber = $value;
    }

    /**
     * Sets the globalDetails property value. The globalDetails property
     * @param string|null $value Value to set for the globalDetails property.
    */
    public function setGlobalDetails(?string $value): void {
        $this->globalDetails = $value;
    }

    /**
     * Sets the globalNomCode property value. The globalNomCode property
     * @param string|null $value Value to set for the globalNomCode property.
    */
    public function setGlobalNomCode(?string $value): void {
        $this->globalNomCode = $value;
    }

    /**
     * Sets the globalTaxCode property value. The globalTaxCode property
     * @param SalesInvoiceAttributesRead_globalTaxCode|null $value Value to set for the globalTaxCode property.
    */
    public function setGlobalTaxCode(?SalesInvoiceAttributesRead_globalTaxCode $value): void {
        $this->globalTaxCode = $value;
    }

    /**
     * Sets the incoterms property value. The incoterms property
     * @param int|null $value Value to set for the incoterms property.
    */
    public function setIncoterms(?int $value): void {
        $this->incoterms = $value;
    }

    /**
     * Sets the incotermsText property value. The incotermsText property
     * @param string|null $value Value to set for the incotermsText property.
    */
    public function setIncotermsText(?string $value): void {
        $this->incotermsText = $value;
    }

    /**
     * Sets the invoiceDate property value. The invoiceDate property
     * @param DateTime|null $value Value to set for the invoiceDate property.
    */
    public function setInvoiceDate(?DateTime $value): void {
        $this->invoiceDate = $value;
    }

    /**
     * Sets the invoiceGross property value. The invoiceGross property
     * @param float|null $value Value to set for the invoiceGross property.
    */
    public function setInvoiceGross(?float $value): void {
        $this->invoiceGross = $value;
    }

    /**
     * Sets the invoiceNet property value. The invoiceNet property
     * @param float|null $value Value to set for the invoiceNet property.
    */
    public function setInvoiceNet(?float $value): void {
        $this->invoiceNet = $value;
    }

    /**
     * Sets the invoiceOrCredit property value. The invoiceOrCredit property
     * @param string|null $value Value to set for the invoiceOrCredit property.
    */
    public function setInvoiceOrCredit(?string $value): void {
        $this->invoiceOrCredit = $value;
    }

    /**
     * Sets the invoicePaymentId property value. The invoicePaymentId property
     * @param string|null $value Value to set for the invoicePaymentId property.
    */
    public function setInvoicePaymentId(?string $value): void {
        $this->invoicePaymentId = $value;
    }

    /**
     * Sets the invoiceTax property value. The invoiceTax property
     * @param float|null $value Value to set for the invoiceTax property.
    */
    public function setInvoiceTax(?float $value): void {
        $this->invoiceTax = $value;
    }

    /**
     * Sets the invoiceType property value. The invoiceType property
     * @param string|null $value Value to set for the invoiceType property.
    */
    public function setInvoiceType(?string $value): void {
        $this->invoiceType = $value;
    }

    /**
     * Sets the invoiceTypeCode property value. The invoiceTypeCode property
     * @param int|null $value Value to set for the invoiceTypeCode property.
    */
    public function setInvoiceTypeCode(?int $value): void {
        $this->invoiceTypeCode = $value;
    }

    /**
     * Sets the itemsGross property value. The itemsGross property
     * @param float|null $value Value to set for the itemsGross property.
    */
    public function setItemsGross(?float $value): void {
        $this->itemsGross = $value;
    }

    /**
     * Sets the itemsNet property value. The itemsNet property
     * @param float|null $value Value to set for the itemsNet property.
    */
    public function setItemsNet(?float $value): void {
        $this->itemsNet = $value;
    }

    /**
     * Sets the itemsTax property value. The itemsTax property
     * @param float|null $value Value to set for the itemsTax property.
    */
    public function setItemsTax(?float $value): void {
        $this->itemsTax = $value;
    }

    /**
     * Sets the name property value. The name property
     * @param string|null $value Value to set for the name property.
    */
    public function setName(?string $value): void {
        $this->name = $value;
    }

    /**
     * Sets the netAmount1 property value. The netAmount1 property
     * @param float|null $value Value to set for the netAmount1 property.
    */
    public function setNetAmount1(?float $value): void {
        $this->netAmount1 = $value;
    }

    /**
     * Sets the netAmount2 property value. The netAmount2 property
     * @param float|null $value Value to set for the netAmount2 property.
    */
    public function setNetAmount2(?float $value): void {
        $this->netAmount2 = $value;
    }

    /**
     * Sets the netAmount3 property value. The netAmount3 property
     * @param float|null $value Value to set for the netAmount3 property.
    */
    public function setNetAmount3(?float $value): void {
        $this->netAmount3 = $value;
    }

    /**
     * Sets the netAmount4 property value. The netAmount4 property
     * @param float|null $value Value to set for the netAmount4 property.
    */
    public function setNetAmount4(?float $value): void {
        $this->netAmount4 = $value;
    }

    /**
     * Sets the netAmount5 property value. The netAmount5 property
     * @param float|null $value Value to set for the netAmount5 property.
    */
    public function setNetAmount5(?float $value): void {
        $this->netAmount5 = $value;
    }

    /**
     * Sets the netValueDiscountComment1 property value. The netValueDiscountComment1 property
     * @param string|null $value Value to set for the netValueDiscountComment1 property.
    */
    public function setNetValueDiscountComment1(?string $value): void {
        $this->netValueDiscountComment1 = $value;
    }

    /**
     * Sets the netValueDiscountComment2 property value. The netValueDiscountComment2 property
     * @param string|null $value Value to set for the netValueDiscountComment2 property.
    */
    public function setNetValueDiscountComment2(?string $value): void {
        $this->netValueDiscountComment2 = $value;
    }

    /**
     * Sets the netValueDiscountDesc property value. The netValueDiscountDesc property
     * @param string|null $value Value to set for the netValueDiscountDesc property.
    */
    public function setNetValueDiscountDesc(?string $value): void {
        $this->netValueDiscountDesc = $value;
    }

    /**
     * Sets the netValueDiscountNet property value. The netValueDiscountNet property
     * @param float|null $value Value to set for the netValueDiscountNet property.
    */
    public function setNetValueDiscountNet(?float $value): void {
        $this->netValueDiscountNet = $value;
    }

    /**
     * Sets the netValueDiscountNetBase property value. The netValueDiscountNetBase property
     * @param float|null $value Value to set for the netValueDiscountNetBase property.
    */
    public function setNetValueDiscountNetBase(?float $value): void {
        $this->netValueDiscountNetBase = $value;
    }

    /**
     * Sets the netValueDiscountRate property value. The netValueDiscountRate property
     * @param float|null $value Value to set for the netValueDiscountRate property.
    */
    public function setNetValueDiscountRate(?float $value): void {
        $this->netValueDiscountRate = $value;
    }

    /**
     * Sets the notes1 property value. The notes1 property
     * @param string|null $value Value to set for the notes1 property.
    */
    public function setNotes1(?string $value): void {
        $this->notes1 = $value;
    }

    /**
     * Sets the notes2 property value. The notes2 property
     * @param string|null $value Value to set for the notes2 property.
    */
    public function setNotes2(?string $value): void {
        $this->notes2 = $value;
    }

    /**
     * Sets the notes3 property value. The notes3 property
     * @param string|null $value Value to set for the notes3 property.
    */
    public function setNotes3(?string $value): void {
        $this->notes3 = $value;
    }

    /**
     * Sets the orderNumber property value. The orderNumber property
     * @param string|null $value Value to set for the orderNumber property.
    */
    public function setOrderNumber(?string $value): void {
        $this->orderNumber = $value;
    }

    /**
     * Sets the orderNumberNumeric property value. The orderNumberNumeric property
     * @param int|null $value Value to set for the orderNumberNumeric property.
    */
    public function setOrderNumberNumeric(?int $value): void {
        $this->orderNumberNumeric = $value;
    }

    /**
     * Sets the ossCountryOfVat property value. The ossCountryOfVat property
     * @param string|null $value Value to set for the ossCountryOfVat property.
    */
    public function setOssCountryOfVat(?string $value): void {
        $this->ossCountryOfVat = $value;
    }

    /**
     * Sets the ossReportingType property value. The ossReportingType property
     * @param int|null $value Value to set for the ossReportingType property.
    */
    public function setOssReportingType(?int $value): void {
        $this->ossReportingType = $value;
    }

    /**
     * Sets the ossReportingTypeName property value. The ossReportingTypeName property
     * @param string|null $value Value to set for the ossReportingTypeName property.
    */
    public function setOssReportingTypeName(?string $value): void {
        $this->ossReportingTypeName = $value;
    }

    /**
     * Sets the paymentDueDate property value. The paymentDueDate property
     * @param DateTime|null $value Value to set for the paymentDueDate property.
    */
    public function setPaymentDueDate(?DateTime $value): void {
        $this->paymentDueDate = $value;
    }

    /**
     * Sets the paymentRef property value. The paymentRef property
     * @param string|null $value Value to set for the paymentRef property.
    */
    public function setPaymentRef(?string $value): void {
        $this->paymentRef = $value;
    }

    /**
     * Sets the paymentType property value. The paymentType property
     * @param int|null $value Value to set for the paymentType property.
    */
    public function setPaymentType(?int $value): void {
        $this->paymentType = $value;
    }

    /**
     * Sets the posted property value. The posted property
     * @param string|null $value Value to set for the posted property.
    */
    public function setPosted(?string $value): void {
        $this->posted = $value;
    }

    /**
     * Sets the postedCode property value. The postedCode property
     * @param int|null $value Value to set for the postedCode property.
    */
    public function setPostedCode(?int $value): void {
        $this->postedCode = $value;
    }

    /**
     * Sets the printed property value. The printed property
     * @param string|null $value Value to set for the printed property.
    */
    public function setPrinted(?string $value): void {
        $this->printed = $value;
    }

    /**
     * Sets the printedCode property value. The printedCode property
     * @param int|null $value Value to set for the printedCode property.
    */
    public function setPrintedCode(?int $value): void {
        $this->printedCode = $value;
    }

    /**
     * Sets the quoteExpiryDate property value. The quoteExpiryDate property
     * @param DateTime|null $value Value to set for the quoteExpiryDate property.
    */
    public function setQuoteExpiryDate(?DateTime $value): void {
        $this->quoteExpiryDate = $value;
    }

    /**
     * Sets the quoteStatus property value. The quoteStatus property
     * @param SalesInvoiceAttributesRead_quoteStatus|null $value Value to set for the quoteStatus property.
    */
    public function setQuoteStatus(?SalesInvoiceAttributesRead_quoteStatus $value): void {
        $this->quoteStatus = $value;
    }

    /**
     * Sets the quoteStatusId property value. The quoteStatusId property
     * @param int|null $value Value to set for the quoteStatusId property.
    */
    public function setQuoteStatusId(?int $value): void {
        $this->quoteStatusId = $value;
    }

    /**
     * Sets the recordCreateDate property value. The recordCreateDate property
     * @param DateTime|null $value Value to set for the recordCreateDate property.
    */
    public function setRecordCreateDate(?DateTime $value): void {
        $this->recordCreateDate = $value;
    }

    /**
     * Sets the recordDeleted property value. The recordDeleted property
     * @param int|null $value Value to set for the recordDeleted property.
    */
    public function setRecordDeleted(?int $value): void {
        $this->recordDeleted = $value;
    }

    /**
     * Sets the recordModifyDate property value. The recordModifyDate property
     * @param DateTime|null $value Value to set for the recordModifyDate property.
    */
    public function setRecordModifyDate(?DateTime $value): void {
        $this->recordModifyDate = $value;
    }

    /**
     * Sets the recurringRef property value. The recurringRef property
     * @param string|null $value Value to set for the recurringRef property.
    */
    public function setRecurringRef(?string $value): void {
        $this->recurringRef = $value;
    }

    /**
     * Sets the resubmitInvoicePaymentRequired property value. The resubmitInvoicePaymentRequired property
     * @param int|null $value Value to set for the resubmitInvoicePaymentRequired property.
    */
    public function setResubmitInvoicePaymentRequired(?int $value): void {
        $this->resubmitInvoicePaymentRequired = $value;
    }

    /**
     * Sets the settlementDiscAmount property value. The settlementDiscAmount property
     * @param float|null $value Value to set for the settlementDiscAmount property.
    */
    public function setSettlementDiscAmount(?float $value): void {
        $this->settlementDiscAmount = $value;
    }

    /**
     * Sets the settlementDiscRate property value. The settlementDiscRate property
     * @param float|null $value Value to set for the settlementDiscRate property.
    */
    public function setSettlementDiscRate(?float $value): void {
        $this->settlementDiscRate = $value;
    }

    /**
     * Sets the settlementDueDays property value. The settlementDueDays property
     * @param int|null $value Value to set for the settlementDueDays property.
    */
    public function setSettlementDueDays(?int $value): void {
        $this->settlementDueDays = $value;
    }

    /**
     * Sets the settlementTotal property value. The settlementTotal property
     * @param float|null $value Value to set for the settlementTotal property.
    */
    public function setSettlementTotal(?float $value): void {
        $this->settlementTotal = $value;
    }

    /**
     * Sets the takenBy property value. The takenBy property
     * @param string|null $value Value to set for the takenBy property.
    */
    public function setTakenBy(?string $value): void {
        $this->takenBy = $value;
    }

    /**
     * Sets the taxAmount1 property value. The taxAmount1 property
     * @param float|null $value Value to set for the taxAmount1 property.
    */
    public function setTaxAmount1(?float $value): void {
        $this->taxAmount1 = $value;
    }

    /**
     * Sets the taxAmount2 property value. The taxAmount2 property
     * @param float|null $value Value to set for the taxAmount2 property.
    */
    public function setTaxAmount2(?float $value): void {
        $this->taxAmount2 = $value;
    }

    /**
     * Sets the taxAmount3 property value. The taxAmount3 property
     * @param float|null $value Value to set for the taxAmount3 property.
    */
    public function setTaxAmount3(?float $value): void {
        $this->taxAmount3 = $value;
    }

    /**
     * Sets the taxAmount4 property value. The taxAmount4 property
     * @param float|null $value Value to set for the taxAmount4 property.
    */
    public function setTaxAmount4(?float $value): void {
        $this->taxAmount4 = $value;
    }

    /**
     * Sets the taxAmount5 property value. The taxAmount5 property
     * @param float|null $value Value to set for the taxAmount5 property.
    */
    public function setTaxAmount5(?float $value): void {
        $this->taxAmount5 = $value;
    }

    /**
     * Sets the taxRate1 property value. The taxRate1 property
     * @param float|null $value Value to set for the taxRate1 property.
    */
    public function setTaxRate1(?float $value): void {
        $this->taxRate1 = $value;
    }

    /**
     * Sets the taxRate2 property value. The taxRate2 property
     * @param float|null $value Value to set for the taxRate2 property.
    */
    public function setTaxRate2(?float $value): void {
        $this->taxRate2 = $value;
    }

    /**
     * Sets the taxRate3 property value. The taxRate3 property
     * @param float|null $value Value to set for the taxRate3 property.
    */
    public function setTaxRate3(?float $value): void {
        $this->taxRate3 = $value;
    }

    /**
     * Sets the taxRate4 property value. The taxRate4 property
     * @param float|null $value Value to set for the taxRate4 property.
    */
    public function setTaxRate4(?float $value): void {
        $this->taxRate4 = $value;
    }

    /**
     * Sets the taxRate5 property value. The taxRate5 property
     * @param float|null $value Value to set for the taxRate5 property.
    */
    public function setTaxRate5(?float $value): void {
        $this->taxRate5 = $value;
    }

    /**
     * Sets the vatRegNumber property value. The vatRegNumber property
     * @param string|null $value Value to set for the vatRegNumber property.
    */
    public function setVatRegNumber(?string $value): void {
        $this->vatRegNumber = $value;
    }

}
