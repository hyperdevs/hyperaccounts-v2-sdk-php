<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use DateTime;
use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class TransactionSplitExceptTypeAttributesWrite implements Parsable
{
    /**
     * @var float|null $amountPaid The amountPaid property
    */
    private ?float $amountPaid = null;

    /**
     * @var TransactionSplitExceptTypeAttributesWrite_bankFlag|null $bankFlag The bankFlag property
    */
    private ?TransactionSplitExceptTypeAttributesWrite_bankFlag $bankFlag = null;

    /**
     * @var int|null $deletedFlag The deletedFlag property
    */
    private ?int $deletedFlag = null;

    /**
     * @var int|null $deptNumber The deptNumber property
    */
    private ?int $deptNumber = null;

    /**
     * @var string|null $details The details property
    */
    private ?string $details = null;

    /**
     * @var float|null $foreignAmountPaid The foreignAmountPaid property
    */
    private ?float $foreignAmountPaid = null;

    /**
     * @var float|null $foreignNetAmount The foreignNetAmount property
    */
    private ?float $foreignNetAmount = null;

    /**
     * @var float|null $foreignTaxAmount The foreignTaxAmount property
    */
    private ?float $foreignTaxAmount = null;

    /**
     * @var int|null $giftAid The giftAid property
    */
    private ?int $giftAid = null;

    /**
     * @var float|null $netAmount The netAmount property
    */
    private ?float $netAmount = null;

    /**
     * @var TransactionSplitExceptTypeAttributesWrite_paidFlag|null $paidFlag The paidFlag property
    */
    private ?TransactionSplitExceptTypeAttributesWrite_paidFlag $paidFlag = null;

    /**
     * @var int|null $smallDonation The smallDonation property
    */
    private ?int $smallDonation = null;

    /**
     * @var float|null $taxAmount The taxAmount property
    */
    private ?float $taxAmount = null;

    /**
     * @var TransactionSplitExceptTypeAttributesWrite_taxCode|null $taxCode The taxCode property
    */
    private ?TransactionSplitExceptTypeAttributesWrite_taxCode $taxCode = null;

    /**
     * @var string|null $userName The userName property
    */
    private ?string $userName = null;

    /**
     * @var TransactionSplitExceptTypeAttributesWrite_vatFlag|null $vatFlag The vatFlag property
    */
    private ?TransactionSplitExceptTypeAttributesWrite_vatFlag $vatFlag = null;

    /**
     * @var DateTime|null $vatReconciledDate The vatReconciledDate property
    */
    private ?DateTime $vatReconciledDate = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return TransactionSplitExceptTypeAttributesWrite
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): TransactionSplitExceptTypeAttributesWrite {
        return new TransactionSplitExceptTypeAttributesWrite();
    }

    /**
     * Gets the amountPaid property value. The amountPaid property
     * @return float|null
    */
    public function getAmountPaid(): ?float {
        return $this->amountPaid;
    }

    /**
     * Gets the bankFlag property value. The bankFlag property
     * @return TransactionSplitExceptTypeAttributesWrite_bankFlag|null
    */
    public function getBankFlag(): ?TransactionSplitExceptTypeAttributesWrite_bankFlag {
        return $this->bankFlag;
    }

    /**
     * Gets the deletedFlag property value. The deletedFlag property
     * @return int|null
    */
    public function getDeletedFlag(): ?int {
        return $this->deletedFlag;
    }

    /**
     * Gets the deptNumber property value. The deptNumber property
     * @return int|null
    */
    public function getDeptNumber(): ?int {
        return $this->deptNumber;
    }

    /**
     * Gets the details property value. The details property
     * @return string|null
    */
    public function getDetails(): ?string {
        return $this->details;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'amountPaid' => fn(ParseNode $n) => $o->setAmountPaid($n->getFloatValue()),
            'bankFlag' => fn(ParseNode $n) => $o->setBankFlag($n->getEnumValue(TransactionSplitExceptTypeAttributesWrite_bankFlag::class)),
            'deletedFlag' => fn(ParseNode $n) => $o->setDeletedFlag($n->getIntegerValue()),
            'deptNumber' => fn(ParseNode $n) => $o->setDeptNumber($n->getIntegerValue()),
            'details' => fn(ParseNode $n) => $o->setDetails($n->getStringValue()),
            'foreignAmountPaid' => fn(ParseNode $n) => $o->setForeignAmountPaid($n->getFloatValue()),
            'foreignNetAmount' => fn(ParseNode $n) => $o->setForeignNetAmount($n->getFloatValue()),
            'foreignTaxAmount' => fn(ParseNode $n) => $o->setForeignTaxAmount($n->getFloatValue()),
            'giftAid' => fn(ParseNode $n) => $o->setGiftAid($n->getIntegerValue()),
            'netAmount' => fn(ParseNode $n) => $o->setNetAmount($n->getFloatValue()),
            'paidFlag' => fn(ParseNode $n) => $o->setPaidFlag($n->getEnumValue(TransactionSplitExceptTypeAttributesWrite_paidFlag::class)),
            'smallDonation' => fn(ParseNode $n) => $o->setSmallDonation($n->getIntegerValue()),
            'taxAmount' => fn(ParseNode $n) => $o->setTaxAmount($n->getFloatValue()),
            'taxCode' => fn(ParseNode $n) => $o->setTaxCode($n->getEnumValue(TransactionSplitExceptTypeAttributesWrite_taxCode::class)),
            'userName' => fn(ParseNode $n) => $o->setUserName($n->getStringValue()),
            'vatFlag' => fn(ParseNode $n) => $o->setVatFlag($n->getEnumValue(TransactionSplitExceptTypeAttributesWrite_vatFlag::class)),
            'vatReconciledDate' => fn(ParseNode $n) => $o->setVatReconciledDate($n->getDateTimeValue()),
        ];
    }

    /**
     * Gets the foreignAmountPaid property value. The foreignAmountPaid property
     * @return float|null
    */
    public function getForeignAmountPaid(): ?float {
        return $this->foreignAmountPaid;
    }

    /**
     * Gets the foreignNetAmount property value. The foreignNetAmount property
     * @return float|null
    */
    public function getForeignNetAmount(): ?float {
        return $this->foreignNetAmount;
    }

    /**
     * Gets the foreignTaxAmount property value. The foreignTaxAmount property
     * @return float|null
    */
    public function getForeignTaxAmount(): ?float {
        return $this->foreignTaxAmount;
    }

    /**
     * Gets the giftAid property value. The giftAid property
     * @return int|null
    */
    public function getGiftAid(): ?int {
        return $this->giftAid;
    }

    /**
     * Gets the netAmount property value. The netAmount property
     * @return float|null
    */
    public function getNetAmount(): ?float {
        return $this->netAmount;
    }

    /**
     * Gets the paidFlag property value. The paidFlag property
     * @return TransactionSplitExceptTypeAttributesWrite_paidFlag|null
    */
    public function getPaidFlag(): ?TransactionSplitExceptTypeAttributesWrite_paidFlag {
        return $this->paidFlag;
    }

    /**
     * Gets the smallDonation property value. The smallDonation property
     * @return int|null
    */
    public function getSmallDonation(): ?int {
        return $this->smallDonation;
    }

    /**
     * Gets the taxAmount property value. The taxAmount property
     * @return float|null
    */
    public function getTaxAmount(): ?float {
        return $this->taxAmount;
    }

    /**
     * Gets the taxCode property value. The taxCode property
     * @return TransactionSplitExceptTypeAttributesWrite_taxCode|null
    */
    public function getTaxCode(): ?TransactionSplitExceptTypeAttributesWrite_taxCode {
        return $this->taxCode;
    }

    /**
     * Gets the userName property value. The userName property
     * @return string|null
    */
    public function getUserName(): ?string {
        return $this->userName;
    }

    /**
     * Gets the vatFlag property value. The vatFlag property
     * @return TransactionSplitExceptTypeAttributesWrite_vatFlag|null
    */
    public function getVatFlag(): ?TransactionSplitExceptTypeAttributesWrite_vatFlag {
        return $this->vatFlag;
    }

    /**
     * Gets the vatReconciledDate property value. The vatReconciledDate property
     * @return DateTime|null
    */
    public function getVatReconciledDate(): ?DateTime {
        return $this->vatReconciledDate;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeFloatValue('amountPaid', $this->getAmountPaid());
        $writer->writeEnumValue('bankFlag', $this->getBankFlag());
        $writer->writeIntegerValue('deletedFlag', $this->getDeletedFlag());
        $writer->writeIntegerValue('deptNumber', $this->getDeptNumber());
        $writer->writeStringValue('details', $this->getDetails());
        $writer->writeFloatValue('foreignAmountPaid', $this->getForeignAmountPaid());
        $writer->writeFloatValue('foreignNetAmount', $this->getForeignNetAmount());
        $writer->writeFloatValue('foreignTaxAmount', $this->getForeignTaxAmount());
        $writer->writeIntegerValue('giftAid', $this->getGiftAid());
        $writer->writeFloatValue('netAmount', $this->getNetAmount());
        $writer->writeEnumValue('paidFlag', $this->getPaidFlag());
        $writer->writeIntegerValue('smallDonation', $this->getSmallDonation());
        $writer->writeFloatValue('taxAmount', $this->getTaxAmount());
        $writer->writeEnumValue('taxCode', $this->getTaxCode());
        $writer->writeStringValue('userName', $this->getUserName());
        $writer->writeEnumValue('vatFlag', $this->getVatFlag());
        $writer->writeDateTimeValue('vatReconciledDate', $this->getVatReconciledDate());
    }

    /**
     * Sets the amountPaid property value. The amountPaid property
     * @param float|null $value Value to set for the amountPaid property.
    */
    public function setAmountPaid(?float $value): void {
        $this->amountPaid = $value;
    }

    /**
     * Sets the bankFlag property value. The bankFlag property
     * @param TransactionSplitExceptTypeAttributesWrite_bankFlag|null $value Value to set for the bankFlag property.
    */
    public function setBankFlag(?TransactionSplitExceptTypeAttributesWrite_bankFlag $value): void {
        $this->bankFlag = $value;
    }

    /**
     * Sets the deletedFlag property value. The deletedFlag property
     * @param int|null $value Value to set for the deletedFlag property.
    */
    public function setDeletedFlag(?int $value): void {
        $this->deletedFlag = $value;
    }

    /**
     * Sets the deptNumber property value. The deptNumber property
     * @param int|null $value Value to set for the deptNumber property.
    */
    public function setDeptNumber(?int $value): void {
        $this->deptNumber = $value;
    }

    /**
     * Sets the details property value. The details property
     * @param string|null $value Value to set for the details property.
    */
    public function setDetails(?string $value): void {
        $this->details = $value;
    }

    /**
     * Sets the foreignAmountPaid property value. The foreignAmountPaid property
     * @param float|null $value Value to set for the foreignAmountPaid property.
    */
    public function setForeignAmountPaid(?float $value): void {
        $this->foreignAmountPaid = $value;
    }

    /**
     * Sets the foreignNetAmount property value. The foreignNetAmount property
     * @param float|null $value Value to set for the foreignNetAmount property.
    */
    public function setForeignNetAmount(?float $value): void {
        $this->foreignNetAmount = $value;
    }

    /**
     * Sets the foreignTaxAmount property value. The foreignTaxAmount property
     * @param float|null $value Value to set for the foreignTaxAmount property.
    */
    public function setForeignTaxAmount(?float $value): void {
        $this->foreignTaxAmount = $value;
    }

    /**
     * Sets the giftAid property value. The giftAid property
     * @param int|null $value Value to set for the giftAid property.
    */
    public function setGiftAid(?int $value): void {
        $this->giftAid = $value;
    }

    /**
     * Sets the netAmount property value. The netAmount property
     * @param float|null $value Value to set for the netAmount property.
    */
    public function setNetAmount(?float $value): void {
        $this->netAmount = $value;
    }

    /**
     * Sets the paidFlag property value. The paidFlag property
     * @param TransactionSplitExceptTypeAttributesWrite_paidFlag|null $value Value to set for the paidFlag property.
    */
    public function setPaidFlag(?TransactionSplitExceptTypeAttributesWrite_paidFlag $value): void {
        $this->paidFlag = $value;
    }

    /**
     * Sets the smallDonation property value. The smallDonation property
     * @param int|null $value Value to set for the smallDonation property.
    */
    public function setSmallDonation(?int $value): void {
        $this->smallDonation = $value;
    }

    /**
     * Sets the taxAmount property value. The taxAmount property
     * @param float|null $value Value to set for the taxAmount property.
    */
    public function setTaxAmount(?float $value): void {
        $this->taxAmount = $value;
    }

    /**
     * Sets the taxCode property value. The taxCode property
     * @param TransactionSplitExceptTypeAttributesWrite_taxCode|null $value Value to set for the taxCode property.
    */
    public function setTaxCode(?TransactionSplitExceptTypeAttributesWrite_taxCode $value): void {
        $this->taxCode = $value;
    }

    /**
     * Sets the userName property value. The userName property
     * @param string|null $value Value to set for the userName property.
    */
    public function setUserName(?string $value): void {
        $this->userName = $value;
    }

    /**
     * Sets the vatFlag property value. The vatFlag property
     * @param TransactionSplitExceptTypeAttributesWrite_vatFlag|null $value Value to set for the vatFlag property.
    */
    public function setVatFlag(?TransactionSplitExceptTypeAttributesWrite_vatFlag $value): void {
        $this->vatFlag = $value;
    }

    /**
     * Sets the vatReconciledDate property value. The vatReconciledDate property
     * @param DateTime|null $value Value to set for the vatReconciledDate property.
    */
    public function setVatReconciledDate(?DateTime $value): void {
        $this->vatReconciledDate = $value;
    }

}
