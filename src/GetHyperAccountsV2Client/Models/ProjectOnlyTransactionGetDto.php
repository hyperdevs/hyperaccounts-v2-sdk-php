<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class ProjectOnlyTransactionGetDto implements Parsable
{
    /**
     * @var ProjectOnlyTransactionAttributesRead|null $attributes The attributes property
    */
    private ?ProjectOnlyTransactionAttributesRead $attributes = null;

    /**
     * @var ProjectOnlyTransactionIncluded|null $included The included property
    */
    private ?ProjectOnlyTransactionIncluded $included = null;

    /**
     * @var ResourceLinks|null $links The links property
    */
    private ?ResourceLinks $links = null;

    /**
     * @var int|null $projectTranId The projectTranId property
    */
    private ?int $projectTranId = null;

    /**
     * @var ProjectOnlyTransactionRelationships|null $relationships The relationships property
    */
    private ?ProjectOnlyTransactionRelationships $relationships = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return ProjectOnlyTransactionGetDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): ProjectOnlyTransactionGetDto {
        return new ProjectOnlyTransactionGetDto();
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return ProjectOnlyTransactionAttributesRead|null
    */
    public function getAttributes(): ?ProjectOnlyTransactionAttributesRead {
        return $this->attributes;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([ProjectOnlyTransactionAttributesRead::class, 'createFromDiscriminatorValue'])),
            'included' => fn(ParseNode $n) => $o->setIncluded($n->getObjectValue([ProjectOnlyTransactionIncluded::class, 'createFromDiscriminatorValue'])),
            'links' => fn(ParseNode $n) => $o->setLinks($n->getObjectValue([ResourceLinks::class, 'createFromDiscriminatorValue'])),
            'projectTranId' => fn(ParseNode $n) => $o->setProjectTranId($n->getIntegerValue()),
            'relationships' => fn(ParseNode $n) => $o->setRelationships($n->getObjectValue([ProjectOnlyTransactionRelationships::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the included property value. The included property
     * @return ProjectOnlyTransactionIncluded|null
    */
    public function getIncluded(): ?ProjectOnlyTransactionIncluded {
        return $this->included;
    }

    /**
     * Gets the links property value. The links property
     * @return ResourceLinks|null
    */
    public function getLinks(): ?ResourceLinks {
        return $this->links;
    }

    /**
     * Gets the projectTranId property value. The projectTranId property
     * @return int|null
    */
    public function getProjectTranId(): ?int {
        return $this->projectTranId;
    }

    /**
     * Gets the relationships property value. The relationships property
     * @return ProjectOnlyTransactionRelationships|null
    */
    public function getRelationships(): ?ProjectOnlyTransactionRelationships {
        return $this->relationships;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('attributes', $this->getAttributes());
        $writer->writeObjectValue('included', $this->getIncluded());
        $writer->writeObjectValue('links', $this->getLinks());
        $writer->writeIntegerValue('projectTranId', $this->getProjectTranId());
        $writer->writeObjectValue('relationships', $this->getRelationships());
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param ProjectOnlyTransactionAttributesRead|null $value Value to set for the attributes property.
    */
    public function setAttributes(?ProjectOnlyTransactionAttributesRead $value): void {
        $this->attributes = $value;
    }

    /**
     * Sets the included property value. The included property
     * @param ProjectOnlyTransactionIncluded|null $value Value to set for the included property.
    */
    public function setIncluded(?ProjectOnlyTransactionIncluded $value): void {
        $this->included = $value;
    }

    /**
     * Sets the links property value. The links property
     * @param ResourceLinks|null $value Value to set for the links property.
    */
    public function setLinks(?ResourceLinks $value): void {
        $this->links = $value;
    }

    /**
     * Sets the projectTranId property value. The projectTranId property
     * @param int|null $value Value to set for the projectTranId property.
    */
    public function setProjectTranId(?int $value): void {
        $this->projectTranId = $value;
    }

    /**
     * Sets the relationships property value. The relationships property
     * @param ProjectOnlyTransactionRelationships|null $value Value to set for the relationships property.
    */
    public function setRelationships(?ProjectOnlyTransactionRelationships $value): void {
        $this->relationships = $value;
    }

}
