<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class TransactionSplitPostAllocatePaymentDto implements Parsable
{
    /**
     * @var TransactionSplitAllocatePaymentAttributesWrite|null $attributes The attributes property
    */
    private ?TransactionSplitAllocatePaymentAttributesWrite $attributes = null;

    /**
     * @var TransactionSplitPostAllocateRelationshipsWrite|null $relationships The relationships property
    */
    private ?TransactionSplitPostAllocateRelationshipsWrite $relationships = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return TransactionSplitPostAllocatePaymentDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): TransactionSplitPostAllocatePaymentDto {
        return new TransactionSplitPostAllocatePaymentDto();
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return TransactionSplitAllocatePaymentAttributesWrite|null
    */
    public function getAttributes(): ?TransactionSplitAllocatePaymentAttributesWrite {
        return $this->attributes;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([TransactionSplitAllocatePaymentAttributesWrite::class, 'createFromDiscriminatorValue'])),
            'relationships' => fn(ParseNode $n) => $o->setRelationships($n->getObjectValue([TransactionSplitPostAllocateRelationshipsWrite::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the relationships property value. The relationships property
     * @return TransactionSplitPostAllocateRelationshipsWrite|null
    */
    public function getRelationships(): ?TransactionSplitPostAllocateRelationshipsWrite {
        return $this->relationships;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('attributes', $this->getAttributes());
        $writer->writeObjectValue('relationships', $this->getRelationships());
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param TransactionSplitAllocatePaymentAttributesWrite|null $value Value to set for the attributes property.
    */
    public function setAttributes(?TransactionSplitAllocatePaymentAttributesWrite $value): void {
        $this->attributes = $value;
    }

    /**
     * Sets the relationships property value. The relationships property
     * @param TransactionSplitPostAllocateRelationshipsWrite|null $value Value to set for the relationships property.
    */
    public function setRelationships(?TransactionSplitPostAllocateRelationshipsWrite $value): void {
        $this->relationships = $value;
    }

}
