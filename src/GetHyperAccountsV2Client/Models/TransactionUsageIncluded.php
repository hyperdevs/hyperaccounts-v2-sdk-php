<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class TransactionUsageIncluded implements Parsable
{
    /**
     * @var TransactionSplitGetDto|null $split The split property
    */
    private ?TransactionSplitGetDto $split = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return TransactionUsageIncluded
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): TransactionUsageIncluded {
        return new TransactionUsageIncluded();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'split' => fn(ParseNode $n) => $o->setSplit($n->getObjectValue([TransactionSplitGetDto::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the split property value. The split property
     * @return TransactionSplitGetDto|null
    */
    public function getSplit(): ?TransactionSplitGetDto {
        return $this->split;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('split', $this->getSplit());
    }

    /**
     * Sets the split property value. The split property
     * @param TransactionSplitGetDto|null $value Value to set for the split property.
    */
    public function setSplit(?TransactionSplitGetDto $value): void {
        $this->split = $value;
    }

}
