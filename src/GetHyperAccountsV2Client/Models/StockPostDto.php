<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class StockPostDto implements Parsable
{
    /**
     * @var StockAttributesWrite|null $attributes The attributes property
    */
    private ?StockAttributesWrite $attributes = null;

    /**
     * @var StockRelationshipsWrite|null $relationships The relationships property
    */
    private ?StockRelationshipsWrite $relationships = null;

    /**
     * @var string|null $stockCode The stockCode property
    */
    private ?string $stockCode = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return StockPostDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): StockPostDto {
        return new StockPostDto();
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return StockAttributesWrite|null
    */
    public function getAttributes(): ?StockAttributesWrite {
        return $this->attributes;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([StockAttributesWrite::class, 'createFromDiscriminatorValue'])),
            'relationships' => fn(ParseNode $n) => $o->setRelationships($n->getObjectValue([StockRelationshipsWrite::class, 'createFromDiscriminatorValue'])),
            'stockCode' => fn(ParseNode $n) => $o->setStockCode($n->getStringValue()),
        ];
    }

    /**
     * Gets the relationships property value. The relationships property
     * @return StockRelationshipsWrite|null
    */
    public function getRelationships(): ?StockRelationshipsWrite {
        return $this->relationships;
    }

    /**
     * Gets the stockCode property value. The stockCode property
     * @return string|null
    */
    public function getStockCode(): ?string {
        return $this->stockCode;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('attributes', $this->getAttributes());
        $writer->writeObjectValue('relationships', $this->getRelationships());
        $writer->writeStringValue('stockCode', $this->getStockCode());
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param StockAttributesWrite|null $value Value to set for the attributes property.
    */
    public function setAttributes(?StockAttributesWrite $value): void {
        $this->attributes = $value;
    }

    /**
     * Sets the relationships property value. The relationships property
     * @param StockRelationshipsWrite|null $value Value to set for the relationships property.
    */
    public function setRelationships(?StockRelationshipsWrite $value): void {
        $this->relationships = $value;
    }

    /**
     * Sets the stockCode property value. The stockCode property
     * @param string|null $value Value to set for the stockCode property.
    */
    public function setStockCode(?string $value): void {
        $this->stockCode = $value;
    }

}
