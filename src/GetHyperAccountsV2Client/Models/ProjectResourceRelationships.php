<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class ProjectResourceRelationships implements Parsable
{
    /**
     * @var ProjectOnlyTransactionRelatedListRelationship|null $projectOnlyTransactions The projectOnlyTransactions property
    */
    private ?ProjectOnlyTransactionRelatedListRelationship $projectOnlyTransactions = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return ProjectResourceRelationships
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): ProjectResourceRelationships {
        return new ProjectResourceRelationships();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'projectOnlyTransactions' => fn(ParseNode $n) => $o->setProjectOnlyTransactions($n->getObjectValue([ProjectOnlyTransactionRelatedListRelationship::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the projectOnlyTransactions property value. The projectOnlyTransactions property
     * @return ProjectOnlyTransactionRelatedListRelationship|null
    */
    public function getProjectOnlyTransactions(): ?ProjectOnlyTransactionRelatedListRelationship {
        return $this->projectOnlyTransactions;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('projectOnlyTransactions', $this->getProjectOnlyTransactions());
    }

    /**
     * Sets the projectOnlyTransactions property value. The projectOnlyTransactions property
     * @param ProjectOnlyTransactionRelatedListRelationship|null $value Value to set for the projectOnlyTransactions property.
    */
    public function setProjectOnlyTransactions(?ProjectOnlyTransactionRelatedListRelationship $value): void {
        $this->projectOnlyTransactions = $value;
    }

}
