<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class TaxCodeGetDto implements Parsable
{
    /**
     * @var TaxCodeAttributesRead|null $attributes The attributes property
    */
    private ?TaxCodeAttributesRead $attributes = null;

    /**
     * @var ResourceLinks|null $links The links property
    */
    private ?ResourceLinks $links = null;

    /**
     * @var int|null $taxCodeId The taxCodeId property
    */
    private ?int $taxCodeId = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return TaxCodeGetDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): TaxCodeGetDto {
        return new TaxCodeGetDto();
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return TaxCodeAttributesRead|null
    */
    public function getAttributes(): ?TaxCodeAttributesRead {
        return $this->attributes;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([TaxCodeAttributesRead::class, 'createFromDiscriminatorValue'])),
            'links' => fn(ParseNode $n) => $o->setLinks($n->getObjectValue([ResourceLinks::class, 'createFromDiscriminatorValue'])),
            'taxCodeId' => fn(ParseNode $n) => $o->setTaxCodeId($n->getIntegerValue()),
        ];
    }

    /**
     * Gets the links property value. The links property
     * @return ResourceLinks|null
    */
    public function getLinks(): ?ResourceLinks {
        return $this->links;
    }

    /**
     * Gets the taxCodeId property value. The taxCodeId property
     * @return int|null
    */
    public function getTaxCodeId(): ?int {
        return $this->taxCodeId;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('attributes', $this->getAttributes());
        $writer->writeObjectValue('links', $this->getLinks());
        $writer->writeIntegerValue('taxCodeId', $this->getTaxCodeId());
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param TaxCodeAttributesRead|null $value Value to set for the attributes property.
    */
    public function setAttributes(?TaxCodeAttributesRead $value): void {
        $this->attributes = $value;
    }

    /**
     * Sets the links property value. The links property
     * @param ResourceLinks|null $value Value to set for the links property.
    */
    public function setLinks(?ResourceLinks $value): void {
        $this->links = $value;
    }

    /**
     * Sets the taxCodeId property value. The taxCodeId property
     * @param int|null $value Value to set for the taxCodeId property.
    */
    public function setTaxCodeId(?int $value): void {
        $this->taxCodeId = $value;
    }

}
