<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class SupplierRelationships implements Parsable
{
    /**
     * @var GoodsReceivedNoteRelatedListRelationship|null $goodsReceivedNotes The goodsReceivedNotes property
    */
    private ?GoodsReceivedNoteRelatedListRelationship $goodsReceivedNotes = null;

    /**
     * @var PurchaseOrderRelatedListRelationship|null $purchaseOrders The purchaseOrders property
    */
    private ?PurchaseOrderRelatedListRelationship $purchaseOrders = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return SupplierRelationships
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): SupplierRelationships {
        return new SupplierRelationships();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'goodsReceivedNotes' => fn(ParseNode $n) => $o->setGoodsReceivedNotes($n->getObjectValue([GoodsReceivedNoteRelatedListRelationship::class, 'createFromDiscriminatorValue'])),
            'purchaseOrders' => fn(ParseNode $n) => $o->setPurchaseOrders($n->getObjectValue([PurchaseOrderRelatedListRelationship::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the goodsReceivedNotes property value. The goodsReceivedNotes property
     * @return GoodsReceivedNoteRelatedListRelationship|null
    */
    public function getGoodsReceivedNotes(): ?GoodsReceivedNoteRelatedListRelationship {
        return $this->goodsReceivedNotes;
    }

    /**
     * Gets the purchaseOrders property value. The purchaseOrders property
     * @return PurchaseOrderRelatedListRelationship|null
    */
    public function getPurchaseOrders(): ?PurchaseOrderRelatedListRelationship {
        return $this->purchaseOrders;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('goodsReceivedNotes', $this->getGoodsReceivedNotes());
        $writer->writeObjectValue('purchaseOrders', $this->getPurchaseOrders());
    }

    /**
     * Sets the goodsReceivedNotes property value. The goodsReceivedNotes property
     * @param GoodsReceivedNoteRelatedListRelationship|null $value Value to set for the goodsReceivedNotes property.
    */
    public function setGoodsReceivedNotes(?GoodsReceivedNoteRelatedListRelationship $value): void {
        $this->goodsReceivedNotes = $value;
    }

    /**
     * Sets the purchaseOrders property value. The purchaseOrders property
     * @param PurchaseOrderRelatedListRelationship|null $value Value to set for the purchaseOrders property.
    */
    public function setPurchaseOrders(?PurchaseOrderRelatedListRelationship $value): void {
        $this->purchaseOrders = $value;
    }

}
