<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class PurchaseOrderIncluded implements Parsable
{
    /**
     * @var GoodsReceivedNoteCollection|null $goodsReceivedNotes The goodsReceivedNotes property
    */
    private ?GoodsReceivedNoteCollection $goodsReceivedNotes = null;

    /**
     * @var PurchaseOrderItemCollection|null $items The items property
    */
    private ?PurchaseOrderItemCollection $items = null;

    /**
     * @var NominalGetDto|null $nominalCode The nominalCode property
    */
    private ?NominalGetDto $nominalCode = null;

    /**
     * @var ProjectGetDto|null $project The project property
    */
    private ?ProjectGetDto $project = null;

    /**
     * @var SupplierGetDto|null $supplier The supplier property
    */
    private ?SupplierGetDto $supplier = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return PurchaseOrderIncluded
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): PurchaseOrderIncluded {
        return new PurchaseOrderIncluded();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'goodsReceivedNotes' => fn(ParseNode $n) => $o->setGoodsReceivedNotes($n->getObjectValue([GoodsReceivedNoteCollection::class, 'createFromDiscriminatorValue'])),
            'items' => fn(ParseNode $n) => $o->setItems($n->getObjectValue([PurchaseOrderItemCollection::class, 'createFromDiscriminatorValue'])),
            'nominalCode' => fn(ParseNode $n) => $o->setNominalCode($n->getObjectValue([NominalGetDto::class, 'createFromDiscriminatorValue'])),
            'project' => fn(ParseNode $n) => $o->setProject($n->getObjectValue([ProjectGetDto::class, 'createFromDiscriminatorValue'])),
            'supplier' => fn(ParseNode $n) => $o->setSupplier($n->getObjectValue([SupplierGetDto::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the goodsReceivedNotes property value. The goodsReceivedNotes property
     * @return GoodsReceivedNoteCollection|null
    */
    public function getGoodsReceivedNotes(): ?GoodsReceivedNoteCollection {
        return $this->goodsReceivedNotes;
    }

    /**
     * Gets the items property value. The items property
     * @return PurchaseOrderItemCollection|null
    */
    public function getItems(): ?PurchaseOrderItemCollection {
        return $this->items;
    }

    /**
     * Gets the nominalCode property value. The nominalCode property
     * @return NominalGetDto|null
    */
    public function getNominalCode(): ?NominalGetDto {
        return $this->nominalCode;
    }

    /**
     * Gets the project property value. The project property
     * @return ProjectGetDto|null
    */
    public function getProject(): ?ProjectGetDto {
        return $this->project;
    }

    /**
     * Gets the supplier property value. The supplier property
     * @return SupplierGetDto|null
    */
    public function getSupplier(): ?SupplierGetDto {
        return $this->supplier;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('goodsReceivedNotes', $this->getGoodsReceivedNotes());
        $writer->writeObjectValue('items', $this->getItems());
        $writer->writeObjectValue('nominalCode', $this->getNominalCode());
        $writer->writeObjectValue('project', $this->getProject());
        $writer->writeObjectValue('supplier', $this->getSupplier());
    }

    /**
     * Sets the goodsReceivedNotes property value. The goodsReceivedNotes property
     * @param GoodsReceivedNoteCollection|null $value Value to set for the goodsReceivedNotes property.
    */
    public function setGoodsReceivedNotes(?GoodsReceivedNoteCollection $value): void {
        $this->goodsReceivedNotes = $value;
    }

    /**
     * Sets the items property value. The items property
     * @param PurchaseOrderItemCollection|null $value Value to set for the items property.
    */
    public function setItems(?PurchaseOrderItemCollection $value): void {
        $this->items = $value;
    }

    /**
     * Sets the nominalCode property value. The nominalCode property
     * @param NominalGetDto|null $value Value to set for the nominalCode property.
    */
    public function setNominalCode(?NominalGetDto $value): void {
        $this->nominalCode = $value;
    }

    /**
     * Sets the project property value. The project property
     * @param ProjectGetDto|null $value Value to set for the project property.
    */
    public function setProject(?ProjectGetDto $value): void {
        $this->project = $value;
    }

    /**
     * Sets the supplier property value. The supplier property
     * @param SupplierGetDto|null $value Value to set for the supplier property.
    */
    public function setSupplier(?SupplierGetDto $value): void {
        $this->supplier = $value;
    }

}
