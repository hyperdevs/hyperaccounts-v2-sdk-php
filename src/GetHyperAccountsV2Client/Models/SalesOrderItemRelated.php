<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class SalesOrderItemRelated implements Parsable
{
    /**
     * @var int|null $itemid The itemid property
    */
    private ?int $itemid = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return SalesOrderItemRelated
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): SalesOrderItemRelated {
        return new SalesOrderItemRelated();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'itemid' => fn(ParseNode $n) => $o->setItemid($n->getIntegerValue()),
        ];
    }

    /**
     * Gets the itemid property value. The itemid property
     * @return int|null
    */
    public function getItemid(): ?int {
        return $this->itemid;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeIntegerValue('itemid', $this->getItemid());
    }

    /**
     * Sets the itemid property value. The itemid property
     * @param int|null $value Value to set for the itemid property.
    */
    public function setItemid(?int $value): void {
        $this->itemid = $value;
    }

}
