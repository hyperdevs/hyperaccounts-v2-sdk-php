<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use DateTime;
use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class PurchaseOrderAttributesWrite implements Parsable
{
    /**
     * @var string|null $address1 The address1 property
    */
    private ?string $address1 = null;

    /**
     * @var string|null $address2 The address2 property
    */
    private ?string $address2 = null;

    /**
     * @var string|null $address3 The address3 property
    */
    private ?string $address3 = null;

    /**
     * @var string|null $address4 The address4 property
    */
    private ?string $address4 = null;

    /**
     * @var string|null $address5 The address5 property
    */
    private ?string $address5 = null;

    /**
     * @var string|null $analysis1 The analysis1 property
    */
    private ?string $analysis1 = null;

    /**
     * @var string|null $analysis2 The analysis2 property
    */
    private ?string $analysis2 = null;

    /**
     * @var string|null $analysis3 The analysis3 property
    */
    private ?string $analysis3 = null;

    /**
     * @var int|null $carrDeptNumber The carrDeptNumber property
    */
    private ?int $carrDeptNumber = null;

    /**
     * @var float|null $carrNet The carrNet property
    */
    private ?float $carrNet = null;

    /**
     * @var string|null $carrNomCode The carrNomCode property
    */
    private ?string $carrNomCode = null;

    /**
     * @var float|null $carrTax The carrTax property
    */
    private ?float $carrTax = null;

    /**
     * @var PurchaseOrderAttributesWrite_carrTaxCode|null $carrTaxCode The carrTaxCode property
    */
    private ?PurchaseOrderAttributesWrite_carrTaxCode $carrTaxCode = null;

    /**
     * @var string|null $contactName The contactName property
    */
    private ?string $contactName = null;

    /**
     * @var int|null $currency The currency property
    */
    private ?int $currency = null;

    /**
     * @var string|null $delAddress1 The delAddress1 property
    */
    private ?string $delAddress1 = null;

    /**
     * @var string|null $delAddress2 The delAddress2 property
    */
    private ?string $delAddress2 = null;

    /**
     * @var string|null $delAddress3 The delAddress3 property
    */
    private ?string $delAddress3 = null;

    /**
     * @var string|null $delAddress4 The delAddress4 property
    */
    private ?string $delAddress4 = null;

    /**
     * @var string|null $delAddress5 The delAddress5 property
    */
    private ?string $delAddress5 = null;

    /**
     * @var float|null $euroGross The euroGross property
    */
    private ?float $euroGross = null;

    /**
     * @var float|null $euroRate The euroRate property
    */
    private ?float $euroRate = null;

    /**
     * @var float|null $foreignRate The foreignRate property
    */
    private ?float $foreignRate = null;

    /**
     * @var float|null $itemsNet The itemsNet property
    */
    private ?float $itemsNet = null;

    /**
     * @var float|null $itemsTax The itemsTax property
    */
    private ?float $itemsTax = null;

    /**
     * @var string|null $name The name property
    */
    private ?string $name = null;

    /**
     * @var string|null $notes1 The notes1 property
    */
    private ?string $notes1 = null;

    /**
     * @var string|null $notes2 The notes2 property
    */
    private ?string $notes2 = null;

    /**
     * @var string|null $notes3 The notes3 property
    */
    private ?string $notes3 = null;

    /**
     * @var DateTime|null $orderDate The orderDate property
    */
    private ?DateTime $orderDate = null;

    /**
     * @var string|null $paymentRef The paymentRef property
    */
    private ?string $paymentRef = null;

    /**
     * @var int|null $paymentType The paymentType property
    */
    private ?int $paymentType = null;

    /**
     * @var int|null $printedCode The printedCode property
    */
    private ?int $printedCode = null;

    /**
     * @var float|null $settlementDiscRate The settlementDiscRate property
    */
    private ?float $settlementDiscRate = null;

    /**
     * @var int|null $settlementDueDays The settlementDueDays property
    */
    private ?int $settlementDueDays = null;

    /**
     * @var string|null $takenBy The takenBy property
    */
    private ?string $takenBy = null;

    /**
     * @var string|null $vatRegNumber The vatRegNumber property
    */
    private ?string $vatRegNumber = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return PurchaseOrderAttributesWrite
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): PurchaseOrderAttributesWrite {
        return new PurchaseOrderAttributesWrite();
    }

    /**
     * Gets the address1 property value. The address1 property
     * @return string|null
    */
    public function getAddress1(): ?string {
        return $this->address1;
    }

    /**
     * Gets the address2 property value. The address2 property
     * @return string|null
    */
    public function getAddress2(): ?string {
        return $this->address2;
    }

    /**
     * Gets the address3 property value. The address3 property
     * @return string|null
    */
    public function getAddress3(): ?string {
        return $this->address3;
    }

    /**
     * Gets the address4 property value. The address4 property
     * @return string|null
    */
    public function getAddress4(): ?string {
        return $this->address4;
    }

    /**
     * Gets the address5 property value. The address5 property
     * @return string|null
    */
    public function getAddress5(): ?string {
        return $this->address5;
    }

    /**
     * Gets the analysis1 property value. The analysis1 property
     * @return string|null
    */
    public function getAnalysis1(): ?string {
        return $this->analysis1;
    }

    /**
     * Gets the analysis2 property value. The analysis2 property
     * @return string|null
    */
    public function getAnalysis2(): ?string {
        return $this->analysis2;
    }

    /**
     * Gets the analysis3 property value. The analysis3 property
     * @return string|null
    */
    public function getAnalysis3(): ?string {
        return $this->analysis3;
    }

    /**
     * Gets the carrDeptNumber property value. The carrDeptNumber property
     * @return int|null
    */
    public function getCarrDeptNumber(): ?int {
        return $this->carrDeptNumber;
    }

    /**
     * Gets the carrNet property value. The carrNet property
     * @return float|null
    */
    public function getCarrNet(): ?float {
        return $this->carrNet;
    }

    /**
     * Gets the carrNomCode property value. The carrNomCode property
     * @return string|null
    */
    public function getCarrNomCode(): ?string {
        return $this->carrNomCode;
    }

    /**
     * Gets the carrTax property value. The carrTax property
     * @return float|null
    */
    public function getCarrTax(): ?float {
        return $this->carrTax;
    }

    /**
     * Gets the carrTaxCode property value. The carrTaxCode property
     * @return PurchaseOrderAttributesWrite_carrTaxCode|null
    */
    public function getCarrTaxCode(): ?PurchaseOrderAttributesWrite_carrTaxCode {
        return $this->carrTaxCode;
    }

    /**
     * Gets the contactName property value. The contactName property
     * @return string|null
    */
    public function getContactName(): ?string {
        return $this->contactName;
    }

    /**
     * Gets the currency property value. The currency property
     * @return int|null
    */
    public function getCurrency(): ?int {
        return $this->currency;
    }

    /**
     * Gets the delAddress1 property value. The delAddress1 property
     * @return string|null
    */
    public function getDelAddress1(): ?string {
        return $this->delAddress1;
    }

    /**
     * Gets the delAddress2 property value. The delAddress2 property
     * @return string|null
    */
    public function getDelAddress2(): ?string {
        return $this->delAddress2;
    }

    /**
     * Gets the delAddress3 property value. The delAddress3 property
     * @return string|null
    */
    public function getDelAddress3(): ?string {
        return $this->delAddress3;
    }

    /**
     * Gets the delAddress4 property value. The delAddress4 property
     * @return string|null
    */
    public function getDelAddress4(): ?string {
        return $this->delAddress4;
    }

    /**
     * Gets the delAddress5 property value. The delAddress5 property
     * @return string|null
    */
    public function getDelAddress5(): ?string {
        return $this->delAddress5;
    }

    /**
     * Gets the euroGross property value. The euroGross property
     * @return float|null
    */
    public function getEuroGross(): ?float {
        return $this->euroGross;
    }

    /**
     * Gets the euroRate property value. The euroRate property
     * @return float|null
    */
    public function getEuroRate(): ?float {
        return $this->euroRate;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'address1' => fn(ParseNode $n) => $o->setAddress1($n->getStringValue()),
            'address2' => fn(ParseNode $n) => $o->setAddress2($n->getStringValue()),
            'address3' => fn(ParseNode $n) => $o->setAddress3($n->getStringValue()),
            'address4' => fn(ParseNode $n) => $o->setAddress4($n->getStringValue()),
            'address5' => fn(ParseNode $n) => $o->setAddress5($n->getStringValue()),
            'analysis1' => fn(ParseNode $n) => $o->setAnalysis1($n->getStringValue()),
            'analysis2' => fn(ParseNode $n) => $o->setAnalysis2($n->getStringValue()),
            'analysis3' => fn(ParseNode $n) => $o->setAnalysis3($n->getStringValue()),
            'carrDeptNumber' => fn(ParseNode $n) => $o->setCarrDeptNumber($n->getIntegerValue()),
            'carrNet' => fn(ParseNode $n) => $o->setCarrNet($n->getFloatValue()),
            'carrNomCode' => fn(ParseNode $n) => $o->setCarrNomCode($n->getStringValue()),
            'carrTax' => fn(ParseNode $n) => $o->setCarrTax($n->getFloatValue()),
            'carrTaxCode' => fn(ParseNode $n) => $o->setCarrTaxCode($n->getEnumValue(PurchaseOrderAttributesWrite_carrTaxCode::class)),
            'contactName' => fn(ParseNode $n) => $o->setContactName($n->getStringValue()),
            'currency' => fn(ParseNode $n) => $o->setCurrency($n->getIntegerValue()),
            'delAddress1' => fn(ParseNode $n) => $o->setDelAddress1($n->getStringValue()),
            'delAddress2' => fn(ParseNode $n) => $o->setDelAddress2($n->getStringValue()),
            'delAddress3' => fn(ParseNode $n) => $o->setDelAddress3($n->getStringValue()),
            'delAddress4' => fn(ParseNode $n) => $o->setDelAddress4($n->getStringValue()),
            'delAddress5' => fn(ParseNode $n) => $o->setDelAddress5($n->getStringValue()),
            'euroGross' => fn(ParseNode $n) => $o->setEuroGross($n->getFloatValue()),
            'euroRate' => fn(ParseNode $n) => $o->setEuroRate($n->getFloatValue()),
            'foreignRate' => fn(ParseNode $n) => $o->setForeignRate($n->getFloatValue()),
            'itemsNet' => fn(ParseNode $n) => $o->setItemsNet($n->getFloatValue()),
            'itemsTax' => fn(ParseNode $n) => $o->setItemsTax($n->getFloatValue()),
            'name' => fn(ParseNode $n) => $o->setName($n->getStringValue()),
            'notes1' => fn(ParseNode $n) => $o->setNotes1($n->getStringValue()),
            'notes2' => fn(ParseNode $n) => $o->setNotes2($n->getStringValue()),
            'notes3' => fn(ParseNode $n) => $o->setNotes3($n->getStringValue()),
            'orderDate' => fn(ParseNode $n) => $o->setOrderDate($n->getDateTimeValue()),
            'paymentRef' => fn(ParseNode $n) => $o->setPaymentRef($n->getStringValue()),
            'paymentType' => fn(ParseNode $n) => $o->setPaymentType($n->getIntegerValue()),
            'printedCode' => fn(ParseNode $n) => $o->setPrintedCode($n->getIntegerValue()),
            'settlementDiscRate' => fn(ParseNode $n) => $o->setSettlementDiscRate($n->getFloatValue()),
            'settlementDueDays' => fn(ParseNode $n) => $o->setSettlementDueDays($n->getIntegerValue()),
            'takenBy' => fn(ParseNode $n) => $o->setTakenBy($n->getStringValue()),
            'vatRegNumber' => fn(ParseNode $n) => $o->setVatRegNumber($n->getStringValue()),
        ];
    }

    /**
     * Gets the foreignRate property value. The foreignRate property
     * @return float|null
    */
    public function getForeignRate(): ?float {
        return $this->foreignRate;
    }

    /**
     * Gets the itemsNet property value. The itemsNet property
     * @return float|null
    */
    public function getItemsNet(): ?float {
        return $this->itemsNet;
    }

    /**
     * Gets the itemsTax property value. The itemsTax property
     * @return float|null
    */
    public function getItemsTax(): ?float {
        return $this->itemsTax;
    }

    /**
     * Gets the name property value. The name property
     * @return string|null
    */
    public function getName(): ?string {
        return $this->name;
    }

    /**
     * Gets the notes1 property value. The notes1 property
     * @return string|null
    */
    public function getNotes1(): ?string {
        return $this->notes1;
    }

    /**
     * Gets the notes2 property value. The notes2 property
     * @return string|null
    */
    public function getNotes2(): ?string {
        return $this->notes2;
    }

    /**
     * Gets the notes3 property value. The notes3 property
     * @return string|null
    */
    public function getNotes3(): ?string {
        return $this->notes3;
    }

    /**
     * Gets the orderDate property value. The orderDate property
     * @return DateTime|null
    */
    public function getOrderDate(): ?DateTime {
        return $this->orderDate;
    }

    /**
     * Gets the paymentRef property value. The paymentRef property
     * @return string|null
    */
    public function getPaymentRef(): ?string {
        return $this->paymentRef;
    }

    /**
     * Gets the paymentType property value. The paymentType property
     * @return int|null
    */
    public function getPaymentType(): ?int {
        return $this->paymentType;
    }

    /**
     * Gets the printedCode property value. The printedCode property
     * @return int|null
    */
    public function getPrintedCode(): ?int {
        return $this->printedCode;
    }

    /**
     * Gets the settlementDiscRate property value. The settlementDiscRate property
     * @return float|null
    */
    public function getSettlementDiscRate(): ?float {
        return $this->settlementDiscRate;
    }

    /**
     * Gets the settlementDueDays property value. The settlementDueDays property
     * @return int|null
    */
    public function getSettlementDueDays(): ?int {
        return $this->settlementDueDays;
    }

    /**
     * Gets the takenBy property value. The takenBy property
     * @return string|null
    */
    public function getTakenBy(): ?string {
        return $this->takenBy;
    }

    /**
     * Gets the vatRegNumber property value. The vatRegNumber property
     * @return string|null
    */
    public function getVatRegNumber(): ?string {
        return $this->vatRegNumber;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeStringValue('address1', $this->getAddress1());
        $writer->writeStringValue('address2', $this->getAddress2());
        $writer->writeStringValue('address3', $this->getAddress3());
        $writer->writeStringValue('address4', $this->getAddress4());
        $writer->writeStringValue('address5', $this->getAddress5());
        $writer->writeStringValue('analysis1', $this->getAnalysis1());
        $writer->writeStringValue('analysis2', $this->getAnalysis2());
        $writer->writeStringValue('analysis3', $this->getAnalysis3());
        $writer->writeIntegerValue('carrDeptNumber', $this->getCarrDeptNumber());
        $writer->writeFloatValue('carrNet', $this->getCarrNet());
        $writer->writeStringValue('carrNomCode', $this->getCarrNomCode());
        $writer->writeFloatValue('carrTax', $this->getCarrTax());
        $writer->writeEnumValue('carrTaxCode', $this->getCarrTaxCode());
        $writer->writeStringValue('contactName', $this->getContactName());
        $writer->writeIntegerValue('currency', $this->getCurrency());
        $writer->writeStringValue('delAddress1', $this->getDelAddress1());
        $writer->writeStringValue('delAddress2', $this->getDelAddress2());
        $writer->writeStringValue('delAddress3', $this->getDelAddress3());
        $writer->writeStringValue('delAddress4', $this->getDelAddress4());
        $writer->writeStringValue('delAddress5', $this->getDelAddress5());
        $writer->writeFloatValue('euroGross', $this->getEuroGross());
        $writer->writeFloatValue('euroRate', $this->getEuroRate());
        $writer->writeFloatValue('foreignRate', $this->getForeignRate());
        $writer->writeFloatValue('itemsNet', $this->getItemsNet());
        $writer->writeFloatValue('itemsTax', $this->getItemsTax());
        $writer->writeStringValue('name', $this->getName());
        $writer->writeStringValue('notes1', $this->getNotes1());
        $writer->writeStringValue('notes2', $this->getNotes2());
        $writer->writeStringValue('notes3', $this->getNotes3());
        $writer->writeDateTimeValue('orderDate', $this->getOrderDate());
        $writer->writeStringValue('paymentRef', $this->getPaymentRef());
        $writer->writeIntegerValue('paymentType', $this->getPaymentType());
        $writer->writeIntegerValue('printedCode', $this->getPrintedCode());
        $writer->writeFloatValue('settlementDiscRate', $this->getSettlementDiscRate());
        $writer->writeIntegerValue('settlementDueDays', $this->getSettlementDueDays());
        $writer->writeStringValue('takenBy', $this->getTakenBy());
        $writer->writeStringValue('vatRegNumber', $this->getVatRegNumber());
    }

    /**
     * Sets the address1 property value. The address1 property
     * @param string|null $value Value to set for the address1 property.
    */
    public function setAddress1(?string $value): void {
        $this->address1 = $value;
    }

    /**
     * Sets the address2 property value. The address2 property
     * @param string|null $value Value to set for the address2 property.
    */
    public function setAddress2(?string $value): void {
        $this->address2 = $value;
    }

    /**
     * Sets the address3 property value. The address3 property
     * @param string|null $value Value to set for the address3 property.
    */
    public function setAddress3(?string $value): void {
        $this->address3 = $value;
    }

    /**
     * Sets the address4 property value. The address4 property
     * @param string|null $value Value to set for the address4 property.
    */
    public function setAddress4(?string $value): void {
        $this->address4 = $value;
    }

    /**
     * Sets the address5 property value. The address5 property
     * @param string|null $value Value to set for the address5 property.
    */
    public function setAddress5(?string $value): void {
        $this->address5 = $value;
    }

    /**
     * Sets the analysis1 property value. The analysis1 property
     * @param string|null $value Value to set for the analysis1 property.
    */
    public function setAnalysis1(?string $value): void {
        $this->analysis1 = $value;
    }

    /**
     * Sets the analysis2 property value. The analysis2 property
     * @param string|null $value Value to set for the analysis2 property.
    */
    public function setAnalysis2(?string $value): void {
        $this->analysis2 = $value;
    }

    /**
     * Sets the analysis3 property value. The analysis3 property
     * @param string|null $value Value to set for the analysis3 property.
    */
    public function setAnalysis3(?string $value): void {
        $this->analysis3 = $value;
    }

    /**
     * Sets the carrDeptNumber property value. The carrDeptNumber property
     * @param int|null $value Value to set for the carrDeptNumber property.
    */
    public function setCarrDeptNumber(?int $value): void {
        $this->carrDeptNumber = $value;
    }

    /**
     * Sets the carrNet property value. The carrNet property
     * @param float|null $value Value to set for the carrNet property.
    */
    public function setCarrNet(?float $value): void {
        $this->carrNet = $value;
    }

    /**
     * Sets the carrNomCode property value. The carrNomCode property
     * @param string|null $value Value to set for the carrNomCode property.
    */
    public function setCarrNomCode(?string $value): void {
        $this->carrNomCode = $value;
    }

    /**
     * Sets the carrTax property value. The carrTax property
     * @param float|null $value Value to set for the carrTax property.
    */
    public function setCarrTax(?float $value): void {
        $this->carrTax = $value;
    }

    /**
     * Sets the carrTaxCode property value. The carrTaxCode property
     * @param PurchaseOrderAttributesWrite_carrTaxCode|null $value Value to set for the carrTaxCode property.
    */
    public function setCarrTaxCode(?PurchaseOrderAttributesWrite_carrTaxCode $value): void {
        $this->carrTaxCode = $value;
    }

    /**
     * Sets the contactName property value. The contactName property
     * @param string|null $value Value to set for the contactName property.
    */
    public function setContactName(?string $value): void {
        $this->contactName = $value;
    }

    /**
     * Sets the currency property value. The currency property
     * @param int|null $value Value to set for the currency property.
    */
    public function setCurrency(?int $value): void {
        $this->currency = $value;
    }

    /**
     * Sets the delAddress1 property value. The delAddress1 property
     * @param string|null $value Value to set for the delAddress1 property.
    */
    public function setDelAddress1(?string $value): void {
        $this->delAddress1 = $value;
    }

    /**
     * Sets the delAddress2 property value. The delAddress2 property
     * @param string|null $value Value to set for the delAddress2 property.
    */
    public function setDelAddress2(?string $value): void {
        $this->delAddress2 = $value;
    }

    /**
     * Sets the delAddress3 property value. The delAddress3 property
     * @param string|null $value Value to set for the delAddress3 property.
    */
    public function setDelAddress3(?string $value): void {
        $this->delAddress3 = $value;
    }

    /**
     * Sets the delAddress4 property value. The delAddress4 property
     * @param string|null $value Value to set for the delAddress4 property.
    */
    public function setDelAddress4(?string $value): void {
        $this->delAddress4 = $value;
    }

    /**
     * Sets the delAddress5 property value. The delAddress5 property
     * @param string|null $value Value to set for the delAddress5 property.
    */
    public function setDelAddress5(?string $value): void {
        $this->delAddress5 = $value;
    }

    /**
     * Sets the euroGross property value. The euroGross property
     * @param float|null $value Value to set for the euroGross property.
    */
    public function setEuroGross(?float $value): void {
        $this->euroGross = $value;
    }

    /**
     * Sets the euroRate property value. The euroRate property
     * @param float|null $value Value to set for the euroRate property.
    */
    public function setEuroRate(?float $value): void {
        $this->euroRate = $value;
    }

    /**
     * Sets the foreignRate property value. The foreignRate property
     * @param float|null $value Value to set for the foreignRate property.
    */
    public function setForeignRate(?float $value): void {
        $this->foreignRate = $value;
    }

    /**
     * Sets the itemsNet property value. The itemsNet property
     * @param float|null $value Value to set for the itemsNet property.
    */
    public function setItemsNet(?float $value): void {
        $this->itemsNet = $value;
    }

    /**
     * Sets the itemsTax property value. The itemsTax property
     * @param float|null $value Value to set for the itemsTax property.
    */
    public function setItemsTax(?float $value): void {
        $this->itemsTax = $value;
    }

    /**
     * Sets the name property value. The name property
     * @param string|null $value Value to set for the name property.
    */
    public function setName(?string $value): void {
        $this->name = $value;
    }

    /**
     * Sets the notes1 property value. The notes1 property
     * @param string|null $value Value to set for the notes1 property.
    */
    public function setNotes1(?string $value): void {
        $this->notes1 = $value;
    }

    /**
     * Sets the notes2 property value. The notes2 property
     * @param string|null $value Value to set for the notes2 property.
    */
    public function setNotes2(?string $value): void {
        $this->notes2 = $value;
    }

    /**
     * Sets the notes3 property value. The notes3 property
     * @param string|null $value Value to set for the notes3 property.
    */
    public function setNotes3(?string $value): void {
        $this->notes3 = $value;
    }

    /**
     * Sets the orderDate property value. The orderDate property
     * @param DateTime|null $value Value to set for the orderDate property.
    */
    public function setOrderDate(?DateTime $value): void {
        $this->orderDate = $value;
    }

    /**
     * Sets the paymentRef property value. The paymentRef property
     * @param string|null $value Value to set for the paymentRef property.
    */
    public function setPaymentRef(?string $value): void {
        $this->paymentRef = $value;
    }

    /**
     * Sets the paymentType property value. The paymentType property
     * @param int|null $value Value to set for the paymentType property.
    */
    public function setPaymentType(?int $value): void {
        $this->paymentType = $value;
    }

    /**
     * Sets the printedCode property value. The printedCode property
     * @param int|null $value Value to set for the printedCode property.
    */
    public function setPrintedCode(?int $value): void {
        $this->printedCode = $value;
    }

    /**
     * Sets the settlementDiscRate property value. The settlementDiscRate property
     * @param float|null $value Value to set for the settlementDiscRate property.
    */
    public function setSettlementDiscRate(?float $value): void {
        $this->settlementDiscRate = $value;
    }

    /**
     * Sets the settlementDueDays property value. The settlementDueDays property
     * @param int|null $value Value to set for the settlementDueDays property.
    */
    public function setSettlementDueDays(?int $value): void {
        $this->settlementDueDays = $value;
    }

    /**
     * Sets the takenBy property value. The takenBy property
     * @param string|null $value Value to set for the takenBy property.
    */
    public function setTakenBy(?string $value): void {
        $this->takenBy = $value;
    }

    /**
     * Sets the vatRegNumber property value. The vatRegNumber property
     * @param string|null $value Value to set for the vatRegNumber property.
    */
    public function setVatRegNumber(?string $value): void {
        $this->vatRegNumber = $value;
    }

}
