<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class ProjectStatusRelationships implements Parsable
{
    /**
     * @var ProjectRelatedListRelationship|null $projects The projects property
    */
    private ?ProjectRelatedListRelationship $projects = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return ProjectStatusRelationships
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): ProjectStatusRelationships {
        return new ProjectStatusRelationships();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'projects' => fn(ParseNode $n) => $o->setProjects($n->getObjectValue([ProjectRelatedListRelationship::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the projects property value. The projects property
     * @return ProjectRelatedListRelationship|null
    */
    public function getProjects(): ?ProjectRelatedListRelationship {
        return $this->projects;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('projects', $this->getProjects());
    }

    /**
     * Sets the projects property value. The projects property
     * @param ProjectRelatedListRelationship|null $value Value to set for the projects property.
    */
    public function setProjects(?ProjectRelatedListRelationship $value): void {
        $this->projects = $value;
    }

}
