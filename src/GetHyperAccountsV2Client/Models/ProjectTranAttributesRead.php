<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use DateTime;
use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class ProjectTranAttributesRead implements Parsable
{
    /**
     * @var int|null $auditTrailId The auditTrailId property
    */
    private ?int $auditTrailId = null;

    /**
     * @var DateTime|null $date The date property
    */
    private ?DateTime $date = null;

    /**
     * @var int|null $popItemId The popItemId property
    */
    private ?int $popItemId = null;

    /**
     * @var DateTime|null $recordCreateDate The recordCreateDate property
    */
    private ?DateTime $recordCreateDate = null;

    /**
     * @var int|null $recordDeleted The recordDeleted property
    */
    private ?int $recordDeleted = null;

    /**
     * @var DateTime|null $recordModifyDate The recordModifyDate property
    */
    private ?DateTime $recordModifyDate = null;

    /**
     * @var int|null $revenueCodeId The revenueCodeId property
    */
    private ?int $revenueCodeId = null;

    /**
     * @var int|null $stockAllocationId The stockAllocationId property
    */
    private ?int $stockAllocationId = null;

    /**
     * @var int|null $stockTrailId The stockTrailId property
    */
    private ?int $stockTrailId = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return ProjectTranAttributesRead
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): ProjectTranAttributesRead {
        return new ProjectTranAttributesRead();
    }

    /**
     * Gets the auditTrailId property value. The auditTrailId property
     * @return int|null
    */
    public function getAuditTrailId(): ?int {
        return $this->auditTrailId;
    }

    /**
     * Gets the date property value. The date property
     * @return DateTime|null
    */
    public function getDate(): ?DateTime {
        return $this->date;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'auditTrailId' => fn(ParseNode $n) => $o->setAuditTrailId($n->getIntegerValue()),
            'date' => fn(ParseNode $n) => $o->setDate($n->getDateTimeValue()),
            'popItemId' => fn(ParseNode $n) => $o->setPopItemId($n->getIntegerValue()),
            'recordCreateDate' => fn(ParseNode $n) => $o->setRecordCreateDate($n->getDateTimeValue()),
            'recordDeleted' => fn(ParseNode $n) => $o->setRecordDeleted($n->getIntegerValue()),
            'recordModifyDate' => fn(ParseNode $n) => $o->setRecordModifyDate($n->getDateTimeValue()),
            'revenueCodeId' => fn(ParseNode $n) => $o->setRevenueCodeId($n->getIntegerValue()),
            'stockAllocationId' => fn(ParseNode $n) => $o->setStockAllocationId($n->getIntegerValue()),
            'stockTrailId' => fn(ParseNode $n) => $o->setStockTrailId($n->getIntegerValue()),
        ];
    }

    /**
     * Gets the popItemId property value. The popItemId property
     * @return int|null
    */
    public function getPopItemId(): ?int {
        return $this->popItemId;
    }

    /**
     * Gets the recordCreateDate property value. The recordCreateDate property
     * @return DateTime|null
    */
    public function getRecordCreateDate(): ?DateTime {
        return $this->recordCreateDate;
    }

    /**
     * Gets the recordDeleted property value. The recordDeleted property
     * @return int|null
    */
    public function getRecordDeleted(): ?int {
        return $this->recordDeleted;
    }

    /**
     * Gets the recordModifyDate property value. The recordModifyDate property
     * @return DateTime|null
    */
    public function getRecordModifyDate(): ?DateTime {
        return $this->recordModifyDate;
    }

    /**
     * Gets the revenueCodeId property value. The revenueCodeId property
     * @return int|null
    */
    public function getRevenueCodeId(): ?int {
        return $this->revenueCodeId;
    }

    /**
     * Gets the stockAllocationId property value. The stockAllocationId property
     * @return int|null
    */
    public function getStockAllocationId(): ?int {
        return $this->stockAllocationId;
    }

    /**
     * Gets the stockTrailId property value. The stockTrailId property
     * @return int|null
    */
    public function getStockTrailId(): ?int {
        return $this->stockTrailId;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeIntegerValue('auditTrailId', $this->getAuditTrailId());
        $writer->writeDateTimeValue('date', $this->getDate());
        $writer->writeIntegerValue('popItemId', $this->getPopItemId());
        $writer->writeDateTimeValue('recordCreateDate', $this->getRecordCreateDate());
        $writer->writeIntegerValue('recordDeleted', $this->getRecordDeleted());
        $writer->writeDateTimeValue('recordModifyDate', $this->getRecordModifyDate());
        $writer->writeIntegerValue('revenueCodeId', $this->getRevenueCodeId());
        $writer->writeIntegerValue('stockAllocationId', $this->getStockAllocationId());
        $writer->writeIntegerValue('stockTrailId', $this->getStockTrailId());
    }

    /**
     * Sets the auditTrailId property value. The auditTrailId property
     * @param int|null $value Value to set for the auditTrailId property.
    */
    public function setAuditTrailId(?int $value): void {
        $this->auditTrailId = $value;
    }

    /**
     * Sets the date property value. The date property
     * @param DateTime|null $value Value to set for the date property.
    */
    public function setDate(?DateTime $value): void {
        $this->date = $value;
    }

    /**
     * Sets the popItemId property value. The popItemId property
     * @param int|null $value Value to set for the popItemId property.
    */
    public function setPopItemId(?int $value): void {
        $this->popItemId = $value;
    }

    /**
     * Sets the recordCreateDate property value. The recordCreateDate property
     * @param DateTime|null $value Value to set for the recordCreateDate property.
    */
    public function setRecordCreateDate(?DateTime $value): void {
        $this->recordCreateDate = $value;
    }

    /**
     * Sets the recordDeleted property value. The recordDeleted property
     * @param int|null $value Value to set for the recordDeleted property.
    */
    public function setRecordDeleted(?int $value): void {
        $this->recordDeleted = $value;
    }

    /**
     * Sets the recordModifyDate property value. The recordModifyDate property
     * @param DateTime|null $value Value to set for the recordModifyDate property.
    */
    public function setRecordModifyDate(?DateTime $value): void {
        $this->recordModifyDate = $value;
    }

    /**
     * Sets the revenueCodeId property value. The revenueCodeId property
     * @param int|null $value Value to set for the revenueCodeId property.
    */
    public function setRevenueCodeId(?int $value): void {
        $this->revenueCodeId = $value;
    }

    /**
     * Sets the stockAllocationId property value. The stockAllocationId property
     * @param int|null $value Value to set for the stockAllocationId property.
    */
    public function setStockAllocationId(?int $value): void {
        $this->stockAllocationId = $value;
    }

    /**
     * Sets the stockTrailId property value. The stockTrailId property
     * @param int|null $value Value to set for the stockTrailId property.
    */
    public function setStockTrailId(?int $value): void {
        $this->stockTrailId = $value;
    }

}
