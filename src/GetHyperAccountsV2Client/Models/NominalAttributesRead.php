<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use DateTime;
use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class NominalAttributesRead implements Parsable
{
    /**
     * @var int|null $accountType The accountType property
    */
    private ?int $accountType = null;

    /**
     * @var float|null $balance The balance property
    */
    private ?float $balance = null;

    /**
     * @var float|null $balanceBf The balanceBf property
    */
    private ?float $balanceBf = null;

    /**
     * @var float|null $balanceFuture The balanceFuture property
    */
    private ?float $balanceFuture = null;

    /**
     * @var float|null $balanceMth1 The balanceMth1 property
    */
    private ?float $balanceMth1 = null;

    /**
     * @var float|null $balanceMth10 The balanceMth10 property
    */
    private ?float $balanceMth10 = null;

    /**
     * @var float|null $balanceMth11 The balanceMth11 property
    */
    private ?float $balanceMth11 = null;

    /**
     * @var float|null $balanceMth12 The balanceMth12 property
    */
    private ?float $balanceMth12 = null;

    /**
     * @var float|null $balanceMth2 The balanceMth2 property
    */
    private ?float $balanceMth2 = null;

    /**
     * @var float|null $balanceMth3 The balanceMth3 property
    */
    private ?float $balanceMth3 = null;

    /**
     * @var float|null $balanceMth4 The balanceMth4 property
    */
    private ?float $balanceMth4 = null;

    /**
     * @var float|null $balanceMth5 The balanceMth5 property
    */
    private ?float $balanceMth5 = null;

    /**
     * @var float|null $balanceMth6 The balanceMth6 property
    */
    private ?float $balanceMth6 = null;

    /**
     * @var float|null $balanceMth7 The balanceMth7 property
    */
    private ?float $balanceMth7 = null;

    /**
     * @var float|null $balanceMth8 The balanceMth8 property
    */
    private ?float $balanceMth8 = null;

    /**
     * @var float|null $balanceMth9 The balanceMth9 property
    */
    private ?float $balanceMth9 = null;

    /**
     * @var float|null $budgetMth1 The budgetMth1 property
    */
    private ?float $budgetMth1 = null;

    /**
     * @var float|null $budgetMth10 The budgetMth10 property
    */
    private ?float $budgetMth10 = null;

    /**
     * @var float|null $budgetMth11 The budgetMth11 property
    */
    private ?float $budgetMth11 = null;

    /**
     * @var float|null $budgetMth12 The budgetMth12 property
    */
    private ?float $budgetMth12 = null;

    /**
     * @var float|null $budgetMth2 The budgetMth2 property
    */
    private ?float $budgetMth2 = null;

    /**
     * @var float|null $budgetMth3 The budgetMth3 property
    */
    private ?float $budgetMth3 = null;

    /**
     * @var float|null $budgetMth4 The budgetMth4 property
    */
    private ?float $budgetMth4 = null;

    /**
     * @var float|null $budgetMth5 The budgetMth5 property
    */
    private ?float $budgetMth5 = null;

    /**
     * @var float|null $budgetMth6 The budgetMth6 property
    */
    private ?float $budgetMth6 = null;

    /**
     * @var float|null $budgetMth7 The budgetMth7 property
    */
    private ?float $budgetMth7 = null;

    /**
     * @var float|null $budgetMth8 The budgetMth8 property
    */
    private ?float $budgetMth8 = null;

    /**
     * @var float|null $budgetMth9 The budgetMth9 property
    */
    private ?float $budgetMth9 = null;

    /**
     * @var float|null $creditBf The creditBf property
    */
    private ?float $creditBf = null;

    /**
     * @var float|null $creditFuture The creditFuture property
    */
    private ?float $creditFuture = null;

    /**
     * @var float|null $creditMth1 The creditMth1 property
    */
    private ?float $creditMth1 = null;

    /**
     * @var float|null $creditMth10 The creditMth10 property
    */
    private ?float $creditMth10 = null;

    /**
     * @var float|null $creditMth11 The creditMth11 property
    */
    private ?float $creditMth11 = null;

    /**
     * @var float|null $creditMth12 The creditMth12 property
    */
    private ?float $creditMth12 = null;

    /**
     * @var float|null $creditMth2 The creditMth2 property
    */
    private ?float $creditMth2 = null;

    /**
     * @var float|null $creditMth3 The creditMth3 property
    */
    private ?float $creditMth3 = null;

    /**
     * @var float|null $creditMth4 The creditMth4 property
    */
    private ?float $creditMth4 = null;

    /**
     * @var float|null $creditMth5 The creditMth5 property
    */
    private ?float $creditMth5 = null;

    /**
     * @var float|null $creditMth6 The creditMth6 property
    */
    private ?float $creditMth6 = null;

    /**
     * @var float|null $creditMth7 The creditMth7 property
    */
    private ?float $creditMth7 = null;

    /**
     * @var float|null $creditMth8 The creditMth8 property
    */
    private ?float $creditMth8 = null;

    /**
     * @var float|null $creditMth9 The creditMth9 property
    */
    private ?float $creditMth9 = null;

    /**
     * @var float|null $debitBf The debitBf property
    */
    private ?float $debitBf = null;

    /**
     * @var float|null $debitFuture The debitFuture property
    */
    private ?float $debitFuture = null;

    /**
     * @var float|null $debitMth1 The debitMth1 property
    */
    private ?float $debitMth1 = null;

    /**
     * @var float|null $debitMth10 The debitMth10 property
    */
    private ?float $debitMth10 = null;

    /**
     * @var float|null $debitMth11 The debitMth11 property
    */
    private ?float $debitMth11 = null;

    /**
     * @var float|null $debitMth12 The debitMth12 property
    */
    private ?float $debitMth12 = null;

    /**
     * @var float|null $debitMth2 The debitMth2 property
    */
    private ?float $debitMth2 = null;

    /**
     * @var float|null $debitMth3 The debitMth3 property
    */
    private ?float $debitMth3 = null;

    /**
     * @var float|null $debitMth4 The debitMth4 property
    */
    private ?float $debitMth4 = null;

    /**
     * @var float|null $debitMth5 The debitMth5 property
    */
    private ?float $debitMth5 = null;

    /**
     * @var float|null $debitMth6 The debitMth6 property
    */
    private ?float $debitMth6 = null;

    /**
     * @var float|null $debitMth7 The debitMth7 property
    */
    private ?float $debitMth7 = null;

    /**
     * @var float|null $debitMth8 The debitMth8 property
    */
    private ?float $debitMth8 = null;

    /**
     * @var float|null $debitMth9 The debitMth9 property
    */
    private ?float $debitMth9 = null;

    /**
     * @var int|null $inactiveFlag The inactiveFlag property
    */
    private ?int $inactiveFlag = null;

    /**
     * @var string|null $name The name property
    */
    private ?string $name = null;

    /**
     * @var float|null $priorYr2Mth1 The priorYr2Mth1 property
    */
    private ?float $priorYr2Mth1 = null;

    /**
     * @var float|null $priorYr2Mth10 The priorYr2Mth10 property
    */
    private ?float $priorYr2Mth10 = null;

    /**
     * @var float|null $priorYr2Mth11 The priorYr2Mth11 property
    */
    private ?float $priorYr2Mth11 = null;

    /**
     * @var float|null $priorYr2Mth12 The priorYr2Mth12 property
    */
    private ?float $priorYr2Mth12 = null;

    /**
     * @var float|null $priorYr2Mth2 The priorYr2Mth2 property
    */
    private ?float $priorYr2Mth2 = null;

    /**
     * @var float|null $priorYr2Mth3 The priorYr2Mth3 property
    */
    private ?float $priorYr2Mth3 = null;

    /**
     * @var float|null $priorYr2Mth4 The priorYr2Mth4 property
    */
    private ?float $priorYr2Mth4 = null;

    /**
     * @var float|null $priorYr2Mth5 The priorYr2Mth5 property
    */
    private ?float $priorYr2Mth5 = null;

    /**
     * @var float|null $priorYr2Mth6 The priorYr2Mth6 property
    */
    private ?float $priorYr2Mth6 = null;

    /**
     * @var float|null $priorYr2Mth7 The priorYr2Mth7 property
    */
    private ?float $priorYr2Mth7 = null;

    /**
     * @var float|null $priorYr2Mth8 The priorYr2Mth8 property
    */
    private ?float $priorYr2Mth8 = null;

    /**
     * @var float|null $priorYr2Mth9 The priorYr2Mth9 property
    */
    private ?float $priorYr2Mth9 = null;

    /**
     * @var float|null $priorYr3Mth1 The priorYr3Mth1 property
    */
    private ?float $priorYr3Mth1 = null;

    /**
     * @var float|null $priorYr3Mth10 The priorYr3Mth10 property
    */
    private ?float $priorYr3Mth10 = null;

    /**
     * @var float|null $priorYr3Mth11 The priorYr3Mth11 property
    */
    private ?float $priorYr3Mth11 = null;

    /**
     * @var float|null $priorYr3Mth12 The priorYr3Mth12 property
    */
    private ?float $priorYr3Mth12 = null;

    /**
     * @var float|null $priorYr3Mth2 The priorYr3Mth2 property
    */
    private ?float $priorYr3Mth2 = null;

    /**
     * @var float|null $priorYr3Mth3 The priorYr3Mth3 property
    */
    private ?float $priorYr3Mth3 = null;

    /**
     * @var float|null $priorYr3Mth4 The priorYr3Mth4 property
    */
    private ?float $priorYr3Mth4 = null;

    /**
     * @var float|null $priorYr3Mth5 The priorYr3Mth5 property
    */
    private ?float $priorYr3Mth5 = null;

    /**
     * @var float|null $priorYr3Mth6 The priorYr3Mth6 property
    */
    private ?float $priorYr3Mth6 = null;

    /**
     * @var float|null $priorYr3Mth7 The priorYr3Mth7 property
    */
    private ?float $priorYr3Mth7 = null;

    /**
     * @var float|null $priorYr3Mth8 The priorYr3Mth8 property
    */
    private ?float $priorYr3Mth8 = null;

    /**
     * @var float|null $priorYr3Mth9 The priorYr3Mth9 property
    */
    private ?float $priorYr3Mth9 = null;

    /**
     * @var float|null $priorYr4Mth1 The priorYr4Mth1 property
    */
    private ?float $priorYr4Mth1 = null;

    /**
     * @var float|null $priorYr4Mth10 The priorYr4Mth10 property
    */
    private ?float $priorYr4Mth10 = null;

    /**
     * @var float|null $priorYr4Mth11 The priorYr4Mth11 property
    */
    private ?float $priorYr4Mth11 = null;

    /**
     * @var float|null $priorYr4Mth12 The priorYr4Mth12 property
    */
    private ?float $priorYr4Mth12 = null;

    /**
     * @var float|null $priorYr4Mth2 The priorYr4Mth2 property
    */
    private ?float $priorYr4Mth2 = null;

    /**
     * @var float|null $priorYr4Mth3 The priorYr4Mth3 property
    */
    private ?float $priorYr4Mth3 = null;

    /**
     * @var float|null $priorYr4Mth4 The priorYr4Mth4 property
    */
    private ?float $priorYr4Mth4 = null;

    /**
     * @var float|null $priorYr4Mth5 The priorYr4Mth5 property
    */
    private ?float $priorYr4Mth5 = null;

    /**
     * @var float|null $priorYr4Mth6 The priorYr4Mth6 property
    */
    private ?float $priorYr4Mth6 = null;

    /**
     * @var float|null $priorYr4Mth7 The priorYr4Mth7 property
    */
    private ?float $priorYr4Mth7 = null;

    /**
     * @var float|null $priorYr4Mth8 The priorYr4Mth8 property
    */
    private ?float $priorYr4Mth8 = null;

    /**
     * @var float|null $priorYr4Mth9 The priorYr4Mth9 property
    */
    private ?float $priorYr4Mth9 = null;

    /**
     * @var float|null $priorYr5Mth1 The priorYr5Mth1 property
    */
    private ?float $priorYr5Mth1 = null;

    /**
     * @var float|null $priorYr5Mth10 The priorYr5Mth10 property
    */
    private ?float $priorYr5Mth10 = null;

    /**
     * @var float|null $priorYr5Mth11 The priorYr5Mth11 property
    */
    private ?float $priorYr5Mth11 = null;

    /**
     * @var float|null $priorYr5Mth12 The priorYr5Mth12 property
    */
    private ?float $priorYr5Mth12 = null;

    /**
     * @var float|null $priorYr5Mth2 The priorYr5Mth2 property
    */
    private ?float $priorYr5Mth2 = null;

    /**
     * @var float|null $priorYr5Mth3 The priorYr5Mth3 property
    */
    private ?float $priorYr5Mth3 = null;

    /**
     * @var float|null $priorYr5Mth4 The priorYr5Mth4 property
    */
    private ?float $priorYr5Mth4 = null;

    /**
     * @var float|null $priorYr5Mth5 The priorYr5Mth5 property
    */
    private ?float $priorYr5Mth5 = null;

    /**
     * @var float|null $priorYr5Mth6 The priorYr5Mth6 property
    */
    private ?float $priorYr5Mth6 = null;

    /**
     * @var float|null $priorYr5Mth7 The priorYr5Mth7 property
    */
    private ?float $priorYr5Mth7 = null;

    /**
     * @var float|null $priorYr5Mth8 The priorYr5Mth8 property
    */
    private ?float $priorYr5Mth8 = null;

    /**
     * @var float|null $priorYr5Mth9 The priorYr5Mth9 property
    */
    private ?float $priorYr5Mth9 = null;

    /**
     * @var float|null $priorYrMth1 The priorYrMth1 property
    */
    private ?float $priorYrMth1 = null;

    /**
     * @var float|null $priorYrMth10 The priorYrMth10 property
    */
    private ?float $priorYrMth10 = null;

    /**
     * @var float|null $priorYrMth11 The priorYrMth11 property
    */
    private ?float $priorYrMth11 = null;

    /**
     * @var float|null $priorYrMth12 The priorYrMth12 property
    */
    private ?float $priorYrMth12 = null;

    /**
     * @var float|null $priorYrMth2 The priorYrMth2 property
    */
    private ?float $priorYrMth2 = null;

    /**
     * @var float|null $priorYrMth3 The priorYrMth3 property
    */
    private ?float $priorYrMth3 = null;

    /**
     * @var float|null $priorYrMth4 The priorYrMth4 property
    */
    private ?float $priorYrMth4 = null;

    /**
     * @var float|null $priorYrMth5 The priorYrMth5 property
    */
    private ?float $priorYrMth5 = null;

    /**
     * @var float|null $priorYrMth6 The priorYrMth6 property
    */
    private ?float $priorYrMth6 = null;

    /**
     * @var float|null $priorYrMth7 The priorYrMth7 property
    */
    private ?float $priorYrMth7 = null;

    /**
     * @var float|null $priorYrMth8 The priorYrMth8 property
    */
    private ?float $priorYrMth8 = null;

    /**
     * @var float|null $priorYrMth9 The priorYrMth9 property
    */
    private ?float $priorYrMth9 = null;

    /**
     * @var int|null $quickRatio The quickRatio property
    */
    private ?int $quickRatio = null;

    /**
     * @var DateTime|null $recordCreateDate The recordCreateDate property
    */
    private ?DateTime $recordCreateDate = null;

    /**
     * @var int|null $recordDeleted The recordDeleted property
    */
    private ?int $recordDeleted = null;

    /**
     * @var DateTime|null $recordModifyDate The recordModifyDate property
    */
    private ?DateTime $recordModifyDate = null;

    /**
     * @var int|null $sofaId The sofaId property
    */
    private ?int $sofaId = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return NominalAttributesRead
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): NominalAttributesRead {
        return new NominalAttributesRead();
    }

    /**
     * Gets the accountType property value. The accountType property
     * @return int|null
    */
    public function getAccountType(): ?int {
        return $this->accountType;
    }

    /**
     * Gets the balance property value. The balance property
     * @return float|null
    */
    public function getBalance(): ?float {
        return $this->balance;
    }

    /**
     * Gets the balanceBf property value. The balanceBf property
     * @return float|null
    */
    public function getBalanceBf(): ?float {
        return $this->balanceBf;
    }

    /**
     * Gets the balanceFuture property value. The balanceFuture property
     * @return float|null
    */
    public function getBalanceFuture(): ?float {
        return $this->balanceFuture;
    }

    /**
     * Gets the balanceMth1 property value. The balanceMth1 property
     * @return float|null
    */
    public function getBalanceMth1(): ?float {
        return $this->balanceMth1;
    }

    /**
     * Gets the balanceMth10 property value. The balanceMth10 property
     * @return float|null
    */
    public function getBalanceMth10(): ?float {
        return $this->balanceMth10;
    }

    /**
     * Gets the balanceMth11 property value. The balanceMth11 property
     * @return float|null
    */
    public function getBalanceMth11(): ?float {
        return $this->balanceMth11;
    }

    /**
     * Gets the balanceMth12 property value. The balanceMth12 property
     * @return float|null
    */
    public function getBalanceMth12(): ?float {
        return $this->balanceMth12;
    }

    /**
     * Gets the balanceMth2 property value. The balanceMth2 property
     * @return float|null
    */
    public function getBalanceMth2(): ?float {
        return $this->balanceMth2;
    }

    /**
     * Gets the balanceMth3 property value. The balanceMth3 property
     * @return float|null
    */
    public function getBalanceMth3(): ?float {
        return $this->balanceMth3;
    }

    /**
     * Gets the balanceMth4 property value. The balanceMth4 property
     * @return float|null
    */
    public function getBalanceMth4(): ?float {
        return $this->balanceMth4;
    }

    /**
     * Gets the balanceMth5 property value. The balanceMth5 property
     * @return float|null
    */
    public function getBalanceMth5(): ?float {
        return $this->balanceMth5;
    }

    /**
     * Gets the balanceMth6 property value. The balanceMth6 property
     * @return float|null
    */
    public function getBalanceMth6(): ?float {
        return $this->balanceMth6;
    }

    /**
     * Gets the balanceMth7 property value. The balanceMth7 property
     * @return float|null
    */
    public function getBalanceMth7(): ?float {
        return $this->balanceMth7;
    }

    /**
     * Gets the balanceMth8 property value. The balanceMth8 property
     * @return float|null
    */
    public function getBalanceMth8(): ?float {
        return $this->balanceMth8;
    }

    /**
     * Gets the balanceMth9 property value. The balanceMth9 property
     * @return float|null
    */
    public function getBalanceMth9(): ?float {
        return $this->balanceMth9;
    }

    /**
     * Gets the budgetMth1 property value. The budgetMth1 property
     * @return float|null
    */
    public function getBudgetMth1(): ?float {
        return $this->budgetMth1;
    }

    /**
     * Gets the budgetMth10 property value. The budgetMth10 property
     * @return float|null
    */
    public function getBudgetMth10(): ?float {
        return $this->budgetMth10;
    }

    /**
     * Gets the budgetMth11 property value. The budgetMth11 property
     * @return float|null
    */
    public function getBudgetMth11(): ?float {
        return $this->budgetMth11;
    }

    /**
     * Gets the budgetMth12 property value. The budgetMth12 property
     * @return float|null
    */
    public function getBudgetMth12(): ?float {
        return $this->budgetMth12;
    }

    /**
     * Gets the budgetMth2 property value. The budgetMth2 property
     * @return float|null
    */
    public function getBudgetMth2(): ?float {
        return $this->budgetMth2;
    }

    /**
     * Gets the budgetMth3 property value. The budgetMth3 property
     * @return float|null
    */
    public function getBudgetMth3(): ?float {
        return $this->budgetMth3;
    }

    /**
     * Gets the budgetMth4 property value. The budgetMth4 property
     * @return float|null
    */
    public function getBudgetMth4(): ?float {
        return $this->budgetMth4;
    }

    /**
     * Gets the budgetMth5 property value. The budgetMth5 property
     * @return float|null
    */
    public function getBudgetMth5(): ?float {
        return $this->budgetMth5;
    }

    /**
     * Gets the budgetMth6 property value. The budgetMth6 property
     * @return float|null
    */
    public function getBudgetMth6(): ?float {
        return $this->budgetMth6;
    }

    /**
     * Gets the budgetMth7 property value. The budgetMth7 property
     * @return float|null
    */
    public function getBudgetMth7(): ?float {
        return $this->budgetMth7;
    }

    /**
     * Gets the budgetMth8 property value. The budgetMth8 property
     * @return float|null
    */
    public function getBudgetMth8(): ?float {
        return $this->budgetMth8;
    }

    /**
     * Gets the budgetMth9 property value. The budgetMth9 property
     * @return float|null
    */
    public function getBudgetMth9(): ?float {
        return $this->budgetMth9;
    }

    /**
     * Gets the creditBf property value. The creditBf property
     * @return float|null
    */
    public function getCreditBf(): ?float {
        return $this->creditBf;
    }

    /**
     * Gets the creditFuture property value. The creditFuture property
     * @return float|null
    */
    public function getCreditFuture(): ?float {
        return $this->creditFuture;
    }

    /**
     * Gets the creditMth1 property value. The creditMth1 property
     * @return float|null
    */
    public function getCreditMth1(): ?float {
        return $this->creditMth1;
    }

    /**
     * Gets the creditMth10 property value. The creditMth10 property
     * @return float|null
    */
    public function getCreditMth10(): ?float {
        return $this->creditMth10;
    }

    /**
     * Gets the creditMth11 property value. The creditMth11 property
     * @return float|null
    */
    public function getCreditMth11(): ?float {
        return $this->creditMth11;
    }

    /**
     * Gets the creditMth12 property value. The creditMth12 property
     * @return float|null
    */
    public function getCreditMth12(): ?float {
        return $this->creditMth12;
    }

    /**
     * Gets the creditMth2 property value. The creditMth2 property
     * @return float|null
    */
    public function getCreditMth2(): ?float {
        return $this->creditMth2;
    }

    /**
     * Gets the creditMth3 property value. The creditMth3 property
     * @return float|null
    */
    public function getCreditMth3(): ?float {
        return $this->creditMth3;
    }

    /**
     * Gets the creditMth4 property value. The creditMth4 property
     * @return float|null
    */
    public function getCreditMth4(): ?float {
        return $this->creditMth4;
    }

    /**
     * Gets the creditMth5 property value. The creditMth5 property
     * @return float|null
    */
    public function getCreditMth5(): ?float {
        return $this->creditMth5;
    }

    /**
     * Gets the creditMth6 property value. The creditMth6 property
     * @return float|null
    */
    public function getCreditMth6(): ?float {
        return $this->creditMth6;
    }

    /**
     * Gets the creditMth7 property value. The creditMth7 property
     * @return float|null
    */
    public function getCreditMth7(): ?float {
        return $this->creditMth7;
    }

    /**
     * Gets the creditMth8 property value. The creditMth8 property
     * @return float|null
    */
    public function getCreditMth8(): ?float {
        return $this->creditMth8;
    }

    /**
     * Gets the creditMth9 property value. The creditMth9 property
     * @return float|null
    */
    public function getCreditMth9(): ?float {
        return $this->creditMth9;
    }

    /**
     * Gets the debitBf property value. The debitBf property
     * @return float|null
    */
    public function getDebitBf(): ?float {
        return $this->debitBf;
    }

    /**
     * Gets the debitFuture property value. The debitFuture property
     * @return float|null
    */
    public function getDebitFuture(): ?float {
        return $this->debitFuture;
    }

    /**
     * Gets the debitMth1 property value. The debitMth1 property
     * @return float|null
    */
    public function getDebitMth1(): ?float {
        return $this->debitMth1;
    }

    /**
     * Gets the debitMth10 property value. The debitMth10 property
     * @return float|null
    */
    public function getDebitMth10(): ?float {
        return $this->debitMth10;
    }

    /**
     * Gets the debitMth11 property value. The debitMth11 property
     * @return float|null
    */
    public function getDebitMth11(): ?float {
        return $this->debitMth11;
    }

    /**
     * Gets the debitMth12 property value. The debitMth12 property
     * @return float|null
    */
    public function getDebitMth12(): ?float {
        return $this->debitMth12;
    }

    /**
     * Gets the debitMth2 property value. The debitMth2 property
     * @return float|null
    */
    public function getDebitMth2(): ?float {
        return $this->debitMth2;
    }

    /**
     * Gets the debitMth3 property value. The debitMth3 property
     * @return float|null
    */
    public function getDebitMth3(): ?float {
        return $this->debitMth3;
    }

    /**
     * Gets the debitMth4 property value. The debitMth4 property
     * @return float|null
    */
    public function getDebitMth4(): ?float {
        return $this->debitMth4;
    }

    /**
     * Gets the debitMth5 property value. The debitMth5 property
     * @return float|null
    */
    public function getDebitMth5(): ?float {
        return $this->debitMth5;
    }

    /**
     * Gets the debitMth6 property value. The debitMth6 property
     * @return float|null
    */
    public function getDebitMth6(): ?float {
        return $this->debitMth6;
    }

    /**
     * Gets the debitMth7 property value. The debitMth7 property
     * @return float|null
    */
    public function getDebitMth7(): ?float {
        return $this->debitMth7;
    }

    /**
     * Gets the debitMth8 property value. The debitMth8 property
     * @return float|null
    */
    public function getDebitMth8(): ?float {
        return $this->debitMth8;
    }

    /**
     * Gets the debitMth9 property value. The debitMth9 property
     * @return float|null
    */
    public function getDebitMth9(): ?float {
        return $this->debitMth9;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'accountType' => fn(ParseNode $n) => $o->setAccountType($n->getIntegerValue()),
            'balance' => fn(ParseNode $n) => $o->setBalance($n->getFloatValue()),
            'balanceBf' => fn(ParseNode $n) => $o->setBalanceBf($n->getFloatValue()),
            'balanceFuture' => fn(ParseNode $n) => $o->setBalanceFuture($n->getFloatValue()),
            'balanceMth1' => fn(ParseNode $n) => $o->setBalanceMth1($n->getFloatValue()),
            'balanceMth10' => fn(ParseNode $n) => $o->setBalanceMth10($n->getFloatValue()),
            'balanceMth11' => fn(ParseNode $n) => $o->setBalanceMth11($n->getFloatValue()),
            'balanceMth12' => fn(ParseNode $n) => $o->setBalanceMth12($n->getFloatValue()),
            'balanceMth2' => fn(ParseNode $n) => $o->setBalanceMth2($n->getFloatValue()),
            'balanceMth3' => fn(ParseNode $n) => $o->setBalanceMth3($n->getFloatValue()),
            'balanceMth4' => fn(ParseNode $n) => $o->setBalanceMth4($n->getFloatValue()),
            'balanceMth5' => fn(ParseNode $n) => $o->setBalanceMth5($n->getFloatValue()),
            'balanceMth6' => fn(ParseNode $n) => $o->setBalanceMth6($n->getFloatValue()),
            'balanceMth7' => fn(ParseNode $n) => $o->setBalanceMth7($n->getFloatValue()),
            'balanceMth8' => fn(ParseNode $n) => $o->setBalanceMth8($n->getFloatValue()),
            'balanceMth9' => fn(ParseNode $n) => $o->setBalanceMth9($n->getFloatValue()),
            'budgetMth1' => fn(ParseNode $n) => $o->setBudgetMth1($n->getFloatValue()),
            'budgetMth10' => fn(ParseNode $n) => $o->setBudgetMth10($n->getFloatValue()),
            'budgetMth11' => fn(ParseNode $n) => $o->setBudgetMth11($n->getFloatValue()),
            'budgetMth12' => fn(ParseNode $n) => $o->setBudgetMth12($n->getFloatValue()),
            'budgetMth2' => fn(ParseNode $n) => $o->setBudgetMth2($n->getFloatValue()),
            'budgetMth3' => fn(ParseNode $n) => $o->setBudgetMth3($n->getFloatValue()),
            'budgetMth4' => fn(ParseNode $n) => $o->setBudgetMth4($n->getFloatValue()),
            'budgetMth5' => fn(ParseNode $n) => $o->setBudgetMth5($n->getFloatValue()),
            'budgetMth6' => fn(ParseNode $n) => $o->setBudgetMth6($n->getFloatValue()),
            'budgetMth7' => fn(ParseNode $n) => $o->setBudgetMth7($n->getFloatValue()),
            'budgetMth8' => fn(ParseNode $n) => $o->setBudgetMth8($n->getFloatValue()),
            'budgetMth9' => fn(ParseNode $n) => $o->setBudgetMth9($n->getFloatValue()),
            'creditBf' => fn(ParseNode $n) => $o->setCreditBf($n->getFloatValue()),
            'creditFuture' => fn(ParseNode $n) => $o->setCreditFuture($n->getFloatValue()),
            'creditMth1' => fn(ParseNode $n) => $o->setCreditMth1($n->getFloatValue()),
            'creditMth10' => fn(ParseNode $n) => $o->setCreditMth10($n->getFloatValue()),
            'creditMth11' => fn(ParseNode $n) => $o->setCreditMth11($n->getFloatValue()),
            'creditMth12' => fn(ParseNode $n) => $o->setCreditMth12($n->getFloatValue()),
            'creditMth2' => fn(ParseNode $n) => $o->setCreditMth2($n->getFloatValue()),
            'creditMth3' => fn(ParseNode $n) => $o->setCreditMth3($n->getFloatValue()),
            'creditMth4' => fn(ParseNode $n) => $o->setCreditMth4($n->getFloatValue()),
            'creditMth5' => fn(ParseNode $n) => $o->setCreditMth5($n->getFloatValue()),
            'creditMth6' => fn(ParseNode $n) => $o->setCreditMth6($n->getFloatValue()),
            'creditMth7' => fn(ParseNode $n) => $o->setCreditMth7($n->getFloatValue()),
            'creditMth8' => fn(ParseNode $n) => $o->setCreditMth8($n->getFloatValue()),
            'creditMth9' => fn(ParseNode $n) => $o->setCreditMth9($n->getFloatValue()),
            'debitBf' => fn(ParseNode $n) => $o->setDebitBf($n->getFloatValue()),
            'debitFuture' => fn(ParseNode $n) => $o->setDebitFuture($n->getFloatValue()),
            'debitMth1' => fn(ParseNode $n) => $o->setDebitMth1($n->getFloatValue()),
            'debitMth10' => fn(ParseNode $n) => $o->setDebitMth10($n->getFloatValue()),
            'debitMth11' => fn(ParseNode $n) => $o->setDebitMth11($n->getFloatValue()),
            'debitMth12' => fn(ParseNode $n) => $o->setDebitMth12($n->getFloatValue()),
            'debitMth2' => fn(ParseNode $n) => $o->setDebitMth2($n->getFloatValue()),
            'debitMth3' => fn(ParseNode $n) => $o->setDebitMth3($n->getFloatValue()),
            'debitMth4' => fn(ParseNode $n) => $o->setDebitMth4($n->getFloatValue()),
            'debitMth5' => fn(ParseNode $n) => $o->setDebitMth5($n->getFloatValue()),
            'debitMth6' => fn(ParseNode $n) => $o->setDebitMth6($n->getFloatValue()),
            'debitMth7' => fn(ParseNode $n) => $o->setDebitMth7($n->getFloatValue()),
            'debitMth8' => fn(ParseNode $n) => $o->setDebitMth8($n->getFloatValue()),
            'debitMth9' => fn(ParseNode $n) => $o->setDebitMth9($n->getFloatValue()),
            'inactiveFlag' => fn(ParseNode $n) => $o->setInactiveFlag($n->getIntegerValue()),
            'name' => fn(ParseNode $n) => $o->setName($n->getStringValue()),
            'priorYr2Mth1' => fn(ParseNode $n) => $o->setPriorYr2Mth1($n->getFloatValue()),
            'priorYr2Mth10' => fn(ParseNode $n) => $o->setPriorYr2Mth10($n->getFloatValue()),
            'priorYr2Mth11' => fn(ParseNode $n) => $o->setPriorYr2Mth11($n->getFloatValue()),
            'priorYr2Mth12' => fn(ParseNode $n) => $o->setPriorYr2Mth12($n->getFloatValue()),
            'priorYr2Mth2' => fn(ParseNode $n) => $o->setPriorYr2Mth2($n->getFloatValue()),
            'priorYr2Mth3' => fn(ParseNode $n) => $o->setPriorYr2Mth3($n->getFloatValue()),
            'priorYr2Mth4' => fn(ParseNode $n) => $o->setPriorYr2Mth4($n->getFloatValue()),
            'priorYr2Mth5' => fn(ParseNode $n) => $o->setPriorYr2Mth5($n->getFloatValue()),
            'priorYr2Mth6' => fn(ParseNode $n) => $o->setPriorYr2Mth6($n->getFloatValue()),
            'priorYr2Mth7' => fn(ParseNode $n) => $o->setPriorYr2Mth7($n->getFloatValue()),
            'priorYr2Mth8' => fn(ParseNode $n) => $o->setPriorYr2Mth8($n->getFloatValue()),
            'priorYr2Mth9' => fn(ParseNode $n) => $o->setPriorYr2Mth9($n->getFloatValue()),
            'priorYr3Mth1' => fn(ParseNode $n) => $o->setPriorYr3Mth1($n->getFloatValue()),
            'priorYr3Mth10' => fn(ParseNode $n) => $o->setPriorYr3Mth10($n->getFloatValue()),
            'priorYr3Mth11' => fn(ParseNode $n) => $o->setPriorYr3Mth11($n->getFloatValue()),
            'priorYr3Mth12' => fn(ParseNode $n) => $o->setPriorYr3Mth12($n->getFloatValue()),
            'priorYr3Mth2' => fn(ParseNode $n) => $o->setPriorYr3Mth2($n->getFloatValue()),
            'priorYr3Mth3' => fn(ParseNode $n) => $o->setPriorYr3Mth3($n->getFloatValue()),
            'priorYr3Mth4' => fn(ParseNode $n) => $o->setPriorYr3Mth4($n->getFloatValue()),
            'priorYr3Mth5' => fn(ParseNode $n) => $o->setPriorYr3Mth5($n->getFloatValue()),
            'priorYr3Mth6' => fn(ParseNode $n) => $o->setPriorYr3Mth6($n->getFloatValue()),
            'priorYr3Mth7' => fn(ParseNode $n) => $o->setPriorYr3Mth7($n->getFloatValue()),
            'priorYr3Mth8' => fn(ParseNode $n) => $o->setPriorYr3Mth8($n->getFloatValue()),
            'priorYr3Mth9' => fn(ParseNode $n) => $o->setPriorYr3Mth9($n->getFloatValue()),
            'priorYr4Mth1' => fn(ParseNode $n) => $o->setPriorYr4Mth1($n->getFloatValue()),
            'priorYr4Mth10' => fn(ParseNode $n) => $o->setPriorYr4Mth10($n->getFloatValue()),
            'priorYr4Mth11' => fn(ParseNode $n) => $o->setPriorYr4Mth11($n->getFloatValue()),
            'priorYr4Mth12' => fn(ParseNode $n) => $o->setPriorYr4Mth12($n->getFloatValue()),
            'priorYr4Mth2' => fn(ParseNode $n) => $o->setPriorYr4Mth2($n->getFloatValue()),
            'priorYr4Mth3' => fn(ParseNode $n) => $o->setPriorYr4Mth3($n->getFloatValue()),
            'priorYr4Mth4' => fn(ParseNode $n) => $o->setPriorYr4Mth4($n->getFloatValue()),
            'priorYr4Mth5' => fn(ParseNode $n) => $o->setPriorYr4Mth5($n->getFloatValue()),
            'priorYr4Mth6' => fn(ParseNode $n) => $o->setPriorYr4Mth6($n->getFloatValue()),
            'priorYr4Mth7' => fn(ParseNode $n) => $o->setPriorYr4Mth7($n->getFloatValue()),
            'priorYr4Mth8' => fn(ParseNode $n) => $o->setPriorYr4Mth8($n->getFloatValue()),
            'priorYr4Mth9' => fn(ParseNode $n) => $o->setPriorYr4Mth9($n->getFloatValue()),
            'priorYr5Mth1' => fn(ParseNode $n) => $o->setPriorYr5Mth1($n->getFloatValue()),
            'priorYr5Mth10' => fn(ParseNode $n) => $o->setPriorYr5Mth10($n->getFloatValue()),
            'priorYr5Mth11' => fn(ParseNode $n) => $o->setPriorYr5Mth11($n->getFloatValue()),
            'priorYr5Mth12' => fn(ParseNode $n) => $o->setPriorYr5Mth12($n->getFloatValue()),
            'priorYr5Mth2' => fn(ParseNode $n) => $o->setPriorYr5Mth2($n->getFloatValue()),
            'priorYr5Mth3' => fn(ParseNode $n) => $o->setPriorYr5Mth3($n->getFloatValue()),
            'priorYr5Mth4' => fn(ParseNode $n) => $o->setPriorYr5Mth4($n->getFloatValue()),
            'priorYr5Mth5' => fn(ParseNode $n) => $o->setPriorYr5Mth5($n->getFloatValue()),
            'priorYr5Mth6' => fn(ParseNode $n) => $o->setPriorYr5Mth6($n->getFloatValue()),
            'priorYr5Mth7' => fn(ParseNode $n) => $o->setPriorYr5Mth7($n->getFloatValue()),
            'priorYr5Mth8' => fn(ParseNode $n) => $o->setPriorYr5Mth8($n->getFloatValue()),
            'priorYr5Mth9' => fn(ParseNode $n) => $o->setPriorYr5Mth9($n->getFloatValue()),
            'priorYrMth1' => fn(ParseNode $n) => $o->setPriorYrMth1($n->getFloatValue()),
            'priorYrMth10' => fn(ParseNode $n) => $o->setPriorYrMth10($n->getFloatValue()),
            'priorYrMth11' => fn(ParseNode $n) => $o->setPriorYrMth11($n->getFloatValue()),
            'priorYrMth12' => fn(ParseNode $n) => $o->setPriorYrMth12($n->getFloatValue()),
            'priorYrMth2' => fn(ParseNode $n) => $o->setPriorYrMth2($n->getFloatValue()),
            'priorYrMth3' => fn(ParseNode $n) => $o->setPriorYrMth3($n->getFloatValue()),
            'priorYrMth4' => fn(ParseNode $n) => $o->setPriorYrMth4($n->getFloatValue()),
            'priorYrMth5' => fn(ParseNode $n) => $o->setPriorYrMth5($n->getFloatValue()),
            'priorYrMth6' => fn(ParseNode $n) => $o->setPriorYrMth6($n->getFloatValue()),
            'priorYrMth7' => fn(ParseNode $n) => $o->setPriorYrMth7($n->getFloatValue()),
            'priorYrMth8' => fn(ParseNode $n) => $o->setPriorYrMth8($n->getFloatValue()),
            'priorYrMth9' => fn(ParseNode $n) => $o->setPriorYrMth9($n->getFloatValue()),
            'quickRatio' => fn(ParseNode $n) => $o->setQuickRatio($n->getIntegerValue()),
            'recordCreateDate' => fn(ParseNode $n) => $o->setRecordCreateDate($n->getDateTimeValue()),
            'recordDeleted' => fn(ParseNode $n) => $o->setRecordDeleted($n->getIntegerValue()),
            'recordModifyDate' => fn(ParseNode $n) => $o->setRecordModifyDate($n->getDateTimeValue()),
            'sofaId' => fn(ParseNode $n) => $o->setSofaId($n->getIntegerValue()),
        ];
    }

    /**
     * Gets the inactiveFlag property value. The inactiveFlag property
     * @return int|null
    */
    public function getInactiveFlag(): ?int {
        return $this->inactiveFlag;
    }

    /**
     * Gets the name property value. The name property
     * @return string|null
    */
    public function getName(): ?string {
        return $this->name;
    }

    /**
     * Gets the priorYr2Mth1 property value. The priorYr2Mth1 property
     * @return float|null
    */
    public function getPriorYr2Mth1(): ?float {
        return $this->priorYr2Mth1;
    }

    /**
     * Gets the priorYr2Mth10 property value. The priorYr2Mth10 property
     * @return float|null
    */
    public function getPriorYr2Mth10(): ?float {
        return $this->priorYr2Mth10;
    }

    /**
     * Gets the priorYr2Mth11 property value. The priorYr2Mth11 property
     * @return float|null
    */
    public function getPriorYr2Mth11(): ?float {
        return $this->priorYr2Mth11;
    }

    /**
     * Gets the priorYr2Mth12 property value. The priorYr2Mth12 property
     * @return float|null
    */
    public function getPriorYr2Mth12(): ?float {
        return $this->priorYr2Mth12;
    }

    /**
     * Gets the priorYr2Mth2 property value. The priorYr2Mth2 property
     * @return float|null
    */
    public function getPriorYr2Mth2(): ?float {
        return $this->priorYr2Mth2;
    }

    /**
     * Gets the priorYr2Mth3 property value. The priorYr2Mth3 property
     * @return float|null
    */
    public function getPriorYr2Mth3(): ?float {
        return $this->priorYr2Mth3;
    }

    /**
     * Gets the priorYr2Mth4 property value. The priorYr2Mth4 property
     * @return float|null
    */
    public function getPriorYr2Mth4(): ?float {
        return $this->priorYr2Mth4;
    }

    /**
     * Gets the priorYr2Mth5 property value. The priorYr2Mth5 property
     * @return float|null
    */
    public function getPriorYr2Mth5(): ?float {
        return $this->priorYr2Mth5;
    }

    /**
     * Gets the priorYr2Mth6 property value. The priorYr2Mth6 property
     * @return float|null
    */
    public function getPriorYr2Mth6(): ?float {
        return $this->priorYr2Mth6;
    }

    /**
     * Gets the priorYr2Mth7 property value. The priorYr2Mth7 property
     * @return float|null
    */
    public function getPriorYr2Mth7(): ?float {
        return $this->priorYr2Mth7;
    }

    /**
     * Gets the priorYr2Mth8 property value. The priorYr2Mth8 property
     * @return float|null
    */
    public function getPriorYr2Mth8(): ?float {
        return $this->priorYr2Mth8;
    }

    /**
     * Gets the priorYr2Mth9 property value. The priorYr2Mth9 property
     * @return float|null
    */
    public function getPriorYr2Mth9(): ?float {
        return $this->priorYr2Mth9;
    }

    /**
     * Gets the priorYr3Mth1 property value. The priorYr3Mth1 property
     * @return float|null
    */
    public function getPriorYr3Mth1(): ?float {
        return $this->priorYr3Mth1;
    }

    /**
     * Gets the priorYr3Mth10 property value. The priorYr3Mth10 property
     * @return float|null
    */
    public function getPriorYr3Mth10(): ?float {
        return $this->priorYr3Mth10;
    }

    /**
     * Gets the priorYr3Mth11 property value. The priorYr3Mth11 property
     * @return float|null
    */
    public function getPriorYr3Mth11(): ?float {
        return $this->priorYr3Mth11;
    }

    /**
     * Gets the priorYr3Mth12 property value. The priorYr3Mth12 property
     * @return float|null
    */
    public function getPriorYr3Mth12(): ?float {
        return $this->priorYr3Mth12;
    }

    /**
     * Gets the priorYr3Mth2 property value. The priorYr3Mth2 property
     * @return float|null
    */
    public function getPriorYr3Mth2(): ?float {
        return $this->priorYr3Mth2;
    }

    /**
     * Gets the priorYr3Mth3 property value. The priorYr3Mth3 property
     * @return float|null
    */
    public function getPriorYr3Mth3(): ?float {
        return $this->priorYr3Mth3;
    }

    /**
     * Gets the priorYr3Mth4 property value. The priorYr3Mth4 property
     * @return float|null
    */
    public function getPriorYr3Mth4(): ?float {
        return $this->priorYr3Mth4;
    }

    /**
     * Gets the priorYr3Mth5 property value. The priorYr3Mth5 property
     * @return float|null
    */
    public function getPriorYr3Mth5(): ?float {
        return $this->priorYr3Mth5;
    }

    /**
     * Gets the priorYr3Mth6 property value. The priorYr3Mth6 property
     * @return float|null
    */
    public function getPriorYr3Mth6(): ?float {
        return $this->priorYr3Mth6;
    }

    /**
     * Gets the priorYr3Mth7 property value. The priorYr3Mth7 property
     * @return float|null
    */
    public function getPriorYr3Mth7(): ?float {
        return $this->priorYr3Mth7;
    }

    /**
     * Gets the priorYr3Mth8 property value. The priorYr3Mth8 property
     * @return float|null
    */
    public function getPriorYr3Mth8(): ?float {
        return $this->priorYr3Mth8;
    }

    /**
     * Gets the priorYr3Mth9 property value. The priorYr3Mth9 property
     * @return float|null
    */
    public function getPriorYr3Mth9(): ?float {
        return $this->priorYr3Mth9;
    }

    /**
     * Gets the priorYr4Mth1 property value. The priorYr4Mth1 property
     * @return float|null
    */
    public function getPriorYr4Mth1(): ?float {
        return $this->priorYr4Mth1;
    }

    /**
     * Gets the priorYr4Mth10 property value. The priorYr4Mth10 property
     * @return float|null
    */
    public function getPriorYr4Mth10(): ?float {
        return $this->priorYr4Mth10;
    }

    /**
     * Gets the priorYr4Mth11 property value. The priorYr4Mth11 property
     * @return float|null
    */
    public function getPriorYr4Mth11(): ?float {
        return $this->priorYr4Mth11;
    }

    /**
     * Gets the priorYr4Mth12 property value. The priorYr4Mth12 property
     * @return float|null
    */
    public function getPriorYr4Mth12(): ?float {
        return $this->priorYr4Mth12;
    }

    /**
     * Gets the priorYr4Mth2 property value. The priorYr4Mth2 property
     * @return float|null
    */
    public function getPriorYr4Mth2(): ?float {
        return $this->priorYr4Mth2;
    }

    /**
     * Gets the priorYr4Mth3 property value. The priorYr4Mth3 property
     * @return float|null
    */
    public function getPriorYr4Mth3(): ?float {
        return $this->priorYr4Mth3;
    }

    /**
     * Gets the priorYr4Mth4 property value. The priorYr4Mth4 property
     * @return float|null
    */
    public function getPriorYr4Mth4(): ?float {
        return $this->priorYr4Mth4;
    }

    /**
     * Gets the priorYr4Mth5 property value. The priorYr4Mth5 property
     * @return float|null
    */
    public function getPriorYr4Mth5(): ?float {
        return $this->priorYr4Mth5;
    }

    /**
     * Gets the priorYr4Mth6 property value. The priorYr4Mth6 property
     * @return float|null
    */
    public function getPriorYr4Mth6(): ?float {
        return $this->priorYr4Mth6;
    }

    /**
     * Gets the priorYr4Mth7 property value. The priorYr4Mth7 property
     * @return float|null
    */
    public function getPriorYr4Mth7(): ?float {
        return $this->priorYr4Mth7;
    }

    /**
     * Gets the priorYr4Mth8 property value. The priorYr4Mth8 property
     * @return float|null
    */
    public function getPriorYr4Mth8(): ?float {
        return $this->priorYr4Mth8;
    }

    /**
     * Gets the priorYr4Mth9 property value. The priorYr4Mth9 property
     * @return float|null
    */
    public function getPriorYr4Mth9(): ?float {
        return $this->priorYr4Mth9;
    }

    /**
     * Gets the priorYr5Mth1 property value. The priorYr5Mth1 property
     * @return float|null
    */
    public function getPriorYr5Mth1(): ?float {
        return $this->priorYr5Mth1;
    }

    /**
     * Gets the priorYr5Mth10 property value. The priorYr5Mth10 property
     * @return float|null
    */
    public function getPriorYr5Mth10(): ?float {
        return $this->priorYr5Mth10;
    }

    /**
     * Gets the priorYr5Mth11 property value. The priorYr5Mth11 property
     * @return float|null
    */
    public function getPriorYr5Mth11(): ?float {
        return $this->priorYr5Mth11;
    }

    /**
     * Gets the priorYr5Mth12 property value. The priorYr5Mth12 property
     * @return float|null
    */
    public function getPriorYr5Mth12(): ?float {
        return $this->priorYr5Mth12;
    }

    /**
     * Gets the priorYr5Mth2 property value. The priorYr5Mth2 property
     * @return float|null
    */
    public function getPriorYr5Mth2(): ?float {
        return $this->priorYr5Mth2;
    }

    /**
     * Gets the priorYr5Mth3 property value. The priorYr5Mth3 property
     * @return float|null
    */
    public function getPriorYr5Mth3(): ?float {
        return $this->priorYr5Mth3;
    }

    /**
     * Gets the priorYr5Mth4 property value. The priorYr5Mth4 property
     * @return float|null
    */
    public function getPriorYr5Mth4(): ?float {
        return $this->priorYr5Mth4;
    }

    /**
     * Gets the priorYr5Mth5 property value. The priorYr5Mth5 property
     * @return float|null
    */
    public function getPriorYr5Mth5(): ?float {
        return $this->priorYr5Mth5;
    }

    /**
     * Gets the priorYr5Mth6 property value. The priorYr5Mth6 property
     * @return float|null
    */
    public function getPriorYr5Mth6(): ?float {
        return $this->priorYr5Mth6;
    }

    /**
     * Gets the priorYr5Mth7 property value. The priorYr5Mth7 property
     * @return float|null
    */
    public function getPriorYr5Mth7(): ?float {
        return $this->priorYr5Mth7;
    }

    /**
     * Gets the priorYr5Mth8 property value. The priorYr5Mth8 property
     * @return float|null
    */
    public function getPriorYr5Mth8(): ?float {
        return $this->priorYr5Mth8;
    }

    /**
     * Gets the priorYr5Mth9 property value. The priorYr5Mth9 property
     * @return float|null
    */
    public function getPriorYr5Mth9(): ?float {
        return $this->priorYr5Mth9;
    }

    /**
     * Gets the priorYrMth1 property value. The priorYrMth1 property
     * @return float|null
    */
    public function getPriorYrMth1(): ?float {
        return $this->priorYrMth1;
    }

    /**
     * Gets the priorYrMth10 property value. The priorYrMth10 property
     * @return float|null
    */
    public function getPriorYrMth10(): ?float {
        return $this->priorYrMth10;
    }

    /**
     * Gets the priorYrMth11 property value. The priorYrMth11 property
     * @return float|null
    */
    public function getPriorYrMth11(): ?float {
        return $this->priorYrMth11;
    }

    /**
     * Gets the priorYrMth12 property value. The priorYrMth12 property
     * @return float|null
    */
    public function getPriorYrMth12(): ?float {
        return $this->priorYrMth12;
    }

    /**
     * Gets the priorYrMth2 property value. The priorYrMth2 property
     * @return float|null
    */
    public function getPriorYrMth2(): ?float {
        return $this->priorYrMth2;
    }

    /**
     * Gets the priorYrMth3 property value. The priorYrMth3 property
     * @return float|null
    */
    public function getPriorYrMth3(): ?float {
        return $this->priorYrMth3;
    }

    /**
     * Gets the priorYrMth4 property value. The priorYrMth4 property
     * @return float|null
    */
    public function getPriorYrMth4(): ?float {
        return $this->priorYrMth4;
    }

    /**
     * Gets the priorYrMth5 property value. The priorYrMth5 property
     * @return float|null
    */
    public function getPriorYrMth5(): ?float {
        return $this->priorYrMth5;
    }

    /**
     * Gets the priorYrMth6 property value. The priorYrMth6 property
     * @return float|null
    */
    public function getPriorYrMth6(): ?float {
        return $this->priorYrMth6;
    }

    /**
     * Gets the priorYrMth7 property value. The priorYrMth7 property
     * @return float|null
    */
    public function getPriorYrMth7(): ?float {
        return $this->priorYrMth7;
    }

    /**
     * Gets the priorYrMth8 property value. The priorYrMth8 property
     * @return float|null
    */
    public function getPriorYrMth8(): ?float {
        return $this->priorYrMth8;
    }

    /**
     * Gets the priorYrMth9 property value. The priorYrMth9 property
     * @return float|null
    */
    public function getPriorYrMth9(): ?float {
        return $this->priorYrMth9;
    }

    /**
     * Gets the quickRatio property value. The quickRatio property
     * @return int|null
    */
    public function getQuickRatio(): ?int {
        return $this->quickRatio;
    }

    /**
     * Gets the recordCreateDate property value. The recordCreateDate property
     * @return DateTime|null
    */
    public function getRecordCreateDate(): ?DateTime {
        return $this->recordCreateDate;
    }

    /**
     * Gets the recordDeleted property value. The recordDeleted property
     * @return int|null
    */
    public function getRecordDeleted(): ?int {
        return $this->recordDeleted;
    }

    /**
     * Gets the recordModifyDate property value. The recordModifyDate property
     * @return DateTime|null
    */
    public function getRecordModifyDate(): ?DateTime {
        return $this->recordModifyDate;
    }

    /**
     * Gets the sofaId property value. The sofaId property
     * @return int|null
    */
    public function getSofaId(): ?int {
        return $this->sofaId;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeIntegerValue('accountType', $this->getAccountType());
        $writer->writeFloatValue('balance', $this->getBalance());
        $writer->writeFloatValue('balanceBf', $this->getBalanceBf());
        $writer->writeFloatValue('balanceFuture', $this->getBalanceFuture());
        $writer->writeFloatValue('balanceMth1', $this->getBalanceMth1());
        $writer->writeFloatValue('balanceMth10', $this->getBalanceMth10());
        $writer->writeFloatValue('balanceMth11', $this->getBalanceMth11());
        $writer->writeFloatValue('balanceMth12', $this->getBalanceMth12());
        $writer->writeFloatValue('balanceMth2', $this->getBalanceMth2());
        $writer->writeFloatValue('balanceMth3', $this->getBalanceMth3());
        $writer->writeFloatValue('balanceMth4', $this->getBalanceMth4());
        $writer->writeFloatValue('balanceMth5', $this->getBalanceMth5());
        $writer->writeFloatValue('balanceMth6', $this->getBalanceMth6());
        $writer->writeFloatValue('balanceMth7', $this->getBalanceMth7());
        $writer->writeFloatValue('balanceMth8', $this->getBalanceMth8());
        $writer->writeFloatValue('balanceMth9', $this->getBalanceMth9());
        $writer->writeFloatValue('budgetMth1', $this->getBudgetMth1());
        $writer->writeFloatValue('budgetMth10', $this->getBudgetMth10());
        $writer->writeFloatValue('budgetMth11', $this->getBudgetMth11());
        $writer->writeFloatValue('budgetMth12', $this->getBudgetMth12());
        $writer->writeFloatValue('budgetMth2', $this->getBudgetMth2());
        $writer->writeFloatValue('budgetMth3', $this->getBudgetMth3());
        $writer->writeFloatValue('budgetMth4', $this->getBudgetMth4());
        $writer->writeFloatValue('budgetMth5', $this->getBudgetMth5());
        $writer->writeFloatValue('budgetMth6', $this->getBudgetMth6());
        $writer->writeFloatValue('budgetMth7', $this->getBudgetMth7());
        $writer->writeFloatValue('budgetMth8', $this->getBudgetMth8());
        $writer->writeFloatValue('budgetMth9', $this->getBudgetMth9());
        $writer->writeFloatValue('creditBf', $this->getCreditBf());
        $writer->writeFloatValue('creditFuture', $this->getCreditFuture());
        $writer->writeFloatValue('creditMth1', $this->getCreditMth1());
        $writer->writeFloatValue('creditMth10', $this->getCreditMth10());
        $writer->writeFloatValue('creditMth11', $this->getCreditMth11());
        $writer->writeFloatValue('creditMth12', $this->getCreditMth12());
        $writer->writeFloatValue('creditMth2', $this->getCreditMth2());
        $writer->writeFloatValue('creditMth3', $this->getCreditMth3());
        $writer->writeFloatValue('creditMth4', $this->getCreditMth4());
        $writer->writeFloatValue('creditMth5', $this->getCreditMth5());
        $writer->writeFloatValue('creditMth6', $this->getCreditMth6());
        $writer->writeFloatValue('creditMth7', $this->getCreditMth7());
        $writer->writeFloatValue('creditMth8', $this->getCreditMth8());
        $writer->writeFloatValue('creditMth9', $this->getCreditMth9());
        $writer->writeFloatValue('debitBf', $this->getDebitBf());
        $writer->writeFloatValue('debitFuture', $this->getDebitFuture());
        $writer->writeFloatValue('debitMth1', $this->getDebitMth1());
        $writer->writeFloatValue('debitMth10', $this->getDebitMth10());
        $writer->writeFloatValue('debitMth11', $this->getDebitMth11());
        $writer->writeFloatValue('debitMth12', $this->getDebitMth12());
        $writer->writeFloatValue('debitMth2', $this->getDebitMth2());
        $writer->writeFloatValue('debitMth3', $this->getDebitMth3());
        $writer->writeFloatValue('debitMth4', $this->getDebitMth4());
        $writer->writeFloatValue('debitMth5', $this->getDebitMth5());
        $writer->writeFloatValue('debitMth6', $this->getDebitMth6());
        $writer->writeFloatValue('debitMth7', $this->getDebitMth7());
        $writer->writeFloatValue('debitMth8', $this->getDebitMth8());
        $writer->writeFloatValue('debitMth9', $this->getDebitMth9());
        $writer->writeIntegerValue('inactiveFlag', $this->getInactiveFlag());
        $writer->writeStringValue('name', $this->getName());
        $writer->writeFloatValue('priorYr2Mth1', $this->getPriorYr2Mth1());
        $writer->writeFloatValue('priorYr2Mth10', $this->getPriorYr2Mth10());
        $writer->writeFloatValue('priorYr2Mth11', $this->getPriorYr2Mth11());
        $writer->writeFloatValue('priorYr2Mth12', $this->getPriorYr2Mth12());
        $writer->writeFloatValue('priorYr2Mth2', $this->getPriorYr2Mth2());
        $writer->writeFloatValue('priorYr2Mth3', $this->getPriorYr2Mth3());
        $writer->writeFloatValue('priorYr2Mth4', $this->getPriorYr2Mth4());
        $writer->writeFloatValue('priorYr2Mth5', $this->getPriorYr2Mth5());
        $writer->writeFloatValue('priorYr2Mth6', $this->getPriorYr2Mth6());
        $writer->writeFloatValue('priorYr2Mth7', $this->getPriorYr2Mth7());
        $writer->writeFloatValue('priorYr2Mth8', $this->getPriorYr2Mth8());
        $writer->writeFloatValue('priorYr2Mth9', $this->getPriorYr2Mth9());
        $writer->writeFloatValue('priorYr3Mth1', $this->getPriorYr3Mth1());
        $writer->writeFloatValue('priorYr3Mth10', $this->getPriorYr3Mth10());
        $writer->writeFloatValue('priorYr3Mth11', $this->getPriorYr3Mth11());
        $writer->writeFloatValue('priorYr3Mth12', $this->getPriorYr3Mth12());
        $writer->writeFloatValue('priorYr3Mth2', $this->getPriorYr3Mth2());
        $writer->writeFloatValue('priorYr3Mth3', $this->getPriorYr3Mth3());
        $writer->writeFloatValue('priorYr3Mth4', $this->getPriorYr3Mth4());
        $writer->writeFloatValue('priorYr3Mth5', $this->getPriorYr3Mth5());
        $writer->writeFloatValue('priorYr3Mth6', $this->getPriorYr3Mth6());
        $writer->writeFloatValue('priorYr3Mth7', $this->getPriorYr3Mth7());
        $writer->writeFloatValue('priorYr3Mth8', $this->getPriorYr3Mth8());
        $writer->writeFloatValue('priorYr3Mth9', $this->getPriorYr3Mth9());
        $writer->writeFloatValue('priorYr4Mth1', $this->getPriorYr4Mth1());
        $writer->writeFloatValue('priorYr4Mth10', $this->getPriorYr4Mth10());
        $writer->writeFloatValue('priorYr4Mth11', $this->getPriorYr4Mth11());
        $writer->writeFloatValue('priorYr4Mth12', $this->getPriorYr4Mth12());
        $writer->writeFloatValue('priorYr4Mth2', $this->getPriorYr4Mth2());
        $writer->writeFloatValue('priorYr4Mth3', $this->getPriorYr4Mth3());
        $writer->writeFloatValue('priorYr4Mth4', $this->getPriorYr4Mth4());
        $writer->writeFloatValue('priorYr4Mth5', $this->getPriorYr4Mth5());
        $writer->writeFloatValue('priorYr4Mth6', $this->getPriorYr4Mth6());
        $writer->writeFloatValue('priorYr4Mth7', $this->getPriorYr4Mth7());
        $writer->writeFloatValue('priorYr4Mth8', $this->getPriorYr4Mth8());
        $writer->writeFloatValue('priorYr4Mth9', $this->getPriorYr4Mth9());
        $writer->writeFloatValue('priorYr5Mth1', $this->getPriorYr5Mth1());
        $writer->writeFloatValue('priorYr5Mth10', $this->getPriorYr5Mth10());
        $writer->writeFloatValue('priorYr5Mth11', $this->getPriorYr5Mth11());
        $writer->writeFloatValue('priorYr5Mth12', $this->getPriorYr5Mth12());
        $writer->writeFloatValue('priorYr5Mth2', $this->getPriorYr5Mth2());
        $writer->writeFloatValue('priorYr5Mth3', $this->getPriorYr5Mth3());
        $writer->writeFloatValue('priorYr5Mth4', $this->getPriorYr5Mth4());
        $writer->writeFloatValue('priorYr5Mth5', $this->getPriorYr5Mth5());
        $writer->writeFloatValue('priorYr5Mth6', $this->getPriorYr5Mth6());
        $writer->writeFloatValue('priorYr5Mth7', $this->getPriorYr5Mth7());
        $writer->writeFloatValue('priorYr5Mth8', $this->getPriorYr5Mth8());
        $writer->writeFloatValue('priorYr5Mth9', $this->getPriorYr5Mth9());
        $writer->writeFloatValue('priorYrMth1', $this->getPriorYrMth1());
        $writer->writeFloatValue('priorYrMth10', $this->getPriorYrMth10());
        $writer->writeFloatValue('priorYrMth11', $this->getPriorYrMth11());
        $writer->writeFloatValue('priorYrMth12', $this->getPriorYrMth12());
        $writer->writeFloatValue('priorYrMth2', $this->getPriorYrMth2());
        $writer->writeFloatValue('priorYrMth3', $this->getPriorYrMth3());
        $writer->writeFloatValue('priorYrMth4', $this->getPriorYrMth4());
        $writer->writeFloatValue('priorYrMth5', $this->getPriorYrMth5());
        $writer->writeFloatValue('priorYrMth6', $this->getPriorYrMth6());
        $writer->writeFloatValue('priorYrMth7', $this->getPriorYrMth7());
        $writer->writeFloatValue('priorYrMth8', $this->getPriorYrMth8());
        $writer->writeFloatValue('priorYrMth9', $this->getPriorYrMth9());
        $writer->writeIntegerValue('quickRatio', $this->getQuickRatio());
        $writer->writeDateTimeValue('recordCreateDate', $this->getRecordCreateDate());
        $writer->writeIntegerValue('recordDeleted', $this->getRecordDeleted());
        $writer->writeDateTimeValue('recordModifyDate', $this->getRecordModifyDate());
        $writer->writeIntegerValue('sofaId', $this->getSofaId());
    }

    /**
     * Sets the accountType property value. The accountType property
     * @param int|null $value Value to set for the accountType property.
    */
    public function setAccountType(?int $value): void {
        $this->accountType = $value;
    }

    /**
     * Sets the balance property value. The balance property
     * @param float|null $value Value to set for the balance property.
    */
    public function setBalance(?float $value): void {
        $this->balance = $value;
    }

    /**
     * Sets the balanceBf property value. The balanceBf property
     * @param float|null $value Value to set for the balanceBf property.
    */
    public function setBalanceBf(?float $value): void {
        $this->balanceBf = $value;
    }

    /**
     * Sets the balanceFuture property value. The balanceFuture property
     * @param float|null $value Value to set for the balanceFuture property.
    */
    public function setBalanceFuture(?float $value): void {
        $this->balanceFuture = $value;
    }

    /**
     * Sets the balanceMth1 property value. The balanceMth1 property
     * @param float|null $value Value to set for the balanceMth1 property.
    */
    public function setBalanceMth1(?float $value): void {
        $this->balanceMth1 = $value;
    }

    /**
     * Sets the balanceMth10 property value. The balanceMth10 property
     * @param float|null $value Value to set for the balanceMth10 property.
    */
    public function setBalanceMth10(?float $value): void {
        $this->balanceMth10 = $value;
    }

    /**
     * Sets the balanceMth11 property value. The balanceMth11 property
     * @param float|null $value Value to set for the balanceMth11 property.
    */
    public function setBalanceMth11(?float $value): void {
        $this->balanceMth11 = $value;
    }

    /**
     * Sets the balanceMth12 property value. The balanceMth12 property
     * @param float|null $value Value to set for the balanceMth12 property.
    */
    public function setBalanceMth12(?float $value): void {
        $this->balanceMth12 = $value;
    }

    /**
     * Sets the balanceMth2 property value. The balanceMth2 property
     * @param float|null $value Value to set for the balanceMth2 property.
    */
    public function setBalanceMth2(?float $value): void {
        $this->balanceMth2 = $value;
    }

    /**
     * Sets the balanceMth3 property value. The balanceMth3 property
     * @param float|null $value Value to set for the balanceMth3 property.
    */
    public function setBalanceMth3(?float $value): void {
        $this->balanceMth3 = $value;
    }

    /**
     * Sets the balanceMth4 property value. The balanceMth4 property
     * @param float|null $value Value to set for the balanceMth4 property.
    */
    public function setBalanceMth4(?float $value): void {
        $this->balanceMth4 = $value;
    }

    /**
     * Sets the balanceMth5 property value. The balanceMth5 property
     * @param float|null $value Value to set for the balanceMth5 property.
    */
    public function setBalanceMth5(?float $value): void {
        $this->balanceMth5 = $value;
    }

    /**
     * Sets the balanceMth6 property value. The balanceMth6 property
     * @param float|null $value Value to set for the balanceMth6 property.
    */
    public function setBalanceMth6(?float $value): void {
        $this->balanceMth6 = $value;
    }

    /**
     * Sets the balanceMth7 property value. The balanceMth7 property
     * @param float|null $value Value to set for the balanceMth7 property.
    */
    public function setBalanceMth7(?float $value): void {
        $this->balanceMth7 = $value;
    }

    /**
     * Sets the balanceMth8 property value. The balanceMth8 property
     * @param float|null $value Value to set for the balanceMth8 property.
    */
    public function setBalanceMth8(?float $value): void {
        $this->balanceMth8 = $value;
    }

    /**
     * Sets the balanceMth9 property value. The balanceMth9 property
     * @param float|null $value Value to set for the balanceMth9 property.
    */
    public function setBalanceMth9(?float $value): void {
        $this->balanceMth9 = $value;
    }

    /**
     * Sets the budgetMth1 property value. The budgetMth1 property
     * @param float|null $value Value to set for the budgetMth1 property.
    */
    public function setBudgetMth1(?float $value): void {
        $this->budgetMth1 = $value;
    }

    /**
     * Sets the budgetMth10 property value. The budgetMth10 property
     * @param float|null $value Value to set for the budgetMth10 property.
    */
    public function setBudgetMth10(?float $value): void {
        $this->budgetMth10 = $value;
    }

    /**
     * Sets the budgetMth11 property value. The budgetMth11 property
     * @param float|null $value Value to set for the budgetMth11 property.
    */
    public function setBudgetMth11(?float $value): void {
        $this->budgetMth11 = $value;
    }

    /**
     * Sets the budgetMth12 property value. The budgetMth12 property
     * @param float|null $value Value to set for the budgetMth12 property.
    */
    public function setBudgetMth12(?float $value): void {
        $this->budgetMth12 = $value;
    }

    /**
     * Sets the budgetMth2 property value. The budgetMth2 property
     * @param float|null $value Value to set for the budgetMth2 property.
    */
    public function setBudgetMth2(?float $value): void {
        $this->budgetMth2 = $value;
    }

    /**
     * Sets the budgetMth3 property value. The budgetMth3 property
     * @param float|null $value Value to set for the budgetMth3 property.
    */
    public function setBudgetMth3(?float $value): void {
        $this->budgetMth3 = $value;
    }

    /**
     * Sets the budgetMth4 property value. The budgetMth4 property
     * @param float|null $value Value to set for the budgetMth4 property.
    */
    public function setBudgetMth4(?float $value): void {
        $this->budgetMth4 = $value;
    }

    /**
     * Sets the budgetMth5 property value. The budgetMth5 property
     * @param float|null $value Value to set for the budgetMth5 property.
    */
    public function setBudgetMth5(?float $value): void {
        $this->budgetMth5 = $value;
    }

    /**
     * Sets the budgetMth6 property value. The budgetMth6 property
     * @param float|null $value Value to set for the budgetMth6 property.
    */
    public function setBudgetMth6(?float $value): void {
        $this->budgetMth6 = $value;
    }

    /**
     * Sets the budgetMth7 property value. The budgetMth7 property
     * @param float|null $value Value to set for the budgetMth7 property.
    */
    public function setBudgetMth7(?float $value): void {
        $this->budgetMth7 = $value;
    }

    /**
     * Sets the budgetMth8 property value. The budgetMth8 property
     * @param float|null $value Value to set for the budgetMth8 property.
    */
    public function setBudgetMth8(?float $value): void {
        $this->budgetMth8 = $value;
    }

    /**
     * Sets the budgetMth9 property value. The budgetMth9 property
     * @param float|null $value Value to set for the budgetMth9 property.
    */
    public function setBudgetMth9(?float $value): void {
        $this->budgetMth9 = $value;
    }

    /**
     * Sets the creditBf property value. The creditBf property
     * @param float|null $value Value to set for the creditBf property.
    */
    public function setCreditBf(?float $value): void {
        $this->creditBf = $value;
    }

    /**
     * Sets the creditFuture property value. The creditFuture property
     * @param float|null $value Value to set for the creditFuture property.
    */
    public function setCreditFuture(?float $value): void {
        $this->creditFuture = $value;
    }

    /**
     * Sets the creditMth1 property value. The creditMth1 property
     * @param float|null $value Value to set for the creditMth1 property.
    */
    public function setCreditMth1(?float $value): void {
        $this->creditMth1 = $value;
    }

    /**
     * Sets the creditMth10 property value. The creditMth10 property
     * @param float|null $value Value to set for the creditMth10 property.
    */
    public function setCreditMth10(?float $value): void {
        $this->creditMth10 = $value;
    }

    /**
     * Sets the creditMth11 property value. The creditMth11 property
     * @param float|null $value Value to set for the creditMth11 property.
    */
    public function setCreditMth11(?float $value): void {
        $this->creditMth11 = $value;
    }

    /**
     * Sets the creditMth12 property value. The creditMth12 property
     * @param float|null $value Value to set for the creditMth12 property.
    */
    public function setCreditMth12(?float $value): void {
        $this->creditMth12 = $value;
    }

    /**
     * Sets the creditMth2 property value. The creditMth2 property
     * @param float|null $value Value to set for the creditMth2 property.
    */
    public function setCreditMth2(?float $value): void {
        $this->creditMth2 = $value;
    }

    /**
     * Sets the creditMth3 property value. The creditMth3 property
     * @param float|null $value Value to set for the creditMth3 property.
    */
    public function setCreditMth3(?float $value): void {
        $this->creditMth3 = $value;
    }

    /**
     * Sets the creditMth4 property value. The creditMth4 property
     * @param float|null $value Value to set for the creditMth4 property.
    */
    public function setCreditMth4(?float $value): void {
        $this->creditMth4 = $value;
    }

    /**
     * Sets the creditMth5 property value. The creditMth5 property
     * @param float|null $value Value to set for the creditMth5 property.
    */
    public function setCreditMth5(?float $value): void {
        $this->creditMth5 = $value;
    }

    /**
     * Sets the creditMth6 property value. The creditMth6 property
     * @param float|null $value Value to set for the creditMth6 property.
    */
    public function setCreditMth6(?float $value): void {
        $this->creditMth6 = $value;
    }

    /**
     * Sets the creditMth7 property value. The creditMth7 property
     * @param float|null $value Value to set for the creditMth7 property.
    */
    public function setCreditMth7(?float $value): void {
        $this->creditMth7 = $value;
    }

    /**
     * Sets the creditMth8 property value. The creditMth8 property
     * @param float|null $value Value to set for the creditMth8 property.
    */
    public function setCreditMth8(?float $value): void {
        $this->creditMth8 = $value;
    }

    /**
     * Sets the creditMth9 property value. The creditMth9 property
     * @param float|null $value Value to set for the creditMth9 property.
    */
    public function setCreditMth9(?float $value): void {
        $this->creditMth9 = $value;
    }

    /**
     * Sets the debitBf property value. The debitBf property
     * @param float|null $value Value to set for the debitBf property.
    */
    public function setDebitBf(?float $value): void {
        $this->debitBf = $value;
    }

    /**
     * Sets the debitFuture property value. The debitFuture property
     * @param float|null $value Value to set for the debitFuture property.
    */
    public function setDebitFuture(?float $value): void {
        $this->debitFuture = $value;
    }

    /**
     * Sets the debitMth1 property value. The debitMth1 property
     * @param float|null $value Value to set for the debitMth1 property.
    */
    public function setDebitMth1(?float $value): void {
        $this->debitMth1 = $value;
    }

    /**
     * Sets the debitMth10 property value. The debitMth10 property
     * @param float|null $value Value to set for the debitMth10 property.
    */
    public function setDebitMth10(?float $value): void {
        $this->debitMth10 = $value;
    }

    /**
     * Sets the debitMth11 property value. The debitMth11 property
     * @param float|null $value Value to set for the debitMth11 property.
    */
    public function setDebitMth11(?float $value): void {
        $this->debitMth11 = $value;
    }

    /**
     * Sets the debitMth12 property value. The debitMth12 property
     * @param float|null $value Value to set for the debitMth12 property.
    */
    public function setDebitMth12(?float $value): void {
        $this->debitMth12 = $value;
    }

    /**
     * Sets the debitMth2 property value. The debitMth2 property
     * @param float|null $value Value to set for the debitMth2 property.
    */
    public function setDebitMth2(?float $value): void {
        $this->debitMth2 = $value;
    }

    /**
     * Sets the debitMth3 property value. The debitMth3 property
     * @param float|null $value Value to set for the debitMth3 property.
    */
    public function setDebitMth3(?float $value): void {
        $this->debitMth3 = $value;
    }

    /**
     * Sets the debitMth4 property value. The debitMth4 property
     * @param float|null $value Value to set for the debitMth4 property.
    */
    public function setDebitMth4(?float $value): void {
        $this->debitMth4 = $value;
    }

    /**
     * Sets the debitMth5 property value. The debitMth5 property
     * @param float|null $value Value to set for the debitMth5 property.
    */
    public function setDebitMth5(?float $value): void {
        $this->debitMth5 = $value;
    }

    /**
     * Sets the debitMth6 property value. The debitMth6 property
     * @param float|null $value Value to set for the debitMth6 property.
    */
    public function setDebitMth6(?float $value): void {
        $this->debitMth6 = $value;
    }

    /**
     * Sets the debitMth7 property value. The debitMth7 property
     * @param float|null $value Value to set for the debitMth7 property.
    */
    public function setDebitMth7(?float $value): void {
        $this->debitMth7 = $value;
    }

    /**
     * Sets the debitMth8 property value. The debitMth8 property
     * @param float|null $value Value to set for the debitMth8 property.
    */
    public function setDebitMth8(?float $value): void {
        $this->debitMth8 = $value;
    }

    /**
     * Sets the debitMth9 property value. The debitMth9 property
     * @param float|null $value Value to set for the debitMth9 property.
    */
    public function setDebitMth9(?float $value): void {
        $this->debitMth9 = $value;
    }

    /**
     * Sets the inactiveFlag property value. The inactiveFlag property
     * @param int|null $value Value to set for the inactiveFlag property.
    */
    public function setInactiveFlag(?int $value): void {
        $this->inactiveFlag = $value;
    }

    /**
     * Sets the name property value. The name property
     * @param string|null $value Value to set for the name property.
    */
    public function setName(?string $value): void {
        $this->name = $value;
    }

    /**
     * Sets the priorYr2Mth1 property value. The priorYr2Mth1 property
     * @param float|null $value Value to set for the priorYr2Mth1 property.
    */
    public function setPriorYr2Mth1(?float $value): void {
        $this->priorYr2Mth1 = $value;
    }

    /**
     * Sets the priorYr2Mth10 property value. The priorYr2Mth10 property
     * @param float|null $value Value to set for the priorYr2Mth10 property.
    */
    public function setPriorYr2Mth10(?float $value): void {
        $this->priorYr2Mth10 = $value;
    }

    /**
     * Sets the priorYr2Mth11 property value. The priorYr2Mth11 property
     * @param float|null $value Value to set for the priorYr2Mth11 property.
    */
    public function setPriorYr2Mth11(?float $value): void {
        $this->priorYr2Mth11 = $value;
    }

    /**
     * Sets the priorYr2Mth12 property value. The priorYr2Mth12 property
     * @param float|null $value Value to set for the priorYr2Mth12 property.
    */
    public function setPriorYr2Mth12(?float $value): void {
        $this->priorYr2Mth12 = $value;
    }

    /**
     * Sets the priorYr2Mth2 property value. The priorYr2Mth2 property
     * @param float|null $value Value to set for the priorYr2Mth2 property.
    */
    public function setPriorYr2Mth2(?float $value): void {
        $this->priorYr2Mth2 = $value;
    }

    /**
     * Sets the priorYr2Mth3 property value. The priorYr2Mth3 property
     * @param float|null $value Value to set for the priorYr2Mth3 property.
    */
    public function setPriorYr2Mth3(?float $value): void {
        $this->priorYr2Mth3 = $value;
    }

    /**
     * Sets the priorYr2Mth4 property value. The priorYr2Mth4 property
     * @param float|null $value Value to set for the priorYr2Mth4 property.
    */
    public function setPriorYr2Mth4(?float $value): void {
        $this->priorYr2Mth4 = $value;
    }

    /**
     * Sets the priorYr2Mth5 property value. The priorYr2Mth5 property
     * @param float|null $value Value to set for the priorYr2Mth5 property.
    */
    public function setPriorYr2Mth5(?float $value): void {
        $this->priorYr2Mth5 = $value;
    }

    /**
     * Sets the priorYr2Mth6 property value. The priorYr2Mth6 property
     * @param float|null $value Value to set for the priorYr2Mth6 property.
    */
    public function setPriorYr2Mth6(?float $value): void {
        $this->priorYr2Mth6 = $value;
    }

    /**
     * Sets the priorYr2Mth7 property value. The priorYr2Mth7 property
     * @param float|null $value Value to set for the priorYr2Mth7 property.
    */
    public function setPriorYr2Mth7(?float $value): void {
        $this->priorYr2Mth7 = $value;
    }

    /**
     * Sets the priorYr2Mth8 property value. The priorYr2Mth8 property
     * @param float|null $value Value to set for the priorYr2Mth8 property.
    */
    public function setPriorYr2Mth8(?float $value): void {
        $this->priorYr2Mth8 = $value;
    }

    /**
     * Sets the priorYr2Mth9 property value. The priorYr2Mth9 property
     * @param float|null $value Value to set for the priorYr2Mth9 property.
    */
    public function setPriorYr2Mth9(?float $value): void {
        $this->priorYr2Mth9 = $value;
    }

    /**
     * Sets the priorYr3Mth1 property value. The priorYr3Mth1 property
     * @param float|null $value Value to set for the priorYr3Mth1 property.
    */
    public function setPriorYr3Mth1(?float $value): void {
        $this->priorYr3Mth1 = $value;
    }

    /**
     * Sets the priorYr3Mth10 property value. The priorYr3Mth10 property
     * @param float|null $value Value to set for the priorYr3Mth10 property.
    */
    public function setPriorYr3Mth10(?float $value): void {
        $this->priorYr3Mth10 = $value;
    }

    /**
     * Sets the priorYr3Mth11 property value. The priorYr3Mth11 property
     * @param float|null $value Value to set for the priorYr3Mth11 property.
    */
    public function setPriorYr3Mth11(?float $value): void {
        $this->priorYr3Mth11 = $value;
    }

    /**
     * Sets the priorYr3Mth12 property value. The priorYr3Mth12 property
     * @param float|null $value Value to set for the priorYr3Mth12 property.
    */
    public function setPriorYr3Mth12(?float $value): void {
        $this->priorYr3Mth12 = $value;
    }

    /**
     * Sets the priorYr3Mth2 property value. The priorYr3Mth2 property
     * @param float|null $value Value to set for the priorYr3Mth2 property.
    */
    public function setPriorYr3Mth2(?float $value): void {
        $this->priorYr3Mth2 = $value;
    }

    /**
     * Sets the priorYr3Mth3 property value. The priorYr3Mth3 property
     * @param float|null $value Value to set for the priorYr3Mth3 property.
    */
    public function setPriorYr3Mth3(?float $value): void {
        $this->priorYr3Mth3 = $value;
    }

    /**
     * Sets the priorYr3Mth4 property value. The priorYr3Mth4 property
     * @param float|null $value Value to set for the priorYr3Mth4 property.
    */
    public function setPriorYr3Mth4(?float $value): void {
        $this->priorYr3Mth4 = $value;
    }

    /**
     * Sets the priorYr3Mth5 property value. The priorYr3Mth5 property
     * @param float|null $value Value to set for the priorYr3Mth5 property.
    */
    public function setPriorYr3Mth5(?float $value): void {
        $this->priorYr3Mth5 = $value;
    }

    /**
     * Sets the priorYr3Mth6 property value. The priorYr3Mth6 property
     * @param float|null $value Value to set for the priorYr3Mth6 property.
    */
    public function setPriorYr3Mth6(?float $value): void {
        $this->priorYr3Mth6 = $value;
    }

    /**
     * Sets the priorYr3Mth7 property value. The priorYr3Mth7 property
     * @param float|null $value Value to set for the priorYr3Mth7 property.
    */
    public function setPriorYr3Mth7(?float $value): void {
        $this->priorYr3Mth7 = $value;
    }

    /**
     * Sets the priorYr3Mth8 property value. The priorYr3Mth8 property
     * @param float|null $value Value to set for the priorYr3Mth8 property.
    */
    public function setPriorYr3Mth8(?float $value): void {
        $this->priorYr3Mth8 = $value;
    }

    /**
     * Sets the priorYr3Mth9 property value. The priorYr3Mth9 property
     * @param float|null $value Value to set for the priorYr3Mth9 property.
    */
    public function setPriorYr3Mth9(?float $value): void {
        $this->priorYr3Mth9 = $value;
    }

    /**
     * Sets the priorYr4Mth1 property value. The priorYr4Mth1 property
     * @param float|null $value Value to set for the priorYr4Mth1 property.
    */
    public function setPriorYr4Mth1(?float $value): void {
        $this->priorYr4Mth1 = $value;
    }

    /**
     * Sets the priorYr4Mth10 property value. The priorYr4Mth10 property
     * @param float|null $value Value to set for the priorYr4Mth10 property.
    */
    public function setPriorYr4Mth10(?float $value): void {
        $this->priorYr4Mth10 = $value;
    }

    /**
     * Sets the priorYr4Mth11 property value. The priorYr4Mth11 property
     * @param float|null $value Value to set for the priorYr4Mth11 property.
    */
    public function setPriorYr4Mth11(?float $value): void {
        $this->priorYr4Mth11 = $value;
    }

    /**
     * Sets the priorYr4Mth12 property value. The priorYr4Mth12 property
     * @param float|null $value Value to set for the priorYr4Mth12 property.
    */
    public function setPriorYr4Mth12(?float $value): void {
        $this->priorYr4Mth12 = $value;
    }

    /**
     * Sets the priorYr4Mth2 property value. The priorYr4Mth2 property
     * @param float|null $value Value to set for the priorYr4Mth2 property.
    */
    public function setPriorYr4Mth2(?float $value): void {
        $this->priorYr4Mth2 = $value;
    }

    /**
     * Sets the priorYr4Mth3 property value. The priorYr4Mth3 property
     * @param float|null $value Value to set for the priorYr4Mth3 property.
    */
    public function setPriorYr4Mth3(?float $value): void {
        $this->priorYr4Mth3 = $value;
    }

    /**
     * Sets the priorYr4Mth4 property value. The priorYr4Mth4 property
     * @param float|null $value Value to set for the priorYr4Mth4 property.
    */
    public function setPriorYr4Mth4(?float $value): void {
        $this->priorYr4Mth4 = $value;
    }

    /**
     * Sets the priorYr4Mth5 property value. The priorYr4Mth5 property
     * @param float|null $value Value to set for the priorYr4Mth5 property.
    */
    public function setPriorYr4Mth5(?float $value): void {
        $this->priorYr4Mth5 = $value;
    }

    /**
     * Sets the priorYr4Mth6 property value. The priorYr4Mth6 property
     * @param float|null $value Value to set for the priorYr4Mth6 property.
    */
    public function setPriorYr4Mth6(?float $value): void {
        $this->priorYr4Mth6 = $value;
    }

    /**
     * Sets the priorYr4Mth7 property value. The priorYr4Mth7 property
     * @param float|null $value Value to set for the priorYr4Mth7 property.
    */
    public function setPriorYr4Mth7(?float $value): void {
        $this->priorYr4Mth7 = $value;
    }

    /**
     * Sets the priorYr4Mth8 property value. The priorYr4Mth8 property
     * @param float|null $value Value to set for the priorYr4Mth8 property.
    */
    public function setPriorYr4Mth8(?float $value): void {
        $this->priorYr4Mth8 = $value;
    }

    /**
     * Sets the priorYr4Mth9 property value. The priorYr4Mth9 property
     * @param float|null $value Value to set for the priorYr4Mth9 property.
    */
    public function setPriorYr4Mth9(?float $value): void {
        $this->priorYr4Mth9 = $value;
    }

    /**
     * Sets the priorYr5Mth1 property value. The priorYr5Mth1 property
     * @param float|null $value Value to set for the priorYr5Mth1 property.
    */
    public function setPriorYr5Mth1(?float $value): void {
        $this->priorYr5Mth1 = $value;
    }

    /**
     * Sets the priorYr5Mth10 property value. The priorYr5Mth10 property
     * @param float|null $value Value to set for the priorYr5Mth10 property.
    */
    public function setPriorYr5Mth10(?float $value): void {
        $this->priorYr5Mth10 = $value;
    }

    /**
     * Sets the priorYr5Mth11 property value. The priorYr5Mth11 property
     * @param float|null $value Value to set for the priorYr5Mth11 property.
    */
    public function setPriorYr5Mth11(?float $value): void {
        $this->priorYr5Mth11 = $value;
    }

    /**
     * Sets the priorYr5Mth12 property value. The priorYr5Mth12 property
     * @param float|null $value Value to set for the priorYr5Mth12 property.
    */
    public function setPriorYr5Mth12(?float $value): void {
        $this->priorYr5Mth12 = $value;
    }

    /**
     * Sets the priorYr5Mth2 property value. The priorYr5Mth2 property
     * @param float|null $value Value to set for the priorYr5Mth2 property.
    */
    public function setPriorYr5Mth2(?float $value): void {
        $this->priorYr5Mth2 = $value;
    }

    /**
     * Sets the priorYr5Mth3 property value. The priorYr5Mth3 property
     * @param float|null $value Value to set for the priorYr5Mth3 property.
    */
    public function setPriorYr5Mth3(?float $value): void {
        $this->priorYr5Mth3 = $value;
    }

    /**
     * Sets the priorYr5Mth4 property value. The priorYr5Mth4 property
     * @param float|null $value Value to set for the priorYr5Mth4 property.
    */
    public function setPriorYr5Mth4(?float $value): void {
        $this->priorYr5Mth4 = $value;
    }

    /**
     * Sets the priorYr5Mth5 property value. The priorYr5Mth5 property
     * @param float|null $value Value to set for the priorYr5Mth5 property.
    */
    public function setPriorYr5Mth5(?float $value): void {
        $this->priorYr5Mth5 = $value;
    }

    /**
     * Sets the priorYr5Mth6 property value. The priorYr5Mth6 property
     * @param float|null $value Value to set for the priorYr5Mth6 property.
    */
    public function setPriorYr5Mth6(?float $value): void {
        $this->priorYr5Mth6 = $value;
    }

    /**
     * Sets the priorYr5Mth7 property value. The priorYr5Mth7 property
     * @param float|null $value Value to set for the priorYr5Mth7 property.
    */
    public function setPriorYr5Mth7(?float $value): void {
        $this->priorYr5Mth7 = $value;
    }

    /**
     * Sets the priorYr5Mth8 property value. The priorYr5Mth8 property
     * @param float|null $value Value to set for the priorYr5Mth8 property.
    */
    public function setPriorYr5Mth8(?float $value): void {
        $this->priorYr5Mth8 = $value;
    }

    /**
     * Sets the priorYr5Mth9 property value. The priorYr5Mth9 property
     * @param float|null $value Value to set for the priorYr5Mth9 property.
    */
    public function setPriorYr5Mth9(?float $value): void {
        $this->priorYr5Mth9 = $value;
    }

    /**
     * Sets the priorYrMth1 property value. The priorYrMth1 property
     * @param float|null $value Value to set for the priorYrMth1 property.
    */
    public function setPriorYrMth1(?float $value): void {
        $this->priorYrMth1 = $value;
    }

    /**
     * Sets the priorYrMth10 property value. The priorYrMth10 property
     * @param float|null $value Value to set for the priorYrMth10 property.
    */
    public function setPriorYrMth10(?float $value): void {
        $this->priorYrMth10 = $value;
    }

    /**
     * Sets the priorYrMth11 property value. The priorYrMth11 property
     * @param float|null $value Value to set for the priorYrMth11 property.
    */
    public function setPriorYrMth11(?float $value): void {
        $this->priorYrMth11 = $value;
    }

    /**
     * Sets the priorYrMth12 property value. The priorYrMth12 property
     * @param float|null $value Value to set for the priorYrMth12 property.
    */
    public function setPriorYrMth12(?float $value): void {
        $this->priorYrMth12 = $value;
    }

    /**
     * Sets the priorYrMth2 property value. The priorYrMth2 property
     * @param float|null $value Value to set for the priorYrMth2 property.
    */
    public function setPriorYrMth2(?float $value): void {
        $this->priorYrMth2 = $value;
    }

    /**
     * Sets the priorYrMth3 property value. The priorYrMth3 property
     * @param float|null $value Value to set for the priorYrMth3 property.
    */
    public function setPriorYrMth3(?float $value): void {
        $this->priorYrMth3 = $value;
    }

    /**
     * Sets the priorYrMth4 property value. The priorYrMth4 property
     * @param float|null $value Value to set for the priorYrMth4 property.
    */
    public function setPriorYrMth4(?float $value): void {
        $this->priorYrMth4 = $value;
    }

    /**
     * Sets the priorYrMth5 property value. The priorYrMth5 property
     * @param float|null $value Value to set for the priorYrMth5 property.
    */
    public function setPriorYrMth5(?float $value): void {
        $this->priorYrMth5 = $value;
    }

    /**
     * Sets the priorYrMth6 property value. The priorYrMth6 property
     * @param float|null $value Value to set for the priorYrMth6 property.
    */
    public function setPriorYrMth6(?float $value): void {
        $this->priorYrMth6 = $value;
    }

    /**
     * Sets the priorYrMth7 property value. The priorYrMth7 property
     * @param float|null $value Value to set for the priorYrMth7 property.
    */
    public function setPriorYrMth7(?float $value): void {
        $this->priorYrMth7 = $value;
    }

    /**
     * Sets the priorYrMth8 property value. The priorYrMth8 property
     * @param float|null $value Value to set for the priorYrMth8 property.
    */
    public function setPriorYrMth8(?float $value): void {
        $this->priorYrMth8 = $value;
    }

    /**
     * Sets the priorYrMth9 property value. The priorYrMth9 property
     * @param float|null $value Value to set for the priorYrMth9 property.
    */
    public function setPriorYrMth9(?float $value): void {
        $this->priorYrMth9 = $value;
    }

    /**
     * Sets the quickRatio property value. The quickRatio property
     * @param int|null $value Value to set for the quickRatio property.
    */
    public function setQuickRatio(?int $value): void {
        $this->quickRatio = $value;
    }

    /**
     * Sets the recordCreateDate property value. The recordCreateDate property
     * @param DateTime|null $value Value to set for the recordCreateDate property.
    */
    public function setRecordCreateDate(?DateTime $value): void {
        $this->recordCreateDate = $value;
    }

    /**
     * Sets the recordDeleted property value. The recordDeleted property
     * @param int|null $value Value to set for the recordDeleted property.
    */
    public function setRecordDeleted(?int $value): void {
        $this->recordDeleted = $value;
    }

    /**
     * Sets the recordModifyDate property value. The recordModifyDate property
     * @param DateTime|null $value Value to set for the recordModifyDate property.
    */
    public function setRecordModifyDate(?DateTime $value): void {
        $this->recordModifyDate = $value;
    }

    /**
     * Sets the sofaId property value. The sofaId property
     * @param int|null $value Value to set for the sofaId property.
    */
    public function setSofaId(?int $value): void {
        $this->sofaId = $value;
    }

}
