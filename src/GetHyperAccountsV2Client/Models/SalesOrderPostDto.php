<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class SalesOrderPostDto implements Parsable
{
    /**
     * @var SalesOrderAttributesWrite|null $attributes The attributes property
    */
    private ?SalesOrderAttributesWrite $attributes = null;

    /**
     * @var int|null $orderNumber The orderNumber property
    */
    private ?int $orderNumber = null;

    /**
     * @var SalesOrderRelationshipsWrite|null $relationships The relationships property
    */
    private ?SalesOrderRelationshipsWrite $relationships = null;

    /**
     * @var SalesOrderReliantChilds|null $reliantChilds The reliantChilds property
    */
    private ?SalesOrderReliantChilds $reliantChilds = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return SalesOrderPostDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): SalesOrderPostDto {
        return new SalesOrderPostDto();
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return SalesOrderAttributesWrite|null
    */
    public function getAttributes(): ?SalesOrderAttributesWrite {
        return $this->attributes;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([SalesOrderAttributesWrite::class, 'createFromDiscriminatorValue'])),
            'orderNumber' => fn(ParseNode $n) => $o->setOrderNumber($n->getIntegerValue()),
            'relationships' => fn(ParseNode $n) => $o->setRelationships($n->getObjectValue([SalesOrderRelationshipsWrite::class, 'createFromDiscriminatorValue'])),
            'reliantChilds' => fn(ParseNode $n) => $o->setReliantChilds($n->getObjectValue([SalesOrderReliantChilds::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the orderNumber property value. The orderNumber property
     * @return int|null
    */
    public function getOrderNumber(): ?int {
        return $this->orderNumber;
    }

    /**
     * Gets the relationships property value. The relationships property
     * @return SalesOrderRelationshipsWrite|null
    */
    public function getRelationships(): ?SalesOrderRelationshipsWrite {
        return $this->relationships;
    }

    /**
     * Gets the reliantChilds property value. The reliantChilds property
     * @return SalesOrderReliantChilds|null
    */
    public function getReliantChilds(): ?SalesOrderReliantChilds {
        return $this->reliantChilds;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('attributes', $this->getAttributes());
        $writer->writeIntegerValue('orderNumber', $this->getOrderNumber());
        $writer->writeObjectValue('relationships', $this->getRelationships());
        $writer->writeObjectValue('reliantChilds', $this->getReliantChilds());
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param SalesOrderAttributesWrite|null $value Value to set for the attributes property.
    */
    public function setAttributes(?SalesOrderAttributesWrite $value): void {
        $this->attributes = $value;
    }

    /**
     * Sets the orderNumber property value. The orderNumber property
     * @param int|null $value Value to set for the orderNumber property.
    */
    public function setOrderNumber(?int $value): void {
        $this->orderNumber = $value;
    }

    /**
     * Sets the relationships property value. The relationships property
     * @param SalesOrderRelationshipsWrite|null $value Value to set for the relationships property.
    */
    public function setRelationships(?SalesOrderRelationshipsWrite $value): void {
        $this->relationships = $value;
    }

    /**
     * Sets the reliantChilds property value. The reliantChilds property
     * @param SalesOrderReliantChilds|null $value Value to set for the reliantChilds property.
    */
    public function setReliantChilds(?SalesOrderReliantChilds $value): void {
        $this->reliantChilds = $value;
    }

}
