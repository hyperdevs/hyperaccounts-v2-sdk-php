<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class ChartOfAccountsCategoryRelationships implements Parsable
{
    /**
     * @var ChartOfAccountsCategoryTitleRelatedRelationship|null $categoryTitle The categoryTitle property
    */
    private ?ChartOfAccountsCategoryTitleRelatedRelationship $categoryTitle = null;

    /**
     * @var ChartOfAccountsRelatedRelationship|null $chart The chart property
    */
    private ?ChartOfAccountsRelatedRelationship $chart = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return ChartOfAccountsCategoryRelationships
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): ChartOfAccountsCategoryRelationships {
        return new ChartOfAccountsCategoryRelationships();
    }

    /**
     * Gets the categoryTitle property value. The categoryTitle property
     * @return ChartOfAccountsCategoryTitleRelatedRelationship|null
    */
    public function getCategoryTitle(): ?ChartOfAccountsCategoryTitleRelatedRelationship {
        return $this->categoryTitle;
    }

    /**
     * Gets the chart property value. The chart property
     * @return ChartOfAccountsRelatedRelationship|null
    */
    public function getChart(): ?ChartOfAccountsRelatedRelationship {
        return $this->chart;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'categoryTitle' => fn(ParseNode $n) => $o->setCategoryTitle($n->getObjectValue([ChartOfAccountsCategoryTitleRelatedRelationship::class, 'createFromDiscriminatorValue'])),
            'chart' => fn(ParseNode $n) => $o->setChart($n->getObjectValue([ChartOfAccountsRelatedRelationship::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('categoryTitle', $this->getCategoryTitle());
        $writer->writeObjectValue('chart', $this->getChart());
    }

    /**
     * Sets the categoryTitle property value. The categoryTitle property
     * @param ChartOfAccountsCategoryTitleRelatedRelationship|null $value Value to set for the categoryTitle property.
    */
    public function setCategoryTitle(?ChartOfAccountsCategoryTitleRelatedRelationship $value): void {
        $this->categoryTitle = $value;
    }

    /**
     * Sets the chart property value. The chart property
     * @param ChartOfAccountsRelatedRelationship|null $value Value to set for the chart property.
    */
    public function setChart(?ChartOfAccountsRelatedRelationship $value): void {
        $this->chart = $value;
    }

}
