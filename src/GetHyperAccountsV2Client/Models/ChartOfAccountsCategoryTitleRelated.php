<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class ChartOfAccountsCategoryTitleRelated implements Parsable
{
    /**
     * @var string|null $compositeKeyChartAndCategory The compositeKeyChartAndCategory property
    */
    private ?string $compositeKeyChartAndCategory = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return ChartOfAccountsCategoryTitleRelated
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): ChartOfAccountsCategoryTitleRelated {
        return new ChartOfAccountsCategoryTitleRelated();
    }

    /**
     * Gets the compositeKeyChartAndCategory property value. The compositeKeyChartAndCategory property
     * @return string|null
    */
    public function getCompositeKeyChartAndCategory(): ?string {
        return $this->compositeKeyChartAndCategory;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'compositeKeyChartAndCategory' => fn(ParseNode $n) => $o->setCompositeKeyChartAndCategory($n->getStringValue()),
        ];
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeStringValue('compositeKeyChartAndCategory', $this->getCompositeKeyChartAndCategory());
    }

    /**
     * Sets the compositeKeyChartAndCategory property value. The compositeKeyChartAndCategory property
     * @param string|null $value Value to set for the compositeKeyChartAndCategory property.
    */
    public function setCompositeKeyChartAndCategory(?string $value): void {
        $this->compositeKeyChartAndCategory = $value;
    }

}
