<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class CompanyGetDto implements Parsable
{
    /**
     * @var CompanyAttributesRead|null $attributes The attributes property
    */
    private ?CompanyAttributesRead $attributes = null;

    /**
     * @var string|null $companyid The companyid property
    */
    private ?string $companyid = null;

    /**
     * @var CompanyIncluded|null $included The included property
    */
    private ?CompanyIncluded $included = null;

    /**
     * @var ResourceLinks|null $links The links property
    */
    private ?ResourceLinks $links = null;

    /**
     * @var CompanyRelationships|null $relationships The relationships property
    */
    private ?CompanyRelationships $relationships = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return CompanyGetDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): CompanyGetDto {
        return new CompanyGetDto();
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return CompanyAttributesRead|null
    */
    public function getAttributes(): ?CompanyAttributesRead {
        return $this->attributes;
    }

    /**
     * Gets the companyid property value. The companyid property
     * @return string|null
    */
    public function getCompanyid(): ?string {
        return $this->companyid;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([CompanyAttributesRead::class, 'createFromDiscriminatorValue'])),
            'companyid' => fn(ParseNode $n) => $o->setCompanyid($n->getStringValue()),
            'included' => fn(ParseNode $n) => $o->setIncluded($n->getObjectValue([CompanyIncluded::class, 'createFromDiscriminatorValue'])),
            'links' => fn(ParseNode $n) => $o->setLinks($n->getObjectValue([ResourceLinks::class, 'createFromDiscriminatorValue'])),
            'relationships' => fn(ParseNode $n) => $o->setRelationships($n->getObjectValue([CompanyRelationships::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the included property value. The included property
     * @return CompanyIncluded|null
    */
    public function getIncluded(): ?CompanyIncluded {
        return $this->included;
    }

    /**
     * Gets the links property value. The links property
     * @return ResourceLinks|null
    */
    public function getLinks(): ?ResourceLinks {
        return $this->links;
    }

    /**
     * Gets the relationships property value. The relationships property
     * @return CompanyRelationships|null
    */
    public function getRelationships(): ?CompanyRelationships {
        return $this->relationships;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('attributes', $this->getAttributes());
        $writer->writeStringValue('companyid', $this->getCompanyid());
        $writer->writeObjectValue('included', $this->getIncluded());
        $writer->writeObjectValue('links', $this->getLinks());
        $writer->writeObjectValue('relationships', $this->getRelationships());
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param CompanyAttributesRead|null $value Value to set for the attributes property.
    */
    public function setAttributes(?CompanyAttributesRead $value): void {
        $this->attributes = $value;
    }

    /**
     * Sets the companyid property value. The companyid property
     * @param string|null $value Value to set for the companyid property.
    */
    public function setCompanyid(?string $value): void {
        $this->companyid = $value;
    }

    /**
     * Sets the included property value. The included property
     * @param CompanyIncluded|null $value Value to set for the included property.
    */
    public function setIncluded(?CompanyIncluded $value): void {
        $this->included = $value;
    }

    /**
     * Sets the links property value. The links property
     * @param ResourceLinks|null $value Value to set for the links property.
    */
    public function setLinks(?ResourceLinks $value): void {
        $this->links = $value;
    }

    /**
     * Sets the relationships property value. The relationships property
     * @param CompanyRelationships|null $value Value to set for the relationships property.
    */
    public function setRelationships(?CompanyRelationships $value): void {
        $this->relationships = $value;
    }

}
