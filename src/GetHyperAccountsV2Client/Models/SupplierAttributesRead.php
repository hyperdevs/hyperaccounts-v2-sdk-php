<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use DateTime;
use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class SupplierAttributesRead implements Parsable
{
    /**
     * @var int|null $accountOnHold The accountOnHold property
    */
    private ?int $accountOnHold = null;

    /**
     * @var string|null $address1 The address1 property
    */
    private ?string $address1 = null;

    /**
     * @var string|null $address2 The address2 property
    */
    private ?string $address2 = null;

    /**
     * @var string|null $address3 The address3 property
    */
    private ?string $address3 = null;

    /**
     * @var string|null $address4 The address4 property
    */
    private ?string $address4 = null;

    /**
     * @var string|null $address5 The address5 property
    */
    private ?string $address5 = null;

    /**
     * @var string|null $analysis1 The analysis1 property
    */
    private ?string $analysis1 = null;

    /**
     * @var string|null $analysis2 The analysis2 property
    */
    private ?string $analysis2 = null;

    /**
     * @var string|null $analysis3 The analysis3 property
    */
    private ?string $analysis3 = null;

    /**
     * @var string|null $bacsRef The bacsRef property
    */
    private ?string $bacsRef = null;

    /**
     * @var float|null $balance The balance property
    */
    private ?float $balance = null;

    /**
     * @var string|null $bankAccountName The bankAccountName property
    */
    private ?string $bankAccountName = null;

    /**
     * @var string|null $bankAccountNumber The bankAccountNumber property
    */
    private ?string $bankAccountNumber = null;

    /**
     * @var string|null $bankAdditionalref1 The bankAdditionalref1 property
    */
    private ?string $bankAdditionalref1 = null;

    /**
     * @var string|null $bankAdditionalref2 The bankAdditionalref2 property
    */
    private ?string $bankAdditionalref2 = null;

    /**
     * @var string|null $bankAdditionalref3 The bankAdditionalref3 property
    */
    private ?string $bankAdditionalref3 = null;

    /**
     * @var string|null $bankAddress1 The bankAddress1 property
    */
    private ?string $bankAddress1 = null;

    /**
     * @var string|null $bankAddress2 The bankAddress2 property
    */
    private ?string $bankAddress2 = null;

    /**
     * @var string|null $bankAddress3 The bankAddress3 property
    */
    private ?string $bankAddress3 = null;

    /**
     * @var string|null $bankAddress4 The bankAddress4 property
    */
    private ?string $bankAddress4 = null;

    /**
     * @var string|null $bankAddress5 The bankAddress5 property
    */
    private ?string $bankAddress5 = null;

    /**
     * @var string|null $bankBic The bankBic property
    */
    private ?string $bankBic = null;

    /**
     * @var string|null $bankIban The bankIban property
    */
    private ?string $bankIban = null;

    /**
     * @var string|null $bankName The bankName property
    */
    private ?string $bankName = null;

    /**
     * @var string|null $bankRollnumber The bankRollnumber property
    */
    private ?string $bankRollnumber = null;

    /**
     * @var string|null $bankSortCode The bankSortCode property
    */
    private ?string $bankSortCode = null;

    /**
     * @var int|null $bureauCode The bureauCode property
    */
    private ?int $bureauCode = null;

    /**
     * @var string|null $cAddress1 The cAddress1 property
    */
    private ?string $cAddress1 = null;

    /**
     * @var string|null $cAddress2 The cAddress2 property
    */
    private ?string $cAddress2 = null;

    /**
     * @var string|null $cAddress3 The cAddress3 property
    */
    private ?string $cAddress3 = null;

    /**
     * @var string|null $cAddress4 The cAddress4 property
    */
    private ?string $cAddress4 = null;

    /**
     * @var string|null $cAddress5 The cAddress5 property
    */
    private ?string $cAddress5 = null;

    /**
     * @var int|null $canCharge The canCharge property
    */
    private ?int $canCharge = null;

    /**
     * @var string|null $contactName The contactName property
    */
    private ?string $contactName = null;

    /**
     * @var string|null $countryCode The countryCode property
    */
    private ?string $countryCode = null;

    /**
     * @var float|null $creditBf The creditBf property
    */
    private ?float $creditBf = null;

    /**
     * @var float|null $creditCf The creditCf property
    */
    private ?float $creditCf = null;

    /**
     * @var float|null $creditLimit The creditLimit property
    */
    private ?float $creditLimit = null;

    /**
     * @var float|null $creditMth1 The creditMth1 property
    */
    private ?float $creditMth1 = null;

    /**
     * @var float|null $creditMth10 The creditMth10 property
    */
    private ?float $creditMth10 = null;

    /**
     * @var float|null $creditMth11 The creditMth11 property
    */
    private ?float $creditMth11 = null;

    /**
     * @var float|null $creditMth12 The creditMth12 property
    */
    private ?float $creditMth12 = null;

    /**
     * @var float|null $creditMth2 The creditMth2 property
    */
    private ?float $creditMth2 = null;

    /**
     * @var float|null $creditMth3 The creditMth3 property
    */
    private ?float $creditMth3 = null;

    /**
     * @var float|null $creditMth4 The creditMth4 property
    */
    private ?float $creditMth4 = null;

    /**
     * @var float|null $creditMth5 The creditMth5 property
    */
    private ?float $creditMth5 = null;

    /**
     * @var float|null $creditMth6 The creditMth6 property
    */
    private ?float $creditMth6 = null;

    /**
     * @var float|null $creditMth7 The creditMth7 property
    */
    private ?float $creditMth7 = null;

    /**
     * @var float|null $creditMth8 The creditMth8 property
    */
    private ?float $creditMth8 = null;

    /**
     * @var float|null $creditMth9 The creditMth9 property
    */
    private ?float $creditMth9 = null;

    /**
     * @var int|null $creditPosCode The creditPosCode property
    */
    private ?int $creditPosCode = null;

    /**
     * @var SupplierAttributesRead_creditPosition|null $creditPosition The creditPosition property
    */
    private ?SupplierAttributesRead_creditPosition $creditPosition = null;

    /**
     * @var string|null $creditRef The creditRef property
    */
    private ?string $creditRef = null;

    /**
     * @var int|null $currency The currency property
    */
    private ?int $currency = null;

    /**
     * @var DateTime|null $dateAccountOpened The dateAccountOpened property
    */
    private ?DateTime $dateAccountOpened = null;

    /**
     * @var DateTime|null $dateCreditApplied The dateCreditApplied property
    */
    private ?DateTime $dateCreditApplied = null;

    /**
     * @var DateTime|null $dateCreditReceived The dateCreditReceived property
    */
    private ?DateTime $dateCreditReceived = null;

    /**
     * @var DateTime|null $dateLastCredit The dateLastCredit property
    */
    private ?DateTime $dateLastCredit = null;

    /**
     * @var DateTime|null $dateNextCredit The dateNextCredit property
    */
    private ?DateTime $dateNextCredit = null;

    /**
     * @var DateTime|null $declarationValidFrom The declarationValidFrom property
    */
    private ?DateTime $declarationValidFrom = null;

    /**
     * @var int|null $defaultFundId The defaultFundId property
    */
    private ?int $defaultFundId = null;

    /**
     * @var string|null $defNomCode The defNomCode property
    */
    private ?string $defNomCode = null;

    /**
     * @var SupplierAttributesRead_defTaxCode|null $defTaxCode The defTaxCode property
    */
    private ?SupplierAttributesRead_defTaxCode $defTaxCode = null;

    /**
     * @var string|null $delAddress1 The delAddress1 property
    */
    private ?string $delAddress1 = null;

    /**
     * @var string|null $delAddress2 The delAddress2 property
    */
    private ?string $delAddress2 = null;

    /**
     * @var string|null $delAddress3 The delAddress3 property
    */
    private ?string $delAddress3 = null;

    /**
     * @var string|null $delAddress4 The delAddress4 property
    */
    private ?string $delAddress4 = null;

    /**
     * @var string|null $delAddress5 The delAddress5 property
    */
    private ?string $delAddress5 = null;

    /**
     * @var string|null $delContactName The delContactName property
    */
    private ?string $delContactName = null;

    /**
     * @var string|null $delFax The delFax property
    */
    private ?string $delFax = null;

    /**
     * @var string|null $delName The delName property
    */
    private ?string $delName = null;

    /**
     * @var string|null $delTelephone The delTelephone property
    */
    private ?string $delTelephone = null;

    /**
     * @var string|null $deptName The deptName property
    */
    private ?string $deptName = null;

    /**
     * @var int|null $deptNumber The deptNumber property
    */
    private ?int $deptNumber = null;

    /**
     * @var float|null $discountRate The discountRate property
    */
    private ?float $discountRate = null;

    /**
     * @var string|null $donorForename The donorForename property
    */
    private ?string $donorForename = null;

    /**
     * @var string|null $donorSurname The donorSurname property
    */
    private ?string $donorSurname = null;

    /**
     * @var string|null $donorTitle The donorTitle property
    */
    private ?string $donorTitle = null;

    /**
     * @var string|null $dunsNumber The dunsNumber property
    */
    private ?string $dunsNumber = null;

    /**
     * @var string|null $eMail The eMail property
    */
    private ?string $eMail = null;

    /**
     * @var string|null $eMail2 The eMail2 property
    */
    private ?string $eMail2 = null;

    /**
     * @var string|null $eMail3 The eMail3 property
    */
    private ?string $eMail3 = null;

    /**
     * @var string|null $eoriNumber The eoriNumber property
    */
    private ?string $eoriNumber = null;

    /**
     * @var string|null $facebookAddress The facebookAddress property
    */
    private ?string $facebookAddress = null;

    /**
     * @var string|null $fax The fax property
    */
    private ?string $fax = null;

    /**
     * @var DateTime|null $firstInvDate The firstInvDate property
    */
    private ?DateTime $firstInvDate = null;

    /**
     * @var float|null $foreignBalance The foreignBalance property
    */
    private ?float $foreignBalance = null;

    /**
     * @var int|null $inactiveFlag The inactiveFlag property
    */
    private ?int $inactiveFlag = null;

    /**
     * @var int|null $incoterms The incoterms property
    */
    private ?int $incoterms = null;

    /**
     * @var string|null $incotermsText The incotermsText property
    */
    private ?string $incotermsText = null;

    /**
     * @var float|null $invoiceBf The invoiceBf property
    */
    private ?float $invoiceBf = null;

    /**
     * @var float|null $invoiceCf The invoiceCf property
    */
    private ?float $invoiceCf = null;

    /**
     * @var float|null $invoiceMth1 The invoiceMth1 property
    */
    private ?float $invoiceMth1 = null;

    /**
     * @var float|null $invoiceMth10 The invoiceMth10 property
    */
    private ?float $invoiceMth10 = null;

    /**
     * @var float|null $invoiceMth11 The invoiceMth11 property
    */
    private ?float $invoiceMth11 = null;

    /**
     * @var float|null $invoiceMth12 The invoiceMth12 property
    */
    private ?float $invoiceMth12 = null;

    /**
     * @var float|null $invoiceMth2 The invoiceMth2 property
    */
    private ?float $invoiceMth2 = null;

    /**
     * @var float|null $invoiceMth3 The invoiceMth3 property
    */
    private ?float $invoiceMth3 = null;

    /**
     * @var float|null $invoiceMth4 The invoiceMth4 property
    */
    private ?float $invoiceMth4 = null;

    /**
     * @var float|null $invoiceMth5 The invoiceMth5 property
    */
    private ?float $invoiceMth5 = null;

    /**
     * @var float|null $invoiceMth6 The invoiceMth6 property
    */
    private ?float $invoiceMth6 = null;

    /**
     * @var float|null $invoiceMth7 The invoiceMth7 property
    */
    private ?float $invoiceMth7 = null;

    /**
     * @var float|null $invoiceMth8 The invoiceMth8 property
    */
    private ?float $invoiceMth8 = null;

    /**
     * @var float|null $invoiceMth9 The invoiceMth9 property
    */
    private ?float $invoiceMth9 = null;

    /**
     * @var DateTime|null $lastInvDate The lastInvDate property
    */
    private ?DateTime $lastInvDate = null;

    /**
     * @var DateTime|null $lastPaymentDate The lastPaymentDate property
    */
    private ?DateTime $lastPaymentDate = null;

    /**
     * @var DateTime|null $lastRefreshedDate The lastRefreshedDate property
    */
    private ?DateTime $lastRefreshedDate = null;

    /**
     * @var int|null $lettersViaEmail The lettersViaEmail property
    */
    private ?int $lettersViaEmail = null;

    /**
     * @var string|null $linkedinAddress The linkedinAddress property
    */
    private ?string $linkedinAddress = null;

    /**
     * @var string|null $mandateid The mandateid property
    */
    private ?string $mandateid = null;

    /**
     * @var string|null $name The name property
    */
    private ?string $name = null;

    /**
     * @var int|null $overrideTaxCode The overrideTaxCode property
    */
    private ?int $overrideTaxCode = null;

    /**
     * @var float|null $paymentBf The paymentBf property
    */
    private ?float $paymentBf = null;

    /**
     * @var float|null $paymentCf The paymentCf property
    */
    private ?float $paymentCf = null;

    /**
     * @var int|null $paymentDueDays The paymentDueDays property
    */
    private ?int $paymentDueDays = null;

    /**
     * @var int|null $paymentDueFrom The paymentDueFrom property
    */
    private ?int $paymentDueFrom = null;

    /**
     * @var string|null $paymentDueFromText The paymentDueFromText property
    */
    private ?string $paymentDueFromText = null;

    /**
     * @var int|null $paymentMethodId The paymentMethodId property
    */
    private ?int $paymentMethodId = null;

    /**
     * @var float|null $paymentMth1 The paymentMth1 property
    */
    private ?float $paymentMth1 = null;

    /**
     * @var float|null $paymentMth10 The paymentMth10 property
    */
    private ?float $paymentMth10 = null;

    /**
     * @var float|null $paymentMth11 The paymentMth11 property
    */
    private ?float $paymentMth11 = null;

    /**
     * @var float|null $paymentMth12 The paymentMth12 property
    */
    private ?float $paymentMth12 = null;

    /**
     * @var float|null $paymentMth2 The paymentMth2 property
    */
    private ?float $paymentMth2 = null;

    /**
     * @var float|null $paymentMth3 The paymentMth3 property
    */
    private ?float $paymentMth3 = null;

    /**
     * @var float|null $paymentMth4 The paymentMth4 property
    */
    private ?float $paymentMth4 = null;

    /**
     * @var float|null $paymentMth5 The paymentMth5 property
    */
    private ?float $paymentMth5 = null;

    /**
     * @var float|null $paymentMth6 The paymentMth6 property
    */
    private ?float $paymentMth6 = null;

    /**
     * @var float|null $paymentMth7 The paymentMth7 property
    */
    private ?float $paymentMth7 = null;

    /**
     * @var float|null $paymentMth8 The paymentMth8 property
    */
    private ?float $paymentMth8 = null;

    /**
     * @var float|null $paymentMth9 The paymentMth9 property
    */
    private ?float $paymentMth9 = null;

    /**
     * @var int|null $priority The priority property
    */
    private ?int $priority = null;

    /**
     * @var float|null $priorYear The priorYear property
    */
    private ?float $priorYear = null;

    /**
     * @var float|null $promisedPayment The promisedPayment property
    */
    private ?float $promisedPayment = null;

    /**
     * @var DateTime|null $promisedPaymentDate The promisedPaymentDate property
    */
    private ?DateTime $promisedPaymentDate = null;

    /**
     * @var DateTime|null $recordCreateDate The recordCreateDate property
    */
    private ?DateTime $recordCreateDate = null;

    /**
     * @var int|null $recordDeleted The recordDeleted property
    */
    private ?int $recordDeleted = null;

    /**
     * @var DateTime|null $recordModifyDate The recordModifyDate property
    */
    private ?DateTime $recordModifyDate = null;

    /**
     * @var string|null $reportPassword The reportPassword property
    */
    private ?string $reportPassword = null;

    /**
     * @var int|null $restrictMail The restrictMail property
    */
    private ?int $restrictMail = null;

    /**
     * @var float|null $settlementDiscRate The settlementDiscRate property
    */
    private ?float $settlementDiscRate = null;

    /**
     * @var int|null $settlementDueDays The settlementDueDays property
    */
    private ?int $settlementDueDays = null;

    /**
     * @var int|null $status The status property
    */
    private ?int $status = null;

    /**
     * @var string|null $statusDescription The statusDescription property
    */
    private ?string $statusDescription = null;

    /**
     * @var int|null $statusNumber The statusNumber property
    */
    private ?int $statusNumber = null;

    /**
     * @var string|null $statusText The statusText property
    */
    private ?string $statusText = null;

    /**
     * @var string|null $telephone The telephone property
    */
    private ?string $telephone = null;

    /**
     * @var string|null $telephone2 The telephone2 property
    */
    private ?string $telephone2 = null;

    /**
     * @var string|null $terms The terms property
    */
    private ?string $terms = null;

    /**
     * @var int|null $termsAgreed The termsAgreed property
    */
    private ?int $termsAgreed = null;

    /**
     * @var string|null $tradeContact The tradeContact property
    */
    private ?string $tradeContact = null;

    /**
     * @var float|null $turnoverMtd The turnoverMtd property
    */
    private ?float $turnoverMtd = null;

    /**
     * @var float|null $turnoverYtd The turnoverYtd property
    */
    private ?float $turnoverYtd = null;

    /**
     * @var string|null $twitterAddress The twitterAddress property
    */
    private ?string $twitterAddress = null;

    /**
     * @var int|null $useBacs The useBacs property
    */
    private ?int $useBacs = null;

    /**
     * @var int|null $useBsoc The useBsoc property
    */
    private ?int $useBsoc = null;

    /**
     * @var string|null $vatRegNumber The vatRegNumber property
    */
    private ?string $vatRegNumber = null;

    /**
     * @var string|null $webAddress The webAddress property
    */
    private ?string $webAddress = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return SupplierAttributesRead
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): SupplierAttributesRead {
        return new SupplierAttributesRead();
    }

    /**
     * Gets the accountOnHold property value. The accountOnHold property
     * @return int|null
    */
    public function getAccountOnHold(): ?int {
        return $this->accountOnHold;
    }

    /**
     * Gets the address1 property value. The address1 property
     * @return string|null
    */
    public function getAddress1(): ?string {
        return $this->address1;
    }

    /**
     * Gets the address2 property value. The address2 property
     * @return string|null
    */
    public function getAddress2(): ?string {
        return $this->address2;
    }

    /**
     * Gets the address3 property value. The address3 property
     * @return string|null
    */
    public function getAddress3(): ?string {
        return $this->address3;
    }

    /**
     * Gets the address4 property value. The address4 property
     * @return string|null
    */
    public function getAddress4(): ?string {
        return $this->address4;
    }

    /**
     * Gets the address5 property value. The address5 property
     * @return string|null
    */
    public function getAddress5(): ?string {
        return $this->address5;
    }

    /**
     * Gets the analysis1 property value. The analysis1 property
     * @return string|null
    */
    public function getAnalysis1(): ?string {
        return $this->analysis1;
    }

    /**
     * Gets the analysis2 property value. The analysis2 property
     * @return string|null
    */
    public function getAnalysis2(): ?string {
        return $this->analysis2;
    }

    /**
     * Gets the analysis3 property value. The analysis3 property
     * @return string|null
    */
    public function getAnalysis3(): ?string {
        return $this->analysis3;
    }

    /**
     * Gets the bacsRef property value. The bacsRef property
     * @return string|null
    */
    public function getBacsRef(): ?string {
        return $this->bacsRef;
    }

    /**
     * Gets the balance property value. The balance property
     * @return float|null
    */
    public function getBalance(): ?float {
        return $this->balance;
    }

    /**
     * Gets the bankAccountName property value. The bankAccountName property
     * @return string|null
    */
    public function getBankAccountName(): ?string {
        return $this->bankAccountName;
    }

    /**
     * Gets the bankAccountNumber property value. The bankAccountNumber property
     * @return string|null
    */
    public function getBankAccountNumber(): ?string {
        return $this->bankAccountNumber;
    }

    /**
     * Gets the bankAdditionalref1 property value. The bankAdditionalref1 property
     * @return string|null
    */
    public function getBankAdditionalref1(): ?string {
        return $this->bankAdditionalref1;
    }

    /**
     * Gets the bankAdditionalref2 property value. The bankAdditionalref2 property
     * @return string|null
    */
    public function getBankAdditionalref2(): ?string {
        return $this->bankAdditionalref2;
    }

    /**
     * Gets the bankAdditionalref3 property value. The bankAdditionalref3 property
     * @return string|null
    */
    public function getBankAdditionalref3(): ?string {
        return $this->bankAdditionalref3;
    }

    /**
     * Gets the bankAddress1 property value. The bankAddress1 property
     * @return string|null
    */
    public function getBankAddress1(): ?string {
        return $this->bankAddress1;
    }

    /**
     * Gets the bankAddress2 property value. The bankAddress2 property
     * @return string|null
    */
    public function getBankAddress2(): ?string {
        return $this->bankAddress2;
    }

    /**
     * Gets the bankAddress3 property value. The bankAddress3 property
     * @return string|null
    */
    public function getBankAddress3(): ?string {
        return $this->bankAddress3;
    }

    /**
     * Gets the bankAddress4 property value. The bankAddress4 property
     * @return string|null
    */
    public function getBankAddress4(): ?string {
        return $this->bankAddress4;
    }

    /**
     * Gets the bankAddress5 property value. The bankAddress5 property
     * @return string|null
    */
    public function getBankAddress5(): ?string {
        return $this->bankAddress5;
    }

    /**
     * Gets the bankBic property value. The bankBic property
     * @return string|null
    */
    public function getBankBic(): ?string {
        return $this->bankBic;
    }

    /**
     * Gets the bankIban property value. The bankIban property
     * @return string|null
    */
    public function getBankIban(): ?string {
        return $this->bankIban;
    }

    /**
     * Gets the bankName property value. The bankName property
     * @return string|null
    */
    public function getBankName(): ?string {
        return $this->bankName;
    }

    /**
     * Gets the bankRollnumber property value. The bankRollnumber property
     * @return string|null
    */
    public function getBankRollnumber(): ?string {
        return $this->bankRollnumber;
    }

    /**
     * Gets the bankSortCode property value. The bankSortCode property
     * @return string|null
    */
    public function getBankSortCode(): ?string {
        return $this->bankSortCode;
    }

    /**
     * Gets the bureauCode property value. The bureauCode property
     * @return int|null
    */
    public function getBureauCode(): ?int {
        return $this->bureauCode;
    }

    /**
     * Gets the cAddress1 property value. The cAddress1 property
     * @return string|null
    */
    public function getCAddress1(): ?string {
        return $this->cAddress1;
    }

    /**
     * Gets the cAddress2 property value. The cAddress2 property
     * @return string|null
    */
    public function getCAddress2(): ?string {
        return $this->cAddress2;
    }

    /**
     * Gets the cAddress3 property value. The cAddress3 property
     * @return string|null
    */
    public function getCAddress3(): ?string {
        return $this->cAddress3;
    }

    /**
     * Gets the cAddress4 property value. The cAddress4 property
     * @return string|null
    */
    public function getCAddress4(): ?string {
        return $this->cAddress4;
    }

    /**
     * Gets the cAddress5 property value. The cAddress5 property
     * @return string|null
    */
    public function getCAddress5(): ?string {
        return $this->cAddress5;
    }

    /**
     * Gets the canCharge property value. The canCharge property
     * @return int|null
    */
    public function getCanCharge(): ?int {
        return $this->canCharge;
    }

    /**
     * Gets the contactName property value. The contactName property
     * @return string|null
    */
    public function getContactName(): ?string {
        return $this->contactName;
    }

    /**
     * Gets the countryCode property value. The countryCode property
     * @return string|null
    */
    public function getCountryCode(): ?string {
        return $this->countryCode;
    }

    /**
     * Gets the creditBf property value. The creditBf property
     * @return float|null
    */
    public function getCreditBf(): ?float {
        return $this->creditBf;
    }

    /**
     * Gets the creditCf property value. The creditCf property
     * @return float|null
    */
    public function getCreditCf(): ?float {
        return $this->creditCf;
    }

    /**
     * Gets the creditLimit property value. The creditLimit property
     * @return float|null
    */
    public function getCreditLimit(): ?float {
        return $this->creditLimit;
    }

    /**
     * Gets the creditMth1 property value. The creditMth1 property
     * @return float|null
    */
    public function getCreditMth1(): ?float {
        return $this->creditMth1;
    }

    /**
     * Gets the creditMth10 property value. The creditMth10 property
     * @return float|null
    */
    public function getCreditMth10(): ?float {
        return $this->creditMth10;
    }

    /**
     * Gets the creditMth11 property value. The creditMth11 property
     * @return float|null
    */
    public function getCreditMth11(): ?float {
        return $this->creditMth11;
    }

    /**
     * Gets the creditMth12 property value. The creditMth12 property
     * @return float|null
    */
    public function getCreditMth12(): ?float {
        return $this->creditMth12;
    }

    /**
     * Gets the creditMth2 property value. The creditMth2 property
     * @return float|null
    */
    public function getCreditMth2(): ?float {
        return $this->creditMth2;
    }

    /**
     * Gets the creditMth3 property value. The creditMth3 property
     * @return float|null
    */
    public function getCreditMth3(): ?float {
        return $this->creditMth3;
    }

    /**
     * Gets the creditMth4 property value. The creditMth4 property
     * @return float|null
    */
    public function getCreditMth4(): ?float {
        return $this->creditMth4;
    }

    /**
     * Gets the creditMth5 property value. The creditMth5 property
     * @return float|null
    */
    public function getCreditMth5(): ?float {
        return $this->creditMth5;
    }

    /**
     * Gets the creditMth6 property value. The creditMth6 property
     * @return float|null
    */
    public function getCreditMth6(): ?float {
        return $this->creditMth6;
    }

    /**
     * Gets the creditMth7 property value. The creditMth7 property
     * @return float|null
    */
    public function getCreditMth7(): ?float {
        return $this->creditMth7;
    }

    /**
     * Gets the creditMth8 property value. The creditMth8 property
     * @return float|null
    */
    public function getCreditMth8(): ?float {
        return $this->creditMth8;
    }

    /**
     * Gets the creditMth9 property value. The creditMth9 property
     * @return float|null
    */
    public function getCreditMth9(): ?float {
        return $this->creditMth9;
    }

    /**
     * Gets the creditPosCode property value. The creditPosCode property
     * @return int|null
    */
    public function getCreditPosCode(): ?int {
        return $this->creditPosCode;
    }

    /**
     * Gets the creditPosition property value. The creditPosition property
     * @return SupplierAttributesRead_creditPosition|null
    */
    public function getCreditPosition(): ?SupplierAttributesRead_creditPosition {
        return $this->creditPosition;
    }

    /**
     * Gets the creditRef property value. The creditRef property
     * @return string|null
    */
    public function getCreditRef(): ?string {
        return $this->creditRef;
    }

    /**
     * Gets the currency property value. The currency property
     * @return int|null
    */
    public function getCurrency(): ?int {
        return $this->currency;
    }

    /**
     * Gets the dateAccountOpened property value. The dateAccountOpened property
     * @return DateTime|null
    */
    public function getDateAccountOpened(): ?DateTime {
        return $this->dateAccountOpened;
    }

    /**
     * Gets the dateCreditApplied property value. The dateCreditApplied property
     * @return DateTime|null
    */
    public function getDateCreditApplied(): ?DateTime {
        return $this->dateCreditApplied;
    }

    /**
     * Gets the dateCreditReceived property value. The dateCreditReceived property
     * @return DateTime|null
    */
    public function getDateCreditReceived(): ?DateTime {
        return $this->dateCreditReceived;
    }

    /**
     * Gets the dateLastCredit property value. The dateLastCredit property
     * @return DateTime|null
    */
    public function getDateLastCredit(): ?DateTime {
        return $this->dateLastCredit;
    }

    /**
     * Gets the dateNextCredit property value. The dateNextCredit property
     * @return DateTime|null
    */
    public function getDateNextCredit(): ?DateTime {
        return $this->dateNextCredit;
    }

    /**
     * Gets the declarationValidFrom property value. The declarationValidFrom property
     * @return DateTime|null
    */
    public function getDeclarationValidFrom(): ?DateTime {
        return $this->declarationValidFrom;
    }

    /**
     * Gets the defaultFundId property value. The defaultFundId property
     * @return int|null
    */
    public function getDefaultFundId(): ?int {
        return $this->defaultFundId;
    }

    /**
     * Gets the defNomCode property value. The defNomCode property
     * @return string|null
    */
    public function getDefNomCode(): ?string {
        return $this->defNomCode;
    }

    /**
     * Gets the defTaxCode property value. The defTaxCode property
     * @return SupplierAttributesRead_defTaxCode|null
    */
    public function getDefTaxCode(): ?SupplierAttributesRead_defTaxCode {
        return $this->defTaxCode;
    }

    /**
     * Gets the delAddress1 property value. The delAddress1 property
     * @return string|null
    */
    public function getDelAddress1(): ?string {
        return $this->delAddress1;
    }

    /**
     * Gets the delAddress2 property value. The delAddress2 property
     * @return string|null
    */
    public function getDelAddress2(): ?string {
        return $this->delAddress2;
    }

    /**
     * Gets the delAddress3 property value. The delAddress3 property
     * @return string|null
    */
    public function getDelAddress3(): ?string {
        return $this->delAddress3;
    }

    /**
     * Gets the delAddress4 property value. The delAddress4 property
     * @return string|null
    */
    public function getDelAddress4(): ?string {
        return $this->delAddress4;
    }

    /**
     * Gets the delAddress5 property value. The delAddress5 property
     * @return string|null
    */
    public function getDelAddress5(): ?string {
        return $this->delAddress5;
    }

    /**
     * Gets the delContactName property value. The delContactName property
     * @return string|null
    */
    public function getDelContactName(): ?string {
        return $this->delContactName;
    }

    /**
     * Gets the delFax property value. The delFax property
     * @return string|null
    */
    public function getDelFax(): ?string {
        return $this->delFax;
    }

    /**
     * Gets the delName property value. The delName property
     * @return string|null
    */
    public function getDelName(): ?string {
        return $this->delName;
    }

    /**
     * Gets the delTelephone property value. The delTelephone property
     * @return string|null
    */
    public function getDelTelephone(): ?string {
        return $this->delTelephone;
    }

    /**
     * Gets the deptName property value. The deptName property
     * @return string|null
    */
    public function getDeptName(): ?string {
        return $this->deptName;
    }

    /**
     * Gets the deptNumber property value. The deptNumber property
     * @return int|null
    */
    public function getDeptNumber(): ?int {
        return $this->deptNumber;
    }

    /**
     * Gets the discountRate property value. The discountRate property
     * @return float|null
    */
    public function getDiscountRate(): ?float {
        return $this->discountRate;
    }

    /**
     * Gets the donorForename property value. The donorForename property
     * @return string|null
    */
    public function getDonorForename(): ?string {
        return $this->donorForename;
    }

    /**
     * Gets the donorSurname property value. The donorSurname property
     * @return string|null
    */
    public function getDonorSurname(): ?string {
        return $this->donorSurname;
    }

    /**
     * Gets the donorTitle property value. The donorTitle property
     * @return string|null
    */
    public function getDonorTitle(): ?string {
        return $this->donorTitle;
    }

    /**
     * Gets the dunsNumber property value. The dunsNumber property
     * @return string|null
    */
    public function getDunsNumber(): ?string {
        return $this->dunsNumber;
    }

    /**
     * Gets the eMail property value. The eMail property
     * @return string|null
    */
    public function getEMail(): ?string {
        return $this->eMail;
    }

    /**
     * Gets the eMail2 property value. The eMail2 property
     * @return string|null
    */
    public function getEMail2(): ?string {
        return $this->eMail2;
    }

    /**
     * Gets the eMail3 property value. The eMail3 property
     * @return string|null
    */
    public function getEMail3(): ?string {
        return $this->eMail3;
    }

    /**
     * Gets the eoriNumber property value. The eoriNumber property
     * @return string|null
    */
    public function getEoriNumber(): ?string {
        return $this->eoriNumber;
    }

    /**
     * Gets the facebookAddress property value. The facebookAddress property
     * @return string|null
    */
    public function getFacebookAddress(): ?string {
        return $this->facebookAddress;
    }

    /**
     * Gets the fax property value. The fax property
     * @return string|null
    */
    public function getFax(): ?string {
        return $this->fax;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'accountOnHold' => fn(ParseNode $n) => $o->setAccountOnHold($n->getIntegerValue()),
            'address1' => fn(ParseNode $n) => $o->setAddress1($n->getStringValue()),
            'address2' => fn(ParseNode $n) => $o->setAddress2($n->getStringValue()),
            'address3' => fn(ParseNode $n) => $o->setAddress3($n->getStringValue()),
            'address4' => fn(ParseNode $n) => $o->setAddress4($n->getStringValue()),
            'address5' => fn(ParseNode $n) => $o->setAddress5($n->getStringValue()),
            'analysis1' => fn(ParseNode $n) => $o->setAnalysis1($n->getStringValue()),
            'analysis2' => fn(ParseNode $n) => $o->setAnalysis2($n->getStringValue()),
            'analysis3' => fn(ParseNode $n) => $o->setAnalysis3($n->getStringValue()),
            'bacsRef' => fn(ParseNode $n) => $o->setBacsRef($n->getStringValue()),
            'balance' => fn(ParseNode $n) => $o->setBalance($n->getFloatValue()),
            'bankAccountName' => fn(ParseNode $n) => $o->setBankAccountName($n->getStringValue()),
            'bankAccountNumber' => fn(ParseNode $n) => $o->setBankAccountNumber($n->getStringValue()),
            'bankAdditionalref1' => fn(ParseNode $n) => $o->setBankAdditionalref1($n->getStringValue()),
            'bankAdditionalref2' => fn(ParseNode $n) => $o->setBankAdditionalref2($n->getStringValue()),
            'bankAdditionalref3' => fn(ParseNode $n) => $o->setBankAdditionalref3($n->getStringValue()),
            'bankAddress1' => fn(ParseNode $n) => $o->setBankAddress1($n->getStringValue()),
            'bankAddress2' => fn(ParseNode $n) => $o->setBankAddress2($n->getStringValue()),
            'bankAddress3' => fn(ParseNode $n) => $o->setBankAddress3($n->getStringValue()),
            'bankAddress4' => fn(ParseNode $n) => $o->setBankAddress4($n->getStringValue()),
            'bankAddress5' => fn(ParseNode $n) => $o->setBankAddress5($n->getStringValue()),
            'bankBic' => fn(ParseNode $n) => $o->setBankBic($n->getStringValue()),
            'bankIban' => fn(ParseNode $n) => $o->setBankIban($n->getStringValue()),
            'bankName' => fn(ParseNode $n) => $o->setBankName($n->getStringValue()),
            'bankRollnumber' => fn(ParseNode $n) => $o->setBankRollnumber($n->getStringValue()),
            'bankSortCode' => fn(ParseNode $n) => $o->setBankSortCode($n->getStringValue()),
            'bureauCode' => fn(ParseNode $n) => $o->setBureauCode($n->getIntegerValue()),
            'cAddress1' => fn(ParseNode $n) => $o->setCAddress1($n->getStringValue()),
            'cAddress2' => fn(ParseNode $n) => $o->setCAddress2($n->getStringValue()),
            'cAddress3' => fn(ParseNode $n) => $o->setCAddress3($n->getStringValue()),
            'cAddress4' => fn(ParseNode $n) => $o->setCAddress4($n->getStringValue()),
            'cAddress5' => fn(ParseNode $n) => $o->setCAddress5($n->getStringValue()),
            'canCharge' => fn(ParseNode $n) => $o->setCanCharge($n->getIntegerValue()),
            'contactName' => fn(ParseNode $n) => $o->setContactName($n->getStringValue()),
            'countryCode' => fn(ParseNode $n) => $o->setCountryCode($n->getStringValue()),
            'creditBf' => fn(ParseNode $n) => $o->setCreditBf($n->getFloatValue()),
            'creditCf' => fn(ParseNode $n) => $o->setCreditCf($n->getFloatValue()),
            'creditLimit' => fn(ParseNode $n) => $o->setCreditLimit($n->getFloatValue()),
            'creditMth1' => fn(ParseNode $n) => $o->setCreditMth1($n->getFloatValue()),
            'creditMth10' => fn(ParseNode $n) => $o->setCreditMth10($n->getFloatValue()),
            'creditMth11' => fn(ParseNode $n) => $o->setCreditMth11($n->getFloatValue()),
            'creditMth12' => fn(ParseNode $n) => $o->setCreditMth12($n->getFloatValue()),
            'creditMth2' => fn(ParseNode $n) => $o->setCreditMth2($n->getFloatValue()),
            'creditMth3' => fn(ParseNode $n) => $o->setCreditMth3($n->getFloatValue()),
            'creditMth4' => fn(ParseNode $n) => $o->setCreditMth4($n->getFloatValue()),
            'creditMth5' => fn(ParseNode $n) => $o->setCreditMth5($n->getFloatValue()),
            'creditMth6' => fn(ParseNode $n) => $o->setCreditMth6($n->getFloatValue()),
            'creditMth7' => fn(ParseNode $n) => $o->setCreditMth7($n->getFloatValue()),
            'creditMth8' => fn(ParseNode $n) => $o->setCreditMth8($n->getFloatValue()),
            'creditMth9' => fn(ParseNode $n) => $o->setCreditMth9($n->getFloatValue()),
            'creditPosCode' => fn(ParseNode $n) => $o->setCreditPosCode($n->getIntegerValue()),
            'creditPosition' => fn(ParseNode $n) => $o->setCreditPosition($n->getEnumValue(SupplierAttributesRead_creditPosition::class)),
            'creditRef' => fn(ParseNode $n) => $o->setCreditRef($n->getStringValue()),
            'currency' => fn(ParseNode $n) => $o->setCurrency($n->getIntegerValue()),
            'dateAccountOpened' => fn(ParseNode $n) => $o->setDateAccountOpened($n->getDateTimeValue()),
            'dateCreditApplied' => fn(ParseNode $n) => $o->setDateCreditApplied($n->getDateTimeValue()),
            'dateCreditReceived' => fn(ParseNode $n) => $o->setDateCreditReceived($n->getDateTimeValue()),
            'dateLastCredit' => fn(ParseNode $n) => $o->setDateLastCredit($n->getDateTimeValue()),
            'dateNextCredit' => fn(ParseNode $n) => $o->setDateNextCredit($n->getDateTimeValue()),
            'declarationValidFrom' => fn(ParseNode $n) => $o->setDeclarationValidFrom($n->getDateTimeValue()),
            'defaultFundId' => fn(ParseNode $n) => $o->setDefaultFundId($n->getIntegerValue()),
            'defNomCode' => fn(ParseNode $n) => $o->setDefNomCode($n->getStringValue()),
            'defTaxCode' => fn(ParseNode $n) => $o->setDefTaxCode($n->getEnumValue(SupplierAttributesRead_defTaxCode::class)),
            'delAddress1' => fn(ParseNode $n) => $o->setDelAddress1($n->getStringValue()),
            'delAddress2' => fn(ParseNode $n) => $o->setDelAddress2($n->getStringValue()),
            'delAddress3' => fn(ParseNode $n) => $o->setDelAddress3($n->getStringValue()),
            'delAddress4' => fn(ParseNode $n) => $o->setDelAddress4($n->getStringValue()),
            'delAddress5' => fn(ParseNode $n) => $o->setDelAddress5($n->getStringValue()),
            'delContactName' => fn(ParseNode $n) => $o->setDelContactName($n->getStringValue()),
            'delFax' => fn(ParseNode $n) => $o->setDelFax($n->getStringValue()),
            'delName' => fn(ParseNode $n) => $o->setDelName($n->getStringValue()),
            'delTelephone' => fn(ParseNode $n) => $o->setDelTelephone($n->getStringValue()),
            'deptName' => fn(ParseNode $n) => $o->setDeptName($n->getStringValue()),
            'deptNumber' => fn(ParseNode $n) => $o->setDeptNumber($n->getIntegerValue()),
            'discountRate' => fn(ParseNode $n) => $o->setDiscountRate($n->getFloatValue()),
            'donorForename' => fn(ParseNode $n) => $o->setDonorForename($n->getStringValue()),
            'donorSurname' => fn(ParseNode $n) => $o->setDonorSurname($n->getStringValue()),
            'donorTitle' => fn(ParseNode $n) => $o->setDonorTitle($n->getStringValue()),
            'dunsNumber' => fn(ParseNode $n) => $o->setDunsNumber($n->getStringValue()),
            'eMail' => fn(ParseNode $n) => $o->setEMail($n->getStringValue()),
            'eMail2' => fn(ParseNode $n) => $o->setEMail2($n->getStringValue()),
            'eMail3' => fn(ParseNode $n) => $o->setEMail3($n->getStringValue()),
            'eoriNumber' => fn(ParseNode $n) => $o->setEoriNumber($n->getStringValue()),
            'facebookAddress' => fn(ParseNode $n) => $o->setFacebookAddress($n->getStringValue()),
            'fax' => fn(ParseNode $n) => $o->setFax($n->getStringValue()),
            'firstInvDate' => fn(ParseNode $n) => $o->setFirstInvDate($n->getDateTimeValue()),
            'foreignBalance' => fn(ParseNode $n) => $o->setForeignBalance($n->getFloatValue()),
            'inactiveFlag' => fn(ParseNode $n) => $o->setInactiveFlag($n->getIntegerValue()),
            'incoterms' => fn(ParseNode $n) => $o->setIncoterms($n->getIntegerValue()),
            'incotermsText' => fn(ParseNode $n) => $o->setIncotermsText($n->getStringValue()),
            'invoiceBf' => fn(ParseNode $n) => $o->setInvoiceBf($n->getFloatValue()),
            'invoiceCf' => fn(ParseNode $n) => $o->setInvoiceCf($n->getFloatValue()),
            'invoiceMth1' => fn(ParseNode $n) => $o->setInvoiceMth1($n->getFloatValue()),
            'invoiceMth10' => fn(ParseNode $n) => $o->setInvoiceMth10($n->getFloatValue()),
            'invoiceMth11' => fn(ParseNode $n) => $o->setInvoiceMth11($n->getFloatValue()),
            'invoiceMth12' => fn(ParseNode $n) => $o->setInvoiceMth12($n->getFloatValue()),
            'invoiceMth2' => fn(ParseNode $n) => $o->setInvoiceMth2($n->getFloatValue()),
            'invoiceMth3' => fn(ParseNode $n) => $o->setInvoiceMth3($n->getFloatValue()),
            'invoiceMth4' => fn(ParseNode $n) => $o->setInvoiceMth4($n->getFloatValue()),
            'invoiceMth5' => fn(ParseNode $n) => $o->setInvoiceMth5($n->getFloatValue()),
            'invoiceMth6' => fn(ParseNode $n) => $o->setInvoiceMth6($n->getFloatValue()),
            'invoiceMth7' => fn(ParseNode $n) => $o->setInvoiceMth7($n->getFloatValue()),
            'invoiceMth8' => fn(ParseNode $n) => $o->setInvoiceMth8($n->getFloatValue()),
            'invoiceMth9' => fn(ParseNode $n) => $o->setInvoiceMth9($n->getFloatValue()),
            'lastInvDate' => fn(ParseNode $n) => $o->setLastInvDate($n->getDateTimeValue()),
            'lastPaymentDate' => fn(ParseNode $n) => $o->setLastPaymentDate($n->getDateTimeValue()),
            'lastRefreshedDate' => fn(ParseNode $n) => $o->setLastRefreshedDate($n->getDateTimeValue()),
            'lettersViaEmail' => fn(ParseNode $n) => $o->setLettersViaEmail($n->getIntegerValue()),
            'linkedinAddress' => fn(ParseNode $n) => $o->setLinkedinAddress($n->getStringValue()),
            'mandateid' => fn(ParseNode $n) => $o->setMandateid($n->getStringValue()),
            'name' => fn(ParseNode $n) => $o->setName($n->getStringValue()),
            'overrideTaxCode' => fn(ParseNode $n) => $o->setOverrideTaxCode($n->getIntegerValue()),
            'paymentBf' => fn(ParseNode $n) => $o->setPaymentBf($n->getFloatValue()),
            'paymentCf' => fn(ParseNode $n) => $o->setPaymentCf($n->getFloatValue()),
            'paymentDueDays' => fn(ParseNode $n) => $o->setPaymentDueDays($n->getIntegerValue()),
            'paymentDueFrom' => fn(ParseNode $n) => $o->setPaymentDueFrom($n->getIntegerValue()),
            'paymentDueFromText' => fn(ParseNode $n) => $o->setPaymentDueFromText($n->getStringValue()),
            'paymentMethodId' => fn(ParseNode $n) => $o->setPaymentMethodId($n->getIntegerValue()),
            'paymentMth1' => fn(ParseNode $n) => $o->setPaymentMth1($n->getFloatValue()),
            'paymentMth10' => fn(ParseNode $n) => $o->setPaymentMth10($n->getFloatValue()),
            'paymentMth11' => fn(ParseNode $n) => $o->setPaymentMth11($n->getFloatValue()),
            'paymentMth12' => fn(ParseNode $n) => $o->setPaymentMth12($n->getFloatValue()),
            'paymentMth2' => fn(ParseNode $n) => $o->setPaymentMth2($n->getFloatValue()),
            'paymentMth3' => fn(ParseNode $n) => $o->setPaymentMth3($n->getFloatValue()),
            'paymentMth4' => fn(ParseNode $n) => $o->setPaymentMth4($n->getFloatValue()),
            'paymentMth5' => fn(ParseNode $n) => $o->setPaymentMth5($n->getFloatValue()),
            'paymentMth6' => fn(ParseNode $n) => $o->setPaymentMth6($n->getFloatValue()),
            'paymentMth7' => fn(ParseNode $n) => $o->setPaymentMth7($n->getFloatValue()),
            'paymentMth8' => fn(ParseNode $n) => $o->setPaymentMth8($n->getFloatValue()),
            'paymentMth9' => fn(ParseNode $n) => $o->setPaymentMth9($n->getFloatValue()),
            'priority' => fn(ParseNode $n) => $o->setPriority($n->getIntegerValue()),
            'priorYear' => fn(ParseNode $n) => $o->setPriorYear($n->getFloatValue()),
            'promisedPayment' => fn(ParseNode $n) => $o->setPromisedPayment($n->getFloatValue()),
            'promisedPaymentDate' => fn(ParseNode $n) => $o->setPromisedPaymentDate($n->getDateTimeValue()),
            'recordCreateDate' => fn(ParseNode $n) => $o->setRecordCreateDate($n->getDateTimeValue()),
            'recordDeleted' => fn(ParseNode $n) => $o->setRecordDeleted($n->getIntegerValue()),
            'recordModifyDate' => fn(ParseNode $n) => $o->setRecordModifyDate($n->getDateTimeValue()),
            'reportPassword' => fn(ParseNode $n) => $o->setReportPassword($n->getStringValue()),
            'restrictMail' => fn(ParseNode $n) => $o->setRestrictMail($n->getIntegerValue()),
            'settlementDiscRate' => fn(ParseNode $n) => $o->setSettlementDiscRate($n->getFloatValue()),
            'settlementDueDays' => fn(ParseNode $n) => $o->setSettlementDueDays($n->getIntegerValue()),
            'status' => fn(ParseNode $n) => $o->setStatus($n->getIntegerValue()),
            'statusDescription' => fn(ParseNode $n) => $o->setStatusDescription($n->getStringValue()),
            'statusNumber' => fn(ParseNode $n) => $o->setStatusNumber($n->getIntegerValue()),
            'statusText' => fn(ParseNode $n) => $o->setStatusText($n->getStringValue()),
            'telephone' => fn(ParseNode $n) => $o->setTelephone($n->getStringValue()),
            'telephone2' => fn(ParseNode $n) => $o->setTelephone2($n->getStringValue()),
            'terms' => fn(ParseNode $n) => $o->setTerms($n->getStringValue()),
            'termsAgreed' => fn(ParseNode $n) => $o->setTermsAgreed($n->getIntegerValue()),
            'tradeContact' => fn(ParseNode $n) => $o->setTradeContact($n->getStringValue()),
            'turnoverMtd' => fn(ParseNode $n) => $o->setTurnoverMtd($n->getFloatValue()),
            'turnoverYtd' => fn(ParseNode $n) => $o->setTurnoverYtd($n->getFloatValue()),
            'twitterAddress' => fn(ParseNode $n) => $o->setTwitterAddress($n->getStringValue()),
            'useBacs' => fn(ParseNode $n) => $o->setUseBacs($n->getIntegerValue()),
            'useBsoc' => fn(ParseNode $n) => $o->setUseBsoc($n->getIntegerValue()),
            'vatRegNumber' => fn(ParseNode $n) => $o->setVatRegNumber($n->getStringValue()),
            'webAddress' => fn(ParseNode $n) => $o->setWebAddress($n->getStringValue()),
        ];
    }

    /**
     * Gets the firstInvDate property value. The firstInvDate property
     * @return DateTime|null
    */
    public function getFirstInvDate(): ?DateTime {
        return $this->firstInvDate;
    }

    /**
     * Gets the foreignBalance property value. The foreignBalance property
     * @return float|null
    */
    public function getForeignBalance(): ?float {
        return $this->foreignBalance;
    }

    /**
     * Gets the inactiveFlag property value. The inactiveFlag property
     * @return int|null
    */
    public function getInactiveFlag(): ?int {
        return $this->inactiveFlag;
    }

    /**
     * Gets the incoterms property value. The incoterms property
     * @return int|null
    */
    public function getIncoterms(): ?int {
        return $this->incoterms;
    }

    /**
     * Gets the incotermsText property value. The incotermsText property
     * @return string|null
    */
    public function getIncotermsText(): ?string {
        return $this->incotermsText;
    }

    /**
     * Gets the invoiceBf property value. The invoiceBf property
     * @return float|null
    */
    public function getInvoiceBf(): ?float {
        return $this->invoiceBf;
    }

    /**
     * Gets the invoiceCf property value. The invoiceCf property
     * @return float|null
    */
    public function getInvoiceCf(): ?float {
        return $this->invoiceCf;
    }

    /**
     * Gets the invoiceMth1 property value. The invoiceMth1 property
     * @return float|null
    */
    public function getInvoiceMth1(): ?float {
        return $this->invoiceMth1;
    }

    /**
     * Gets the invoiceMth10 property value. The invoiceMth10 property
     * @return float|null
    */
    public function getInvoiceMth10(): ?float {
        return $this->invoiceMth10;
    }

    /**
     * Gets the invoiceMth11 property value. The invoiceMth11 property
     * @return float|null
    */
    public function getInvoiceMth11(): ?float {
        return $this->invoiceMth11;
    }

    /**
     * Gets the invoiceMth12 property value. The invoiceMth12 property
     * @return float|null
    */
    public function getInvoiceMth12(): ?float {
        return $this->invoiceMth12;
    }

    /**
     * Gets the invoiceMth2 property value. The invoiceMth2 property
     * @return float|null
    */
    public function getInvoiceMth2(): ?float {
        return $this->invoiceMth2;
    }

    /**
     * Gets the invoiceMth3 property value. The invoiceMth3 property
     * @return float|null
    */
    public function getInvoiceMth3(): ?float {
        return $this->invoiceMth3;
    }

    /**
     * Gets the invoiceMth4 property value. The invoiceMth4 property
     * @return float|null
    */
    public function getInvoiceMth4(): ?float {
        return $this->invoiceMth4;
    }

    /**
     * Gets the invoiceMth5 property value. The invoiceMth5 property
     * @return float|null
    */
    public function getInvoiceMth5(): ?float {
        return $this->invoiceMth5;
    }

    /**
     * Gets the invoiceMth6 property value. The invoiceMth6 property
     * @return float|null
    */
    public function getInvoiceMth6(): ?float {
        return $this->invoiceMth6;
    }

    /**
     * Gets the invoiceMth7 property value. The invoiceMth7 property
     * @return float|null
    */
    public function getInvoiceMth7(): ?float {
        return $this->invoiceMth7;
    }

    /**
     * Gets the invoiceMth8 property value. The invoiceMth8 property
     * @return float|null
    */
    public function getInvoiceMth8(): ?float {
        return $this->invoiceMth8;
    }

    /**
     * Gets the invoiceMth9 property value. The invoiceMth9 property
     * @return float|null
    */
    public function getInvoiceMth9(): ?float {
        return $this->invoiceMth9;
    }

    /**
     * Gets the lastInvDate property value. The lastInvDate property
     * @return DateTime|null
    */
    public function getLastInvDate(): ?DateTime {
        return $this->lastInvDate;
    }

    /**
     * Gets the lastPaymentDate property value. The lastPaymentDate property
     * @return DateTime|null
    */
    public function getLastPaymentDate(): ?DateTime {
        return $this->lastPaymentDate;
    }

    /**
     * Gets the lastRefreshedDate property value. The lastRefreshedDate property
     * @return DateTime|null
    */
    public function getLastRefreshedDate(): ?DateTime {
        return $this->lastRefreshedDate;
    }

    /**
     * Gets the lettersViaEmail property value. The lettersViaEmail property
     * @return int|null
    */
    public function getLettersViaEmail(): ?int {
        return $this->lettersViaEmail;
    }

    /**
     * Gets the linkedinAddress property value. The linkedinAddress property
     * @return string|null
    */
    public function getLinkedinAddress(): ?string {
        return $this->linkedinAddress;
    }

    /**
     * Gets the mandateid property value. The mandateid property
     * @return string|null
    */
    public function getMandateid(): ?string {
        return $this->mandateid;
    }

    /**
     * Gets the name property value. The name property
     * @return string|null
    */
    public function getName(): ?string {
        return $this->name;
    }

    /**
     * Gets the overrideTaxCode property value. The overrideTaxCode property
     * @return int|null
    */
    public function getOverrideTaxCode(): ?int {
        return $this->overrideTaxCode;
    }

    /**
     * Gets the paymentBf property value. The paymentBf property
     * @return float|null
    */
    public function getPaymentBf(): ?float {
        return $this->paymentBf;
    }

    /**
     * Gets the paymentCf property value. The paymentCf property
     * @return float|null
    */
    public function getPaymentCf(): ?float {
        return $this->paymentCf;
    }

    /**
     * Gets the paymentDueDays property value. The paymentDueDays property
     * @return int|null
    */
    public function getPaymentDueDays(): ?int {
        return $this->paymentDueDays;
    }

    /**
     * Gets the paymentDueFrom property value. The paymentDueFrom property
     * @return int|null
    */
    public function getPaymentDueFrom(): ?int {
        return $this->paymentDueFrom;
    }

    /**
     * Gets the paymentDueFromText property value. The paymentDueFromText property
     * @return string|null
    */
    public function getPaymentDueFromText(): ?string {
        return $this->paymentDueFromText;
    }

    /**
     * Gets the paymentMethodId property value. The paymentMethodId property
     * @return int|null
    */
    public function getPaymentMethodId(): ?int {
        return $this->paymentMethodId;
    }

    /**
     * Gets the paymentMth1 property value. The paymentMth1 property
     * @return float|null
    */
    public function getPaymentMth1(): ?float {
        return $this->paymentMth1;
    }

    /**
     * Gets the paymentMth10 property value. The paymentMth10 property
     * @return float|null
    */
    public function getPaymentMth10(): ?float {
        return $this->paymentMth10;
    }

    /**
     * Gets the paymentMth11 property value. The paymentMth11 property
     * @return float|null
    */
    public function getPaymentMth11(): ?float {
        return $this->paymentMth11;
    }

    /**
     * Gets the paymentMth12 property value. The paymentMth12 property
     * @return float|null
    */
    public function getPaymentMth12(): ?float {
        return $this->paymentMth12;
    }

    /**
     * Gets the paymentMth2 property value. The paymentMth2 property
     * @return float|null
    */
    public function getPaymentMth2(): ?float {
        return $this->paymentMth2;
    }

    /**
     * Gets the paymentMth3 property value. The paymentMth3 property
     * @return float|null
    */
    public function getPaymentMth3(): ?float {
        return $this->paymentMth3;
    }

    /**
     * Gets the paymentMth4 property value. The paymentMth4 property
     * @return float|null
    */
    public function getPaymentMth4(): ?float {
        return $this->paymentMth4;
    }

    /**
     * Gets the paymentMth5 property value. The paymentMth5 property
     * @return float|null
    */
    public function getPaymentMth5(): ?float {
        return $this->paymentMth5;
    }

    /**
     * Gets the paymentMth6 property value. The paymentMth6 property
     * @return float|null
    */
    public function getPaymentMth6(): ?float {
        return $this->paymentMth6;
    }

    /**
     * Gets the paymentMth7 property value. The paymentMth7 property
     * @return float|null
    */
    public function getPaymentMth7(): ?float {
        return $this->paymentMth7;
    }

    /**
     * Gets the paymentMth8 property value. The paymentMth8 property
     * @return float|null
    */
    public function getPaymentMth8(): ?float {
        return $this->paymentMth8;
    }

    /**
     * Gets the paymentMth9 property value. The paymentMth9 property
     * @return float|null
    */
    public function getPaymentMth9(): ?float {
        return $this->paymentMth9;
    }

    /**
     * Gets the priority property value. The priority property
     * @return int|null
    */
    public function getPriority(): ?int {
        return $this->priority;
    }

    /**
     * Gets the priorYear property value. The priorYear property
     * @return float|null
    */
    public function getPriorYear(): ?float {
        return $this->priorYear;
    }

    /**
     * Gets the promisedPayment property value. The promisedPayment property
     * @return float|null
    */
    public function getPromisedPayment(): ?float {
        return $this->promisedPayment;
    }

    /**
     * Gets the promisedPaymentDate property value. The promisedPaymentDate property
     * @return DateTime|null
    */
    public function getPromisedPaymentDate(): ?DateTime {
        return $this->promisedPaymentDate;
    }

    /**
     * Gets the recordCreateDate property value. The recordCreateDate property
     * @return DateTime|null
    */
    public function getRecordCreateDate(): ?DateTime {
        return $this->recordCreateDate;
    }

    /**
     * Gets the recordDeleted property value. The recordDeleted property
     * @return int|null
    */
    public function getRecordDeleted(): ?int {
        return $this->recordDeleted;
    }

    /**
     * Gets the recordModifyDate property value. The recordModifyDate property
     * @return DateTime|null
    */
    public function getRecordModifyDate(): ?DateTime {
        return $this->recordModifyDate;
    }

    /**
     * Gets the reportPassword property value. The reportPassword property
     * @return string|null
    */
    public function getReportPassword(): ?string {
        return $this->reportPassword;
    }

    /**
     * Gets the restrictMail property value. The restrictMail property
     * @return int|null
    */
    public function getRestrictMail(): ?int {
        return $this->restrictMail;
    }

    /**
     * Gets the settlementDiscRate property value. The settlementDiscRate property
     * @return float|null
    */
    public function getSettlementDiscRate(): ?float {
        return $this->settlementDiscRate;
    }

    /**
     * Gets the settlementDueDays property value. The settlementDueDays property
     * @return int|null
    */
    public function getSettlementDueDays(): ?int {
        return $this->settlementDueDays;
    }

    /**
     * Gets the status property value. The status property
     * @return int|null
    */
    public function getStatus(): ?int {
        return $this->status;
    }

    /**
     * Gets the statusDescription property value. The statusDescription property
     * @return string|null
    */
    public function getStatusDescription(): ?string {
        return $this->statusDescription;
    }

    /**
     * Gets the statusNumber property value. The statusNumber property
     * @return int|null
    */
    public function getStatusNumber(): ?int {
        return $this->statusNumber;
    }

    /**
     * Gets the statusText property value. The statusText property
     * @return string|null
    */
    public function getStatusText(): ?string {
        return $this->statusText;
    }

    /**
     * Gets the telephone property value. The telephone property
     * @return string|null
    */
    public function getTelephone(): ?string {
        return $this->telephone;
    }

    /**
     * Gets the telephone2 property value. The telephone2 property
     * @return string|null
    */
    public function getTelephone2(): ?string {
        return $this->telephone2;
    }

    /**
     * Gets the terms property value. The terms property
     * @return string|null
    */
    public function getTerms(): ?string {
        return $this->terms;
    }

    /**
     * Gets the termsAgreed property value. The termsAgreed property
     * @return int|null
    */
    public function getTermsAgreed(): ?int {
        return $this->termsAgreed;
    }

    /**
     * Gets the tradeContact property value. The tradeContact property
     * @return string|null
    */
    public function getTradeContact(): ?string {
        return $this->tradeContact;
    }

    /**
     * Gets the turnoverMtd property value. The turnoverMtd property
     * @return float|null
    */
    public function getTurnoverMtd(): ?float {
        return $this->turnoverMtd;
    }

    /**
     * Gets the turnoverYtd property value. The turnoverYtd property
     * @return float|null
    */
    public function getTurnoverYtd(): ?float {
        return $this->turnoverYtd;
    }

    /**
     * Gets the twitterAddress property value. The twitterAddress property
     * @return string|null
    */
    public function getTwitterAddress(): ?string {
        return $this->twitterAddress;
    }

    /**
     * Gets the useBacs property value. The useBacs property
     * @return int|null
    */
    public function getUseBacs(): ?int {
        return $this->useBacs;
    }

    /**
     * Gets the useBsoc property value. The useBsoc property
     * @return int|null
    */
    public function getUseBsoc(): ?int {
        return $this->useBsoc;
    }

    /**
     * Gets the vatRegNumber property value. The vatRegNumber property
     * @return string|null
    */
    public function getVatRegNumber(): ?string {
        return $this->vatRegNumber;
    }

    /**
     * Gets the webAddress property value. The webAddress property
     * @return string|null
    */
    public function getWebAddress(): ?string {
        return $this->webAddress;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeIntegerValue('accountOnHold', $this->getAccountOnHold());
        $writer->writeStringValue('address1', $this->getAddress1());
        $writer->writeStringValue('address2', $this->getAddress2());
        $writer->writeStringValue('address3', $this->getAddress3());
        $writer->writeStringValue('address4', $this->getAddress4());
        $writer->writeStringValue('address5', $this->getAddress5());
        $writer->writeStringValue('analysis1', $this->getAnalysis1());
        $writer->writeStringValue('analysis2', $this->getAnalysis2());
        $writer->writeStringValue('analysis3', $this->getAnalysis3());
        $writer->writeStringValue('bacsRef', $this->getBacsRef());
        $writer->writeFloatValue('balance', $this->getBalance());
        $writer->writeStringValue('bankAccountName', $this->getBankAccountName());
        $writer->writeStringValue('bankAccountNumber', $this->getBankAccountNumber());
        $writer->writeStringValue('bankAdditionalref1', $this->getBankAdditionalref1());
        $writer->writeStringValue('bankAdditionalref2', $this->getBankAdditionalref2());
        $writer->writeStringValue('bankAdditionalref3', $this->getBankAdditionalref3());
        $writer->writeStringValue('bankAddress1', $this->getBankAddress1());
        $writer->writeStringValue('bankAddress2', $this->getBankAddress2());
        $writer->writeStringValue('bankAddress3', $this->getBankAddress3());
        $writer->writeStringValue('bankAddress4', $this->getBankAddress4());
        $writer->writeStringValue('bankAddress5', $this->getBankAddress5());
        $writer->writeStringValue('bankBic', $this->getBankBic());
        $writer->writeStringValue('bankIban', $this->getBankIban());
        $writer->writeStringValue('bankName', $this->getBankName());
        $writer->writeStringValue('bankRollnumber', $this->getBankRollnumber());
        $writer->writeStringValue('bankSortCode', $this->getBankSortCode());
        $writer->writeIntegerValue('bureauCode', $this->getBureauCode());
        $writer->writeStringValue('cAddress1', $this->getCAddress1());
        $writer->writeStringValue('cAddress2', $this->getCAddress2());
        $writer->writeStringValue('cAddress3', $this->getCAddress3());
        $writer->writeStringValue('cAddress4', $this->getCAddress4());
        $writer->writeStringValue('cAddress5', $this->getCAddress5());
        $writer->writeIntegerValue('canCharge', $this->getCanCharge());
        $writer->writeStringValue('contactName', $this->getContactName());
        $writer->writeStringValue('countryCode', $this->getCountryCode());
        $writer->writeFloatValue('creditBf', $this->getCreditBf());
        $writer->writeFloatValue('creditCf', $this->getCreditCf());
        $writer->writeFloatValue('creditLimit', $this->getCreditLimit());
        $writer->writeFloatValue('creditMth1', $this->getCreditMth1());
        $writer->writeFloatValue('creditMth10', $this->getCreditMth10());
        $writer->writeFloatValue('creditMth11', $this->getCreditMth11());
        $writer->writeFloatValue('creditMth12', $this->getCreditMth12());
        $writer->writeFloatValue('creditMth2', $this->getCreditMth2());
        $writer->writeFloatValue('creditMth3', $this->getCreditMth3());
        $writer->writeFloatValue('creditMth4', $this->getCreditMth4());
        $writer->writeFloatValue('creditMth5', $this->getCreditMth5());
        $writer->writeFloatValue('creditMth6', $this->getCreditMth6());
        $writer->writeFloatValue('creditMth7', $this->getCreditMth7());
        $writer->writeFloatValue('creditMth8', $this->getCreditMth8());
        $writer->writeFloatValue('creditMth9', $this->getCreditMth9());
        $writer->writeIntegerValue('creditPosCode', $this->getCreditPosCode());
        $writer->writeEnumValue('creditPosition', $this->getCreditPosition());
        $writer->writeStringValue('creditRef', $this->getCreditRef());
        $writer->writeIntegerValue('currency', $this->getCurrency());
        $writer->writeDateTimeValue('dateAccountOpened', $this->getDateAccountOpened());
        $writer->writeDateTimeValue('dateCreditApplied', $this->getDateCreditApplied());
        $writer->writeDateTimeValue('dateCreditReceived', $this->getDateCreditReceived());
        $writer->writeDateTimeValue('dateLastCredit', $this->getDateLastCredit());
        $writer->writeDateTimeValue('dateNextCredit', $this->getDateNextCredit());
        $writer->writeDateTimeValue('declarationValidFrom', $this->getDeclarationValidFrom());
        $writer->writeIntegerValue('defaultFundId', $this->getDefaultFundId());
        $writer->writeStringValue('defNomCode', $this->getDefNomCode());
        $writer->writeEnumValue('defTaxCode', $this->getDefTaxCode());
        $writer->writeStringValue('delAddress1', $this->getDelAddress1());
        $writer->writeStringValue('delAddress2', $this->getDelAddress2());
        $writer->writeStringValue('delAddress3', $this->getDelAddress3());
        $writer->writeStringValue('delAddress4', $this->getDelAddress4());
        $writer->writeStringValue('delAddress5', $this->getDelAddress5());
        $writer->writeStringValue('delContactName', $this->getDelContactName());
        $writer->writeStringValue('delFax', $this->getDelFax());
        $writer->writeStringValue('delName', $this->getDelName());
        $writer->writeStringValue('delTelephone', $this->getDelTelephone());
        $writer->writeStringValue('deptName', $this->getDeptName());
        $writer->writeIntegerValue('deptNumber', $this->getDeptNumber());
        $writer->writeFloatValue('discountRate', $this->getDiscountRate());
        $writer->writeStringValue('donorForename', $this->getDonorForename());
        $writer->writeStringValue('donorSurname', $this->getDonorSurname());
        $writer->writeStringValue('donorTitle', $this->getDonorTitle());
        $writer->writeStringValue('dunsNumber', $this->getDunsNumber());
        $writer->writeStringValue('eMail', $this->getEMail());
        $writer->writeStringValue('eMail2', $this->getEMail2());
        $writer->writeStringValue('eMail3', $this->getEMail3());
        $writer->writeStringValue('eoriNumber', $this->getEoriNumber());
        $writer->writeStringValue('facebookAddress', $this->getFacebookAddress());
        $writer->writeStringValue('fax', $this->getFax());
        $writer->writeDateTimeValue('firstInvDate', $this->getFirstInvDate());
        $writer->writeFloatValue('foreignBalance', $this->getForeignBalance());
        $writer->writeIntegerValue('inactiveFlag', $this->getInactiveFlag());
        $writer->writeIntegerValue('incoterms', $this->getIncoterms());
        $writer->writeStringValue('incotermsText', $this->getIncotermsText());
        $writer->writeFloatValue('invoiceBf', $this->getInvoiceBf());
        $writer->writeFloatValue('invoiceCf', $this->getInvoiceCf());
        $writer->writeFloatValue('invoiceMth1', $this->getInvoiceMth1());
        $writer->writeFloatValue('invoiceMth10', $this->getInvoiceMth10());
        $writer->writeFloatValue('invoiceMth11', $this->getInvoiceMth11());
        $writer->writeFloatValue('invoiceMth12', $this->getInvoiceMth12());
        $writer->writeFloatValue('invoiceMth2', $this->getInvoiceMth2());
        $writer->writeFloatValue('invoiceMth3', $this->getInvoiceMth3());
        $writer->writeFloatValue('invoiceMth4', $this->getInvoiceMth4());
        $writer->writeFloatValue('invoiceMth5', $this->getInvoiceMth5());
        $writer->writeFloatValue('invoiceMth6', $this->getInvoiceMth6());
        $writer->writeFloatValue('invoiceMth7', $this->getInvoiceMth7());
        $writer->writeFloatValue('invoiceMth8', $this->getInvoiceMth8());
        $writer->writeFloatValue('invoiceMth9', $this->getInvoiceMth9());
        $writer->writeDateTimeValue('lastInvDate', $this->getLastInvDate());
        $writer->writeDateTimeValue('lastPaymentDate', $this->getLastPaymentDate());
        $writer->writeDateTimeValue('lastRefreshedDate', $this->getLastRefreshedDate());
        $writer->writeIntegerValue('lettersViaEmail', $this->getLettersViaEmail());
        $writer->writeStringValue('linkedinAddress', $this->getLinkedinAddress());
        $writer->writeStringValue('mandateid', $this->getMandateid());
        $writer->writeStringValue('name', $this->getName());
        $writer->writeIntegerValue('overrideTaxCode', $this->getOverrideTaxCode());
        $writer->writeFloatValue('paymentBf', $this->getPaymentBf());
        $writer->writeFloatValue('paymentCf', $this->getPaymentCf());
        $writer->writeIntegerValue('paymentDueDays', $this->getPaymentDueDays());
        $writer->writeIntegerValue('paymentDueFrom', $this->getPaymentDueFrom());
        $writer->writeStringValue('paymentDueFromText', $this->getPaymentDueFromText());
        $writer->writeIntegerValue('paymentMethodId', $this->getPaymentMethodId());
        $writer->writeFloatValue('paymentMth1', $this->getPaymentMth1());
        $writer->writeFloatValue('paymentMth10', $this->getPaymentMth10());
        $writer->writeFloatValue('paymentMth11', $this->getPaymentMth11());
        $writer->writeFloatValue('paymentMth12', $this->getPaymentMth12());
        $writer->writeFloatValue('paymentMth2', $this->getPaymentMth2());
        $writer->writeFloatValue('paymentMth3', $this->getPaymentMth3());
        $writer->writeFloatValue('paymentMth4', $this->getPaymentMth4());
        $writer->writeFloatValue('paymentMth5', $this->getPaymentMth5());
        $writer->writeFloatValue('paymentMth6', $this->getPaymentMth6());
        $writer->writeFloatValue('paymentMth7', $this->getPaymentMth7());
        $writer->writeFloatValue('paymentMth8', $this->getPaymentMth8());
        $writer->writeFloatValue('paymentMth9', $this->getPaymentMth9());
        $writer->writeIntegerValue('priority', $this->getPriority());
        $writer->writeFloatValue('priorYear', $this->getPriorYear());
        $writer->writeFloatValue('promisedPayment', $this->getPromisedPayment());
        $writer->writeDateTimeValue('promisedPaymentDate', $this->getPromisedPaymentDate());
        $writer->writeDateTimeValue('recordCreateDate', $this->getRecordCreateDate());
        $writer->writeIntegerValue('recordDeleted', $this->getRecordDeleted());
        $writer->writeDateTimeValue('recordModifyDate', $this->getRecordModifyDate());
        $writer->writeStringValue('reportPassword', $this->getReportPassword());
        $writer->writeIntegerValue('restrictMail', $this->getRestrictMail());
        $writer->writeFloatValue('settlementDiscRate', $this->getSettlementDiscRate());
        $writer->writeIntegerValue('settlementDueDays', $this->getSettlementDueDays());
        $writer->writeIntegerValue('status', $this->getStatus());
        $writer->writeStringValue('statusDescription', $this->getStatusDescription());
        $writer->writeIntegerValue('statusNumber', $this->getStatusNumber());
        $writer->writeStringValue('statusText', $this->getStatusText());
        $writer->writeStringValue('telephone', $this->getTelephone());
        $writer->writeStringValue('telephone2', $this->getTelephone2());
        $writer->writeStringValue('terms', $this->getTerms());
        $writer->writeIntegerValue('termsAgreed', $this->getTermsAgreed());
        $writer->writeStringValue('tradeContact', $this->getTradeContact());
        $writer->writeFloatValue('turnoverMtd', $this->getTurnoverMtd());
        $writer->writeFloatValue('turnoverYtd', $this->getTurnoverYtd());
        $writer->writeStringValue('twitterAddress', $this->getTwitterAddress());
        $writer->writeIntegerValue('useBacs', $this->getUseBacs());
        $writer->writeIntegerValue('useBsoc', $this->getUseBsoc());
        $writer->writeStringValue('vatRegNumber', $this->getVatRegNumber());
        $writer->writeStringValue('webAddress', $this->getWebAddress());
    }

    /**
     * Sets the accountOnHold property value. The accountOnHold property
     * @param int|null $value Value to set for the accountOnHold property.
    */
    public function setAccountOnHold(?int $value): void {
        $this->accountOnHold = $value;
    }

    /**
     * Sets the address1 property value. The address1 property
     * @param string|null $value Value to set for the address1 property.
    */
    public function setAddress1(?string $value): void {
        $this->address1 = $value;
    }

    /**
     * Sets the address2 property value. The address2 property
     * @param string|null $value Value to set for the address2 property.
    */
    public function setAddress2(?string $value): void {
        $this->address2 = $value;
    }

    /**
     * Sets the address3 property value. The address3 property
     * @param string|null $value Value to set for the address3 property.
    */
    public function setAddress3(?string $value): void {
        $this->address3 = $value;
    }

    /**
     * Sets the address4 property value. The address4 property
     * @param string|null $value Value to set for the address4 property.
    */
    public function setAddress4(?string $value): void {
        $this->address4 = $value;
    }

    /**
     * Sets the address5 property value. The address5 property
     * @param string|null $value Value to set for the address5 property.
    */
    public function setAddress5(?string $value): void {
        $this->address5 = $value;
    }

    /**
     * Sets the analysis1 property value. The analysis1 property
     * @param string|null $value Value to set for the analysis1 property.
    */
    public function setAnalysis1(?string $value): void {
        $this->analysis1 = $value;
    }

    /**
     * Sets the analysis2 property value. The analysis2 property
     * @param string|null $value Value to set for the analysis2 property.
    */
    public function setAnalysis2(?string $value): void {
        $this->analysis2 = $value;
    }

    /**
     * Sets the analysis3 property value. The analysis3 property
     * @param string|null $value Value to set for the analysis3 property.
    */
    public function setAnalysis3(?string $value): void {
        $this->analysis3 = $value;
    }

    /**
     * Sets the bacsRef property value. The bacsRef property
     * @param string|null $value Value to set for the bacsRef property.
    */
    public function setBacsRef(?string $value): void {
        $this->bacsRef = $value;
    }

    /**
     * Sets the balance property value. The balance property
     * @param float|null $value Value to set for the balance property.
    */
    public function setBalance(?float $value): void {
        $this->balance = $value;
    }

    /**
     * Sets the bankAccountName property value. The bankAccountName property
     * @param string|null $value Value to set for the bankAccountName property.
    */
    public function setBankAccountName(?string $value): void {
        $this->bankAccountName = $value;
    }

    /**
     * Sets the bankAccountNumber property value. The bankAccountNumber property
     * @param string|null $value Value to set for the bankAccountNumber property.
    */
    public function setBankAccountNumber(?string $value): void {
        $this->bankAccountNumber = $value;
    }

    /**
     * Sets the bankAdditionalref1 property value. The bankAdditionalref1 property
     * @param string|null $value Value to set for the bankAdditionalref1 property.
    */
    public function setBankAdditionalref1(?string $value): void {
        $this->bankAdditionalref1 = $value;
    }

    /**
     * Sets the bankAdditionalref2 property value. The bankAdditionalref2 property
     * @param string|null $value Value to set for the bankAdditionalref2 property.
    */
    public function setBankAdditionalref2(?string $value): void {
        $this->bankAdditionalref2 = $value;
    }

    /**
     * Sets the bankAdditionalref3 property value. The bankAdditionalref3 property
     * @param string|null $value Value to set for the bankAdditionalref3 property.
    */
    public function setBankAdditionalref3(?string $value): void {
        $this->bankAdditionalref3 = $value;
    }

    /**
     * Sets the bankAddress1 property value. The bankAddress1 property
     * @param string|null $value Value to set for the bankAddress1 property.
    */
    public function setBankAddress1(?string $value): void {
        $this->bankAddress1 = $value;
    }

    /**
     * Sets the bankAddress2 property value. The bankAddress2 property
     * @param string|null $value Value to set for the bankAddress2 property.
    */
    public function setBankAddress2(?string $value): void {
        $this->bankAddress2 = $value;
    }

    /**
     * Sets the bankAddress3 property value. The bankAddress3 property
     * @param string|null $value Value to set for the bankAddress3 property.
    */
    public function setBankAddress3(?string $value): void {
        $this->bankAddress3 = $value;
    }

    /**
     * Sets the bankAddress4 property value. The bankAddress4 property
     * @param string|null $value Value to set for the bankAddress4 property.
    */
    public function setBankAddress4(?string $value): void {
        $this->bankAddress4 = $value;
    }

    /**
     * Sets the bankAddress5 property value. The bankAddress5 property
     * @param string|null $value Value to set for the bankAddress5 property.
    */
    public function setBankAddress5(?string $value): void {
        $this->bankAddress5 = $value;
    }

    /**
     * Sets the bankBic property value. The bankBic property
     * @param string|null $value Value to set for the bankBic property.
    */
    public function setBankBic(?string $value): void {
        $this->bankBic = $value;
    }

    /**
     * Sets the bankIban property value. The bankIban property
     * @param string|null $value Value to set for the bankIban property.
    */
    public function setBankIban(?string $value): void {
        $this->bankIban = $value;
    }

    /**
     * Sets the bankName property value. The bankName property
     * @param string|null $value Value to set for the bankName property.
    */
    public function setBankName(?string $value): void {
        $this->bankName = $value;
    }

    /**
     * Sets the bankRollnumber property value. The bankRollnumber property
     * @param string|null $value Value to set for the bankRollnumber property.
    */
    public function setBankRollnumber(?string $value): void {
        $this->bankRollnumber = $value;
    }

    /**
     * Sets the bankSortCode property value. The bankSortCode property
     * @param string|null $value Value to set for the bankSortCode property.
    */
    public function setBankSortCode(?string $value): void {
        $this->bankSortCode = $value;
    }

    /**
     * Sets the bureauCode property value. The bureauCode property
     * @param int|null $value Value to set for the bureauCode property.
    */
    public function setBureauCode(?int $value): void {
        $this->bureauCode = $value;
    }

    /**
     * Sets the cAddress1 property value. The cAddress1 property
     * @param string|null $value Value to set for the cAddress1 property.
    */
    public function setCAddress1(?string $value): void {
        $this->cAddress1 = $value;
    }

    /**
     * Sets the cAddress2 property value. The cAddress2 property
     * @param string|null $value Value to set for the cAddress2 property.
    */
    public function setCAddress2(?string $value): void {
        $this->cAddress2 = $value;
    }

    /**
     * Sets the cAddress3 property value. The cAddress3 property
     * @param string|null $value Value to set for the cAddress3 property.
    */
    public function setCAddress3(?string $value): void {
        $this->cAddress3 = $value;
    }

    /**
     * Sets the cAddress4 property value. The cAddress4 property
     * @param string|null $value Value to set for the cAddress4 property.
    */
    public function setCAddress4(?string $value): void {
        $this->cAddress4 = $value;
    }

    /**
     * Sets the cAddress5 property value. The cAddress5 property
     * @param string|null $value Value to set for the cAddress5 property.
    */
    public function setCAddress5(?string $value): void {
        $this->cAddress5 = $value;
    }

    /**
     * Sets the canCharge property value. The canCharge property
     * @param int|null $value Value to set for the canCharge property.
    */
    public function setCanCharge(?int $value): void {
        $this->canCharge = $value;
    }

    /**
     * Sets the contactName property value. The contactName property
     * @param string|null $value Value to set for the contactName property.
    */
    public function setContactName(?string $value): void {
        $this->contactName = $value;
    }

    /**
     * Sets the countryCode property value. The countryCode property
     * @param string|null $value Value to set for the countryCode property.
    */
    public function setCountryCode(?string $value): void {
        $this->countryCode = $value;
    }

    /**
     * Sets the creditBf property value. The creditBf property
     * @param float|null $value Value to set for the creditBf property.
    */
    public function setCreditBf(?float $value): void {
        $this->creditBf = $value;
    }

    /**
     * Sets the creditCf property value. The creditCf property
     * @param float|null $value Value to set for the creditCf property.
    */
    public function setCreditCf(?float $value): void {
        $this->creditCf = $value;
    }

    /**
     * Sets the creditLimit property value. The creditLimit property
     * @param float|null $value Value to set for the creditLimit property.
    */
    public function setCreditLimit(?float $value): void {
        $this->creditLimit = $value;
    }

    /**
     * Sets the creditMth1 property value. The creditMth1 property
     * @param float|null $value Value to set for the creditMth1 property.
    */
    public function setCreditMth1(?float $value): void {
        $this->creditMth1 = $value;
    }

    /**
     * Sets the creditMth10 property value. The creditMth10 property
     * @param float|null $value Value to set for the creditMth10 property.
    */
    public function setCreditMth10(?float $value): void {
        $this->creditMth10 = $value;
    }

    /**
     * Sets the creditMth11 property value. The creditMth11 property
     * @param float|null $value Value to set for the creditMth11 property.
    */
    public function setCreditMth11(?float $value): void {
        $this->creditMth11 = $value;
    }

    /**
     * Sets the creditMth12 property value. The creditMth12 property
     * @param float|null $value Value to set for the creditMth12 property.
    */
    public function setCreditMth12(?float $value): void {
        $this->creditMth12 = $value;
    }

    /**
     * Sets the creditMth2 property value. The creditMth2 property
     * @param float|null $value Value to set for the creditMth2 property.
    */
    public function setCreditMth2(?float $value): void {
        $this->creditMth2 = $value;
    }

    /**
     * Sets the creditMth3 property value. The creditMth3 property
     * @param float|null $value Value to set for the creditMth3 property.
    */
    public function setCreditMth3(?float $value): void {
        $this->creditMth3 = $value;
    }

    /**
     * Sets the creditMth4 property value. The creditMth4 property
     * @param float|null $value Value to set for the creditMth4 property.
    */
    public function setCreditMth4(?float $value): void {
        $this->creditMth4 = $value;
    }

    /**
     * Sets the creditMth5 property value. The creditMth5 property
     * @param float|null $value Value to set for the creditMth5 property.
    */
    public function setCreditMth5(?float $value): void {
        $this->creditMth5 = $value;
    }

    /**
     * Sets the creditMth6 property value. The creditMth6 property
     * @param float|null $value Value to set for the creditMth6 property.
    */
    public function setCreditMth6(?float $value): void {
        $this->creditMth6 = $value;
    }

    /**
     * Sets the creditMth7 property value. The creditMth7 property
     * @param float|null $value Value to set for the creditMth7 property.
    */
    public function setCreditMth7(?float $value): void {
        $this->creditMth7 = $value;
    }

    /**
     * Sets the creditMth8 property value. The creditMth8 property
     * @param float|null $value Value to set for the creditMth8 property.
    */
    public function setCreditMth8(?float $value): void {
        $this->creditMth8 = $value;
    }

    /**
     * Sets the creditMth9 property value. The creditMth9 property
     * @param float|null $value Value to set for the creditMth9 property.
    */
    public function setCreditMth9(?float $value): void {
        $this->creditMth9 = $value;
    }

    /**
     * Sets the creditPosCode property value. The creditPosCode property
     * @param int|null $value Value to set for the creditPosCode property.
    */
    public function setCreditPosCode(?int $value): void {
        $this->creditPosCode = $value;
    }

    /**
     * Sets the creditPosition property value. The creditPosition property
     * @param SupplierAttributesRead_creditPosition|null $value Value to set for the creditPosition property.
    */
    public function setCreditPosition(?SupplierAttributesRead_creditPosition $value): void {
        $this->creditPosition = $value;
    }

    /**
     * Sets the creditRef property value. The creditRef property
     * @param string|null $value Value to set for the creditRef property.
    */
    public function setCreditRef(?string $value): void {
        $this->creditRef = $value;
    }

    /**
     * Sets the currency property value. The currency property
     * @param int|null $value Value to set for the currency property.
    */
    public function setCurrency(?int $value): void {
        $this->currency = $value;
    }

    /**
     * Sets the dateAccountOpened property value. The dateAccountOpened property
     * @param DateTime|null $value Value to set for the dateAccountOpened property.
    */
    public function setDateAccountOpened(?DateTime $value): void {
        $this->dateAccountOpened = $value;
    }

    /**
     * Sets the dateCreditApplied property value. The dateCreditApplied property
     * @param DateTime|null $value Value to set for the dateCreditApplied property.
    */
    public function setDateCreditApplied(?DateTime $value): void {
        $this->dateCreditApplied = $value;
    }

    /**
     * Sets the dateCreditReceived property value. The dateCreditReceived property
     * @param DateTime|null $value Value to set for the dateCreditReceived property.
    */
    public function setDateCreditReceived(?DateTime $value): void {
        $this->dateCreditReceived = $value;
    }

    /**
     * Sets the dateLastCredit property value. The dateLastCredit property
     * @param DateTime|null $value Value to set for the dateLastCredit property.
    */
    public function setDateLastCredit(?DateTime $value): void {
        $this->dateLastCredit = $value;
    }

    /**
     * Sets the dateNextCredit property value. The dateNextCredit property
     * @param DateTime|null $value Value to set for the dateNextCredit property.
    */
    public function setDateNextCredit(?DateTime $value): void {
        $this->dateNextCredit = $value;
    }

    /**
     * Sets the declarationValidFrom property value. The declarationValidFrom property
     * @param DateTime|null $value Value to set for the declarationValidFrom property.
    */
    public function setDeclarationValidFrom(?DateTime $value): void {
        $this->declarationValidFrom = $value;
    }

    /**
     * Sets the defaultFundId property value. The defaultFundId property
     * @param int|null $value Value to set for the defaultFundId property.
    */
    public function setDefaultFundId(?int $value): void {
        $this->defaultFundId = $value;
    }

    /**
     * Sets the defNomCode property value. The defNomCode property
     * @param string|null $value Value to set for the defNomCode property.
    */
    public function setDefNomCode(?string $value): void {
        $this->defNomCode = $value;
    }

    /**
     * Sets the defTaxCode property value. The defTaxCode property
     * @param SupplierAttributesRead_defTaxCode|null $value Value to set for the defTaxCode property.
    */
    public function setDefTaxCode(?SupplierAttributesRead_defTaxCode $value): void {
        $this->defTaxCode = $value;
    }

    /**
     * Sets the delAddress1 property value. The delAddress1 property
     * @param string|null $value Value to set for the delAddress1 property.
    */
    public function setDelAddress1(?string $value): void {
        $this->delAddress1 = $value;
    }

    /**
     * Sets the delAddress2 property value. The delAddress2 property
     * @param string|null $value Value to set for the delAddress2 property.
    */
    public function setDelAddress2(?string $value): void {
        $this->delAddress2 = $value;
    }

    /**
     * Sets the delAddress3 property value. The delAddress3 property
     * @param string|null $value Value to set for the delAddress3 property.
    */
    public function setDelAddress3(?string $value): void {
        $this->delAddress3 = $value;
    }

    /**
     * Sets the delAddress4 property value. The delAddress4 property
     * @param string|null $value Value to set for the delAddress4 property.
    */
    public function setDelAddress4(?string $value): void {
        $this->delAddress4 = $value;
    }

    /**
     * Sets the delAddress5 property value. The delAddress5 property
     * @param string|null $value Value to set for the delAddress5 property.
    */
    public function setDelAddress5(?string $value): void {
        $this->delAddress5 = $value;
    }

    /**
     * Sets the delContactName property value. The delContactName property
     * @param string|null $value Value to set for the delContactName property.
    */
    public function setDelContactName(?string $value): void {
        $this->delContactName = $value;
    }

    /**
     * Sets the delFax property value. The delFax property
     * @param string|null $value Value to set for the delFax property.
    */
    public function setDelFax(?string $value): void {
        $this->delFax = $value;
    }

    /**
     * Sets the delName property value. The delName property
     * @param string|null $value Value to set for the delName property.
    */
    public function setDelName(?string $value): void {
        $this->delName = $value;
    }

    /**
     * Sets the delTelephone property value. The delTelephone property
     * @param string|null $value Value to set for the delTelephone property.
    */
    public function setDelTelephone(?string $value): void {
        $this->delTelephone = $value;
    }

    /**
     * Sets the deptName property value. The deptName property
     * @param string|null $value Value to set for the deptName property.
    */
    public function setDeptName(?string $value): void {
        $this->deptName = $value;
    }

    /**
     * Sets the deptNumber property value. The deptNumber property
     * @param int|null $value Value to set for the deptNumber property.
    */
    public function setDeptNumber(?int $value): void {
        $this->deptNumber = $value;
    }

    /**
     * Sets the discountRate property value. The discountRate property
     * @param float|null $value Value to set for the discountRate property.
    */
    public function setDiscountRate(?float $value): void {
        $this->discountRate = $value;
    }

    /**
     * Sets the donorForename property value. The donorForename property
     * @param string|null $value Value to set for the donorForename property.
    */
    public function setDonorForename(?string $value): void {
        $this->donorForename = $value;
    }

    /**
     * Sets the donorSurname property value. The donorSurname property
     * @param string|null $value Value to set for the donorSurname property.
    */
    public function setDonorSurname(?string $value): void {
        $this->donorSurname = $value;
    }

    /**
     * Sets the donorTitle property value. The donorTitle property
     * @param string|null $value Value to set for the donorTitle property.
    */
    public function setDonorTitle(?string $value): void {
        $this->donorTitle = $value;
    }

    /**
     * Sets the dunsNumber property value. The dunsNumber property
     * @param string|null $value Value to set for the dunsNumber property.
    */
    public function setDunsNumber(?string $value): void {
        $this->dunsNumber = $value;
    }

    /**
     * Sets the eMail property value. The eMail property
     * @param string|null $value Value to set for the eMail property.
    */
    public function setEMail(?string $value): void {
        $this->eMail = $value;
    }

    /**
     * Sets the eMail2 property value. The eMail2 property
     * @param string|null $value Value to set for the eMail2 property.
    */
    public function setEMail2(?string $value): void {
        $this->eMail2 = $value;
    }

    /**
     * Sets the eMail3 property value. The eMail3 property
     * @param string|null $value Value to set for the eMail3 property.
    */
    public function setEMail3(?string $value): void {
        $this->eMail3 = $value;
    }

    /**
     * Sets the eoriNumber property value. The eoriNumber property
     * @param string|null $value Value to set for the eoriNumber property.
    */
    public function setEoriNumber(?string $value): void {
        $this->eoriNumber = $value;
    }

    /**
     * Sets the facebookAddress property value. The facebookAddress property
     * @param string|null $value Value to set for the facebookAddress property.
    */
    public function setFacebookAddress(?string $value): void {
        $this->facebookAddress = $value;
    }

    /**
     * Sets the fax property value. The fax property
     * @param string|null $value Value to set for the fax property.
    */
    public function setFax(?string $value): void {
        $this->fax = $value;
    }

    /**
     * Sets the firstInvDate property value. The firstInvDate property
     * @param DateTime|null $value Value to set for the firstInvDate property.
    */
    public function setFirstInvDate(?DateTime $value): void {
        $this->firstInvDate = $value;
    }

    /**
     * Sets the foreignBalance property value. The foreignBalance property
     * @param float|null $value Value to set for the foreignBalance property.
    */
    public function setForeignBalance(?float $value): void {
        $this->foreignBalance = $value;
    }

    /**
     * Sets the inactiveFlag property value. The inactiveFlag property
     * @param int|null $value Value to set for the inactiveFlag property.
    */
    public function setInactiveFlag(?int $value): void {
        $this->inactiveFlag = $value;
    }

    /**
     * Sets the incoterms property value. The incoterms property
     * @param int|null $value Value to set for the incoterms property.
    */
    public function setIncoterms(?int $value): void {
        $this->incoterms = $value;
    }

    /**
     * Sets the incotermsText property value. The incotermsText property
     * @param string|null $value Value to set for the incotermsText property.
    */
    public function setIncotermsText(?string $value): void {
        $this->incotermsText = $value;
    }

    /**
     * Sets the invoiceBf property value. The invoiceBf property
     * @param float|null $value Value to set for the invoiceBf property.
    */
    public function setInvoiceBf(?float $value): void {
        $this->invoiceBf = $value;
    }

    /**
     * Sets the invoiceCf property value. The invoiceCf property
     * @param float|null $value Value to set for the invoiceCf property.
    */
    public function setInvoiceCf(?float $value): void {
        $this->invoiceCf = $value;
    }

    /**
     * Sets the invoiceMth1 property value. The invoiceMth1 property
     * @param float|null $value Value to set for the invoiceMth1 property.
    */
    public function setInvoiceMth1(?float $value): void {
        $this->invoiceMth1 = $value;
    }

    /**
     * Sets the invoiceMth10 property value. The invoiceMth10 property
     * @param float|null $value Value to set for the invoiceMth10 property.
    */
    public function setInvoiceMth10(?float $value): void {
        $this->invoiceMth10 = $value;
    }

    /**
     * Sets the invoiceMth11 property value. The invoiceMth11 property
     * @param float|null $value Value to set for the invoiceMth11 property.
    */
    public function setInvoiceMth11(?float $value): void {
        $this->invoiceMth11 = $value;
    }

    /**
     * Sets the invoiceMth12 property value. The invoiceMth12 property
     * @param float|null $value Value to set for the invoiceMth12 property.
    */
    public function setInvoiceMth12(?float $value): void {
        $this->invoiceMth12 = $value;
    }

    /**
     * Sets the invoiceMth2 property value. The invoiceMth2 property
     * @param float|null $value Value to set for the invoiceMth2 property.
    */
    public function setInvoiceMth2(?float $value): void {
        $this->invoiceMth2 = $value;
    }

    /**
     * Sets the invoiceMth3 property value. The invoiceMth3 property
     * @param float|null $value Value to set for the invoiceMth3 property.
    */
    public function setInvoiceMth3(?float $value): void {
        $this->invoiceMth3 = $value;
    }

    /**
     * Sets the invoiceMth4 property value. The invoiceMth4 property
     * @param float|null $value Value to set for the invoiceMth4 property.
    */
    public function setInvoiceMth4(?float $value): void {
        $this->invoiceMth4 = $value;
    }

    /**
     * Sets the invoiceMth5 property value. The invoiceMth5 property
     * @param float|null $value Value to set for the invoiceMth5 property.
    */
    public function setInvoiceMth5(?float $value): void {
        $this->invoiceMth5 = $value;
    }

    /**
     * Sets the invoiceMth6 property value. The invoiceMth6 property
     * @param float|null $value Value to set for the invoiceMth6 property.
    */
    public function setInvoiceMth6(?float $value): void {
        $this->invoiceMth6 = $value;
    }

    /**
     * Sets the invoiceMth7 property value. The invoiceMth7 property
     * @param float|null $value Value to set for the invoiceMth7 property.
    */
    public function setInvoiceMth7(?float $value): void {
        $this->invoiceMth7 = $value;
    }

    /**
     * Sets the invoiceMth8 property value. The invoiceMth8 property
     * @param float|null $value Value to set for the invoiceMth8 property.
    */
    public function setInvoiceMth8(?float $value): void {
        $this->invoiceMth8 = $value;
    }

    /**
     * Sets the invoiceMth9 property value. The invoiceMth9 property
     * @param float|null $value Value to set for the invoiceMth9 property.
    */
    public function setInvoiceMth9(?float $value): void {
        $this->invoiceMth9 = $value;
    }

    /**
     * Sets the lastInvDate property value. The lastInvDate property
     * @param DateTime|null $value Value to set for the lastInvDate property.
    */
    public function setLastInvDate(?DateTime $value): void {
        $this->lastInvDate = $value;
    }

    /**
     * Sets the lastPaymentDate property value. The lastPaymentDate property
     * @param DateTime|null $value Value to set for the lastPaymentDate property.
    */
    public function setLastPaymentDate(?DateTime $value): void {
        $this->lastPaymentDate = $value;
    }

    /**
     * Sets the lastRefreshedDate property value. The lastRefreshedDate property
     * @param DateTime|null $value Value to set for the lastRefreshedDate property.
    */
    public function setLastRefreshedDate(?DateTime $value): void {
        $this->lastRefreshedDate = $value;
    }

    /**
     * Sets the lettersViaEmail property value. The lettersViaEmail property
     * @param int|null $value Value to set for the lettersViaEmail property.
    */
    public function setLettersViaEmail(?int $value): void {
        $this->lettersViaEmail = $value;
    }

    /**
     * Sets the linkedinAddress property value. The linkedinAddress property
     * @param string|null $value Value to set for the linkedinAddress property.
    */
    public function setLinkedinAddress(?string $value): void {
        $this->linkedinAddress = $value;
    }

    /**
     * Sets the mandateid property value. The mandateid property
     * @param string|null $value Value to set for the mandateid property.
    */
    public function setMandateid(?string $value): void {
        $this->mandateid = $value;
    }

    /**
     * Sets the name property value. The name property
     * @param string|null $value Value to set for the name property.
    */
    public function setName(?string $value): void {
        $this->name = $value;
    }

    /**
     * Sets the overrideTaxCode property value. The overrideTaxCode property
     * @param int|null $value Value to set for the overrideTaxCode property.
    */
    public function setOverrideTaxCode(?int $value): void {
        $this->overrideTaxCode = $value;
    }

    /**
     * Sets the paymentBf property value. The paymentBf property
     * @param float|null $value Value to set for the paymentBf property.
    */
    public function setPaymentBf(?float $value): void {
        $this->paymentBf = $value;
    }

    /**
     * Sets the paymentCf property value. The paymentCf property
     * @param float|null $value Value to set for the paymentCf property.
    */
    public function setPaymentCf(?float $value): void {
        $this->paymentCf = $value;
    }

    /**
     * Sets the paymentDueDays property value. The paymentDueDays property
     * @param int|null $value Value to set for the paymentDueDays property.
    */
    public function setPaymentDueDays(?int $value): void {
        $this->paymentDueDays = $value;
    }

    /**
     * Sets the paymentDueFrom property value. The paymentDueFrom property
     * @param int|null $value Value to set for the paymentDueFrom property.
    */
    public function setPaymentDueFrom(?int $value): void {
        $this->paymentDueFrom = $value;
    }

    /**
     * Sets the paymentDueFromText property value. The paymentDueFromText property
     * @param string|null $value Value to set for the paymentDueFromText property.
    */
    public function setPaymentDueFromText(?string $value): void {
        $this->paymentDueFromText = $value;
    }

    /**
     * Sets the paymentMethodId property value. The paymentMethodId property
     * @param int|null $value Value to set for the paymentMethodId property.
    */
    public function setPaymentMethodId(?int $value): void {
        $this->paymentMethodId = $value;
    }

    /**
     * Sets the paymentMth1 property value. The paymentMth1 property
     * @param float|null $value Value to set for the paymentMth1 property.
    */
    public function setPaymentMth1(?float $value): void {
        $this->paymentMth1 = $value;
    }

    /**
     * Sets the paymentMth10 property value. The paymentMth10 property
     * @param float|null $value Value to set for the paymentMth10 property.
    */
    public function setPaymentMth10(?float $value): void {
        $this->paymentMth10 = $value;
    }

    /**
     * Sets the paymentMth11 property value. The paymentMth11 property
     * @param float|null $value Value to set for the paymentMth11 property.
    */
    public function setPaymentMth11(?float $value): void {
        $this->paymentMth11 = $value;
    }

    /**
     * Sets the paymentMth12 property value. The paymentMth12 property
     * @param float|null $value Value to set for the paymentMth12 property.
    */
    public function setPaymentMth12(?float $value): void {
        $this->paymentMth12 = $value;
    }

    /**
     * Sets the paymentMth2 property value. The paymentMth2 property
     * @param float|null $value Value to set for the paymentMth2 property.
    */
    public function setPaymentMth2(?float $value): void {
        $this->paymentMth2 = $value;
    }

    /**
     * Sets the paymentMth3 property value. The paymentMth3 property
     * @param float|null $value Value to set for the paymentMth3 property.
    */
    public function setPaymentMth3(?float $value): void {
        $this->paymentMth3 = $value;
    }

    /**
     * Sets the paymentMth4 property value. The paymentMth4 property
     * @param float|null $value Value to set for the paymentMth4 property.
    */
    public function setPaymentMth4(?float $value): void {
        $this->paymentMth4 = $value;
    }

    /**
     * Sets the paymentMth5 property value. The paymentMth5 property
     * @param float|null $value Value to set for the paymentMth5 property.
    */
    public function setPaymentMth5(?float $value): void {
        $this->paymentMth5 = $value;
    }

    /**
     * Sets the paymentMth6 property value. The paymentMth6 property
     * @param float|null $value Value to set for the paymentMth6 property.
    */
    public function setPaymentMth6(?float $value): void {
        $this->paymentMth6 = $value;
    }

    /**
     * Sets the paymentMth7 property value. The paymentMth7 property
     * @param float|null $value Value to set for the paymentMth7 property.
    */
    public function setPaymentMth7(?float $value): void {
        $this->paymentMth7 = $value;
    }

    /**
     * Sets the paymentMth8 property value. The paymentMth8 property
     * @param float|null $value Value to set for the paymentMth8 property.
    */
    public function setPaymentMth8(?float $value): void {
        $this->paymentMth8 = $value;
    }

    /**
     * Sets the paymentMth9 property value. The paymentMth9 property
     * @param float|null $value Value to set for the paymentMth9 property.
    */
    public function setPaymentMth9(?float $value): void {
        $this->paymentMth9 = $value;
    }

    /**
     * Sets the priority property value. The priority property
     * @param int|null $value Value to set for the priority property.
    */
    public function setPriority(?int $value): void {
        $this->priority = $value;
    }

    /**
     * Sets the priorYear property value. The priorYear property
     * @param float|null $value Value to set for the priorYear property.
    */
    public function setPriorYear(?float $value): void {
        $this->priorYear = $value;
    }

    /**
     * Sets the promisedPayment property value. The promisedPayment property
     * @param float|null $value Value to set for the promisedPayment property.
    */
    public function setPromisedPayment(?float $value): void {
        $this->promisedPayment = $value;
    }

    /**
     * Sets the promisedPaymentDate property value. The promisedPaymentDate property
     * @param DateTime|null $value Value to set for the promisedPaymentDate property.
    */
    public function setPromisedPaymentDate(?DateTime $value): void {
        $this->promisedPaymentDate = $value;
    }

    /**
     * Sets the recordCreateDate property value. The recordCreateDate property
     * @param DateTime|null $value Value to set for the recordCreateDate property.
    */
    public function setRecordCreateDate(?DateTime $value): void {
        $this->recordCreateDate = $value;
    }

    /**
     * Sets the recordDeleted property value. The recordDeleted property
     * @param int|null $value Value to set for the recordDeleted property.
    */
    public function setRecordDeleted(?int $value): void {
        $this->recordDeleted = $value;
    }

    /**
     * Sets the recordModifyDate property value. The recordModifyDate property
     * @param DateTime|null $value Value to set for the recordModifyDate property.
    */
    public function setRecordModifyDate(?DateTime $value): void {
        $this->recordModifyDate = $value;
    }

    /**
     * Sets the reportPassword property value. The reportPassword property
     * @param string|null $value Value to set for the reportPassword property.
    */
    public function setReportPassword(?string $value): void {
        $this->reportPassword = $value;
    }

    /**
     * Sets the restrictMail property value. The restrictMail property
     * @param int|null $value Value to set for the restrictMail property.
    */
    public function setRestrictMail(?int $value): void {
        $this->restrictMail = $value;
    }

    /**
     * Sets the settlementDiscRate property value. The settlementDiscRate property
     * @param float|null $value Value to set for the settlementDiscRate property.
    */
    public function setSettlementDiscRate(?float $value): void {
        $this->settlementDiscRate = $value;
    }

    /**
     * Sets the settlementDueDays property value. The settlementDueDays property
     * @param int|null $value Value to set for the settlementDueDays property.
    */
    public function setSettlementDueDays(?int $value): void {
        $this->settlementDueDays = $value;
    }

    /**
     * Sets the status property value. The status property
     * @param int|null $value Value to set for the status property.
    */
    public function setStatus(?int $value): void {
        $this->status = $value;
    }

    /**
     * Sets the statusDescription property value. The statusDescription property
     * @param string|null $value Value to set for the statusDescription property.
    */
    public function setStatusDescription(?string $value): void {
        $this->statusDescription = $value;
    }

    /**
     * Sets the statusNumber property value. The statusNumber property
     * @param int|null $value Value to set for the statusNumber property.
    */
    public function setStatusNumber(?int $value): void {
        $this->statusNumber = $value;
    }

    /**
     * Sets the statusText property value. The statusText property
     * @param string|null $value Value to set for the statusText property.
    */
    public function setStatusText(?string $value): void {
        $this->statusText = $value;
    }

    /**
     * Sets the telephone property value. The telephone property
     * @param string|null $value Value to set for the telephone property.
    */
    public function setTelephone(?string $value): void {
        $this->telephone = $value;
    }

    /**
     * Sets the telephone2 property value. The telephone2 property
     * @param string|null $value Value to set for the telephone2 property.
    */
    public function setTelephone2(?string $value): void {
        $this->telephone2 = $value;
    }

    /**
     * Sets the terms property value. The terms property
     * @param string|null $value Value to set for the terms property.
    */
    public function setTerms(?string $value): void {
        $this->terms = $value;
    }

    /**
     * Sets the termsAgreed property value. The termsAgreed property
     * @param int|null $value Value to set for the termsAgreed property.
    */
    public function setTermsAgreed(?int $value): void {
        $this->termsAgreed = $value;
    }

    /**
     * Sets the tradeContact property value. The tradeContact property
     * @param string|null $value Value to set for the tradeContact property.
    */
    public function setTradeContact(?string $value): void {
        $this->tradeContact = $value;
    }

    /**
     * Sets the turnoverMtd property value. The turnoverMtd property
     * @param float|null $value Value to set for the turnoverMtd property.
    */
    public function setTurnoverMtd(?float $value): void {
        $this->turnoverMtd = $value;
    }

    /**
     * Sets the turnoverYtd property value. The turnoverYtd property
     * @param float|null $value Value to set for the turnoverYtd property.
    */
    public function setTurnoverYtd(?float $value): void {
        $this->turnoverYtd = $value;
    }

    /**
     * Sets the twitterAddress property value. The twitterAddress property
     * @param string|null $value Value to set for the twitterAddress property.
    */
    public function setTwitterAddress(?string $value): void {
        $this->twitterAddress = $value;
    }

    /**
     * Sets the useBacs property value. The useBacs property
     * @param int|null $value Value to set for the useBacs property.
    */
    public function setUseBacs(?int $value): void {
        $this->useBacs = $value;
    }

    /**
     * Sets the useBsoc property value. The useBsoc property
     * @param int|null $value Value to set for the useBsoc property.
    */
    public function setUseBsoc(?int $value): void {
        $this->useBsoc = $value;
    }

    /**
     * Sets the vatRegNumber property value. The vatRegNumber property
     * @param string|null $value Value to set for the vatRegNumber property.
    */
    public function setVatRegNumber(?string $value): void {
        $this->vatRegNumber = $value;
    }

    /**
     * Sets the webAddress property value. The webAddress property
     * @param string|null $value Value to set for the webAddress property.
    */
    public function setWebAddress(?string $value): void {
        $this->webAddress = $value;
    }

}
