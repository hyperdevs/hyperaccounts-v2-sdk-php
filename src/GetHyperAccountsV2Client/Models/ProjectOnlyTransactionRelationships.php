<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class ProjectOnlyTransactionRelationships implements Parsable
{
    /**
     * @var ProjectResourceRelatedRelationship|null $projectResource The projectResource property
    */
    private ?ProjectResourceRelatedRelationship $projectResource = null;

    /**
     * @var ProjectTranRelatedRelationship|null $projectTran The projectTran property
    */
    private ?ProjectTranRelatedRelationship $projectTran = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return ProjectOnlyTransactionRelationships
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): ProjectOnlyTransactionRelationships {
        return new ProjectOnlyTransactionRelationships();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'projectResource' => fn(ParseNode $n) => $o->setProjectResource($n->getObjectValue([ProjectResourceRelatedRelationship::class, 'createFromDiscriminatorValue'])),
            'projectTran' => fn(ParseNode $n) => $o->setProjectTran($n->getObjectValue([ProjectTranRelatedRelationship::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the projectResource property value. The projectResource property
     * @return ProjectResourceRelatedRelationship|null
    */
    public function getProjectResource(): ?ProjectResourceRelatedRelationship {
        return $this->projectResource;
    }

    /**
     * Gets the projectTran property value. The projectTran property
     * @return ProjectTranRelatedRelationship|null
    */
    public function getProjectTran(): ?ProjectTranRelatedRelationship {
        return $this->projectTran;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('projectResource', $this->getProjectResource());
        $writer->writeObjectValue('projectTran', $this->getProjectTran());
    }

    /**
     * Sets the projectResource property value. The projectResource property
     * @param ProjectResourceRelatedRelationship|null $value Value to set for the projectResource property.
    */
    public function setProjectResource(?ProjectResourceRelatedRelationship $value): void {
        $this->projectResource = $value;
    }

    /**
     * Sets the projectTran property value. The projectTran property
     * @param ProjectTranRelatedRelationship|null $value Value to set for the projectTran property.
    */
    public function setProjectTran(?ProjectTranRelatedRelationship $value): void {
        $this->projectTran = $value;
    }

}
