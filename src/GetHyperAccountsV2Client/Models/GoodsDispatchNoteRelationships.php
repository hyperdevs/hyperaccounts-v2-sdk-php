<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class GoodsDispatchNoteRelationships implements Parsable
{
    /**
     * @var CustomerRelatedRelationship|null $customer The customer property
    */
    private ?CustomerRelatedRelationship $customer = null;

    /**
     * @var SalesOrderRelatedRelationship|null $salesOrder The salesOrder property
    */
    private ?SalesOrderRelatedRelationship $salesOrder = null;

    /**
     * @var StockRelatedRelationship|null $stock The stock property
    */
    private ?StockRelatedRelationship $stock = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return GoodsDispatchNoteRelationships
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): GoodsDispatchNoteRelationships {
        return new GoodsDispatchNoteRelationships();
    }

    /**
     * Gets the customer property value. The customer property
     * @return CustomerRelatedRelationship|null
    */
    public function getCustomer(): ?CustomerRelatedRelationship {
        return $this->customer;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'customer' => fn(ParseNode $n) => $o->setCustomer($n->getObjectValue([CustomerRelatedRelationship::class, 'createFromDiscriminatorValue'])),
            'salesOrder' => fn(ParseNode $n) => $o->setSalesOrder($n->getObjectValue([SalesOrderRelatedRelationship::class, 'createFromDiscriminatorValue'])),
            'stock' => fn(ParseNode $n) => $o->setStock($n->getObjectValue([StockRelatedRelationship::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the salesOrder property value. The salesOrder property
     * @return SalesOrderRelatedRelationship|null
    */
    public function getSalesOrder(): ?SalesOrderRelatedRelationship {
        return $this->salesOrder;
    }

    /**
     * Gets the stock property value. The stock property
     * @return StockRelatedRelationship|null
    */
    public function getStock(): ?StockRelatedRelationship {
        return $this->stock;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('customer', $this->getCustomer());
        $writer->writeObjectValue('salesOrder', $this->getSalesOrder());
        $writer->writeObjectValue('stock', $this->getStock());
    }

    /**
     * Sets the customer property value. The customer property
     * @param CustomerRelatedRelationship|null $value Value to set for the customer property.
    */
    public function setCustomer(?CustomerRelatedRelationship $value): void {
        $this->customer = $value;
    }

    /**
     * Sets the salesOrder property value. The salesOrder property
     * @param SalesOrderRelatedRelationship|null $value Value to set for the salesOrder property.
    */
    public function setSalesOrder(?SalesOrderRelatedRelationship $value): void {
        $this->salesOrder = $value;
    }

    /**
     * Sets the stock property value. The stock property
     * @param StockRelatedRelationship|null $value Value to set for the stock property.
    */
    public function setStock(?StockRelatedRelationship $value): void {
        $this->stock = $value;
    }

}
