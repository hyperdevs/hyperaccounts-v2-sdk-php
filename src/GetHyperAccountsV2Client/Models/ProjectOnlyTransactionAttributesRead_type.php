<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Enum;

class ProjectOnlyTransactionAttributesRead_type extends Enum {
    public const C_C = 'CC';
    public const C_D = 'CD';
}
