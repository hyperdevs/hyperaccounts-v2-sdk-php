<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class TransactionSplitRelatedRequired implements Parsable
{
    /**
     * @var int|null $splitNumber The splitNumber property
    */
    private ?int $splitNumber = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return TransactionSplitRelatedRequired
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): TransactionSplitRelatedRequired {
        return new TransactionSplitRelatedRequired();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'splitNumber' => fn(ParseNode $n) => $o->setSplitNumber($n->getIntegerValue()),
        ];
    }

    /**
     * Gets the splitNumber property value. The splitNumber property
     * @return int|null
    */
    public function getSplitNumber(): ?int {
        return $this->splitNumber;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeIntegerValue('splitNumber', $this->getSplitNumber());
    }

    /**
     * Sets the splitNumber property value. The splitNumber property
     * @param int|null $value Value to set for the splitNumber property.
    */
    public function setSplitNumber(?int $value): void {
        $this->splitNumber = $value;
    }

}
