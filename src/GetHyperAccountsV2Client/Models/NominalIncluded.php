<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class NominalIncluded implements Parsable
{
    /**
     * @var PurchaseOrderItemCollection|null $purchaseOrderItems The purchaseOrderItems property
    */
    private ?PurchaseOrderItemCollection $purchaseOrderItems = null;

    /**
     * @var SalesInvoiceItemCollection|null $salesInvoiceItems The salesInvoiceItems property
    */
    private ?SalesInvoiceItemCollection $salesInvoiceItems = null;

    /**
     * @var SalesOrderItemCollection|null $salesOrderItems The salesOrderItems property
    */
    private ?SalesOrderItemCollection $salesOrderItems = null;

    /**
     * @var StockCollection|null $stocks The stocks property
    */
    private ?StockCollection $stocks = null;

    /**
     * @var TransactionSplitCollection|null $transactionSplits The transactionSplits property
    */
    private ?TransactionSplitCollection $transactionSplits = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return NominalIncluded
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): NominalIncluded {
        return new NominalIncluded();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'purchaseOrderItems' => fn(ParseNode $n) => $o->setPurchaseOrderItems($n->getObjectValue([PurchaseOrderItemCollection::class, 'createFromDiscriminatorValue'])),
            'salesInvoiceItems' => fn(ParseNode $n) => $o->setSalesInvoiceItems($n->getObjectValue([SalesInvoiceItemCollection::class, 'createFromDiscriminatorValue'])),
            'salesOrderItems' => fn(ParseNode $n) => $o->setSalesOrderItems($n->getObjectValue([SalesOrderItemCollection::class, 'createFromDiscriminatorValue'])),
            'stocks' => fn(ParseNode $n) => $o->setStocks($n->getObjectValue([StockCollection::class, 'createFromDiscriminatorValue'])),
            'transactionSplits' => fn(ParseNode $n) => $o->setTransactionSplits($n->getObjectValue([TransactionSplitCollection::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the purchaseOrderItems property value. The purchaseOrderItems property
     * @return PurchaseOrderItemCollection|null
    */
    public function getPurchaseOrderItems(): ?PurchaseOrderItemCollection {
        return $this->purchaseOrderItems;
    }

    /**
     * Gets the salesInvoiceItems property value. The salesInvoiceItems property
     * @return SalesInvoiceItemCollection|null
    */
    public function getSalesInvoiceItems(): ?SalesInvoiceItemCollection {
        return $this->salesInvoiceItems;
    }

    /**
     * Gets the salesOrderItems property value. The salesOrderItems property
     * @return SalesOrderItemCollection|null
    */
    public function getSalesOrderItems(): ?SalesOrderItemCollection {
        return $this->salesOrderItems;
    }

    /**
     * Gets the stocks property value. The stocks property
     * @return StockCollection|null
    */
    public function getStocks(): ?StockCollection {
        return $this->stocks;
    }

    /**
     * Gets the transactionSplits property value. The transactionSplits property
     * @return TransactionSplitCollection|null
    */
    public function getTransactionSplits(): ?TransactionSplitCollection {
        return $this->transactionSplits;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('purchaseOrderItems', $this->getPurchaseOrderItems());
        $writer->writeObjectValue('salesInvoiceItems', $this->getSalesInvoiceItems());
        $writer->writeObjectValue('salesOrderItems', $this->getSalesOrderItems());
        $writer->writeObjectValue('stocks', $this->getStocks());
        $writer->writeObjectValue('transactionSplits', $this->getTransactionSplits());
    }

    /**
     * Sets the purchaseOrderItems property value. The purchaseOrderItems property
     * @param PurchaseOrderItemCollection|null $value Value to set for the purchaseOrderItems property.
    */
    public function setPurchaseOrderItems(?PurchaseOrderItemCollection $value): void {
        $this->purchaseOrderItems = $value;
    }

    /**
     * Sets the salesInvoiceItems property value. The salesInvoiceItems property
     * @param SalesInvoiceItemCollection|null $value Value to set for the salesInvoiceItems property.
    */
    public function setSalesInvoiceItems(?SalesInvoiceItemCollection $value): void {
        $this->salesInvoiceItems = $value;
    }

    /**
     * Sets the salesOrderItems property value. The salesOrderItems property
     * @param SalesOrderItemCollection|null $value Value to set for the salesOrderItems property.
    */
    public function setSalesOrderItems(?SalesOrderItemCollection $value): void {
        $this->salesOrderItems = $value;
    }

    /**
     * Sets the stocks property value. The stocks property
     * @param StockCollection|null $value Value to set for the stocks property.
    */
    public function setStocks(?StockCollection $value): void {
        $this->stocks = $value;
    }

    /**
     * Sets the transactionSplits property value. The transactionSplits property
     * @param TransactionSplitCollection|null $value Value to set for the transactionSplits property.
    */
    public function setTransactionSplits(?TransactionSplitCollection $value): void {
        $this->transactionSplits = $value;
    }

}
