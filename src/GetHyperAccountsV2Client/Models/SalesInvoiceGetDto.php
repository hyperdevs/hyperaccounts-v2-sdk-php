<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class SalesInvoiceGetDto implements Parsable
{
    /**
     * @var SalesInvoiceAttributesRead|null $attributes The attributes property
    */
    private ?SalesInvoiceAttributesRead $attributes = null;

    /**
     * @var SalesInvoiceIncluded|null $included The included property
    */
    private ?SalesInvoiceIncluded $included = null;

    /**
     * @var int|null $invoiceNumber The invoiceNumber property
    */
    private ?int $invoiceNumber = null;

    /**
     * @var ResourceLinks|null $links The links property
    */
    private ?ResourceLinks $links = null;

    /**
     * @var SalesInvoiceRelationships|null $relationships The relationships property
    */
    private ?SalesInvoiceRelationships $relationships = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return SalesInvoiceGetDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): SalesInvoiceGetDto {
        return new SalesInvoiceGetDto();
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return SalesInvoiceAttributesRead|null
    */
    public function getAttributes(): ?SalesInvoiceAttributesRead {
        return $this->attributes;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([SalesInvoiceAttributesRead::class, 'createFromDiscriminatorValue'])),
            'included' => fn(ParseNode $n) => $o->setIncluded($n->getObjectValue([SalesInvoiceIncluded::class, 'createFromDiscriminatorValue'])),
            'invoiceNumber' => fn(ParseNode $n) => $o->setInvoiceNumber($n->getIntegerValue()),
            'links' => fn(ParseNode $n) => $o->setLinks($n->getObjectValue([ResourceLinks::class, 'createFromDiscriminatorValue'])),
            'relationships' => fn(ParseNode $n) => $o->setRelationships($n->getObjectValue([SalesInvoiceRelationships::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the included property value. The included property
     * @return SalesInvoiceIncluded|null
    */
    public function getIncluded(): ?SalesInvoiceIncluded {
        return $this->included;
    }

    /**
     * Gets the invoiceNumber property value. The invoiceNumber property
     * @return int|null
    */
    public function getInvoiceNumber(): ?int {
        return $this->invoiceNumber;
    }

    /**
     * Gets the links property value. The links property
     * @return ResourceLinks|null
    */
    public function getLinks(): ?ResourceLinks {
        return $this->links;
    }

    /**
     * Gets the relationships property value. The relationships property
     * @return SalesInvoiceRelationships|null
    */
    public function getRelationships(): ?SalesInvoiceRelationships {
        return $this->relationships;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('attributes', $this->getAttributes());
        $writer->writeObjectValue('included', $this->getIncluded());
        $writer->writeIntegerValue('invoiceNumber', $this->getInvoiceNumber());
        $writer->writeObjectValue('links', $this->getLinks());
        $writer->writeObjectValue('relationships', $this->getRelationships());
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param SalesInvoiceAttributesRead|null $value Value to set for the attributes property.
    */
    public function setAttributes(?SalesInvoiceAttributesRead $value): void {
        $this->attributes = $value;
    }

    /**
     * Sets the included property value. The included property
     * @param SalesInvoiceIncluded|null $value Value to set for the included property.
    */
    public function setIncluded(?SalesInvoiceIncluded $value): void {
        $this->included = $value;
    }

    /**
     * Sets the invoiceNumber property value. The invoiceNumber property
     * @param int|null $value Value to set for the invoiceNumber property.
    */
    public function setInvoiceNumber(?int $value): void {
        $this->invoiceNumber = $value;
    }

    /**
     * Sets the links property value. The links property
     * @param ResourceLinks|null $value Value to set for the links property.
    */
    public function setLinks(?ResourceLinks $value): void {
        $this->links = $value;
    }

    /**
     * Sets the relationships property value. The relationships property
     * @param SalesInvoiceRelationships|null $value Value to set for the relationships property.
    */
    public function setRelationships(?SalesInvoiceRelationships $value): void {
        $this->relationships = $value;
    }

}
