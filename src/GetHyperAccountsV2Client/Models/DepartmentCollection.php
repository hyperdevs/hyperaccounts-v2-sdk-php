<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class DepartmentCollection implements Parsable
{
    /**
     * @var array<DepartmentGetDto>|null $data The data property
    */
    private ?array $data = null;

    /**
     * @var ResourceLinks|null $links The links property
    */
    private ?ResourceLinks $links = null;

    /**
     * @var MetaCollection|null $meta The meta property
    */
    private ?MetaCollection $meta = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return DepartmentCollection
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): DepartmentCollection {
        return new DepartmentCollection();
    }

    /**
     * Gets the data property value. The data property
     * @return array<DepartmentGetDto>|null
    */
    public function getData(): ?array {
        return $this->data;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'data' => fn(ParseNode $n) => $o->setData($n->getCollectionOfObjectValues([DepartmentGetDto::class, 'createFromDiscriminatorValue'])),
            'links' => fn(ParseNode $n) => $o->setLinks($n->getObjectValue([ResourceLinks::class, 'createFromDiscriminatorValue'])),
            'meta' => fn(ParseNode $n) => $o->setMeta($n->getObjectValue([MetaCollection::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the links property value. The links property
     * @return ResourceLinks|null
    */
    public function getLinks(): ?ResourceLinks {
        return $this->links;
    }

    /**
     * Gets the meta property value. The meta property
     * @return MetaCollection|null
    */
    public function getMeta(): ?MetaCollection {
        return $this->meta;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeCollectionOfObjectValues('data', $this->getData());
        $writer->writeObjectValue('links', $this->getLinks());
        $writer->writeObjectValue('meta', $this->getMeta());
    }

    /**
     * Sets the data property value. The data property
     * @param array<DepartmentGetDto>|null $value Value to set for the data property.
    */
    public function setData(?array $value): void {
        $this->data = $value;
    }

    /**
     * Sets the links property value. The links property
     * @param ResourceLinks|null $value Value to set for the links property.
    */
    public function setLinks(?ResourceLinks $value): void {
        $this->links = $value;
    }

    /**
     * Sets the meta property value. The meta property
     * @param MetaCollection|null $value Value to set for the meta property.
    */
    public function setMeta(?MetaCollection $value): void {
        $this->meta = $value;
    }

}
