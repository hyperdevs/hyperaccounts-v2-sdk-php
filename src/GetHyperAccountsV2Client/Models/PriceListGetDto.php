<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class PriceListGetDto implements Parsable
{
    /**
     * @var PriceListAttributesRead|null $attributes The attributes property
    */
    private ?PriceListAttributesRead $attributes = null;

    /**
     * @var ResourceLinks|null $links The links property
    */
    private ?ResourceLinks $links = null;

    /**
     * @var string|null $pricingRef The pricingRef property
    */
    private ?string $pricingRef = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return PriceListGetDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): PriceListGetDto {
        return new PriceListGetDto();
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return PriceListAttributesRead|null
    */
    public function getAttributes(): ?PriceListAttributesRead {
        return $this->attributes;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([PriceListAttributesRead::class, 'createFromDiscriminatorValue'])),
            'links' => fn(ParseNode $n) => $o->setLinks($n->getObjectValue([ResourceLinks::class, 'createFromDiscriminatorValue'])),
            'pricingRef' => fn(ParseNode $n) => $o->setPricingRef($n->getStringValue()),
        ];
    }

    /**
     * Gets the links property value. The links property
     * @return ResourceLinks|null
    */
    public function getLinks(): ?ResourceLinks {
        return $this->links;
    }

    /**
     * Gets the pricingRef property value. The pricingRef property
     * @return string|null
    */
    public function getPricingRef(): ?string {
        return $this->pricingRef;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('attributes', $this->getAttributes());
        $writer->writeObjectValue('links', $this->getLinks());
        $writer->writeStringValue('pricingRef', $this->getPricingRef());
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param PriceListAttributesRead|null $value Value to set for the attributes property.
    */
    public function setAttributes(?PriceListAttributesRead $value): void {
        $this->attributes = $value;
    }

    /**
     * Sets the links property value. The links property
     * @param ResourceLinks|null $value Value to set for the links property.
    */
    public function setLinks(?ResourceLinks $value): void {
        $this->links = $value;
    }

    /**
     * Sets the pricingRef property value. The pricingRef property
     * @param string|null $value Value to set for the pricingRef property.
    */
    public function setPricingRef(?string $value): void {
        $this->pricingRef = $value;
    }

}
