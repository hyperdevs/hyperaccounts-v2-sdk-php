<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class CustomerAddressIncluded implements Parsable
{
    /**
     * @var CustomerGetDto|null $customer The customer property
    */
    private ?CustomerGetDto $customer = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return CustomerAddressIncluded
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): CustomerAddressIncluded {
        return new CustomerAddressIncluded();
    }

    /**
     * Gets the customer property value. The customer property
     * @return CustomerGetDto|null
    */
    public function getCustomer(): ?CustomerGetDto {
        return $this->customer;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'customer' => fn(ParseNode $n) => $o->setCustomer($n->getObjectValue([CustomerGetDto::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('customer', $this->getCustomer());
    }

    /**
     * Sets the customer property value. The customer property
     * @param CustomerGetDto|null $value Value to set for the customer property.
    */
    public function setCustomer(?CustomerGetDto $value): void {
        $this->customer = $value;
    }

}
