<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class SalesInvoiceIncluded implements Parsable
{
    /**
     * @var CustomerGetDto|null $customer The customer property
    */
    private ?CustomerGetDto $customer = null;

    /**
     * @var SalesInvoiceItemCollection|null $items The items property
    */
    private ?SalesInvoiceItemCollection $items = null;

    /**
     * @var ProjectGetDto|null $project The project property
    */
    private ?ProjectGetDto $project = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return SalesInvoiceIncluded
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): SalesInvoiceIncluded {
        return new SalesInvoiceIncluded();
    }

    /**
     * Gets the customer property value. The customer property
     * @return CustomerGetDto|null
    */
    public function getCustomer(): ?CustomerGetDto {
        return $this->customer;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'customer' => fn(ParseNode $n) => $o->setCustomer($n->getObjectValue([CustomerGetDto::class, 'createFromDiscriminatorValue'])),
            'items' => fn(ParseNode $n) => $o->setItems($n->getObjectValue([SalesInvoiceItemCollection::class, 'createFromDiscriminatorValue'])),
            'project' => fn(ParseNode $n) => $o->setProject($n->getObjectValue([ProjectGetDto::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the items property value. The items property
     * @return SalesInvoiceItemCollection|null
    */
    public function getItems(): ?SalesInvoiceItemCollection {
        return $this->items;
    }

    /**
     * Gets the project property value. The project property
     * @return ProjectGetDto|null
    */
    public function getProject(): ?ProjectGetDto {
        return $this->project;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('customer', $this->getCustomer());
        $writer->writeObjectValue('items', $this->getItems());
        $writer->writeObjectValue('project', $this->getProject());
    }

    /**
     * Sets the customer property value. The customer property
     * @param CustomerGetDto|null $value Value to set for the customer property.
    */
    public function setCustomer(?CustomerGetDto $value): void {
        $this->customer = $value;
    }

    /**
     * Sets the items property value. The items property
     * @param SalesInvoiceItemCollection|null $value Value to set for the items property.
    */
    public function setItems(?SalesInvoiceItemCollection $value): void {
        $this->items = $value;
    }

    /**
     * Sets the project property value. The project property
     * @param ProjectGetDto|null $value Value to set for the project property.
    */
    public function setProject(?ProjectGetDto $value): void {
        $this->project = $value;
    }

}
