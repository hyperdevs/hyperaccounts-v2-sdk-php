<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Enum;

class WebhookAttributesRead_sageFileName extends Enum {
    public const CUSTOMERS = 'Customers';
    public const SALES_INVOICES = 'SalesInvoices';
    public const SALES_INVOICE_ITEMS = 'SalesInvoiceItems';
    public const SUPPLIERS = 'Suppliers';
    public const PURCHASE_ORDERS = 'PurchaseOrders';
    public const PURCHASE_ORDER_ITEMS = 'PurchaseOrderItems';
    public const BANKS = 'Banks';
    public const CUSTOMER_ADDRESSES = 'CustomerAddresses';
    public const DEPARTMENTS = 'Departments';
    public const TRANSACTION_HEADERS = 'TransactionHeaders';
    public const TRANSACTION_SPLITS = 'TransactionSplits';
    public const TRANSACTION_USAGES = 'TransactionUsages';
    public const SALES_ORDERS = 'SalesOrders';
    public const SALES_ORDER_ITEMS = 'SalesOrderItems';
    public const STOCKS = 'Stocks';
    public const TAX_CODES = 'TaxCodes';
    public const GOODS_RECEIVED_NOTES = 'GoodsReceivedNotes';
    public const PROJECTS = 'Projects';
    public const PROJECT_STATUSES = 'ProjectStatuses';
    public const WEBHOOKS = 'Webhooks';
    public const PROJECT_COST_CODES = 'ProjectCostCodes';
    public const STOCK_TRANSACTIONS = 'StockTransactions';
    public const DOCUMENT_LINKS = 'DocumentLinks';
    public const PROJECT_BUDGETS = 'ProjectBudgets';
    public const FIXED_ASSETS = 'FixedAssets';
    public const NOMINALS = 'Nominals';
    public const PRICE_LISTS = 'PriceLists';
    public const PRICES = 'Prices';
    public const CURRENCIES = 'Currencies';
    public const GOODS_DISPATCH_NOTES = 'GoodsDispatchNotes';
    public const PROJECT_RESOURCES = 'ProjectResources';
    public const PROJECT_ONLY_TRANSACTIONS = 'ProjectOnlyTransactions';
    public const PROJECT_TRANS = 'ProjectTrans';
    public const COMPANY = 'Company';
    public const CHART_OF_ACCOUNTS = 'ChartOfAccounts';
    public const CHART_OF_ACCOUNTS_CATEGORY_TITLES = 'ChartOfAccountsCategoryTitles';
    public const CHART_OF_ACCOUNTS_CATEGORIES = 'ChartOfAccountsCategories';
}
