<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class GoodsReceivedNoteRelationships implements Parsable
{
    /**
     * @var PurchaseOrderRelatedRelationship|null $purchaseOrder The purchaseOrder property
    */
    private ?PurchaseOrderRelatedRelationship $purchaseOrder = null;

    /**
     * @var StockRelatedRelationship|null $stock The stock property
    */
    private ?StockRelatedRelationship $stock = null;

    /**
     * @var SupplierRelatedRelationship|null $supplier The supplier property
    */
    private ?SupplierRelatedRelationship $supplier = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return GoodsReceivedNoteRelationships
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): GoodsReceivedNoteRelationships {
        return new GoodsReceivedNoteRelationships();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'purchaseOrder' => fn(ParseNode $n) => $o->setPurchaseOrder($n->getObjectValue([PurchaseOrderRelatedRelationship::class, 'createFromDiscriminatorValue'])),
            'stock' => fn(ParseNode $n) => $o->setStock($n->getObjectValue([StockRelatedRelationship::class, 'createFromDiscriminatorValue'])),
            'supplier' => fn(ParseNode $n) => $o->setSupplier($n->getObjectValue([SupplierRelatedRelationship::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the purchaseOrder property value. The purchaseOrder property
     * @return PurchaseOrderRelatedRelationship|null
    */
    public function getPurchaseOrder(): ?PurchaseOrderRelatedRelationship {
        return $this->purchaseOrder;
    }

    /**
     * Gets the stock property value. The stock property
     * @return StockRelatedRelationship|null
    */
    public function getStock(): ?StockRelatedRelationship {
        return $this->stock;
    }

    /**
     * Gets the supplier property value. The supplier property
     * @return SupplierRelatedRelationship|null
    */
    public function getSupplier(): ?SupplierRelatedRelationship {
        return $this->supplier;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('purchaseOrder', $this->getPurchaseOrder());
        $writer->writeObjectValue('stock', $this->getStock());
        $writer->writeObjectValue('supplier', $this->getSupplier());
    }

    /**
     * Sets the purchaseOrder property value. The purchaseOrder property
     * @param PurchaseOrderRelatedRelationship|null $value Value to set for the purchaseOrder property.
    */
    public function setPurchaseOrder(?PurchaseOrderRelatedRelationship $value): void {
        $this->purchaseOrder = $value;
    }

    /**
     * Sets the stock property value. The stock property
     * @param StockRelatedRelationship|null $value Value to set for the stock property.
    */
    public function setStock(?StockRelatedRelationship $value): void {
        $this->stock = $value;
    }

    /**
     * Sets the supplier property value. The supplier property
     * @param SupplierRelatedRelationship|null $value Value to set for the supplier property.
    */
    public function setSupplier(?SupplierRelatedRelationship $value): void {
        $this->supplier = $value;
    }

}
