<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class ChartOfAccountsCategoryTitleRelationships implements Parsable
{
    /**
     * @var ChartOfAccountsCategoryRelatedListRelationship|null $categories The categories property
    */
    private ?ChartOfAccountsCategoryRelatedListRelationship $categories = null;

    /**
     * @var ChartOfAccountsRelatedRelationship|null $chart The chart property
    */
    private ?ChartOfAccountsRelatedRelationship $chart = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return ChartOfAccountsCategoryTitleRelationships
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): ChartOfAccountsCategoryTitleRelationships {
        return new ChartOfAccountsCategoryTitleRelationships();
    }

    /**
     * Gets the categories property value. The categories property
     * @return ChartOfAccountsCategoryRelatedListRelationship|null
    */
    public function getCategories(): ?ChartOfAccountsCategoryRelatedListRelationship {
        return $this->categories;
    }

    /**
     * Gets the chart property value. The chart property
     * @return ChartOfAccountsRelatedRelationship|null
    */
    public function getChart(): ?ChartOfAccountsRelatedRelationship {
        return $this->chart;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'categories' => fn(ParseNode $n) => $o->setCategories($n->getObjectValue([ChartOfAccountsCategoryRelatedListRelationship::class, 'createFromDiscriminatorValue'])),
            'chart' => fn(ParseNode $n) => $o->setChart($n->getObjectValue([ChartOfAccountsRelatedRelationship::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('categories', $this->getCategories());
        $writer->writeObjectValue('chart', $this->getChart());
    }

    /**
     * Sets the categories property value. The categories property
     * @param ChartOfAccountsCategoryRelatedListRelationship|null $value Value to set for the categories property.
    */
    public function setCategories(?ChartOfAccountsCategoryRelatedListRelationship $value): void {
        $this->categories = $value;
    }

    /**
     * Sets the chart property value. The chart property
     * @param ChartOfAccountsRelatedRelationship|null $value Value to set for the chart property.
    */
    public function setChart(?ChartOfAccountsRelatedRelationship $value): void {
        $this->chart = $value;
    }

}
