<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class ProjectBudgetRelationships implements Parsable
{
    /**
     * @var ProjectRelatedRelationship|null $project The project property
    */
    private ?ProjectRelatedRelationship $project = null;

    /**
     * @var ProjectCostCodeRelatedRelationship|null $projectCostCode The projectCostCode property
    */
    private ?ProjectCostCodeRelatedRelationship $projectCostCode = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return ProjectBudgetRelationships
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): ProjectBudgetRelationships {
        return new ProjectBudgetRelationships();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'project' => fn(ParseNode $n) => $o->setProject($n->getObjectValue([ProjectRelatedRelationship::class, 'createFromDiscriminatorValue'])),
            'projectCostCode' => fn(ParseNode $n) => $o->setProjectCostCode($n->getObjectValue([ProjectCostCodeRelatedRelationship::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the project property value. The project property
     * @return ProjectRelatedRelationship|null
    */
    public function getProject(): ?ProjectRelatedRelationship {
        return $this->project;
    }

    /**
     * Gets the projectCostCode property value. The projectCostCode property
     * @return ProjectCostCodeRelatedRelationship|null
    */
    public function getProjectCostCode(): ?ProjectCostCodeRelatedRelationship {
        return $this->projectCostCode;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('project', $this->getProject());
        $writer->writeObjectValue('projectCostCode', $this->getProjectCostCode());
    }

    /**
     * Sets the project property value. The project property
     * @param ProjectRelatedRelationship|null $value Value to set for the project property.
    */
    public function setProject(?ProjectRelatedRelationship $value): void {
        $this->project = $value;
    }

    /**
     * Sets the projectCostCode property value. The projectCostCode property
     * @param ProjectCostCodeRelatedRelationship|null $value Value to set for the projectCostCode property.
    */
    public function setProjectCostCode(?ProjectCostCodeRelatedRelationship $value): void {
        $this->projectCostCode = $value;
    }

}
