<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use DateTime;
use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class GoodsReceivedNoteAttributesRead implements Parsable
{
    /**
     * @var DateTime|null $date The date property
    */
    private ?DateTime $date = null;

    /**
     * @var string|null $description The description property
    */
    private ?string $description = null;

    /**
     * @var int|null $grnNumber The grnNumber property
    */
    private ?int $grnNumber = null;

    /**
     * @var int|null $itemNumber The itemNumber property
    */
    private ?int $itemNumber = null;

    /**
     * @var float|null $qtyOnOrder The qtyOnOrder property
    */
    private ?float $qtyOnOrder = null;

    /**
     * @var float|null $qtyReceived The qtyReceived property
    */
    private ?float $qtyReceived = null;

    /**
     * @var DateTime|null $recordCreateDate The recordCreateDate property
    */
    private ?DateTime $recordCreateDate = null;

    /**
     * @var int|null $recordDeleted The recordDeleted property
    */
    private ?int $recordDeleted = null;

    /**
     * @var DateTime|null $recordModifyDate The recordModifyDate property
    */
    private ?DateTime $recordModifyDate = null;

    /**
     * @var string|null $supplierGrnNumber The supplierGrnNumber property
    */
    private ?string $supplierGrnNumber = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return GoodsReceivedNoteAttributesRead
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): GoodsReceivedNoteAttributesRead {
        return new GoodsReceivedNoteAttributesRead();
    }

    /**
     * Gets the date property value. The date property
     * @return DateTime|null
    */
    public function getDate(): ?DateTime {
        return $this->date;
    }

    /**
     * Gets the description property value. The description property
     * @return string|null
    */
    public function getDescription(): ?string {
        return $this->description;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'date' => fn(ParseNode $n) => $o->setDate($n->getDateTimeValue()),
            'description' => fn(ParseNode $n) => $o->setDescription($n->getStringValue()),
            'grnNumber' => fn(ParseNode $n) => $o->setGrnNumber($n->getIntegerValue()),
            'itemNumber' => fn(ParseNode $n) => $o->setItemNumber($n->getIntegerValue()),
            'qtyOnOrder' => fn(ParseNode $n) => $o->setQtyOnOrder($n->getFloatValue()),
            'qtyReceived' => fn(ParseNode $n) => $o->setQtyReceived($n->getFloatValue()),
            'recordCreateDate' => fn(ParseNode $n) => $o->setRecordCreateDate($n->getDateTimeValue()),
            'recordDeleted' => fn(ParseNode $n) => $o->setRecordDeleted($n->getIntegerValue()),
            'recordModifyDate' => fn(ParseNode $n) => $o->setRecordModifyDate($n->getDateTimeValue()),
            'supplierGrnNumber' => fn(ParseNode $n) => $o->setSupplierGrnNumber($n->getStringValue()),
        ];
    }

    /**
     * Gets the grnNumber property value. The grnNumber property
     * @return int|null
    */
    public function getGrnNumber(): ?int {
        return $this->grnNumber;
    }

    /**
     * Gets the itemNumber property value. The itemNumber property
     * @return int|null
    */
    public function getItemNumber(): ?int {
        return $this->itemNumber;
    }

    /**
     * Gets the qtyOnOrder property value. The qtyOnOrder property
     * @return float|null
    */
    public function getQtyOnOrder(): ?float {
        return $this->qtyOnOrder;
    }

    /**
     * Gets the qtyReceived property value. The qtyReceived property
     * @return float|null
    */
    public function getQtyReceived(): ?float {
        return $this->qtyReceived;
    }

    /**
     * Gets the recordCreateDate property value. The recordCreateDate property
     * @return DateTime|null
    */
    public function getRecordCreateDate(): ?DateTime {
        return $this->recordCreateDate;
    }

    /**
     * Gets the recordDeleted property value. The recordDeleted property
     * @return int|null
    */
    public function getRecordDeleted(): ?int {
        return $this->recordDeleted;
    }

    /**
     * Gets the recordModifyDate property value. The recordModifyDate property
     * @return DateTime|null
    */
    public function getRecordModifyDate(): ?DateTime {
        return $this->recordModifyDate;
    }

    /**
     * Gets the supplierGrnNumber property value. The supplierGrnNumber property
     * @return string|null
    */
    public function getSupplierGrnNumber(): ?string {
        return $this->supplierGrnNumber;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeDateTimeValue('date', $this->getDate());
        $writer->writeStringValue('description', $this->getDescription());
        $writer->writeIntegerValue('grnNumber', $this->getGrnNumber());
        $writer->writeIntegerValue('itemNumber', $this->getItemNumber());
        $writer->writeFloatValue('qtyOnOrder', $this->getQtyOnOrder());
        $writer->writeFloatValue('qtyReceived', $this->getQtyReceived());
        $writer->writeDateTimeValue('recordCreateDate', $this->getRecordCreateDate());
        $writer->writeIntegerValue('recordDeleted', $this->getRecordDeleted());
        $writer->writeDateTimeValue('recordModifyDate', $this->getRecordModifyDate());
        $writer->writeStringValue('supplierGrnNumber', $this->getSupplierGrnNumber());
    }

    /**
     * Sets the date property value. The date property
     * @param DateTime|null $value Value to set for the date property.
    */
    public function setDate(?DateTime $value): void {
        $this->date = $value;
    }

    /**
     * Sets the description property value. The description property
     * @param string|null $value Value to set for the description property.
    */
    public function setDescription(?string $value): void {
        $this->description = $value;
    }

    /**
     * Sets the grnNumber property value. The grnNumber property
     * @param int|null $value Value to set for the grnNumber property.
    */
    public function setGrnNumber(?int $value): void {
        $this->grnNumber = $value;
    }

    /**
     * Sets the itemNumber property value. The itemNumber property
     * @param int|null $value Value to set for the itemNumber property.
    */
    public function setItemNumber(?int $value): void {
        $this->itemNumber = $value;
    }

    /**
     * Sets the qtyOnOrder property value. The qtyOnOrder property
     * @param float|null $value Value to set for the qtyOnOrder property.
    */
    public function setQtyOnOrder(?float $value): void {
        $this->qtyOnOrder = $value;
    }

    /**
     * Sets the qtyReceived property value. The qtyReceived property
     * @param float|null $value Value to set for the qtyReceived property.
    */
    public function setQtyReceived(?float $value): void {
        $this->qtyReceived = $value;
    }

    /**
     * Sets the recordCreateDate property value. The recordCreateDate property
     * @param DateTime|null $value Value to set for the recordCreateDate property.
    */
    public function setRecordCreateDate(?DateTime $value): void {
        $this->recordCreateDate = $value;
    }

    /**
     * Sets the recordDeleted property value. The recordDeleted property
     * @param int|null $value Value to set for the recordDeleted property.
    */
    public function setRecordDeleted(?int $value): void {
        $this->recordDeleted = $value;
    }

    /**
     * Sets the recordModifyDate property value. The recordModifyDate property
     * @param DateTime|null $value Value to set for the recordModifyDate property.
    */
    public function setRecordModifyDate(?DateTime $value): void {
        $this->recordModifyDate = $value;
    }

    /**
     * Sets the supplierGrnNumber property value. The supplierGrnNumber property
     * @param string|null $value Value to set for the supplierGrnNumber property.
    */
    public function setSupplierGrnNumber(?string $value): void {
        $this->supplierGrnNumber = $value;
    }

}
