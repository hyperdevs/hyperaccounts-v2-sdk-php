<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class TransactionHeaderCreateJournalReliantChilds implements Parsable
{
    /**
     * @var array<TransactionSplitCreateJournalPostDto>|null $splits The splits property
    */
    private ?array $splits = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return TransactionHeaderCreateJournalReliantChilds
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): TransactionHeaderCreateJournalReliantChilds {
        return new TransactionHeaderCreateJournalReliantChilds();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'splits' => fn(ParseNode $n) => $o->setSplits($n->getCollectionOfObjectValues([TransactionSplitCreateJournalPostDto::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the splits property value. The splits property
     * @return array<TransactionSplitCreateJournalPostDto>|null
    */
    public function getSplits(): ?array {
        return $this->splits;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeCollectionOfObjectValues('splits', $this->getSplits());
    }

    /**
     * Sets the splits property value. The splits property
     * @param array<TransactionSplitCreateJournalPostDto>|null $value Value to set for the splits property.
    */
    public function setSplits(?array $value): void {
        $this->splits = $value;
    }

}
