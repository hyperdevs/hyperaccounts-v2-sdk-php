<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use DateTime;
use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class TransactionSplitAttributesRead implements Parsable
{
    /**
     * @var string|null $accountRef The accountRef property
    */
    private ?string $accountRef = null;

    /**
     * @var float|null $aged30 The aged30 property
    */
    private ?float $aged30 = null;

    /**
     * @var float|null $aged60 The aged60 property
    */
    private ?float $aged60 = null;

    /**
     * @var float|null $aged90 The aged90 property
    */
    private ?float $aged90 = null;

    /**
     * @var float|null $agedBalance The agedBalance property
    */
    private ?float $agedBalance = null;

    /**
     * @var float|null $agedCum30 The agedCum30 property
    */
    private ?float $agedCum30 = null;

    /**
     * @var float|null $agedCum60 The agedCum60 property
    */
    private ?float $agedCum60 = null;

    /**
     * @var float|null $agedCum90 The agedCum90 property
    */
    private ?float $agedCum90 = null;

    /**
     * @var float|null $agedCumCurrent The agedCumCurrent property
    */
    private ?float $agedCumCurrent = null;

    /**
     * @var float|null $agedCurrent The agedCurrent property
    */
    private ?float $agedCurrent = null;

    /**
     * @var float|null $agedFuture The agedFuture property
    */
    private ?float $agedFuture = null;

    /**
     * @var float|null $agedOlder The agedOlder property
    */
    private ?float $agedOlder = null;

    /**
     * @var float|null $amountPaid The amountPaid property
    */
    private ?float $amountPaid = null;

    /**
     * @var string|null $bankCode The bankCode property
    */
    private ?string $bankCode = null;

    /**
     * @var TransactionSplitAttributesRead_bankFlag|null $bankFlag The bankFlag property
    */
    private ?TransactionSplitAttributesRead_bankFlag $bankFlag = null;

    /**
     * @var DateTime|null $date The date property
    */
    private ?DateTime $date = null;

    /**
     * @var DateTime|null $dateEntered The dateEntered property
    */
    private ?DateTime $dateEntered = null;

    /**
     * @var int|null $dateFlag The dateFlag property
    */
    private ?int $dateFlag = null;

    /**
     * @var int|null $deletedFlag The deletedFlag property
    */
    private ?int $deletedFlag = null;

    /**
     * @var string|null $deptName The deptName property
    */
    private ?string $deptName = null;

    /**
     * @var int|null $deptNumber The deptNumber property
    */
    private ?int $deptNumber = null;

    /**
     * @var string|null $details The details property
    */
    private ?string $details = null;

    /**
     * @var int|null $disputeCode The disputeCode property
    */
    private ?int $disputeCode = null;

    /**
     * @var int|null $disputed The disputed property
    */
    private ?int $disputed = null;

    /**
     * @var string|null $extraRef The extraRef property
    */
    private ?string $extraRef = null;

    /**
     * @var float|null $foreignAged30 The foreignAged30 property
    */
    private ?float $foreignAged30 = null;

    /**
     * @var float|null $foreignAged60 The foreignAged60 property
    */
    private ?float $foreignAged60 = null;

    /**
     * @var float|null $foreignAged90 The foreignAged90 property
    */
    private ?float $foreignAged90 = null;

    /**
     * @var float|null $foreignAgedBalance The foreignAgedBalance property
    */
    private ?float $foreignAgedBalance = null;

    /**
     * @var float|null $foreignAgedCum30 The foreignAgedCum30 property
    */
    private ?float $foreignAgedCum30 = null;

    /**
     * @var float|null $foreignAgedCum60 The foreignAgedCum60 property
    */
    private ?float $foreignAgedCum60 = null;

    /**
     * @var float|null $foreignAgedCum90 The foreignAgedCum90 property
    */
    private ?float $foreignAgedCum90 = null;

    /**
     * @var float|null $foreignAgedCumCurrent The foreignAgedCumCurrent property
    */
    private ?float $foreignAgedCumCurrent = null;

    /**
     * @var float|null $foreignAgedCurrent The foreignAgedCurrent property
    */
    private ?float $foreignAgedCurrent = null;

    /**
     * @var float|null $foreignAgedFuture The foreignAgedFuture property
    */
    private ?float $foreignAgedFuture = null;

    /**
     * @var float|null $foreignAgedOlder The foreignAgedOlder property
    */
    private ?float $foreignAgedOlder = null;

    /**
     * @var float|null $foreignAmountPaid The foreignAmountPaid property
    */
    private ?float $foreignAmountPaid = null;

    /**
     * @var float|null $foreignGrossAmount The foreignGrossAmount property
    */
    private ?float $foreignGrossAmount = null;

    /**
     * @var float|null $foreignNetAmount The foreignNetAmount property
    */
    private ?float $foreignNetAmount = null;

    /**
     * @var float|null $foreignOutstanding The foreignOutstanding property
    */
    private ?float $foreignOutstanding = null;

    /**
     * @var float|null $foreignTaxAmount The foreignTaxAmount property
    */
    private ?float $foreignTaxAmount = null;

    /**
     * @var int|null $fundId The fundId property
    */
    private ?int $fundId = null;

    /**
     * @var int|null $gasdsClaimSubmitted The gasdsClaimSubmitted property
    */
    private ?int $gasdsClaimSubmitted = null;

    /**
     * @var int|null $giftAid The giftAid property
    */
    private ?int $giftAid = null;

    /**
     * @var float|null $grossAmount The grossAmount property
    */
    private ?float $grossAmount = null;

    /**
     * @var TransactionSplitAttributesRead_hasExternalLink|null $hasExternalLink The hasExternalLink property
    */
    private ?TransactionSplitAttributesRead_hasExternalLink $hasExternalLink = null;

    /**
     * @var string|null $invRef The invRef property
    */
    private ?string $invRef = null;

    /**
     * @var float|null $netAmount The netAmount property
    */
    private ?float $netAmount = null;

    /**
     * @var string|null $ossCountryOfVat The ossCountryOfVat property
    */
    private ?string $ossCountryOfVat = null;

    /**
     * @var int|null $ossReportingType The ossReportingType property
    */
    private ?int $ossReportingType = null;

    /**
     * @var string|null $ossReportingTypeName The ossReportingTypeName property
    */
    private ?string $ossReportingTypeName = null;

    /**
     * @var float|null $outstanding The outstanding property
    */
    private ?float $outstanding = null;

    /**
     * @var TransactionSplitAttributesRead_paidFlag|null $paidFlag The paidFlag property
    */
    private ?TransactionSplitAttributesRead_paidFlag $paidFlag = null;

    /**
     * @var string|null $paidStatus The paidStatus property
    */
    private ?string $paidStatus = null;

    /**
     * @var float|null $payment The payment property
    */
    private ?float $payment = null;

    /**
     * @var DateTime|null $recordCreateDate The recordCreateDate property
    */
    private ?DateTime $recordCreateDate = null;

    /**
     * @var int|null $recordDeleted The recordDeleted property
    */
    private ?int $recordDeleted = null;

    /**
     * @var DateTime|null $recordModifyDate The recordModifyDate property
    */
    private ?DateTime $recordModifyDate = null;

    /**
     * @var string|null $rtdFlag The rtdFlag property
    */
    private ?string $rtdFlag = null;

    /**
     * @var int|null $smallDonation The smallDonation property
    */
    private ?int $smallDonation = null;

    /**
     * @var string|null $stmtText The stmtText property
    */
    private ?string $stmtText = null;

    /**
     * @var float|null $taxAmount The taxAmount property
    */
    private ?float $taxAmount = null;

    /**
     * @var TransactionSplitAttributesRead_taxCode|null $taxCode The taxCode property
    */
    private ?TransactionSplitAttributesRead_taxCode $taxCode = null;

    /**
     * @var int|null $tranNumber The tranNumber property
    */
    private ?int $tranNumber = null;

    /**
     * @var TransactionSplitAttributesRead_type|null $type The type property
    */
    private ?TransactionSplitAttributesRead_type $type = null;

    /**
     * @var string|null $userName The userName property
    */
    private ?string $userName = null;

    /**
     * @var TransactionSplitAttributesRead_vatFlag|null $vatFlag The vatFlag property
    */
    private ?TransactionSplitAttributesRead_vatFlag $vatFlag = null;

    /**
     * @var int|null $vatFlagCode The vatFlagCode property
    */
    private ?int $vatFlagCode = null;

    /**
     * @var int|null $vatLedgerReturnId The vatLedgerReturnId property
    */
    private ?int $vatLedgerReturnId = null;

    /**
     * @var DateTime|null $vatReconciledDate The vatReconciledDate property
    */
    private ?DateTime $vatReconciledDate = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return TransactionSplitAttributesRead
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): TransactionSplitAttributesRead {
        return new TransactionSplitAttributesRead();
    }

    /**
     * Gets the accountRef property value. The accountRef property
     * @return string|null
    */
    public function getAccountRef(): ?string {
        return $this->accountRef;
    }

    /**
     * Gets the aged30 property value. The aged30 property
     * @return float|null
    */
    public function getAged30(): ?float {
        return $this->aged30;
    }

    /**
     * Gets the aged60 property value. The aged60 property
     * @return float|null
    */
    public function getAged60(): ?float {
        return $this->aged60;
    }

    /**
     * Gets the aged90 property value. The aged90 property
     * @return float|null
    */
    public function getAged90(): ?float {
        return $this->aged90;
    }

    /**
     * Gets the agedBalance property value. The agedBalance property
     * @return float|null
    */
    public function getAgedBalance(): ?float {
        return $this->agedBalance;
    }

    /**
     * Gets the agedCum30 property value. The agedCum30 property
     * @return float|null
    */
    public function getAgedCum30(): ?float {
        return $this->agedCum30;
    }

    /**
     * Gets the agedCum60 property value. The agedCum60 property
     * @return float|null
    */
    public function getAgedCum60(): ?float {
        return $this->agedCum60;
    }

    /**
     * Gets the agedCum90 property value. The agedCum90 property
     * @return float|null
    */
    public function getAgedCum90(): ?float {
        return $this->agedCum90;
    }

    /**
     * Gets the agedCumCurrent property value. The agedCumCurrent property
     * @return float|null
    */
    public function getAgedCumCurrent(): ?float {
        return $this->agedCumCurrent;
    }

    /**
     * Gets the agedCurrent property value. The agedCurrent property
     * @return float|null
    */
    public function getAgedCurrent(): ?float {
        return $this->agedCurrent;
    }

    /**
     * Gets the agedFuture property value. The agedFuture property
     * @return float|null
    */
    public function getAgedFuture(): ?float {
        return $this->agedFuture;
    }

    /**
     * Gets the agedOlder property value. The agedOlder property
     * @return float|null
    */
    public function getAgedOlder(): ?float {
        return $this->agedOlder;
    }

    /**
     * Gets the amountPaid property value. The amountPaid property
     * @return float|null
    */
    public function getAmountPaid(): ?float {
        return $this->amountPaid;
    }

    /**
     * Gets the bankCode property value. The bankCode property
     * @return string|null
    */
    public function getBankCode(): ?string {
        return $this->bankCode;
    }

    /**
     * Gets the bankFlag property value. The bankFlag property
     * @return TransactionSplitAttributesRead_bankFlag|null
    */
    public function getBankFlag(): ?TransactionSplitAttributesRead_bankFlag {
        return $this->bankFlag;
    }

    /**
     * Gets the date property value. The date property
     * @return DateTime|null
    */
    public function getDate(): ?DateTime {
        return $this->date;
    }

    /**
     * Gets the dateEntered property value. The dateEntered property
     * @return DateTime|null
    */
    public function getDateEntered(): ?DateTime {
        return $this->dateEntered;
    }

    /**
     * Gets the dateFlag property value. The dateFlag property
     * @return int|null
    */
    public function getDateFlag(): ?int {
        return $this->dateFlag;
    }

    /**
     * Gets the deletedFlag property value. The deletedFlag property
     * @return int|null
    */
    public function getDeletedFlag(): ?int {
        return $this->deletedFlag;
    }

    /**
     * Gets the deptName property value. The deptName property
     * @return string|null
    */
    public function getDeptName(): ?string {
        return $this->deptName;
    }

    /**
     * Gets the deptNumber property value. The deptNumber property
     * @return int|null
    */
    public function getDeptNumber(): ?int {
        return $this->deptNumber;
    }

    /**
     * Gets the details property value. The details property
     * @return string|null
    */
    public function getDetails(): ?string {
        return $this->details;
    }

    /**
     * Gets the disputeCode property value. The disputeCode property
     * @return int|null
    */
    public function getDisputeCode(): ?int {
        return $this->disputeCode;
    }

    /**
     * Gets the disputed property value. The disputed property
     * @return int|null
    */
    public function getDisputed(): ?int {
        return $this->disputed;
    }

    /**
     * Gets the extraRef property value. The extraRef property
     * @return string|null
    */
    public function getExtraRef(): ?string {
        return $this->extraRef;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'accountRef' => fn(ParseNode $n) => $o->setAccountRef($n->getStringValue()),
            'aged30' => fn(ParseNode $n) => $o->setAged30($n->getFloatValue()),
            'aged60' => fn(ParseNode $n) => $o->setAged60($n->getFloatValue()),
            'aged90' => fn(ParseNode $n) => $o->setAged90($n->getFloatValue()),
            'agedBalance' => fn(ParseNode $n) => $o->setAgedBalance($n->getFloatValue()),
            'agedCum30' => fn(ParseNode $n) => $o->setAgedCum30($n->getFloatValue()),
            'agedCum60' => fn(ParseNode $n) => $o->setAgedCum60($n->getFloatValue()),
            'agedCum90' => fn(ParseNode $n) => $o->setAgedCum90($n->getFloatValue()),
            'agedCumCurrent' => fn(ParseNode $n) => $o->setAgedCumCurrent($n->getFloatValue()),
            'agedCurrent' => fn(ParseNode $n) => $o->setAgedCurrent($n->getFloatValue()),
            'agedFuture' => fn(ParseNode $n) => $o->setAgedFuture($n->getFloatValue()),
            'agedOlder' => fn(ParseNode $n) => $o->setAgedOlder($n->getFloatValue()),
            'amountPaid' => fn(ParseNode $n) => $o->setAmountPaid($n->getFloatValue()),
            'bankCode' => fn(ParseNode $n) => $o->setBankCode($n->getStringValue()),
            'bankFlag' => fn(ParseNode $n) => $o->setBankFlag($n->getEnumValue(TransactionSplitAttributesRead_bankFlag::class)),
            'date' => fn(ParseNode $n) => $o->setDate($n->getDateTimeValue()),
            'dateEntered' => fn(ParseNode $n) => $o->setDateEntered($n->getDateTimeValue()),
            'dateFlag' => fn(ParseNode $n) => $o->setDateFlag($n->getIntegerValue()),
            'deletedFlag' => fn(ParseNode $n) => $o->setDeletedFlag($n->getIntegerValue()),
            'deptName' => fn(ParseNode $n) => $o->setDeptName($n->getStringValue()),
            'deptNumber' => fn(ParseNode $n) => $o->setDeptNumber($n->getIntegerValue()),
            'details' => fn(ParseNode $n) => $o->setDetails($n->getStringValue()),
            'disputeCode' => fn(ParseNode $n) => $o->setDisputeCode($n->getIntegerValue()),
            'disputed' => fn(ParseNode $n) => $o->setDisputed($n->getIntegerValue()),
            'extraRef' => fn(ParseNode $n) => $o->setExtraRef($n->getStringValue()),
            'foreignAged30' => fn(ParseNode $n) => $o->setForeignAged30($n->getFloatValue()),
            'foreignAged60' => fn(ParseNode $n) => $o->setForeignAged60($n->getFloatValue()),
            'foreignAged90' => fn(ParseNode $n) => $o->setForeignAged90($n->getFloatValue()),
            'foreignAgedBalance' => fn(ParseNode $n) => $o->setForeignAgedBalance($n->getFloatValue()),
            'foreignAgedCum30' => fn(ParseNode $n) => $o->setForeignAgedCum30($n->getFloatValue()),
            'foreignAgedCum60' => fn(ParseNode $n) => $o->setForeignAgedCum60($n->getFloatValue()),
            'foreignAgedCum90' => fn(ParseNode $n) => $o->setForeignAgedCum90($n->getFloatValue()),
            'foreignAgedCumCurrent' => fn(ParseNode $n) => $o->setForeignAgedCumCurrent($n->getFloatValue()),
            'foreignAgedCurrent' => fn(ParseNode $n) => $o->setForeignAgedCurrent($n->getFloatValue()),
            'foreignAgedFuture' => fn(ParseNode $n) => $o->setForeignAgedFuture($n->getFloatValue()),
            'foreignAgedOlder' => fn(ParseNode $n) => $o->setForeignAgedOlder($n->getFloatValue()),
            'foreignAmountPaid' => fn(ParseNode $n) => $o->setForeignAmountPaid($n->getFloatValue()),
            'foreignGrossAmount' => fn(ParseNode $n) => $o->setForeignGrossAmount($n->getFloatValue()),
            'foreignNetAmount' => fn(ParseNode $n) => $o->setForeignNetAmount($n->getFloatValue()),
            'foreignOutstanding' => fn(ParseNode $n) => $o->setForeignOutstanding($n->getFloatValue()),
            'foreignTaxAmount' => fn(ParseNode $n) => $o->setForeignTaxAmount($n->getFloatValue()),
            'fundId' => fn(ParseNode $n) => $o->setFundId($n->getIntegerValue()),
            'gasdsClaimSubmitted' => fn(ParseNode $n) => $o->setGasdsClaimSubmitted($n->getIntegerValue()),
            'giftAid' => fn(ParseNode $n) => $o->setGiftAid($n->getIntegerValue()),
            'grossAmount' => fn(ParseNode $n) => $o->setGrossAmount($n->getFloatValue()),
            'hasExternalLink' => fn(ParseNode $n) => $o->setHasExternalLink($n->getEnumValue(TransactionSplitAttributesRead_hasExternalLink::class)),
            'invRef' => fn(ParseNode $n) => $o->setInvRef($n->getStringValue()),
            'netAmount' => fn(ParseNode $n) => $o->setNetAmount($n->getFloatValue()),
            'ossCountryOfVat' => fn(ParseNode $n) => $o->setOssCountryOfVat($n->getStringValue()),
            'ossReportingType' => fn(ParseNode $n) => $o->setOssReportingType($n->getIntegerValue()),
            'ossReportingTypeName' => fn(ParseNode $n) => $o->setOssReportingTypeName($n->getStringValue()),
            'outstanding' => fn(ParseNode $n) => $o->setOutstanding($n->getFloatValue()),
            'paidFlag' => fn(ParseNode $n) => $o->setPaidFlag($n->getEnumValue(TransactionSplitAttributesRead_paidFlag::class)),
            'paidStatus' => fn(ParseNode $n) => $o->setPaidStatus($n->getStringValue()),
            'payment' => fn(ParseNode $n) => $o->setPayment($n->getFloatValue()),
            'recordCreateDate' => fn(ParseNode $n) => $o->setRecordCreateDate($n->getDateTimeValue()),
            'recordDeleted' => fn(ParseNode $n) => $o->setRecordDeleted($n->getIntegerValue()),
            'recordModifyDate' => fn(ParseNode $n) => $o->setRecordModifyDate($n->getDateTimeValue()),
            'rtdFlag' => fn(ParseNode $n) => $o->setRtdFlag($n->getStringValue()),
            'smallDonation' => fn(ParseNode $n) => $o->setSmallDonation($n->getIntegerValue()),
            'stmtText' => fn(ParseNode $n) => $o->setStmtText($n->getStringValue()),
            'taxAmount' => fn(ParseNode $n) => $o->setTaxAmount($n->getFloatValue()),
            'taxCode' => fn(ParseNode $n) => $o->setTaxCode($n->getEnumValue(TransactionSplitAttributesRead_taxCode::class)),
            'tranNumber' => fn(ParseNode $n) => $o->setTranNumber($n->getIntegerValue()),
            'type' => fn(ParseNode $n) => $o->setType($n->getEnumValue(TransactionSplitAttributesRead_type::class)),
            'userName' => fn(ParseNode $n) => $o->setUserName($n->getStringValue()),
            'vatFlag' => fn(ParseNode $n) => $o->setVatFlag($n->getEnumValue(TransactionSplitAttributesRead_vatFlag::class)),
            'vatFlagCode' => fn(ParseNode $n) => $o->setVatFlagCode($n->getIntegerValue()),
            'vatLedgerReturnId' => fn(ParseNode $n) => $o->setVatLedgerReturnId($n->getIntegerValue()),
            'vatReconciledDate' => fn(ParseNode $n) => $o->setVatReconciledDate($n->getDateTimeValue()),
        ];
    }

    /**
     * Gets the foreignAged30 property value. The foreignAged30 property
     * @return float|null
    */
    public function getForeignAged30(): ?float {
        return $this->foreignAged30;
    }

    /**
     * Gets the foreignAged60 property value. The foreignAged60 property
     * @return float|null
    */
    public function getForeignAged60(): ?float {
        return $this->foreignAged60;
    }

    /**
     * Gets the foreignAged90 property value. The foreignAged90 property
     * @return float|null
    */
    public function getForeignAged90(): ?float {
        return $this->foreignAged90;
    }

    /**
     * Gets the foreignAgedBalance property value. The foreignAgedBalance property
     * @return float|null
    */
    public function getForeignAgedBalance(): ?float {
        return $this->foreignAgedBalance;
    }

    /**
     * Gets the foreignAgedCum30 property value. The foreignAgedCum30 property
     * @return float|null
    */
    public function getForeignAgedCum30(): ?float {
        return $this->foreignAgedCum30;
    }

    /**
     * Gets the foreignAgedCum60 property value. The foreignAgedCum60 property
     * @return float|null
    */
    public function getForeignAgedCum60(): ?float {
        return $this->foreignAgedCum60;
    }

    /**
     * Gets the foreignAgedCum90 property value. The foreignAgedCum90 property
     * @return float|null
    */
    public function getForeignAgedCum90(): ?float {
        return $this->foreignAgedCum90;
    }

    /**
     * Gets the foreignAgedCumCurrent property value. The foreignAgedCumCurrent property
     * @return float|null
    */
    public function getForeignAgedCumCurrent(): ?float {
        return $this->foreignAgedCumCurrent;
    }

    /**
     * Gets the foreignAgedCurrent property value. The foreignAgedCurrent property
     * @return float|null
    */
    public function getForeignAgedCurrent(): ?float {
        return $this->foreignAgedCurrent;
    }

    /**
     * Gets the foreignAgedFuture property value. The foreignAgedFuture property
     * @return float|null
    */
    public function getForeignAgedFuture(): ?float {
        return $this->foreignAgedFuture;
    }

    /**
     * Gets the foreignAgedOlder property value. The foreignAgedOlder property
     * @return float|null
    */
    public function getForeignAgedOlder(): ?float {
        return $this->foreignAgedOlder;
    }

    /**
     * Gets the foreignAmountPaid property value. The foreignAmountPaid property
     * @return float|null
    */
    public function getForeignAmountPaid(): ?float {
        return $this->foreignAmountPaid;
    }

    /**
     * Gets the foreignGrossAmount property value. The foreignGrossAmount property
     * @return float|null
    */
    public function getForeignGrossAmount(): ?float {
        return $this->foreignGrossAmount;
    }

    /**
     * Gets the foreignNetAmount property value. The foreignNetAmount property
     * @return float|null
    */
    public function getForeignNetAmount(): ?float {
        return $this->foreignNetAmount;
    }

    /**
     * Gets the foreignOutstanding property value. The foreignOutstanding property
     * @return float|null
    */
    public function getForeignOutstanding(): ?float {
        return $this->foreignOutstanding;
    }

    /**
     * Gets the foreignTaxAmount property value. The foreignTaxAmount property
     * @return float|null
    */
    public function getForeignTaxAmount(): ?float {
        return $this->foreignTaxAmount;
    }

    /**
     * Gets the fundId property value. The fundId property
     * @return int|null
    */
    public function getFundId(): ?int {
        return $this->fundId;
    }

    /**
     * Gets the gasdsClaimSubmitted property value. The gasdsClaimSubmitted property
     * @return int|null
    */
    public function getGasdsClaimSubmitted(): ?int {
        return $this->gasdsClaimSubmitted;
    }

    /**
     * Gets the giftAid property value. The giftAid property
     * @return int|null
    */
    public function getGiftAid(): ?int {
        return $this->giftAid;
    }

    /**
     * Gets the grossAmount property value. The grossAmount property
     * @return float|null
    */
    public function getGrossAmount(): ?float {
        return $this->grossAmount;
    }

    /**
     * Gets the hasExternalLink property value. The hasExternalLink property
     * @return TransactionSplitAttributesRead_hasExternalLink|null
    */
    public function getHasExternalLink(): ?TransactionSplitAttributesRead_hasExternalLink {
        return $this->hasExternalLink;
    }

    /**
     * Gets the invRef property value. The invRef property
     * @return string|null
    */
    public function getInvRef(): ?string {
        return $this->invRef;
    }

    /**
     * Gets the netAmount property value. The netAmount property
     * @return float|null
    */
    public function getNetAmount(): ?float {
        return $this->netAmount;
    }

    /**
     * Gets the ossCountryOfVat property value. The ossCountryOfVat property
     * @return string|null
    */
    public function getOssCountryOfVat(): ?string {
        return $this->ossCountryOfVat;
    }

    /**
     * Gets the ossReportingType property value. The ossReportingType property
     * @return int|null
    */
    public function getOssReportingType(): ?int {
        return $this->ossReportingType;
    }

    /**
     * Gets the ossReportingTypeName property value. The ossReportingTypeName property
     * @return string|null
    */
    public function getOssReportingTypeName(): ?string {
        return $this->ossReportingTypeName;
    }

    /**
     * Gets the outstanding property value. The outstanding property
     * @return float|null
    */
    public function getOutstanding(): ?float {
        return $this->outstanding;
    }

    /**
     * Gets the paidFlag property value. The paidFlag property
     * @return TransactionSplitAttributesRead_paidFlag|null
    */
    public function getPaidFlag(): ?TransactionSplitAttributesRead_paidFlag {
        return $this->paidFlag;
    }

    /**
     * Gets the paidStatus property value. The paidStatus property
     * @return string|null
    */
    public function getPaidStatus(): ?string {
        return $this->paidStatus;
    }

    /**
     * Gets the payment property value. The payment property
     * @return float|null
    */
    public function getPayment(): ?float {
        return $this->payment;
    }

    /**
     * Gets the recordCreateDate property value. The recordCreateDate property
     * @return DateTime|null
    */
    public function getRecordCreateDate(): ?DateTime {
        return $this->recordCreateDate;
    }

    /**
     * Gets the recordDeleted property value. The recordDeleted property
     * @return int|null
    */
    public function getRecordDeleted(): ?int {
        return $this->recordDeleted;
    }

    /**
     * Gets the recordModifyDate property value. The recordModifyDate property
     * @return DateTime|null
    */
    public function getRecordModifyDate(): ?DateTime {
        return $this->recordModifyDate;
    }

    /**
     * Gets the rtdFlag property value. The rtdFlag property
     * @return string|null
    */
    public function getRtdFlag(): ?string {
        return $this->rtdFlag;
    }

    /**
     * Gets the smallDonation property value. The smallDonation property
     * @return int|null
    */
    public function getSmallDonation(): ?int {
        return $this->smallDonation;
    }

    /**
     * Gets the stmtText property value. The stmtText property
     * @return string|null
    */
    public function getStmtText(): ?string {
        return $this->stmtText;
    }

    /**
     * Gets the taxAmount property value. The taxAmount property
     * @return float|null
    */
    public function getTaxAmount(): ?float {
        return $this->taxAmount;
    }

    /**
     * Gets the taxCode property value. The taxCode property
     * @return TransactionSplitAttributesRead_taxCode|null
    */
    public function getTaxCode(): ?TransactionSplitAttributesRead_taxCode {
        return $this->taxCode;
    }

    /**
     * Gets the tranNumber property value. The tranNumber property
     * @return int|null
    */
    public function getTranNumber(): ?int {
        return $this->tranNumber;
    }

    /**
     * Gets the type property value. The type property
     * @return TransactionSplitAttributesRead_type|null
    */
    public function getType(): ?TransactionSplitAttributesRead_type {
        return $this->type;
    }

    /**
     * Gets the userName property value. The userName property
     * @return string|null
    */
    public function getUserName(): ?string {
        return $this->userName;
    }

    /**
     * Gets the vatFlag property value. The vatFlag property
     * @return TransactionSplitAttributesRead_vatFlag|null
    */
    public function getVatFlag(): ?TransactionSplitAttributesRead_vatFlag {
        return $this->vatFlag;
    }

    /**
     * Gets the vatFlagCode property value. The vatFlagCode property
     * @return int|null
    */
    public function getVatFlagCode(): ?int {
        return $this->vatFlagCode;
    }

    /**
     * Gets the vatLedgerReturnId property value. The vatLedgerReturnId property
     * @return int|null
    */
    public function getVatLedgerReturnId(): ?int {
        return $this->vatLedgerReturnId;
    }

    /**
     * Gets the vatReconciledDate property value. The vatReconciledDate property
     * @return DateTime|null
    */
    public function getVatReconciledDate(): ?DateTime {
        return $this->vatReconciledDate;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeStringValue('accountRef', $this->getAccountRef());
        $writer->writeFloatValue('aged30', $this->getAged30());
        $writer->writeFloatValue('aged60', $this->getAged60());
        $writer->writeFloatValue('aged90', $this->getAged90());
        $writer->writeFloatValue('agedBalance', $this->getAgedBalance());
        $writer->writeFloatValue('agedCum30', $this->getAgedCum30());
        $writer->writeFloatValue('agedCum60', $this->getAgedCum60());
        $writer->writeFloatValue('agedCum90', $this->getAgedCum90());
        $writer->writeFloatValue('agedCumCurrent', $this->getAgedCumCurrent());
        $writer->writeFloatValue('agedCurrent', $this->getAgedCurrent());
        $writer->writeFloatValue('agedFuture', $this->getAgedFuture());
        $writer->writeFloatValue('agedOlder', $this->getAgedOlder());
        $writer->writeFloatValue('amountPaid', $this->getAmountPaid());
        $writer->writeStringValue('bankCode', $this->getBankCode());
        $writer->writeEnumValue('bankFlag', $this->getBankFlag());
        $writer->writeDateTimeValue('date', $this->getDate());
        $writer->writeDateTimeValue('dateEntered', $this->getDateEntered());
        $writer->writeIntegerValue('dateFlag', $this->getDateFlag());
        $writer->writeIntegerValue('deletedFlag', $this->getDeletedFlag());
        $writer->writeStringValue('deptName', $this->getDeptName());
        $writer->writeIntegerValue('deptNumber', $this->getDeptNumber());
        $writer->writeStringValue('details', $this->getDetails());
        $writer->writeIntegerValue('disputeCode', $this->getDisputeCode());
        $writer->writeIntegerValue('disputed', $this->getDisputed());
        $writer->writeStringValue('extraRef', $this->getExtraRef());
        $writer->writeFloatValue('foreignAged30', $this->getForeignAged30());
        $writer->writeFloatValue('foreignAged60', $this->getForeignAged60());
        $writer->writeFloatValue('foreignAged90', $this->getForeignAged90());
        $writer->writeFloatValue('foreignAgedBalance', $this->getForeignAgedBalance());
        $writer->writeFloatValue('foreignAgedCum30', $this->getForeignAgedCum30());
        $writer->writeFloatValue('foreignAgedCum60', $this->getForeignAgedCum60());
        $writer->writeFloatValue('foreignAgedCum90', $this->getForeignAgedCum90());
        $writer->writeFloatValue('foreignAgedCumCurrent', $this->getForeignAgedCumCurrent());
        $writer->writeFloatValue('foreignAgedCurrent', $this->getForeignAgedCurrent());
        $writer->writeFloatValue('foreignAgedFuture', $this->getForeignAgedFuture());
        $writer->writeFloatValue('foreignAgedOlder', $this->getForeignAgedOlder());
        $writer->writeFloatValue('foreignAmountPaid', $this->getForeignAmountPaid());
        $writer->writeFloatValue('foreignGrossAmount', $this->getForeignGrossAmount());
        $writer->writeFloatValue('foreignNetAmount', $this->getForeignNetAmount());
        $writer->writeFloatValue('foreignOutstanding', $this->getForeignOutstanding());
        $writer->writeFloatValue('foreignTaxAmount', $this->getForeignTaxAmount());
        $writer->writeIntegerValue('fundId', $this->getFundId());
        $writer->writeIntegerValue('gasdsClaimSubmitted', $this->getGasdsClaimSubmitted());
        $writer->writeIntegerValue('giftAid', $this->getGiftAid());
        $writer->writeFloatValue('grossAmount', $this->getGrossAmount());
        $writer->writeEnumValue('hasExternalLink', $this->getHasExternalLink());
        $writer->writeStringValue('invRef', $this->getInvRef());
        $writer->writeFloatValue('netAmount', $this->getNetAmount());
        $writer->writeStringValue('ossCountryOfVat', $this->getOssCountryOfVat());
        $writer->writeIntegerValue('ossReportingType', $this->getOssReportingType());
        $writer->writeStringValue('ossReportingTypeName', $this->getOssReportingTypeName());
        $writer->writeFloatValue('outstanding', $this->getOutstanding());
        $writer->writeEnumValue('paidFlag', $this->getPaidFlag());
        $writer->writeStringValue('paidStatus', $this->getPaidStatus());
        $writer->writeFloatValue('payment', $this->getPayment());
        $writer->writeDateTimeValue('recordCreateDate', $this->getRecordCreateDate());
        $writer->writeIntegerValue('recordDeleted', $this->getRecordDeleted());
        $writer->writeDateTimeValue('recordModifyDate', $this->getRecordModifyDate());
        $writer->writeStringValue('rtdFlag', $this->getRtdFlag());
        $writer->writeIntegerValue('smallDonation', $this->getSmallDonation());
        $writer->writeStringValue('stmtText', $this->getStmtText());
        $writer->writeFloatValue('taxAmount', $this->getTaxAmount());
        $writer->writeEnumValue('taxCode', $this->getTaxCode());
        $writer->writeIntegerValue('tranNumber', $this->getTranNumber());
        $writer->writeEnumValue('type', $this->getType());
        $writer->writeStringValue('userName', $this->getUserName());
        $writer->writeEnumValue('vatFlag', $this->getVatFlag());
        $writer->writeIntegerValue('vatFlagCode', $this->getVatFlagCode());
        $writer->writeIntegerValue('vatLedgerReturnId', $this->getVatLedgerReturnId());
        $writer->writeDateTimeValue('vatReconciledDate', $this->getVatReconciledDate());
    }

    /**
     * Sets the accountRef property value. The accountRef property
     * @param string|null $value Value to set for the accountRef property.
    */
    public function setAccountRef(?string $value): void {
        $this->accountRef = $value;
    }

    /**
     * Sets the aged30 property value. The aged30 property
     * @param float|null $value Value to set for the aged30 property.
    */
    public function setAged30(?float $value): void {
        $this->aged30 = $value;
    }

    /**
     * Sets the aged60 property value. The aged60 property
     * @param float|null $value Value to set for the aged60 property.
    */
    public function setAged60(?float $value): void {
        $this->aged60 = $value;
    }

    /**
     * Sets the aged90 property value. The aged90 property
     * @param float|null $value Value to set for the aged90 property.
    */
    public function setAged90(?float $value): void {
        $this->aged90 = $value;
    }

    /**
     * Sets the agedBalance property value. The agedBalance property
     * @param float|null $value Value to set for the agedBalance property.
    */
    public function setAgedBalance(?float $value): void {
        $this->agedBalance = $value;
    }

    /**
     * Sets the agedCum30 property value. The agedCum30 property
     * @param float|null $value Value to set for the agedCum30 property.
    */
    public function setAgedCum30(?float $value): void {
        $this->agedCum30 = $value;
    }

    /**
     * Sets the agedCum60 property value. The agedCum60 property
     * @param float|null $value Value to set for the agedCum60 property.
    */
    public function setAgedCum60(?float $value): void {
        $this->agedCum60 = $value;
    }

    /**
     * Sets the agedCum90 property value. The agedCum90 property
     * @param float|null $value Value to set for the agedCum90 property.
    */
    public function setAgedCum90(?float $value): void {
        $this->agedCum90 = $value;
    }

    /**
     * Sets the agedCumCurrent property value. The agedCumCurrent property
     * @param float|null $value Value to set for the agedCumCurrent property.
    */
    public function setAgedCumCurrent(?float $value): void {
        $this->agedCumCurrent = $value;
    }

    /**
     * Sets the agedCurrent property value. The agedCurrent property
     * @param float|null $value Value to set for the agedCurrent property.
    */
    public function setAgedCurrent(?float $value): void {
        $this->agedCurrent = $value;
    }

    /**
     * Sets the agedFuture property value. The agedFuture property
     * @param float|null $value Value to set for the agedFuture property.
    */
    public function setAgedFuture(?float $value): void {
        $this->agedFuture = $value;
    }

    /**
     * Sets the agedOlder property value. The agedOlder property
     * @param float|null $value Value to set for the agedOlder property.
    */
    public function setAgedOlder(?float $value): void {
        $this->agedOlder = $value;
    }

    /**
     * Sets the amountPaid property value. The amountPaid property
     * @param float|null $value Value to set for the amountPaid property.
    */
    public function setAmountPaid(?float $value): void {
        $this->amountPaid = $value;
    }

    /**
     * Sets the bankCode property value. The bankCode property
     * @param string|null $value Value to set for the bankCode property.
    */
    public function setBankCode(?string $value): void {
        $this->bankCode = $value;
    }

    /**
     * Sets the bankFlag property value. The bankFlag property
     * @param TransactionSplitAttributesRead_bankFlag|null $value Value to set for the bankFlag property.
    */
    public function setBankFlag(?TransactionSplitAttributesRead_bankFlag $value): void {
        $this->bankFlag = $value;
    }

    /**
     * Sets the date property value. The date property
     * @param DateTime|null $value Value to set for the date property.
    */
    public function setDate(?DateTime $value): void {
        $this->date = $value;
    }

    /**
     * Sets the dateEntered property value. The dateEntered property
     * @param DateTime|null $value Value to set for the dateEntered property.
    */
    public function setDateEntered(?DateTime $value): void {
        $this->dateEntered = $value;
    }

    /**
     * Sets the dateFlag property value. The dateFlag property
     * @param int|null $value Value to set for the dateFlag property.
    */
    public function setDateFlag(?int $value): void {
        $this->dateFlag = $value;
    }

    /**
     * Sets the deletedFlag property value. The deletedFlag property
     * @param int|null $value Value to set for the deletedFlag property.
    */
    public function setDeletedFlag(?int $value): void {
        $this->deletedFlag = $value;
    }

    /**
     * Sets the deptName property value. The deptName property
     * @param string|null $value Value to set for the deptName property.
    */
    public function setDeptName(?string $value): void {
        $this->deptName = $value;
    }

    /**
     * Sets the deptNumber property value. The deptNumber property
     * @param int|null $value Value to set for the deptNumber property.
    */
    public function setDeptNumber(?int $value): void {
        $this->deptNumber = $value;
    }

    /**
     * Sets the details property value. The details property
     * @param string|null $value Value to set for the details property.
    */
    public function setDetails(?string $value): void {
        $this->details = $value;
    }

    /**
     * Sets the disputeCode property value. The disputeCode property
     * @param int|null $value Value to set for the disputeCode property.
    */
    public function setDisputeCode(?int $value): void {
        $this->disputeCode = $value;
    }

    /**
     * Sets the disputed property value. The disputed property
     * @param int|null $value Value to set for the disputed property.
    */
    public function setDisputed(?int $value): void {
        $this->disputed = $value;
    }

    /**
     * Sets the extraRef property value. The extraRef property
     * @param string|null $value Value to set for the extraRef property.
    */
    public function setExtraRef(?string $value): void {
        $this->extraRef = $value;
    }

    /**
     * Sets the foreignAged30 property value. The foreignAged30 property
     * @param float|null $value Value to set for the foreignAged30 property.
    */
    public function setForeignAged30(?float $value): void {
        $this->foreignAged30 = $value;
    }

    /**
     * Sets the foreignAged60 property value. The foreignAged60 property
     * @param float|null $value Value to set for the foreignAged60 property.
    */
    public function setForeignAged60(?float $value): void {
        $this->foreignAged60 = $value;
    }

    /**
     * Sets the foreignAged90 property value. The foreignAged90 property
     * @param float|null $value Value to set for the foreignAged90 property.
    */
    public function setForeignAged90(?float $value): void {
        $this->foreignAged90 = $value;
    }

    /**
     * Sets the foreignAgedBalance property value. The foreignAgedBalance property
     * @param float|null $value Value to set for the foreignAgedBalance property.
    */
    public function setForeignAgedBalance(?float $value): void {
        $this->foreignAgedBalance = $value;
    }

    /**
     * Sets the foreignAgedCum30 property value. The foreignAgedCum30 property
     * @param float|null $value Value to set for the foreignAgedCum30 property.
    */
    public function setForeignAgedCum30(?float $value): void {
        $this->foreignAgedCum30 = $value;
    }

    /**
     * Sets the foreignAgedCum60 property value. The foreignAgedCum60 property
     * @param float|null $value Value to set for the foreignAgedCum60 property.
    */
    public function setForeignAgedCum60(?float $value): void {
        $this->foreignAgedCum60 = $value;
    }

    /**
     * Sets the foreignAgedCum90 property value. The foreignAgedCum90 property
     * @param float|null $value Value to set for the foreignAgedCum90 property.
    */
    public function setForeignAgedCum90(?float $value): void {
        $this->foreignAgedCum90 = $value;
    }

    /**
     * Sets the foreignAgedCumCurrent property value. The foreignAgedCumCurrent property
     * @param float|null $value Value to set for the foreignAgedCumCurrent property.
    */
    public function setForeignAgedCumCurrent(?float $value): void {
        $this->foreignAgedCumCurrent = $value;
    }

    /**
     * Sets the foreignAgedCurrent property value. The foreignAgedCurrent property
     * @param float|null $value Value to set for the foreignAgedCurrent property.
    */
    public function setForeignAgedCurrent(?float $value): void {
        $this->foreignAgedCurrent = $value;
    }

    /**
     * Sets the foreignAgedFuture property value. The foreignAgedFuture property
     * @param float|null $value Value to set for the foreignAgedFuture property.
    */
    public function setForeignAgedFuture(?float $value): void {
        $this->foreignAgedFuture = $value;
    }

    /**
     * Sets the foreignAgedOlder property value. The foreignAgedOlder property
     * @param float|null $value Value to set for the foreignAgedOlder property.
    */
    public function setForeignAgedOlder(?float $value): void {
        $this->foreignAgedOlder = $value;
    }

    /**
     * Sets the foreignAmountPaid property value. The foreignAmountPaid property
     * @param float|null $value Value to set for the foreignAmountPaid property.
    */
    public function setForeignAmountPaid(?float $value): void {
        $this->foreignAmountPaid = $value;
    }

    /**
     * Sets the foreignGrossAmount property value. The foreignGrossAmount property
     * @param float|null $value Value to set for the foreignGrossAmount property.
    */
    public function setForeignGrossAmount(?float $value): void {
        $this->foreignGrossAmount = $value;
    }

    /**
     * Sets the foreignNetAmount property value. The foreignNetAmount property
     * @param float|null $value Value to set for the foreignNetAmount property.
    */
    public function setForeignNetAmount(?float $value): void {
        $this->foreignNetAmount = $value;
    }

    /**
     * Sets the foreignOutstanding property value. The foreignOutstanding property
     * @param float|null $value Value to set for the foreignOutstanding property.
    */
    public function setForeignOutstanding(?float $value): void {
        $this->foreignOutstanding = $value;
    }

    /**
     * Sets the foreignTaxAmount property value. The foreignTaxAmount property
     * @param float|null $value Value to set for the foreignTaxAmount property.
    */
    public function setForeignTaxAmount(?float $value): void {
        $this->foreignTaxAmount = $value;
    }

    /**
     * Sets the fundId property value. The fundId property
     * @param int|null $value Value to set for the fundId property.
    */
    public function setFundId(?int $value): void {
        $this->fundId = $value;
    }

    /**
     * Sets the gasdsClaimSubmitted property value. The gasdsClaimSubmitted property
     * @param int|null $value Value to set for the gasdsClaimSubmitted property.
    */
    public function setGasdsClaimSubmitted(?int $value): void {
        $this->gasdsClaimSubmitted = $value;
    }

    /**
     * Sets the giftAid property value. The giftAid property
     * @param int|null $value Value to set for the giftAid property.
    */
    public function setGiftAid(?int $value): void {
        $this->giftAid = $value;
    }

    /**
     * Sets the grossAmount property value. The grossAmount property
     * @param float|null $value Value to set for the grossAmount property.
    */
    public function setGrossAmount(?float $value): void {
        $this->grossAmount = $value;
    }

    /**
     * Sets the hasExternalLink property value. The hasExternalLink property
     * @param TransactionSplitAttributesRead_hasExternalLink|null $value Value to set for the hasExternalLink property.
    */
    public function setHasExternalLink(?TransactionSplitAttributesRead_hasExternalLink $value): void {
        $this->hasExternalLink = $value;
    }

    /**
     * Sets the invRef property value. The invRef property
     * @param string|null $value Value to set for the invRef property.
    */
    public function setInvRef(?string $value): void {
        $this->invRef = $value;
    }

    /**
     * Sets the netAmount property value. The netAmount property
     * @param float|null $value Value to set for the netAmount property.
    */
    public function setNetAmount(?float $value): void {
        $this->netAmount = $value;
    }

    /**
     * Sets the ossCountryOfVat property value. The ossCountryOfVat property
     * @param string|null $value Value to set for the ossCountryOfVat property.
    */
    public function setOssCountryOfVat(?string $value): void {
        $this->ossCountryOfVat = $value;
    }

    /**
     * Sets the ossReportingType property value. The ossReportingType property
     * @param int|null $value Value to set for the ossReportingType property.
    */
    public function setOssReportingType(?int $value): void {
        $this->ossReportingType = $value;
    }

    /**
     * Sets the ossReportingTypeName property value. The ossReportingTypeName property
     * @param string|null $value Value to set for the ossReportingTypeName property.
    */
    public function setOssReportingTypeName(?string $value): void {
        $this->ossReportingTypeName = $value;
    }

    /**
     * Sets the outstanding property value. The outstanding property
     * @param float|null $value Value to set for the outstanding property.
    */
    public function setOutstanding(?float $value): void {
        $this->outstanding = $value;
    }

    /**
     * Sets the paidFlag property value. The paidFlag property
     * @param TransactionSplitAttributesRead_paidFlag|null $value Value to set for the paidFlag property.
    */
    public function setPaidFlag(?TransactionSplitAttributesRead_paidFlag $value): void {
        $this->paidFlag = $value;
    }

    /**
     * Sets the paidStatus property value. The paidStatus property
     * @param string|null $value Value to set for the paidStatus property.
    */
    public function setPaidStatus(?string $value): void {
        $this->paidStatus = $value;
    }

    /**
     * Sets the payment property value. The payment property
     * @param float|null $value Value to set for the payment property.
    */
    public function setPayment(?float $value): void {
        $this->payment = $value;
    }

    /**
     * Sets the recordCreateDate property value. The recordCreateDate property
     * @param DateTime|null $value Value to set for the recordCreateDate property.
    */
    public function setRecordCreateDate(?DateTime $value): void {
        $this->recordCreateDate = $value;
    }

    /**
     * Sets the recordDeleted property value. The recordDeleted property
     * @param int|null $value Value to set for the recordDeleted property.
    */
    public function setRecordDeleted(?int $value): void {
        $this->recordDeleted = $value;
    }

    /**
     * Sets the recordModifyDate property value. The recordModifyDate property
     * @param DateTime|null $value Value to set for the recordModifyDate property.
    */
    public function setRecordModifyDate(?DateTime $value): void {
        $this->recordModifyDate = $value;
    }

    /**
     * Sets the rtdFlag property value. The rtdFlag property
     * @param string|null $value Value to set for the rtdFlag property.
    */
    public function setRtdFlag(?string $value): void {
        $this->rtdFlag = $value;
    }

    /**
     * Sets the smallDonation property value. The smallDonation property
     * @param int|null $value Value to set for the smallDonation property.
    */
    public function setSmallDonation(?int $value): void {
        $this->smallDonation = $value;
    }

    /**
     * Sets the stmtText property value. The stmtText property
     * @param string|null $value Value to set for the stmtText property.
    */
    public function setStmtText(?string $value): void {
        $this->stmtText = $value;
    }

    /**
     * Sets the taxAmount property value. The taxAmount property
     * @param float|null $value Value to set for the taxAmount property.
    */
    public function setTaxAmount(?float $value): void {
        $this->taxAmount = $value;
    }

    /**
     * Sets the taxCode property value. The taxCode property
     * @param TransactionSplitAttributesRead_taxCode|null $value Value to set for the taxCode property.
    */
    public function setTaxCode(?TransactionSplitAttributesRead_taxCode $value): void {
        $this->taxCode = $value;
    }

    /**
     * Sets the tranNumber property value. The tranNumber property
     * @param int|null $value Value to set for the tranNumber property.
    */
    public function setTranNumber(?int $value): void {
        $this->tranNumber = $value;
    }

    /**
     * Sets the type property value. The type property
     * @param TransactionSplitAttributesRead_type|null $value Value to set for the type property.
    */
    public function setType(?TransactionSplitAttributesRead_type $value): void {
        $this->type = $value;
    }

    /**
     * Sets the userName property value. The userName property
     * @param string|null $value Value to set for the userName property.
    */
    public function setUserName(?string $value): void {
        $this->userName = $value;
    }

    /**
     * Sets the vatFlag property value. The vatFlag property
     * @param TransactionSplitAttributesRead_vatFlag|null $value Value to set for the vatFlag property.
    */
    public function setVatFlag(?TransactionSplitAttributesRead_vatFlag $value): void {
        $this->vatFlag = $value;
    }

    /**
     * Sets the vatFlagCode property value. The vatFlagCode property
     * @param int|null $value Value to set for the vatFlagCode property.
    */
    public function setVatFlagCode(?int $value): void {
        $this->vatFlagCode = $value;
    }

    /**
     * Sets the vatLedgerReturnId property value. The vatLedgerReturnId property
     * @param int|null $value Value to set for the vatLedgerReturnId property.
    */
    public function setVatLedgerReturnId(?int $value): void {
        $this->vatLedgerReturnId = $value;
    }

    /**
     * Sets the vatReconciledDate property value. The vatReconciledDate property
     * @param DateTime|null $value Value to set for the vatReconciledDate property.
    */
    public function setVatReconciledDate(?DateTime $value): void {
        $this->vatReconciledDate = $value;
    }

}
