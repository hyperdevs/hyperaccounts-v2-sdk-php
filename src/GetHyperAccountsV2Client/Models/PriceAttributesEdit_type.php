<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Enum;

class PriceAttributesEdit_type extends Enum {
    public const CUSTOMER = 'Customer';
    public const SPECIAL = 'Special';
    public const SUPPLIER = 'Supplier';
}
