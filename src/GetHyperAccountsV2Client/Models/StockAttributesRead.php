<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use DateTime;
use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class StockAttributesRead implements Parsable
{
    /**
     * @var int|null $assemblyLevel The assemblyLevel property
    */
    private ?int $assemblyLevel = null;

    /**
     * @var float|null $averageCostPrice The averageCostPrice property
    */
    private ?float $averageCostPrice = null;

    /**
     * @var string|null $barcode The barcode property
    */
    private ?string $barcode = null;

    /**
     * @var float|null $budgetQtySoldBf The budgetQtySoldBf property
    */
    private ?float $budgetQtySoldBf = null;

    /**
     * @var float|null $budgetQtySoldFuture The budgetQtySoldFuture property
    */
    private ?float $budgetQtySoldFuture = null;

    /**
     * @var float|null $budgetQtySoldMth1 The budgetQtySoldMth1 property
    */
    private ?float $budgetQtySoldMth1 = null;

    /**
     * @var float|null $budgetQtySoldMth10 The budgetQtySoldMth10 property
    */
    private ?float $budgetQtySoldMth10 = null;

    /**
     * @var float|null $budgetQtySoldMth11 The budgetQtySoldMth11 property
    */
    private ?float $budgetQtySoldMth11 = null;

    /**
     * @var float|null $budgetQtySoldMth12 The budgetQtySoldMth12 property
    */
    private ?float $budgetQtySoldMth12 = null;

    /**
     * @var float|null $budgetQtySoldMth2 The budgetQtySoldMth2 property
    */
    private ?float $budgetQtySoldMth2 = null;

    /**
     * @var float|null $budgetQtySoldMth3 The budgetQtySoldMth3 property
    */
    private ?float $budgetQtySoldMth3 = null;

    /**
     * @var float|null $budgetQtySoldMth4 The budgetQtySoldMth4 property
    */
    private ?float $budgetQtySoldMth4 = null;

    /**
     * @var float|null $budgetQtySoldMth5 The budgetQtySoldMth5 property
    */
    private ?float $budgetQtySoldMth5 = null;

    /**
     * @var float|null $budgetQtySoldMth6 The budgetQtySoldMth6 property
    */
    private ?float $budgetQtySoldMth6 = null;

    /**
     * @var float|null $budgetQtySoldMth7 The budgetQtySoldMth7 property
    */
    private ?float $budgetQtySoldMth7 = null;

    /**
     * @var float|null $budgetQtySoldMth8 The budgetQtySoldMth8 property
    */
    private ?float $budgetQtySoldMth8 = null;

    /**
     * @var float|null $budgetQtySoldMth9 The budgetQtySoldMth9 property
    */
    private ?float $budgetQtySoldMth9 = null;

    /**
     * @var float|null $budgetSalesBf The budgetSalesBf property
    */
    private ?float $budgetSalesBf = null;

    /**
     * @var float|null $budgetSalesFuture The budgetSalesFuture property
    */
    private ?float $budgetSalesFuture = null;

    /**
     * @var float|null $budgetSalesMth1 The budgetSalesMth1 property
    */
    private ?float $budgetSalesMth1 = null;

    /**
     * @var float|null $budgetSalesMth10 The budgetSalesMth10 property
    */
    private ?float $budgetSalesMth10 = null;

    /**
     * @var float|null $budgetSalesMth11 The budgetSalesMth11 property
    */
    private ?float $budgetSalesMth11 = null;

    /**
     * @var float|null $budgetSalesMth12 The budgetSalesMth12 property
    */
    private ?float $budgetSalesMth12 = null;

    /**
     * @var float|null $budgetSalesMth2 The budgetSalesMth2 property
    */
    private ?float $budgetSalesMth2 = null;

    /**
     * @var float|null $budgetSalesMth3 The budgetSalesMth3 property
    */
    private ?float $budgetSalesMth3 = null;

    /**
     * @var float|null $budgetSalesMth4 The budgetSalesMth4 property
    */
    private ?float $budgetSalesMth4 = null;

    /**
     * @var float|null $budgetSalesMth5 The budgetSalesMth5 property
    */
    private ?float $budgetSalesMth5 = null;

    /**
     * @var float|null $budgetSalesMth6 The budgetSalesMth6 property
    */
    private ?float $budgetSalesMth6 = null;

    /**
     * @var float|null $budgetSalesMth7 The budgetSalesMth7 property
    */
    private ?float $budgetSalesMth7 = null;

    /**
     * @var float|null $budgetSalesMth8 The budgetSalesMth8 property
    */
    private ?float $budgetSalesMth8 = null;

    /**
     * @var float|null $budgetSalesMth9 The budgetSalesMth9 property
    */
    private ?float $budgetSalesMth9 = null;

    /**
     * @var string|null $commodityCode The commodityCode property
    */
    private ?string $commodityCode = null;

    /**
     * @var string|null $componentCode1 The componentCode1 property
    */
    private ?string $componentCode1 = null;

    /**
     * @var string|null $componentCode10 The componentCode10 property
    */
    private ?string $componentCode10 = null;

    /**
     * @var string|null $componentCode11 The componentCode11 property
    */
    private ?string $componentCode11 = null;

    /**
     * @var string|null $componentCode12 The componentCode12 property
    */
    private ?string $componentCode12 = null;

    /**
     * @var string|null $componentCode13 The componentCode13 property
    */
    private ?string $componentCode13 = null;

    /**
     * @var string|null $componentCode14 The componentCode14 property
    */
    private ?string $componentCode14 = null;

    /**
     * @var string|null $componentCode15 The componentCode15 property
    */
    private ?string $componentCode15 = null;

    /**
     * @var string|null $componentCode16 The componentCode16 property
    */
    private ?string $componentCode16 = null;

    /**
     * @var string|null $componentCode17 The componentCode17 property
    */
    private ?string $componentCode17 = null;

    /**
     * @var string|null $componentCode18 The componentCode18 property
    */
    private ?string $componentCode18 = null;

    /**
     * @var string|null $componentCode19 The componentCode19 property
    */
    private ?string $componentCode19 = null;

    /**
     * @var string|null $componentCode2 The componentCode2 property
    */
    private ?string $componentCode2 = null;

    /**
     * @var string|null $componentCode20 The componentCode20 property
    */
    private ?string $componentCode20 = null;

    /**
     * @var string|null $componentCode21 The componentCode21 property
    */
    private ?string $componentCode21 = null;

    /**
     * @var string|null $componentCode22 The componentCode22 property
    */
    private ?string $componentCode22 = null;

    /**
     * @var string|null $componentCode23 The componentCode23 property
    */
    private ?string $componentCode23 = null;

    /**
     * @var string|null $componentCode24 The componentCode24 property
    */
    private ?string $componentCode24 = null;

    /**
     * @var string|null $componentCode25 The componentCode25 property
    */
    private ?string $componentCode25 = null;

    /**
     * @var string|null $componentCode26 The componentCode26 property
    */
    private ?string $componentCode26 = null;

    /**
     * @var string|null $componentCode27 The componentCode27 property
    */
    private ?string $componentCode27 = null;

    /**
     * @var string|null $componentCode28 The componentCode28 property
    */
    private ?string $componentCode28 = null;

    /**
     * @var string|null $componentCode29 The componentCode29 property
    */
    private ?string $componentCode29 = null;

    /**
     * @var string|null $componentCode3 The componentCode3 property
    */
    private ?string $componentCode3 = null;

    /**
     * @var string|null $componentCode30 The componentCode30 property
    */
    private ?string $componentCode30 = null;

    /**
     * @var string|null $componentCode31 The componentCode31 property
    */
    private ?string $componentCode31 = null;

    /**
     * @var string|null $componentCode32 The componentCode32 property
    */
    private ?string $componentCode32 = null;

    /**
     * @var string|null $componentCode33 The componentCode33 property
    */
    private ?string $componentCode33 = null;

    /**
     * @var string|null $componentCode34 The componentCode34 property
    */
    private ?string $componentCode34 = null;

    /**
     * @var string|null $componentCode35 The componentCode35 property
    */
    private ?string $componentCode35 = null;

    /**
     * @var string|null $componentCode36 The componentCode36 property
    */
    private ?string $componentCode36 = null;

    /**
     * @var string|null $componentCode37 The componentCode37 property
    */
    private ?string $componentCode37 = null;

    /**
     * @var string|null $componentCode38 The componentCode38 property
    */
    private ?string $componentCode38 = null;

    /**
     * @var string|null $componentCode39 The componentCode39 property
    */
    private ?string $componentCode39 = null;

    /**
     * @var string|null $componentCode4 The componentCode4 property
    */
    private ?string $componentCode4 = null;

    /**
     * @var string|null $componentCode40 The componentCode40 property
    */
    private ?string $componentCode40 = null;

    /**
     * @var string|null $componentCode41 The componentCode41 property
    */
    private ?string $componentCode41 = null;

    /**
     * @var string|null $componentCode42 The componentCode42 property
    */
    private ?string $componentCode42 = null;

    /**
     * @var string|null $componentCode43 The componentCode43 property
    */
    private ?string $componentCode43 = null;

    /**
     * @var string|null $componentCode44 The componentCode44 property
    */
    private ?string $componentCode44 = null;

    /**
     * @var string|null $componentCode45 The componentCode45 property
    */
    private ?string $componentCode45 = null;

    /**
     * @var string|null $componentCode46 The componentCode46 property
    */
    private ?string $componentCode46 = null;

    /**
     * @var string|null $componentCode47 The componentCode47 property
    */
    private ?string $componentCode47 = null;

    /**
     * @var string|null $componentCode48 The componentCode48 property
    */
    private ?string $componentCode48 = null;

    /**
     * @var string|null $componentCode49 The componentCode49 property
    */
    private ?string $componentCode49 = null;

    /**
     * @var string|null $componentCode5 The componentCode5 property
    */
    private ?string $componentCode5 = null;

    /**
     * @var string|null $componentCode50 The componentCode50 property
    */
    private ?string $componentCode50 = null;

    /**
     * @var string|null $componentCode6 The componentCode6 property
    */
    private ?string $componentCode6 = null;

    /**
     * @var string|null $componentCode7 The componentCode7 property
    */
    private ?string $componentCode7 = null;

    /**
     * @var string|null $componentCode8 The componentCode8 property
    */
    private ?string $componentCode8 = null;

    /**
     * @var string|null $componentCode9 The componentCode9 property
    */
    private ?string $componentCode9 = null;

    /**
     * @var float|null $componentQty1 The componentQty1 property
    */
    private ?float $componentQty1 = null;

    /**
     * @var float|null $componentQty10 The componentQty10 property
    */
    private ?float $componentQty10 = null;

    /**
     * @var float|null $componentQty11 The componentQty11 property
    */
    private ?float $componentQty11 = null;

    /**
     * @var float|null $componentQty12 The componentQty12 property
    */
    private ?float $componentQty12 = null;

    /**
     * @var float|null $componentQty13 The componentQty13 property
    */
    private ?float $componentQty13 = null;

    /**
     * @var float|null $componentQty14 The componentQty14 property
    */
    private ?float $componentQty14 = null;

    /**
     * @var float|null $componentQty15 The componentQty15 property
    */
    private ?float $componentQty15 = null;

    /**
     * @var float|null $componentQty16 The componentQty16 property
    */
    private ?float $componentQty16 = null;

    /**
     * @var float|null $componentQty17 The componentQty17 property
    */
    private ?float $componentQty17 = null;

    /**
     * @var float|null $componentQty18 The componentQty18 property
    */
    private ?float $componentQty18 = null;

    /**
     * @var float|null $componentQty19 The componentQty19 property
    */
    private ?float $componentQty19 = null;

    /**
     * @var float|null $componentQty2 The componentQty2 property
    */
    private ?float $componentQty2 = null;

    /**
     * @var float|null $componentQty20 The componentQty20 property
    */
    private ?float $componentQty20 = null;

    /**
     * @var float|null $componentQty21 The componentQty21 property
    */
    private ?float $componentQty21 = null;

    /**
     * @var float|null $componentQty22 The componentQty22 property
    */
    private ?float $componentQty22 = null;

    /**
     * @var float|null $componentQty23 The componentQty23 property
    */
    private ?float $componentQty23 = null;

    /**
     * @var float|null $componentQty24 The componentQty24 property
    */
    private ?float $componentQty24 = null;

    /**
     * @var float|null $componentQty25 The componentQty25 property
    */
    private ?float $componentQty25 = null;

    /**
     * @var float|null $componentQty26 The componentQty26 property
    */
    private ?float $componentQty26 = null;

    /**
     * @var float|null $componentQty27 The componentQty27 property
    */
    private ?float $componentQty27 = null;

    /**
     * @var float|null $componentQty28 The componentQty28 property
    */
    private ?float $componentQty28 = null;

    /**
     * @var float|null $componentQty29 The componentQty29 property
    */
    private ?float $componentQty29 = null;

    /**
     * @var float|null $componentQty3 The componentQty3 property
    */
    private ?float $componentQty3 = null;

    /**
     * @var float|null $componentQty30 The componentQty30 property
    */
    private ?float $componentQty30 = null;

    /**
     * @var float|null $componentQty31 The componentQty31 property
    */
    private ?float $componentQty31 = null;

    /**
     * @var float|null $componentQty32 The componentQty32 property
    */
    private ?float $componentQty32 = null;

    /**
     * @var float|null $componentQty33 The componentQty33 property
    */
    private ?float $componentQty33 = null;

    /**
     * @var float|null $componentQty34 The componentQty34 property
    */
    private ?float $componentQty34 = null;

    /**
     * @var float|null $componentQty35 The componentQty35 property
    */
    private ?float $componentQty35 = null;

    /**
     * @var float|null $componentQty36 The componentQty36 property
    */
    private ?float $componentQty36 = null;

    /**
     * @var float|null $componentQty37 The componentQty37 property
    */
    private ?float $componentQty37 = null;

    /**
     * @var float|null $componentQty38 The componentQty38 property
    */
    private ?float $componentQty38 = null;

    /**
     * @var float|null $componentQty39 The componentQty39 property
    */
    private ?float $componentQty39 = null;

    /**
     * @var float|null $componentQty4 The componentQty4 property
    */
    private ?float $componentQty4 = null;

    /**
     * @var float|null $componentQty40 The componentQty40 property
    */
    private ?float $componentQty40 = null;

    /**
     * @var float|null $componentQty41 The componentQty41 property
    */
    private ?float $componentQty41 = null;

    /**
     * @var float|null $componentQty42 The componentQty42 property
    */
    private ?float $componentQty42 = null;

    /**
     * @var float|null $componentQty43 The componentQty43 property
    */
    private ?float $componentQty43 = null;

    /**
     * @var float|null $componentQty44 The componentQty44 property
    */
    private ?float $componentQty44 = null;

    /**
     * @var float|null $componentQty45 The componentQty45 property
    */
    private ?float $componentQty45 = null;

    /**
     * @var float|null $componentQty46 The componentQty46 property
    */
    private ?float $componentQty46 = null;

    /**
     * @var float|null $componentQty47 The componentQty47 property
    */
    private ?float $componentQty47 = null;

    /**
     * @var float|null $componentQty48 The componentQty48 property
    */
    private ?float $componentQty48 = null;

    /**
     * @var float|null $componentQty49 The componentQty49 property
    */
    private ?float $componentQty49 = null;

    /**
     * @var float|null $componentQty5 The componentQty5 property
    */
    private ?float $componentQty5 = null;

    /**
     * @var float|null $componentQty50 The componentQty50 property
    */
    private ?float $componentQty50 = null;

    /**
     * @var float|null $componentQty6 The componentQty6 property
    */
    private ?float $componentQty6 = null;

    /**
     * @var float|null $componentQty7 The componentQty7 property
    */
    private ?float $componentQty7 = null;

    /**
     * @var float|null $componentQty8 The componentQty8 property
    */
    private ?float $componentQty8 = null;

    /**
     * @var float|null $componentQty9 The componentQty9 property
    */
    private ?float $componentQty9 = null;

    /**
     * @var float|null $costBf The costBf property
    */
    private ?float $costBf = null;

    /**
     * @var float|null $costFuture The costFuture property
    */
    private ?float $costFuture = null;

    /**
     * @var float|null $costMth1 The costMth1 property
    */
    private ?float $costMth1 = null;

    /**
     * @var float|null $costMth10 The costMth10 property
    */
    private ?float $costMth10 = null;

    /**
     * @var float|null $costMth11 The costMth11 property
    */
    private ?float $costMth11 = null;

    /**
     * @var float|null $costMth12 The costMth12 property
    */
    private ?float $costMth12 = null;

    /**
     * @var float|null $costMth2 The costMth2 property
    */
    private ?float $costMth2 = null;

    /**
     * @var float|null $costMth3 The costMth3 property
    */
    private ?float $costMth3 = null;

    /**
     * @var float|null $costMth4 The costMth4 property
    */
    private ?float $costMth4 = null;

    /**
     * @var float|null $costMth5 The costMth5 property
    */
    private ?float $costMth5 = null;

    /**
     * @var float|null $costMth6 The costMth6 property
    */
    private ?float $costMth6 = null;

    /**
     * @var float|null $costMth7 The costMth7 property
    */
    private ?float $costMth7 = null;

    /**
     * @var float|null $costMth8 The costMth8 property
    */
    private ?float $costMth8 = null;

    /**
     * @var float|null $costMth9 The costMth9 property
    */
    private ?float $costMth9 = null;

    /**
     * @var string|null $countryCodeOfOrigin The countryCodeOfOrigin property
    */
    private ?string $countryCodeOfOrigin = null;

    /**
     * @var string|null $deptName The deptName property
    */
    private ?string $deptName = null;

    /**
     * @var int|null $deptNumber The deptNumber property
    */
    private ?int $deptNumber = null;

    /**
     * @var string|null $description The description property
    */
    private ?string $description = null;

    /**
     * @var float|null $discALevel10Qty The discALevel10Qty property
    */
    private ?float $discALevel10Qty = null;

    /**
     * @var float|null $discALevel10Rate The discALevel10Rate property
    */
    private ?float $discALevel10Rate = null;

    /**
     * @var float|null $discALevel1Qty The discALevel1Qty property
    */
    private ?float $discALevel1Qty = null;

    /**
     * @var float|null $discALevel1Rate The discALevel1Rate property
    */
    private ?float $discALevel1Rate = null;

    /**
     * @var float|null $discALevel2Qty The discALevel2Qty property
    */
    private ?float $discALevel2Qty = null;

    /**
     * @var float|null $discALevel2Rate The discALevel2Rate property
    */
    private ?float $discALevel2Rate = null;

    /**
     * @var float|null $discALevel3Qty The discALevel3Qty property
    */
    private ?float $discALevel3Qty = null;

    /**
     * @var float|null $discALevel3Rate The discALevel3Rate property
    */
    private ?float $discALevel3Rate = null;

    /**
     * @var float|null $discALevel4Qty The discALevel4Qty property
    */
    private ?float $discALevel4Qty = null;

    /**
     * @var float|null $discALevel4Rate The discALevel4Rate property
    */
    private ?float $discALevel4Rate = null;

    /**
     * @var float|null $discALevel5Qty The discALevel5Qty property
    */
    private ?float $discALevel5Qty = null;

    /**
     * @var float|null $discALevel5Rate The discALevel5Rate property
    */
    private ?float $discALevel5Rate = null;

    /**
     * @var float|null $discALevel6Qty The discALevel6Qty property
    */
    private ?float $discALevel6Qty = null;

    /**
     * @var float|null $discALevel6Rate The discALevel6Rate property
    */
    private ?float $discALevel6Rate = null;

    /**
     * @var float|null $discALevel7Qty The discALevel7Qty property
    */
    private ?float $discALevel7Qty = null;

    /**
     * @var float|null $discALevel7Rate The discALevel7Rate property
    */
    private ?float $discALevel7Rate = null;

    /**
     * @var float|null $discALevel8Qty The discALevel8Qty property
    */
    private ?float $discALevel8Qty = null;

    /**
     * @var float|null $discALevel8Rate The discALevel8Rate property
    */
    private ?float $discALevel8Rate = null;

    /**
     * @var float|null $discALevel9Qty The discALevel9Qty property
    */
    private ?float $discALevel9Qty = null;

    /**
     * @var float|null $discALevel9Rate The discALevel9Rate property
    */
    private ?float $discALevel9Rate = null;

    /**
     * @var float|null $discBLevel10Qty The discBLevel10Qty property
    */
    private ?float $discBLevel10Qty = null;

    /**
     * @var float|null $discBLevel10Rate The discBLevel10Rate property
    */
    private ?float $discBLevel10Rate = null;

    /**
     * @var float|null $discBLevel1Qty The discBLevel1Qty property
    */
    private ?float $discBLevel1Qty = null;

    /**
     * @var float|null $discBLevel1Rate The discBLevel1Rate property
    */
    private ?float $discBLevel1Rate = null;

    /**
     * @var float|null $discBLevel2Qty The discBLevel2Qty property
    */
    private ?float $discBLevel2Qty = null;

    /**
     * @var float|null $discBLevel2Rate The discBLevel2Rate property
    */
    private ?float $discBLevel2Rate = null;

    /**
     * @var float|null $discBLevel3Qty The discBLevel3Qty property
    */
    private ?float $discBLevel3Qty = null;

    /**
     * @var float|null $discBLevel3Rate The discBLevel3Rate property
    */
    private ?float $discBLevel3Rate = null;

    /**
     * @var float|null $discBLevel4Qty The discBLevel4Qty property
    */
    private ?float $discBLevel4Qty = null;

    /**
     * @var float|null $discBLevel4Rate The discBLevel4Rate property
    */
    private ?float $discBLevel4Rate = null;

    /**
     * @var float|null $discBLevel5Qty The discBLevel5Qty property
    */
    private ?float $discBLevel5Qty = null;

    /**
     * @var float|null $discBLevel5Rate The discBLevel5Rate property
    */
    private ?float $discBLevel5Rate = null;

    /**
     * @var float|null $discBLevel6Qty The discBLevel6Qty property
    */
    private ?float $discBLevel6Qty = null;

    /**
     * @var float|null $discBLevel6Rate The discBLevel6Rate property
    */
    private ?float $discBLevel6Rate = null;

    /**
     * @var float|null $discBLevel7Qty The discBLevel7Qty property
    */
    private ?float $discBLevel7Qty = null;

    /**
     * @var float|null $discBLevel7Rate The discBLevel7Rate property
    */
    private ?float $discBLevel7Rate = null;

    /**
     * @var float|null $discBLevel8Qty The discBLevel8Qty property
    */
    private ?float $discBLevel8Qty = null;

    /**
     * @var float|null $discBLevel8Rate The discBLevel8Rate property
    */
    private ?float $discBLevel8Rate = null;

    /**
     * @var float|null $discBLevel9Qty The discBLevel9Qty property
    */
    private ?float $discBLevel9Qty = null;

    /**
     * @var float|null $discBLevel9Rate The discBLevel9Rate property
    */
    private ?float $discBLevel9Rate = null;

    /**
     * @var float|null $discCLevel10Qty The discCLevel10Qty property
    */
    private ?float $discCLevel10Qty = null;

    /**
     * @var float|null $discCLevel10Rate The discCLevel10Rate property
    */
    private ?float $discCLevel10Rate = null;

    /**
     * @var float|null $discCLevel1Qty The discCLevel1Qty property
    */
    private ?float $discCLevel1Qty = null;

    /**
     * @var float|null $discCLevel1Rate The discCLevel1Rate property
    */
    private ?float $discCLevel1Rate = null;

    /**
     * @var float|null $discCLevel2Qty The discCLevel2Qty property
    */
    private ?float $discCLevel2Qty = null;

    /**
     * @var float|null $discCLevel2Rate The discCLevel2Rate property
    */
    private ?float $discCLevel2Rate = null;

    /**
     * @var float|null $discCLevel3Qty The discCLevel3Qty property
    */
    private ?float $discCLevel3Qty = null;

    /**
     * @var float|null $discCLevel3Rate The discCLevel3Rate property
    */
    private ?float $discCLevel3Rate = null;

    /**
     * @var float|null $discCLevel4Qty The discCLevel4Qty property
    */
    private ?float $discCLevel4Qty = null;

    /**
     * @var float|null $discCLevel4Rate The discCLevel4Rate property
    */
    private ?float $discCLevel4Rate = null;

    /**
     * @var float|null $discCLevel5Qty The discCLevel5Qty property
    */
    private ?float $discCLevel5Qty = null;

    /**
     * @var float|null $discCLevel5Rate The discCLevel5Rate property
    */
    private ?float $discCLevel5Rate = null;

    /**
     * @var float|null $discCLevel6Qty The discCLevel6Qty property
    */
    private ?float $discCLevel6Qty = null;

    /**
     * @var float|null $discCLevel6Rate The discCLevel6Rate property
    */
    private ?float $discCLevel6Rate = null;

    /**
     * @var float|null $discCLevel7Qty The discCLevel7Qty property
    */
    private ?float $discCLevel7Qty = null;

    /**
     * @var float|null $discCLevel7Rate The discCLevel7Rate property
    */
    private ?float $discCLevel7Rate = null;

    /**
     * @var float|null $discCLevel8Qty The discCLevel8Qty property
    */
    private ?float $discCLevel8Qty = null;

    /**
     * @var float|null $discCLevel8Rate The discCLevel8Rate property
    */
    private ?float $discCLevel8Rate = null;

    /**
     * @var float|null $discCLevel9Qty The discCLevel9Qty property
    */
    private ?float $discCLevel9Qty = null;

    /**
     * @var float|null $discCLevel9Rate The discCLevel9Rate property
    */
    private ?float $discCLevel9Rate = null;

    /**
     * @var float|null $discDLevel10Qty The discDLevel10Qty property
    */
    private ?float $discDLevel10Qty = null;

    /**
     * @var float|null $discDLevel10Rate The discDLevel10Rate property
    */
    private ?float $discDLevel10Rate = null;

    /**
     * @var float|null $discDLevel1Qty The discDLevel1Qty property
    */
    private ?float $discDLevel1Qty = null;

    /**
     * @var float|null $discDLevel1Rate The discDLevel1Rate property
    */
    private ?float $discDLevel1Rate = null;

    /**
     * @var float|null $discDLevel2Qty The discDLevel2Qty property
    */
    private ?float $discDLevel2Qty = null;

    /**
     * @var float|null $discDLevel2Rate The discDLevel2Rate property
    */
    private ?float $discDLevel2Rate = null;

    /**
     * @var float|null $discDLevel3Qty The discDLevel3Qty property
    */
    private ?float $discDLevel3Qty = null;

    /**
     * @var float|null $discDLevel3Rate The discDLevel3Rate property
    */
    private ?float $discDLevel3Rate = null;

    /**
     * @var float|null $discDLevel4Qty The discDLevel4Qty property
    */
    private ?float $discDLevel4Qty = null;

    /**
     * @var float|null $discDLevel4Rate The discDLevel4Rate property
    */
    private ?float $discDLevel4Rate = null;

    /**
     * @var float|null $discDLevel5Qty The discDLevel5Qty property
    */
    private ?float $discDLevel5Qty = null;

    /**
     * @var float|null $discDLevel5Rate The discDLevel5Rate property
    */
    private ?float $discDLevel5Rate = null;

    /**
     * @var float|null $discDLevel6Qty The discDLevel6Qty property
    */
    private ?float $discDLevel6Qty = null;

    /**
     * @var float|null $discDLevel6Rate The discDLevel6Rate property
    */
    private ?float $discDLevel6Rate = null;

    /**
     * @var float|null $discDLevel7Qty The discDLevel7Qty property
    */
    private ?float $discDLevel7Qty = null;

    /**
     * @var float|null $discDLevel7Rate The discDLevel7Rate property
    */
    private ?float $discDLevel7Rate = null;

    /**
     * @var float|null $discDLevel8Qty The discDLevel8Qty property
    */
    private ?float $discDLevel8Qty = null;

    /**
     * @var float|null $discDLevel8Rate The discDLevel8Rate property
    */
    private ?float $discDLevel8Rate = null;

    /**
     * @var float|null $discDLevel9Qty The discDLevel9Qty property
    */
    private ?float $discDLevel9Qty = null;

    /**
     * @var float|null $discDLevel9Rate The discDLevel9Rate property
    */
    private ?float $discDLevel9Rate = null;

    /**
     * @var float|null $discELevel10Qty The discELevel10Qty property
    */
    private ?float $discELevel10Qty = null;

    /**
     * @var float|null $discELevel10Rate The discELevel10Rate property
    */
    private ?float $discELevel10Rate = null;

    /**
     * @var float|null $discELevel1Qty The discELevel1Qty property
    */
    private ?float $discELevel1Qty = null;

    /**
     * @var float|null $discELevel1Rate The discELevel1Rate property
    */
    private ?float $discELevel1Rate = null;

    /**
     * @var float|null $discELevel2Qty The discELevel2Qty property
    */
    private ?float $discELevel2Qty = null;

    /**
     * @var float|null $discELevel2Rate The discELevel2Rate property
    */
    private ?float $discELevel2Rate = null;

    /**
     * @var float|null $discELevel3Qty The discELevel3Qty property
    */
    private ?float $discELevel3Qty = null;

    /**
     * @var float|null $discELevel3Rate The discELevel3Rate property
    */
    private ?float $discELevel3Rate = null;

    /**
     * @var float|null $discELevel4Qty The discELevel4Qty property
    */
    private ?float $discELevel4Qty = null;

    /**
     * @var float|null $discELevel4Rate The discELevel4Rate property
    */
    private ?float $discELevel4Rate = null;

    /**
     * @var float|null $discELevel5Qty The discELevel5Qty property
    */
    private ?float $discELevel5Qty = null;

    /**
     * @var float|null $discELevel5Rate The discELevel5Rate property
    */
    private ?float $discELevel5Rate = null;

    /**
     * @var float|null $discELevel6Qty The discELevel6Qty property
    */
    private ?float $discELevel6Qty = null;

    /**
     * @var float|null $discELevel6Rate The discELevel6Rate property
    */
    private ?float $discELevel6Rate = null;

    /**
     * @var float|null $discELevel7Qty The discELevel7Qty property
    */
    private ?float $discELevel7Qty = null;

    /**
     * @var float|null $discELevel7Rate The discELevel7Rate property
    */
    private ?float $discELevel7Rate = null;

    /**
     * @var float|null $discELevel8Qty The discELevel8Qty property
    */
    private ?float $discELevel8Qty = null;

    /**
     * @var float|null $discELevel8Rate The discELevel8Rate property
    */
    private ?float $discELevel8Rate = null;

    /**
     * @var float|null $discELevel9Qty The discELevel9Qty property
    */
    private ?float $discELevel9Qty = null;

    /**
     * @var float|null $discELevel9Rate The discELevel9Rate property
    */
    private ?float $discELevel9Rate = null;

    /**
     * @var string|null $hasBom The hasBom property
    */
    private ?string $hasBom = null;

    /**
     * @var int|null $hasNoComponent The hasNoComponent property
    */
    private ?int $hasNoComponent = null;

    /**
     * @var int|null $ignoreStkLvlFlag The ignoreStkLvlFlag property
    */
    private ?int $ignoreStkLvlFlag = null;

    /**
     * @var int|null $inactiveFlag The inactiveFlag property
    */
    private ?int $inactiveFlag = null;

    /**
     * @var string|null $intrastatCommCode The intrastatCommCode property
    */
    private ?string $intrastatCommCode = null;

    /**
     * @var string|null $intrastatImportDutyCode The intrastatImportDutyCode property
    */
    private ?string $intrastatImportDutyCode = null;

    /**
     * @var float|null $lastDiscPurchasePrice The lastDiscPurchasePrice property
    */
    private ?float $lastDiscPurchasePrice = null;

    /**
     * @var DateTime|null $lastPurchaseDate The lastPurchaseDate property
    */
    private ?DateTime $lastPurchaseDate = null;

    /**
     * @var float|null $lastPurchasePrice The lastPurchasePrice property
    */
    private ?float $lastPurchasePrice = null;

    /**
     * @var DateTime|null $lastSaleDate The lastSaleDate property
    */
    private ?DateTime $lastSaleDate = null;

    /**
     * @var int|null $linkLevel The linkLevel property
    */
    private ?int $linkLevel = null;

    /**
     * @var string|null $location The location property
    */
    private ?string $location = null;

    /**
     * @var float|null $priorYrCostBf The priorYrCostBf property
    */
    private ?float $priorYrCostBf = null;

    /**
     * @var float|null $priorYrCostFuture The priorYrCostFuture property
    */
    private ?float $priorYrCostFuture = null;

    /**
     * @var float|null $priorYrCostMth1 The priorYrCostMth1 property
    */
    private ?float $priorYrCostMth1 = null;

    /**
     * @var float|null $priorYrCostMth10 The priorYrCostMth10 property
    */
    private ?float $priorYrCostMth10 = null;

    /**
     * @var float|null $priorYrCostMth11 The priorYrCostMth11 property
    */
    private ?float $priorYrCostMth11 = null;

    /**
     * @var float|null $priorYrCostMth12 The priorYrCostMth12 property
    */
    private ?float $priorYrCostMth12 = null;

    /**
     * @var float|null $priorYrCostMth2 The priorYrCostMth2 property
    */
    private ?float $priorYrCostMth2 = null;

    /**
     * @var float|null $priorYrCostMth3 The priorYrCostMth3 property
    */
    private ?float $priorYrCostMth3 = null;

    /**
     * @var float|null $priorYrCostMth4 The priorYrCostMth4 property
    */
    private ?float $priorYrCostMth4 = null;

    /**
     * @var float|null $priorYrCostMth5 The priorYrCostMth5 property
    */
    private ?float $priorYrCostMth5 = null;

    /**
     * @var float|null $priorYrCostMth6 The priorYrCostMth6 property
    */
    private ?float $priorYrCostMth6 = null;

    /**
     * @var float|null $priorYrCostMth7 The priorYrCostMth7 property
    */
    private ?float $priorYrCostMth7 = null;

    /**
     * @var float|null $priorYrCostMth8 The priorYrCostMth8 property
    */
    private ?float $priorYrCostMth8 = null;

    /**
     * @var float|null $priorYrCostMth9 The priorYrCostMth9 property
    */
    private ?float $priorYrCostMth9 = null;

    /**
     * @var float|null $priorYrQtySoldBf The priorYrQtySoldBf property
    */
    private ?float $priorYrQtySoldBf = null;

    /**
     * @var float|null $priorYrQtySoldFuture The priorYrQtySoldFuture property
    */
    private ?float $priorYrQtySoldFuture = null;

    /**
     * @var float|null $priorYrQtySoldMth1 The priorYrQtySoldMth1 property
    */
    private ?float $priorYrQtySoldMth1 = null;

    /**
     * @var float|null $priorYrQtySoldMth10 The priorYrQtySoldMth10 property
    */
    private ?float $priorYrQtySoldMth10 = null;

    /**
     * @var float|null $priorYrQtySoldMth11 The priorYrQtySoldMth11 property
    */
    private ?float $priorYrQtySoldMth11 = null;

    /**
     * @var float|null $priorYrQtySoldMth12 The priorYrQtySoldMth12 property
    */
    private ?float $priorYrQtySoldMth12 = null;

    /**
     * @var float|null $priorYrQtySoldMth2 The priorYrQtySoldMth2 property
    */
    private ?float $priorYrQtySoldMth2 = null;

    /**
     * @var float|null $priorYrQtySoldMth3 The priorYrQtySoldMth3 property
    */
    private ?float $priorYrQtySoldMth3 = null;

    /**
     * @var float|null $priorYrQtySoldMth4 The priorYrQtySoldMth4 property
    */
    private ?float $priorYrQtySoldMth4 = null;

    /**
     * @var float|null $priorYrQtySoldMth5 The priorYrQtySoldMth5 property
    */
    private ?float $priorYrQtySoldMth5 = null;

    /**
     * @var float|null $priorYrQtySoldMth6 The priorYrQtySoldMth6 property
    */
    private ?float $priorYrQtySoldMth6 = null;

    /**
     * @var float|null $priorYrQtySoldMth7 The priorYrQtySoldMth7 property
    */
    private ?float $priorYrQtySoldMth7 = null;

    /**
     * @var float|null $priorYrQtySoldMth8 The priorYrQtySoldMth8 property
    */
    private ?float $priorYrQtySoldMth8 = null;

    /**
     * @var float|null $priorYrQtySoldMth9 The priorYrQtySoldMth9 property
    */
    private ?float $priorYrQtySoldMth9 = null;

    /**
     * @var float|null $priorYrSalesBf The priorYrSalesBf property
    */
    private ?float $priorYrSalesBf = null;

    /**
     * @var float|null $priorYrSalesFuture The priorYrSalesFuture property
    */
    private ?float $priorYrSalesFuture = null;

    /**
     * @var float|null $priorYrSalesMth1 The priorYrSalesMth1 property
    */
    private ?float $priorYrSalesMth1 = null;

    /**
     * @var float|null $priorYrSalesMth10 The priorYrSalesMth10 property
    */
    private ?float $priorYrSalesMth10 = null;

    /**
     * @var float|null $priorYrSalesMth11 The priorYrSalesMth11 property
    */
    private ?float $priorYrSalesMth11 = null;

    /**
     * @var float|null $priorYrSalesMth12 The priorYrSalesMth12 property
    */
    private ?float $priorYrSalesMth12 = null;

    /**
     * @var float|null $priorYrSalesMth2 The priorYrSalesMth2 property
    */
    private ?float $priorYrSalesMth2 = null;

    /**
     * @var float|null $priorYrSalesMth3 The priorYrSalesMth3 property
    */
    private ?float $priorYrSalesMth3 = null;

    /**
     * @var float|null $priorYrSalesMth4 The priorYrSalesMth4 property
    */
    private ?float $priorYrSalesMth4 = null;

    /**
     * @var float|null $priorYrSalesMth5 The priorYrSalesMth5 property
    */
    private ?float $priorYrSalesMth5 = null;

    /**
     * @var float|null $priorYrSalesMth6 The priorYrSalesMth6 property
    */
    private ?float $priorYrSalesMth6 = null;

    /**
     * @var float|null $priorYrSalesMth7 The priorYrSalesMth7 property
    */
    private ?float $priorYrSalesMth7 = null;

    /**
     * @var float|null $priorYrSalesMth8 The priorYrSalesMth8 property
    */
    private ?float $priorYrSalesMth8 = null;

    /**
     * @var float|null $priorYrSalesMth9 The priorYrSalesMth9 property
    */
    private ?float $priorYrSalesMth9 = null;

    /**
     * @var string|null $purchaseNominalCode The purchaseNominalCode property
    */
    private ?string $purchaseNominalCode = null;

    /**
     * @var string|null $purchaseRef The purchaseRef property
    */
    private ?string $purchaseRef = null;

    /**
     * @var float|null $qtyAllocated The qtyAllocated property
    */
    private ?float $qtyAllocated = null;

    /**
     * @var float|null $qtyInStock The qtyInStock property
    */
    private ?float $qtyInStock = null;

    /**
     * @var float|null $qtyLastOrder The qtyLastOrder property
    */
    private ?float $qtyLastOrder = null;

    /**
     * @var float|null $qtyLastStockTake The qtyLastStockTake property
    */
    private ?float $qtyLastStockTake = null;

    /**
     * @var float|null $qtyMakeup The qtyMakeup property
    */
    private ?float $qtyMakeup = null;

    /**
     * @var float|null $qtyOnOrder The qtyOnOrder property
    */
    private ?float $qtyOnOrder = null;

    /**
     * @var float|null $qtyReorder The qtyReorder property
    */
    private ?float $qtyReorder = null;

    /**
     * @var float|null $qtyReorderLevel The qtyReorderLevel property
    */
    private ?float $qtyReorderLevel = null;

    /**
     * @var float|null $qtySoldBf The qtySoldBf property
    */
    private ?float $qtySoldBf = null;

    /**
     * @var float|null $qtySoldFuture The qtySoldFuture property
    */
    private ?float $qtySoldFuture = null;

    /**
     * @var float|null $qtySoldMth1 The qtySoldMth1 property
    */
    private ?float $qtySoldMth1 = null;

    /**
     * @var float|null $qtySoldMth10 The qtySoldMth10 property
    */
    private ?float $qtySoldMth10 = null;

    /**
     * @var float|null $qtySoldMth11 The qtySoldMth11 property
    */
    private ?float $qtySoldMth11 = null;

    /**
     * @var float|null $qtySoldMth12 The qtySoldMth12 property
    */
    private ?float $qtySoldMth12 = null;

    /**
     * @var float|null $qtySoldMth2 The qtySoldMth2 property
    */
    private ?float $qtySoldMth2 = null;

    /**
     * @var float|null $qtySoldMth3 The qtySoldMth3 property
    */
    private ?float $qtySoldMth3 = null;

    /**
     * @var float|null $qtySoldMth4 The qtySoldMth4 property
    */
    private ?float $qtySoldMth4 = null;

    /**
     * @var float|null $qtySoldMth5 The qtySoldMth5 property
    */
    private ?float $qtySoldMth5 = null;

    /**
     * @var float|null $qtySoldMth6 The qtySoldMth6 property
    */
    private ?float $qtySoldMth6 = null;

    /**
     * @var float|null $qtySoldMth7 The qtySoldMth7 property
    */
    private ?float $qtySoldMth7 = null;

    /**
     * @var float|null $qtySoldMth8 The qtySoldMth8 property
    */
    private ?float $qtySoldMth8 = null;

    /**
     * @var float|null $qtySoldMth9 The qtySoldMth9 property
    */
    private ?float $qtySoldMth9 = null;

    /**
     * @var DateTime|null $recordCreateDate The recordCreateDate property
    */
    private ?DateTime $recordCreateDate = null;

    /**
     * @var int|null $recordDeleted The recordDeleted property
    */
    private ?int $recordDeleted = null;

    /**
     * @var DateTime|null $recordModifyDate The recordModifyDate property
    */
    private ?DateTime $recordModifyDate = null;

    /**
     * @var float|null $salesBf The salesBf property
    */
    private ?float $salesBf = null;

    /**
     * @var float|null $salesFuture The salesFuture property
    */
    private ?float $salesFuture = null;

    /**
     * @var float|null $salesMth1 The salesMth1 property
    */
    private ?float $salesMth1 = null;

    /**
     * @var float|null $salesMth10 The salesMth10 property
    */
    private ?float $salesMth10 = null;

    /**
     * @var float|null $salesMth11 The salesMth11 property
    */
    private ?float $salesMth11 = null;

    /**
     * @var float|null $salesMth12 The salesMth12 property
    */
    private ?float $salesMth12 = null;

    /**
     * @var float|null $salesMth2 The salesMth2 property
    */
    private ?float $salesMth2 = null;

    /**
     * @var float|null $salesMth3 The salesMth3 property
    */
    private ?float $salesMth3 = null;

    /**
     * @var float|null $salesMth4 The salesMth4 property
    */
    private ?float $salesMth4 = null;

    /**
     * @var float|null $salesMth5 The salesMth5 property
    */
    private ?float $salesMth5 = null;

    /**
     * @var float|null $salesMth6 The salesMth6 property
    */
    private ?float $salesMth6 = null;

    /**
     * @var float|null $salesMth7 The salesMth7 property
    */
    private ?float $salesMth7 = null;

    /**
     * @var float|null $salesMth8 The salesMth8 property
    */
    private ?float $salesMth8 = null;

    /**
     * @var float|null $salesMth9 The salesMth9 property
    */
    private ?float $salesMth9 = null;

    /**
     * @var float|null $salesPrice The salesPrice property
    */
    private ?float $salesPrice = null;

    /**
     * @var int|null $stockCat The stockCat property
    */
    private ?int $stockCat = null;

    /**
     * @var string|null $stockCatName The stockCatName property
    */
    private ?string $stockCatName = null;

    /**
     * @var DateTime|null $stockTakeDate The stockTakeDate property
    */
    private ?DateTime $stockTakeDate = null;

    /**
     * @var string|null $supplierPartNumber The supplierPartNumber property
    */
    private ?string $supplierPartNumber = null;

    /**
     * @var float|null $suppUnitQty The suppUnitQty property
    */
    private ?float $suppUnitQty = null;

    /**
     * @var StockAttributesRead_taxCode|null $taxCode The taxCode property
    */
    private ?StockAttributesRead_taxCode $taxCode = null;

    /**
     * @var int|null $thisRecord The thisRecord property
    */
    private ?int $thisRecord = null;

    /**
     * @var string|null $unitOfSale The unitOfSale property
    */
    private ?string $unitOfSale = null;

    /**
     * @var float|null $unitWeight The unitWeight property
    */
    private ?float $unitWeight = null;

    /**
     * @var string|null $webCategory1 The webCategory1 property
    */
    private ?string $webCategory1 = null;

    /**
     * @var string|null $webCategory2 The webCategory2 property
    */
    private ?string $webCategory2 = null;

    /**
     * @var string|null $webCategory3 The webCategory3 property
    */
    private ?string $webCategory3 = null;

    /**
     * @var string|null $webDescription The webDescription property
    */
    private ?string $webDescription = null;

    /**
     * @var string|null $webDetails The webDetails property
    */
    private ?string $webDetails = null;

    /**
     * @var string|null $webImageFile The webImageFile property
    */
    private ?string $webImageFile = null;

    /**
     * @var int|null $webPublish The webPublish property
    */
    private ?int $webPublish = null;

    /**
     * @var int|null $webSpecial The webSpecial property
    */
    private ?int $webSpecial = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return StockAttributesRead
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): StockAttributesRead {
        return new StockAttributesRead();
    }

    /**
     * Gets the assemblyLevel property value. The assemblyLevel property
     * @return int|null
    */
    public function getAssemblyLevel(): ?int {
        return $this->assemblyLevel;
    }

    /**
     * Gets the averageCostPrice property value. The averageCostPrice property
     * @return float|null
    */
    public function getAverageCostPrice(): ?float {
        return $this->averageCostPrice;
    }

    /**
     * Gets the barcode property value. The barcode property
     * @return string|null
    */
    public function getBarcode(): ?string {
        return $this->barcode;
    }

    /**
     * Gets the budgetQtySoldBf property value. The budgetQtySoldBf property
     * @return float|null
    */
    public function getBudgetQtySoldBf(): ?float {
        return $this->budgetQtySoldBf;
    }

    /**
     * Gets the budgetQtySoldFuture property value. The budgetQtySoldFuture property
     * @return float|null
    */
    public function getBudgetQtySoldFuture(): ?float {
        return $this->budgetQtySoldFuture;
    }

    /**
     * Gets the budgetQtySoldMth1 property value. The budgetQtySoldMth1 property
     * @return float|null
    */
    public function getBudgetQtySoldMth1(): ?float {
        return $this->budgetQtySoldMth1;
    }

    /**
     * Gets the budgetQtySoldMth10 property value. The budgetQtySoldMth10 property
     * @return float|null
    */
    public function getBudgetQtySoldMth10(): ?float {
        return $this->budgetQtySoldMth10;
    }

    /**
     * Gets the budgetQtySoldMth11 property value. The budgetQtySoldMth11 property
     * @return float|null
    */
    public function getBudgetQtySoldMth11(): ?float {
        return $this->budgetQtySoldMth11;
    }

    /**
     * Gets the budgetQtySoldMth12 property value. The budgetQtySoldMth12 property
     * @return float|null
    */
    public function getBudgetQtySoldMth12(): ?float {
        return $this->budgetQtySoldMth12;
    }

    /**
     * Gets the budgetQtySoldMth2 property value. The budgetQtySoldMth2 property
     * @return float|null
    */
    public function getBudgetQtySoldMth2(): ?float {
        return $this->budgetQtySoldMth2;
    }

    /**
     * Gets the budgetQtySoldMth3 property value. The budgetQtySoldMth3 property
     * @return float|null
    */
    public function getBudgetQtySoldMth3(): ?float {
        return $this->budgetQtySoldMth3;
    }

    /**
     * Gets the budgetQtySoldMth4 property value. The budgetQtySoldMth4 property
     * @return float|null
    */
    public function getBudgetQtySoldMth4(): ?float {
        return $this->budgetQtySoldMth4;
    }

    /**
     * Gets the budgetQtySoldMth5 property value. The budgetQtySoldMth5 property
     * @return float|null
    */
    public function getBudgetQtySoldMth5(): ?float {
        return $this->budgetQtySoldMth5;
    }

    /**
     * Gets the budgetQtySoldMth6 property value. The budgetQtySoldMth6 property
     * @return float|null
    */
    public function getBudgetQtySoldMth6(): ?float {
        return $this->budgetQtySoldMth6;
    }

    /**
     * Gets the budgetQtySoldMth7 property value. The budgetQtySoldMth7 property
     * @return float|null
    */
    public function getBudgetQtySoldMth7(): ?float {
        return $this->budgetQtySoldMth7;
    }

    /**
     * Gets the budgetQtySoldMth8 property value. The budgetQtySoldMth8 property
     * @return float|null
    */
    public function getBudgetQtySoldMth8(): ?float {
        return $this->budgetQtySoldMth8;
    }

    /**
     * Gets the budgetQtySoldMth9 property value. The budgetQtySoldMth9 property
     * @return float|null
    */
    public function getBudgetQtySoldMth9(): ?float {
        return $this->budgetQtySoldMth9;
    }

    /**
     * Gets the budgetSalesBf property value. The budgetSalesBf property
     * @return float|null
    */
    public function getBudgetSalesBf(): ?float {
        return $this->budgetSalesBf;
    }

    /**
     * Gets the budgetSalesFuture property value. The budgetSalesFuture property
     * @return float|null
    */
    public function getBudgetSalesFuture(): ?float {
        return $this->budgetSalesFuture;
    }

    /**
     * Gets the budgetSalesMth1 property value. The budgetSalesMth1 property
     * @return float|null
    */
    public function getBudgetSalesMth1(): ?float {
        return $this->budgetSalesMth1;
    }

    /**
     * Gets the budgetSalesMth10 property value. The budgetSalesMth10 property
     * @return float|null
    */
    public function getBudgetSalesMth10(): ?float {
        return $this->budgetSalesMth10;
    }

    /**
     * Gets the budgetSalesMth11 property value. The budgetSalesMth11 property
     * @return float|null
    */
    public function getBudgetSalesMth11(): ?float {
        return $this->budgetSalesMth11;
    }

    /**
     * Gets the budgetSalesMth12 property value. The budgetSalesMth12 property
     * @return float|null
    */
    public function getBudgetSalesMth12(): ?float {
        return $this->budgetSalesMth12;
    }

    /**
     * Gets the budgetSalesMth2 property value. The budgetSalesMth2 property
     * @return float|null
    */
    public function getBudgetSalesMth2(): ?float {
        return $this->budgetSalesMth2;
    }

    /**
     * Gets the budgetSalesMth3 property value. The budgetSalesMth3 property
     * @return float|null
    */
    public function getBudgetSalesMth3(): ?float {
        return $this->budgetSalesMth3;
    }

    /**
     * Gets the budgetSalesMth4 property value. The budgetSalesMth4 property
     * @return float|null
    */
    public function getBudgetSalesMth4(): ?float {
        return $this->budgetSalesMth4;
    }

    /**
     * Gets the budgetSalesMth5 property value. The budgetSalesMth5 property
     * @return float|null
    */
    public function getBudgetSalesMth5(): ?float {
        return $this->budgetSalesMth5;
    }

    /**
     * Gets the budgetSalesMth6 property value. The budgetSalesMth6 property
     * @return float|null
    */
    public function getBudgetSalesMth6(): ?float {
        return $this->budgetSalesMth6;
    }

    /**
     * Gets the budgetSalesMth7 property value. The budgetSalesMth7 property
     * @return float|null
    */
    public function getBudgetSalesMth7(): ?float {
        return $this->budgetSalesMth7;
    }

    /**
     * Gets the budgetSalesMth8 property value. The budgetSalesMth8 property
     * @return float|null
    */
    public function getBudgetSalesMth8(): ?float {
        return $this->budgetSalesMth8;
    }

    /**
     * Gets the budgetSalesMth9 property value. The budgetSalesMth9 property
     * @return float|null
    */
    public function getBudgetSalesMth9(): ?float {
        return $this->budgetSalesMth9;
    }

    /**
     * Gets the commodityCode property value. The commodityCode property
     * @return string|null
    */
    public function getCommodityCode(): ?string {
        return $this->commodityCode;
    }

    /**
     * Gets the componentCode1 property value. The componentCode1 property
     * @return string|null
    */
    public function getComponentCode1(): ?string {
        return $this->componentCode1;
    }

    /**
     * Gets the componentCode10 property value. The componentCode10 property
     * @return string|null
    */
    public function getComponentCode10(): ?string {
        return $this->componentCode10;
    }

    /**
     * Gets the componentCode11 property value. The componentCode11 property
     * @return string|null
    */
    public function getComponentCode11(): ?string {
        return $this->componentCode11;
    }

    /**
     * Gets the componentCode12 property value. The componentCode12 property
     * @return string|null
    */
    public function getComponentCode12(): ?string {
        return $this->componentCode12;
    }

    /**
     * Gets the componentCode13 property value. The componentCode13 property
     * @return string|null
    */
    public function getComponentCode13(): ?string {
        return $this->componentCode13;
    }

    /**
     * Gets the componentCode14 property value. The componentCode14 property
     * @return string|null
    */
    public function getComponentCode14(): ?string {
        return $this->componentCode14;
    }

    /**
     * Gets the componentCode15 property value. The componentCode15 property
     * @return string|null
    */
    public function getComponentCode15(): ?string {
        return $this->componentCode15;
    }

    /**
     * Gets the componentCode16 property value. The componentCode16 property
     * @return string|null
    */
    public function getComponentCode16(): ?string {
        return $this->componentCode16;
    }

    /**
     * Gets the componentCode17 property value. The componentCode17 property
     * @return string|null
    */
    public function getComponentCode17(): ?string {
        return $this->componentCode17;
    }

    /**
     * Gets the componentCode18 property value. The componentCode18 property
     * @return string|null
    */
    public function getComponentCode18(): ?string {
        return $this->componentCode18;
    }

    /**
     * Gets the componentCode19 property value. The componentCode19 property
     * @return string|null
    */
    public function getComponentCode19(): ?string {
        return $this->componentCode19;
    }

    /**
     * Gets the componentCode2 property value. The componentCode2 property
     * @return string|null
    */
    public function getComponentCode2(): ?string {
        return $this->componentCode2;
    }

    /**
     * Gets the componentCode20 property value. The componentCode20 property
     * @return string|null
    */
    public function getComponentCode20(): ?string {
        return $this->componentCode20;
    }

    /**
     * Gets the componentCode21 property value. The componentCode21 property
     * @return string|null
    */
    public function getComponentCode21(): ?string {
        return $this->componentCode21;
    }

    /**
     * Gets the componentCode22 property value. The componentCode22 property
     * @return string|null
    */
    public function getComponentCode22(): ?string {
        return $this->componentCode22;
    }

    /**
     * Gets the componentCode23 property value. The componentCode23 property
     * @return string|null
    */
    public function getComponentCode23(): ?string {
        return $this->componentCode23;
    }

    /**
     * Gets the componentCode24 property value. The componentCode24 property
     * @return string|null
    */
    public function getComponentCode24(): ?string {
        return $this->componentCode24;
    }

    /**
     * Gets the componentCode25 property value. The componentCode25 property
     * @return string|null
    */
    public function getComponentCode25(): ?string {
        return $this->componentCode25;
    }

    /**
     * Gets the componentCode26 property value. The componentCode26 property
     * @return string|null
    */
    public function getComponentCode26(): ?string {
        return $this->componentCode26;
    }

    /**
     * Gets the componentCode27 property value. The componentCode27 property
     * @return string|null
    */
    public function getComponentCode27(): ?string {
        return $this->componentCode27;
    }

    /**
     * Gets the componentCode28 property value. The componentCode28 property
     * @return string|null
    */
    public function getComponentCode28(): ?string {
        return $this->componentCode28;
    }

    /**
     * Gets the componentCode29 property value. The componentCode29 property
     * @return string|null
    */
    public function getComponentCode29(): ?string {
        return $this->componentCode29;
    }

    /**
     * Gets the componentCode3 property value. The componentCode3 property
     * @return string|null
    */
    public function getComponentCode3(): ?string {
        return $this->componentCode3;
    }

    /**
     * Gets the componentCode30 property value. The componentCode30 property
     * @return string|null
    */
    public function getComponentCode30(): ?string {
        return $this->componentCode30;
    }

    /**
     * Gets the componentCode31 property value. The componentCode31 property
     * @return string|null
    */
    public function getComponentCode31(): ?string {
        return $this->componentCode31;
    }

    /**
     * Gets the componentCode32 property value. The componentCode32 property
     * @return string|null
    */
    public function getComponentCode32(): ?string {
        return $this->componentCode32;
    }

    /**
     * Gets the componentCode33 property value. The componentCode33 property
     * @return string|null
    */
    public function getComponentCode33(): ?string {
        return $this->componentCode33;
    }

    /**
     * Gets the componentCode34 property value. The componentCode34 property
     * @return string|null
    */
    public function getComponentCode34(): ?string {
        return $this->componentCode34;
    }

    /**
     * Gets the componentCode35 property value. The componentCode35 property
     * @return string|null
    */
    public function getComponentCode35(): ?string {
        return $this->componentCode35;
    }

    /**
     * Gets the componentCode36 property value. The componentCode36 property
     * @return string|null
    */
    public function getComponentCode36(): ?string {
        return $this->componentCode36;
    }

    /**
     * Gets the componentCode37 property value. The componentCode37 property
     * @return string|null
    */
    public function getComponentCode37(): ?string {
        return $this->componentCode37;
    }

    /**
     * Gets the componentCode38 property value. The componentCode38 property
     * @return string|null
    */
    public function getComponentCode38(): ?string {
        return $this->componentCode38;
    }

    /**
     * Gets the componentCode39 property value. The componentCode39 property
     * @return string|null
    */
    public function getComponentCode39(): ?string {
        return $this->componentCode39;
    }

    /**
     * Gets the componentCode4 property value. The componentCode4 property
     * @return string|null
    */
    public function getComponentCode4(): ?string {
        return $this->componentCode4;
    }

    /**
     * Gets the componentCode40 property value. The componentCode40 property
     * @return string|null
    */
    public function getComponentCode40(): ?string {
        return $this->componentCode40;
    }

    /**
     * Gets the componentCode41 property value. The componentCode41 property
     * @return string|null
    */
    public function getComponentCode41(): ?string {
        return $this->componentCode41;
    }

    /**
     * Gets the componentCode42 property value. The componentCode42 property
     * @return string|null
    */
    public function getComponentCode42(): ?string {
        return $this->componentCode42;
    }

    /**
     * Gets the componentCode43 property value. The componentCode43 property
     * @return string|null
    */
    public function getComponentCode43(): ?string {
        return $this->componentCode43;
    }

    /**
     * Gets the componentCode44 property value. The componentCode44 property
     * @return string|null
    */
    public function getComponentCode44(): ?string {
        return $this->componentCode44;
    }

    /**
     * Gets the componentCode45 property value. The componentCode45 property
     * @return string|null
    */
    public function getComponentCode45(): ?string {
        return $this->componentCode45;
    }

    /**
     * Gets the componentCode46 property value. The componentCode46 property
     * @return string|null
    */
    public function getComponentCode46(): ?string {
        return $this->componentCode46;
    }

    /**
     * Gets the componentCode47 property value. The componentCode47 property
     * @return string|null
    */
    public function getComponentCode47(): ?string {
        return $this->componentCode47;
    }

    /**
     * Gets the componentCode48 property value. The componentCode48 property
     * @return string|null
    */
    public function getComponentCode48(): ?string {
        return $this->componentCode48;
    }

    /**
     * Gets the componentCode49 property value. The componentCode49 property
     * @return string|null
    */
    public function getComponentCode49(): ?string {
        return $this->componentCode49;
    }

    /**
     * Gets the componentCode5 property value. The componentCode5 property
     * @return string|null
    */
    public function getComponentCode5(): ?string {
        return $this->componentCode5;
    }

    /**
     * Gets the componentCode50 property value. The componentCode50 property
     * @return string|null
    */
    public function getComponentCode50(): ?string {
        return $this->componentCode50;
    }

    /**
     * Gets the componentCode6 property value. The componentCode6 property
     * @return string|null
    */
    public function getComponentCode6(): ?string {
        return $this->componentCode6;
    }

    /**
     * Gets the componentCode7 property value. The componentCode7 property
     * @return string|null
    */
    public function getComponentCode7(): ?string {
        return $this->componentCode7;
    }

    /**
     * Gets the componentCode8 property value. The componentCode8 property
     * @return string|null
    */
    public function getComponentCode8(): ?string {
        return $this->componentCode8;
    }

    /**
     * Gets the componentCode9 property value. The componentCode9 property
     * @return string|null
    */
    public function getComponentCode9(): ?string {
        return $this->componentCode9;
    }

    /**
     * Gets the componentQty1 property value. The componentQty1 property
     * @return float|null
    */
    public function getComponentQty1(): ?float {
        return $this->componentQty1;
    }

    /**
     * Gets the componentQty10 property value. The componentQty10 property
     * @return float|null
    */
    public function getComponentQty10(): ?float {
        return $this->componentQty10;
    }

    /**
     * Gets the componentQty11 property value. The componentQty11 property
     * @return float|null
    */
    public function getComponentQty11(): ?float {
        return $this->componentQty11;
    }

    /**
     * Gets the componentQty12 property value. The componentQty12 property
     * @return float|null
    */
    public function getComponentQty12(): ?float {
        return $this->componentQty12;
    }

    /**
     * Gets the componentQty13 property value. The componentQty13 property
     * @return float|null
    */
    public function getComponentQty13(): ?float {
        return $this->componentQty13;
    }

    /**
     * Gets the componentQty14 property value. The componentQty14 property
     * @return float|null
    */
    public function getComponentQty14(): ?float {
        return $this->componentQty14;
    }

    /**
     * Gets the componentQty15 property value. The componentQty15 property
     * @return float|null
    */
    public function getComponentQty15(): ?float {
        return $this->componentQty15;
    }

    /**
     * Gets the componentQty16 property value. The componentQty16 property
     * @return float|null
    */
    public function getComponentQty16(): ?float {
        return $this->componentQty16;
    }

    /**
     * Gets the componentQty17 property value. The componentQty17 property
     * @return float|null
    */
    public function getComponentQty17(): ?float {
        return $this->componentQty17;
    }

    /**
     * Gets the componentQty18 property value. The componentQty18 property
     * @return float|null
    */
    public function getComponentQty18(): ?float {
        return $this->componentQty18;
    }

    /**
     * Gets the componentQty19 property value. The componentQty19 property
     * @return float|null
    */
    public function getComponentQty19(): ?float {
        return $this->componentQty19;
    }

    /**
     * Gets the componentQty2 property value. The componentQty2 property
     * @return float|null
    */
    public function getComponentQty2(): ?float {
        return $this->componentQty2;
    }

    /**
     * Gets the componentQty20 property value. The componentQty20 property
     * @return float|null
    */
    public function getComponentQty20(): ?float {
        return $this->componentQty20;
    }

    /**
     * Gets the componentQty21 property value. The componentQty21 property
     * @return float|null
    */
    public function getComponentQty21(): ?float {
        return $this->componentQty21;
    }

    /**
     * Gets the componentQty22 property value. The componentQty22 property
     * @return float|null
    */
    public function getComponentQty22(): ?float {
        return $this->componentQty22;
    }

    /**
     * Gets the componentQty23 property value. The componentQty23 property
     * @return float|null
    */
    public function getComponentQty23(): ?float {
        return $this->componentQty23;
    }

    /**
     * Gets the componentQty24 property value. The componentQty24 property
     * @return float|null
    */
    public function getComponentQty24(): ?float {
        return $this->componentQty24;
    }

    /**
     * Gets the componentQty25 property value. The componentQty25 property
     * @return float|null
    */
    public function getComponentQty25(): ?float {
        return $this->componentQty25;
    }

    /**
     * Gets the componentQty26 property value. The componentQty26 property
     * @return float|null
    */
    public function getComponentQty26(): ?float {
        return $this->componentQty26;
    }

    /**
     * Gets the componentQty27 property value. The componentQty27 property
     * @return float|null
    */
    public function getComponentQty27(): ?float {
        return $this->componentQty27;
    }

    /**
     * Gets the componentQty28 property value. The componentQty28 property
     * @return float|null
    */
    public function getComponentQty28(): ?float {
        return $this->componentQty28;
    }

    /**
     * Gets the componentQty29 property value. The componentQty29 property
     * @return float|null
    */
    public function getComponentQty29(): ?float {
        return $this->componentQty29;
    }

    /**
     * Gets the componentQty3 property value. The componentQty3 property
     * @return float|null
    */
    public function getComponentQty3(): ?float {
        return $this->componentQty3;
    }

    /**
     * Gets the componentQty30 property value. The componentQty30 property
     * @return float|null
    */
    public function getComponentQty30(): ?float {
        return $this->componentQty30;
    }

    /**
     * Gets the componentQty31 property value. The componentQty31 property
     * @return float|null
    */
    public function getComponentQty31(): ?float {
        return $this->componentQty31;
    }

    /**
     * Gets the componentQty32 property value. The componentQty32 property
     * @return float|null
    */
    public function getComponentQty32(): ?float {
        return $this->componentQty32;
    }

    /**
     * Gets the componentQty33 property value. The componentQty33 property
     * @return float|null
    */
    public function getComponentQty33(): ?float {
        return $this->componentQty33;
    }

    /**
     * Gets the componentQty34 property value. The componentQty34 property
     * @return float|null
    */
    public function getComponentQty34(): ?float {
        return $this->componentQty34;
    }

    /**
     * Gets the componentQty35 property value. The componentQty35 property
     * @return float|null
    */
    public function getComponentQty35(): ?float {
        return $this->componentQty35;
    }

    /**
     * Gets the componentQty36 property value. The componentQty36 property
     * @return float|null
    */
    public function getComponentQty36(): ?float {
        return $this->componentQty36;
    }

    /**
     * Gets the componentQty37 property value. The componentQty37 property
     * @return float|null
    */
    public function getComponentQty37(): ?float {
        return $this->componentQty37;
    }

    /**
     * Gets the componentQty38 property value. The componentQty38 property
     * @return float|null
    */
    public function getComponentQty38(): ?float {
        return $this->componentQty38;
    }

    /**
     * Gets the componentQty39 property value. The componentQty39 property
     * @return float|null
    */
    public function getComponentQty39(): ?float {
        return $this->componentQty39;
    }

    /**
     * Gets the componentQty4 property value. The componentQty4 property
     * @return float|null
    */
    public function getComponentQty4(): ?float {
        return $this->componentQty4;
    }

    /**
     * Gets the componentQty40 property value. The componentQty40 property
     * @return float|null
    */
    public function getComponentQty40(): ?float {
        return $this->componentQty40;
    }

    /**
     * Gets the componentQty41 property value. The componentQty41 property
     * @return float|null
    */
    public function getComponentQty41(): ?float {
        return $this->componentQty41;
    }

    /**
     * Gets the componentQty42 property value. The componentQty42 property
     * @return float|null
    */
    public function getComponentQty42(): ?float {
        return $this->componentQty42;
    }

    /**
     * Gets the componentQty43 property value. The componentQty43 property
     * @return float|null
    */
    public function getComponentQty43(): ?float {
        return $this->componentQty43;
    }

    /**
     * Gets the componentQty44 property value. The componentQty44 property
     * @return float|null
    */
    public function getComponentQty44(): ?float {
        return $this->componentQty44;
    }

    /**
     * Gets the componentQty45 property value. The componentQty45 property
     * @return float|null
    */
    public function getComponentQty45(): ?float {
        return $this->componentQty45;
    }

    /**
     * Gets the componentQty46 property value. The componentQty46 property
     * @return float|null
    */
    public function getComponentQty46(): ?float {
        return $this->componentQty46;
    }

    /**
     * Gets the componentQty47 property value. The componentQty47 property
     * @return float|null
    */
    public function getComponentQty47(): ?float {
        return $this->componentQty47;
    }

    /**
     * Gets the componentQty48 property value. The componentQty48 property
     * @return float|null
    */
    public function getComponentQty48(): ?float {
        return $this->componentQty48;
    }

    /**
     * Gets the componentQty49 property value. The componentQty49 property
     * @return float|null
    */
    public function getComponentQty49(): ?float {
        return $this->componentQty49;
    }

    /**
     * Gets the componentQty5 property value. The componentQty5 property
     * @return float|null
    */
    public function getComponentQty5(): ?float {
        return $this->componentQty5;
    }

    /**
     * Gets the componentQty50 property value. The componentQty50 property
     * @return float|null
    */
    public function getComponentQty50(): ?float {
        return $this->componentQty50;
    }

    /**
     * Gets the componentQty6 property value. The componentQty6 property
     * @return float|null
    */
    public function getComponentQty6(): ?float {
        return $this->componentQty6;
    }

    /**
     * Gets the componentQty7 property value. The componentQty7 property
     * @return float|null
    */
    public function getComponentQty7(): ?float {
        return $this->componentQty7;
    }

    /**
     * Gets the componentQty8 property value. The componentQty8 property
     * @return float|null
    */
    public function getComponentQty8(): ?float {
        return $this->componentQty8;
    }

    /**
     * Gets the componentQty9 property value. The componentQty9 property
     * @return float|null
    */
    public function getComponentQty9(): ?float {
        return $this->componentQty9;
    }

    /**
     * Gets the costBf property value. The costBf property
     * @return float|null
    */
    public function getCostBf(): ?float {
        return $this->costBf;
    }

    /**
     * Gets the costFuture property value. The costFuture property
     * @return float|null
    */
    public function getCostFuture(): ?float {
        return $this->costFuture;
    }

    /**
     * Gets the costMth1 property value. The costMth1 property
     * @return float|null
    */
    public function getCostMth1(): ?float {
        return $this->costMth1;
    }

    /**
     * Gets the costMth10 property value. The costMth10 property
     * @return float|null
    */
    public function getCostMth10(): ?float {
        return $this->costMth10;
    }

    /**
     * Gets the costMth11 property value. The costMth11 property
     * @return float|null
    */
    public function getCostMth11(): ?float {
        return $this->costMth11;
    }

    /**
     * Gets the costMth12 property value. The costMth12 property
     * @return float|null
    */
    public function getCostMth12(): ?float {
        return $this->costMth12;
    }

    /**
     * Gets the costMth2 property value. The costMth2 property
     * @return float|null
    */
    public function getCostMth2(): ?float {
        return $this->costMth2;
    }

    /**
     * Gets the costMth3 property value. The costMth3 property
     * @return float|null
    */
    public function getCostMth3(): ?float {
        return $this->costMth3;
    }

    /**
     * Gets the costMth4 property value. The costMth4 property
     * @return float|null
    */
    public function getCostMth4(): ?float {
        return $this->costMth4;
    }

    /**
     * Gets the costMth5 property value. The costMth5 property
     * @return float|null
    */
    public function getCostMth5(): ?float {
        return $this->costMth5;
    }

    /**
     * Gets the costMth6 property value. The costMth6 property
     * @return float|null
    */
    public function getCostMth6(): ?float {
        return $this->costMth6;
    }

    /**
     * Gets the costMth7 property value. The costMth7 property
     * @return float|null
    */
    public function getCostMth7(): ?float {
        return $this->costMth7;
    }

    /**
     * Gets the costMth8 property value. The costMth8 property
     * @return float|null
    */
    public function getCostMth8(): ?float {
        return $this->costMth8;
    }

    /**
     * Gets the costMth9 property value. The costMth9 property
     * @return float|null
    */
    public function getCostMth9(): ?float {
        return $this->costMth9;
    }

    /**
     * Gets the countryCodeOfOrigin property value. The countryCodeOfOrigin property
     * @return string|null
    */
    public function getCountryCodeOfOrigin(): ?string {
        return $this->countryCodeOfOrigin;
    }

    /**
     * Gets the deptName property value. The deptName property
     * @return string|null
    */
    public function getDeptName(): ?string {
        return $this->deptName;
    }

    /**
     * Gets the deptNumber property value. The deptNumber property
     * @return int|null
    */
    public function getDeptNumber(): ?int {
        return $this->deptNumber;
    }

    /**
     * Gets the description property value. The description property
     * @return string|null
    */
    public function getDescription(): ?string {
        return $this->description;
    }

    /**
     * Gets the discALevel10Qty property value. The discALevel10Qty property
     * @return float|null
    */
    public function getDiscALevel10Qty(): ?float {
        return $this->discALevel10Qty;
    }

    /**
     * Gets the discALevel10Rate property value. The discALevel10Rate property
     * @return float|null
    */
    public function getDiscALevel10Rate(): ?float {
        return $this->discALevel10Rate;
    }

    /**
     * Gets the discALevel1Qty property value. The discALevel1Qty property
     * @return float|null
    */
    public function getDiscALevel1Qty(): ?float {
        return $this->discALevel1Qty;
    }

    /**
     * Gets the discALevel1Rate property value. The discALevel1Rate property
     * @return float|null
    */
    public function getDiscALevel1Rate(): ?float {
        return $this->discALevel1Rate;
    }

    /**
     * Gets the discALevel2Qty property value. The discALevel2Qty property
     * @return float|null
    */
    public function getDiscALevel2Qty(): ?float {
        return $this->discALevel2Qty;
    }

    /**
     * Gets the discALevel2Rate property value. The discALevel2Rate property
     * @return float|null
    */
    public function getDiscALevel2Rate(): ?float {
        return $this->discALevel2Rate;
    }

    /**
     * Gets the discALevel3Qty property value. The discALevel3Qty property
     * @return float|null
    */
    public function getDiscALevel3Qty(): ?float {
        return $this->discALevel3Qty;
    }

    /**
     * Gets the discALevel3Rate property value. The discALevel3Rate property
     * @return float|null
    */
    public function getDiscALevel3Rate(): ?float {
        return $this->discALevel3Rate;
    }

    /**
     * Gets the discALevel4Qty property value. The discALevel4Qty property
     * @return float|null
    */
    public function getDiscALevel4Qty(): ?float {
        return $this->discALevel4Qty;
    }

    /**
     * Gets the discALevel4Rate property value. The discALevel4Rate property
     * @return float|null
    */
    public function getDiscALevel4Rate(): ?float {
        return $this->discALevel4Rate;
    }

    /**
     * Gets the discALevel5Qty property value. The discALevel5Qty property
     * @return float|null
    */
    public function getDiscALevel5Qty(): ?float {
        return $this->discALevel5Qty;
    }

    /**
     * Gets the discALevel5Rate property value. The discALevel5Rate property
     * @return float|null
    */
    public function getDiscALevel5Rate(): ?float {
        return $this->discALevel5Rate;
    }

    /**
     * Gets the discALevel6Qty property value. The discALevel6Qty property
     * @return float|null
    */
    public function getDiscALevel6Qty(): ?float {
        return $this->discALevel6Qty;
    }

    /**
     * Gets the discALevel6Rate property value. The discALevel6Rate property
     * @return float|null
    */
    public function getDiscALevel6Rate(): ?float {
        return $this->discALevel6Rate;
    }

    /**
     * Gets the discALevel7Qty property value. The discALevel7Qty property
     * @return float|null
    */
    public function getDiscALevel7Qty(): ?float {
        return $this->discALevel7Qty;
    }

    /**
     * Gets the discALevel7Rate property value. The discALevel7Rate property
     * @return float|null
    */
    public function getDiscALevel7Rate(): ?float {
        return $this->discALevel7Rate;
    }

    /**
     * Gets the discALevel8Qty property value. The discALevel8Qty property
     * @return float|null
    */
    public function getDiscALevel8Qty(): ?float {
        return $this->discALevel8Qty;
    }

    /**
     * Gets the discALevel8Rate property value. The discALevel8Rate property
     * @return float|null
    */
    public function getDiscALevel8Rate(): ?float {
        return $this->discALevel8Rate;
    }

    /**
     * Gets the discALevel9Qty property value. The discALevel9Qty property
     * @return float|null
    */
    public function getDiscALevel9Qty(): ?float {
        return $this->discALevel9Qty;
    }

    /**
     * Gets the discALevel9Rate property value. The discALevel9Rate property
     * @return float|null
    */
    public function getDiscALevel9Rate(): ?float {
        return $this->discALevel9Rate;
    }

    /**
     * Gets the discBLevel10Qty property value. The discBLevel10Qty property
     * @return float|null
    */
    public function getDiscBLevel10Qty(): ?float {
        return $this->discBLevel10Qty;
    }

    /**
     * Gets the discBLevel10Rate property value. The discBLevel10Rate property
     * @return float|null
    */
    public function getDiscBLevel10Rate(): ?float {
        return $this->discBLevel10Rate;
    }

    /**
     * Gets the discBLevel1Qty property value. The discBLevel1Qty property
     * @return float|null
    */
    public function getDiscBLevel1Qty(): ?float {
        return $this->discBLevel1Qty;
    }

    /**
     * Gets the discBLevel1Rate property value. The discBLevel1Rate property
     * @return float|null
    */
    public function getDiscBLevel1Rate(): ?float {
        return $this->discBLevel1Rate;
    }

    /**
     * Gets the discBLevel2Qty property value. The discBLevel2Qty property
     * @return float|null
    */
    public function getDiscBLevel2Qty(): ?float {
        return $this->discBLevel2Qty;
    }

    /**
     * Gets the discBLevel2Rate property value. The discBLevel2Rate property
     * @return float|null
    */
    public function getDiscBLevel2Rate(): ?float {
        return $this->discBLevel2Rate;
    }

    /**
     * Gets the discBLevel3Qty property value. The discBLevel3Qty property
     * @return float|null
    */
    public function getDiscBLevel3Qty(): ?float {
        return $this->discBLevel3Qty;
    }

    /**
     * Gets the discBLevel3Rate property value. The discBLevel3Rate property
     * @return float|null
    */
    public function getDiscBLevel3Rate(): ?float {
        return $this->discBLevel3Rate;
    }

    /**
     * Gets the discBLevel4Qty property value. The discBLevel4Qty property
     * @return float|null
    */
    public function getDiscBLevel4Qty(): ?float {
        return $this->discBLevel4Qty;
    }

    /**
     * Gets the discBLevel4Rate property value. The discBLevel4Rate property
     * @return float|null
    */
    public function getDiscBLevel4Rate(): ?float {
        return $this->discBLevel4Rate;
    }

    /**
     * Gets the discBLevel5Qty property value. The discBLevel5Qty property
     * @return float|null
    */
    public function getDiscBLevel5Qty(): ?float {
        return $this->discBLevel5Qty;
    }

    /**
     * Gets the discBLevel5Rate property value. The discBLevel5Rate property
     * @return float|null
    */
    public function getDiscBLevel5Rate(): ?float {
        return $this->discBLevel5Rate;
    }

    /**
     * Gets the discBLevel6Qty property value. The discBLevel6Qty property
     * @return float|null
    */
    public function getDiscBLevel6Qty(): ?float {
        return $this->discBLevel6Qty;
    }

    /**
     * Gets the discBLevel6Rate property value. The discBLevel6Rate property
     * @return float|null
    */
    public function getDiscBLevel6Rate(): ?float {
        return $this->discBLevel6Rate;
    }

    /**
     * Gets the discBLevel7Qty property value. The discBLevel7Qty property
     * @return float|null
    */
    public function getDiscBLevel7Qty(): ?float {
        return $this->discBLevel7Qty;
    }

    /**
     * Gets the discBLevel7Rate property value. The discBLevel7Rate property
     * @return float|null
    */
    public function getDiscBLevel7Rate(): ?float {
        return $this->discBLevel7Rate;
    }

    /**
     * Gets the discBLevel8Qty property value. The discBLevel8Qty property
     * @return float|null
    */
    public function getDiscBLevel8Qty(): ?float {
        return $this->discBLevel8Qty;
    }

    /**
     * Gets the discBLevel8Rate property value. The discBLevel8Rate property
     * @return float|null
    */
    public function getDiscBLevel8Rate(): ?float {
        return $this->discBLevel8Rate;
    }

    /**
     * Gets the discBLevel9Qty property value. The discBLevel9Qty property
     * @return float|null
    */
    public function getDiscBLevel9Qty(): ?float {
        return $this->discBLevel9Qty;
    }

    /**
     * Gets the discBLevel9Rate property value. The discBLevel9Rate property
     * @return float|null
    */
    public function getDiscBLevel9Rate(): ?float {
        return $this->discBLevel9Rate;
    }

    /**
     * Gets the discCLevel10Qty property value. The discCLevel10Qty property
     * @return float|null
    */
    public function getDiscCLevel10Qty(): ?float {
        return $this->discCLevel10Qty;
    }

    /**
     * Gets the discCLevel10Rate property value. The discCLevel10Rate property
     * @return float|null
    */
    public function getDiscCLevel10Rate(): ?float {
        return $this->discCLevel10Rate;
    }

    /**
     * Gets the discCLevel1Qty property value. The discCLevel1Qty property
     * @return float|null
    */
    public function getDiscCLevel1Qty(): ?float {
        return $this->discCLevel1Qty;
    }

    /**
     * Gets the discCLevel1Rate property value. The discCLevel1Rate property
     * @return float|null
    */
    public function getDiscCLevel1Rate(): ?float {
        return $this->discCLevel1Rate;
    }

    /**
     * Gets the discCLevel2Qty property value. The discCLevel2Qty property
     * @return float|null
    */
    public function getDiscCLevel2Qty(): ?float {
        return $this->discCLevel2Qty;
    }

    /**
     * Gets the discCLevel2Rate property value. The discCLevel2Rate property
     * @return float|null
    */
    public function getDiscCLevel2Rate(): ?float {
        return $this->discCLevel2Rate;
    }

    /**
     * Gets the discCLevel3Qty property value. The discCLevel3Qty property
     * @return float|null
    */
    public function getDiscCLevel3Qty(): ?float {
        return $this->discCLevel3Qty;
    }

    /**
     * Gets the discCLevel3Rate property value. The discCLevel3Rate property
     * @return float|null
    */
    public function getDiscCLevel3Rate(): ?float {
        return $this->discCLevel3Rate;
    }

    /**
     * Gets the discCLevel4Qty property value. The discCLevel4Qty property
     * @return float|null
    */
    public function getDiscCLevel4Qty(): ?float {
        return $this->discCLevel4Qty;
    }

    /**
     * Gets the discCLevel4Rate property value. The discCLevel4Rate property
     * @return float|null
    */
    public function getDiscCLevel4Rate(): ?float {
        return $this->discCLevel4Rate;
    }

    /**
     * Gets the discCLevel5Qty property value. The discCLevel5Qty property
     * @return float|null
    */
    public function getDiscCLevel5Qty(): ?float {
        return $this->discCLevel5Qty;
    }

    /**
     * Gets the discCLevel5Rate property value. The discCLevel5Rate property
     * @return float|null
    */
    public function getDiscCLevel5Rate(): ?float {
        return $this->discCLevel5Rate;
    }

    /**
     * Gets the discCLevel6Qty property value. The discCLevel6Qty property
     * @return float|null
    */
    public function getDiscCLevel6Qty(): ?float {
        return $this->discCLevel6Qty;
    }

    /**
     * Gets the discCLevel6Rate property value. The discCLevel6Rate property
     * @return float|null
    */
    public function getDiscCLevel6Rate(): ?float {
        return $this->discCLevel6Rate;
    }

    /**
     * Gets the discCLevel7Qty property value. The discCLevel7Qty property
     * @return float|null
    */
    public function getDiscCLevel7Qty(): ?float {
        return $this->discCLevel7Qty;
    }

    /**
     * Gets the discCLevel7Rate property value. The discCLevel7Rate property
     * @return float|null
    */
    public function getDiscCLevel7Rate(): ?float {
        return $this->discCLevel7Rate;
    }

    /**
     * Gets the discCLevel8Qty property value. The discCLevel8Qty property
     * @return float|null
    */
    public function getDiscCLevel8Qty(): ?float {
        return $this->discCLevel8Qty;
    }

    /**
     * Gets the discCLevel8Rate property value. The discCLevel8Rate property
     * @return float|null
    */
    public function getDiscCLevel8Rate(): ?float {
        return $this->discCLevel8Rate;
    }

    /**
     * Gets the discCLevel9Qty property value. The discCLevel9Qty property
     * @return float|null
    */
    public function getDiscCLevel9Qty(): ?float {
        return $this->discCLevel9Qty;
    }

    /**
     * Gets the discCLevel9Rate property value. The discCLevel9Rate property
     * @return float|null
    */
    public function getDiscCLevel9Rate(): ?float {
        return $this->discCLevel9Rate;
    }

    /**
     * Gets the discDLevel10Qty property value. The discDLevel10Qty property
     * @return float|null
    */
    public function getDiscDLevel10Qty(): ?float {
        return $this->discDLevel10Qty;
    }

    /**
     * Gets the discDLevel10Rate property value. The discDLevel10Rate property
     * @return float|null
    */
    public function getDiscDLevel10Rate(): ?float {
        return $this->discDLevel10Rate;
    }

    /**
     * Gets the discDLevel1Qty property value. The discDLevel1Qty property
     * @return float|null
    */
    public function getDiscDLevel1Qty(): ?float {
        return $this->discDLevel1Qty;
    }

    /**
     * Gets the discDLevel1Rate property value. The discDLevel1Rate property
     * @return float|null
    */
    public function getDiscDLevel1Rate(): ?float {
        return $this->discDLevel1Rate;
    }

    /**
     * Gets the discDLevel2Qty property value. The discDLevel2Qty property
     * @return float|null
    */
    public function getDiscDLevel2Qty(): ?float {
        return $this->discDLevel2Qty;
    }

    /**
     * Gets the discDLevel2Rate property value. The discDLevel2Rate property
     * @return float|null
    */
    public function getDiscDLevel2Rate(): ?float {
        return $this->discDLevel2Rate;
    }

    /**
     * Gets the discDLevel3Qty property value. The discDLevel3Qty property
     * @return float|null
    */
    public function getDiscDLevel3Qty(): ?float {
        return $this->discDLevel3Qty;
    }

    /**
     * Gets the discDLevel3Rate property value. The discDLevel3Rate property
     * @return float|null
    */
    public function getDiscDLevel3Rate(): ?float {
        return $this->discDLevel3Rate;
    }

    /**
     * Gets the discDLevel4Qty property value. The discDLevel4Qty property
     * @return float|null
    */
    public function getDiscDLevel4Qty(): ?float {
        return $this->discDLevel4Qty;
    }

    /**
     * Gets the discDLevel4Rate property value. The discDLevel4Rate property
     * @return float|null
    */
    public function getDiscDLevel4Rate(): ?float {
        return $this->discDLevel4Rate;
    }

    /**
     * Gets the discDLevel5Qty property value. The discDLevel5Qty property
     * @return float|null
    */
    public function getDiscDLevel5Qty(): ?float {
        return $this->discDLevel5Qty;
    }

    /**
     * Gets the discDLevel5Rate property value. The discDLevel5Rate property
     * @return float|null
    */
    public function getDiscDLevel5Rate(): ?float {
        return $this->discDLevel5Rate;
    }

    /**
     * Gets the discDLevel6Qty property value. The discDLevel6Qty property
     * @return float|null
    */
    public function getDiscDLevel6Qty(): ?float {
        return $this->discDLevel6Qty;
    }

    /**
     * Gets the discDLevel6Rate property value. The discDLevel6Rate property
     * @return float|null
    */
    public function getDiscDLevel6Rate(): ?float {
        return $this->discDLevel6Rate;
    }

    /**
     * Gets the discDLevel7Qty property value. The discDLevel7Qty property
     * @return float|null
    */
    public function getDiscDLevel7Qty(): ?float {
        return $this->discDLevel7Qty;
    }

    /**
     * Gets the discDLevel7Rate property value. The discDLevel7Rate property
     * @return float|null
    */
    public function getDiscDLevel7Rate(): ?float {
        return $this->discDLevel7Rate;
    }

    /**
     * Gets the discDLevel8Qty property value. The discDLevel8Qty property
     * @return float|null
    */
    public function getDiscDLevel8Qty(): ?float {
        return $this->discDLevel8Qty;
    }

    /**
     * Gets the discDLevel8Rate property value. The discDLevel8Rate property
     * @return float|null
    */
    public function getDiscDLevel8Rate(): ?float {
        return $this->discDLevel8Rate;
    }

    /**
     * Gets the discDLevel9Qty property value. The discDLevel9Qty property
     * @return float|null
    */
    public function getDiscDLevel9Qty(): ?float {
        return $this->discDLevel9Qty;
    }

    /**
     * Gets the discDLevel9Rate property value. The discDLevel9Rate property
     * @return float|null
    */
    public function getDiscDLevel9Rate(): ?float {
        return $this->discDLevel9Rate;
    }

    /**
     * Gets the discELevel10Qty property value. The discELevel10Qty property
     * @return float|null
    */
    public function getDiscELevel10Qty(): ?float {
        return $this->discELevel10Qty;
    }

    /**
     * Gets the discELevel10Rate property value. The discELevel10Rate property
     * @return float|null
    */
    public function getDiscELevel10Rate(): ?float {
        return $this->discELevel10Rate;
    }

    /**
     * Gets the discELevel1Qty property value. The discELevel1Qty property
     * @return float|null
    */
    public function getDiscELevel1Qty(): ?float {
        return $this->discELevel1Qty;
    }

    /**
     * Gets the discELevel1Rate property value. The discELevel1Rate property
     * @return float|null
    */
    public function getDiscELevel1Rate(): ?float {
        return $this->discELevel1Rate;
    }

    /**
     * Gets the discELevel2Qty property value. The discELevel2Qty property
     * @return float|null
    */
    public function getDiscELevel2Qty(): ?float {
        return $this->discELevel2Qty;
    }

    /**
     * Gets the discELevel2Rate property value. The discELevel2Rate property
     * @return float|null
    */
    public function getDiscELevel2Rate(): ?float {
        return $this->discELevel2Rate;
    }

    /**
     * Gets the discELevel3Qty property value. The discELevel3Qty property
     * @return float|null
    */
    public function getDiscELevel3Qty(): ?float {
        return $this->discELevel3Qty;
    }

    /**
     * Gets the discELevel3Rate property value. The discELevel3Rate property
     * @return float|null
    */
    public function getDiscELevel3Rate(): ?float {
        return $this->discELevel3Rate;
    }

    /**
     * Gets the discELevel4Qty property value. The discELevel4Qty property
     * @return float|null
    */
    public function getDiscELevel4Qty(): ?float {
        return $this->discELevel4Qty;
    }

    /**
     * Gets the discELevel4Rate property value. The discELevel4Rate property
     * @return float|null
    */
    public function getDiscELevel4Rate(): ?float {
        return $this->discELevel4Rate;
    }

    /**
     * Gets the discELevel5Qty property value. The discELevel5Qty property
     * @return float|null
    */
    public function getDiscELevel5Qty(): ?float {
        return $this->discELevel5Qty;
    }

    /**
     * Gets the discELevel5Rate property value. The discELevel5Rate property
     * @return float|null
    */
    public function getDiscELevel5Rate(): ?float {
        return $this->discELevel5Rate;
    }

    /**
     * Gets the discELevel6Qty property value. The discELevel6Qty property
     * @return float|null
    */
    public function getDiscELevel6Qty(): ?float {
        return $this->discELevel6Qty;
    }

    /**
     * Gets the discELevel6Rate property value. The discELevel6Rate property
     * @return float|null
    */
    public function getDiscELevel6Rate(): ?float {
        return $this->discELevel6Rate;
    }

    /**
     * Gets the discELevel7Qty property value. The discELevel7Qty property
     * @return float|null
    */
    public function getDiscELevel7Qty(): ?float {
        return $this->discELevel7Qty;
    }

    /**
     * Gets the discELevel7Rate property value. The discELevel7Rate property
     * @return float|null
    */
    public function getDiscELevel7Rate(): ?float {
        return $this->discELevel7Rate;
    }

    /**
     * Gets the discELevel8Qty property value. The discELevel8Qty property
     * @return float|null
    */
    public function getDiscELevel8Qty(): ?float {
        return $this->discELevel8Qty;
    }

    /**
     * Gets the discELevel8Rate property value. The discELevel8Rate property
     * @return float|null
    */
    public function getDiscELevel8Rate(): ?float {
        return $this->discELevel8Rate;
    }

    /**
     * Gets the discELevel9Qty property value. The discELevel9Qty property
     * @return float|null
    */
    public function getDiscELevel9Qty(): ?float {
        return $this->discELevel9Qty;
    }

    /**
     * Gets the discELevel9Rate property value. The discELevel9Rate property
     * @return float|null
    */
    public function getDiscELevel9Rate(): ?float {
        return $this->discELevel9Rate;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'assemblyLevel' => fn(ParseNode $n) => $o->setAssemblyLevel($n->getIntegerValue()),
            'averageCostPrice' => fn(ParseNode $n) => $o->setAverageCostPrice($n->getFloatValue()),
            'barcode' => fn(ParseNode $n) => $o->setBarcode($n->getStringValue()),
            'budgetQtySoldBf' => fn(ParseNode $n) => $o->setBudgetQtySoldBf($n->getFloatValue()),
            'budgetQtySoldFuture' => fn(ParseNode $n) => $o->setBudgetQtySoldFuture($n->getFloatValue()),
            'budgetQtySoldMth1' => fn(ParseNode $n) => $o->setBudgetQtySoldMth1($n->getFloatValue()),
            'budgetQtySoldMth10' => fn(ParseNode $n) => $o->setBudgetQtySoldMth10($n->getFloatValue()),
            'budgetQtySoldMth11' => fn(ParseNode $n) => $o->setBudgetQtySoldMth11($n->getFloatValue()),
            'budgetQtySoldMth12' => fn(ParseNode $n) => $o->setBudgetQtySoldMth12($n->getFloatValue()),
            'budgetQtySoldMth2' => fn(ParseNode $n) => $o->setBudgetQtySoldMth2($n->getFloatValue()),
            'budgetQtySoldMth3' => fn(ParseNode $n) => $o->setBudgetQtySoldMth3($n->getFloatValue()),
            'budgetQtySoldMth4' => fn(ParseNode $n) => $o->setBudgetQtySoldMth4($n->getFloatValue()),
            'budgetQtySoldMth5' => fn(ParseNode $n) => $o->setBudgetQtySoldMth5($n->getFloatValue()),
            'budgetQtySoldMth6' => fn(ParseNode $n) => $o->setBudgetQtySoldMth6($n->getFloatValue()),
            'budgetQtySoldMth7' => fn(ParseNode $n) => $o->setBudgetQtySoldMth7($n->getFloatValue()),
            'budgetQtySoldMth8' => fn(ParseNode $n) => $o->setBudgetQtySoldMth8($n->getFloatValue()),
            'budgetQtySoldMth9' => fn(ParseNode $n) => $o->setBudgetQtySoldMth9($n->getFloatValue()),
            'budgetSalesBf' => fn(ParseNode $n) => $o->setBudgetSalesBf($n->getFloatValue()),
            'budgetSalesFuture' => fn(ParseNode $n) => $o->setBudgetSalesFuture($n->getFloatValue()),
            'budgetSalesMth1' => fn(ParseNode $n) => $o->setBudgetSalesMth1($n->getFloatValue()),
            'budgetSalesMth10' => fn(ParseNode $n) => $o->setBudgetSalesMth10($n->getFloatValue()),
            'budgetSalesMth11' => fn(ParseNode $n) => $o->setBudgetSalesMth11($n->getFloatValue()),
            'budgetSalesMth12' => fn(ParseNode $n) => $o->setBudgetSalesMth12($n->getFloatValue()),
            'budgetSalesMth2' => fn(ParseNode $n) => $o->setBudgetSalesMth2($n->getFloatValue()),
            'budgetSalesMth3' => fn(ParseNode $n) => $o->setBudgetSalesMth3($n->getFloatValue()),
            'budgetSalesMth4' => fn(ParseNode $n) => $o->setBudgetSalesMth4($n->getFloatValue()),
            'budgetSalesMth5' => fn(ParseNode $n) => $o->setBudgetSalesMth5($n->getFloatValue()),
            'budgetSalesMth6' => fn(ParseNode $n) => $o->setBudgetSalesMth6($n->getFloatValue()),
            'budgetSalesMth7' => fn(ParseNode $n) => $o->setBudgetSalesMth7($n->getFloatValue()),
            'budgetSalesMth8' => fn(ParseNode $n) => $o->setBudgetSalesMth8($n->getFloatValue()),
            'budgetSalesMth9' => fn(ParseNode $n) => $o->setBudgetSalesMth9($n->getFloatValue()),
            'commodityCode' => fn(ParseNode $n) => $o->setCommodityCode($n->getStringValue()),
            'componentCode1' => fn(ParseNode $n) => $o->setComponentCode1($n->getStringValue()),
            'componentCode10' => fn(ParseNode $n) => $o->setComponentCode10($n->getStringValue()),
            'componentCode11' => fn(ParseNode $n) => $o->setComponentCode11($n->getStringValue()),
            'componentCode12' => fn(ParseNode $n) => $o->setComponentCode12($n->getStringValue()),
            'componentCode13' => fn(ParseNode $n) => $o->setComponentCode13($n->getStringValue()),
            'componentCode14' => fn(ParseNode $n) => $o->setComponentCode14($n->getStringValue()),
            'componentCode15' => fn(ParseNode $n) => $o->setComponentCode15($n->getStringValue()),
            'componentCode16' => fn(ParseNode $n) => $o->setComponentCode16($n->getStringValue()),
            'componentCode17' => fn(ParseNode $n) => $o->setComponentCode17($n->getStringValue()),
            'componentCode18' => fn(ParseNode $n) => $o->setComponentCode18($n->getStringValue()),
            'componentCode19' => fn(ParseNode $n) => $o->setComponentCode19($n->getStringValue()),
            'componentCode2' => fn(ParseNode $n) => $o->setComponentCode2($n->getStringValue()),
            'componentCode20' => fn(ParseNode $n) => $o->setComponentCode20($n->getStringValue()),
            'componentCode21' => fn(ParseNode $n) => $o->setComponentCode21($n->getStringValue()),
            'componentCode22' => fn(ParseNode $n) => $o->setComponentCode22($n->getStringValue()),
            'componentCode23' => fn(ParseNode $n) => $o->setComponentCode23($n->getStringValue()),
            'componentCode24' => fn(ParseNode $n) => $o->setComponentCode24($n->getStringValue()),
            'componentCode25' => fn(ParseNode $n) => $o->setComponentCode25($n->getStringValue()),
            'componentCode26' => fn(ParseNode $n) => $o->setComponentCode26($n->getStringValue()),
            'componentCode27' => fn(ParseNode $n) => $o->setComponentCode27($n->getStringValue()),
            'componentCode28' => fn(ParseNode $n) => $o->setComponentCode28($n->getStringValue()),
            'componentCode29' => fn(ParseNode $n) => $o->setComponentCode29($n->getStringValue()),
            'componentCode3' => fn(ParseNode $n) => $o->setComponentCode3($n->getStringValue()),
            'componentCode30' => fn(ParseNode $n) => $o->setComponentCode30($n->getStringValue()),
            'componentCode31' => fn(ParseNode $n) => $o->setComponentCode31($n->getStringValue()),
            'componentCode32' => fn(ParseNode $n) => $o->setComponentCode32($n->getStringValue()),
            'componentCode33' => fn(ParseNode $n) => $o->setComponentCode33($n->getStringValue()),
            'componentCode34' => fn(ParseNode $n) => $o->setComponentCode34($n->getStringValue()),
            'componentCode35' => fn(ParseNode $n) => $o->setComponentCode35($n->getStringValue()),
            'componentCode36' => fn(ParseNode $n) => $o->setComponentCode36($n->getStringValue()),
            'componentCode37' => fn(ParseNode $n) => $o->setComponentCode37($n->getStringValue()),
            'componentCode38' => fn(ParseNode $n) => $o->setComponentCode38($n->getStringValue()),
            'componentCode39' => fn(ParseNode $n) => $o->setComponentCode39($n->getStringValue()),
            'componentCode4' => fn(ParseNode $n) => $o->setComponentCode4($n->getStringValue()),
            'componentCode40' => fn(ParseNode $n) => $o->setComponentCode40($n->getStringValue()),
            'componentCode41' => fn(ParseNode $n) => $o->setComponentCode41($n->getStringValue()),
            'componentCode42' => fn(ParseNode $n) => $o->setComponentCode42($n->getStringValue()),
            'componentCode43' => fn(ParseNode $n) => $o->setComponentCode43($n->getStringValue()),
            'componentCode44' => fn(ParseNode $n) => $o->setComponentCode44($n->getStringValue()),
            'componentCode45' => fn(ParseNode $n) => $o->setComponentCode45($n->getStringValue()),
            'componentCode46' => fn(ParseNode $n) => $o->setComponentCode46($n->getStringValue()),
            'componentCode47' => fn(ParseNode $n) => $o->setComponentCode47($n->getStringValue()),
            'componentCode48' => fn(ParseNode $n) => $o->setComponentCode48($n->getStringValue()),
            'componentCode49' => fn(ParseNode $n) => $o->setComponentCode49($n->getStringValue()),
            'componentCode5' => fn(ParseNode $n) => $o->setComponentCode5($n->getStringValue()),
            'componentCode50' => fn(ParseNode $n) => $o->setComponentCode50($n->getStringValue()),
            'componentCode6' => fn(ParseNode $n) => $o->setComponentCode6($n->getStringValue()),
            'componentCode7' => fn(ParseNode $n) => $o->setComponentCode7($n->getStringValue()),
            'componentCode8' => fn(ParseNode $n) => $o->setComponentCode8($n->getStringValue()),
            'componentCode9' => fn(ParseNode $n) => $o->setComponentCode9($n->getStringValue()),
            'componentQty1' => fn(ParseNode $n) => $o->setComponentQty1($n->getFloatValue()),
            'componentQty10' => fn(ParseNode $n) => $o->setComponentQty10($n->getFloatValue()),
            'componentQty11' => fn(ParseNode $n) => $o->setComponentQty11($n->getFloatValue()),
            'componentQty12' => fn(ParseNode $n) => $o->setComponentQty12($n->getFloatValue()),
            'componentQty13' => fn(ParseNode $n) => $o->setComponentQty13($n->getFloatValue()),
            'componentQty14' => fn(ParseNode $n) => $o->setComponentQty14($n->getFloatValue()),
            'componentQty15' => fn(ParseNode $n) => $o->setComponentQty15($n->getFloatValue()),
            'componentQty16' => fn(ParseNode $n) => $o->setComponentQty16($n->getFloatValue()),
            'componentQty17' => fn(ParseNode $n) => $o->setComponentQty17($n->getFloatValue()),
            'componentQty18' => fn(ParseNode $n) => $o->setComponentQty18($n->getFloatValue()),
            'componentQty19' => fn(ParseNode $n) => $o->setComponentQty19($n->getFloatValue()),
            'componentQty2' => fn(ParseNode $n) => $o->setComponentQty2($n->getFloatValue()),
            'componentQty20' => fn(ParseNode $n) => $o->setComponentQty20($n->getFloatValue()),
            'componentQty21' => fn(ParseNode $n) => $o->setComponentQty21($n->getFloatValue()),
            'componentQty22' => fn(ParseNode $n) => $o->setComponentQty22($n->getFloatValue()),
            'componentQty23' => fn(ParseNode $n) => $o->setComponentQty23($n->getFloatValue()),
            'componentQty24' => fn(ParseNode $n) => $o->setComponentQty24($n->getFloatValue()),
            'componentQty25' => fn(ParseNode $n) => $o->setComponentQty25($n->getFloatValue()),
            'componentQty26' => fn(ParseNode $n) => $o->setComponentQty26($n->getFloatValue()),
            'componentQty27' => fn(ParseNode $n) => $o->setComponentQty27($n->getFloatValue()),
            'componentQty28' => fn(ParseNode $n) => $o->setComponentQty28($n->getFloatValue()),
            'componentQty29' => fn(ParseNode $n) => $o->setComponentQty29($n->getFloatValue()),
            'componentQty3' => fn(ParseNode $n) => $o->setComponentQty3($n->getFloatValue()),
            'componentQty30' => fn(ParseNode $n) => $o->setComponentQty30($n->getFloatValue()),
            'componentQty31' => fn(ParseNode $n) => $o->setComponentQty31($n->getFloatValue()),
            'componentQty32' => fn(ParseNode $n) => $o->setComponentQty32($n->getFloatValue()),
            'componentQty33' => fn(ParseNode $n) => $o->setComponentQty33($n->getFloatValue()),
            'componentQty34' => fn(ParseNode $n) => $o->setComponentQty34($n->getFloatValue()),
            'componentQty35' => fn(ParseNode $n) => $o->setComponentQty35($n->getFloatValue()),
            'componentQty36' => fn(ParseNode $n) => $o->setComponentQty36($n->getFloatValue()),
            'componentQty37' => fn(ParseNode $n) => $o->setComponentQty37($n->getFloatValue()),
            'componentQty38' => fn(ParseNode $n) => $o->setComponentQty38($n->getFloatValue()),
            'componentQty39' => fn(ParseNode $n) => $o->setComponentQty39($n->getFloatValue()),
            'componentQty4' => fn(ParseNode $n) => $o->setComponentQty4($n->getFloatValue()),
            'componentQty40' => fn(ParseNode $n) => $o->setComponentQty40($n->getFloatValue()),
            'componentQty41' => fn(ParseNode $n) => $o->setComponentQty41($n->getFloatValue()),
            'componentQty42' => fn(ParseNode $n) => $o->setComponentQty42($n->getFloatValue()),
            'componentQty43' => fn(ParseNode $n) => $o->setComponentQty43($n->getFloatValue()),
            'componentQty44' => fn(ParseNode $n) => $o->setComponentQty44($n->getFloatValue()),
            'componentQty45' => fn(ParseNode $n) => $o->setComponentQty45($n->getFloatValue()),
            'componentQty46' => fn(ParseNode $n) => $o->setComponentQty46($n->getFloatValue()),
            'componentQty47' => fn(ParseNode $n) => $o->setComponentQty47($n->getFloatValue()),
            'componentQty48' => fn(ParseNode $n) => $o->setComponentQty48($n->getFloatValue()),
            'componentQty49' => fn(ParseNode $n) => $o->setComponentQty49($n->getFloatValue()),
            'componentQty5' => fn(ParseNode $n) => $o->setComponentQty5($n->getFloatValue()),
            'componentQty50' => fn(ParseNode $n) => $o->setComponentQty50($n->getFloatValue()),
            'componentQty6' => fn(ParseNode $n) => $o->setComponentQty6($n->getFloatValue()),
            'componentQty7' => fn(ParseNode $n) => $o->setComponentQty7($n->getFloatValue()),
            'componentQty8' => fn(ParseNode $n) => $o->setComponentQty8($n->getFloatValue()),
            'componentQty9' => fn(ParseNode $n) => $o->setComponentQty9($n->getFloatValue()),
            'costBf' => fn(ParseNode $n) => $o->setCostBf($n->getFloatValue()),
            'costFuture' => fn(ParseNode $n) => $o->setCostFuture($n->getFloatValue()),
            'costMth1' => fn(ParseNode $n) => $o->setCostMth1($n->getFloatValue()),
            'costMth10' => fn(ParseNode $n) => $o->setCostMth10($n->getFloatValue()),
            'costMth11' => fn(ParseNode $n) => $o->setCostMth11($n->getFloatValue()),
            'costMth12' => fn(ParseNode $n) => $o->setCostMth12($n->getFloatValue()),
            'costMth2' => fn(ParseNode $n) => $o->setCostMth2($n->getFloatValue()),
            'costMth3' => fn(ParseNode $n) => $o->setCostMth3($n->getFloatValue()),
            'costMth4' => fn(ParseNode $n) => $o->setCostMth4($n->getFloatValue()),
            'costMth5' => fn(ParseNode $n) => $o->setCostMth5($n->getFloatValue()),
            'costMth6' => fn(ParseNode $n) => $o->setCostMth6($n->getFloatValue()),
            'costMth7' => fn(ParseNode $n) => $o->setCostMth7($n->getFloatValue()),
            'costMth8' => fn(ParseNode $n) => $o->setCostMth8($n->getFloatValue()),
            'costMth9' => fn(ParseNode $n) => $o->setCostMth9($n->getFloatValue()),
            'countryCodeOfOrigin' => fn(ParseNode $n) => $o->setCountryCodeOfOrigin($n->getStringValue()),
            'deptName' => fn(ParseNode $n) => $o->setDeptName($n->getStringValue()),
            'deptNumber' => fn(ParseNode $n) => $o->setDeptNumber($n->getIntegerValue()),
            'description' => fn(ParseNode $n) => $o->setDescription($n->getStringValue()),
            'discALevel10Qty' => fn(ParseNode $n) => $o->setDiscALevel10Qty($n->getFloatValue()),
            'discALevel10Rate' => fn(ParseNode $n) => $o->setDiscALevel10Rate($n->getFloatValue()),
            'discALevel1Qty' => fn(ParseNode $n) => $o->setDiscALevel1Qty($n->getFloatValue()),
            'discALevel1Rate' => fn(ParseNode $n) => $o->setDiscALevel1Rate($n->getFloatValue()),
            'discALevel2Qty' => fn(ParseNode $n) => $o->setDiscALevel2Qty($n->getFloatValue()),
            'discALevel2Rate' => fn(ParseNode $n) => $o->setDiscALevel2Rate($n->getFloatValue()),
            'discALevel3Qty' => fn(ParseNode $n) => $o->setDiscALevel3Qty($n->getFloatValue()),
            'discALevel3Rate' => fn(ParseNode $n) => $o->setDiscALevel3Rate($n->getFloatValue()),
            'discALevel4Qty' => fn(ParseNode $n) => $o->setDiscALevel4Qty($n->getFloatValue()),
            'discALevel4Rate' => fn(ParseNode $n) => $o->setDiscALevel4Rate($n->getFloatValue()),
            'discALevel5Qty' => fn(ParseNode $n) => $o->setDiscALevel5Qty($n->getFloatValue()),
            'discALevel5Rate' => fn(ParseNode $n) => $o->setDiscALevel5Rate($n->getFloatValue()),
            'discALevel6Qty' => fn(ParseNode $n) => $o->setDiscALevel6Qty($n->getFloatValue()),
            'discALevel6Rate' => fn(ParseNode $n) => $o->setDiscALevel6Rate($n->getFloatValue()),
            'discALevel7Qty' => fn(ParseNode $n) => $o->setDiscALevel7Qty($n->getFloatValue()),
            'discALevel7Rate' => fn(ParseNode $n) => $o->setDiscALevel7Rate($n->getFloatValue()),
            'discALevel8Qty' => fn(ParseNode $n) => $o->setDiscALevel8Qty($n->getFloatValue()),
            'discALevel8Rate' => fn(ParseNode $n) => $o->setDiscALevel8Rate($n->getFloatValue()),
            'discALevel9Qty' => fn(ParseNode $n) => $o->setDiscALevel9Qty($n->getFloatValue()),
            'discALevel9Rate' => fn(ParseNode $n) => $o->setDiscALevel9Rate($n->getFloatValue()),
            'discBLevel10Qty' => fn(ParseNode $n) => $o->setDiscBLevel10Qty($n->getFloatValue()),
            'discBLevel10Rate' => fn(ParseNode $n) => $o->setDiscBLevel10Rate($n->getFloatValue()),
            'discBLevel1Qty' => fn(ParseNode $n) => $o->setDiscBLevel1Qty($n->getFloatValue()),
            'discBLevel1Rate' => fn(ParseNode $n) => $o->setDiscBLevel1Rate($n->getFloatValue()),
            'discBLevel2Qty' => fn(ParseNode $n) => $o->setDiscBLevel2Qty($n->getFloatValue()),
            'discBLevel2Rate' => fn(ParseNode $n) => $o->setDiscBLevel2Rate($n->getFloatValue()),
            'discBLevel3Qty' => fn(ParseNode $n) => $o->setDiscBLevel3Qty($n->getFloatValue()),
            'discBLevel3Rate' => fn(ParseNode $n) => $o->setDiscBLevel3Rate($n->getFloatValue()),
            'discBLevel4Qty' => fn(ParseNode $n) => $o->setDiscBLevel4Qty($n->getFloatValue()),
            'discBLevel4Rate' => fn(ParseNode $n) => $o->setDiscBLevel4Rate($n->getFloatValue()),
            'discBLevel5Qty' => fn(ParseNode $n) => $o->setDiscBLevel5Qty($n->getFloatValue()),
            'discBLevel5Rate' => fn(ParseNode $n) => $o->setDiscBLevel5Rate($n->getFloatValue()),
            'discBLevel6Qty' => fn(ParseNode $n) => $o->setDiscBLevel6Qty($n->getFloatValue()),
            'discBLevel6Rate' => fn(ParseNode $n) => $o->setDiscBLevel6Rate($n->getFloatValue()),
            'discBLevel7Qty' => fn(ParseNode $n) => $o->setDiscBLevel7Qty($n->getFloatValue()),
            'discBLevel7Rate' => fn(ParseNode $n) => $o->setDiscBLevel7Rate($n->getFloatValue()),
            'discBLevel8Qty' => fn(ParseNode $n) => $o->setDiscBLevel8Qty($n->getFloatValue()),
            'discBLevel8Rate' => fn(ParseNode $n) => $o->setDiscBLevel8Rate($n->getFloatValue()),
            'discBLevel9Qty' => fn(ParseNode $n) => $o->setDiscBLevel9Qty($n->getFloatValue()),
            'discBLevel9Rate' => fn(ParseNode $n) => $o->setDiscBLevel9Rate($n->getFloatValue()),
            'discCLevel10Qty' => fn(ParseNode $n) => $o->setDiscCLevel10Qty($n->getFloatValue()),
            'discCLevel10Rate' => fn(ParseNode $n) => $o->setDiscCLevel10Rate($n->getFloatValue()),
            'discCLevel1Qty' => fn(ParseNode $n) => $o->setDiscCLevel1Qty($n->getFloatValue()),
            'discCLevel1Rate' => fn(ParseNode $n) => $o->setDiscCLevel1Rate($n->getFloatValue()),
            'discCLevel2Qty' => fn(ParseNode $n) => $o->setDiscCLevel2Qty($n->getFloatValue()),
            'discCLevel2Rate' => fn(ParseNode $n) => $o->setDiscCLevel2Rate($n->getFloatValue()),
            'discCLevel3Qty' => fn(ParseNode $n) => $o->setDiscCLevel3Qty($n->getFloatValue()),
            'discCLevel3Rate' => fn(ParseNode $n) => $o->setDiscCLevel3Rate($n->getFloatValue()),
            'discCLevel4Qty' => fn(ParseNode $n) => $o->setDiscCLevel4Qty($n->getFloatValue()),
            'discCLevel4Rate' => fn(ParseNode $n) => $o->setDiscCLevel4Rate($n->getFloatValue()),
            'discCLevel5Qty' => fn(ParseNode $n) => $o->setDiscCLevel5Qty($n->getFloatValue()),
            'discCLevel5Rate' => fn(ParseNode $n) => $o->setDiscCLevel5Rate($n->getFloatValue()),
            'discCLevel6Qty' => fn(ParseNode $n) => $o->setDiscCLevel6Qty($n->getFloatValue()),
            'discCLevel6Rate' => fn(ParseNode $n) => $o->setDiscCLevel6Rate($n->getFloatValue()),
            'discCLevel7Qty' => fn(ParseNode $n) => $o->setDiscCLevel7Qty($n->getFloatValue()),
            'discCLevel7Rate' => fn(ParseNode $n) => $o->setDiscCLevel7Rate($n->getFloatValue()),
            'discCLevel8Qty' => fn(ParseNode $n) => $o->setDiscCLevel8Qty($n->getFloatValue()),
            'discCLevel8Rate' => fn(ParseNode $n) => $o->setDiscCLevel8Rate($n->getFloatValue()),
            'discCLevel9Qty' => fn(ParseNode $n) => $o->setDiscCLevel9Qty($n->getFloatValue()),
            'discCLevel9Rate' => fn(ParseNode $n) => $o->setDiscCLevel9Rate($n->getFloatValue()),
            'discDLevel10Qty' => fn(ParseNode $n) => $o->setDiscDLevel10Qty($n->getFloatValue()),
            'discDLevel10Rate' => fn(ParseNode $n) => $o->setDiscDLevel10Rate($n->getFloatValue()),
            'discDLevel1Qty' => fn(ParseNode $n) => $o->setDiscDLevel1Qty($n->getFloatValue()),
            'discDLevel1Rate' => fn(ParseNode $n) => $o->setDiscDLevel1Rate($n->getFloatValue()),
            'discDLevel2Qty' => fn(ParseNode $n) => $o->setDiscDLevel2Qty($n->getFloatValue()),
            'discDLevel2Rate' => fn(ParseNode $n) => $o->setDiscDLevel2Rate($n->getFloatValue()),
            'discDLevel3Qty' => fn(ParseNode $n) => $o->setDiscDLevel3Qty($n->getFloatValue()),
            'discDLevel3Rate' => fn(ParseNode $n) => $o->setDiscDLevel3Rate($n->getFloatValue()),
            'discDLevel4Qty' => fn(ParseNode $n) => $o->setDiscDLevel4Qty($n->getFloatValue()),
            'discDLevel4Rate' => fn(ParseNode $n) => $o->setDiscDLevel4Rate($n->getFloatValue()),
            'discDLevel5Qty' => fn(ParseNode $n) => $o->setDiscDLevel5Qty($n->getFloatValue()),
            'discDLevel5Rate' => fn(ParseNode $n) => $o->setDiscDLevel5Rate($n->getFloatValue()),
            'discDLevel6Qty' => fn(ParseNode $n) => $o->setDiscDLevel6Qty($n->getFloatValue()),
            'discDLevel6Rate' => fn(ParseNode $n) => $o->setDiscDLevel6Rate($n->getFloatValue()),
            'discDLevel7Qty' => fn(ParseNode $n) => $o->setDiscDLevel7Qty($n->getFloatValue()),
            'discDLevel7Rate' => fn(ParseNode $n) => $o->setDiscDLevel7Rate($n->getFloatValue()),
            'discDLevel8Qty' => fn(ParseNode $n) => $o->setDiscDLevel8Qty($n->getFloatValue()),
            'discDLevel8Rate' => fn(ParseNode $n) => $o->setDiscDLevel8Rate($n->getFloatValue()),
            'discDLevel9Qty' => fn(ParseNode $n) => $o->setDiscDLevel9Qty($n->getFloatValue()),
            'discDLevel9Rate' => fn(ParseNode $n) => $o->setDiscDLevel9Rate($n->getFloatValue()),
            'discELevel10Qty' => fn(ParseNode $n) => $o->setDiscELevel10Qty($n->getFloatValue()),
            'discELevel10Rate' => fn(ParseNode $n) => $o->setDiscELevel10Rate($n->getFloatValue()),
            'discELevel1Qty' => fn(ParseNode $n) => $o->setDiscELevel1Qty($n->getFloatValue()),
            'discELevel1Rate' => fn(ParseNode $n) => $o->setDiscELevel1Rate($n->getFloatValue()),
            'discELevel2Qty' => fn(ParseNode $n) => $o->setDiscELevel2Qty($n->getFloatValue()),
            'discELevel2Rate' => fn(ParseNode $n) => $o->setDiscELevel2Rate($n->getFloatValue()),
            'discELevel3Qty' => fn(ParseNode $n) => $o->setDiscELevel3Qty($n->getFloatValue()),
            'discELevel3Rate' => fn(ParseNode $n) => $o->setDiscELevel3Rate($n->getFloatValue()),
            'discELevel4Qty' => fn(ParseNode $n) => $o->setDiscELevel4Qty($n->getFloatValue()),
            'discELevel4Rate' => fn(ParseNode $n) => $o->setDiscELevel4Rate($n->getFloatValue()),
            'discELevel5Qty' => fn(ParseNode $n) => $o->setDiscELevel5Qty($n->getFloatValue()),
            'discELevel5Rate' => fn(ParseNode $n) => $o->setDiscELevel5Rate($n->getFloatValue()),
            'discELevel6Qty' => fn(ParseNode $n) => $o->setDiscELevel6Qty($n->getFloatValue()),
            'discELevel6Rate' => fn(ParseNode $n) => $o->setDiscELevel6Rate($n->getFloatValue()),
            'discELevel7Qty' => fn(ParseNode $n) => $o->setDiscELevel7Qty($n->getFloatValue()),
            'discELevel7Rate' => fn(ParseNode $n) => $o->setDiscELevel7Rate($n->getFloatValue()),
            'discELevel8Qty' => fn(ParseNode $n) => $o->setDiscELevel8Qty($n->getFloatValue()),
            'discELevel8Rate' => fn(ParseNode $n) => $o->setDiscELevel8Rate($n->getFloatValue()),
            'discELevel9Qty' => fn(ParseNode $n) => $o->setDiscELevel9Qty($n->getFloatValue()),
            'discELevel9Rate' => fn(ParseNode $n) => $o->setDiscELevel9Rate($n->getFloatValue()),
            'hasBom' => fn(ParseNode $n) => $o->setHasBom($n->getStringValue()),
            'hasNoComponent' => fn(ParseNode $n) => $o->setHasNoComponent($n->getIntegerValue()),
            'ignoreStkLvlFlag' => fn(ParseNode $n) => $o->setIgnoreStkLvlFlag($n->getIntegerValue()),
            'inactiveFlag' => fn(ParseNode $n) => $o->setInactiveFlag($n->getIntegerValue()),
            'intrastatCommCode' => fn(ParseNode $n) => $o->setIntrastatCommCode($n->getStringValue()),
            'intrastatImportDutyCode' => fn(ParseNode $n) => $o->setIntrastatImportDutyCode($n->getStringValue()),
            'lastDiscPurchasePrice' => fn(ParseNode $n) => $o->setLastDiscPurchasePrice($n->getFloatValue()),
            'lastPurchaseDate' => fn(ParseNode $n) => $o->setLastPurchaseDate($n->getDateTimeValue()),
            'lastPurchasePrice' => fn(ParseNode $n) => $o->setLastPurchasePrice($n->getFloatValue()),
            'lastSaleDate' => fn(ParseNode $n) => $o->setLastSaleDate($n->getDateTimeValue()),
            'linkLevel' => fn(ParseNode $n) => $o->setLinkLevel($n->getIntegerValue()),
            'location' => fn(ParseNode $n) => $o->setLocation($n->getStringValue()),
            'priorYrCostBf' => fn(ParseNode $n) => $o->setPriorYrCostBf($n->getFloatValue()),
            'priorYrCostFuture' => fn(ParseNode $n) => $o->setPriorYrCostFuture($n->getFloatValue()),
            'priorYrCostMth1' => fn(ParseNode $n) => $o->setPriorYrCostMth1($n->getFloatValue()),
            'priorYrCostMth10' => fn(ParseNode $n) => $o->setPriorYrCostMth10($n->getFloatValue()),
            'priorYrCostMth11' => fn(ParseNode $n) => $o->setPriorYrCostMth11($n->getFloatValue()),
            'priorYrCostMth12' => fn(ParseNode $n) => $o->setPriorYrCostMth12($n->getFloatValue()),
            'priorYrCostMth2' => fn(ParseNode $n) => $o->setPriorYrCostMth2($n->getFloatValue()),
            'priorYrCostMth3' => fn(ParseNode $n) => $o->setPriorYrCostMth3($n->getFloatValue()),
            'priorYrCostMth4' => fn(ParseNode $n) => $o->setPriorYrCostMth4($n->getFloatValue()),
            'priorYrCostMth5' => fn(ParseNode $n) => $o->setPriorYrCostMth5($n->getFloatValue()),
            'priorYrCostMth6' => fn(ParseNode $n) => $o->setPriorYrCostMth6($n->getFloatValue()),
            'priorYrCostMth7' => fn(ParseNode $n) => $o->setPriorYrCostMth7($n->getFloatValue()),
            'priorYrCostMth8' => fn(ParseNode $n) => $o->setPriorYrCostMth8($n->getFloatValue()),
            'priorYrCostMth9' => fn(ParseNode $n) => $o->setPriorYrCostMth9($n->getFloatValue()),
            'priorYrQtySoldBf' => fn(ParseNode $n) => $o->setPriorYrQtySoldBf($n->getFloatValue()),
            'priorYrQtySoldFuture' => fn(ParseNode $n) => $o->setPriorYrQtySoldFuture($n->getFloatValue()),
            'priorYrQtySoldMth1' => fn(ParseNode $n) => $o->setPriorYrQtySoldMth1($n->getFloatValue()),
            'priorYrQtySoldMth10' => fn(ParseNode $n) => $o->setPriorYrQtySoldMth10($n->getFloatValue()),
            'priorYrQtySoldMth11' => fn(ParseNode $n) => $o->setPriorYrQtySoldMth11($n->getFloatValue()),
            'priorYrQtySoldMth12' => fn(ParseNode $n) => $o->setPriorYrQtySoldMth12($n->getFloatValue()),
            'priorYrQtySoldMth2' => fn(ParseNode $n) => $o->setPriorYrQtySoldMth2($n->getFloatValue()),
            'priorYrQtySoldMth3' => fn(ParseNode $n) => $o->setPriorYrQtySoldMth3($n->getFloatValue()),
            'priorYrQtySoldMth4' => fn(ParseNode $n) => $o->setPriorYrQtySoldMth4($n->getFloatValue()),
            'priorYrQtySoldMth5' => fn(ParseNode $n) => $o->setPriorYrQtySoldMth5($n->getFloatValue()),
            'priorYrQtySoldMth6' => fn(ParseNode $n) => $o->setPriorYrQtySoldMth6($n->getFloatValue()),
            'priorYrQtySoldMth7' => fn(ParseNode $n) => $o->setPriorYrQtySoldMth7($n->getFloatValue()),
            'priorYrQtySoldMth8' => fn(ParseNode $n) => $o->setPriorYrQtySoldMth8($n->getFloatValue()),
            'priorYrQtySoldMth9' => fn(ParseNode $n) => $o->setPriorYrQtySoldMth9($n->getFloatValue()),
            'priorYrSalesBf' => fn(ParseNode $n) => $o->setPriorYrSalesBf($n->getFloatValue()),
            'priorYrSalesFuture' => fn(ParseNode $n) => $o->setPriorYrSalesFuture($n->getFloatValue()),
            'priorYrSalesMth1' => fn(ParseNode $n) => $o->setPriorYrSalesMth1($n->getFloatValue()),
            'priorYrSalesMth10' => fn(ParseNode $n) => $o->setPriorYrSalesMth10($n->getFloatValue()),
            'priorYrSalesMth11' => fn(ParseNode $n) => $o->setPriorYrSalesMth11($n->getFloatValue()),
            'priorYrSalesMth12' => fn(ParseNode $n) => $o->setPriorYrSalesMth12($n->getFloatValue()),
            'priorYrSalesMth2' => fn(ParseNode $n) => $o->setPriorYrSalesMth2($n->getFloatValue()),
            'priorYrSalesMth3' => fn(ParseNode $n) => $o->setPriorYrSalesMth3($n->getFloatValue()),
            'priorYrSalesMth4' => fn(ParseNode $n) => $o->setPriorYrSalesMth4($n->getFloatValue()),
            'priorYrSalesMth5' => fn(ParseNode $n) => $o->setPriorYrSalesMth5($n->getFloatValue()),
            'priorYrSalesMth6' => fn(ParseNode $n) => $o->setPriorYrSalesMth6($n->getFloatValue()),
            'priorYrSalesMth7' => fn(ParseNode $n) => $o->setPriorYrSalesMth7($n->getFloatValue()),
            'priorYrSalesMth8' => fn(ParseNode $n) => $o->setPriorYrSalesMth8($n->getFloatValue()),
            'priorYrSalesMth9' => fn(ParseNode $n) => $o->setPriorYrSalesMth9($n->getFloatValue()),
            'purchaseNominalCode' => fn(ParseNode $n) => $o->setPurchaseNominalCode($n->getStringValue()),
            'purchaseRef' => fn(ParseNode $n) => $o->setPurchaseRef($n->getStringValue()),
            'qtyAllocated' => fn(ParseNode $n) => $o->setQtyAllocated($n->getFloatValue()),
            'qtyInStock' => fn(ParseNode $n) => $o->setQtyInStock($n->getFloatValue()),
            'qtyLastOrder' => fn(ParseNode $n) => $o->setQtyLastOrder($n->getFloatValue()),
            'qtyLastStockTake' => fn(ParseNode $n) => $o->setQtyLastStockTake($n->getFloatValue()),
            'qtyMakeup' => fn(ParseNode $n) => $o->setQtyMakeup($n->getFloatValue()),
            'qtyOnOrder' => fn(ParseNode $n) => $o->setQtyOnOrder($n->getFloatValue()),
            'qtyReorder' => fn(ParseNode $n) => $o->setQtyReorder($n->getFloatValue()),
            'qtyReorderLevel' => fn(ParseNode $n) => $o->setQtyReorderLevel($n->getFloatValue()),
            'qtySoldBf' => fn(ParseNode $n) => $o->setQtySoldBf($n->getFloatValue()),
            'qtySoldFuture' => fn(ParseNode $n) => $o->setQtySoldFuture($n->getFloatValue()),
            'qtySoldMth1' => fn(ParseNode $n) => $o->setQtySoldMth1($n->getFloatValue()),
            'qtySoldMth10' => fn(ParseNode $n) => $o->setQtySoldMth10($n->getFloatValue()),
            'qtySoldMth11' => fn(ParseNode $n) => $o->setQtySoldMth11($n->getFloatValue()),
            'qtySoldMth12' => fn(ParseNode $n) => $o->setQtySoldMth12($n->getFloatValue()),
            'qtySoldMth2' => fn(ParseNode $n) => $o->setQtySoldMth2($n->getFloatValue()),
            'qtySoldMth3' => fn(ParseNode $n) => $o->setQtySoldMth3($n->getFloatValue()),
            'qtySoldMth4' => fn(ParseNode $n) => $o->setQtySoldMth4($n->getFloatValue()),
            'qtySoldMth5' => fn(ParseNode $n) => $o->setQtySoldMth5($n->getFloatValue()),
            'qtySoldMth6' => fn(ParseNode $n) => $o->setQtySoldMth6($n->getFloatValue()),
            'qtySoldMth7' => fn(ParseNode $n) => $o->setQtySoldMth7($n->getFloatValue()),
            'qtySoldMth8' => fn(ParseNode $n) => $o->setQtySoldMth8($n->getFloatValue()),
            'qtySoldMth9' => fn(ParseNode $n) => $o->setQtySoldMth9($n->getFloatValue()),
            'recordCreateDate' => fn(ParseNode $n) => $o->setRecordCreateDate($n->getDateTimeValue()),
            'recordDeleted' => fn(ParseNode $n) => $o->setRecordDeleted($n->getIntegerValue()),
            'recordModifyDate' => fn(ParseNode $n) => $o->setRecordModifyDate($n->getDateTimeValue()),
            'salesBf' => fn(ParseNode $n) => $o->setSalesBf($n->getFloatValue()),
            'salesFuture' => fn(ParseNode $n) => $o->setSalesFuture($n->getFloatValue()),
            'salesMth1' => fn(ParseNode $n) => $o->setSalesMth1($n->getFloatValue()),
            'salesMth10' => fn(ParseNode $n) => $o->setSalesMth10($n->getFloatValue()),
            'salesMth11' => fn(ParseNode $n) => $o->setSalesMth11($n->getFloatValue()),
            'salesMth12' => fn(ParseNode $n) => $o->setSalesMth12($n->getFloatValue()),
            'salesMth2' => fn(ParseNode $n) => $o->setSalesMth2($n->getFloatValue()),
            'salesMth3' => fn(ParseNode $n) => $o->setSalesMth3($n->getFloatValue()),
            'salesMth4' => fn(ParseNode $n) => $o->setSalesMth4($n->getFloatValue()),
            'salesMth5' => fn(ParseNode $n) => $o->setSalesMth5($n->getFloatValue()),
            'salesMth6' => fn(ParseNode $n) => $o->setSalesMth6($n->getFloatValue()),
            'salesMth7' => fn(ParseNode $n) => $o->setSalesMth7($n->getFloatValue()),
            'salesMth8' => fn(ParseNode $n) => $o->setSalesMth8($n->getFloatValue()),
            'salesMth9' => fn(ParseNode $n) => $o->setSalesMth9($n->getFloatValue()),
            'salesPrice' => fn(ParseNode $n) => $o->setSalesPrice($n->getFloatValue()),
            'stockCat' => fn(ParseNode $n) => $o->setStockCat($n->getIntegerValue()),
            'stockCatName' => fn(ParseNode $n) => $o->setStockCatName($n->getStringValue()),
            'stockTakeDate' => fn(ParseNode $n) => $o->setStockTakeDate($n->getDateTimeValue()),
            'supplierPartNumber' => fn(ParseNode $n) => $o->setSupplierPartNumber($n->getStringValue()),
            'suppUnitQty' => fn(ParseNode $n) => $o->setSuppUnitQty($n->getFloatValue()),
            'taxCode' => fn(ParseNode $n) => $o->setTaxCode($n->getEnumValue(StockAttributesRead_taxCode::class)),
            'thisRecord' => fn(ParseNode $n) => $o->setThisRecord($n->getIntegerValue()),
            'unitOfSale' => fn(ParseNode $n) => $o->setUnitOfSale($n->getStringValue()),
            'unitWeight' => fn(ParseNode $n) => $o->setUnitWeight($n->getFloatValue()),
            'webCategory1' => fn(ParseNode $n) => $o->setWebCategory1($n->getStringValue()),
            'webCategory2' => fn(ParseNode $n) => $o->setWebCategory2($n->getStringValue()),
            'webCategory3' => fn(ParseNode $n) => $o->setWebCategory3($n->getStringValue()),
            'webDescription' => fn(ParseNode $n) => $o->setWebDescription($n->getStringValue()),
            'webDetails' => fn(ParseNode $n) => $o->setWebDetails($n->getStringValue()),
            'webImageFile' => fn(ParseNode $n) => $o->setWebImageFile($n->getStringValue()),
            'webPublish' => fn(ParseNode $n) => $o->setWebPublish($n->getIntegerValue()),
            'webSpecial' => fn(ParseNode $n) => $o->setWebSpecial($n->getIntegerValue()),
        ];
    }

    /**
     * Gets the hasBom property value. The hasBom property
     * @return string|null
    */
    public function getHasBom(): ?string {
        return $this->hasBom;
    }

    /**
     * Gets the hasNoComponent property value. The hasNoComponent property
     * @return int|null
    */
    public function getHasNoComponent(): ?int {
        return $this->hasNoComponent;
    }

    /**
     * Gets the ignoreStkLvlFlag property value. The ignoreStkLvlFlag property
     * @return int|null
    */
    public function getIgnoreStkLvlFlag(): ?int {
        return $this->ignoreStkLvlFlag;
    }

    /**
     * Gets the inactiveFlag property value. The inactiveFlag property
     * @return int|null
    */
    public function getInactiveFlag(): ?int {
        return $this->inactiveFlag;
    }

    /**
     * Gets the intrastatCommCode property value. The intrastatCommCode property
     * @return string|null
    */
    public function getIntrastatCommCode(): ?string {
        return $this->intrastatCommCode;
    }

    /**
     * Gets the intrastatImportDutyCode property value. The intrastatImportDutyCode property
     * @return string|null
    */
    public function getIntrastatImportDutyCode(): ?string {
        return $this->intrastatImportDutyCode;
    }

    /**
     * Gets the lastDiscPurchasePrice property value. The lastDiscPurchasePrice property
     * @return float|null
    */
    public function getLastDiscPurchasePrice(): ?float {
        return $this->lastDiscPurchasePrice;
    }

    /**
     * Gets the lastPurchaseDate property value. The lastPurchaseDate property
     * @return DateTime|null
    */
    public function getLastPurchaseDate(): ?DateTime {
        return $this->lastPurchaseDate;
    }

    /**
     * Gets the lastPurchasePrice property value. The lastPurchasePrice property
     * @return float|null
    */
    public function getLastPurchasePrice(): ?float {
        return $this->lastPurchasePrice;
    }

    /**
     * Gets the lastSaleDate property value. The lastSaleDate property
     * @return DateTime|null
    */
    public function getLastSaleDate(): ?DateTime {
        return $this->lastSaleDate;
    }

    /**
     * Gets the linkLevel property value. The linkLevel property
     * @return int|null
    */
    public function getLinkLevel(): ?int {
        return $this->linkLevel;
    }

    /**
     * Gets the location property value. The location property
     * @return string|null
    */
    public function getLocation(): ?string {
        return $this->location;
    }

    /**
     * Gets the priorYrCostBf property value. The priorYrCostBf property
     * @return float|null
    */
    public function getPriorYrCostBf(): ?float {
        return $this->priorYrCostBf;
    }

    /**
     * Gets the priorYrCostFuture property value. The priorYrCostFuture property
     * @return float|null
    */
    public function getPriorYrCostFuture(): ?float {
        return $this->priorYrCostFuture;
    }

    /**
     * Gets the priorYrCostMth1 property value. The priorYrCostMth1 property
     * @return float|null
    */
    public function getPriorYrCostMth1(): ?float {
        return $this->priorYrCostMth1;
    }

    /**
     * Gets the priorYrCostMth10 property value. The priorYrCostMth10 property
     * @return float|null
    */
    public function getPriorYrCostMth10(): ?float {
        return $this->priorYrCostMth10;
    }

    /**
     * Gets the priorYrCostMth11 property value. The priorYrCostMth11 property
     * @return float|null
    */
    public function getPriorYrCostMth11(): ?float {
        return $this->priorYrCostMth11;
    }

    /**
     * Gets the priorYrCostMth12 property value. The priorYrCostMth12 property
     * @return float|null
    */
    public function getPriorYrCostMth12(): ?float {
        return $this->priorYrCostMth12;
    }

    /**
     * Gets the priorYrCostMth2 property value. The priorYrCostMth2 property
     * @return float|null
    */
    public function getPriorYrCostMth2(): ?float {
        return $this->priorYrCostMth2;
    }

    /**
     * Gets the priorYrCostMth3 property value. The priorYrCostMth3 property
     * @return float|null
    */
    public function getPriorYrCostMth3(): ?float {
        return $this->priorYrCostMth3;
    }

    /**
     * Gets the priorYrCostMth4 property value. The priorYrCostMth4 property
     * @return float|null
    */
    public function getPriorYrCostMth4(): ?float {
        return $this->priorYrCostMth4;
    }

    /**
     * Gets the priorYrCostMth5 property value. The priorYrCostMth5 property
     * @return float|null
    */
    public function getPriorYrCostMth5(): ?float {
        return $this->priorYrCostMth5;
    }

    /**
     * Gets the priorYrCostMth6 property value. The priorYrCostMth6 property
     * @return float|null
    */
    public function getPriorYrCostMth6(): ?float {
        return $this->priorYrCostMth6;
    }

    /**
     * Gets the priorYrCostMth7 property value. The priorYrCostMth7 property
     * @return float|null
    */
    public function getPriorYrCostMth7(): ?float {
        return $this->priorYrCostMth7;
    }

    /**
     * Gets the priorYrCostMth8 property value. The priorYrCostMth8 property
     * @return float|null
    */
    public function getPriorYrCostMth8(): ?float {
        return $this->priorYrCostMth8;
    }

    /**
     * Gets the priorYrCostMth9 property value. The priorYrCostMth9 property
     * @return float|null
    */
    public function getPriorYrCostMth9(): ?float {
        return $this->priorYrCostMth9;
    }

    /**
     * Gets the priorYrQtySoldBf property value. The priorYrQtySoldBf property
     * @return float|null
    */
    public function getPriorYrQtySoldBf(): ?float {
        return $this->priorYrQtySoldBf;
    }

    /**
     * Gets the priorYrQtySoldFuture property value. The priorYrQtySoldFuture property
     * @return float|null
    */
    public function getPriorYrQtySoldFuture(): ?float {
        return $this->priorYrQtySoldFuture;
    }

    /**
     * Gets the priorYrQtySoldMth1 property value. The priorYrQtySoldMth1 property
     * @return float|null
    */
    public function getPriorYrQtySoldMth1(): ?float {
        return $this->priorYrQtySoldMth1;
    }

    /**
     * Gets the priorYrQtySoldMth10 property value. The priorYrQtySoldMth10 property
     * @return float|null
    */
    public function getPriorYrQtySoldMth10(): ?float {
        return $this->priorYrQtySoldMth10;
    }

    /**
     * Gets the priorYrQtySoldMth11 property value. The priorYrQtySoldMth11 property
     * @return float|null
    */
    public function getPriorYrQtySoldMth11(): ?float {
        return $this->priorYrQtySoldMth11;
    }

    /**
     * Gets the priorYrQtySoldMth12 property value. The priorYrQtySoldMth12 property
     * @return float|null
    */
    public function getPriorYrQtySoldMth12(): ?float {
        return $this->priorYrQtySoldMth12;
    }

    /**
     * Gets the priorYrQtySoldMth2 property value. The priorYrQtySoldMth2 property
     * @return float|null
    */
    public function getPriorYrQtySoldMth2(): ?float {
        return $this->priorYrQtySoldMth2;
    }

    /**
     * Gets the priorYrQtySoldMth3 property value. The priorYrQtySoldMth3 property
     * @return float|null
    */
    public function getPriorYrQtySoldMth3(): ?float {
        return $this->priorYrQtySoldMth3;
    }

    /**
     * Gets the priorYrQtySoldMth4 property value. The priorYrQtySoldMth4 property
     * @return float|null
    */
    public function getPriorYrQtySoldMth4(): ?float {
        return $this->priorYrQtySoldMth4;
    }

    /**
     * Gets the priorYrQtySoldMth5 property value. The priorYrQtySoldMth5 property
     * @return float|null
    */
    public function getPriorYrQtySoldMth5(): ?float {
        return $this->priorYrQtySoldMth5;
    }

    /**
     * Gets the priorYrQtySoldMth6 property value. The priorYrQtySoldMth6 property
     * @return float|null
    */
    public function getPriorYrQtySoldMth6(): ?float {
        return $this->priorYrQtySoldMth6;
    }

    /**
     * Gets the priorYrQtySoldMth7 property value. The priorYrQtySoldMth7 property
     * @return float|null
    */
    public function getPriorYrQtySoldMth7(): ?float {
        return $this->priorYrQtySoldMth7;
    }

    /**
     * Gets the priorYrQtySoldMth8 property value. The priorYrQtySoldMth8 property
     * @return float|null
    */
    public function getPriorYrQtySoldMth8(): ?float {
        return $this->priorYrQtySoldMth8;
    }

    /**
     * Gets the priorYrQtySoldMth9 property value. The priorYrQtySoldMth9 property
     * @return float|null
    */
    public function getPriorYrQtySoldMth9(): ?float {
        return $this->priorYrQtySoldMth9;
    }

    /**
     * Gets the priorYrSalesBf property value. The priorYrSalesBf property
     * @return float|null
    */
    public function getPriorYrSalesBf(): ?float {
        return $this->priorYrSalesBf;
    }

    /**
     * Gets the priorYrSalesFuture property value. The priorYrSalesFuture property
     * @return float|null
    */
    public function getPriorYrSalesFuture(): ?float {
        return $this->priorYrSalesFuture;
    }

    /**
     * Gets the priorYrSalesMth1 property value. The priorYrSalesMth1 property
     * @return float|null
    */
    public function getPriorYrSalesMth1(): ?float {
        return $this->priorYrSalesMth1;
    }

    /**
     * Gets the priorYrSalesMth10 property value. The priorYrSalesMth10 property
     * @return float|null
    */
    public function getPriorYrSalesMth10(): ?float {
        return $this->priorYrSalesMth10;
    }

    /**
     * Gets the priorYrSalesMth11 property value. The priorYrSalesMth11 property
     * @return float|null
    */
    public function getPriorYrSalesMth11(): ?float {
        return $this->priorYrSalesMth11;
    }

    /**
     * Gets the priorYrSalesMth12 property value. The priorYrSalesMth12 property
     * @return float|null
    */
    public function getPriorYrSalesMth12(): ?float {
        return $this->priorYrSalesMth12;
    }

    /**
     * Gets the priorYrSalesMth2 property value. The priorYrSalesMth2 property
     * @return float|null
    */
    public function getPriorYrSalesMth2(): ?float {
        return $this->priorYrSalesMth2;
    }

    /**
     * Gets the priorYrSalesMth3 property value. The priorYrSalesMth3 property
     * @return float|null
    */
    public function getPriorYrSalesMth3(): ?float {
        return $this->priorYrSalesMth3;
    }

    /**
     * Gets the priorYrSalesMth4 property value. The priorYrSalesMth4 property
     * @return float|null
    */
    public function getPriorYrSalesMth4(): ?float {
        return $this->priorYrSalesMth4;
    }

    /**
     * Gets the priorYrSalesMth5 property value. The priorYrSalesMth5 property
     * @return float|null
    */
    public function getPriorYrSalesMth5(): ?float {
        return $this->priorYrSalesMth5;
    }

    /**
     * Gets the priorYrSalesMth6 property value. The priorYrSalesMth6 property
     * @return float|null
    */
    public function getPriorYrSalesMth6(): ?float {
        return $this->priorYrSalesMth6;
    }

    /**
     * Gets the priorYrSalesMth7 property value. The priorYrSalesMth7 property
     * @return float|null
    */
    public function getPriorYrSalesMth7(): ?float {
        return $this->priorYrSalesMth7;
    }

    /**
     * Gets the priorYrSalesMth8 property value. The priorYrSalesMth8 property
     * @return float|null
    */
    public function getPriorYrSalesMth8(): ?float {
        return $this->priorYrSalesMth8;
    }

    /**
     * Gets the priorYrSalesMth9 property value. The priorYrSalesMth9 property
     * @return float|null
    */
    public function getPriorYrSalesMth9(): ?float {
        return $this->priorYrSalesMth9;
    }

    /**
     * Gets the purchaseNominalCode property value. The purchaseNominalCode property
     * @return string|null
    */
    public function getPurchaseNominalCode(): ?string {
        return $this->purchaseNominalCode;
    }

    /**
     * Gets the purchaseRef property value. The purchaseRef property
     * @return string|null
    */
    public function getPurchaseRef(): ?string {
        return $this->purchaseRef;
    }

    /**
     * Gets the qtyAllocated property value. The qtyAllocated property
     * @return float|null
    */
    public function getQtyAllocated(): ?float {
        return $this->qtyAllocated;
    }

    /**
     * Gets the qtyInStock property value. The qtyInStock property
     * @return float|null
    */
    public function getQtyInStock(): ?float {
        return $this->qtyInStock;
    }

    /**
     * Gets the qtyLastOrder property value. The qtyLastOrder property
     * @return float|null
    */
    public function getQtyLastOrder(): ?float {
        return $this->qtyLastOrder;
    }

    /**
     * Gets the qtyLastStockTake property value. The qtyLastStockTake property
     * @return float|null
    */
    public function getQtyLastStockTake(): ?float {
        return $this->qtyLastStockTake;
    }

    /**
     * Gets the qtyMakeup property value. The qtyMakeup property
     * @return float|null
    */
    public function getQtyMakeup(): ?float {
        return $this->qtyMakeup;
    }

    /**
     * Gets the qtyOnOrder property value. The qtyOnOrder property
     * @return float|null
    */
    public function getQtyOnOrder(): ?float {
        return $this->qtyOnOrder;
    }

    /**
     * Gets the qtyReorder property value. The qtyReorder property
     * @return float|null
    */
    public function getQtyReorder(): ?float {
        return $this->qtyReorder;
    }

    /**
     * Gets the qtyReorderLevel property value. The qtyReorderLevel property
     * @return float|null
    */
    public function getQtyReorderLevel(): ?float {
        return $this->qtyReorderLevel;
    }

    /**
     * Gets the qtySoldBf property value. The qtySoldBf property
     * @return float|null
    */
    public function getQtySoldBf(): ?float {
        return $this->qtySoldBf;
    }

    /**
     * Gets the qtySoldFuture property value. The qtySoldFuture property
     * @return float|null
    */
    public function getQtySoldFuture(): ?float {
        return $this->qtySoldFuture;
    }

    /**
     * Gets the qtySoldMth1 property value. The qtySoldMth1 property
     * @return float|null
    */
    public function getQtySoldMth1(): ?float {
        return $this->qtySoldMth1;
    }

    /**
     * Gets the qtySoldMth10 property value. The qtySoldMth10 property
     * @return float|null
    */
    public function getQtySoldMth10(): ?float {
        return $this->qtySoldMth10;
    }

    /**
     * Gets the qtySoldMth11 property value. The qtySoldMth11 property
     * @return float|null
    */
    public function getQtySoldMth11(): ?float {
        return $this->qtySoldMth11;
    }

    /**
     * Gets the qtySoldMth12 property value. The qtySoldMth12 property
     * @return float|null
    */
    public function getQtySoldMth12(): ?float {
        return $this->qtySoldMth12;
    }

    /**
     * Gets the qtySoldMth2 property value. The qtySoldMth2 property
     * @return float|null
    */
    public function getQtySoldMth2(): ?float {
        return $this->qtySoldMth2;
    }

    /**
     * Gets the qtySoldMth3 property value. The qtySoldMth3 property
     * @return float|null
    */
    public function getQtySoldMth3(): ?float {
        return $this->qtySoldMth3;
    }

    /**
     * Gets the qtySoldMth4 property value. The qtySoldMth4 property
     * @return float|null
    */
    public function getQtySoldMth4(): ?float {
        return $this->qtySoldMth4;
    }

    /**
     * Gets the qtySoldMth5 property value. The qtySoldMth5 property
     * @return float|null
    */
    public function getQtySoldMth5(): ?float {
        return $this->qtySoldMth5;
    }

    /**
     * Gets the qtySoldMth6 property value. The qtySoldMth6 property
     * @return float|null
    */
    public function getQtySoldMth6(): ?float {
        return $this->qtySoldMth6;
    }

    /**
     * Gets the qtySoldMth7 property value. The qtySoldMth7 property
     * @return float|null
    */
    public function getQtySoldMth7(): ?float {
        return $this->qtySoldMth7;
    }

    /**
     * Gets the qtySoldMth8 property value. The qtySoldMth8 property
     * @return float|null
    */
    public function getQtySoldMth8(): ?float {
        return $this->qtySoldMth8;
    }

    /**
     * Gets the qtySoldMth9 property value. The qtySoldMth9 property
     * @return float|null
    */
    public function getQtySoldMth9(): ?float {
        return $this->qtySoldMth9;
    }

    /**
     * Gets the recordCreateDate property value. The recordCreateDate property
     * @return DateTime|null
    */
    public function getRecordCreateDate(): ?DateTime {
        return $this->recordCreateDate;
    }

    /**
     * Gets the recordDeleted property value. The recordDeleted property
     * @return int|null
    */
    public function getRecordDeleted(): ?int {
        return $this->recordDeleted;
    }

    /**
     * Gets the recordModifyDate property value. The recordModifyDate property
     * @return DateTime|null
    */
    public function getRecordModifyDate(): ?DateTime {
        return $this->recordModifyDate;
    }

    /**
     * Gets the salesBf property value. The salesBf property
     * @return float|null
    */
    public function getSalesBf(): ?float {
        return $this->salesBf;
    }

    /**
     * Gets the salesFuture property value. The salesFuture property
     * @return float|null
    */
    public function getSalesFuture(): ?float {
        return $this->salesFuture;
    }

    /**
     * Gets the salesMth1 property value. The salesMth1 property
     * @return float|null
    */
    public function getSalesMth1(): ?float {
        return $this->salesMth1;
    }

    /**
     * Gets the salesMth10 property value. The salesMth10 property
     * @return float|null
    */
    public function getSalesMth10(): ?float {
        return $this->salesMth10;
    }

    /**
     * Gets the salesMth11 property value. The salesMth11 property
     * @return float|null
    */
    public function getSalesMth11(): ?float {
        return $this->salesMth11;
    }

    /**
     * Gets the salesMth12 property value. The salesMth12 property
     * @return float|null
    */
    public function getSalesMth12(): ?float {
        return $this->salesMth12;
    }

    /**
     * Gets the salesMth2 property value. The salesMth2 property
     * @return float|null
    */
    public function getSalesMth2(): ?float {
        return $this->salesMth2;
    }

    /**
     * Gets the salesMth3 property value. The salesMth3 property
     * @return float|null
    */
    public function getSalesMth3(): ?float {
        return $this->salesMth3;
    }

    /**
     * Gets the salesMth4 property value. The salesMth4 property
     * @return float|null
    */
    public function getSalesMth4(): ?float {
        return $this->salesMth4;
    }

    /**
     * Gets the salesMth5 property value. The salesMth5 property
     * @return float|null
    */
    public function getSalesMth5(): ?float {
        return $this->salesMth5;
    }

    /**
     * Gets the salesMth6 property value. The salesMth6 property
     * @return float|null
    */
    public function getSalesMth6(): ?float {
        return $this->salesMth6;
    }

    /**
     * Gets the salesMth7 property value. The salesMth7 property
     * @return float|null
    */
    public function getSalesMth7(): ?float {
        return $this->salesMth7;
    }

    /**
     * Gets the salesMth8 property value. The salesMth8 property
     * @return float|null
    */
    public function getSalesMth8(): ?float {
        return $this->salesMth8;
    }

    /**
     * Gets the salesMth9 property value. The salesMth9 property
     * @return float|null
    */
    public function getSalesMth9(): ?float {
        return $this->salesMth9;
    }

    /**
     * Gets the salesPrice property value. The salesPrice property
     * @return float|null
    */
    public function getSalesPrice(): ?float {
        return $this->salesPrice;
    }

    /**
     * Gets the stockCat property value. The stockCat property
     * @return int|null
    */
    public function getStockCat(): ?int {
        return $this->stockCat;
    }

    /**
     * Gets the stockCatName property value. The stockCatName property
     * @return string|null
    */
    public function getStockCatName(): ?string {
        return $this->stockCatName;
    }

    /**
     * Gets the stockTakeDate property value. The stockTakeDate property
     * @return DateTime|null
    */
    public function getStockTakeDate(): ?DateTime {
        return $this->stockTakeDate;
    }

    /**
     * Gets the supplierPartNumber property value. The supplierPartNumber property
     * @return string|null
    */
    public function getSupplierPartNumber(): ?string {
        return $this->supplierPartNumber;
    }

    /**
     * Gets the suppUnitQty property value. The suppUnitQty property
     * @return float|null
    */
    public function getSuppUnitQty(): ?float {
        return $this->suppUnitQty;
    }

    /**
     * Gets the taxCode property value. The taxCode property
     * @return StockAttributesRead_taxCode|null
    */
    public function getTaxCode(): ?StockAttributesRead_taxCode {
        return $this->taxCode;
    }

    /**
     * Gets the thisRecord property value. The thisRecord property
     * @return int|null
    */
    public function getThisRecord(): ?int {
        return $this->thisRecord;
    }

    /**
     * Gets the unitOfSale property value. The unitOfSale property
     * @return string|null
    */
    public function getUnitOfSale(): ?string {
        return $this->unitOfSale;
    }

    /**
     * Gets the unitWeight property value. The unitWeight property
     * @return float|null
    */
    public function getUnitWeight(): ?float {
        return $this->unitWeight;
    }

    /**
     * Gets the webCategory1 property value. The webCategory1 property
     * @return string|null
    */
    public function getWebCategory1(): ?string {
        return $this->webCategory1;
    }

    /**
     * Gets the webCategory2 property value. The webCategory2 property
     * @return string|null
    */
    public function getWebCategory2(): ?string {
        return $this->webCategory2;
    }

    /**
     * Gets the webCategory3 property value. The webCategory3 property
     * @return string|null
    */
    public function getWebCategory3(): ?string {
        return $this->webCategory3;
    }

    /**
     * Gets the webDescription property value. The webDescription property
     * @return string|null
    */
    public function getWebDescription(): ?string {
        return $this->webDescription;
    }

    /**
     * Gets the webDetails property value. The webDetails property
     * @return string|null
    */
    public function getWebDetails(): ?string {
        return $this->webDetails;
    }

    /**
     * Gets the webImageFile property value. The webImageFile property
     * @return string|null
    */
    public function getWebImageFile(): ?string {
        return $this->webImageFile;
    }

    /**
     * Gets the webPublish property value. The webPublish property
     * @return int|null
    */
    public function getWebPublish(): ?int {
        return $this->webPublish;
    }

    /**
     * Gets the webSpecial property value. The webSpecial property
     * @return int|null
    */
    public function getWebSpecial(): ?int {
        return $this->webSpecial;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeIntegerValue('assemblyLevel', $this->getAssemblyLevel());
        $writer->writeFloatValue('averageCostPrice', $this->getAverageCostPrice());
        $writer->writeStringValue('barcode', $this->getBarcode());
        $writer->writeFloatValue('budgetQtySoldBf', $this->getBudgetQtySoldBf());
        $writer->writeFloatValue('budgetQtySoldFuture', $this->getBudgetQtySoldFuture());
        $writer->writeFloatValue('budgetQtySoldMth1', $this->getBudgetQtySoldMth1());
        $writer->writeFloatValue('budgetQtySoldMth10', $this->getBudgetQtySoldMth10());
        $writer->writeFloatValue('budgetQtySoldMth11', $this->getBudgetQtySoldMth11());
        $writer->writeFloatValue('budgetQtySoldMth12', $this->getBudgetQtySoldMth12());
        $writer->writeFloatValue('budgetQtySoldMth2', $this->getBudgetQtySoldMth2());
        $writer->writeFloatValue('budgetQtySoldMth3', $this->getBudgetQtySoldMth3());
        $writer->writeFloatValue('budgetQtySoldMth4', $this->getBudgetQtySoldMth4());
        $writer->writeFloatValue('budgetQtySoldMth5', $this->getBudgetQtySoldMth5());
        $writer->writeFloatValue('budgetQtySoldMth6', $this->getBudgetQtySoldMth6());
        $writer->writeFloatValue('budgetQtySoldMth7', $this->getBudgetQtySoldMth7());
        $writer->writeFloatValue('budgetQtySoldMth8', $this->getBudgetQtySoldMth8());
        $writer->writeFloatValue('budgetQtySoldMth9', $this->getBudgetQtySoldMth9());
        $writer->writeFloatValue('budgetSalesBf', $this->getBudgetSalesBf());
        $writer->writeFloatValue('budgetSalesFuture', $this->getBudgetSalesFuture());
        $writer->writeFloatValue('budgetSalesMth1', $this->getBudgetSalesMth1());
        $writer->writeFloatValue('budgetSalesMth10', $this->getBudgetSalesMth10());
        $writer->writeFloatValue('budgetSalesMth11', $this->getBudgetSalesMth11());
        $writer->writeFloatValue('budgetSalesMth12', $this->getBudgetSalesMth12());
        $writer->writeFloatValue('budgetSalesMth2', $this->getBudgetSalesMth2());
        $writer->writeFloatValue('budgetSalesMth3', $this->getBudgetSalesMth3());
        $writer->writeFloatValue('budgetSalesMth4', $this->getBudgetSalesMth4());
        $writer->writeFloatValue('budgetSalesMth5', $this->getBudgetSalesMth5());
        $writer->writeFloatValue('budgetSalesMth6', $this->getBudgetSalesMth6());
        $writer->writeFloatValue('budgetSalesMth7', $this->getBudgetSalesMth7());
        $writer->writeFloatValue('budgetSalesMth8', $this->getBudgetSalesMth8());
        $writer->writeFloatValue('budgetSalesMth9', $this->getBudgetSalesMth9());
        $writer->writeStringValue('commodityCode', $this->getCommodityCode());
        $writer->writeStringValue('componentCode1', $this->getComponentCode1());
        $writer->writeStringValue('componentCode10', $this->getComponentCode10());
        $writer->writeStringValue('componentCode11', $this->getComponentCode11());
        $writer->writeStringValue('componentCode12', $this->getComponentCode12());
        $writer->writeStringValue('componentCode13', $this->getComponentCode13());
        $writer->writeStringValue('componentCode14', $this->getComponentCode14());
        $writer->writeStringValue('componentCode15', $this->getComponentCode15());
        $writer->writeStringValue('componentCode16', $this->getComponentCode16());
        $writer->writeStringValue('componentCode17', $this->getComponentCode17());
        $writer->writeStringValue('componentCode18', $this->getComponentCode18());
        $writer->writeStringValue('componentCode19', $this->getComponentCode19());
        $writer->writeStringValue('componentCode2', $this->getComponentCode2());
        $writer->writeStringValue('componentCode20', $this->getComponentCode20());
        $writer->writeStringValue('componentCode21', $this->getComponentCode21());
        $writer->writeStringValue('componentCode22', $this->getComponentCode22());
        $writer->writeStringValue('componentCode23', $this->getComponentCode23());
        $writer->writeStringValue('componentCode24', $this->getComponentCode24());
        $writer->writeStringValue('componentCode25', $this->getComponentCode25());
        $writer->writeStringValue('componentCode26', $this->getComponentCode26());
        $writer->writeStringValue('componentCode27', $this->getComponentCode27());
        $writer->writeStringValue('componentCode28', $this->getComponentCode28());
        $writer->writeStringValue('componentCode29', $this->getComponentCode29());
        $writer->writeStringValue('componentCode3', $this->getComponentCode3());
        $writer->writeStringValue('componentCode30', $this->getComponentCode30());
        $writer->writeStringValue('componentCode31', $this->getComponentCode31());
        $writer->writeStringValue('componentCode32', $this->getComponentCode32());
        $writer->writeStringValue('componentCode33', $this->getComponentCode33());
        $writer->writeStringValue('componentCode34', $this->getComponentCode34());
        $writer->writeStringValue('componentCode35', $this->getComponentCode35());
        $writer->writeStringValue('componentCode36', $this->getComponentCode36());
        $writer->writeStringValue('componentCode37', $this->getComponentCode37());
        $writer->writeStringValue('componentCode38', $this->getComponentCode38());
        $writer->writeStringValue('componentCode39', $this->getComponentCode39());
        $writer->writeStringValue('componentCode4', $this->getComponentCode4());
        $writer->writeStringValue('componentCode40', $this->getComponentCode40());
        $writer->writeStringValue('componentCode41', $this->getComponentCode41());
        $writer->writeStringValue('componentCode42', $this->getComponentCode42());
        $writer->writeStringValue('componentCode43', $this->getComponentCode43());
        $writer->writeStringValue('componentCode44', $this->getComponentCode44());
        $writer->writeStringValue('componentCode45', $this->getComponentCode45());
        $writer->writeStringValue('componentCode46', $this->getComponentCode46());
        $writer->writeStringValue('componentCode47', $this->getComponentCode47());
        $writer->writeStringValue('componentCode48', $this->getComponentCode48());
        $writer->writeStringValue('componentCode49', $this->getComponentCode49());
        $writer->writeStringValue('componentCode5', $this->getComponentCode5());
        $writer->writeStringValue('componentCode50', $this->getComponentCode50());
        $writer->writeStringValue('componentCode6', $this->getComponentCode6());
        $writer->writeStringValue('componentCode7', $this->getComponentCode7());
        $writer->writeStringValue('componentCode8', $this->getComponentCode8());
        $writer->writeStringValue('componentCode9', $this->getComponentCode9());
        $writer->writeFloatValue('componentQty1', $this->getComponentQty1());
        $writer->writeFloatValue('componentQty10', $this->getComponentQty10());
        $writer->writeFloatValue('componentQty11', $this->getComponentQty11());
        $writer->writeFloatValue('componentQty12', $this->getComponentQty12());
        $writer->writeFloatValue('componentQty13', $this->getComponentQty13());
        $writer->writeFloatValue('componentQty14', $this->getComponentQty14());
        $writer->writeFloatValue('componentQty15', $this->getComponentQty15());
        $writer->writeFloatValue('componentQty16', $this->getComponentQty16());
        $writer->writeFloatValue('componentQty17', $this->getComponentQty17());
        $writer->writeFloatValue('componentQty18', $this->getComponentQty18());
        $writer->writeFloatValue('componentQty19', $this->getComponentQty19());
        $writer->writeFloatValue('componentQty2', $this->getComponentQty2());
        $writer->writeFloatValue('componentQty20', $this->getComponentQty20());
        $writer->writeFloatValue('componentQty21', $this->getComponentQty21());
        $writer->writeFloatValue('componentQty22', $this->getComponentQty22());
        $writer->writeFloatValue('componentQty23', $this->getComponentQty23());
        $writer->writeFloatValue('componentQty24', $this->getComponentQty24());
        $writer->writeFloatValue('componentQty25', $this->getComponentQty25());
        $writer->writeFloatValue('componentQty26', $this->getComponentQty26());
        $writer->writeFloatValue('componentQty27', $this->getComponentQty27());
        $writer->writeFloatValue('componentQty28', $this->getComponentQty28());
        $writer->writeFloatValue('componentQty29', $this->getComponentQty29());
        $writer->writeFloatValue('componentQty3', $this->getComponentQty3());
        $writer->writeFloatValue('componentQty30', $this->getComponentQty30());
        $writer->writeFloatValue('componentQty31', $this->getComponentQty31());
        $writer->writeFloatValue('componentQty32', $this->getComponentQty32());
        $writer->writeFloatValue('componentQty33', $this->getComponentQty33());
        $writer->writeFloatValue('componentQty34', $this->getComponentQty34());
        $writer->writeFloatValue('componentQty35', $this->getComponentQty35());
        $writer->writeFloatValue('componentQty36', $this->getComponentQty36());
        $writer->writeFloatValue('componentQty37', $this->getComponentQty37());
        $writer->writeFloatValue('componentQty38', $this->getComponentQty38());
        $writer->writeFloatValue('componentQty39', $this->getComponentQty39());
        $writer->writeFloatValue('componentQty4', $this->getComponentQty4());
        $writer->writeFloatValue('componentQty40', $this->getComponentQty40());
        $writer->writeFloatValue('componentQty41', $this->getComponentQty41());
        $writer->writeFloatValue('componentQty42', $this->getComponentQty42());
        $writer->writeFloatValue('componentQty43', $this->getComponentQty43());
        $writer->writeFloatValue('componentQty44', $this->getComponentQty44());
        $writer->writeFloatValue('componentQty45', $this->getComponentQty45());
        $writer->writeFloatValue('componentQty46', $this->getComponentQty46());
        $writer->writeFloatValue('componentQty47', $this->getComponentQty47());
        $writer->writeFloatValue('componentQty48', $this->getComponentQty48());
        $writer->writeFloatValue('componentQty49', $this->getComponentQty49());
        $writer->writeFloatValue('componentQty5', $this->getComponentQty5());
        $writer->writeFloatValue('componentQty50', $this->getComponentQty50());
        $writer->writeFloatValue('componentQty6', $this->getComponentQty6());
        $writer->writeFloatValue('componentQty7', $this->getComponentQty7());
        $writer->writeFloatValue('componentQty8', $this->getComponentQty8());
        $writer->writeFloatValue('componentQty9', $this->getComponentQty9());
        $writer->writeFloatValue('costBf', $this->getCostBf());
        $writer->writeFloatValue('costFuture', $this->getCostFuture());
        $writer->writeFloatValue('costMth1', $this->getCostMth1());
        $writer->writeFloatValue('costMth10', $this->getCostMth10());
        $writer->writeFloatValue('costMth11', $this->getCostMth11());
        $writer->writeFloatValue('costMth12', $this->getCostMth12());
        $writer->writeFloatValue('costMth2', $this->getCostMth2());
        $writer->writeFloatValue('costMth3', $this->getCostMth3());
        $writer->writeFloatValue('costMth4', $this->getCostMth4());
        $writer->writeFloatValue('costMth5', $this->getCostMth5());
        $writer->writeFloatValue('costMth6', $this->getCostMth6());
        $writer->writeFloatValue('costMth7', $this->getCostMth7());
        $writer->writeFloatValue('costMth8', $this->getCostMth8());
        $writer->writeFloatValue('costMth9', $this->getCostMth9());
        $writer->writeStringValue('countryCodeOfOrigin', $this->getCountryCodeOfOrigin());
        $writer->writeStringValue('deptName', $this->getDeptName());
        $writer->writeIntegerValue('deptNumber', $this->getDeptNumber());
        $writer->writeStringValue('description', $this->getDescription());
        $writer->writeFloatValue('discALevel10Qty', $this->getDiscALevel10Qty());
        $writer->writeFloatValue('discALevel10Rate', $this->getDiscALevel10Rate());
        $writer->writeFloatValue('discALevel1Qty', $this->getDiscALevel1Qty());
        $writer->writeFloatValue('discALevel1Rate', $this->getDiscALevel1Rate());
        $writer->writeFloatValue('discALevel2Qty', $this->getDiscALevel2Qty());
        $writer->writeFloatValue('discALevel2Rate', $this->getDiscALevel2Rate());
        $writer->writeFloatValue('discALevel3Qty', $this->getDiscALevel3Qty());
        $writer->writeFloatValue('discALevel3Rate', $this->getDiscALevel3Rate());
        $writer->writeFloatValue('discALevel4Qty', $this->getDiscALevel4Qty());
        $writer->writeFloatValue('discALevel4Rate', $this->getDiscALevel4Rate());
        $writer->writeFloatValue('discALevel5Qty', $this->getDiscALevel5Qty());
        $writer->writeFloatValue('discALevel5Rate', $this->getDiscALevel5Rate());
        $writer->writeFloatValue('discALevel6Qty', $this->getDiscALevel6Qty());
        $writer->writeFloatValue('discALevel6Rate', $this->getDiscALevel6Rate());
        $writer->writeFloatValue('discALevel7Qty', $this->getDiscALevel7Qty());
        $writer->writeFloatValue('discALevel7Rate', $this->getDiscALevel7Rate());
        $writer->writeFloatValue('discALevel8Qty', $this->getDiscALevel8Qty());
        $writer->writeFloatValue('discALevel8Rate', $this->getDiscALevel8Rate());
        $writer->writeFloatValue('discALevel9Qty', $this->getDiscALevel9Qty());
        $writer->writeFloatValue('discALevel9Rate', $this->getDiscALevel9Rate());
        $writer->writeFloatValue('discBLevel10Qty', $this->getDiscBLevel10Qty());
        $writer->writeFloatValue('discBLevel10Rate', $this->getDiscBLevel10Rate());
        $writer->writeFloatValue('discBLevel1Qty', $this->getDiscBLevel1Qty());
        $writer->writeFloatValue('discBLevel1Rate', $this->getDiscBLevel1Rate());
        $writer->writeFloatValue('discBLevel2Qty', $this->getDiscBLevel2Qty());
        $writer->writeFloatValue('discBLevel2Rate', $this->getDiscBLevel2Rate());
        $writer->writeFloatValue('discBLevel3Qty', $this->getDiscBLevel3Qty());
        $writer->writeFloatValue('discBLevel3Rate', $this->getDiscBLevel3Rate());
        $writer->writeFloatValue('discBLevel4Qty', $this->getDiscBLevel4Qty());
        $writer->writeFloatValue('discBLevel4Rate', $this->getDiscBLevel4Rate());
        $writer->writeFloatValue('discBLevel5Qty', $this->getDiscBLevel5Qty());
        $writer->writeFloatValue('discBLevel5Rate', $this->getDiscBLevel5Rate());
        $writer->writeFloatValue('discBLevel6Qty', $this->getDiscBLevel6Qty());
        $writer->writeFloatValue('discBLevel6Rate', $this->getDiscBLevel6Rate());
        $writer->writeFloatValue('discBLevel7Qty', $this->getDiscBLevel7Qty());
        $writer->writeFloatValue('discBLevel7Rate', $this->getDiscBLevel7Rate());
        $writer->writeFloatValue('discBLevel8Qty', $this->getDiscBLevel8Qty());
        $writer->writeFloatValue('discBLevel8Rate', $this->getDiscBLevel8Rate());
        $writer->writeFloatValue('discBLevel9Qty', $this->getDiscBLevel9Qty());
        $writer->writeFloatValue('discBLevel9Rate', $this->getDiscBLevel9Rate());
        $writer->writeFloatValue('discCLevel10Qty', $this->getDiscCLevel10Qty());
        $writer->writeFloatValue('discCLevel10Rate', $this->getDiscCLevel10Rate());
        $writer->writeFloatValue('discCLevel1Qty', $this->getDiscCLevel1Qty());
        $writer->writeFloatValue('discCLevel1Rate', $this->getDiscCLevel1Rate());
        $writer->writeFloatValue('discCLevel2Qty', $this->getDiscCLevel2Qty());
        $writer->writeFloatValue('discCLevel2Rate', $this->getDiscCLevel2Rate());
        $writer->writeFloatValue('discCLevel3Qty', $this->getDiscCLevel3Qty());
        $writer->writeFloatValue('discCLevel3Rate', $this->getDiscCLevel3Rate());
        $writer->writeFloatValue('discCLevel4Qty', $this->getDiscCLevel4Qty());
        $writer->writeFloatValue('discCLevel4Rate', $this->getDiscCLevel4Rate());
        $writer->writeFloatValue('discCLevel5Qty', $this->getDiscCLevel5Qty());
        $writer->writeFloatValue('discCLevel5Rate', $this->getDiscCLevel5Rate());
        $writer->writeFloatValue('discCLevel6Qty', $this->getDiscCLevel6Qty());
        $writer->writeFloatValue('discCLevel6Rate', $this->getDiscCLevel6Rate());
        $writer->writeFloatValue('discCLevel7Qty', $this->getDiscCLevel7Qty());
        $writer->writeFloatValue('discCLevel7Rate', $this->getDiscCLevel7Rate());
        $writer->writeFloatValue('discCLevel8Qty', $this->getDiscCLevel8Qty());
        $writer->writeFloatValue('discCLevel8Rate', $this->getDiscCLevel8Rate());
        $writer->writeFloatValue('discCLevel9Qty', $this->getDiscCLevel9Qty());
        $writer->writeFloatValue('discCLevel9Rate', $this->getDiscCLevel9Rate());
        $writer->writeFloatValue('discDLevel10Qty', $this->getDiscDLevel10Qty());
        $writer->writeFloatValue('discDLevel10Rate', $this->getDiscDLevel10Rate());
        $writer->writeFloatValue('discDLevel1Qty', $this->getDiscDLevel1Qty());
        $writer->writeFloatValue('discDLevel1Rate', $this->getDiscDLevel1Rate());
        $writer->writeFloatValue('discDLevel2Qty', $this->getDiscDLevel2Qty());
        $writer->writeFloatValue('discDLevel2Rate', $this->getDiscDLevel2Rate());
        $writer->writeFloatValue('discDLevel3Qty', $this->getDiscDLevel3Qty());
        $writer->writeFloatValue('discDLevel3Rate', $this->getDiscDLevel3Rate());
        $writer->writeFloatValue('discDLevel4Qty', $this->getDiscDLevel4Qty());
        $writer->writeFloatValue('discDLevel4Rate', $this->getDiscDLevel4Rate());
        $writer->writeFloatValue('discDLevel5Qty', $this->getDiscDLevel5Qty());
        $writer->writeFloatValue('discDLevel5Rate', $this->getDiscDLevel5Rate());
        $writer->writeFloatValue('discDLevel6Qty', $this->getDiscDLevel6Qty());
        $writer->writeFloatValue('discDLevel6Rate', $this->getDiscDLevel6Rate());
        $writer->writeFloatValue('discDLevel7Qty', $this->getDiscDLevel7Qty());
        $writer->writeFloatValue('discDLevel7Rate', $this->getDiscDLevel7Rate());
        $writer->writeFloatValue('discDLevel8Qty', $this->getDiscDLevel8Qty());
        $writer->writeFloatValue('discDLevel8Rate', $this->getDiscDLevel8Rate());
        $writer->writeFloatValue('discDLevel9Qty', $this->getDiscDLevel9Qty());
        $writer->writeFloatValue('discDLevel9Rate', $this->getDiscDLevel9Rate());
        $writer->writeFloatValue('discELevel10Qty', $this->getDiscELevel10Qty());
        $writer->writeFloatValue('discELevel10Rate', $this->getDiscELevel10Rate());
        $writer->writeFloatValue('discELevel1Qty', $this->getDiscELevel1Qty());
        $writer->writeFloatValue('discELevel1Rate', $this->getDiscELevel1Rate());
        $writer->writeFloatValue('discELevel2Qty', $this->getDiscELevel2Qty());
        $writer->writeFloatValue('discELevel2Rate', $this->getDiscELevel2Rate());
        $writer->writeFloatValue('discELevel3Qty', $this->getDiscELevel3Qty());
        $writer->writeFloatValue('discELevel3Rate', $this->getDiscELevel3Rate());
        $writer->writeFloatValue('discELevel4Qty', $this->getDiscELevel4Qty());
        $writer->writeFloatValue('discELevel4Rate', $this->getDiscELevel4Rate());
        $writer->writeFloatValue('discELevel5Qty', $this->getDiscELevel5Qty());
        $writer->writeFloatValue('discELevel5Rate', $this->getDiscELevel5Rate());
        $writer->writeFloatValue('discELevel6Qty', $this->getDiscELevel6Qty());
        $writer->writeFloatValue('discELevel6Rate', $this->getDiscELevel6Rate());
        $writer->writeFloatValue('discELevel7Qty', $this->getDiscELevel7Qty());
        $writer->writeFloatValue('discELevel7Rate', $this->getDiscELevel7Rate());
        $writer->writeFloatValue('discELevel8Qty', $this->getDiscELevel8Qty());
        $writer->writeFloatValue('discELevel8Rate', $this->getDiscELevel8Rate());
        $writer->writeFloatValue('discELevel9Qty', $this->getDiscELevel9Qty());
        $writer->writeFloatValue('discELevel9Rate', $this->getDiscELevel9Rate());
        $writer->writeStringValue('hasBom', $this->getHasBom());
        $writer->writeIntegerValue('hasNoComponent', $this->getHasNoComponent());
        $writer->writeIntegerValue('ignoreStkLvlFlag', $this->getIgnoreStkLvlFlag());
        $writer->writeIntegerValue('inactiveFlag', $this->getInactiveFlag());
        $writer->writeStringValue('intrastatCommCode', $this->getIntrastatCommCode());
        $writer->writeStringValue('intrastatImportDutyCode', $this->getIntrastatImportDutyCode());
        $writer->writeFloatValue('lastDiscPurchasePrice', $this->getLastDiscPurchasePrice());
        $writer->writeDateTimeValue('lastPurchaseDate', $this->getLastPurchaseDate());
        $writer->writeFloatValue('lastPurchasePrice', $this->getLastPurchasePrice());
        $writer->writeDateTimeValue('lastSaleDate', $this->getLastSaleDate());
        $writer->writeIntegerValue('linkLevel', $this->getLinkLevel());
        $writer->writeStringValue('location', $this->getLocation());
        $writer->writeFloatValue('priorYrCostBf', $this->getPriorYrCostBf());
        $writer->writeFloatValue('priorYrCostFuture', $this->getPriorYrCostFuture());
        $writer->writeFloatValue('priorYrCostMth1', $this->getPriorYrCostMth1());
        $writer->writeFloatValue('priorYrCostMth10', $this->getPriorYrCostMth10());
        $writer->writeFloatValue('priorYrCostMth11', $this->getPriorYrCostMth11());
        $writer->writeFloatValue('priorYrCostMth12', $this->getPriorYrCostMth12());
        $writer->writeFloatValue('priorYrCostMth2', $this->getPriorYrCostMth2());
        $writer->writeFloatValue('priorYrCostMth3', $this->getPriorYrCostMth3());
        $writer->writeFloatValue('priorYrCostMth4', $this->getPriorYrCostMth4());
        $writer->writeFloatValue('priorYrCostMth5', $this->getPriorYrCostMth5());
        $writer->writeFloatValue('priorYrCostMth6', $this->getPriorYrCostMth6());
        $writer->writeFloatValue('priorYrCostMth7', $this->getPriorYrCostMth7());
        $writer->writeFloatValue('priorYrCostMth8', $this->getPriorYrCostMth8());
        $writer->writeFloatValue('priorYrCostMth9', $this->getPriorYrCostMth9());
        $writer->writeFloatValue('priorYrQtySoldBf', $this->getPriorYrQtySoldBf());
        $writer->writeFloatValue('priorYrQtySoldFuture', $this->getPriorYrQtySoldFuture());
        $writer->writeFloatValue('priorYrQtySoldMth1', $this->getPriorYrQtySoldMth1());
        $writer->writeFloatValue('priorYrQtySoldMth10', $this->getPriorYrQtySoldMth10());
        $writer->writeFloatValue('priorYrQtySoldMth11', $this->getPriorYrQtySoldMth11());
        $writer->writeFloatValue('priorYrQtySoldMth12', $this->getPriorYrQtySoldMth12());
        $writer->writeFloatValue('priorYrQtySoldMth2', $this->getPriorYrQtySoldMth2());
        $writer->writeFloatValue('priorYrQtySoldMth3', $this->getPriorYrQtySoldMth3());
        $writer->writeFloatValue('priorYrQtySoldMth4', $this->getPriorYrQtySoldMth4());
        $writer->writeFloatValue('priorYrQtySoldMth5', $this->getPriorYrQtySoldMth5());
        $writer->writeFloatValue('priorYrQtySoldMth6', $this->getPriorYrQtySoldMth6());
        $writer->writeFloatValue('priorYrQtySoldMth7', $this->getPriorYrQtySoldMth7());
        $writer->writeFloatValue('priorYrQtySoldMth8', $this->getPriorYrQtySoldMth8());
        $writer->writeFloatValue('priorYrQtySoldMth9', $this->getPriorYrQtySoldMth9());
        $writer->writeFloatValue('priorYrSalesBf', $this->getPriorYrSalesBf());
        $writer->writeFloatValue('priorYrSalesFuture', $this->getPriorYrSalesFuture());
        $writer->writeFloatValue('priorYrSalesMth1', $this->getPriorYrSalesMth1());
        $writer->writeFloatValue('priorYrSalesMth10', $this->getPriorYrSalesMth10());
        $writer->writeFloatValue('priorYrSalesMth11', $this->getPriorYrSalesMth11());
        $writer->writeFloatValue('priorYrSalesMth12', $this->getPriorYrSalesMth12());
        $writer->writeFloatValue('priorYrSalesMth2', $this->getPriorYrSalesMth2());
        $writer->writeFloatValue('priorYrSalesMth3', $this->getPriorYrSalesMth3());
        $writer->writeFloatValue('priorYrSalesMth4', $this->getPriorYrSalesMth4());
        $writer->writeFloatValue('priorYrSalesMth5', $this->getPriorYrSalesMth5());
        $writer->writeFloatValue('priorYrSalesMth6', $this->getPriorYrSalesMth6());
        $writer->writeFloatValue('priorYrSalesMth7', $this->getPriorYrSalesMth7());
        $writer->writeFloatValue('priorYrSalesMth8', $this->getPriorYrSalesMth8());
        $writer->writeFloatValue('priorYrSalesMth9', $this->getPriorYrSalesMth9());
        $writer->writeStringValue('purchaseNominalCode', $this->getPurchaseNominalCode());
        $writer->writeStringValue('purchaseRef', $this->getPurchaseRef());
        $writer->writeFloatValue('qtyAllocated', $this->getQtyAllocated());
        $writer->writeFloatValue('qtyInStock', $this->getQtyInStock());
        $writer->writeFloatValue('qtyLastOrder', $this->getQtyLastOrder());
        $writer->writeFloatValue('qtyLastStockTake', $this->getQtyLastStockTake());
        $writer->writeFloatValue('qtyMakeup', $this->getQtyMakeup());
        $writer->writeFloatValue('qtyOnOrder', $this->getQtyOnOrder());
        $writer->writeFloatValue('qtyReorder', $this->getQtyReorder());
        $writer->writeFloatValue('qtyReorderLevel', $this->getQtyReorderLevel());
        $writer->writeFloatValue('qtySoldBf', $this->getQtySoldBf());
        $writer->writeFloatValue('qtySoldFuture', $this->getQtySoldFuture());
        $writer->writeFloatValue('qtySoldMth1', $this->getQtySoldMth1());
        $writer->writeFloatValue('qtySoldMth10', $this->getQtySoldMth10());
        $writer->writeFloatValue('qtySoldMth11', $this->getQtySoldMth11());
        $writer->writeFloatValue('qtySoldMth12', $this->getQtySoldMth12());
        $writer->writeFloatValue('qtySoldMth2', $this->getQtySoldMth2());
        $writer->writeFloatValue('qtySoldMth3', $this->getQtySoldMth3());
        $writer->writeFloatValue('qtySoldMth4', $this->getQtySoldMth4());
        $writer->writeFloatValue('qtySoldMth5', $this->getQtySoldMth5());
        $writer->writeFloatValue('qtySoldMth6', $this->getQtySoldMth6());
        $writer->writeFloatValue('qtySoldMth7', $this->getQtySoldMth7());
        $writer->writeFloatValue('qtySoldMth8', $this->getQtySoldMth8());
        $writer->writeFloatValue('qtySoldMth9', $this->getQtySoldMth9());
        $writer->writeDateTimeValue('recordCreateDate', $this->getRecordCreateDate());
        $writer->writeIntegerValue('recordDeleted', $this->getRecordDeleted());
        $writer->writeDateTimeValue('recordModifyDate', $this->getRecordModifyDate());
        $writer->writeFloatValue('salesBf', $this->getSalesBf());
        $writer->writeFloatValue('salesFuture', $this->getSalesFuture());
        $writer->writeFloatValue('salesMth1', $this->getSalesMth1());
        $writer->writeFloatValue('salesMth10', $this->getSalesMth10());
        $writer->writeFloatValue('salesMth11', $this->getSalesMth11());
        $writer->writeFloatValue('salesMth12', $this->getSalesMth12());
        $writer->writeFloatValue('salesMth2', $this->getSalesMth2());
        $writer->writeFloatValue('salesMth3', $this->getSalesMth3());
        $writer->writeFloatValue('salesMth4', $this->getSalesMth4());
        $writer->writeFloatValue('salesMth5', $this->getSalesMth5());
        $writer->writeFloatValue('salesMth6', $this->getSalesMth6());
        $writer->writeFloatValue('salesMth7', $this->getSalesMth7());
        $writer->writeFloatValue('salesMth8', $this->getSalesMth8());
        $writer->writeFloatValue('salesMth9', $this->getSalesMth9());
        $writer->writeFloatValue('salesPrice', $this->getSalesPrice());
        $writer->writeIntegerValue('stockCat', $this->getStockCat());
        $writer->writeStringValue('stockCatName', $this->getStockCatName());
        $writer->writeDateTimeValue('stockTakeDate', $this->getStockTakeDate());
        $writer->writeStringValue('supplierPartNumber', $this->getSupplierPartNumber());
        $writer->writeFloatValue('suppUnitQty', $this->getSuppUnitQty());
        $writer->writeEnumValue('taxCode', $this->getTaxCode());
        $writer->writeIntegerValue('thisRecord', $this->getThisRecord());
        $writer->writeStringValue('unitOfSale', $this->getUnitOfSale());
        $writer->writeFloatValue('unitWeight', $this->getUnitWeight());
        $writer->writeStringValue('webCategory1', $this->getWebCategory1());
        $writer->writeStringValue('webCategory2', $this->getWebCategory2());
        $writer->writeStringValue('webCategory3', $this->getWebCategory3());
        $writer->writeStringValue('webDescription', $this->getWebDescription());
        $writer->writeStringValue('webDetails', $this->getWebDetails());
        $writer->writeStringValue('webImageFile', $this->getWebImageFile());
        $writer->writeIntegerValue('webPublish', $this->getWebPublish());
        $writer->writeIntegerValue('webSpecial', $this->getWebSpecial());
    }

    /**
     * Sets the assemblyLevel property value. The assemblyLevel property
     * @param int|null $value Value to set for the assemblyLevel property.
    */
    public function setAssemblyLevel(?int $value): void {
        $this->assemblyLevel = $value;
    }

    /**
     * Sets the averageCostPrice property value. The averageCostPrice property
     * @param float|null $value Value to set for the averageCostPrice property.
    */
    public function setAverageCostPrice(?float $value): void {
        $this->averageCostPrice = $value;
    }

    /**
     * Sets the barcode property value. The barcode property
     * @param string|null $value Value to set for the barcode property.
    */
    public function setBarcode(?string $value): void {
        $this->barcode = $value;
    }

    /**
     * Sets the budgetQtySoldBf property value. The budgetQtySoldBf property
     * @param float|null $value Value to set for the budgetQtySoldBf property.
    */
    public function setBudgetQtySoldBf(?float $value): void {
        $this->budgetQtySoldBf = $value;
    }

    /**
     * Sets the budgetQtySoldFuture property value. The budgetQtySoldFuture property
     * @param float|null $value Value to set for the budgetQtySoldFuture property.
    */
    public function setBudgetQtySoldFuture(?float $value): void {
        $this->budgetQtySoldFuture = $value;
    }

    /**
     * Sets the budgetQtySoldMth1 property value. The budgetQtySoldMth1 property
     * @param float|null $value Value to set for the budgetQtySoldMth1 property.
    */
    public function setBudgetQtySoldMth1(?float $value): void {
        $this->budgetQtySoldMth1 = $value;
    }

    /**
     * Sets the budgetQtySoldMth10 property value. The budgetQtySoldMth10 property
     * @param float|null $value Value to set for the budgetQtySoldMth10 property.
    */
    public function setBudgetQtySoldMth10(?float $value): void {
        $this->budgetQtySoldMth10 = $value;
    }

    /**
     * Sets the budgetQtySoldMth11 property value. The budgetQtySoldMth11 property
     * @param float|null $value Value to set for the budgetQtySoldMth11 property.
    */
    public function setBudgetQtySoldMth11(?float $value): void {
        $this->budgetQtySoldMth11 = $value;
    }

    /**
     * Sets the budgetQtySoldMth12 property value. The budgetQtySoldMth12 property
     * @param float|null $value Value to set for the budgetQtySoldMth12 property.
    */
    public function setBudgetQtySoldMth12(?float $value): void {
        $this->budgetQtySoldMth12 = $value;
    }

    /**
     * Sets the budgetQtySoldMth2 property value. The budgetQtySoldMth2 property
     * @param float|null $value Value to set for the budgetQtySoldMth2 property.
    */
    public function setBudgetQtySoldMth2(?float $value): void {
        $this->budgetQtySoldMth2 = $value;
    }

    /**
     * Sets the budgetQtySoldMth3 property value. The budgetQtySoldMth3 property
     * @param float|null $value Value to set for the budgetQtySoldMth3 property.
    */
    public function setBudgetQtySoldMth3(?float $value): void {
        $this->budgetQtySoldMth3 = $value;
    }

    /**
     * Sets the budgetQtySoldMth4 property value. The budgetQtySoldMth4 property
     * @param float|null $value Value to set for the budgetQtySoldMth4 property.
    */
    public function setBudgetQtySoldMth4(?float $value): void {
        $this->budgetQtySoldMth4 = $value;
    }

    /**
     * Sets the budgetQtySoldMth5 property value. The budgetQtySoldMth5 property
     * @param float|null $value Value to set for the budgetQtySoldMth5 property.
    */
    public function setBudgetQtySoldMth5(?float $value): void {
        $this->budgetQtySoldMth5 = $value;
    }

    /**
     * Sets the budgetQtySoldMth6 property value. The budgetQtySoldMth6 property
     * @param float|null $value Value to set for the budgetQtySoldMth6 property.
    */
    public function setBudgetQtySoldMth6(?float $value): void {
        $this->budgetQtySoldMth6 = $value;
    }

    /**
     * Sets the budgetQtySoldMth7 property value. The budgetQtySoldMth7 property
     * @param float|null $value Value to set for the budgetQtySoldMth7 property.
    */
    public function setBudgetQtySoldMth7(?float $value): void {
        $this->budgetQtySoldMth7 = $value;
    }

    /**
     * Sets the budgetQtySoldMth8 property value. The budgetQtySoldMth8 property
     * @param float|null $value Value to set for the budgetQtySoldMth8 property.
    */
    public function setBudgetQtySoldMth8(?float $value): void {
        $this->budgetQtySoldMth8 = $value;
    }

    /**
     * Sets the budgetQtySoldMth9 property value. The budgetQtySoldMth9 property
     * @param float|null $value Value to set for the budgetQtySoldMth9 property.
    */
    public function setBudgetQtySoldMth9(?float $value): void {
        $this->budgetQtySoldMth9 = $value;
    }

    /**
     * Sets the budgetSalesBf property value. The budgetSalesBf property
     * @param float|null $value Value to set for the budgetSalesBf property.
    */
    public function setBudgetSalesBf(?float $value): void {
        $this->budgetSalesBf = $value;
    }

    /**
     * Sets the budgetSalesFuture property value. The budgetSalesFuture property
     * @param float|null $value Value to set for the budgetSalesFuture property.
    */
    public function setBudgetSalesFuture(?float $value): void {
        $this->budgetSalesFuture = $value;
    }

    /**
     * Sets the budgetSalesMth1 property value. The budgetSalesMth1 property
     * @param float|null $value Value to set for the budgetSalesMth1 property.
    */
    public function setBudgetSalesMth1(?float $value): void {
        $this->budgetSalesMth1 = $value;
    }

    /**
     * Sets the budgetSalesMth10 property value. The budgetSalesMth10 property
     * @param float|null $value Value to set for the budgetSalesMth10 property.
    */
    public function setBudgetSalesMth10(?float $value): void {
        $this->budgetSalesMth10 = $value;
    }

    /**
     * Sets the budgetSalesMth11 property value. The budgetSalesMth11 property
     * @param float|null $value Value to set for the budgetSalesMth11 property.
    */
    public function setBudgetSalesMth11(?float $value): void {
        $this->budgetSalesMth11 = $value;
    }

    /**
     * Sets the budgetSalesMth12 property value. The budgetSalesMth12 property
     * @param float|null $value Value to set for the budgetSalesMth12 property.
    */
    public function setBudgetSalesMth12(?float $value): void {
        $this->budgetSalesMth12 = $value;
    }

    /**
     * Sets the budgetSalesMth2 property value. The budgetSalesMth2 property
     * @param float|null $value Value to set for the budgetSalesMth2 property.
    */
    public function setBudgetSalesMth2(?float $value): void {
        $this->budgetSalesMth2 = $value;
    }

    /**
     * Sets the budgetSalesMth3 property value. The budgetSalesMth3 property
     * @param float|null $value Value to set for the budgetSalesMth3 property.
    */
    public function setBudgetSalesMth3(?float $value): void {
        $this->budgetSalesMth3 = $value;
    }

    /**
     * Sets the budgetSalesMth4 property value. The budgetSalesMth4 property
     * @param float|null $value Value to set for the budgetSalesMth4 property.
    */
    public function setBudgetSalesMth4(?float $value): void {
        $this->budgetSalesMth4 = $value;
    }

    /**
     * Sets the budgetSalesMth5 property value. The budgetSalesMth5 property
     * @param float|null $value Value to set for the budgetSalesMth5 property.
    */
    public function setBudgetSalesMth5(?float $value): void {
        $this->budgetSalesMth5 = $value;
    }

    /**
     * Sets the budgetSalesMth6 property value. The budgetSalesMth6 property
     * @param float|null $value Value to set for the budgetSalesMth6 property.
    */
    public function setBudgetSalesMth6(?float $value): void {
        $this->budgetSalesMth6 = $value;
    }

    /**
     * Sets the budgetSalesMth7 property value. The budgetSalesMth7 property
     * @param float|null $value Value to set for the budgetSalesMth7 property.
    */
    public function setBudgetSalesMth7(?float $value): void {
        $this->budgetSalesMth7 = $value;
    }

    /**
     * Sets the budgetSalesMth8 property value. The budgetSalesMth8 property
     * @param float|null $value Value to set for the budgetSalesMth8 property.
    */
    public function setBudgetSalesMth8(?float $value): void {
        $this->budgetSalesMth8 = $value;
    }

    /**
     * Sets the budgetSalesMth9 property value. The budgetSalesMth9 property
     * @param float|null $value Value to set for the budgetSalesMth9 property.
    */
    public function setBudgetSalesMth9(?float $value): void {
        $this->budgetSalesMth9 = $value;
    }

    /**
     * Sets the commodityCode property value. The commodityCode property
     * @param string|null $value Value to set for the commodityCode property.
    */
    public function setCommodityCode(?string $value): void {
        $this->commodityCode = $value;
    }

    /**
     * Sets the componentCode1 property value. The componentCode1 property
     * @param string|null $value Value to set for the componentCode1 property.
    */
    public function setComponentCode1(?string $value): void {
        $this->componentCode1 = $value;
    }

    /**
     * Sets the componentCode10 property value. The componentCode10 property
     * @param string|null $value Value to set for the componentCode10 property.
    */
    public function setComponentCode10(?string $value): void {
        $this->componentCode10 = $value;
    }

    /**
     * Sets the componentCode11 property value. The componentCode11 property
     * @param string|null $value Value to set for the componentCode11 property.
    */
    public function setComponentCode11(?string $value): void {
        $this->componentCode11 = $value;
    }

    /**
     * Sets the componentCode12 property value. The componentCode12 property
     * @param string|null $value Value to set for the componentCode12 property.
    */
    public function setComponentCode12(?string $value): void {
        $this->componentCode12 = $value;
    }

    /**
     * Sets the componentCode13 property value. The componentCode13 property
     * @param string|null $value Value to set for the componentCode13 property.
    */
    public function setComponentCode13(?string $value): void {
        $this->componentCode13 = $value;
    }

    /**
     * Sets the componentCode14 property value. The componentCode14 property
     * @param string|null $value Value to set for the componentCode14 property.
    */
    public function setComponentCode14(?string $value): void {
        $this->componentCode14 = $value;
    }

    /**
     * Sets the componentCode15 property value. The componentCode15 property
     * @param string|null $value Value to set for the componentCode15 property.
    */
    public function setComponentCode15(?string $value): void {
        $this->componentCode15 = $value;
    }

    /**
     * Sets the componentCode16 property value. The componentCode16 property
     * @param string|null $value Value to set for the componentCode16 property.
    */
    public function setComponentCode16(?string $value): void {
        $this->componentCode16 = $value;
    }

    /**
     * Sets the componentCode17 property value. The componentCode17 property
     * @param string|null $value Value to set for the componentCode17 property.
    */
    public function setComponentCode17(?string $value): void {
        $this->componentCode17 = $value;
    }

    /**
     * Sets the componentCode18 property value. The componentCode18 property
     * @param string|null $value Value to set for the componentCode18 property.
    */
    public function setComponentCode18(?string $value): void {
        $this->componentCode18 = $value;
    }

    /**
     * Sets the componentCode19 property value. The componentCode19 property
     * @param string|null $value Value to set for the componentCode19 property.
    */
    public function setComponentCode19(?string $value): void {
        $this->componentCode19 = $value;
    }

    /**
     * Sets the componentCode2 property value. The componentCode2 property
     * @param string|null $value Value to set for the componentCode2 property.
    */
    public function setComponentCode2(?string $value): void {
        $this->componentCode2 = $value;
    }

    /**
     * Sets the componentCode20 property value. The componentCode20 property
     * @param string|null $value Value to set for the componentCode20 property.
    */
    public function setComponentCode20(?string $value): void {
        $this->componentCode20 = $value;
    }

    /**
     * Sets the componentCode21 property value. The componentCode21 property
     * @param string|null $value Value to set for the componentCode21 property.
    */
    public function setComponentCode21(?string $value): void {
        $this->componentCode21 = $value;
    }

    /**
     * Sets the componentCode22 property value. The componentCode22 property
     * @param string|null $value Value to set for the componentCode22 property.
    */
    public function setComponentCode22(?string $value): void {
        $this->componentCode22 = $value;
    }

    /**
     * Sets the componentCode23 property value. The componentCode23 property
     * @param string|null $value Value to set for the componentCode23 property.
    */
    public function setComponentCode23(?string $value): void {
        $this->componentCode23 = $value;
    }

    /**
     * Sets the componentCode24 property value. The componentCode24 property
     * @param string|null $value Value to set for the componentCode24 property.
    */
    public function setComponentCode24(?string $value): void {
        $this->componentCode24 = $value;
    }

    /**
     * Sets the componentCode25 property value. The componentCode25 property
     * @param string|null $value Value to set for the componentCode25 property.
    */
    public function setComponentCode25(?string $value): void {
        $this->componentCode25 = $value;
    }

    /**
     * Sets the componentCode26 property value. The componentCode26 property
     * @param string|null $value Value to set for the componentCode26 property.
    */
    public function setComponentCode26(?string $value): void {
        $this->componentCode26 = $value;
    }

    /**
     * Sets the componentCode27 property value. The componentCode27 property
     * @param string|null $value Value to set for the componentCode27 property.
    */
    public function setComponentCode27(?string $value): void {
        $this->componentCode27 = $value;
    }

    /**
     * Sets the componentCode28 property value. The componentCode28 property
     * @param string|null $value Value to set for the componentCode28 property.
    */
    public function setComponentCode28(?string $value): void {
        $this->componentCode28 = $value;
    }

    /**
     * Sets the componentCode29 property value. The componentCode29 property
     * @param string|null $value Value to set for the componentCode29 property.
    */
    public function setComponentCode29(?string $value): void {
        $this->componentCode29 = $value;
    }

    /**
     * Sets the componentCode3 property value. The componentCode3 property
     * @param string|null $value Value to set for the componentCode3 property.
    */
    public function setComponentCode3(?string $value): void {
        $this->componentCode3 = $value;
    }

    /**
     * Sets the componentCode30 property value. The componentCode30 property
     * @param string|null $value Value to set for the componentCode30 property.
    */
    public function setComponentCode30(?string $value): void {
        $this->componentCode30 = $value;
    }

    /**
     * Sets the componentCode31 property value. The componentCode31 property
     * @param string|null $value Value to set for the componentCode31 property.
    */
    public function setComponentCode31(?string $value): void {
        $this->componentCode31 = $value;
    }

    /**
     * Sets the componentCode32 property value. The componentCode32 property
     * @param string|null $value Value to set for the componentCode32 property.
    */
    public function setComponentCode32(?string $value): void {
        $this->componentCode32 = $value;
    }

    /**
     * Sets the componentCode33 property value. The componentCode33 property
     * @param string|null $value Value to set for the componentCode33 property.
    */
    public function setComponentCode33(?string $value): void {
        $this->componentCode33 = $value;
    }

    /**
     * Sets the componentCode34 property value. The componentCode34 property
     * @param string|null $value Value to set for the componentCode34 property.
    */
    public function setComponentCode34(?string $value): void {
        $this->componentCode34 = $value;
    }

    /**
     * Sets the componentCode35 property value. The componentCode35 property
     * @param string|null $value Value to set for the componentCode35 property.
    */
    public function setComponentCode35(?string $value): void {
        $this->componentCode35 = $value;
    }

    /**
     * Sets the componentCode36 property value. The componentCode36 property
     * @param string|null $value Value to set for the componentCode36 property.
    */
    public function setComponentCode36(?string $value): void {
        $this->componentCode36 = $value;
    }

    /**
     * Sets the componentCode37 property value. The componentCode37 property
     * @param string|null $value Value to set for the componentCode37 property.
    */
    public function setComponentCode37(?string $value): void {
        $this->componentCode37 = $value;
    }

    /**
     * Sets the componentCode38 property value. The componentCode38 property
     * @param string|null $value Value to set for the componentCode38 property.
    */
    public function setComponentCode38(?string $value): void {
        $this->componentCode38 = $value;
    }

    /**
     * Sets the componentCode39 property value. The componentCode39 property
     * @param string|null $value Value to set for the componentCode39 property.
    */
    public function setComponentCode39(?string $value): void {
        $this->componentCode39 = $value;
    }

    /**
     * Sets the componentCode4 property value. The componentCode4 property
     * @param string|null $value Value to set for the componentCode4 property.
    */
    public function setComponentCode4(?string $value): void {
        $this->componentCode4 = $value;
    }

    /**
     * Sets the componentCode40 property value. The componentCode40 property
     * @param string|null $value Value to set for the componentCode40 property.
    */
    public function setComponentCode40(?string $value): void {
        $this->componentCode40 = $value;
    }

    /**
     * Sets the componentCode41 property value. The componentCode41 property
     * @param string|null $value Value to set for the componentCode41 property.
    */
    public function setComponentCode41(?string $value): void {
        $this->componentCode41 = $value;
    }

    /**
     * Sets the componentCode42 property value. The componentCode42 property
     * @param string|null $value Value to set for the componentCode42 property.
    */
    public function setComponentCode42(?string $value): void {
        $this->componentCode42 = $value;
    }

    /**
     * Sets the componentCode43 property value. The componentCode43 property
     * @param string|null $value Value to set for the componentCode43 property.
    */
    public function setComponentCode43(?string $value): void {
        $this->componentCode43 = $value;
    }

    /**
     * Sets the componentCode44 property value. The componentCode44 property
     * @param string|null $value Value to set for the componentCode44 property.
    */
    public function setComponentCode44(?string $value): void {
        $this->componentCode44 = $value;
    }

    /**
     * Sets the componentCode45 property value. The componentCode45 property
     * @param string|null $value Value to set for the componentCode45 property.
    */
    public function setComponentCode45(?string $value): void {
        $this->componentCode45 = $value;
    }

    /**
     * Sets the componentCode46 property value. The componentCode46 property
     * @param string|null $value Value to set for the componentCode46 property.
    */
    public function setComponentCode46(?string $value): void {
        $this->componentCode46 = $value;
    }

    /**
     * Sets the componentCode47 property value. The componentCode47 property
     * @param string|null $value Value to set for the componentCode47 property.
    */
    public function setComponentCode47(?string $value): void {
        $this->componentCode47 = $value;
    }

    /**
     * Sets the componentCode48 property value. The componentCode48 property
     * @param string|null $value Value to set for the componentCode48 property.
    */
    public function setComponentCode48(?string $value): void {
        $this->componentCode48 = $value;
    }

    /**
     * Sets the componentCode49 property value. The componentCode49 property
     * @param string|null $value Value to set for the componentCode49 property.
    */
    public function setComponentCode49(?string $value): void {
        $this->componentCode49 = $value;
    }

    /**
     * Sets the componentCode5 property value. The componentCode5 property
     * @param string|null $value Value to set for the componentCode5 property.
    */
    public function setComponentCode5(?string $value): void {
        $this->componentCode5 = $value;
    }

    /**
     * Sets the componentCode50 property value. The componentCode50 property
     * @param string|null $value Value to set for the componentCode50 property.
    */
    public function setComponentCode50(?string $value): void {
        $this->componentCode50 = $value;
    }

    /**
     * Sets the componentCode6 property value. The componentCode6 property
     * @param string|null $value Value to set for the componentCode6 property.
    */
    public function setComponentCode6(?string $value): void {
        $this->componentCode6 = $value;
    }

    /**
     * Sets the componentCode7 property value. The componentCode7 property
     * @param string|null $value Value to set for the componentCode7 property.
    */
    public function setComponentCode7(?string $value): void {
        $this->componentCode7 = $value;
    }

    /**
     * Sets the componentCode8 property value. The componentCode8 property
     * @param string|null $value Value to set for the componentCode8 property.
    */
    public function setComponentCode8(?string $value): void {
        $this->componentCode8 = $value;
    }

    /**
     * Sets the componentCode9 property value. The componentCode9 property
     * @param string|null $value Value to set for the componentCode9 property.
    */
    public function setComponentCode9(?string $value): void {
        $this->componentCode9 = $value;
    }

    /**
     * Sets the componentQty1 property value. The componentQty1 property
     * @param float|null $value Value to set for the componentQty1 property.
    */
    public function setComponentQty1(?float $value): void {
        $this->componentQty1 = $value;
    }

    /**
     * Sets the componentQty10 property value. The componentQty10 property
     * @param float|null $value Value to set for the componentQty10 property.
    */
    public function setComponentQty10(?float $value): void {
        $this->componentQty10 = $value;
    }

    /**
     * Sets the componentQty11 property value. The componentQty11 property
     * @param float|null $value Value to set for the componentQty11 property.
    */
    public function setComponentQty11(?float $value): void {
        $this->componentQty11 = $value;
    }

    /**
     * Sets the componentQty12 property value. The componentQty12 property
     * @param float|null $value Value to set for the componentQty12 property.
    */
    public function setComponentQty12(?float $value): void {
        $this->componentQty12 = $value;
    }

    /**
     * Sets the componentQty13 property value. The componentQty13 property
     * @param float|null $value Value to set for the componentQty13 property.
    */
    public function setComponentQty13(?float $value): void {
        $this->componentQty13 = $value;
    }

    /**
     * Sets the componentQty14 property value. The componentQty14 property
     * @param float|null $value Value to set for the componentQty14 property.
    */
    public function setComponentQty14(?float $value): void {
        $this->componentQty14 = $value;
    }

    /**
     * Sets the componentQty15 property value. The componentQty15 property
     * @param float|null $value Value to set for the componentQty15 property.
    */
    public function setComponentQty15(?float $value): void {
        $this->componentQty15 = $value;
    }

    /**
     * Sets the componentQty16 property value. The componentQty16 property
     * @param float|null $value Value to set for the componentQty16 property.
    */
    public function setComponentQty16(?float $value): void {
        $this->componentQty16 = $value;
    }

    /**
     * Sets the componentQty17 property value. The componentQty17 property
     * @param float|null $value Value to set for the componentQty17 property.
    */
    public function setComponentQty17(?float $value): void {
        $this->componentQty17 = $value;
    }

    /**
     * Sets the componentQty18 property value. The componentQty18 property
     * @param float|null $value Value to set for the componentQty18 property.
    */
    public function setComponentQty18(?float $value): void {
        $this->componentQty18 = $value;
    }

    /**
     * Sets the componentQty19 property value. The componentQty19 property
     * @param float|null $value Value to set for the componentQty19 property.
    */
    public function setComponentQty19(?float $value): void {
        $this->componentQty19 = $value;
    }

    /**
     * Sets the componentQty2 property value. The componentQty2 property
     * @param float|null $value Value to set for the componentQty2 property.
    */
    public function setComponentQty2(?float $value): void {
        $this->componentQty2 = $value;
    }

    /**
     * Sets the componentQty20 property value. The componentQty20 property
     * @param float|null $value Value to set for the componentQty20 property.
    */
    public function setComponentQty20(?float $value): void {
        $this->componentQty20 = $value;
    }

    /**
     * Sets the componentQty21 property value. The componentQty21 property
     * @param float|null $value Value to set for the componentQty21 property.
    */
    public function setComponentQty21(?float $value): void {
        $this->componentQty21 = $value;
    }

    /**
     * Sets the componentQty22 property value. The componentQty22 property
     * @param float|null $value Value to set for the componentQty22 property.
    */
    public function setComponentQty22(?float $value): void {
        $this->componentQty22 = $value;
    }

    /**
     * Sets the componentQty23 property value. The componentQty23 property
     * @param float|null $value Value to set for the componentQty23 property.
    */
    public function setComponentQty23(?float $value): void {
        $this->componentQty23 = $value;
    }

    /**
     * Sets the componentQty24 property value. The componentQty24 property
     * @param float|null $value Value to set for the componentQty24 property.
    */
    public function setComponentQty24(?float $value): void {
        $this->componentQty24 = $value;
    }

    /**
     * Sets the componentQty25 property value. The componentQty25 property
     * @param float|null $value Value to set for the componentQty25 property.
    */
    public function setComponentQty25(?float $value): void {
        $this->componentQty25 = $value;
    }

    /**
     * Sets the componentQty26 property value. The componentQty26 property
     * @param float|null $value Value to set for the componentQty26 property.
    */
    public function setComponentQty26(?float $value): void {
        $this->componentQty26 = $value;
    }

    /**
     * Sets the componentQty27 property value. The componentQty27 property
     * @param float|null $value Value to set for the componentQty27 property.
    */
    public function setComponentQty27(?float $value): void {
        $this->componentQty27 = $value;
    }

    /**
     * Sets the componentQty28 property value. The componentQty28 property
     * @param float|null $value Value to set for the componentQty28 property.
    */
    public function setComponentQty28(?float $value): void {
        $this->componentQty28 = $value;
    }

    /**
     * Sets the componentQty29 property value. The componentQty29 property
     * @param float|null $value Value to set for the componentQty29 property.
    */
    public function setComponentQty29(?float $value): void {
        $this->componentQty29 = $value;
    }

    /**
     * Sets the componentQty3 property value. The componentQty3 property
     * @param float|null $value Value to set for the componentQty3 property.
    */
    public function setComponentQty3(?float $value): void {
        $this->componentQty3 = $value;
    }

    /**
     * Sets the componentQty30 property value. The componentQty30 property
     * @param float|null $value Value to set for the componentQty30 property.
    */
    public function setComponentQty30(?float $value): void {
        $this->componentQty30 = $value;
    }

    /**
     * Sets the componentQty31 property value. The componentQty31 property
     * @param float|null $value Value to set for the componentQty31 property.
    */
    public function setComponentQty31(?float $value): void {
        $this->componentQty31 = $value;
    }

    /**
     * Sets the componentQty32 property value. The componentQty32 property
     * @param float|null $value Value to set for the componentQty32 property.
    */
    public function setComponentQty32(?float $value): void {
        $this->componentQty32 = $value;
    }

    /**
     * Sets the componentQty33 property value. The componentQty33 property
     * @param float|null $value Value to set for the componentQty33 property.
    */
    public function setComponentQty33(?float $value): void {
        $this->componentQty33 = $value;
    }

    /**
     * Sets the componentQty34 property value. The componentQty34 property
     * @param float|null $value Value to set for the componentQty34 property.
    */
    public function setComponentQty34(?float $value): void {
        $this->componentQty34 = $value;
    }

    /**
     * Sets the componentQty35 property value. The componentQty35 property
     * @param float|null $value Value to set for the componentQty35 property.
    */
    public function setComponentQty35(?float $value): void {
        $this->componentQty35 = $value;
    }

    /**
     * Sets the componentQty36 property value. The componentQty36 property
     * @param float|null $value Value to set for the componentQty36 property.
    */
    public function setComponentQty36(?float $value): void {
        $this->componentQty36 = $value;
    }

    /**
     * Sets the componentQty37 property value. The componentQty37 property
     * @param float|null $value Value to set for the componentQty37 property.
    */
    public function setComponentQty37(?float $value): void {
        $this->componentQty37 = $value;
    }

    /**
     * Sets the componentQty38 property value. The componentQty38 property
     * @param float|null $value Value to set for the componentQty38 property.
    */
    public function setComponentQty38(?float $value): void {
        $this->componentQty38 = $value;
    }

    /**
     * Sets the componentQty39 property value. The componentQty39 property
     * @param float|null $value Value to set for the componentQty39 property.
    */
    public function setComponentQty39(?float $value): void {
        $this->componentQty39 = $value;
    }

    /**
     * Sets the componentQty4 property value. The componentQty4 property
     * @param float|null $value Value to set for the componentQty4 property.
    */
    public function setComponentQty4(?float $value): void {
        $this->componentQty4 = $value;
    }

    /**
     * Sets the componentQty40 property value. The componentQty40 property
     * @param float|null $value Value to set for the componentQty40 property.
    */
    public function setComponentQty40(?float $value): void {
        $this->componentQty40 = $value;
    }

    /**
     * Sets the componentQty41 property value. The componentQty41 property
     * @param float|null $value Value to set for the componentQty41 property.
    */
    public function setComponentQty41(?float $value): void {
        $this->componentQty41 = $value;
    }

    /**
     * Sets the componentQty42 property value. The componentQty42 property
     * @param float|null $value Value to set for the componentQty42 property.
    */
    public function setComponentQty42(?float $value): void {
        $this->componentQty42 = $value;
    }

    /**
     * Sets the componentQty43 property value. The componentQty43 property
     * @param float|null $value Value to set for the componentQty43 property.
    */
    public function setComponentQty43(?float $value): void {
        $this->componentQty43 = $value;
    }

    /**
     * Sets the componentQty44 property value. The componentQty44 property
     * @param float|null $value Value to set for the componentQty44 property.
    */
    public function setComponentQty44(?float $value): void {
        $this->componentQty44 = $value;
    }

    /**
     * Sets the componentQty45 property value. The componentQty45 property
     * @param float|null $value Value to set for the componentQty45 property.
    */
    public function setComponentQty45(?float $value): void {
        $this->componentQty45 = $value;
    }

    /**
     * Sets the componentQty46 property value. The componentQty46 property
     * @param float|null $value Value to set for the componentQty46 property.
    */
    public function setComponentQty46(?float $value): void {
        $this->componentQty46 = $value;
    }

    /**
     * Sets the componentQty47 property value. The componentQty47 property
     * @param float|null $value Value to set for the componentQty47 property.
    */
    public function setComponentQty47(?float $value): void {
        $this->componentQty47 = $value;
    }

    /**
     * Sets the componentQty48 property value. The componentQty48 property
     * @param float|null $value Value to set for the componentQty48 property.
    */
    public function setComponentQty48(?float $value): void {
        $this->componentQty48 = $value;
    }

    /**
     * Sets the componentQty49 property value. The componentQty49 property
     * @param float|null $value Value to set for the componentQty49 property.
    */
    public function setComponentQty49(?float $value): void {
        $this->componentQty49 = $value;
    }

    /**
     * Sets the componentQty5 property value. The componentQty5 property
     * @param float|null $value Value to set for the componentQty5 property.
    */
    public function setComponentQty5(?float $value): void {
        $this->componentQty5 = $value;
    }

    /**
     * Sets the componentQty50 property value. The componentQty50 property
     * @param float|null $value Value to set for the componentQty50 property.
    */
    public function setComponentQty50(?float $value): void {
        $this->componentQty50 = $value;
    }

    /**
     * Sets the componentQty6 property value. The componentQty6 property
     * @param float|null $value Value to set for the componentQty6 property.
    */
    public function setComponentQty6(?float $value): void {
        $this->componentQty6 = $value;
    }

    /**
     * Sets the componentQty7 property value. The componentQty7 property
     * @param float|null $value Value to set for the componentQty7 property.
    */
    public function setComponentQty7(?float $value): void {
        $this->componentQty7 = $value;
    }

    /**
     * Sets the componentQty8 property value. The componentQty8 property
     * @param float|null $value Value to set for the componentQty8 property.
    */
    public function setComponentQty8(?float $value): void {
        $this->componentQty8 = $value;
    }

    /**
     * Sets the componentQty9 property value. The componentQty9 property
     * @param float|null $value Value to set for the componentQty9 property.
    */
    public function setComponentQty9(?float $value): void {
        $this->componentQty9 = $value;
    }

    /**
     * Sets the costBf property value. The costBf property
     * @param float|null $value Value to set for the costBf property.
    */
    public function setCostBf(?float $value): void {
        $this->costBf = $value;
    }

    /**
     * Sets the costFuture property value. The costFuture property
     * @param float|null $value Value to set for the costFuture property.
    */
    public function setCostFuture(?float $value): void {
        $this->costFuture = $value;
    }

    /**
     * Sets the costMth1 property value. The costMth1 property
     * @param float|null $value Value to set for the costMth1 property.
    */
    public function setCostMth1(?float $value): void {
        $this->costMth1 = $value;
    }

    /**
     * Sets the costMth10 property value. The costMth10 property
     * @param float|null $value Value to set for the costMth10 property.
    */
    public function setCostMth10(?float $value): void {
        $this->costMth10 = $value;
    }

    /**
     * Sets the costMth11 property value. The costMth11 property
     * @param float|null $value Value to set for the costMth11 property.
    */
    public function setCostMth11(?float $value): void {
        $this->costMth11 = $value;
    }

    /**
     * Sets the costMth12 property value. The costMth12 property
     * @param float|null $value Value to set for the costMth12 property.
    */
    public function setCostMth12(?float $value): void {
        $this->costMth12 = $value;
    }

    /**
     * Sets the costMth2 property value. The costMth2 property
     * @param float|null $value Value to set for the costMth2 property.
    */
    public function setCostMth2(?float $value): void {
        $this->costMth2 = $value;
    }

    /**
     * Sets the costMth3 property value. The costMth3 property
     * @param float|null $value Value to set for the costMth3 property.
    */
    public function setCostMth3(?float $value): void {
        $this->costMth3 = $value;
    }

    /**
     * Sets the costMth4 property value. The costMth4 property
     * @param float|null $value Value to set for the costMth4 property.
    */
    public function setCostMth4(?float $value): void {
        $this->costMth4 = $value;
    }

    /**
     * Sets the costMth5 property value. The costMth5 property
     * @param float|null $value Value to set for the costMth5 property.
    */
    public function setCostMth5(?float $value): void {
        $this->costMth5 = $value;
    }

    /**
     * Sets the costMth6 property value. The costMth6 property
     * @param float|null $value Value to set for the costMth6 property.
    */
    public function setCostMth6(?float $value): void {
        $this->costMth6 = $value;
    }

    /**
     * Sets the costMth7 property value. The costMth7 property
     * @param float|null $value Value to set for the costMth7 property.
    */
    public function setCostMth7(?float $value): void {
        $this->costMth7 = $value;
    }

    /**
     * Sets the costMth8 property value. The costMth8 property
     * @param float|null $value Value to set for the costMth8 property.
    */
    public function setCostMth8(?float $value): void {
        $this->costMth8 = $value;
    }

    /**
     * Sets the costMth9 property value. The costMth9 property
     * @param float|null $value Value to set for the costMth9 property.
    */
    public function setCostMth9(?float $value): void {
        $this->costMth9 = $value;
    }

    /**
     * Sets the countryCodeOfOrigin property value. The countryCodeOfOrigin property
     * @param string|null $value Value to set for the countryCodeOfOrigin property.
    */
    public function setCountryCodeOfOrigin(?string $value): void {
        $this->countryCodeOfOrigin = $value;
    }

    /**
     * Sets the deptName property value. The deptName property
     * @param string|null $value Value to set for the deptName property.
    */
    public function setDeptName(?string $value): void {
        $this->deptName = $value;
    }

    /**
     * Sets the deptNumber property value. The deptNumber property
     * @param int|null $value Value to set for the deptNumber property.
    */
    public function setDeptNumber(?int $value): void {
        $this->deptNumber = $value;
    }

    /**
     * Sets the description property value. The description property
     * @param string|null $value Value to set for the description property.
    */
    public function setDescription(?string $value): void {
        $this->description = $value;
    }

    /**
     * Sets the discALevel10Qty property value. The discALevel10Qty property
     * @param float|null $value Value to set for the discALevel10Qty property.
    */
    public function setDiscALevel10Qty(?float $value): void {
        $this->discALevel10Qty = $value;
    }

    /**
     * Sets the discALevel10Rate property value. The discALevel10Rate property
     * @param float|null $value Value to set for the discALevel10Rate property.
    */
    public function setDiscALevel10Rate(?float $value): void {
        $this->discALevel10Rate = $value;
    }

    /**
     * Sets the discALevel1Qty property value. The discALevel1Qty property
     * @param float|null $value Value to set for the discALevel1Qty property.
    */
    public function setDiscALevel1Qty(?float $value): void {
        $this->discALevel1Qty = $value;
    }

    /**
     * Sets the discALevel1Rate property value. The discALevel1Rate property
     * @param float|null $value Value to set for the discALevel1Rate property.
    */
    public function setDiscALevel1Rate(?float $value): void {
        $this->discALevel1Rate = $value;
    }

    /**
     * Sets the discALevel2Qty property value. The discALevel2Qty property
     * @param float|null $value Value to set for the discALevel2Qty property.
    */
    public function setDiscALevel2Qty(?float $value): void {
        $this->discALevel2Qty = $value;
    }

    /**
     * Sets the discALevel2Rate property value. The discALevel2Rate property
     * @param float|null $value Value to set for the discALevel2Rate property.
    */
    public function setDiscALevel2Rate(?float $value): void {
        $this->discALevel2Rate = $value;
    }

    /**
     * Sets the discALevel3Qty property value. The discALevel3Qty property
     * @param float|null $value Value to set for the discALevel3Qty property.
    */
    public function setDiscALevel3Qty(?float $value): void {
        $this->discALevel3Qty = $value;
    }

    /**
     * Sets the discALevel3Rate property value. The discALevel3Rate property
     * @param float|null $value Value to set for the discALevel3Rate property.
    */
    public function setDiscALevel3Rate(?float $value): void {
        $this->discALevel3Rate = $value;
    }

    /**
     * Sets the discALevel4Qty property value. The discALevel4Qty property
     * @param float|null $value Value to set for the discALevel4Qty property.
    */
    public function setDiscALevel4Qty(?float $value): void {
        $this->discALevel4Qty = $value;
    }

    /**
     * Sets the discALevel4Rate property value. The discALevel4Rate property
     * @param float|null $value Value to set for the discALevel4Rate property.
    */
    public function setDiscALevel4Rate(?float $value): void {
        $this->discALevel4Rate = $value;
    }

    /**
     * Sets the discALevel5Qty property value. The discALevel5Qty property
     * @param float|null $value Value to set for the discALevel5Qty property.
    */
    public function setDiscALevel5Qty(?float $value): void {
        $this->discALevel5Qty = $value;
    }

    /**
     * Sets the discALevel5Rate property value. The discALevel5Rate property
     * @param float|null $value Value to set for the discALevel5Rate property.
    */
    public function setDiscALevel5Rate(?float $value): void {
        $this->discALevel5Rate = $value;
    }

    /**
     * Sets the discALevel6Qty property value. The discALevel6Qty property
     * @param float|null $value Value to set for the discALevel6Qty property.
    */
    public function setDiscALevel6Qty(?float $value): void {
        $this->discALevel6Qty = $value;
    }

    /**
     * Sets the discALevel6Rate property value. The discALevel6Rate property
     * @param float|null $value Value to set for the discALevel6Rate property.
    */
    public function setDiscALevel6Rate(?float $value): void {
        $this->discALevel6Rate = $value;
    }

    /**
     * Sets the discALevel7Qty property value. The discALevel7Qty property
     * @param float|null $value Value to set for the discALevel7Qty property.
    */
    public function setDiscALevel7Qty(?float $value): void {
        $this->discALevel7Qty = $value;
    }

    /**
     * Sets the discALevel7Rate property value. The discALevel7Rate property
     * @param float|null $value Value to set for the discALevel7Rate property.
    */
    public function setDiscALevel7Rate(?float $value): void {
        $this->discALevel7Rate = $value;
    }

    /**
     * Sets the discALevel8Qty property value. The discALevel8Qty property
     * @param float|null $value Value to set for the discALevel8Qty property.
    */
    public function setDiscALevel8Qty(?float $value): void {
        $this->discALevel8Qty = $value;
    }

    /**
     * Sets the discALevel8Rate property value. The discALevel8Rate property
     * @param float|null $value Value to set for the discALevel8Rate property.
    */
    public function setDiscALevel8Rate(?float $value): void {
        $this->discALevel8Rate = $value;
    }

    /**
     * Sets the discALevel9Qty property value. The discALevel9Qty property
     * @param float|null $value Value to set for the discALevel9Qty property.
    */
    public function setDiscALevel9Qty(?float $value): void {
        $this->discALevel9Qty = $value;
    }

    /**
     * Sets the discALevel9Rate property value. The discALevel9Rate property
     * @param float|null $value Value to set for the discALevel9Rate property.
    */
    public function setDiscALevel9Rate(?float $value): void {
        $this->discALevel9Rate = $value;
    }

    /**
     * Sets the discBLevel10Qty property value. The discBLevel10Qty property
     * @param float|null $value Value to set for the discBLevel10Qty property.
    */
    public function setDiscBLevel10Qty(?float $value): void {
        $this->discBLevel10Qty = $value;
    }

    /**
     * Sets the discBLevel10Rate property value. The discBLevel10Rate property
     * @param float|null $value Value to set for the discBLevel10Rate property.
    */
    public function setDiscBLevel10Rate(?float $value): void {
        $this->discBLevel10Rate = $value;
    }

    /**
     * Sets the discBLevel1Qty property value. The discBLevel1Qty property
     * @param float|null $value Value to set for the discBLevel1Qty property.
    */
    public function setDiscBLevel1Qty(?float $value): void {
        $this->discBLevel1Qty = $value;
    }

    /**
     * Sets the discBLevel1Rate property value. The discBLevel1Rate property
     * @param float|null $value Value to set for the discBLevel1Rate property.
    */
    public function setDiscBLevel1Rate(?float $value): void {
        $this->discBLevel1Rate = $value;
    }

    /**
     * Sets the discBLevel2Qty property value. The discBLevel2Qty property
     * @param float|null $value Value to set for the discBLevel2Qty property.
    */
    public function setDiscBLevel2Qty(?float $value): void {
        $this->discBLevel2Qty = $value;
    }

    /**
     * Sets the discBLevel2Rate property value. The discBLevel2Rate property
     * @param float|null $value Value to set for the discBLevel2Rate property.
    */
    public function setDiscBLevel2Rate(?float $value): void {
        $this->discBLevel2Rate = $value;
    }

    /**
     * Sets the discBLevel3Qty property value. The discBLevel3Qty property
     * @param float|null $value Value to set for the discBLevel3Qty property.
    */
    public function setDiscBLevel3Qty(?float $value): void {
        $this->discBLevel3Qty = $value;
    }

    /**
     * Sets the discBLevel3Rate property value. The discBLevel3Rate property
     * @param float|null $value Value to set for the discBLevel3Rate property.
    */
    public function setDiscBLevel3Rate(?float $value): void {
        $this->discBLevel3Rate = $value;
    }

    /**
     * Sets the discBLevel4Qty property value. The discBLevel4Qty property
     * @param float|null $value Value to set for the discBLevel4Qty property.
    */
    public function setDiscBLevel4Qty(?float $value): void {
        $this->discBLevel4Qty = $value;
    }

    /**
     * Sets the discBLevel4Rate property value. The discBLevel4Rate property
     * @param float|null $value Value to set for the discBLevel4Rate property.
    */
    public function setDiscBLevel4Rate(?float $value): void {
        $this->discBLevel4Rate = $value;
    }

    /**
     * Sets the discBLevel5Qty property value. The discBLevel5Qty property
     * @param float|null $value Value to set for the discBLevel5Qty property.
    */
    public function setDiscBLevel5Qty(?float $value): void {
        $this->discBLevel5Qty = $value;
    }

    /**
     * Sets the discBLevel5Rate property value. The discBLevel5Rate property
     * @param float|null $value Value to set for the discBLevel5Rate property.
    */
    public function setDiscBLevel5Rate(?float $value): void {
        $this->discBLevel5Rate = $value;
    }

    /**
     * Sets the discBLevel6Qty property value. The discBLevel6Qty property
     * @param float|null $value Value to set for the discBLevel6Qty property.
    */
    public function setDiscBLevel6Qty(?float $value): void {
        $this->discBLevel6Qty = $value;
    }

    /**
     * Sets the discBLevel6Rate property value. The discBLevel6Rate property
     * @param float|null $value Value to set for the discBLevel6Rate property.
    */
    public function setDiscBLevel6Rate(?float $value): void {
        $this->discBLevel6Rate = $value;
    }

    /**
     * Sets the discBLevel7Qty property value. The discBLevel7Qty property
     * @param float|null $value Value to set for the discBLevel7Qty property.
    */
    public function setDiscBLevel7Qty(?float $value): void {
        $this->discBLevel7Qty = $value;
    }

    /**
     * Sets the discBLevel7Rate property value. The discBLevel7Rate property
     * @param float|null $value Value to set for the discBLevel7Rate property.
    */
    public function setDiscBLevel7Rate(?float $value): void {
        $this->discBLevel7Rate = $value;
    }

    /**
     * Sets the discBLevel8Qty property value. The discBLevel8Qty property
     * @param float|null $value Value to set for the discBLevel8Qty property.
    */
    public function setDiscBLevel8Qty(?float $value): void {
        $this->discBLevel8Qty = $value;
    }

    /**
     * Sets the discBLevel8Rate property value. The discBLevel8Rate property
     * @param float|null $value Value to set for the discBLevel8Rate property.
    */
    public function setDiscBLevel8Rate(?float $value): void {
        $this->discBLevel8Rate = $value;
    }

    /**
     * Sets the discBLevel9Qty property value. The discBLevel9Qty property
     * @param float|null $value Value to set for the discBLevel9Qty property.
    */
    public function setDiscBLevel9Qty(?float $value): void {
        $this->discBLevel9Qty = $value;
    }

    /**
     * Sets the discBLevel9Rate property value. The discBLevel9Rate property
     * @param float|null $value Value to set for the discBLevel9Rate property.
    */
    public function setDiscBLevel9Rate(?float $value): void {
        $this->discBLevel9Rate = $value;
    }

    /**
     * Sets the discCLevel10Qty property value. The discCLevel10Qty property
     * @param float|null $value Value to set for the discCLevel10Qty property.
    */
    public function setDiscCLevel10Qty(?float $value): void {
        $this->discCLevel10Qty = $value;
    }

    /**
     * Sets the discCLevel10Rate property value. The discCLevel10Rate property
     * @param float|null $value Value to set for the discCLevel10Rate property.
    */
    public function setDiscCLevel10Rate(?float $value): void {
        $this->discCLevel10Rate = $value;
    }

    /**
     * Sets the discCLevel1Qty property value. The discCLevel1Qty property
     * @param float|null $value Value to set for the discCLevel1Qty property.
    */
    public function setDiscCLevel1Qty(?float $value): void {
        $this->discCLevel1Qty = $value;
    }

    /**
     * Sets the discCLevel1Rate property value. The discCLevel1Rate property
     * @param float|null $value Value to set for the discCLevel1Rate property.
    */
    public function setDiscCLevel1Rate(?float $value): void {
        $this->discCLevel1Rate = $value;
    }

    /**
     * Sets the discCLevel2Qty property value. The discCLevel2Qty property
     * @param float|null $value Value to set for the discCLevel2Qty property.
    */
    public function setDiscCLevel2Qty(?float $value): void {
        $this->discCLevel2Qty = $value;
    }

    /**
     * Sets the discCLevel2Rate property value. The discCLevel2Rate property
     * @param float|null $value Value to set for the discCLevel2Rate property.
    */
    public function setDiscCLevel2Rate(?float $value): void {
        $this->discCLevel2Rate = $value;
    }

    /**
     * Sets the discCLevel3Qty property value. The discCLevel3Qty property
     * @param float|null $value Value to set for the discCLevel3Qty property.
    */
    public function setDiscCLevel3Qty(?float $value): void {
        $this->discCLevel3Qty = $value;
    }

    /**
     * Sets the discCLevel3Rate property value. The discCLevel3Rate property
     * @param float|null $value Value to set for the discCLevel3Rate property.
    */
    public function setDiscCLevel3Rate(?float $value): void {
        $this->discCLevel3Rate = $value;
    }

    /**
     * Sets the discCLevel4Qty property value. The discCLevel4Qty property
     * @param float|null $value Value to set for the discCLevel4Qty property.
    */
    public function setDiscCLevel4Qty(?float $value): void {
        $this->discCLevel4Qty = $value;
    }

    /**
     * Sets the discCLevel4Rate property value. The discCLevel4Rate property
     * @param float|null $value Value to set for the discCLevel4Rate property.
    */
    public function setDiscCLevel4Rate(?float $value): void {
        $this->discCLevel4Rate = $value;
    }

    /**
     * Sets the discCLevel5Qty property value. The discCLevel5Qty property
     * @param float|null $value Value to set for the discCLevel5Qty property.
    */
    public function setDiscCLevel5Qty(?float $value): void {
        $this->discCLevel5Qty = $value;
    }

    /**
     * Sets the discCLevel5Rate property value. The discCLevel5Rate property
     * @param float|null $value Value to set for the discCLevel5Rate property.
    */
    public function setDiscCLevel5Rate(?float $value): void {
        $this->discCLevel5Rate = $value;
    }

    /**
     * Sets the discCLevel6Qty property value. The discCLevel6Qty property
     * @param float|null $value Value to set for the discCLevel6Qty property.
    */
    public function setDiscCLevel6Qty(?float $value): void {
        $this->discCLevel6Qty = $value;
    }

    /**
     * Sets the discCLevel6Rate property value. The discCLevel6Rate property
     * @param float|null $value Value to set for the discCLevel6Rate property.
    */
    public function setDiscCLevel6Rate(?float $value): void {
        $this->discCLevel6Rate = $value;
    }

    /**
     * Sets the discCLevel7Qty property value. The discCLevel7Qty property
     * @param float|null $value Value to set for the discCLevel7Qty property.
    */
    public function setDiscCLevel7Qty(?float $value): void {
        $this->discCLevel7Qty = $value;
    }

    /**
     * Sets the discCLevel7Rate property value. The discCLevel7Rate property
     * @param float|null $value Value to set for the discCLevel7Rate property.
    */
    public function setDiscCLevel7Rate(?float $value): void {
        $this->discCLevel7Rate = $value;
    }

    /**
     * Sets the discCLevel8Qty property value. The discCLevel8Qty property
     * @param float|null $value Value to set for the discCLevel8Qty property.
    */
    public function setDiscCLevel8Qty(?float $value): void {
        $this->discCLevel8Qty = $value;
    }

    /**
     * Sets the discCLevel8Rate property value. The discCLevel8Rate property
     * @param float|null $value Value to set for the discCLevel8Rate property.
    */
    public function setDiscCLevel8Rate(?float $value): void {
        $this->discCLevel8Rate = $value;
    }

    /**
     * Sets the discCLevel9Qty property value. The discCLevel9Qty property
     * @param float|null $value Value to set for the discCLevel9Qty property.
    */
    public function setDiscCLevel9Qty(?float $value): void {
        $this->discCLevel9Qty = $value;
    }

    /**
     * Sets the discCLevel9Rate property value. The discCLevel9Rate property
     * @param float|null $value Value to set for the discCLevel9Rate property.
    */
    public function setDiscCLevel9Rate(?float $value): void {
        $this->discCLevel9Rate = $value;
    }

    /**
     * Sets the discDLevel10Qty property value. The discDLevel10Qty property
     * @param float|null $value Value to set for the discDLevel10Qty property.
    */
    public function setDiscDLevel10Qty(?float $value): void {
        $this->discDLevel10Qty = $value;
    }

    /**
     * Sets the discDLevel10Rate property value. The discDLevel10Rate property
     * @param float|null $value Value to set for the discDLevel10Rate property.
    */
    public function setDiscDLevel10Rate(?float $value): void {
        $this->discDLevel10Rate = $value;
    }

    /**
     * Sets the discDLevel1Qty property value. The discDLevel1Qty property
     * @param float|null $value Value to set for the discDLevel1Qty property.
    */
    public function setDiscDLevel1Qty(?float $value): void {
        $this->discDLevel1Qty = $value;
    }

    /**
     * Sets the discDLevel1Rate property value. The discDLevel1Rate property
     * @param float|null $value Value to set for the discDLevel1Rate property.
    */
    public function setDiscDLevel1Rate(?float $value): void {
        $this->discDLevel1Rate = $value;
    }

    /**
     * Sets the discDLevel2Qty property value. The discDLevel2Qty property
     * @param float|null $value Value to set for the discDLevel2Qty property.
    */
    public function setDiscDLevel2Qty(?float $value): void {
        $this->discDLevel2Qty = $value;
    }

    /**
     * Sets the discDLevel2Rate property value. The discDLevel2Rate property
     * @param float|null $value Value to set for the discDLevel2Rate property.
    */
    public function setDiscDLevel2Rate(?float $value): void {
        $this->discDLevel2Rate = $value;
    }

    /**
     * Sets the discDLevel3Qty property value. The discDLevel3Qty property
     * @param float|null $value Value to set for the discDLevel3Qty property.
    */
    public function setDiscDLevel3Qty(?float $value): void {
        $this->discDLevel3Qty = $value;
    }

    /**
     * Sets the discDLevel3Rate property value. The discDLevel3Rate property
     * @param float|null $value Value to set for the discDLevel3Rate property.
    */
    public function setDiscDLevel3Rate(?float $value): void {
        $this->discDLevel3Rate = $value;
    }

    /**
     * Sets the discDLevel4Qty property value. The discDLevel4Qty property
     * @param float|null $value Value to set for the discDLevel4Qty property.
    */
    public function setDiscDLevel4Qty(?float $value): void {
        $this->discDLevel4Qty = $value;
    }

    /**
     * Sets the discDLevel4Rate property value. The discDLevel4Rate property
     * @param float|null $value Value to set for the discDLevel4Rate property.
    */
    public function setDiscDLevel4Rate(?float $value): void {
        $this->discDLevel4Rate = $value;
    }

    /**
     * Sets the discDLevel5Qty property value. The discDLevel5Qty property
     * @param float|null $value Value to set for the discDLevel5Qty property.
    */
    public function setDiscDLevel5Qty(?float $value): void {
        $this->discDLevel5Qty = $value;
    }

    /**
     * Sets the discDLevel5Rate property value. The discDLevel5Rate property
     * @param float|null $value Value to set for the discDLevel5Rate property.
    */
    public function setDiscDLevel5Rate(?float $value): void {
        $this->discDLevel5Rate = $value;
    }

    /**
     * Sets the discDLevel6Qty property value. The discDLevel6Qty property
     * @param float|null $value Value to set for the discDLevel6Qty property.
    */
    public function setDiscDLevel6Qty(?float $value): void {
        $this->discDLevel6Qty = $value;
    }

    /**
     * Sets the discDLevel6Rate property value. The discDLevel6Rate property
     * @param float|null $value Value to set for the discDLevel6Rate property.
    */
    public function setDiscDLevel6Rate(?float $value): void {
        $this->discDLevel6Rate = $value;
    }

    /**
     * Sets the discDLevel7Qty property value. The discDLevel7Qty property
     * @param float|null $value Value to set for the discDLevel7Qty property.
    */
    public function setDiscDLevel7Qty(?float $value): void {
        $this->discDLevel7Qty = $value;
    }

    /**
     * Sets the discDLevel7Rate property value. The discDLevel7Rate property
     * @param float|null $value Value to set for the discDLevel7Rate property.
    */
    public function setDiscDLevel7Rate(?float $value): void {
        $this->discDLevel7Rate = $value;
    }

    /**
     * Sets the discDLevel8Qty property value. The discDLevel8Qty property
     * @param float|null $value Value to set for the discDLevel8Qty property.
    */
    public function setDiscDLevel8Qty(?float $value): void {
        $this->discDLevel8Qty = $value;
    }

    /**
     * Sets the discDLevel8Rate property value. The discDLevel8Rate property
     * @param float|null $value Value to set for the discDLevel8Rate property.
    */
    public function setDiscDLevel8Rate(?float $value): void {
        $this->discDLevel8Rate = $value;
    }

    /**
     * Sets the discDLevel9Qty property value. The discDLevel9Qty property
     * @param float|null $value Value to set for the discDLevel9Qty property.
    */
    public function setDiscDLevel9Qty(?float $value): void {
        $this->discDLevel9Qty = $value;
    }

    /**
     * Sets the discDLevel9Rate property value. The discDLevel9Rate property
     * @param float|null $value Value to set for the discDLevel9Rate property.
    */
    public function setDiscDLevel9Rate(?float $value): void {
        $this->discDLevel9Rate = $value;
    }

    /**
     * Sets the discELevel10Qty property value. The discELevel10Qty property
     * @param float|null $value Value to set for the discELevel10Qty property.
    */
    public function setDiscELevel10Qty(?float $value): void {
        $this->discELevel10Qty = $value;
    }

    /**
     * Sets the discELevel10Rate property value. The discELevel10Rate property
     * @param float|null $value Value to set for the discELevel10Rate property.
    */
    public function setDiscELevel10Rate(?float $value): void {
        $this->discELevel10Rate = $value;
    }

    /**
     * Sets the discELevel1Qty property value. The discELevel1Qty property
     * @param float|null $value Value to set for the discELevel1Qty property.
    */
    public function setDiscELevel1Qty(?float $value): void {
        $this->discELevel1Qty = $value;
    }

    /**
     * Sets the discELevel1Rate property value. The discELevel1Rate property
     * @param float|null $value Value to set for the discELevel1Rate property.
    */
    public function setDiscELevel1Rate(?float $value): void {
        $this->discELevel1Rate = $value;
    }

    /**
     * Sets the discELevel2Qty property value. The discELevel2Qty property
     * @param float|null $value Value to set for the discELevel2Qty property.
    */
    public function setDiscELevel2Qty(?float $value): void {
        $this->discELevel2Qty = $value;
    }

    /**
     * Sets the discELevel2Rate property value. The discELevel2Rate property
     * @param float|null $value Value to set for the discELevel2Rate property.
    */
    public function setDiscELevel2Rate(?float $value): void {
        $this->discELevel2Rate = $value;
    }

    /**
     * Sets the discELevel3Qty property value. The discELevel3Qty property
     * @param float|null $value Value to set for the discELevel3Qty property.
    */
    public function setDiscELevel3Qty(?float $value): void {
        $this->discELevel3Qty = $value;
    }

    /**
     * Sets the discELevel3Rate property value. The discELevel3Rate property
     * @param float|null $value Value to set for the discELevel3Rate property.
    */
    public function setDiscELevel3Rate(?float $value): void {
        $this->discELevel3Rate = $value;
    }

    /**
     * Sets the discELevel4Qty property value. The discELevel4Qty property
     * @param float|null $value Value to set for the discELevel4Qty property.
    */
    public function setDiscELevel4Qty(?float $value): void {
        $this->discELevel4Qty = $value;
    }

    /**
     * Sets the discELevel4Rate property value. The discELevel4Rate property
     * @param float|null $value Value to set for the discELevel4Rate property.
    */
    public function setDiscELevel4Rate(?float $value): void {
        $this->discELevel4Rate = $value;
    }

    /**
     * Sets the discELevel5Qty property value. The discELevel5Qty property
     * @param float|null $value Value to set for the discELevel5Qty property.
    */
    public function setDiscELevel5Qty(?float $value): void {
        $this->discELevel5Qty = $value;
    }

    /**
     * Sets the discELevel5Rate property value. The discELevel5Rate property
     * @param float|null $value Value to set for the discELevel5Rate property.
    */
    public function setDiscELevel5Rate(?float $value): void {
        $this->discELevel5Rate = $value;
    }

    /**
     * Sets the discELevel6Qty property value. The discELevel6Qty property
     * @param float|null $value Value to set for the discELevel6Qty property.
    */
    public function setDiscELevel6Qty(?float $value): void {
        $this->discELevel6Qty = $value;
    }

    /**
     * Sets the discELevel6Rate property value. The discELevel6Rate property
     * @param float|null $value Value to set for the discELevel6Rate property.
    */
    public function setDiscELevel6Rate(?float $value): void {
        $this->discELevel6Rate = $value;
    }

    /**
     * Sets the discELevel7Qty property value. The discELevel7Qty property
     * @param float|null $value Value to set for the discELevel7Qty property.
    */
    public function setDiscELevel7Qty(?float $value): void {
        $this->discELevel7Qty = $value;
    }

    /**
     * Sets the discELevel7Rate property value. The discELevel7Rate property
     * @param float|null $value Value to set for the discELevel7Rate property.
    */
    public function setDiscELevel7Rate(?float $value): void {
        $this->discELevel7Rate = $value;
    }

    /**
     * Sets the discELevel8Qty property value. The discELevel8Qty property
     * @param float|null $value Value to set for the discELevel8Qty property.
    */
    public function setDiscELevel8Qty(?float $value): void {
        $this->discELevel8Qty = $value;
    }

    /**
     * Sets the discELevel8Rate property value. The discELevel8Rate property
     * @param float|null $value Value to set for the discELevel8Rate property.
    */
    public function setDiscELevel8Rate(?float $value): void {
        $this->discELevel8Rate = $value;
    }

    /**
     * Sets the discELevel9Qty property value. The discELevel9Qty property
     * @param float|null $value Value to set for the discELevel9Qty property.
    */
    public function setDiscELevel9Qty(?float $value): void {
        $this->discELevel9Qty = $value;
    }

    /**
     * Sets the discELevel9Rate property value. The discELevel9Rate property
     * @param float|null $value Value to set for the discELevel9Rate property.
    */
    public function setDiscELevel9Rate(?float $value): void {
        $this->discELevel9Rate = $value;
    }

    /**
     * Sets the hasBom property value. The hasBom property
     * @param string|null $value Value to set for the hasBom property.
    */
    public function setHasBom(?string $value): void {
        $this->hasBom = $value;
    }

    /**
     * Sets the hasNoComponent property value. The hasNoComponent property
     * @param int|null $value Value to set for the hasNoComponent property.
    */
    public function setHasNoComponent(?int $value): void {
        $this->hasNoComponent = $value;
    }

    /**
     * Sets the ignoreStkLvlFlag property value. The ignoreStkLvlFlag property
     * @param int|null $value Value to set for the ignoreStkLvlFlag property.
    */
    public function setIgnoreStkLvlFlag(?int $value): void {
        $this->ignoreStkLvlFlag = $value;
    }

    /**
     * Sets the inactiveFlag property value. The inactiveFlag property
     * @param int|null $value Value to set for the inactiveFlag property.
    */
    public function setInactiveFlag(?int $value): void {
        $this->inactiveFlag = $value;
    }

    /**
     * Sets the intrastatCommCode property value. The intrastatCommCode property
     * @param string|null $value Value to set for the intrastatCommCode property.
    */
    public function setIntrastatCommCode(?string $value): void {
        $this->intrastatCommCode = $value;
    }

    /**
     * Sets the intrastatImportDutyCode property value. The intrastatImportDutyCode property
     * @param string|null $value Value to set for the intrastatImportDutyCode property.
    */
    public function setIntrastatImportDutyCode(?string $value): void {
        $this->intrastatImportDutyCode = $value;
    }

    /**
     * Sets the lastDiscPurchasePrice property value. The lastDiscPurchasePrice property
     * @param float|null $value Value to set for the lastDiscPurchasePrice property.
    */
    public function setLastDiscPurchasePrice(?float $value): void {
        $this->lastDiscPurchasePrice = $value;
    }

    /**
     * Sets the lastPurchaseDate property value. The lastPurchaseDate property
     * @param DateTime|null $value Value to set for the lastPurchaseDate property.
    */
    public function setLastPurchaseDate(?DateTime $value): void {
        $this->lastPurchaseDate = $value;
    }

    /**
     * Sets the lastPurchasePrice property value. The lastPurchasePrice property
     * @param float|null $value Value to set for the lastPurchasePrice property.
    */
    public function setLastPurchasePrice(?float $value): void {
        $this->lastPurchasePrice = $value;
    }

    /**
     * Sets the lastSaleDate property value. The lastSaleDate property
     * @param DateTime|null $value Value to set for the lastSaleDate property.
    */
    public function setLastSaleDate(?DateTime $value): void {
        $this->lastSaleDate = $value;
    }

    /**
     * Sets the linkLevel property value. The linkLevel property
     * @param int|null $value Value to set for the linkLevel property.
    */
    public function setLinkLevel(?int $value): void {
        $this->linkLevel = $value;
    }

    /**
     * Sets the location property value. The location property
     * @param string|null $value Value to set for the location property.
    */
    public function setLocation(?string $value): void {
        $this->location = $value;
    }

    /**
     * Sets the priorYrCostBf property value. The priorYrCostBf property
     * @param float|null $value Value to set for the priorYrCostBf property.
    */
    public function setPriorYrCostBf(?float $value): void {
        $this->priorYrCostBf = $value;
    }

    /**
     * Sets the priorYrCostFuture property value. The priorYrCostFuture property
     * @param float|null $value Value to set for the priorYrCostFuture property.
    */
    public function setPriorYrCostFuture(?float $value): void {
        $this->priorYrCostFuture = $value;
    }

    /**
     * Sets the priorYrCostMth1 property value. The priorYrCostMth1 property
     * @param float|null $value Value to set for the priorYrCostMth1 property.
    */
    public function setPriorYrCostMth1(?float $value): void {
        $this->priorYrCostMth1 = $value;
    }

    /**
     * Sets the priorYrCostMth10 property value. The priorYrCostMth10 property
     * @param float|null $value Value to set for the priorYrCostMth10 property.
    */
    public function setPriorYrCostMth10(?float $value): void {
        $this->priorYrCostMth10 = $value;
    }

    /**
     * Sets the priorYrCostMth11 property value. The priorYrCostMth11 property
     * @param float|null $value Value to set for the priorYrCostMth11 property.
    */
    public function setPriorYrCostMth11(?float $value): void {
        $this->priorYrCostMth11 = $value;
    }

    /**
     * Sets the priorYrCostMth12 property value. The priorYrCostMth12 property
     * @param float|null $value Value to set for the priorYrCostMth12 property.
    */
    public function setPriorYrCostMth12(?float $value): void {
        $this->priorYrCostMth12 = $value;
    }

    /**
     * Sets the priorYrCostMth2 property value. The priorYrCostMth2 property
     * @param float|null $value Value to set for the priorYrCostMth2 property.
    */
    public function setPriorYrCostMth2(?float $value): void {
        $this->priorYrCostMth2 = $value;
    }

    /**
     * Sets the priorYrCostMth3 property value. The priorYrCostMth3 property
     * @param float|null $value Value to set for the priorYrCostMth3 property.
    */
    public function setPriorYrCostMth3(?float $value): void {
        $this->priorYrCostMth3 = $value;
    }

    /**
     * Sets the priorYrCostMth4 property value. The priorYrCostMth4 property
     * @param float|null $value Value to set for the priorYrCostMth4 property.
    */
    public function setPriorYrCostMth4(?float $value): void {
        $this->priorYrCostMth4 = $value;
    }

    /**
     * Sets the priorYrCostMth5 property value. The priorYrCostMth5 property
     * @param float|null $value Value to set for the priorYrCostMth5 property.
    */
    public function setPriorYrCostMth5(?float $value): void {
        $this->priorYrCostMth5 = $value;
    }

    /**
     * Sets the priorYrCostMth6 property value. The priorYrCostMth6 property
     * @param float|null $value Value to set for the priorYrCostMth6 property.
    */
    public function setPriorYrCostMth6(?float $value): void {
        $this->priorYrCostMth6 = $value;
    }

    /**
     * Sets the priorYrCostMth7 property value. The priorYrCostMth7 property
     * @param float|null $value Value to set for the priorYrCostMth7 property.
    */
    public function setPriorYrCostMth7(?float $value): void {
        $this->priorYrCostMth7 = $value;
    }

    /**
     * Sets the priorYrCostMth8 property value. The priorYrCostMth8 property
     * @param float|null $value Value to set for the priorYrCostMth8 property.
    */
    public function setPriorYrCostMth8(?float $value): void {
        $this->priorYrCostMth8 = $value;
    }

    /**
     * Sets the priorYrCostMth9 property value. The priorYrCostMth9 property
     * @param float|null $value Value to set for the priorYrCostMth9 property.
    */
    public function setPriorYrCostMth9(?float $value): void {
        $this->priorYrCostMth9 = $value;
    }

    /**
     * Sets the priorYrQtySoldBf property value. The priorYrQtySoldBf property
     * @param float|null $value Value to set for the priorYrQtySoldBf property.
    */
    public function setPriorYrQtySoldBf(?float $value): void {
        $this->priorYrQtySoldBf = $value;
    }

    /**
     * Sets the priorYrQtySoldFuture property value. The priorYrQtySoldFuture property
     * @param float|null $value Value to set for the priorYrQtySoldFuture property.
    */
    public function setPriorYrQtySoldFuture(?float $value): void {
        $this->priorYrQtySoldFuture = $value;
    }

    /**
     * Sets the priorYrQtySoldMth1 property value. The priorYrQtySoldMth1 property
     * @param float|null $value Value to set for the priorYrQtySoldMth1 property.
    */
    public function setPriorYrQtySoldMth1(?float $value): void {
        $this->priorYrQtySoldMth1 = $value;
    }

    /**
     * Sets the priorYrQtySoldMth10 property value. The priorYrQtySoldMth10 property
     * @param float|null $value Value to set for the priorYrQtySoldMth10 property.
    */
    public function setPriorYrQtySoldMth10(?float $value): void {
        $this->priorYrQtySoldMth10 = $value;
    }

    /**
     * Sets the priorYrQtySoldMth11 property value. The priorYrQtySoldMth11 property
     * @param float|null $value Value to set for the priorYrQtySoldMth11 property.
    */
    public function setPriorYrQtySoldMth11(?float $value): void {
        $this->priorYrQtySoldMth11 = $value;
    }

    /**
     * Sets the priorYrQtySoldMth12 property value. The priorYrQtySoldMth12 property
     * @param float|null $value Value to set for the priorYrQtySoldMth12 property.
    */
    public function setPriorYrQtySoldMth12(?float $value): void {
        $this->priorYrQtySoldMth12 = $value;
    }

    /**
     * Sets the priorYrQtySoldMth2 property value. The priorYrQtySoldMth2 property
     * @param float|null $value Value to set for the priorYrQtySoldMth2 property.
    */
    public function setPriorYrQtySoldMth2(?float $value): void {
        $this->priorYrQtySoldMth2 = $value;
    }

    /**
     * Sets the priorYrQtySoldMth3 property value. The priorYrQtySoldMth3 property
     * @param float|null $value Value to set for the priorYrQtySoldMth3 property.
    */
    public function setPriorYrQtySoldMth3(?float $value): void {
        $this->priorYrQtySoldMth3 = $value;
    }

    /**
     * Sets the priorYrQtySoldMth4 property value. The priorYrQtySoldMth4 property
     * @param float|null $value Value to set for the priorYrQtySoldMth4 property.
    */
    public function setPriorYrQtySoldMth4(?float $value): void {
        $this->priorYrQtySoldMth4 = $value;
    }

    /**
     * Sets the priorYrQtySoldMth5 property value. The priorYrQtySoldMth5 property
     * @param float|null $value Value to set for the priorYrQtySoldMth5 property.
    */
    public function setPriorYrQtySoldMth5(?float $value): void {
        $this->priorYrQtySoldMth5 = $value;
    }

    /**
     * Sets the priorYrQtySoldMth6 property value. The priorYrQtySoldMth6 property
     * @param float|null $value Value to set for the priorYrQtySoldMth6 property.
    */
    public function setPriorYrQtySoldMth6(?float $value): void {
        $this->priorYrQtySoldMth6 = $value;
    }

    /**
     * Sets the priorYrQtySoldMth7 property value. The priorYrQtySoldMth7 property
     * @param float|null $value Value to set for the priorYrQtySoldMth7 property.
    */
    public function setPriorYrQtySoldMth7(?float $value): void {
        $this->priorYrQtySoldMth7 = $value;
    }

    /**
     * Sets the priorYrQtySoldMth8 property value. The priorYrQtySoldMth8 property
     * @param float|null $value Value to set for the priorYrQtySoldMth8 property.
    */
    public function setPriorYrQtySoldMth8(?float $value): void {
        $this->priorYrQtySoldMth8 = $value;
    }

    /**
     * Sets the priorYrQtySoldMth9 property value. The priorYrQtySoldMth9 property
     * @param float|null $value Value to set for the priorYrQtySoldMth9 property.
    */
    public function setPriorYrQtySoldMth9(?float $value): void {
        $this->priorYrQtySoldMth9 = $value;
    }

    /**
     * Sets the priorYrSalesBf property value. The priorYrSalesBf property
     * @param float|null $value Value to set for the priorYrSalesBf property.
    */
    public function setPriorYrSalesBf(?float $value): void {
        $this->priorYrSalesBf = $value;
    }

    /**
     * Sets the priorYrSalesFuture property value. The priorYrSalesFuture property
     * @param float|null $value Value to set for the priorYrSalesFuture property.
    */
    public function setPriorYrSalesFuture(?float $value): void {
        $this->priorYrSalesFuture = $value;
    }

    /**
     * Sets the priorYrSalesMth1 property value. The priorYrSalesMth1 property
     * @param float|null $value Value to set for the priorYrSalesMth1 property.
    */
    public function setPriorYrSalesMth1(?float $value): void {
        $this->priorYrSalesMth1 = $value;
    }

    /**
     * Sets the priorYrSalesMth10 property value. The priorYrSalesMth10 property
     * @param float|null $value Value to set for the priorYrSalesMth10 property.
    */
    public function setPriorYrSalesMth10(?float $value): void {
        $this->priorYrSalesMth10 = $value;
    }

    /**
     * Sets the priorYrSalesMth11 property value. The priorYrSalesMth11 property
     * @param float|null $value Value to set for the priorYrSalesMth11 property.
    */
    public function setPriorYrSalesMth11(?float $value): void {
        $this->priorYrSalesMth11 = $value;
    }

    /**
     * Sets the priorYrSalesMth12 property value. The priorYrSalesMth12 property
     * @param float|null $value Value to set for the priorYrSalesMth12 property.
    */
    public function setPriorYrSalesMth12(?float $value): void {
        $this->priorYrSalesMth12 = $value;
    }

    /**
     * Sets the priorYrSalesMth2 property value. The priorYrSalesMth2 property
     * @param float|null $value Value to set for the priorYrSalesMth2 property.
    */
    public function setPriorYrSalesMth2(?float $value): void {
        $this->priorYrSalesMth2 = $value;
    }

    /**
     * Sets the priorYrSalesMth3 property value. The priorYrSalesMth3 property
     * @param float|null $value Value to set for the priorYrSalesMth3 property.
    */
    public function setPriorYrSalesMth3(?float $value): void {
        $this->priorYrSalesMth3 = $value;
    }

    /**
     * Sets the priorYrSalesMth4 property value. The priorYrSalesMth4 property
     * @param float|null $value Value to set for the priorYrSalesMth4 property.
    */
    public function setPriorYrSalesMth4(?float $value): void {
        $this->priorYrSalesMth4 = $value;
    }

    /**
     * Sets the priorYrSalesMth5 property value. The priorYrSalesMth5 property
     * @param float|null $value Value to set for the priorYrSalesMth5 property.
    */
    public function setPriorYrSalesMth5(?float $value): void {
        $this->priorYrSalesMth5 = $value;
    }

    /**
     * Sets the priorYrSalesMth6 property value. The priorYrSalesMth6 property
     * @param float|null $value Value to set for the priorYrSalesMth6 property.
    */
    public function setPriorYrSalesMth6(?float $value): void {
        $this->priorYrSalesMth6 = $value;
    }

    /**
     * Sets the priorYrSalesMth7 property value. The priorYrSalesMth7 property
     * @param float|null $value Value to set for the priorYrSalesMth7 property.
    */
    public function setPriorYrSalesMth7(?float $value): void {
        $this->priorYrSalesMth7 = $value;
    }

    /**
     * Sets the priorYrSalesMth8 property value. The priorYrSalesMth8 property
     * @param float|null $value Value to set for the priorYrSalesMth8 property.
    */
    public function setPriorYrSalesMth8(?float $value): void {
        $this->priorYrSalesMth8 = $value;
    }

    /**
     * Sets the priorYrSalesMth9 property value. The priorYrSalesMth9 property
     * @param float|null $value Value to set for the priorYrSalesMth9 property.
    */
    public function setPriorYrSalesMth9(?float $value): void {
        $this->priorYrSalesMth9 = $value;
    }

    /**
     * Sets the purchaseNominalCode property value. The purchaseNominalCode property
     * @param string|null $value Value to set for the purchaseNominalCode property.
    */
    public function setPurchaseNominalCode(?string $value): void {
        $this->purchaseNominalCode = $value;
    }

    /**
     * Sets the purchaseRef property value. The purchaseRef property
     * @param string|null $value Value to set for the purchaseRef property.
    */
    public function setPurchaseRef(?string $value): void {
        $this->purchaseRef = $value;
    }

    /**
     * Sets the qtyAllocated property value. The qtyAllocated property
     * @param float|null $value Value to set for the qtyAllocated property.
    */
    public function setQtyAllocated(?float $value): void {
        $this->qtyAllocated = $value;
    }

    /**
     * Sets the qtyInStock property value. The qtyInStock property
     * @param float|null $value Value to set for the qtyInStock property.
    */
    public function setQtyInStock(?float $value): void {
        $this->qtyInStock = $value;
    }

    /**
     * Sets the qtyLastOrder property value. The qtyLastOrder property
     * @param float|null $value Value to set for the qtyLastOrder property.
    */
    public function setQtyLastOrder(?float $value): void {
        $this->qtyLastOrder = $value;
    }

    /**
     * Sets the qtyLastStockTake property value. The qtyLastStockTake property
     * @param float|null $value Value to set for the qtyLastStockTake property.
    */
    public function setQtyLastStockTake(?float $value): void {
        $this->qtyLastStockTake = $value;
    }

    /**
     * Sets the qtyMakeup property value. The qtyMakeup property
     * @param float|null $value Value to set for the qtyMakeup property.
    */
    public function setQtyMakeup(?float $value): void {
        $this->qtyMakeup = $value;
    }

    /**
     * Sets the qtyOnOrder property value. The qtyOnOrder property
     * @param float|null $value Value to set for the qtyOnOrder property.
    */
    public function setQtyOnOrder(?float $value): void {
        $this->qtyOnOrder = $value;
    }

    /**
     * Sets the qtyReorder property value. The qtyReorder property
     * @param float|null $value Value to set for the qtyReorder property.
    */
    public function setQtyReorder(?float $value): void {
        $this->qtyReorder = $value;
    }

    /**
     * Sets the qtyReorderLevel property value. The qtyReorderLevel property
     * @param float|null $value Value to set for the qtyReorderLevel property.
    */
    public function setQtyReorderLevel(?float $value): void {
        $this->qtyReorderLevel = $value;
    }

    /**
     * Sets the qtySoldBf property value. The qtySoldBf property
     * @param float|null $value Value to set for the qtySoldBf property.
    */
    public function setQtySoldBf(?float $value): void {
        $this->qtySoldBf = $value;
    }

    /**
     * Sets the qtySoldFuture property value. The qtySoldFuture property
     * @param float|null $value Value to set for the qtySoldFuture property.
    */
    public function setQtySoldFuture(?float $value): void {
        $this->qtySoldFuture = $value;
    }

    /**
     * Sets the qtySoldMth1 property value. The qtySoldMth1 property
     * @param float|null $value Value to set for the qtySoldMth1 property.
    */
    public function setQtySoldMth1(?float $value): void {
        $this->qtySoldMth1 = $value;
    }

    /**
     * Sets the qtySoldMth10 property value. The qtySoldMth10 property
     * @param float|null $value Value to set for the qtySoldMth10 property.
    */
    public function setQtySoldMth10(?float $value): void {
        $this->qtySoldMth10 = $value;
    }

    /**
     * Sets the qtySoldMth11 property value. The qtySoldMth11 property
     * @param float|null $value Value to set for the qtySoldMth11 property.
    */
    public function setQtySoldMth11(?float $value): void {
        $this->qtySoldMth11 = $value;
    }

    /**
     * Sets the qtySoldMth12 property value. The qtySoldMth12 property
     * @param float|null $value Value to set for the qtySoldMth12 property.
    */
    public function setQtySoldMth12(?float $value): void {
        $this->qtySoldMth12 = $value;
    }

    /**
     * Sets the qtySoldMth2 property value. The qtySoldMth2 property
     * @param float|null $value Value to set for the qtySoldMth2 property.
    */
    public function setQtySoldMth2(?float $value): void {
        $this->qtySoldMth2 = $value;
    }

    /**
     * Sets the qtySoldMth3 property value. The qtySoldMth3 property
     * @param float|null $value Value to set for the qtySoldMth3 property.
    */
    public function setQtySoldMth3(?float $value): void {
        $this->qtySoldMth3 = $value;
    }

    /**
     * Sets the qtySoldMth4 property value. The qtySoldMth4 property
     * @param float|null $value Value to set for the qtySoldMth4 property.
    */
    public function setQtySoldMth4(?float $value): void {
        $this->qtySoldMth4 = $value;
    }

    /**
     * Sets the qtySoldMth5 property value. The qtySoldMth5 property
     * @param float|null $value Value to set for the qtySoldMth5 property.
    */
    public function setQtySoldMth5(?float $value): void {
        $this->qtySoldMth5 = $value;
    }

    /**
     * Sets the qtySoldMth6 property value. The qtySoldMth6 property
     * @param float|null $value Value to set for the qtySoldMth6 property.
    */
    public function setQtySoldMth6(?float $value): void {
        $this->qtySoldMth6 = $value;
    }

    /**
     * Sets the qtySoldMth7 property value. The qtySoldMth7 property
     * @param float|null $value Value to set for the qtySoldMth7 property.
    */
    public function setQtySoldMth7(?float $value): void {
        $this->qtySoldMth7 = $value;
    }

    /**
     * Sets the qtySoldMth8 property value. The qtySoldMth8 property
     * @param float|null $value Value to set for the qtySoldMth8 property.
    */
    public function setQtySoldMth8(?float $value): void {
        $this->qtySoldMth8 = $value;
    }

    /**
     * Sets the qtySoldMth9 property value. The qtySoldMth9 property
     * @param float|null $value Value to set for the qtySoldMth9 property.
    */
    public function setQtySoldMth9(?float $value): void {
        $this->qtySoldMth9 = $value;
    }

    /**
     * Sets the recordCreateDate property value. The recordCreateDate property
     * @param DateTime|null $value Value to set for the recordCreateDate property.
    */
    public function setRecordCreateDate(?DateTime $value): void {
        $this->recordCreateDate = $value;
    }

    /**
     * Sets the recordDeleted property value. The recordDeleted property
     * @param int|null $value Value to set for the recordDeleted property.
    */
    public function setRecordDeleted(?int $value): void {
        $this->recordDeleted = $value;
    }

    /**
     * Sets the recordModifyDate property value. The recordModifyDate property
     * @param DateTime|null $value Value to set for the recordModifyDate property.
    */
    public function setRecordModifyDate(?DateTime $value): void {
        $this->recordModifyDate = $value;
    }

    /**
     * Sets the salesBf property value. The salesBf property
     * @param float|null $value Value to set for the salesBf property.
    */
    public function setSalesBf(?float $value): void {
        $this->salesBf = $value;
    }

    /**
     * Sets the salesFuture property value. The salesFuture property
     * @param float|null $value Value to set for the salesFuture property.
    */
    public function setSalesFuture(?float $value): void {
        $this->salesFuture = $value;
    }

    /**
     * Sets the salesMth1 property value. The salesMth1 property
     * @param float|null $value Value to set for the salesMth1 property.
    */
    public function setSalesMth1(?float $value): void {
        $this->salesMth1 = $value;
    }

    /**
     * Sets the salesMth10 property value. The salesMth10 property
     * @param float|null $value Value to set for the salesMth10 property.
    */
    public function setSalesMth10(?float $value): void {
        $this->salesMth10 = $value;
    }

    /**
     * Sets the salesMth11 property value. The salesMth11 property
     * @param float|null $value Value to set for the salesMth11 property.
    */
    public function setSalesMth11(?float $value): void {
        $this->salesMth11 = $value;
    }

    /**
     * Sets the salesMth12 property value. The salesMth12 property
     * @param float|null $value Value to set for the salesMth12 property.
    */
    public function setSalesMth12(?float $value): void {
        $this->salesMth12 = $value;
    }

    /**
     * Sets the salesMth2 property value. The salesMth2 property
     * @param float|null $value Value to set for the salesMth2 property.
    */
    public function setSalesMth2(?float $value): void {
        $this->salesMth2 = $value;
    }

    /**
     * Sets the salesMth3 property value. The salesMth3 property
     * @param float|null $value Value to set for the salesMth3 property.
    */
    public function setSalesMth3(?float $value): void {
        $this->salesMth3 = $value;
    }

    /**
     * Sets the salesMth4 property value. The salesMth4 property
     * @param float|null $value Value to set for the salesMth4 property.
    */
    public function setSalesMth4(?float $value): void {
        $this->salesMth4 = $value;
    }

    /**
     * Sets the salesMth5 property value. The salesMth5 property
     * @param float|null $value Value to set for the salesMth5 property.
    */
    public function setSalesMth5(?float $value): void {
        $this->salesMth5 = $value;
    }

    /**
     * Sets the salesMth6 property value. The salesMth6 property
     * @param float|null $value Value to set for the salesMth6 property.
    */
    public function setSalesMth6(?float $value): void {
        $this->salesMth6 = $value;
    }

    /**
     * Sets the salesMth7 property value. The salesMth7 property
     * @param float|null $value Value to set for the salesMth7 property.
    */
    public function setSalesMth7(?float $value): void {
        $this->salesMth7 = $value;
    }

    /**
     * Sets the salesMth8 property value. The salesMth8 property
     * @param float|null $value Value to set for the salesMth8 property.
    */
    public function setSalesMth8(?float $value): void {
        $this->salesMth8 = $value;
    }

    /**
     * Sets the salesMth9 property value. The salesMth9 property
     * @param float|null $value Value to set for the salesMth9 property.
    */
    public function setSalesMth9(?float $value): void {
        $this->salesMth9 = $value;
    }

    /**
     * Sets the salesPrice property value. The salesPrice property
     * @param float|null $value Value to set for the salesPrice property.
    */
    public function setSalesPrice(?float $value): void {
        $this->salesPrice = $value;
    }

    /**
     * Sets the stockCat property value. The stockCat property
     * @param int|null $value Value to set for the stockCat property.
    */
    public function setStockCat(?int $value): void {
        $this->stockCat = $value;
    }

    /**
     * Sets the stockCatName property value. The stockCatName property
     * @param string|null $value Value to set for the stockCatName property.
    */
    public function setStockCatName(?string $value): void {
        $this->stockCatName = $value;
    }

    /**
     * Sets the stockTakeDate property value. The stockTakeDate property
     * @param DateTime|null $value Value to set for the stockTakeDate property.
    */
    public function setStockTakeDate(?DateTime $value): void {
        $this->stockTakeDate = $value;
    }

    /**
     * Sets the supplierPartNumber property value. The supplierPartNumber property
     * @param string|null $value Value to set for the supplierPartNumber property.
    */
    public function setSupplierPartNumber(?string $value): void {
        $this->supplierPartNumber = $value;
    }

    /**
     * Sets the suppUnitQty property value. The suppUnitQty property
     * @param float|null $value Value to set for the suppUnitQty property.
    */
    public function setSuppUnitQty(?float $value): void {
        $this->suppUnitQty = $value;
    }

    /**
     * Sets the taxCode property value. The taxCode property
     * @param StockAttributesRead_taxCode|null $value Value to set for the taxCode property.
    */
    public function setTaxCode(?StockAttributesRead_taxCode $value): void {
        $this->taxCode = $value;
    }

    /**
     * Sets the thisRecord property value. The thisRecord property
     * @param int|null $value Value to set for the thisRecord property.
    */
    public function setThisRecord(?int $value): void {
        $this->thisRecord = $value;
    }

    /**
     * Sets the unitOfSale property value. The unitOfSale property
     * @param string|null $value Value to set for the unitOfSale property.
    */
    public function setUnitOfSale(?string $value): void {
        $this->unitOfSale = $value;
    }

    /**
     * Sets the unitWeight property value. The unitWeight property
     * @param float|null $value Value to set for the unitWeight property.
    */
    public function setUnitWeight(?float $value): void {
        $this->unitWeight = $value;
    }

    /**
     * Sets the webCategory1 property value. The webCategory1 property
     * @param string|null $value Value to set for the webCategory1 property.
    */
    public function setWebCategory1(?string $value): void {
        $this->webCategory1 = $value;
    }

    /**
     * Sets the webCategory2 property value. The webCategory2 property
     * @param string|null $value Value to set for the webCategory2 property.
    */
    public function setWebCategory2(?string $value): void {
        $this->webCategory2 = $value;
    }

    /**
     * Sets the webCategory3 property value. The webCategory3 property
     * @param string|null $value Value to set for the webCategory3 property.
    */
    public function setWebCategory3(?string $value): void {
        $this->webCategory3 = $value;
    }

    /**
     * Sets the webDescription property value. The webDescription property
     * @param string|null $value Value to set for the webDescription property.
    */
    public function setWebDescription(?string $value): void {
        $this->webDescription = $value;
    }

    /**
     * Sets the webDetails property value. The webDetails property
     * @param string|null $value Value to set for the webDetails property.
    */
    public function setWebDetails(?string $value): void {
        $this->webDetails = $value;
    }

    /**
     * Sets the webImageFile property value. The webImageFile property
     * @param string|null $value Value to set for the webImageFile property.
    */
    public function setWebImageFile(?string $value): void {
        $this->webImageFile = $value;
    }

    /**
     * Sets the webPublish property value. The webPublish property
     * @param int|null $value Value to set for the webPublish property.
    */
    public function setWebPublish(?int $value): void {
        $this->webPublish = $value;
    }

    /**
     * Sets the webSpecial property value. The webSpecial property
     * @param int|null $value Value to set for the webSpecial property.
    */
    public function setWebSpecial(?int $value): void {
        $this->webSpecial = $value;
    }

}
