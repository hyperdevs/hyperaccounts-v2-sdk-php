<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class PurchaseOrderItemGetDto implements Parsable
{
    /**
     * @var PurchaseOrderItemAttributesRead|null $attributes The attributes property
    */
    private ?PurchaseOrderItemAttributesRead $attributes = null;

    /**
     * @var PurchaseOrderItemIncluded|null $included The included property
    */
    private ?PurchaseOrderItemIncluded $included = null;

    /**
     * @var int|null $itemid The itemid property
    */
    private ?int $itemid = null;

    /**
     * @var ResourceLinks|null $links The links property
    */
    private ?ResourceLinks $links = null;

    /**
     * @var PurchaseOrderItemRelationshipsRead|null $relationships The relationships property
    */
    private ?PurchaseOrderItemRelationshipsRead $relationships = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return PurchaseOrderItemGetDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): PurchaseOrderItemGetDto {
        return new PurchaseOrderItemGetDto();
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return PurchaseOrderItemAttributesRead|null
    */
    public function getAttributes(): ?PurchaseOrderItemAttributesRead {
        return $this->attributes;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([PurchaseOrderItemAttributesRead::class, 'createFromDiscriminatorValue'])),
            'included' => fn(ParseNode $n) => $o->setIncluded($n->getObjectValue([PurchaseOrderItemIncluded::class, 'createFromDiscriminatorValue'])),
            'itemid' => fn(ParseNode $n) => $o->setItemid($n->getIntegerValue()),
            'links' => fn(ParseNode $n) => $o->setLinks($n->getObjectValue([ResourceLinks::class, 'createFromDiscriminatorValue'])),
            'relationships' => fn(ParseNode $n) => $o->setRelationships($n->getObjectValue([PurchaseOrderItemRelationshipsRead::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the included property value. The included property
     * @return PurchaseOrderItemIncluded|null
    */
    public function getIncluded(): ?PurchaseOrderItemIncluded {
        return $this->included;
    }

    /**
     * Gets the itemid property value. The itemid property
     * @return int|null
    */
    public function getItemid(): ?int {
        return $this->itemid;
    }

    /**
     * Gets the links property value. The links property
     * @return ResourceLinks|null
    */
    public function getLinks(): ?ResourceLinks {
        return $this->links;
    }

    /**
     * Gets the relationships property value. The relationships property
     * @return PurchaseOrderItemRelationshipsRead|null
    */
    public function getRelationships(): ?PurchaseOrderItemRelationshipsRead {
        return $this->relationships;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('attributes', $this->getAttributes());
        $writer->writeObjectValue('included', $this->getIncluded());
        $writer->writeIntegerValue('itemid', $this->getItemid());
        $writer->writeObjectValue('links', $this->getLinks());
        $writer->writeObjectValue('relationships', $this->getRelationships());
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param PurchaseOrderItemAttributesRead|null $value Value to set for the attributes property.
    */
    public function setAttributes(?PurchaseOrderItemAttributesRead $value): void {
        $this->attributes = $value;
    }

    /**
     * Sets the included property value. The included property
     * @param PurchaseOrderItemIncluded|null $value Value to set for the included property.
    */
    public function setIncluded(?PurchaseOrderItemIncluded $value): void {
        $this->included = $value;
    }

    /**
     * Sets the itemid property value. The itemid property
     * @param int|null $value Value to set for the itemid property.
    */
    public function setItemid(?int $value): void {
        $this->itemid = $value;
    }

    /**
     * Sets the links property value. The links property
     * @param ResourceLinks|null $value Value to set for the links property.
    */
    public function setLinks(?ResourceLinks $value): void {
        $this->links = $value;
    }

    /**
     * Sets the relationships property value. The relationships property
     * @param PurchaseOrderItemRelationshipsRead|null $value Value to set for the relationships property.
    */
    public function setRelationships(?PurchaseOrderItemRelationshipsRead $value): void {
        $this->relationships = $value;
    }

}
