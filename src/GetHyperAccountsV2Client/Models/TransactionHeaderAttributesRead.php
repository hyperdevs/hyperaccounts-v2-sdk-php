<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use DateTime;
use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class TransactionHeaderAttributesRead implements Parsable
{
    /**
     * @var string|null $accountRef The accountRef property
    */
    private ?string $accountRef = null;

    /**
     * @var float|null $aged30 The aged30 property
    */
    private ?float $aged30 = null;

    /**
     * @var float|null $aged60 The aged60 property
    */
    private ?float $aged60 = null;

    /**
     * @var float|null $aged90 The aged90 property
    */
    private ?float $aged90 = null;

    /**
     * @var float|null $agedBalance The agedBalance property
    */
    private ?float $agedBalance = null;

    /**
     * @var float|null $agedCum30 The agedCum30 property
    */
    private ?float $agedCum30 = null;

    /**
     * @var float|null $agedCum60 The agedCum60 property
    */
    private ?float $agedCum60 = null;

    /**
     * @var float|null $agedCum90 The agedCum90 property
    */
    private ?float $agedCum90 = null;

    /**
     * @var float|null $agedCumCurrent The agedCumCurrent property
    */
    private ?float $agedCumCurrent = null;

    /**
     * @var float|null $agedCurrent The agedCurrent property
    */
    private ?float $agedCurrent = null;

    /**
     * @var float|null $agedFuture The agedFuture property
    */
    private ?float $agedFuture = null;

    /**
     * @var float|null $agedOlder The agedOlder property
    */
    private ?float $agedOlder = null;

    /**
     * @var float|null $amountPaid The amountPaid property
    */
    private ?float $amountPaid = null;

    /**
     * @var int|null $attachmentAdded The attachmentAdded property
    */
    private ?int $attachmentAdded = null;

    /**
     * @var int|null $autoPostedTransactionFlag The autoPostedTransactionFlag property
    */
    private ?int $autoPostedTransactionFlag = null;

    /**
     * @var float|null $bankAmount The bankAmount property
    */
    private ?float $bankAmount = null;

    /**
     * @var string|null $bankChargeAccount The bankChargeAccount property
    */
    private ?string $bankChargeAccount = null;

    /**
     * @var int|null $bankChargeIsCustomer The bankChargeIsCustomer property
    */
    private ?int $bankChargeIsCustomer = null;

    /**
     * @var string|null $bankCode The bankCode property
    */
    private ?string $bankCode = null;

    /**
     * @var TransactionHeaderAttributesRead_bankFlag|null $bankFlag The bankFlag property
    */
    private ?TransactionHeaderAttributesRead_bankFlag $bankFlag = null;

    /**
     * @var string|null $bankName The bankName property
    */
    private ?string $bankName = null;

    /**
     * @var int|null $bankReconciliationId The bankReconciliationId property
    */
    private ?int $bankReconciliationId = null;

    /**
     * @var string|null $bankReconciliationRef The bankReconciliationRef property
    */
    private ?string $bankReconciliationRef = null;

    /**
     * @var int|null $cisReconciled The cisReconciled property
    */
    private ?int $cisReconciled = null;

    /**
     * @var string|null $countryCode The countryCode property
    */
    private ?string $countryCode = null;

    /**
     * @var int|null $currency The currency property
    */
    private ?int $currency = null;

    /**
     * @var int|null $currencyType The currencyType property
    */
    private ?int $currencyType = null;

    /**
     * @var DateTime|null $date The date property
    */
    private ?DateTime $date = null;

    /**
     * @var DateTime|null $dateAmended The dateAmended property
    */
    private ?DateTime $dateAmended = null;

    /**
     * @var DateTime|null $dateBankReconciled The dateBankReconciled property
    */
    private ?DateTime $dateBankReconciled = null;

    /**
     * @var DateTime|null $dateBf The dateBf property
    */
    private ?DateTime $dateBf = null;

    /**
     * @var DateTime|null $dateEntered The dateEntered property
    */
    private ?DateTime $dateEntered = null;

    /**
     * @var int|null $dateFlag The dateFlag property
    */
    private ?int $dateFlag = null;

    /**
     * @var DateTime|null $dateFrom The dateFrom property
    */
    private ?DateTime $dateFrom = null;

    /**
     * @var DateTime|null $dateTo The dateTo property
    */
    private ?DateTime $dateTo = null;

    /**
     * @var int|null $deletedFlag The deletedFlag property
    */
    private ?int $deletedFlag = null;

    /**
     * @var DateTime|null $depositDate The depositDate property
    */
    private ?DateTime $depositDate = null;

    /**
     * @var TransactionHeaderAttributesRead_depositFlag|null $depositFlag The depositFlag property
    */
    private ?TransactionHeaderAttributesRead_depositFlag $depositFlag = null;

    /**
     * @var string|null $details The details property
    */
    private ?string $details = null;

    /**
     * @var int|null $disputed The disputed property
    */
    private ?int $disputed = null;

    /**
     * @var DateTime|null $dueDate The dueDate property
    */
    private ?DateTime $dueDate = null;

    /**
     * @var int|null $electronicTrans The electronicTrans property
    */
    private ?int $electronicTrans = null;

    /**
     * @var float|null $euroGross The euroGross property
    */
    private ?float $euroGross = null;

    /**
     * @var float|null $euroRate The euroRate property
    */
    private ?float $euroRate = null;

    /**
     * @var int|null $financeCharge The financeCharge property
    */
    private ?int $financeCharge = null;

    /**
     * @var float|null $foreignAged30 The foreignAged30 property
    */
    private ?float $foreignAged30 = null;

    /**
     * @var float|null $foreignAged60 The foreignAged60 property
    */
    private ?float $foreignAged60 = null;

    /**
     * @var float|null $foreignAged90 The foreignAged90 property
    */
    private ?float $foreignAged90 = null;

    /**
     * @var float|null $foreignAgedBalance The foreignAgedBalance property
    */
    private ?float $foreignAgedBalance = null;

    /**
     * @var float|null $foreignAgedCum30 The foreignAgedCum30 property
    */
    private ?float $foreignAgedCum30 = null;

    /**
     * @var float|null $foreignAgedCum60 The foreignAgedCum60 property
    */
    private ?float $foreignAgedCum60 = null;

    /**
     * @var float|null $foreignAgedCum90 The foreignAgedCum90 property
    */
    private ?float $foreignAgedCum90 = null;

    /**
     * @var float|null $foreignAgedCumCurrent The foreignAgedCumCurrent property
    */
    private ?float $foreignAgedCumCurrent = null;

    /**
     * @var float|null $foreignAgedCurrent The foreignAgedCurrent property
    */
    private ?float $foreignAgedCurrent = null;

    /**
     * @var float|null $foreignAgedFuture The foreignAgedFuture property
    */
    private ?float $foreignAgedFuture = null;

    /**
     * @var float|null $foreignAgedOlder The foreignAgedOlder property
    */
    private ?float $foreignAgedOlder = null;

    /**
     * @var float|null $foreignAmountPaid The foreignAmountPaid property
    */
    private ?float $foreignAmountPaid = null;

    /**
     * @var float|null $foreignBankAmount The foreignBankAmount property
    */
    private ?float $foreignBankAmount = null;

    /**
     * @var float|null $foreignGrossAmount The foreignGrossAmount property
    */
    private ?float $foreignGrossAmount = null;

    /**
     * @var float|null $foreignNetAmount The foreignNetAmount property
    */
    private ?float $foreignNetAmount = null;

    /**
     * @var float|null $foreignOutstanding The foreignOutstanding property
    */
    private ?float $foreignOutstanding = null;

    /**
     * @var float|null $foreignRate The foreignRate property
    */
    private ?float $foreignRate = null;

    /**
     * @var float|null $foreignTaxAmount The foreignTaxAmount property
    */
    private ?float $foreignTaxAmount = null;

    /**
     * @var float|null $grossAmount The grossAmount property
    */
    private ?float $grossAmount = null;

    /**
     * @var float|null $interestRate The interestRate property
    */
    private ?float $interestRate = null;

    /**
     * @var string|null $invRef The invRef property
    */
    private ?string $invRef = null;

    /**
     * @var int|null $invRefNumeric The invRefNumeric property
    */
    private ?int $invRefNumeric = null;

    /**
     * @var int|null $isCisTransaction The isCisTransaction property
    */
    private ?int $isCisTransaction = null;

    /**
     * @var int|null $isDiscount The isDiscount property
    */
    private ?int $isDiscount = null;

    /**
     * @var int|null $isInvoicePayment The isInvoicePayment property
    */
    private ?int $isInvoicePayment = null;

    /**
     * @var int|null $ispReference The ispReference property
    */
    private ?int $ispReference = null;

    /**
     * @var int|null $itemCount The itemCount property
    */
    private ?int $itemCount = null;

    /**
     * @var DateTime|null $lastChargeDate The lastChargeDate property
    */
    private ?DateTime $lastChargeDate = null;

    /**
     * @var float|null $netAmount The netAmount property
    */
    private ?float $netAmount = null;

    /**
     * @var float|null $outstanding The outstanding property
    */
    private ?float $outstanding = null;

    /**
     * @var DateTime|null $overriddenClosedLedgerDate The overriddenClosedLedgerDate property
    */
    private ?DateTime $overriddenClosedLedgerDate = null;

    /**
     * @var TransactionHeaderAttributesRead_paidFlag|null $paidFlag The paidFlag property
    */
    private ?TransactionHeaderAttributesRead_paidFlag $paidFlag = null;

    /**
     * @var string|null $paidStatus The paidStatus property
    */
    private ?string $paidStatus = null;

    /**
     * @var float|null $payment The payment property
    */
    private ?float $payment = null;

    /**
     * @var int|null $postedByDirectDebitSettlement The postedByDirectDebitSettlement property
    */
    private ?int $postedByDirectDebitSettlement = null;

    /**
     * @var DateTime|null $recordCreateDate The recordCreateDate property
    */
    private ?DateTime $recordCreateDate = null;

    /**
     * @var int|null $recordDeleted The recordDeleted property
    */
    private ?int $recordDeleted = null;

    /**
     * @var DateTime|null $recordModifyDate The recordModifyDate property
    */
    private ?DateTime $recordModifyDate = null;

    /**
     * @var int|null $recurringEntryId The recurringEntryId property
    */
    private ?int $recurringEntryId = null;

    /**
     * @var int|null $revalTransactionFlag The revalTransactionFlag property
    */
    private ?int $revalTransactionFlag = null;

    /**
     * @var string|null $salesPurchaseRef The salesPurchaseRef property
    */
    private ?string $salesPurchaseRef = null;

    /**
     * @var string|null $spsRef The spsRef property
    */
    private ?string $spsRef = null;

    /**
     * @var float|null $taxAmount The taxAmount property
    */
    private ?float $taxAmount = null;

    /**
     * @var string|null $taxId The taxId property
    */
    private ?string $taxId = null;

    /**
     * @var int|null $tranNumber The tranNumber property
    */
    private ?int $tranNumber = null;

    /**
     * @var TransactionHeaderAttributesRead_type|null $type The type property
    */
    private ?TransactionHeaderAttributesRead_type $type = null;

    /**
     * @var string|null $userName The userName property
    */
    private ?string $userName = null;

    /**
     * @var string|null $userNameAmended The userNameAmended property
    */
    private ?string $userNameAmended = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return TransactionHeaderAttributesRead
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): TransactionHeaderAttributesRead {
        return new TransactionHeaderAttributesRead();
    }

    /**
     * Gets the accountRef property value. The accountRef property
     * @return string|null
    */
    public function getAccountRef(): ?string {
        return $this->accountRef;
    }

    /**
     * Gets the aged30 property value. The aged30 property
     * @return float|null
    */
    public function getAged30(): ?float {
        return $this->aged30;
    }

    /**
     * Gets the aged60 property value. The aged60 property
     * @return float|null
    */
    public function getAged60(): ?float {
        return $this->aged60;
    }

    /**
     * Gets the aged90 property value. The aged90 property
     * @return float|null
    */
    public function getAged90(): ?float {
        return $this->aged90;
    }

    /**
     * Gets the agedBalance property value. The agedBalance property
     * @return float|null
    */
    public function getAgedBalance(): ?float {
        return $this->agedBalance;
    }

    /**
     * Gets the agedCum30 property value. The agedCum30 property
     * @return float|null
    */
    public function getAgedCum30(): ?float {
        return $this->agedCum30;
    }

    /**
     * Gets the agedCum60 property value. The agedCum60 property
     * @return float|null
    */
    public function getAgedCum60(): ?float {
        return $this->agedCum60;
    }

    /**
     * Gets the agedCum90 property value. The agedCum90 property
     * @return float|null
    */
    public function getAgedCum90(): ?float {
        return $this->agedCum90;
    }

    /**
     * Gets the agedCumCurrent property value. The agedCumCurrent property
     * @return float|null
    */
    public function getAgedCumCurrent(): ?float {
        return $this->agedCumCurrent;
    }

    /**
     * Gets the agedCurrent property value. The agedCurrent property
     * @return float|null
    */
    public function getAgedCurrent(): ?float {
        return $this->agedCurrent;
    }

    /**
     * Gets the agedFuture property value. The agedFuture property
     * @return float|null
    */
    public function getAgedFuture(): ?float {
        return $this->agedFuture;
    }

    /**
     * Gets the agedOlder property value. The agedOlder property
     * @return float|null
    */
    public function getAgedOlder(): ?float {
        return $this->agedOlder;
    }

    /**
     * Gets the amountPaid property value. The amountPaid property
     * @return float|null
    */
    public function getAmountPaid(): ?float {
        return $this->amountPaid;
    }

    /**
     * Gets the attachmentAdded property value. The attachmentAdded property
     * @return int|null
    */
    public function getAttachmentAdded(): ?int {
        return $this->attachmentAdded;
    }

    /**
     * Gets the autoPostedTransactionFlag property value. The autoPostedTransactionFlag property
     * @return int|null
    */
    public function getAutoPostedTransactionFlag(): ?int {
        return $this->autoPostedTransactionFlag;
    }

    /**
     * Gets the bankAmount property value. The bankAmount property
     * @return float|null
    */
    public function getBankAmount(): ?float {
        return $this->bankAmount;
    }

    /**
     * Gets the bankChargeAccount property value. The bankChargeAccount property
     * @return string|null
    */
    public function getBankChargeAccount(): ?string {
        return $this->bankChargeAccount;
    }

    /**
     * Gets the bankChargeIsCustomer property value. The bankChargeIsCustomer property
     * @return int|null
    */
    public function getBankChargeIsCustomer(): ?int {
        return $this->bankChargeIsCustomer;
    }

    /**
     * Gets the bankCode property value. The bankCode property
     * @return string|null
    */
    public function getBankCode(): ?string {
        return $this->bankCode;
    }

    /**
     * Gets the bankFlag property value. The bankFlag property
     * @return TransactionHeaderAttributesRead_bankFlag|null
    */
    public function getBankFlag(): ?TransactionHeaderAttributesRead_bankFlag {
        return $this->bankFlag;
    }

    /**
     * Gets the bankName property value. The bankName property
     * @return string|null
    */
    public function getBankName(): ?string {
        return $this->bankName;
    }

    /**
     * Gets the bankReconciliationId property value. The bankReconciliationId property
     * @return int|null
    */
    public function getBankReconciliationId(): ?int {
        return $this->bankReconciliationId;
    }

    /**
     * Gets the bankReconciliationRef property value. The bankReconciliationRef property
     * @return string|null
    */
    public function getBankReconciliationRef(): ?string {
        return $this->bankReconciliationRef;
    }

    /**
     * Gets the cisReconciled property value. The cisReconciled property
     * @return int|null
    */
    public function getCisReconciled(): ?int {
        return $this->cisReconciled;
    }

    /**
     * Gets the countryCode property value. The countryCode property
     * @return string|null
    */
    public function getCountryCode(): ?string {
        return $this->countryCode;
    }

    /**
     * Gets the currency property value. The currency property
     * @return int|null
    */
    public function getCurrency(): ?int {
        return $this->currency;
    }

    /**
     * Gets the currencyType property value. The currencyType property
     * @return int|null
    */
    public function getCurrencyType(): ?int {
        return $this->currencyType;
    }

    /**
     * Gets the date property value. The date property
     * @return DateTime|null
    */
    public function getDate(): ?DateTime {
        return $this->date;
    }

    /**
     * Gets the dateAmended property value. The dateAmended property
     * @return DateTime|null
    */
    public function getDateAmended(): ?DateTime {
        return $this->dateAmended;
    }

    /**
     * Gets the dateBankReconciled property value. The dateBankReconciled property
     * @return DateTime|null
    */
    public function getDateBankReconciled(): ?DateTime {
        return $this->dateBankReconciled;
    }

    /**
     * Gets the dateBf property value. The dateBf property
     * @return DateTime|null
    */
    public function getDateBf(): ?DateTime {
        return $this->dateBf;
    }

    /**
     * Gets the dateEntered property value. The dateEntered property
     * @return DateTime|null
    */
    public function getDateEntered(): ?DateTime {
        return $this->dateEntered;
    }

    /**
     * Gets the dateFlag property value. The dateFlag property
     * @return int|null
    */
    public function getDateFlag(): ?int {
        return $this->dateFlag;
    }

    /**
     * Gets the dateFrom property value. The dateFrom property
     * @return DateTime|null
    */
    public function getDateFrom(): ?DateTime {
        return $this->dateFrom;
    }

    /**
     * Gets the dateTo property value. The dateTo property
     * @return DateTime|null
    */
    public function getDateTo(): ?DateTime {
        return $this->dateTo;
    }

    /**
     * Gets the deletedFlag property value. The deletedFlag property
     * @return int|null
    */
    public function getDeletedFlag(): ?int {
        return $this->deletedFlag;
    }

    /**
     * Gets the depositDate property value. The depositDate property
     * @return DateTime|null
    */
    public function getDepositDate(): ?DateTime {
        return $this->depositDate;
    }

    /**
     * Gets the depositFlag property value. The depositFlag property
     * @return TransactionHeaderAttributesRead_depositFlag|null
    */
    public function getDepositFlag(): ?TransactionHeaderAttributesRead_depositFlag {
        return $this->depositFlag;
    }

    /**
     * Gets the details property value. The details property
     * @return string|null
    */
    public function getDetails(): ?string {
        return $this->details;
    }

    /**
     * Gets the disputed property value. The disputed property
     * @return int|null
    */
    public function getDisputed(): ?int {
        return $this->disputed;
    }

    /**
     * Gets the dueDate property value. The dueDate property
     * @return DateTime|null
    */
    public function getDueDate(): ?DateTime {
        return $this->dueDate;
    }

    /**
     * Gets the electronicTrans property value. The electronicTrans property
     * @return int|null
    */
    public function getElectronicTrans(): ?int {
        return $this->electronicTrans;
    }

    /**
     * Gets the euroGross property value. The euroGross property
     * @return float|null
    */
    public function getEuroGross(): ?float {
        return $this->euroGross;
    }

    /**
     * Gets the euroRate property value. The euroRate property
     * @return float|null
    */
    public function getEuroRate(): ?float {
        return $this->euroRate;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'accountRef' => fn(ParseNode $n) => $o->setAccountRef($n->getStringValue()),
            'aged30' => fn(ParseNode $n) => $o->setAged30($n->getFloatValue()),
            'aged60' => fn(ParseNode $n) => $o->setAged60($n->getFloatValue()),
            'aged90' => fn(ParseNode $n) => $o->setAged90($n->getFloatValue()),
            'agedBalance' => fn(ParseNode $n) => $o->setAgedBalance($n->getFloatValue()),
            'agedCum30' => fn(ParseNode $n) => $o->setAgedCum30($n->getFloatValue()),
            'agedCum60' => fn(ParseNode $n) => $o->setAgedCum60($n->getFloatValue()),
            'agedCum90' => fn(ParseNode $n) => $o->setAgedCum90($n->getFloatValue()),
            'agedCumCurrent' => fn(ParseNode $n) => $o->setAgedCumCurrent($n->getFloatValue()),
            'agedCurrent' => fn(ParseNode $n) => $o->setAgedCurrent($n->getFloatValue()),
            'agedFuture' => fn(ParseNode $n) => $o->setAgedFuture($n->getFloatValue()),
            'agedOlder' => fn(ParseNode $n) => $o->setAgedOlder($n->getFloatValue()),
            'amountPaid' => fn(ParseNode $n) => $o->setAmountPaid($n->getFloatValue()),
            'attachmentAdded' => fn(ParseNode $n) => $o->setAttachmentAdded($n->getIntegerValue()),
            'autoPostedTransactionFlag' => fn(ParseNode $n) => $o->setAutoPostedTransactionFlag($n->getIntegerValue()),
            'bankAmount' => fn(ParseNode $n) => $o->setBankAmount($n->getFloatValue()),
            'bankChargeAccount' => fn(ParseNode $n) => $o->setBankChargeAccount($n->getStringValue()),
            'bankChargeIsCustomer' => fn(ParseNode $n) => $o->setBankChargeIsCustomer($n->getIntegerValue()),
            'bankCode' => fn(ParseNode $n) => $o->setBankCode($n->getStringValue()),
            'bankFlag' => fn(ParseNode $n) => $o->setBankFlag($n->getEnumValue(TransactionHeaderAttributesRead_bankFlag::class)),
            'bankName' => fn(ParseNode $n) => $o->setBankName($n->getStringValue()),
            'bankReconciliationId' => fn(ParseNode $n) => $o->setBankReconciliationId($n->getIntegerValue()),
            'bankReconciliationRef' => fn(ParseNode $n) => $o->setBankReconciliationRef($n->getStringValue()),
            'cisReconciled' => fn(ParseNode $n) => $o->setCisReconciled($n->getIntegerValue()),
            'countryCode' => fn(ParseNode $n) => $o->setCountryCode($n->getStringValue()),
            'currency' => fn(ParseNode $n) => $o->setCurrency($n->getIntegerValue()),
            'currencyType' => fn(ParseNode $n) => $o->setCurrencyType($n->getIntegerValue()),
            'date' => fn(ParseNode $n) => $o->setDate($n->getDateTimeValue()),
            'dateAmended' => fn(ParseNode $n) => $o->setDateAmended($n->getDateTimeValue()),
            'dateBankReconciled' => fn(ParseNode $n) => $o->setDateBankReconciled($n->getDateTimeValue()),
            'dateBf' => fn(ParseNode $n) => $o->setDateBf($n->getDateTimeValue()),
            'dateEntered' => fn(ParseNode $n) => $o->setDateEntered($n->getDateTimeValue()),
            'dateFlag' => fn(ParseNode $n) => $o->setDateFlag($n->getIntegerValue()),
            'dateFrom' => fn(ParseNode $n) => $o->setDateFrom($n->getDateTimeValue()),
            'dateTo' => fn(ParseNode $n) => $o->setDateTo($n->getDateTimeValue()),
            'deletedFlag' => fn(ParseNode $n) => $o->setDeletedFlag($n->getIntegerValue()),
            'depositDate' => fn(ParseNode $n) => $o->setDepositDate($n->getDateTimeValue()),
            'depositFlag' => fn(ParseNode $n) => $o->setDepositFlag($n->getEnumValue(TransactionHeaderAttributesRead_depositFlag::class)),
            'details' => fn(ParseNode $n) => $o->setDetails($n->getStringValue()),
            'disputed' => fn(ParseNode $n) => $o->setDisputed($n->getIntegerValue()),
            'dueDate' => fn(ParseNode $n) => $o->setDueDate($n->getDateTimeValue()),
            'electronicTrans' => fn(ParseNode $n) => $o->setElectronicTrans($n->getIntegerValue()),
            'euroGross' => fn(ParseNode $n) => $o->setEuroGross($n->getFloatValue()),
            'euroRate' => fn(ParseNode $n) => $o->setEuroRate($n->getFloatValue()),
            'financeCharge' => fn(ParseNode $n) => $o->setFinanceCharge($n->getIntegerValue()),
            'foreignAged30' => fn(ParseNode $n) => $o->setForeignAged30($n->getFloatValue()),
            'foreignAged60' => fn(ParseNode $n) => $o->setForeignAged60($n->getFloatValue()),
            'foreignAged90' => fn(ParseNode $n) => $o->setForeignAged90($n->getFloatValue()),
            'foreignAgedBalance' => fn(ParseNode $n) => $o->setForeignAgedBalance($n->getFloatValue()),
            'foreignAgedCum30' => fn(ParseNode $n) => $o->setForeignAgedCum30($n->getFloatValue()),
            'foreignAgedCum60' => fn(ParseNode $n) => $o->setForeignAgedCum60($n->getFloatValue()),
            'foreignAgedCum90' => fn(ParseNode $n) => $o->setForeignAgedCum90($n->getFloatValue()),
            'foreignAgedCumCurrent' => fn(ParseNode $n) => $o->setForeignAgedCumCurrent($n->getFloatValue()),
            'foreignAgedCurrent' => fn(ParseNode $n) => $o->setForeignAgedCurrent($n->getFloatValue()),
            'foreignAgedFuture' => fn(ParseNode $n) => $o->setForeignAgedFuture($n->getFloatValue()),
            'foreignAgedOlder' => fn(ParseNode $n) => $o->setForeignAgedOlder($n->getFloatValue()),
            'foreignAmountPaid' => fn(ParseNode $n) => $o->setForeignAmountPaid($n->getFloatValue()),
            'foreignBankAmount' => fn(ParseNode $n) => $o->setForeignBankAmount($n->getFloatValue()),
            'foreignGrossAmount' => fn(ParseNode $n) => $o->setForeignGrossAmount($n->getFloatValue()),
            'foreignNetAmount' => fn(ParseNode $n) => $o->setForeignNetAmount($n->getFloatValue()),
            'foreignOutstanding' => fn(ParseNode $n) => $o->setForeignOutstanding($n->getFloatValue()),
            'foreignRate' => fn(ParseNode $n) => $o->setForeignRate($n->getFloatValue()),
            'foreignTaxAmount' => fn(ParseNode $n) => $o->setForeignTaxAmount($n->getFloatValue()),
            'grossAmount' => fn(ParseNode $n) => $o->setGrossAmount($n->getFloatValue()),
            'interestRate' => fn(ParseNode $n) => $o->setInterestRate($n->getFloatValue()),
            'invRef' => fn(ParseNode $n) => $o->setInvRef($n->getStringValue()),
            'invRefNumeric' => fn(ParseNode $n) => $o->setInvRefNumeric($n->getIntegerValue()),
            'isCisTransaction' => fn(ParseNode $n) => $o->setIsCisTransaction($n->getIntegerValue()),
            'isDiscount' => fn(ParseNode $n) => $o->setIsDiscount($n->getIntegerValue()),
            'isInvoicePayment' => fn(ParseNode $n) => $o->setIsInvoicePayment($n->getIntegerValue()),
            'ispReference' => fn(ParseNode $n) => $o->setIspReference($n->getIntegerValue()),
            'itemCount' => fn(ParseNode $n) => $o->setItemCount($n->getIntegerValue()),
            'lastChargeDate' => fn(ParseNode $n) => $o->setLastChargeDate($n->getDateTimeValue()),
            'netAmount' => fn(ParseNode $n) => $o->setNetAmount($n->getFloatValue()),
            'outstanding' => fn(ParseNode $n) => $o->setOutstanding($n->getFloatValue()),
            'overriddenClosedLedgerDate' => fn(ParseNode $n) => $o->setOverriddenClosedLedgerDate($n->getDateTimeValue()),
            'paidFlag' => fn(ParseNode $n) => $o->setPaidFlag($n->getEnumValue(TransactionHeaderAttributesRead_paidFlag::class)),
            'paidStatus' => fn(ParseNode $n) => $o->setPaidStatus($n->getStringValue()),
            'payment' => fn(ParseNode $n) => $o->setPayment($n->getFloatValue()),
            'postedByDirectDebitSettlement' => fn(ParseNode $n) => $o->setPostedByDirectDebitSettlement($n->getIntegerValue()),
            'recordCreateDate' => fn(ParseNode $n) => $o->setRecordCreateDate($n->getDateTimeValue()),
            'recordDeleted' => fn(ParseNode $n) => $o->setRecordDeleted($n->getIntegerValue()),
            'recordModifyDate' => fn(ParseNode $n) => $o->setRecordModifyDate($n->getDateTimeValue()),
            'recurringEntryId' => fn(ParseNode $n) => $o->setRecurringEntryId($n->getIntegerValue()),
            'revalTransactionFlag' => fn(ParseNode $n) => $o->setRevalTransactionFlag($n->getIntegerValue()),
            'salesPurchaseRef' => fn(ParseNode $n) => $o->setSalesPurchaseRef($n->getStringValue()),
            'spsRef' => fn(ParseNode $n) => $o->setSpsRef($n->getStringValue()),
            'taxAmount' => fn(ParseNode $n) => $o->setTaxAmount($n->getFloatValue()),
            'taxId' => fn(ParseNode $n) => $o->setTaxId($n->getStringValue()),
            'tranNumber' => fn(ParseNode $n) => $o->setTranNumber($n->getIntegerValue()),
            'type' => fn(ParseNode $n) => $o->setType($n->getEnumValue(TransactionHeaderAttributesRead_type::class)),
            'userName' => fn(ParseNode $n) => $o->setUserName($n->getStringValue()),
            'userNameAmended' => fn(ParseNode $n) => $o->setUserNameAmended($n->getStringValue()),
        ];
    }

    /**
     * Gets the financeCharge property value. The financeCharge property
     * @return int|null
    */
    public function getFinanceCharge(): ?int {
        return $this->financeCharge;
    }

    /**
     * Gets the foreignAged30 property value. The foreignAged30 property
     * @return float|null
    */
    public function getForeignAged30(): ?float {
        return $this->foreignAged30;
    }

    /**
     * Gets the foreignAged60 property value. The foreignAged60 property
     * @return float|null
    */
    public function getForeignAged60(): ?float {
        return $this->foreignAged60;
    }

    /**
     * Gets the foreignAged90 property value. The foreignAged90 property
     * @return float|null
    */
    public function getForeignAged90(): ?float {
        return $this->foreignAged90;
    }

    /**
     * Gets the foreignAgedBalance property value. The foreignAgedBalance property
     * @return float|null
    */
    public function getForeignAgedBalance(): ?float {
        return $this->foreignAgedBalance;
    }

    /**
     * Gets the foreignAgedCum30 property value. The foreignAgedCum30 property
     * @return float|null
    */
    public function getForeignAgedCum30(): ?float {
        return $this->foreignAgedCum30;
    }

    /**
     * Gets the foreignAgedCum60 property value. The foreignAgedCum60 property
     * @return float|null
    */
    public function getForeignAgedCum60(): ?float {
        return $this->foreignAgedCum60;
    }

    /**
     * Gets the foreignAgedCum90 property value. The foreignAgedCum90 property
     * @return float|null
    */
    public function getForeignAgedCum90(): ?float {
        return $this->foreignAgedCum90;
    }

    /**
     * Gets the foreignAgedCumCurrent property value. The foreignAgedCumCurrent property
     * @return float|null
    */
    public function getForeignAgedCumCurrent(): ?float {
        return $this->foreignAgedCumCurrent;
    }

    /**
     * Gets the foreignAgedCurrent property value. The foreignAgedCurrent property
     * @return float|null
    */
    public function getForeignAgedCurrent(): ?float {
        return $this->foreignAgedCurrent;
    }

    /**
     * Gets the foreignAgedFuture property value. The foreignAgedFuture property
     * @return float|null
    */
    public function getForeignAgedFuture(): ?float {
        return $this->foreignAgedFuture;
    }

    /**
     * Gets the foreignAgedOlder property value. The foreignAgedOlder property
     * @return float|null
    */
    public function getForeignAgedOlder(): ?float {
        return $this->foreignAgedOlder;
    }

    /**
     * Gets the foreignAmountPaid property value. The foreignAmountPaid property
     * @return float|null
    */
    public function getForeignAmountPaid(): ?float {
        return $this->foreignAmountPaid;
    }

    /**
     * Gets the foreignBankAmount property value. The foreignBankAmount property
     * @return float|null
    */
    public function getForeignBankAmount(): ?float {
        return $this->foreignBankAmount;
    }

    /**
     * Gets the foreignGrossAmount property value. The foreignGrossAmount property
     * @return float|null
    */
    public function getForeignGrossAmount(): ?float {
        return $this->foreignGrossAmount;
    }

    /**
     * Gets the foreignNetAmount property value. The foreignNetAmount property
     * @return float|null
    */
    public function getForeignNetAmount(): ?float {
        return $this->foreignNetAmount;
    }

    /**
     * Gets the foreignOutstanding property value. The foreignOutstanding property
     * @return float|null
    */
    public function getForeignOutstanding(): ?float {
        return $this->foreignOutstanding;
    }

    /**
     * Gets the foreignRate property value. The foreignRate property
     * @return float|null
    */
    public function getForeignRate(): ?float {
        return $this->foreignRate;
    }

    /**
     * Gets the foreignTaxAmount property value. The foreignTaxAmount property
     * @return float|null
    */
    public function getForeignTaxAmount(): ?float {
        return $this->foreignTaxAmount;
    }

    /**
     * Gets the grossAmount property value. The grossAmount property
     * @return float|null
    */
    public function getGrossAmount(): ?float {
        return $this->grossAmount;
    }

    /**
     * Gets the interestRate property value. The interestRate property
     * @return float|null
    */
    public function getInterestRate(): ?float {
        return $this->interestRate;
    }

    /**
     * Gets the invRef property value. The invRef property
     * @return string|null
    */
    public function getInvRef(): ?string {
        return $this->invRef;
    }

    /**
     * Gets the invRefNumeric property value. The invRefNumeric property
     * @return int|null
    */
    public function getInvRefNumeric(): ?int {
        return $this->invRefNumeric;
    }

    /**
     * Gets the isCisTransaction property value. The isCisTransaction property
     * @return int|null
    */
    public function getIsCisTransaction(): ?int {
        return $this->isCisTransaction;
    }

    /**
     * Gets the isDiscount property value. The isDiscount property
     * @return int|null
    */
    public function getIsDiscount(): ?int {
        return $this->isDiscount;
    }

    /**
     * Gets the isInvoicePayment property value. The isInvoicePayment property
     * @return int|null
    */
    public function getIsInvoicePayment(): ?int {
        return $this->isInvoicePayment;
    }

    /**
     * Gets the ispReference property value. The ispReference property
     * @return int|null
    */
    public function getIspReference(): ?int {
        return $this->ispReference;
    }

    /**
     * Gets the itemCount property value. The itemCount property
     * @return int|null
    */
    public function getItemCount(): ?int {
        return $this->itemCount;
    }

    /**
     * Gets the lastChargeDate property value. The lastChargeDate property
     * @return DateTime|null
    */
    public function getLastChargeDate(): ?DateTime {
        return $this->lastChargeDate;
    }

    /**
     * Gets the netAmount property value. The netAmount property
     * @return float|null
    */
    public function getNetAmount(): ?float {
        return $this->netAmount;
    }

    /**
     * Gets the outstanding property value. The outstanding property
     * @return float|null
    */
    public function getOutstanding(): ?float {
        return $this->outstanding;
    }

    /**
     * Gets the overriddenClosedLedgerDate property value. The overriddenClosedLedgerDate property
     * @return DateTime|null
    */
    public function getOverriddenClosedLedgerDate(): ?DateTime {
        return $this->overriddenClosedLedgerDate;
    }

    /**
     * Gets the paidFlag property value. The paidFlag property
     * @return TransactionHeaderAttributesRead_paidFlag|null
    */
    public function getPaidFlag(): ?TransactionHeaderAttributesRead_paidFlag {
        return $this->paidFlag;
    }

    /**
     * Gets the paidStatus property value. The paidStatus property
     * @return string|null
    */
    public function getPaidStatus(): ?string {
        return $this->paidStatus;
    }

    /**
     * Gets the payment property value. The payment property
     * @return float|null
    */
    public function getPayment(): ?float {
        return $this->payment;
    }

    /**
     * Gets the postedByDirectDebitSettlement property value. The postedByDirectDebitSettlement property
     * @return int|null
    */
    public function getPostedByDirectDebitSettlement(): ?int {
        return $this->postedByDirectDebitSettlement;
    }

    /**
     * Gets the recordCreateDate property value. The recordCreateDate property
     * @return DateTime|null
    */
    public function getRecordCreateDate(): ?DateTime {
        return $this->recordCreateDate;
    }

    /**
     * Gets the recordDeleted property value. The recordDeleted property
     * @return int|null
    */
    public function getRecordDeleted(): ?int {
        return $this->recordDeleted;
    }

    /**
     * Gets the recordModifyDate property value. The recordModifyDate property
     * @return DateTime|null
    */
    public function getRecordModifyDate(): ?DateTime {
        return $this->recordModifyDate;
    }

    /**
     * Gets the recurringEntryId property value. The recurringEntryId property
     * @return int|null
    */
    public function getRecurringEntryId(): ?int {
        return $this->recurringEntryId;
    }

    /**
     * Gets the revalTransactionFlag property value. The revalTransactionFlag property
     * @return int|null
    */
    public function getRevalTransactionFlag(): ?int {
        return $this->revalTransactionFlag;
    }

    /**
     * Gets the salesPurchaseRef property value. The salesPurchaseRef property
     * @return string|null
    */
    public function getSalesPurchaseRef(): ?string {
        return $this->salesPurchaseRef;
    }

    /**
     * Gets the spsRef property value. The spsRef property
     * @return string|null
    */
    public function getSpsRef(): ?string {
        return $this->spsRef;
    }

    /**
     * Gets the taxAmount property value. The taxAmount property
     * @return float|null
    */
    public function getTaxAmount(): ?float {
        return $this->taxAmount;
    }

    /**
     * Gets the taxId property value. The taxId property
     * @return string|null
    */
    public function getTaxId(): ?string {
        return $this->taxId;
    }

    /**
     * Gets the tranNumber property value. The tranNumber property
     * @return int|null
    */
    public function getTranNumber(): ?int {
        return $this->tranNumber;
    }

    /**
     * Gets the type property value. The type property
     * @return TransactionHeaderAttributesRead_type|null
    */
    public function getType(): ?TransactionHeaderAttributesRead_type {
        return $this->type;
    }

    /**
     * Gets the userName property value. The userName property
     * @return string|null
    */
    public function getUserName(): ?string {
        return $this->userName;
    }

    /**
     * Gets the userNameAmended property value. The userNameAmended property
     * @return string|null
    */
    public function getUserNameAmended(): ?string {
        return $this->userNameAmended;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeStringValue('accountRef', $this->getAccountRef());
        $writer->writeFloatValue('aged30', $this->getAged30());
        $writer->writeFloatValue('aged60', $this->getAged60());
        $writer->writeFloatValue('aged90', $this->getAged90());
        $writer->writeFloatValue('agedBalance', $this->getAgedBalance());
        $writer->writeFloatValue('agedCum30', $this->getAgedCum30());
        $writer->writeFloatValue('agedCum60', $this->getAgedCum60());
        $writer->writeFloatValue('agedCum90', $this->getAgedCum90());
        $writer->writeFloatValue('agedCumCurrent', $this->getAgedCumCurrent());
        $writer->writeFloatValue('agedCurrent', $this->getAgedCurrent());
        $writer->writeFloatValue('agedFuture', $this->getAgedFuture());
        $writer->writeFloatValue('agedOlder', $this->getAgedOlder());
        $writer->writeFloatValue('amountPaid', $this->getAmountPaid());
        $writer->writeIntegerValue('attachmentAdded', $this->getAttachmentAdded());
        $writer->writeIntegerValue('autoPostedTransactionFlag', $this->getAutoPostedTransactionFlag());
        $writer->writeFloatValue('bankAmount', $this->getBankAmount());
        $writer->writeStringValue('bankChargeAccount', $this->getBankChargeAccount());
        $writer->writeIntegerValue('bankChargeIsCustomer', $this->getBankChargeIsCustomer());
        $writer->writeStringValue('bankCode', $this->getBankCode());
        $writer->writeEnumValue('bankFlag', $this->getBankFlag());
        $writer->writeStringValue('bankName', $this->getBankName());
        $writer->writeIntegerValue('bankReconciliationId', $this->getBankReconciliationId());
        $writer->writeStringValue('bankReconciliationRef', $this->getBankReconciliationRef());
        $writer->writeIntegerValue('cisReconciled', $this->getCisReconciled());
        $writer->writeStringValue('countryCode', $this->getCountryCode());
        $writer->writeIntegerValue('currency', $this->getCurrency());
        $writer->writeIntegerValue('currencyType', $this->getCurrencyType());
        $writer->writeDateTimeValue('date', $this->getDate());
        $writer->writeDateTimeValue('dateAmended', $this->getDateAmended());
        $writer->writeDateTimeValue('dateBankReconciled', $this->getDateBankReconciled());
        $writer->writeDateTimeValue('dateBf', $this->getDateBf());
        $writer->writeDateTimeValue('dateEntered', $this->getDateEntered());
        $writer->writeIntegerValue('dateFlag', $this->getDateFlag());
        $writer->writeDateTimeValue('dateFrom', $this->getDateFrom());
        $writer->writeDateTimeValue('dateTo', $this->getDateTo());
        $writer->writeIntegerValue('deletedFlag', $this->getDeletedFlag());
        $writer->writeDateTimeValue('depositDate', $this->getDepositDate());
        $writer->writeEnumValue('depositFlag', $this->getDepositFlag());
        $writer->writeStringValue('details', $this->getDetails());
        $writer->writeIntegerValue('disputed', $this->getDisputed());
        $writer->writeDateTimeValue('dueDate', $this->getDueDate());
        $writer->writeIntegerValue('electronicTrans', $this->getElectronicTrans());
        $writer->writeFloatValue('euroGross', $this->getEuroGross());
        $writer->writeFloatValue('euroRate', $this->getEuroRate());
        $writer->writeIntegerValue('financeCharge', $this->getFinanceCharge());
        $writer->writeFloatValue('foreignAged30', $this->getForeignAged30());
        $writer->writeFloatValue('foreignAged60', $this->getForeignAged60());
        $writer->writeFloatValue('foreignAged90', $this->getForeignAged90());
        $writer->writeFloatValue('foreignAgedBalance', $this->getForeignAgedBalance());
        $writer->writeFloatValue('foreignAgedCum30', $this->getForeignAgedCum30());
        $writer->writeFloatValue('foreignAgedCum60', $this->getForeignAgedCum60());
        $writer->writeFloatValue('foreignAgedCum90', $this->getForeignAgedCum90());
        $writer->writeFloatValue('foreignAgedCumCurrent', $this->getForeignAgedCumCurrent());
        $writer->writeFloatValue('foreignAgedCurrent', $this->getForeignAgedCurrent());
        $writer->writeFloatValue('foreignAgedFuture', $this->getForeignAgedFuture());
        $writer->writeFloatValue('foreignAgedOlder', $this->getForeignAgedOlder());
        $writer->writeFloatValue('foreignAmountPaid', $this->getForeignAmountPaid());
        $writer->writeFloatValue('foreignBankAmount', $this->getForeignBankAmount());
        $writer->writeFloatValue('foreignGrossAmount', $this->getForeignGrossAmount());
        $writer->writeFloatValue('foreignNetAmount', $this->getForeignNetAmount());
        $writer->writeFloatValue('foreignOutstanding', $this->getForeignOutstanding());
        $writer->writeFloatValue('foreignRate', $this->getForeignRate());
        $writer->writeFloatValue('foreignTaxAmount', $this->getForeignTaxAmount());
        $writer->writeFloatValue('grossAmount', $this->getGrossAmount());
        $writer->writeFloatValue('interestRate', $this->getInterestRate());
        $writer->writeStringValue('invRef', $this->getInvRef());
        $writer->writeIntegerValue('invRefNumeric', $this->getInvRefNumeric());
        $writer->writeIntegerValue('isCisTransaction', $this->getIsCisTransaction());
        $writer->writeIntegerValue('isDiscount', $this->getIsDiscount());
        $writer->writeIntegerValue('isInvoicePayment', $this->getIsInvoicePayment());
        $writer->writeIntegerValue('ispReference', $this->getIspReference());
        $writer->writeIntegerValue('itemCount', $this->getItemCount());
        $writer->writeDateTimeValue('lastChargeDate', $this->getLastChargeDate());
        $writer->writeFloatValue('netAmount', $this->getNetAmount());
        $writer->writeFloatValue('outstanding', $this->getOutstanding());
        $writer->writeDateTimeValue('overriddenClosedLedgerDate', $this->getOverriddenClosedLedgerDate());
        $writer->writeEnumValue('paidFlag', $this->getPaidFlag());
        $writer->writeStringValue('paidStatus', $this->getPaidStatus());
        $writer->writeFloatValue('payment', $this->getPayment());
        $writer->writeIntegerValue('postedByDirectDebitSettlement', $this->getPostedByDirectDebitSettlement());
        $writer->writeDateTimeValue('recordCreateDate', $this->getRecordCreateDate());
        $writer->writeIntegerValue('recordDeleted', $this->getRecordDeleted());
        $writer->writeDateTimeValue('recordModifyDate', $this->getRecordModifyDate());
        $writer->writeIntegerValue('recurringEntryId', $this->getRecurringEntryId());
        $writer->writeIntegerValue('revalTransactionFlag', $this->getRevalTransactionFlag());
        $writer->writeStringValue('salesPurchaseRef', $this->getSalesPurchaseRef());
        $writer->writeStringValue('spsRef', $this->getSpsRef());
        $writer->writeFloatValue('taxAmount', $this->getTaxAmount());
        $writer->writeStringValue('taxId', $this->getTaxId());
        $writer->writeIntegerValue('tranNumber', $this->getTranNumber());
        $writer->writeEnumValue('type', $this->getType());
        $writer->writeStringValue('userName', $this->getUserName());
        $writer->writeStringValue('userNameAmended', $this->getUserNameAmended());
    }

    /**
     * Sets the accountRef property value. The accountRef property
     * @param string|null $value Value to set for the accountRef property.
    */
    public function setAccountRef(?string $value): void {
        $this->accountRef = $value;
    }

    /**
     * Sets the aged30 property value. The aged30 property
     * @param float|null $value Value to set for the aged30 property.
    */
    public function setAged30(?float $value): void {
        $this->aged30 = $value;
    }

    /**
     * Sets the aged60 property value. The aged60 property
     * @param float|null $value Value to set for the aged60 property.
    */
    public function setAged60(?float $value): void {
        $this->aged60 = $value;
    }

    /**
     * Sets the aged90 property value. The aged90 property
     * @param float|null $value Value to set for the aged90 property.
    */
    public function setAged90(?float $value): void {
        $this->aged90 = $value;
    }

    /**
     * Sets the agedBalance property value. The agedBalance property
     * @param float|null $value Value to set for the agedBalance property.
    */
    public function setAgedBalance(?float $value): void {
        $this->agedBalance = $value;
    }

    /**
     * Sets the agedCum30 property value. The agedCum30 property
     * @param float|null $value Value to set for the agedCum30 property.
    */
    public function setAgedCum30(?float $value): void {
        $this->agedCum30 = $value;
    }

    /**
     * Sets the agedCum60 property value. The agedCum60 property
     * @param float|null $value Value to set for the agedCum60 property.
    */
    public function setAgedCum60(?float $value): void {
        $this->agedCum60 = $value;
    }

    /**
     * Sets the agedCum90 property value. The agedCum90 property
     * @param float|null $value Value to set for the agedCum90 property.
    */
    public function setAgedCum90(?float $value): void {
        $this->agedCum90 = $value;
    }

    /**
     * Sets the agedCumCurrent property value. The agedCumCurrent property
     * @param float|null $value Value to set for the agedCumCurrent property.
    */
    public function setAgedCumCurrent(?float $value): void {
        $this->agedCumCurrent = $value;
    }

    /**
     * Sets the agedCurrent property value. The agedCurrent property
     * @param float|null $value Value to set for the agedCurrent property.
    */
    public function setAgedCurrent(?float $value): void {
        $this->agedCurrent = $value;
    }

    /**
     * Sets the agedFuture property value. The agedFuture property
     * @param float|null $value Value to set for the agedFuture property.
    */
    public function setAgedFuture(?float $value): void {
        $this->agedFuture = $value;
    }

    /**
     * Sets the agedOlder property value. The agedOlder property
     * @param float|null $value Value to set for the agedOlder property.
    */
    public function setAgedOlder(?float $value): void {
        $this->agedOlder = $value;
    }

    /**
     * Sets the amountPaid property value. The amountPaid property
     * @param float|null $value Value to set for the amountPaid property.
    */
    public function setAmountPaid(?float $value): void {
        $this->amountPaid = $value;
    }

    /**
     * Sets the attachmentAdded property value. The attachmentAdded property
     * @param int|null $value Value to set for the attachmentAdded property.
    */
    public function setAttachmentAdded(?int $value): void {
        $this->attachmentAdded = $value;
    }

    /**
     * Sets the autoPostedTransactionFlag property value. The autoPostedTransactionFlag property
     * @param int|null $value Value to set for the autoPostedTransactionFlag property.
    */
    public function setAutoPostedTransactionFlag(?int $value): void {
        $this->autoPostedTransactionFlag = $value;
    }

    /**
     * Sets the bankAmount property value. The bankAmount property
     * @param float|null $value Value to set for the bankAmount property.
    */
    public function setBankAmount(?float $value): void {
        $this->bankAmount = $value;
    }

    /**
     * Sets the bankChargeAccount property value. The bankChargeAccount property
     * @param string|null $value Value to set for the bankChargeAccount property.
    */
    public function setBankChargeAccount(?string $value): void {
        $this->bankChargeAccount = $value;
    }

    /**
     * Sets the bankChargeIsCustomer property value. The bankChargeIsCustomer property
     * @param int|null $value Value to set for the bankChargeIsCustomer property.
    */
    public function setBankChargeIsCustomer(?int $value): void {
        $this->bankChargeIsCustomer = $value;
    }

    /**
     * Sets the bankCode property value. The bankCode property
     * @param string|null $value Value to set for the bankCode property.
    */
    public function setBankCode(?string $value): void {
        $this->bankCode = $value;
    }

    /**
     * Sets the bankFlag property value. The bankFlag property
     * @param TransactionHeaderAttributesRead_bankFlag|null $value Value to set for the bankFlag property.
    */
    public function setBankFlag(?TransactionHeaderAttributesRead_bankFlag $value): void {
        $this->bankFlag = $value;
    }

    /**
     * Sets the bankName property value. The bankName property
     * @param string|null $value Value to set for the bankName property.
    */
    public function setBankName(?string $value): void {
        $this->bankName = $value;
    }

    /**
     * Sets the bankReconciliationId property value. The bankReconciliationId property
     * @param int|null $value Value to set for the bankReconciliationId property.
    */
    public function setBankReconciliationId(?int $value): void {
        $this->bankReconciliationId = $value;
    }

    /**
     * Sets the bankReconciliationRef property value. The bankReconciliationRef property
     * @param string|null $value Value to set for the bankReconciliationRef property.
    */
    public function setBankReconciliationRef(?string $value): void {
        $this->bankReconciliationRef = $value;
    }

    /**
     * Sets the cisReconciled property value. The cisReconciled property
     * @param int|null $value Value to set for the cisReconciled property.
    */
    public function setCisReconciled(?int $value): void {
        $this->cisReconciled = $value;
    }

    /**
     * Sets the countryCode property value. The countryCode property
     * @param string|null $value Value to set for the countryCode property.
    */
    public function setCountryCode(?string $value): void {
        $this->countryCode = $value;
    }

    /**
     * Sets the currency property value. The currency property
     * @param int|null $value Value to set for the currency property.
    */
    public function setCurrency(?int $value): void {
        $this->currency = $value;
    }

    /**
     * Sets the currencyType property value. The currencyType property
     * @param int|null $value Value to set for the currencyType property.
    */
    public function setCurrencyType(?int $value): void {
        $this->currencyType = $value;
    }

    /**
     * Sets the date property value. The date property
     * @param DateTime|null $value Value to set for the date property.
    */
    public function setDate(?DateTime $value): void {
        $this->date = $value;
    }

    /**
     * Sets the dateAmended property value. The dateAmended property
     * @param DateTime|null $value Value to set for the dateAmended property.
    */
    public function setDateAmended(?DateTime $value): void {
        $this->dateAmended = $value;
    }

    /**
     * Sets the dateBankReconciled property value. The dateBankReconciled property
     * @param DateTime|null $value Value to set for the dateBankReconciled property.
    */
    public function setDateBankReconciled(?DateTime $value): void {
        $this->dateBankReconciled = $value;
    }

    /**
     * Sets the dateBf property value. The dateBf property
     * @param DateTime|null $value Value to set for the dateBf property.
    */
    public function setDateBf(?DateTime $value): void {
        $this->dateBf = $value;
    }

    /**
     * Sets the dateEntered property value. The dateEntered property
     * @param DateTime|null $value Value to set for the dateEntered property.
    */
    public function setDateEntered(?DateTime $value): void {
        $this->dateEntered = $value;
    }

    /**
     * Sets the dateFlag property value. The dateFlag property
     * @param int|null $value Value to set for the dateFlag property.
    */
    public function setDateFlag(?int $value): void {
        $this->dateFlag = $value;
    }

    /**
     * Sets the dateFrom property value. The dateFrom property
     * @param DateTime|null $value Value to set for the dateFrom property.
    */
    public function setDateFrom(?DateTime $value): void {
        $this->dateFrom = $value;
    }

    /**
     * Sets the dateTo property value. The dateTo property
     * @param DateTime|null $value Value to set for the dateTo property.
    */
    public function setDateTo(?DateTime $value): void {
        $this->dateTo = $value;
    }

    /**
     * Sets the deletedFlag property value. The deletedFlag property
     * @param int|null $value Value to set for the deletedFlag property.
    */
    public function setDeletedFlag(?int $value): void {
        $this->deletedFlag = $value;
    }

    /**
     * Sets the depositDate property value. The depositDate property
     * @param DateTime|null $value Value to set for the depositDate property.
    */
    public function setDepositDate(?DateTime $value): void {
        $this->depositDate = $value;
    }

    /**
     * Sets the depositFlag property value. The depositFlag property
     * @param TransactionHeaderAttributesRead_depositFlag|null $value Value to set for the depositFlag property.
    */
    public function setDepositFlag(?TransactionHeaderAttributesRead_depositFlag $value): void {
        $this->depositFlag = $value;
    }

    /**
     * Sets the details property value. The details property
     * @param string|null $value Value to set for the details property.
    */
    public function setDetails(?string $value): void {
        $this->details = $value;
    }

    /**
     * Sets the disputed property value. The disputed property
     * @param int|null $value Value to set for the disputed property.
    */
    public function setDisputed(?int $value): void {
        $this->disputed = $value;
    }

    /**
     * Sets the dueDate property value. The dueDate property
     * @param DateTime|null $value Value to set for the dueDate property.
    */
    public function setDueDate(?DateTime $value): void {
        $this->dueDate = $value;
    }

    /**
     * Sets the electronicTrans property value. The electronicTrans property
     * @param int|null $value Value to set for the electronicTrans property.
    */
    public function setElectronicTrans(?int $value): void {
        $this->electronicTrans = $value;
    }

    /**
     * Sets the euroGross property value. The euroGross property
     * @param float|null $value Value to set for the euroGross property.
    */
    public function setEuroGross(?float $value): void {
        $this->euroGross = $value;
    }

    /**
     * Sets the euroRate property value. The euroRate property
     * @param float|null $value Value to set for the euroRate property.
    */
    public function setEuroRate(?float $value): void {
        $this->euroRate = $value;
    }

    /**
     * Sets the financeCharge property value. The financeCharge property
     * @param int|null $value Value to set for the financeCharge property.
    */
    public function setFinanceCharge(?int $value): void {
        $this->financeCharge = $value;
    }

    /**
     * Sets the foreignAged30 property value. The foreignAged30 property
     * @param float|null $value Value to set for the foreignAged30 property.
    */
    public function setForeignAged30(?float $value): void {
        $this->foreignAged30 = $value;
    }

    /**
     * Sets the foreignAged60 property value. The foreignAged60 property
     * @param float|null $value Value to set for the foreignAged60 property.
    */
    public function setForeignAged60(?float $value): void {
        $this->foreignAged60 = $value;
    }

    /**
     * Sets the foreignAged90 property value. The foreignAged90 property
     * @param float|null $value Value to set for the foreignAged90 property.
    */
    public function setForeignAged90(?float $value): void {
        $this->foreignAged90 = $value;
    }

    /**
     * Sets the foreignAgedBalance property value. The foreignAgedBalance property
     * @param float|null $value Value to set for the foreignAgedBalance property.
    */
    public function setForeignAgedBalance(?float $value): void {
        $this->foreignAgedBalance = $value;
    }

    /**
     * Sets the foreignAgedCum30 property value. The foreignAgedCum30 property
     * @param float|null $value Value to set for the foreignAgedCum30 property.
    */
    public function setForeignAgedCum30(?float $value): void {
        $this->foreignAgedCum30 = $value;
    }

    /**
     * Sets the foreignAgedCum60 property value. The foreignAgedCum60 property
     * @param float|null $value Value to set for the foreignAgedCum60 property.
    */
    public function setForeignAgedCum60(?float $value): void {
        $this->foreignAgedCum60 = $value;
    }

    /**
     * Sets the foreignAgedCum90 property value. The foreignAgedCum90 property
     * @param float|null $value Value to set for the foreignAgedCum90 property.
    */
    public function setForeignAgedCum90(?float $value): void {
        $this->foreignAgedCum90 = $value;
    }

    /**
     * Sets the foreignAgedCumCurrent property value. The foreignAgedCumCurrent property
     * @param float|null $value Value to set for the foreignAgedCumCurrent property.
    */
    public function setForeignAgedCumCurrent(?float $value): void {
        $this->foreignAgedCumCurrent = $value;
    }

    /**
     * Sets the foreignAgedCurrent property value. The foreignAgedCurrent property
     * @param float|null $value Value to set for the foreignAgedCurrent property.
    */
    public function setForeignAgedCurrent(?float $value): void {
        $this->foreignAgedCurrent = $value;
    }

    /**
     * Sets the foreignAgedFuture property value. The foreignAgedFuture property
     * @param float|null $value Value to set for the foreignAgedFuture property.
    */
    public function setForeignAgedFuture(?float $value): void {
        $this->foreignAgedFuture = $value;
    }

    /**
     * Sets the foreignAgedOlder property value. The foreignAgedOlder property
     * @param float|null $value Value to set for the foreignAgedOlder property.
    */
    public function setForeignAgedOlder(?float $value): void {
        $this->foreignAgedOlder = $value;
    }

    /**
     * Sets the foreignAmountPaid property value. The foreignAmountPaid property
     * @param float|null $value Value to set for the foreignAmountPaid property.
    */
    public function setForeignAmountPaid(?float $value): void {
        $this->foreignAmountPaid = $value;
    }

    /**
     * Sets the foreignBankAmount property value. The foreignBankAmount property
     * @param float|null $value Value to set for the foreignBankAmount property.
    */
    public function setForeignBankAmount(?float $value): void {
        $this->foreignBankAmount = $value;
    }

    /**
     * Sets the foreignGrossAmount property value. The foreignGrossAmount property
     * @param float|null $value Value to set for the foreignGrossAmount property.
    */
    public function setForeignGrossAmount(?float $value): void {
        $this->foreignGrossAmount = $value;
    }

    /**
     * Sets the foreignNetAmount property value. The foreignNetAmount property
     * @param float|null $value Value to set for the foreignNetAmount property.
    */
    public function setForeignNetAmount(?float $value): void {
        $this->foreignNetAmount = $value;
    }

    /**
     * Sets the foreignOutstanding property value. The foreignOutstanding property
     * @param float|null $value Value to set for the foreignOutstanding property.
    */
    public function setForeignOutstanding(?float $value): void {
        $this->foreignOutstanding = $value;
    }

    /**
     * Sets the foreignRate property value. The foreignRate property
     * @param float|null $value Value to set for the foreignRate property.
    */
    public function setForeignRate(?float $value): void {
        $this->foreignRate = $value;
    }

    /**
     * Sets the foreignTaxAmount property value. The foreignTaxAmount property
     * @param float|null $value Value to set for the foreignTaxAmount property.
    */
    public function setForeignTaxAmount(?float $value): void {
        $this->foreignTaxAmount = $value;
    }

    /**
     * Sets the grossAmount property value. The grossAmount property
     * @param float|null $value Value to set for the grossAmount property.
    */
    public function setGrossAmount(?float $value): void {
        $this->grossAmount = $value;
    }

    /**
     * Sets the interestRate property value. The interestRate property
     * @param float|null $value Value to set for the interestRate property.
    */
    public function setInterestRate(?float $value): void {
        $this->interestRate = $value;
    }

    /**
     * Sets the invRef property value. The invRef property
     * @param string|null $value Value to set for the invRef property.
    */
    public function setInvRef(?string $value): void {
        $this->invRef = $value;
    }

    /**
     * Sets the invRefNumeric property value. The invRefNumeric property
     * @param int|null $value Value to set for the invRefNumeric property.
    */
    public function setInvRefNumeric(?int $value): void {
        $this->invRefNumeric = $value;
    }

    /**
     * Sets the isCisTransaction property value. The isCisTransaction property
     * @param int|null $value Value to set for the isCisTransaction property.
    */
    public function setIsCisTransaction(?int $value): void {
        $this->isCisTransaction = $value;
    }

    /**
     * Sets the isDiscount property value. The isDiscount property
     * @param int|null $value Value to set for the isDiscount property.
    */
    public function setIsDiscount(?int $value): void {
        $this->isDiscount = $value;
    }

    /**
     * Sets the isInvoicePayment property value. The isInvoicePayment property
     * @param int|null $value Value to set for the isInvoicePayment property.
    */
    public function setIsInvoicePayment(?int $value): void {
        $this->isInvoicePayment = $value;
    }

    /**
     * Sets the ispReference property value. The ispReference property
     * @param int|null $value Value to set for the ispReference property.
    */
    public function setIspReference(?int $value): void {
        $this->ispReference = $value;
    }

    /**
     * Sets the itemCount property value. The itemCount property
     * @param int|null $value Value to set for the itemCount property.
    */
    public function setItemCount(?int $value): void {
        $this->itemCount = $value;
    }

    /**
     * Sets the lastChargeDate property value. The lastChargeDate property
     * @param DateTime|null $value Value to set for the lastChargeDate property.
    */
    public function setLastChargeDate(?DateTime $value): void {
        $this->lastChargeDate = $value;
    }

    /**
     * Sets the netAmount property value. The netAmount property
     * @param float|null $value Value to set for the netAmount property.
    */
    public function setNetAmount(?float $value): void {
        $this->netAmount = $value;
    }

    /**
     * Sets the outstanding property value. The outstanding property
     * @param float|null $value Value to set for the outstanding property.
    */
    public function setOutstanding(?float $value): void {
        $this->outstanding = $value;
    }

    /**
     * Sets the overriddenClosedLedgerDate property value. The overriddenClosedLedgerDate property
     * @param DateTime|null $value Value to set for the overriddenClosedLedgerDate property.
    */
    public function setOverriddenClosedLedgerDate(?DateTime $value): void {
        $this->overriddenClosedLedgerDate = $value;
    }

    /**
     * Sets the paidFlag property value. The paidFlag property
     * @param TransactionHeaderAttributesRead_paidFlag|null $value Value to set for the paidFlag property.
    */
    public function setPaidFlag(?TransactionHeaderAttributesRead_paidFlag $value): void {
        $this->paidFlag = $value;
    }

    /**
     * Sets the paidStatus property value. The paidStatus property
     * @param string|null $value Value to set for the paidStatus property.
    */
    public function setPaidStatus(?string $value): void {
        $this->paidStatus = $value;
    }

    /**
     * Sets the payment property value. The payment property
     * @param float|null $value Value to set for the payment property.
    */
    public function setPayment(?float $value): void {
        $this->payment = $value;
    }

    /**
     * Sets the postedByDirectDebitSettlement property value. The postedByDirectDebitSettlement property
     * @param int|null $value Value to set for the postedByDirectDebitSettlement property.
    */
    public function setPostedByDirectDebitSettlement(?int $value): void {
        $this->postedByDirectDebitSettlement = $value;
    }

    /**
     * Sets the recordCreateDate property value. The recordCreateDate property
     * @param DateTime|null $value Value to set for the recordCreateDate property.
    */
    public function setRecordCreateDate(?DateTime $value): void {
        $this->recordCreateDate = $value;
    }

    /**
     * Sets the recordDeleted property value. The recordDeleted property
     * @param int|null $value Value to set for the recordDeleted property.
    */
    public function setRecordDeleted(?int $value): void {
        $this->recordDeleted = $value;
    }

    /**
     * Sets the recordModifyDate property value. The recordModifyDate property
     * @param DateTime|null $value Value to set for the recordModifyDate property.
    */
    public function setRecordModifyDate(?DateTime $value): void {
        $this->recordModifyDate = $value;
    }

    /**
     * Sets the recurringEntryId property value. The recurringEntryId property
     * @param int|null $value Value to set for the recurringEntryId property.
    */
    public function setRecurringEntryId(?int $value): void {
        $this->recurringEntryId = $value;
    }

    /**
     * Sets the revalTransactionFlag property value. The revalTransactionFlag property
     * @param int|null $value Value to set for the revalTransactionFlag property.
    */
    public function setRevalTransactionFlag(?int $value): void {
        $this->revalTransactionFlag = $value;
    }

    /**
     * Sets the salesPurchaseRef property value. The salesPurchaseRef property
     * @param string|null $value Value to set for the salesPurchaseRef property.
    */
    public function setSalesPurchaseRef(?string $value): void {
        $this->salesPurchaseRef = $value;
    }

    /**
     * Sets the spsRef property value. The spsRef property
     * @param string|null $value Value to set for the spsRef property.
    */
    public function setSpsRef(?string $value): void {
        $this->spsRef = $value;
    }

    /**
     * Sets the taxAmount property value. The taxAmount property
     * @param float|null $value Value to set for the taxAmount property.
    */
    public function setTaxAmount(?float $value): void {
        $this->taxAmount = $value;
    }

    /**
     * Sets the taxId property value. The taxId property
     * @param string|null $value Value to set for the taxId property.
    */
    public function setTaxId(?string $value): void {
        $this->taxId = $value;
    }

    /**
     * Sets the tranNumber property value. The tranNumber property
     * @param int|null $value Value to set for the tranNumber property.
    */
    public function setTranNumber(?int $value): void {
        $this->tranNumber = $value;
    }

    /**
     * Sets the type property value. The type property
     * @param TransactionHeaderAttributesRead_type|null $value Value to set for the type property.
    */
    public function setType(?TransactionHeaderAttributesRead_type $value): void {
        $this->type = $value;
    }

    /**
     * Sets the userName property value. The userName property
     * @param string|null $value Value to set for the userName property.
    */
    public function setUserName(?string $value): void {
        $this->userName = $value;
    }

    /**
     * Sets the userNameAmended property value. The userNameAmended property
     * @param string|null $value Value to set for the userNameAmended property.
    */
    public function setUserNameAmended(?string $value): void {
        $this->userNameAmended = $value;
    }

}
