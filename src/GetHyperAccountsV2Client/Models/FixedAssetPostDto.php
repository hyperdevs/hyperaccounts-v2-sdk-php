<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class FixedAssetPostDto implements Parsable
{
    /**
     * @var string|null $assetRef The assetRef property
    */
    private ?string $assetRef = null;

    /**
     * @var FixedAssetAttributesWrite|null $attributes The attributes property
    */
    private ?FixedAssetAttributesWrite $attributes = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return FixedAssetPostDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): FixedAssetPostDto {
        return new FixedAssetPostDto();
    }

    /**
     * Gets the assetRef property value. The assetRef property
     * @return string|null
    */
    public function getAssetRef(): ?string {
        return $this->assetRef;
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return FixedAssetAttributesWrite|null
    */
    public function getAttributes(): ?FixedAssetAttributesWrite {
        return $this->attributes;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'assetRef' => fn(ParseNode $n) => $o->setAssetRef($n->getStringValue()),
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([FixedAssetAttributesWrite::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeStringValue('assetRef', $this->getAssetRef());
        $writer->writeObjectValue('attributes', $this->getAttributes());
    }

    /**
     * Sets the assetRef property value. The assetRef property
     * @param string|null $value Value to set for the assetRef property.
    */
    public function setAssetRef(?string $value): void {
        $this->assetRef = $value;
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param FixedAssetAttributesWrite|null $value Value to set for the attributes property.
    */
    public function setAttributes(?FixedAssetAttributesWrite $value): void {
        $this->attributes = $value;
    }

}
