<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class TransactionHeaderPostDto implements Parsable
{
    /**
     * @var TransactionHeaderAttributesWrite|null $attributes The attributes property
    */
    private ?TransactionHeaderAttributesWrite $attributes = null;

    /**
     * @var TransactionHeaderReliantChilds|null $reliantChilds The reliantChilds property
    */
    private ?TransactionHeaderReliantChilds $reliantChilds = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return TransactionHeaderPostDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): TransactionHeaderPostDto {
        return new TransactionHeaderPostDto();
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return TransactionHeaderAttributesWrite|null
    */
    public function getAttributes(): ?TransactionHeaderAttributesWrite {
        return $this->attributes;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([TransactionHeaderAttributesWrite::class, 'createFromDiscriminatorValue'])),
            'reliantChilds' => fn(ParseNode $n) => $o->setReliantChilds($n->getObjectValue([TransactionHeaderReliantChilds::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the reliantChilds property value. The reliantChilds property
     * @return TransactionHeaderReliantChilds|null
    */
    public function getReliantChilds(): ?TransactionHeaderReliantChilds {
        return $this->reliantChilds;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('attributes', $this->getAttributes());
        $writer->writeObjectValue('reliantChilds', $this->getReliantChilds());
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param TransactionHeaderAttributesWrite|null $value Value to set for the attributes property.
    */
    public function setAttributes(?TransactionHeaderAttributesWrite $value): void {
        $this->attributes = $value;
    }

    /**
     * Sets the reliantChilds property value. The reliantChilds property
     * @param TransactionHeaderReliantChilds|null $value Value to set for the reliantChilds property.
    */
    public function setReliantChilds(?TransactionHeaderReliantChilds $value): void {
        $this->reliantChilds = $value;
    }

}
