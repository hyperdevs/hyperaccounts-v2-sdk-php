<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class ProjectStatusRelated implements Parsable
{
    /**
     * @var int|null $statusId The statusId property
    */
    private ?int $statusId = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return ProjectStatusRelated
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): ProjectStatusRelated {
        return new ProjectStatusRelated();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'statusId' => fn(ParseNode $n) => $o->setStatusId($n->getIntegerValue()),
        ];
    }

    /**
     * Gets the statusId property value. The statusId property
     * @return int|null
    */
    public function getStatusId(): ?int {
        return $this->statusId;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeIntegerValue('statusId', $this->getStatusId());
    }

    /**
     * Sets the statusId property value. The statusId property
     * @param int|null $value Value to set for the statusId property.
    */
    public function setStatusId(?int $value): void {
        $this->statusId = $value;
    }

}
