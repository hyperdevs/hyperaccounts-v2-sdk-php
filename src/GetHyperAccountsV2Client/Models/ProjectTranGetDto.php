<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class ProjectTranGetDto implements Parsable
{
    /**
     * @var ProjectTranAttributesRead|null $attributes The attributes property
    */
    private ?ProjectTranAttributesRead $attributes = null;

    /**
     * @var ProjectTranIncluded|null $included The included property
    */
    private ?ProjectTranIncluded $included = null;

    /**
     * @var ResourceLinks|null $links The links property
    */
    private ?ResourceLinks $links = null;

    /**
     * @var int|null $projectTranId The projectTranId property
    */
    private ?int $projectTranId = null;

    /**
     * @var ProjectTranRelationships|null $relationships The relationships property
    */
    private ?ProjectTranRelationships $relationships = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return ProjectTranGetDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): ProjectTranGetDto {
        return new ProjectTranGetDto();
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return ProjectTranAttributesRead|null
    */
    public function getAttributes(): ?ProjectTranAttributesRead {
        return $this->attributes;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([ProjectTranAttributesRead::class, 'createFromDiscriminatorValue'])),
            'included' => fn(ParseNode $n) => $o->setIncluded($n->getObjectValue([ProjectTranIncluded::class, 'createFromDiscriminatorValue'])),
            'links' => fn(ParseNode $n) => $o->setLinks($n->getObjectValue([ResourceLinks::class, 'createFromDiscriminatorValue'])),
            'projectTranId' => fn(ParseNode $n) => $o->setProjectTranId($n->getIntegerValue()),
            'relationships' => fn(ParseNode $n) => $o->setRelationships($n->getObjectValue([ProjectTranRelationships::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the included property value. The included property
     * @return ProjectTranIncluded|null
    */
    public function getIncluded(): ?ProjectTranIncluded {
        return $this->included;
    }

    /**
     * Gets the links property value. The links property
     * @return ResourceLinks|null
    */
    public function getLinks(): ?ResourceLinks {
        return $this->links;
    }

    /**
     * Gets the projectTranId property value. The projectTranId property
     * @return int|null
    */
    public function getProjectTranId(): ?int {
        return $this->projectTranId;
    }

    /**
     * Gets the relationships property value. The relationships property
     * @return ProjectTranRelationships|null
    */
    public function getRelationships(): ?ProjectTranRelationships {
        return $this->relationships;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('attributes', $this->getAttributes());
        $writer->writeObjectValue('included', $this->getIncluded());
        $writer->writeObjectValue('links', $this->getLinks());
        $writer->writeIntegerValue('projectTranId', $this->getProjectTranId());
        $writer->writeObjectValue('relationships', $this->getRelationships());
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param ProjectTranAttributesRead|null $value Value to set for the attributes property.
    */
    public function setAttributes(?ProjectTranAttributesRead $value): void {
        $this->attributes = $value;
    }

    /**
     * Sets the included property value. The included property
     * @param ProjectTranIncluded|null $value Value to set for the included property.
    */
    public function setIncluded(?ProjectTranIncluded $value): void {
        $this->included = $value;
    }

    /**
     * Sets the links property value. The links property
     * @param ResourceLinks|null $value Value to set for the links property.
    */
    public function setLinks(?ResourceLinks $value): void {
        $this->links = $value;
    }

    /**
     * Sets the projectTranId property value. The projectTranId property
     * @param int|null $value Value to set for the projectTranId property.
    */
    public function setProjectTranId(?int $value): void {
        $this->projectTranId = $value;
    }

    /**
     * Sets the relationships property value. The relationships property
     * @param ProjectTranRelationships|null $value Value to set for the relationships property.
    */
    public function setRelationships(?ProjectTranRelationships $value): void {
        $this->relationships = $value;
    }

}
