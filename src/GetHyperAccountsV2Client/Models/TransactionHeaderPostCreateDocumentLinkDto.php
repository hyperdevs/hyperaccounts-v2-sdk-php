<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class TransactionHeaderPostCreateDocumentLinkDto implements Parsable
{
    /**
     * @var TransactionHeaderCreateDocumentAttributesWrite|null $attributes The attributes property
    */
    private ?TransactionHeaderCreateDocumentAttributesWrite $attributes = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return TransactionHeaderPostCreateDocumentLinkDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): TransactionHeaderPostCreateDocumentLinkDto {
        return new TransactionHeaderPostCreateDocumentLinkDto();
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return TransactionHeaderCreateDocumentAttributesWrite|null
    */
    public function getAttributes(): ?TransactionHeaderCreateDocumentAttributesWrite {
        return $this->attributes;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([TransactionHeaderCreateDocumentAttributesWrite::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('attributes', $this->getAttributes());
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param TransactionHeaderCreateDocumentAttributesWrite|null $value Value to set for the attributes property.
    */
    public function setAttributes(?TransactionHeaderCreateDocumentAttributesWrite $value): void {
        $this->attributes = $value;
    }

}
