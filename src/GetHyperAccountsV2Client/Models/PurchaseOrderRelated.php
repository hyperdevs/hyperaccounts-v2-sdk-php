<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class PurchaseOrderRelated implements Parsable
{
    /**
     * @var int|null $orderNumber The orderNumber property
    */
    private ?int $orderNumber = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return PurchaseOrderRelated
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): PurchaseOrderRelated {
        return new PurchaseOrderRelated();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'orderNumber' => fn(ParseNode $n) => $o->setOrderNumber($n->getIntegerValue()),
        ];
    }

    /**
     * Gets the orderNumber property value. The orderNumber property
     * @return int|null
    */
    public function getOrderNumber(): ?int {
        return $this->orderNumber;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeIntegerValue('orderNumber', $this->getOrderNumber());
    }

    /**
     * Sets the orderNumber property value. The orderNumber property
     * @param int|null $value Value to set for the orderNumber property.
    */
    public function setOrderNumber(?int $value): void {
        $this->orderNumber = $value;
    }

}
