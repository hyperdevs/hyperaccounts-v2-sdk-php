<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class WebhookAttributesWrite implements Parsable
{
    /**
     * @var bool|null $active The active property
    */
    private ?bool $active = null;

    /**
     * @var int|null $apiVersion The apiVersion property
    */
    private ?int $apiVersion = null;

    /**
     * @var string|null $authorizationHeader The authorizationHeader property
    */
    private ?string $authorizationHeader = null;

    /**
     * @var string|null $authorizationToken The authorizationToken property
    */
    private ?string $authorizationToken = null;

    /**
     * @var WebhookAttributesWrite_sageFileName|null $sageFileName The sageFileName property
    */
    private ?WebhookAttributesWrite_sageFileName $sageFileName = null;

    /**
     * @var string|null $url The url property
    */
    private ?string $url = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return WebhookAttributesWrite
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): WebhookAttributesWrite {
        return new WebhookAttributesWrite();
    }

    /**
     * Gets the active property value. The active property
     * @return bool|null
    */
    public function getActive(): ?bool {
        return $this->active;
    }

    /**
     * Gets the apiVersion property value. The apiVersion property
     * @return int|null
    */
    public function getApiVersion(): ?int {
        return $this->apiVersion;
    }

    /**
     * Gets the authorizationHeader property value. The authorizationHeader property
     * @return string|null
    */
    public function getAuthorizationHeader(): ?string {
        return $this->authorizationHeader;
    }

    /**
     * Gets the authorizationToken property value. The authorizationToken property
     * @return string|null
    */
    public function getAuthorizationToken(): ?string {
        return $this->authorizationToken;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'active' => fn(ParseNode $n) => $o->setActive($n->getBooleanValue()),
            'apiVersion' => fn(ParseNode $n) => $o->setApiVersion($n->getIntegerValue()),
            'authorizationHeader' => fn(ParseNode $n) => $o->setAuthorizationHeader($n->getStringValue()),
            'authorizationToken' => fn(ParseNode $n) => $o->setAuthorizationToken($n->getStringValue()),
            'sageFileName' => fn(ParseNode $n) => $o->setSageFileName($n->getEnumValue(WebhookAttributesWrite_sageFileName::class)),
            'url' => fn(ParseNode $n) => $o->setUrl($n->getStringValue()),
        ];
    }

    /**
     * Gets the sageFileName property value. The sageFileName property
     * @return WebhookAttributesWrite_sageFileName|null
    */
    public function getSageFileName(): ?WebhookAttributesWrite_sageFileName {
        return $this->sageFileName;
    }

    /**
     * Gets the url property value. The url property
     * @return string|null
    */
    public function getUrl(): ?string {
        return $this->url;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeBooleanValue('active', $this->getActive());
        $writer->writeIntegerValue('apiVersion', $this->getApiVersion());
        $writer->writeStringValue('authorizationHeader', $this->getAuthorizationHeader());
        $writer->writeStringValue('authorizationToken', $this->getAuthorizationToken());
        $writer->writeEnumValue('sageFileName', $this->getSageFileName());
        $writer->writeStringValue('url', $this->getUrl());
    }

    /**
     * Sets the active property value. The active property
     * @param bool|null $value Value to set for the active property.
    */
    public function setActive(?bool $value): void {
        $this->active = $value;
    }

    /**
     * Sets the apiVersion property value. The apiVersion property
     * @param int|null $value Value to set for the apiVersion property.
    */
    public function setApiVersion(?int $value): void {
        $this->apiVersion = $value;
    }

    /**
     * Sets the authorizationHeader property value. The authorizationHeader property
     * @param string|null $value Value to set for the authorizationHeader property.
    */
    public function setAuthorizationHeader(?string $value): void {
        $this->authorizationHeader = $value;
    }

    /**
     * Sets the authorizationToken property value. The authorizationToken property
     * @param string|null $value Value to set for the authorizationToken property.
    */
    public function setAuthorizationToken(?string $value): void {
        $this->authorizationToken = $value;
    }

    /**
     * Sets the sageFileName property value. The sageFileName property
     * @param WebhookAttributesWrite_sageFileName|null $value Value to set for the sageFileName property.
    */
    public function setSageFileName(?WebhookAttributesWrite_sageFileName $value): void {
        $this->sageFileName = $value;
    }

    /**
     * Sets the url property value. The url property
     * @param string|null $value Value to set for the url property.
    */
    public function setUrl(?string $value): void {
        $this->url = $value;
    }

}
