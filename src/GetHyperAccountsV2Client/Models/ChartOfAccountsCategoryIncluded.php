<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class ChartOfAccountsCategoryIncluded implements Parsable
{
    /**
     * @var ChartOfAccountsGetDto|null $chart The chart property
    */
    private ?ChartOfAccountsGetDto $chart = null;

    /**
     * @var ChartOfAccountsCategoryTitleGetDto|null $chartOfAccountsCategoryTitle The chartOfAccountsCategoryTitle property
    */
    private ?ChartOfAccountsCategoryTitleGetDto $chartOfAccountsCategoryTitle = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return ChartOfAccountsCategoryIncluded
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): ChartOfAccountsCategoryIncluded {
        return new ChartOfAccountsCategoryIncluded();
    }

    /**
     * Gets the chart property value. The chart property
     * @return ChartOfAccountsGetDto|null
    */
    public function getChart(): ?ChartOfAccountsGetDto {
        return $this->chart;
    }

    /**
     * Gets the chartOfAccountsCategoryTitle property value. The chartOfAccountsCategoryTitle property
     * @return ChartOfAccountsCategoryTitleGetDto|null
    */
    public function getChartOfAccountsCategoryTitle(): ?ChartOfAccountsCategoryTitleGetDto {
        return $this->chartOfAccountsCategoryTitle;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'chart' => fn(ParseNode $n) => $o->setChart($n->getObjectValue([ChartOfAccountsGetDto::class, 'createFromDiscriminatorValue'])),
            'chartOfAccountsCategoryTitle' => fn(ParseNode $n) => $o->setChartOfAccountsCategoryTitle($n->getObjectValue([ChartOfAccountsCategoryTitleGetDto::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('chart', $this->getChart());
        $writer->writeObjectValue('chartOfAccountsCategoryTitle', $this->getChartOfAccountsCategoryTitle());
    }

    /**
     * Sets the chart property value. The chart property
     * @param ChartOfAccountsGetDto|null $value Value to set for the chart property.
    */
    public function setChart(?ChartOfAccountsGetDto $value): void {
        $this->chart = $value;
    }

    /**
     * Sets the chartOfAccountsCategoryTitle property value. The chartOfAccountsCategoryTitle property
     * @param ChartOfAccountsCategoryTitleGetDto|null $value Value to set for the chartOfAccountsCategoryTitle property.
    */
    public function setChartOfAccountsCategoryTitle(?ChartOfAccountsCategoryTitleGetDto $value): void {
        $this->chartOfAccountsCategoryTitle = $value;
    }

}
