<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Enum;

class TransactionSplitAttributesRead_type extends Enum {
    public const S_I = 'SI';
    public const S_C = 'SC';
    public const S_R = 'SR';
    public const S_A = 'SA';
    public const S_D = 'SD';
    public const P_I = 'PI';
    public const P_C = 'PC';
    public const P_P = 'PP';
    public const P_A = 'PA';
    public const P_D = 'PD';
    public const B_P = 'BP';
    public const B_R = 'BR';
    public const C_P = 'CP';
    public const C_R = 'CR';
    public const J_D = 'JD';
    public const J_C = 'JC';
    public const V_P = 'VP';
    public const V_R = 'VR';
    public const C_C = 'CC';
    public const C_D = 'CD';
    public const P_A_I = 'PAI';
    public const P_A_O = 'PAO';
    public const C_O = 'CO';
    public const S_P = 'SP';
    public const P_R = 'PR';
}
