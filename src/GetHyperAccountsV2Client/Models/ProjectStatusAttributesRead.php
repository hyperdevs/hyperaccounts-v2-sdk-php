<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use DateTime;
use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class ProjectStatusAttributesRead implements Parsable
{
    /**
     * @var int|null $allowDelete The allowDelete property
    */
    private ?int $allowDelete = null;

    /**
     * @var int|null $allowPosting The allowPosting property
    */
    private ?int $allowPosting = null;

    /**
     * @var int|null $isDefault The isDefault property
    */
    private ?int $isDefault = null;

    /**
     * @var string|null $name The name property
    */
    private ?string $name = null;

    /**
     * @var DateTime|null $recordCreateDate The recordCreateDate property
    */
    private ?DateTime $recordCreateDate = null;

    /**
     * @var int|null $recordDeleted The recordDeleted property
    */
    private ?int $recordDeleted = null;

    /**
     * @var DateTime|null $recordModifyDate The recordModifyDate property
    */
    private ?DateTime $recordModifyDate = null;

    /**
     * @var string|null $reference The reference property
    */
    private ?string $reference = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return ProjectStatusAttributesRead
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): ProjectStatusAttributesRead {
        return new ProjectStatusAttributesRead();
    }

    /**
     * Gets the allowDelete property value. The allowDelete property
     * @return int|null
    */
    public function getAllowDelete(): ?int {
        return $this->allowDelete;
    }

    /**
     * Gets the allowPosting property value. The allowPosting property
     * @return int|null
    */
    public function getAllowPosting(): ?int {
        return $this->allowPosting;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'allowDelete' => fn(ParseNode $n) => $o->setAllowDelete($n->getIntegerValue()),
            'allowPosting' => fn(ParseNode $n) => $o->setAllowPosting($n->getIntegerValue()),
            'isDefault' => fn(ParseNode $n) => $o->setIsDefault($n->getIntegerValue()),
            'name' => fn(ParseNode $n) => $o->setName($n->getStringValue()),
            'recordCreateDate' => fn(ParseNode $n) => $o->setRecordCreateDate($n->getDateTimeValue()),
            'recordDeleted' => fn(ParseNode $n) => $o->setRecordDeleted($n->getIntegerValue()),
            'recordModifyDate' => fn(ParseNode $n) => $o->setRecordModifyDate($n->getDateTimeValue()),
            'reference' => fn(ParseNode $n) => $o->setReference($n->getStringValue()),
        ];
    }

    /**
     * Gets the isDefault property value. The isDefault property
     * @return int|null
    */
    public function getIsDefault(): ?int {
        return $this->isDefault;
    }

    /**
     * Gets the name property value. The name property
     * @return string|null
    */
    public function getName(): ?string {
        return $this->name;
    }

    /**
     * Gets the recordCreateDate property value. The recordCreateDate property
     * @return DateTime|null
    */
    public function getRecordCreateDate(): ?DateTime {
        return $this->recordCreateDate;
    }

    /**
     * Gets the recordDeleted property value. The recordDeleted property
     * @return int|null
    */
    public function getRecordDeleted(): ?int {
        return $this->recordDeleted;
    }

    /**
     * Gets the recordModifyDate property value. The recordModifyDate property
     * @return DateTime|null
    */
    public function getRecordModifyDate(): ?DateTime {
        return $this->recordModifyDate;
    }

    /**
     * Gets the reference property value. The reference property
     * @return string|null
    */
    public function getReference(): ?string {
        return $this->reference;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeIntegerValue('allowDelete', $this->getAllowDelete());
        $writer->writeIntegerValue('allowPosting', $this->getAllowPosting());
        $writer->writeIntegerValue('isDefault', $this->getIsDefault());
        $writer->writeStringValue('name', $this->getName());
        $writer->writeDateTimeValue('recordCreateDate', $this->getRecordCreateDate());
        $writer->writeIntegerValue('recordDeleted', $this->getRecordDeleted());
        $writer->writeDateTimeValue('recordModifyDate', $this->getRecordModifyDate());
        $writer->writeStringValue('reference', $this->getReference());
    }

    /**
     * Sets the allowDelete property value. The allowDelete property
     * @param int|null $value Value to set for the allowDelete property.
    */
    public function setAllowDelete(?int $value): void {
        $this->allowDelete = $value;
    }

    /**
     * Sets the allowPosting property value. The allowPosting property
     * @param int|null $value Value to set for the allowPosting property.
    */
    public function setAllowPosting(?int $value): void {
        $this->allowPosting = $value;
    }

    /**
     * Sets the isDefault property value. The isDefault property
     * @param int|null $value Value to set for the isDefault property.
    */
    public function setIsDefault(?int $value): void {
        $this->isDefault = $value;
    }

    /**
     * Sets the name property value. The name property
     * @param string|null $value Value to set for the name property.
    */
    public function setName(?string $value): void {
        $this->name = $value;
    }

    /**
     * Sets the recordCreateDate property value. The recordCreateDate property
     * @param DateTime|null $value Value to set for the recordCreateDate property.
    */
    public function setRecordCreateDate(?DateTime $value): void {
        $this->recordCreateDate = $value;
    }

    /**
     * Sets the recordDeleted property value. The recordDeleted property
     * @param int|null $value Value to set for the recordDeleted property.
    */
    public function setRecordDeleted(?int $value): void {
        $this->recordDeleted = $value;
    }

    /**
     * Sets the recordModifyDate property value. The recordModifyDate property
     * @param DateTime|null $value Value to set for the recordModifyDate property.
    */
    public function setRecordModifyDate(?DateTime $value): void {
        $this->recordModifyDate = $value;
    }

    /**
     * Sets the reference property value. The reference property
     * @param string|null $value Value to set for the reference property.
    */
    public function setReference(?string $value): void {
        $this->reference = $value;
    }

}
