<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use DateTime;
use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class ProjectOnlyTransactionAttributesRead implements Parsable
{
    /**
     * @var string|null $accountRef The accountRef property
    */
    private ?string $accountRef = null;

    /**
     * @var string|null $deptName The deptName property
    */
    private ?string $deptName = null;

    /**
     * @var int|null $deptNumber The deptNumber property
    */
    private ?int $deptNumber = null;

    /**
     * @var string|null $details The details property
    */
    private ?string $details = null;

    /**
     * @var string|null $extraRef The extraRef property
    */
    private ?string $extraRef = null;

    /**
     * @var string|null $nominalCode The nominalCode property
    */
    private ?string $nominalCode = null;

    /**
     * @var float|null $quantity The quantity property
    */
    private ?float $quantity = null;

    /**
     * @var float|null $rate The rate property
    */
    private ?float $rate = null;

    /**
     * @var DateTime|null $recordCreateDate The recordCreateDate property
    */
    private ?DateTime $recordCreateDate = null;

    /**
     * @var int|null $recordDeleted The recordDeleted property
    */
    private ?int $recordDeleted = null;

    /**
     * @var DateTime|null $recordModifyDate The recordModifyDate property
    */
    private ?DateTime $recordModifyDate = null;

    /**
     * @var string|null $reference The reference property
    */
    private ?string $reference = null;

    /**
     * @var float|null $taxAmount The taxAmount property
    */
    private ?float $taxAmount = null;

    /**
     * @var string|null $taxCode The taxCode property
    */
    private ?string $taxCode = null;

    /**
     * @var ProjectOnlyTransactionAttributesRead_type|null $type The type property
    */
    private ?ProjectOnlyTransactionAttributesRead_type $type = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return ProjectOnlyTransactionAttributesRead
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): ProjectOnlyTransactionAttributesRead {
        return new ProjectOnlyTransactionAttributesRead();
    }

    /**
     * Gets the accountRef property value. The accountRef property
     * @return string|null
    */
    public function getAccountRef(): ?string {
        return $this->accountRef;
    }

    /**
     * Gets the deptName property value. The deptName property
     * @return string|null
    */
    public function getDeptName(): ?string {
        return $this->deptName;
    }

    /**
     * Gets the deptNumber property value. The deptNumber property
     * @return int|null
    */
    public function getDeptNumber(): ?int {
        return $this->deptNumber;
    }

    /**
     * Gets the details property value. The details property
     * @return string|null
    */
    public function getDetails(): ?string {
        return $this->details;
    }

    /**
     * Gets the extraRef property value. The extraRef property
     * @return string|null
    */
    public function getExtraRef(): ?string {
        return $this->extraRef;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'accountRef' => fn(ParseNode $n) => $o->setAccountRef($n->getStringValue()),
            'deptName' => fn(ParseNode $n) => $o->setDeptName($n->getStringValue()),
            'deptNumber' => fn(ParseNode $n) => $o->setDeptNumber($n->getIntegerValue()),
            'details' => fn(ParseNode $n) => $o->setDetails($n->getStringValue()),
            'extraRef' => fn(ParseNode $n) => $o->setExtraRef($n->getStringValue()),
            'nominalCode' => fn(ParseNode $n) => $o->setNominalCode($n->getStringValue()),
            'quantity' => fn(ParseNode $n) => $o->setQuantity($n->getFloatValue()),
            'rate' => fn(ParseNode $n) => $o->setRate($n->getFloatValue()),
            'recordCreateDate' => fn(ParseNode $n) => $o->setRecordCreateDate($n->getDateTimeValue()),
            'recordDeleted' => fn(ParseNode $n) => $o->setRecordDeleted($n->getIntegerValue()),
            'recordModifyDate' => fn(ParseNode $n) => $o->setRecordModifyDate($n->getDateTimeValue()),
            'reference' => fn(ParseNode $n) => $o->setReference($n->getStringValue()),
            'taxAmount' => fn(ParseNode $n) => $o->setTaxAmount($n->getFloatValue()),
            'taxCode' => fn(ParseNode $n) => $o->setTaxCode($n->getStringValue()),
            'type' => fn(ParseNode $n) => $o->setType($n->getEnumValue(ProjectOnlyTransactionAttributesRead_type::class)),
        ];
    }

    /**
     * Gets the nominalCode property value. The nominalCode property
     * @return string|null
    */
    public function getNominalCode(): ?string {
        return $this->nominalCode;
    }

    /**
     * Gets the quantity property value. The quantity property
     * @return float|null
    */
    public function getQuantity(): ?float {
        return $this->quantity;
    }

    /**
     * Gets the rate property value. The rate property
     * @return float|null
    */
    public function getRate(): ?float {
        return $this->rate;
    }

    /**
     * Gets the recordCreateDate property value. The recordCreateDate property
     * @return DateTime|null
    */
    public function getRecordCreateDate(): ?DateTime {
        return $this->recordCreateDate;
    }

    /**
     * Gets the recordDeleted property value. The recordDeleted property
     * @return int|null
    */
    public function getRecordDeleted(): ?int {
        return $this->recordDeleted;
    }

    /**
     * Gets the recordModifyDate property value. The recordModifyDate property
     * @return DateTime|null
    */
    public function getRecordModifyDate(): ?DateTime {
        return $this->recordModifyDate;
    }

    /**
     * Gets the reference property value. The reference property
     * @return string|null
    */
    public function getReference(): ?string {
        return $this->reference;
    }

    /**
     * Gets the taxAmount property value. The taxAmount property
     * @return float|null
    */
    public function getTaxAmount(): ?float {
        return $this->taxAmount;
    }

    /**
     * Gets the taxCode property value. The taxCode property
     * @return string|null
    */
    public function getTaxCode(): ?string {
        return $this->taxCode;
    }

    /**
     * Gets the type property value. The type property
     * @return ProjectOnlyTransactionAttributesRead_type|null
    */
    public function getType(): ?ProjectOnlyTransactionAttributesRead_type {
        return $this->type;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeStringValue('accountRef', $this->getAccountRef());
        $writer->writeStringValue('deptName', $this->getDeptName());
        $writer->writeIntegerValue('deptNumber', $this->getDeptNumber());
        $writer->writeStringValue('details', $this->getDetails());
        $writer->writeStringValue('extraRef', $this->getExtraRef());
        $writer->writeStringValue('nominalCode', $this->getNominalCode());
        $writer->writeFloatValue('quantity', $this->getQuantity());
        $writer->writeFloatValue('rate', $this->getRate());
        $writer->writeDateTimeValue('recordCreateDate', $this->getRecordCreateDate());
        $writer->writeIntegerValue('recordDeleted', $this->getRecordDeleted());
        $writer->writeDateTimeValue('recordModifyDate', $this->getRecordModifyDate());
        $writer->writeStringValue('reference', $this->getReference());
        $writer->writeFloatValue('taxAmount', $this->getTaxAmount());
        $writer->writeStringValue('taxCode', $this->getTaxCode());
        $writer->writeEnumValue('type', $this->getType());
    }

    /**
     * Sets the accountRef property value. The accountRef property
     * @param string|null $value Value to set for the accountRef property.
    */
    public function setAccountRef(?string $value): void {
        $this->accountRef = $value;
    }

    /**
     * Sets the deptName property value. The deptName property
     * @param string|null $value Value to set for the deptName property.
    */
    public function setDeptName(?string $value): void {
        $this->deptName = $value;
    }

    /**
     * Sets the deptNumber property value. The deptNumber property
     * @param int|null $value Value to set for the deptNumber property.
    */
    public function setDeptNumber(?int $value): void {
        $this->deptNumber = $value;
    }

    /**
     * Sets the details property value. The details property
     * @param string|null $value Value to set for the details property.
    */
    public function setDetails(?string $value): void {
        $this->details = $value;
    }

    /**
     * Sets the extraRef property value. The extraRef property
     * @param string|null $value Value to set for the extraRef property.
    */
    public function setExtraRef(?string $value): void {
        $this->extraRef = $value;
    }

    /**
     * Sets the nominalCode property value. The nominalCode property
     * @param string|null $value Value to set for the nominalCode property.
    */
    public function setNominalCode(?string $value): void {
        $this->nominalCode = $value;
    }

    /**
     * Sets the quantity property value. The quantity property
     * @param float|null $value Value to set for the quantity property.
    */
    public function setQuantity(?float $value): void {
        $this->quantity = $value;
    }

    /**
     * Sets the rate property value. The rate property
     * @param float|null $value Value to set for the rate property.
    */
    public function setRate(?float $value): void {
        $this->rate = $value;
    }

    /**
     * Sets the recordCreateDate property value. The recordCreateDate property
     * @param DateTime|null $value Value to set for the recordCreateDate property.
    */
    public function setRecordCreateDate(?DateTime $value): void {
        $this->recordCreateDate = $value;
    }

    /**
     * Sets the recordDeleted property value. The recordDeleted property
     * @param int|null $value Value to set for the recordDeleted property.
    */
    public function setRecordDeleted(?int $value): void {
        $this->recordDeleted = $value;
    }

    /**
     * Sets the recordModifyDate property value. The recordModifyDate property
     * @param DateTime|null $value Value to set for the recordModifyDate property.
    */
    public function setRecordModifyDate(?DateTime $value): void {
        $this->recordModifyDate = $value;
    }

    /**
     * Sets the reference property value. The reference property
     * @param string|null $value Value to set for the reference property.
    */
    public function setReference(?string $value): void {
        $this->reference = $value;
    }

    /**
     * Sets the taxAmount property value. The taxAmount property
     * @param float|null $value Value to set for the taxAmount property.
    */
    public function setTaxAmount(?float $value): void {
        $this->taxAmount = $value;
    }

    /**
     * Sets the taxCode property value. The taxCode property
     * @param string|null $value Value to set for the taxCode property.
    */
    public function setTaxCode(?string $value): void {
        $this->taxCode = $value;
    }

    /**
     * Sets the type property value. The type property
     * @param ProjectOnlyTransactionAttributesRead_type|null $value Value to set for the type property.
    */
    public function setType(?ProjectOnlyTransactionAttributesRead_type $value): void {
        $this->type = $value;
    }

}
