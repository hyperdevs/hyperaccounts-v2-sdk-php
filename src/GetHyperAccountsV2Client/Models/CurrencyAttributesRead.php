<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use DateTime;
use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class CurrencyAttributesRead implements Parsable
{
    /**
     * @var int|null $baseCurrency The baseCurrency property
    */
    private ?int $baseCurrency = null;

    /**
     * @var string|null $code The code property
    */
    private ?string $code = null;

    /**
     * @var int|null $emuMember The emuMember property
    */
    private ?int $emuMember = null;

    /**
     * @var string|null $majorCurrencyUnit The majorCurrencyUnit property
    */
    private ?string $majorCurrencyUnit = null;

    /**
     * @var string|null $minorCurrencyUnit The minorCurrencyUnit property
    */
    private ?string $minorCurrencyUnit = null;

    /**
     * @var string|null $name The name property
    */
    private ?string $name = null;

    /**
     * @var float|null $rate The rate property
    */
    private ?float $rate = null;

    /**
     * @var DateTime|null $recordCreateDate The recordCreateDate property
    */
    private ?DateTime $recordCreateDate = null;

    /**
     * @var int|null $recordDeleted The recordDeleted property
    */
    private ?int $recordDeleted = null;

    /**
     * @var DateTime|null $recordModifyDate The recordModifyDate property
    */
    private ?DateTime $recordModifyDate = null;

    /**
     * @var string|null $symbol The symbol property
    */
    private ?string $symbol = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return CurrencyAttributesRead
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): CurrencyAttributesRead {
        return new CurrencyAttributesRead();
    }

    /**
     * Gets the baseCurrency property value. The baseCurrency property
     * @return int|null
    */
    public function getBaseCurrency(): ?int {
        return $this->baseCurrency;
    }

    /**
     * Gets the code property value. The code property
     * @return string|null
    */
    public function getCode(): ?string {
        return $this->code;
    }

    /**
     * Gets the emuMember property value. The emuMember property
     * @return int|null
    */
    public function getEmuMember(): ?int {
        return $this->emuMember;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'baseCurrency' => fn(ParseNode $n) => $o->setBaseCurrency($n->getIntegerValue()),
            'code' => fn(ParseNode $n) => $o->setCode($n->getStringValue()),
            'emuMember' => fn(ParseNode $n) => $o->setEmuMember($n->getIntegerValue()),
            'majorCurrencyUnit' => fn(ParseNode $n) => $o->setMajorCurrencyUnit($n->getStringValue()),
            'minorCurrencyUnit' => fn(ParseNode $n) => $o->setMinorCurrencyUnit($n->getStringValue()),
            'name' => fn(ParseNode $n) => $o->setName($n->getStringValue()),
            'rate' => fn(ParseNode $n) => $o->setRate($n->getFloatValue()),
            'recordCreateDate' => fn(ParseNode $n) => $o->setRecordCreateDate($n->getDateTimeValue()),
            'recordDeleted' => fn(ParseNode $n) => $o->setRecordDeleted($n->getIntegerValue()),
            'recordModifyDate' => fn(ParseNode $n) => $o->setRecordModifyDate($n->getDateTimeValue()),
            'symbol' => fn(ParseNode $n) => $o->setSymbol($n->getStringValue()),
        ];
    }

    /**
     * Gets the majorCurrencyUnit property value. The majorCurrencyUnit property
     * @return string|null
    */
    public function getMajorCurrencyUnit(): ?string {
        return $this->majorCurrencyUnit;
    }

    /**
     * Gets the minorCurrencyUnit property value. The minorCurrencyUnit property
     * @return string|null
    */
    public function getMinorCurrencyUnit(): ?string {
        return $this->minorCurrencyUnit;
    }

    /**
     * Gets the name property value. The name property
     * @return string|null
    */
    public function getName(): ?string {
        return $this->name;
    }

    /**
     * Gets the rate property value. The rate property
     * @return float|null
    */
    public function getRate(): ?float {
        return $this->rate;
    }

    /**
     * Gets the recordCreateDate property value. The recordCreateDate property
     * @return DateTime|null
    */
    public function getRecordCreateDate(): ?DateTime {
        return $this->recordCreateDate;
    }

    /**
     * Gets the recordDeleted property value. The recordDeleted property
     * @return int|null
    */
    public function getRecordDeleted(): ?int {
        return $this->recordDeleted;
    }

    /**
     * Gets the recordModifyDate property value. The recordModifyDate property
     * @return DateTime|null
    */
    public function getRecordModifyDate(): ?DateTime {
        return $this->recordModifyDate;
    }

    /**
     * Gets the symbol property value. The symbol property
     * @return string|null
    */
    public function getSymbol(): ?string {
        return $this->symbol;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeIntegerValue('baseCurrency', $this->getBaseCurrency());
        $writer->writeStringValue('code', $this->getCode());
        $writer->writeIntegerValue('emuMember', $this->getEmuMember());
        $writer->writeStringValue('majorCurrencyUnit', $this->getMajorCurrencyUnit());
        $writer->writeStringValue('minorCurrencyUnit', $this->getMinorCurrencyUnit());
        $writer->writeStringValue('name', $this->getName());
        $writer->writeFloatValue('rate', $this->getRate());
        $writer->writeDateTimeValue('recordCreateDate', $this->getRecordCreateDate());
        $writer->writeIntegerValue('recordDeleted', $this->getRecordDeleted());
        $writer->writeDateTimeValue('recordModifyDate', $this->getRecordModifyDate());
        $writer->writeStringValue('symbol', $this->getSymbol());
    }

    /**
     * Sets the baseCurrency property value. The baseCurrency property
     * @param int|null $value Value to set for the baseCurrency property.
    */
    public function setBaseCurrency(?int $value): void {
        $this->baseCurrency = $value;
    }

    /**
     * Sets the code property value. The code property
     * @param string|null $value Value to set for the code property.
    */
    public function setCode(?string $value): void {
        $this->code = $value;
    }

    /**
     * Sets the emuMember property value. The emuMember property
     * @param int|null $value Value to set for the emuMember property.
    */
    public function setEmuMember(?int $value): void {
        $this->emuMember = $value;
    }

    /**
     * Sets the majorCurrencyUnit property value. The majorCurrencyUnit property
     * @param string|null $value Value to set for the majorCurrencyUnit property.
    */
    public function setMajorCurrencyUnit(?string $value): void {
        $this->majorCurrencyUnit = $value;
    }

    /**
     * Sets the minorCurrencyUnit property value. The minorCurrencyUnit property
     * @param string|null $value Value to set for the minorCurrencyUnit property.
    */
    public function setMinorCurrencyUnit(?string $value): void {
        $this->minorCurrencyUnit = $value;
    }

    /**
     * Sets the name property value. The name property
     * @param string|null $value Value to set for the name property.
    */
    public function setName(?string $value): void {
        $this->name = $value;
    }

    /**
     * Sets the rate property value. The rate property
     * @param float|null $value Value to set for the rate property.
    */
    public function setRate(?float $value): void {
        $this->rate = $value;
    }

    /**
     * Sets the recordCreateDate property value. The recordCreateDate property
     * @param DateTime|null $value Value to set for the recordCreateDate property.
    */
    public function setRecordCreateDate(?DateTime $value): void {
        $this->recordCreateDate = $value;
    }

    /**
     * Sets the recordDeleted property value. The recordDeleted property
     * @param int|null $value Value to set for the recordDeleted property.
    */
    public function setRecordDeleted(?int $value): void {
        $this->recordDeleted = $value;
    }

    /**
     * Sets the recordModifyDate property value. The recordModifyDate property
     * @param DateTime|null $value Value to set for the recordModifyDate property.
    */
    public function setRecordModifyDate(?DateTime $value): void {
        $this->recordModifyDate = $value;
    }

    /**
     * Sets the symbol property value. The symbol property
     * @param string|null $value Value to set for the symbol property.
    */
    public function setSymbol(?string $value): void {
        $this->symbol = $value;
    }

}
