<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use DateTime;
use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class ProjectAttributesEdit implements Parsable
{
    /**
     * @var string|null $address1 The address1 property
    */
    private ?string $address1 = null;

    /**
     * @var string|null $address2 The address2 property
    */
    private ?string $address2 = null;

    /**
     * @var string|null $address3 The address3 property
    */
    private ?string $address3 = null;

    /**
     * @var string|null $address4 The address4 property
    */
    private ?string $address4 = null;

    /**
     * @var string|null $address5 The address5 property
    */
    private ?string $address5 = null;

    /**
     * @var string|null $analysis1 The analysis1 property
    */
    private ?string $analysis1 = null;

    /**
     * @var string|null $analysis2 The analysis2 property
    */
    private ?string $analysis2 = null;

    /**
     * @var string|null $analysis3 The analysis3 property
    */
    private ?string $analysis3 = null;

    /**
     * @var float|null $committedCost The committedCost property
    */
    private ?float $committedCost = null;

    /**
     * @var string|null $contactName The contactName property
    */
    private ?string $contactName = null;

    /**
     * @var string|null $countryCode The countryCode property
    */
    private ?string $countryCode = null;

    /**
     * @var string|null $description The description property
    */
    private ?string $description = null;

    /**
     * @var string|null $eMail The eMail property
    */
    private ?string $eMail = null;

    /**
     * @var DateTime|null $endDate The endDate property
    */
    private ?DateTime $endDate = null;

    /**
     * @var string|null $fax The fax property
    */
    private ?string $fax = null;

    /**
     * @var string|null $name The name property
    */
    private ?string $name = null;

    /**
     * @var string|null $orderNumber The orderNumber property
    */
    private ?string $orderNumber = null;

    /**
     * @var string|null $reference The reference property
    */
    private ?string $reference = null;

    /**
     * @var DateTime|null $startDate The startDate property
    */
    private ?DateTime $startDate = null;

    /**
     * @var string|null $telephone The telephone property
    */
    private ?string $telephone = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return ProjectAttributesEdit
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): ProjectAttributesEdit {
        return new ProjectAttributesEdit();
    }

    /**
     * Gets the address1 property value. The address1 property
     * @return string|null
    */
    public function getAddress1(): ?string {
        return $this->address1;
    }

    /**
     * Gets the address2 property value. The address2 property
     * @return string|null
    */
    public function getAddress2(): ?string {
        return $this->address2;
    }

    /**
     * Gets the address3 property value. The address3 property
     * @return string|null
    */
    public function getAddress3(): ?string {
        return $this->address3;
    }

    /**
     * Gets the address4 property value. The address4 property
     * @return string|null
    */
    public function getAddress4(): ?string {
        return $this->address4;
    }

    /**
     * Gets the address5 property value. The address5 property
     * @return string|null
    */
    public function getAddress5(): ?string {
        return $this->address5;
    }

    /**
     * Gets the analysis1 property value. The analysis1 property
     * @return string|null
    */
    public function getAnalysis1(): ?string {
        return $this->analysis1;
    }

    /**
     * Gets the analysis2 property value. The analysis2 property
     * @return string|null
    */
    public function getAnalysis2(): ?string {
        return $this->analysis2;
    }

    /**
     * Gets the analysis3 property value. The analysis3 property
     * @return string|null
    */
    public function getAnalysis3(): ?string {
        return $this->analysis3;
    }

    /**
     * Gets the committedCost property value. The committedCost property
     * @return float|null
    */
    public function getCommittedCost(): ?float {
        return $this->committedCost;
    }

    /**
     * Gets the contactName property value. The contactName property
     * @return string|null
    */
    public function getContactName(): ?string {
        return $this->contactName;
    }

    /**
     * Gets the countryCode property value. The countryCode property
     * @return string|null
    */
    public function getCountryCode(): ?string {
        return $this->countryCode;
    }

    /**
     * Gets the description property value. The description property
     * @return string|null
    */
    public function getDescription(): ?string {
        return $this->description;
    }

    /**
     * Gets the eMail property value. The eMail property
     * @return string|null
    */
    public function getEMail(): ?string {
        return $this->eMail;
    }

    /**
     * Gets the endDate property value. The endDate property
     * @return DateTime|null
    */
    public function getEndDate(): ?DateTime {
        return $this->endDate;
    }

    /**
     * Gets the fax property value. The fax property
     * @return string|null
    */
    public function getFax(): ?string {
        return $this->fax;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'address1' => fn(ParseNode $n) => $o->setAddress1($n->getStringValue()),
            'address2' => fn(ParseNode $n) => $o->setAddress2($n->getStringValue()),
            'address3' => fn(ParseNode $n) => $o->setAddress3($n->getStringValue()),
            'address4' => fn(ParseNode $n) => $o->setAddress4($n->getStringValue()),
            'address5' => fn(ParseNode $n) => $o->setAddress5($n->getStringValue()),
            'analysis1' => fn(ParseNode $n) => $o->setAnalysis1($n->getStringValue()),
            'analysis2' => fn(ParseNode $n) => $o->setAnalysis2($n->getStringValue()),
            'analysis3' => fn(ParseNode $n) => $o->setAnalysis3($n->getStringValue()),
            'committedCost' => fn(ParseNode $n) => $o->setCommittedCost($n->getFloatValue()),
            'contactName' => fn(ParseNode $n) => $o->setContactName($n->getStringValue()),
            'countryCode' => fn(ParseNode $n) => $o->setCountryCode($n->getStringValue()),
            'description' => fn(ParseNode $n) => $o->setDescription($n->getStringValue()),
            'eMail' => fn(ParseNode $n) => $o->setEMail($n->getStringValue()),
            'endDate' => fn(ParseNode $n) => $o->setEndDate($n->getDateTimeValue()),
            'fax' => fn(ParseNode $n) => $o->setFax($n->getStringValue()),
            'name' => fn(ParseNode $n) => $o->setName($n->getStringValue()),
            'orderNumber' => fn(ParseNode $n) => $o->setOrderNumber($n->getStringValue()),
            'reference' => fn(ParseNode $n) => $o->setReference($n->getStringValue()),
            'startDate' => fn(ParseNode $n) => $o->setStartDate($n->getDateTimeValue()),
            'telephone' => fn(ParseNode $n) => $o->setTelephone($n->getStringValue()),
        ];
    }

    /**
     * Gets the name property value. The name property
     * @return string|null
    */
    public function getName(): ?string {
        return $this->name;
    }

    /**
     * Gets the orderNumber property value. The orderNumber property
     * @return string|null
    */
    public function getOrderNumber(): ?string {
        return $this->orderNumber;
    }

    /**
     * Gets the reference property value. The reference property
     * @return string|null
    */
    public function getReference(): ?string {
        return $this->reference;
    }

    /**
     * Gets the startDate property value. The startDate property
     * @return DateTime|null
    */
    public function getStartDate(): ?DateTime {
        return $this->startDate;
    }

    /**
     * Gets the telephone property value. The telephone property
     * @return string|null
    */
    public function getTelephone(): ?string {
        return $this->telephone;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeStringValue('address1', $this->getAddress1());
        $writer->writeStringValue('address2', $this->getAddress2());
        $writer->writeStringValue('address3', $this->getAddress3());
        $writer->writeStringValue('address4', $this->getAddress4());
        $writer->writeStringValue('address5', $this->getAddress5());
        $writer->writeStringValue('analysis1', $this->getAnalysis1());
        $writer->writeStringValue('analysis2', $this->getAnalysis2());
        $writer->writeStringValue('analysis3', $this->getAnalysis3());
        $writer->writeFloatValue('committedCost', $this->getCommittedCost());
        $writer->writeStringValue('contactName', $this->getContactName());
        $writer->writeStringValue('countryCode', $this->getCountryCode());
        $writer->writeStringValue('description', $this->getDescription());
        $writer->writeStringValue('eMail', $this->getEMail());
        $writer->writeDateTimeValue('endDate', $this->getEndDate());
        $writer->writeStringValue('fax', $this->getFax());
        $writer->writeStringValue('name', $this->getName());
        $writer->writeStringValue('orderNumber', $this->getOrderNumber());
        $writer->writeStringValue('reference', $this->getReference());
        $writer->writeDateTimeValue('startDate', $this->getStartDate());
        $writer->writeStringValue('telephone', $this->getTelephone());
    }

    /**
     * Sets the address1 property value. The address1 property
     * @param string|null $value Value to set for the address1 property.
    */
    public function setAddress1(?string $value): void {
        $this->address1 = $value;
    }

    /**
     * Sets the address2 property value. The address2 property
     * @param string|null $value Value to set for the address2 property.
    */
    public function setAddress2(?string $value): void {
        $this->address2 = $value;
    }

    /**
     * Sets the address3 property value. The address3 property
     * @param string|null $value Value to set for the address3 property.
    */
    public function setAddress3(?string $value): void {
        $this->address3 = $value;
    }

    /**
     * Sets the address4 property value. The address4 property
     * @param string|null $value Value to set for the address4 property.
    */
    public function setAddress4(?string $value): void {
        $this->address4 = $value;
    }

    /**
     * Sets the address5 property value. The address5 property
     * @param string|null $value Value to set for the address5 property.
    */
    public function setAddress5(?string $value): void {
        $this->address5 = $value;
    }

    /**
     * Sets the analysis1 property value. The analysis1 property
     * @param string|null $value Value to set for the analysis1 property.
    */
    public function setAnalysis1(?string $value): void {
        $this->analysis1 = $value;
    }

    /**
     * Sets the analysis2 property value. The analysis2 property
     * @param string|null $value Value to set for the analysis2 property.
    */
    public function setAnalysis2(?string $value): void {
        $this->analysis2 = $value;
    }

    /**
     * Sets the analysis3 property value. The analysis3 property
     * @param string|null $value Value to set for the analysis3 property.
    */
    public function setAnalysis3(?string $value): void {
        $this->analysis3 = $value;
    }

    /**
     * Sets the committedCost property value. The committedCost property
     * @param float|null $value Value to set for the committedCost property.
    */
    public function setCommittedCost(?float $value): void {
        $this->committedCost = $value;
    }

    /**
     * Sets the contactName property value. The contactName property
     * @param string|null $value Value to set for the contactName property.
    */
    public function setContactName(?string $value): void {
        $this->contactName = $value;
    }

    /**
     * Sets the countryCode property value. The countryCode property
     * @param string|null $value Value to set for the countryCode property.
    */
    public function setCountryCode(?string $value): void {
        $this->countryCode = $value;
    }

    /**
     * Sets the description property value. The description property
     * @param string|null $value Value to set for the description property.
    */
    public function setDescription(?string $value): void {
        $this->description = $value;
    }

    /**
     * Sets the eMail property value. The eMail property
     * @param string|null $value Value to set for the eMail property.
    */
    public function setEMail(?string $value): void {
        $this->eMail = $value;
    }

    /**
     * Sets the endDate property value. The endDate property
     * @param DateTime|null $value Value to set for the endDate property.
    */
    public function setEndDate(?DateTime $value): void {
        $this->endDate = $value;
    }

    /**
     * Sets the fax property value. The fax property
     * @param string|null $value Value to set for the fax property.
    */
    public function setFax(?string $value): void {
        $this->fax = $value;
    }

    /**
     * Sets the name property value. The name property
     * @param string|null $value Value to set for the name property.
    */
    public function setName(?string $value): void {
        $this->name = $value;
    }

    /**
     * Sets the orderNumber property value. The orderNumber property
     * @param string|null $value Value to set for the orderNumber property.
    */
    public function setOrderNumber(?string $value): void {
        $this->orderNumber = $value;
    }

    /**
     * Sets the reference property value. The reference property
     * @param string|null $value Value to set for the reference property.
    */
    public function setReference(?string $value): void {
        $this->reference = $value;
    }

    /**
     * Sets the startDate property value. The startDate property
     * @param DateTime|null $value Value to set for the startDate property.
    */
    public function setStartDate(?DateTime $value): void {
        $this->startDate = $value;
    }

    /**
     * Sets the telephone property value. The telephone property
     * @param string|null $value Value to set for the telephone property.
    */
    public function setTelephone(?string $value): void {
        $this->telephone = $value;
    }

}
