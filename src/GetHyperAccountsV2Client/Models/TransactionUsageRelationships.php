<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class TransactionUsageRelationships implements Parsable
{
    /**
     * @var TransactionSplitRelatedRelationship|null $split The split property
    */
    private ?TransactionSplitRelatedRelationship $split = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return TransactionUsageRelationships
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): TransactionUsageRelationships {
        return new TransactionUsageRelationships();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'split' => fn(ParseNode $n) => $o->setSplit($n->getObjectValue([TransactionSplitRelatedRelationship::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the split property value. The split property
     * @return TransactionSplitRelatedRelationship|null
    */
    public function getSplit(): ?TransactionSplitRelatedRelationship {
        return $this->split;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('split', $this->getSplit());
    }

    /**
     * Sets the split property value. The split property
     * @param TransactionSplitRelatedRelationship|null $value Value to set for the split property.
    */
    public function setSplit(?TransactionSplitRelatedRelationship $value): void {
        $this->split = $value;
    }

}
