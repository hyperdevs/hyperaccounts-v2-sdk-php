<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class SalesOrderGetDto implements Parsable
{
    /**
     * @var SalesOrderAttributesRead|null $attributes The attributes property
    */
    private ?SalesOrderAttributesRead $attributes = null;

    /**
     * @var SalesOrderIncluded|null $included The included property
    */
    private ?SalesOrderIncluded $included = null;

    /**
     * @var ResourceLinks|null $links The links property
    */
    private ?ResourceLinks $links = null;

    /**
     * @var int|null $orderNumber The orderNumber property
    */
    private ?int $orderNumber = null;

    /**
     * @var SalesOrderRelationships|null $relationships The relationships property
    */
    private ?SalesOrderRelationships $relationships = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return SalesOrderGetDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): SalesOrderGetDto {
        return new SalesOrderGetDto();
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return SalesOrderAttributesRead|null
    */
    public function getAttributes(): ?SalesOrderAttributesRead {
        return $this->attributes;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([SalesOrderAttributesRead::class, 'createFromDiscriminatorValue'])),
            'included' => fn(ParseNode $n) => $o->setIncluded($n->getObjectValue([SalesOrderIncluded::class, 'createFromDiscriminatorValue'])),
            'links' => fn(ParseNode $n) => $o->setLinks($n->getObjectValue([ResourceLinks::class, 'createFromDiscriminatorValue'])),
            'orderNumber' => fn(ParseNode $n) => $o->setOrderNumber($n->getIntegerValue()),
            'relationships' => fn(ParseNode $n) => $o->setRelationships($n->getObjectValue([SalesOrderRelationships::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the included property value. The included property
     * @return SalesOrderIncluded|null
    */
    public function getIncluded(): ?SalesOrderIncluded {
        return $this->included;
    }

    /**
     * Gets the links property value. The links property
     * @return ResourceLinks|null
    */
    public function getLinks(): ?ResourceLinks {
        return $this->links;
    }

    /**
     * Gets the orderNumber property value. The orderNumber property
     * @return int|null
    */
    public function getOrderNumber(): ?int {
        return $this->orderNumber;
    }

    /**
     * Gets the relationships property value. The relationships property
     * @return SalesOrderRelationships|null
    */
    public function getRelationships(): ?SalesOrderRelationships {
        return $this->relationships;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('attributes', $this->getAttributes());
        $writer->writeObjectValue('included', $this->getIncluded());
        $writer->writeObjectValue('links', $this->getLinks());
        $writer->writeIntegerValue('orderNumber', $this->getOrderNumber());
        $writer->writeObjectValue('relationships', $this->getRelationships());
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param SalesOrderAttributesRead|null $value Value to set for the attributes property.
    */
    public function setAttributes(?SalesOrderAttributesRead $value): void {
        $this->attributes = $value;
    }

    /**
     * Sets the included property value. The included property
     * @param SalesOrderIncluded|null $value Value to set for the included property.
    */
    public function setIncluded(?SalesOrderIncluded $value): void {
        $this->included = $value;
    }

    /**
     * Sets the links property value. The links property
     * @param ResourceLinks|null $value Value to set for the links property.
    */
    public function setLinks(?ResourceLinks $value): void {
        $this->links = $value;
    }

    /**
     * Sets the orderNumber property value. The orderNumber property
     * @param int|null $value Value to set for the orderNumber property.
    */
    public function setOrderNumber(?int $value): void {
        $this->orderNumber = $value;
    }

    /**
     * Sets the relationships property value. The relationships property
     * @param SalesOrderRelationships|null $value Value to set for the relationships property.
    */
    public function setRelationships(?SalesOrderRelationships $value): void {
        $this->relationships = $value;
    }

}
