<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use DateTime;
use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class ChartOfAccountsCategoryAttributesRead implements Parsable
{
    /**
     * @var int|null $category The category property
    */
    private ?int $category = null;

    /**
     * @var int|null $flagAssetLiability The flagAssetLiability property
    */
    private ?int $flagAssetLiability = null;

    /**
     * @var string|null $high The high property
    */
    private ?string $high = null;

    /**
     * @var string|null $low The low property
    */
    private ?string $low = null;

    /**
     * @var string|null $name The name property
    */
    private ?string $name = null;

    /**
     * @var DateTime|null $recordCreateDate The recordCreateDate property
    */
    private ?DateTime $recordCreateDate = null;

    /**
     * @var int|null $recordDeleted The recordDeleted property
    */
    private ?int $recordDeleted = null;

    /**
     * @var DateTime|null $recordModifyDate The recordModifyDate property
    */
    private ?DateTime $recordModifyDate = null;

    /**
     * @var int|null $sortOrder The sortOrder property
    */
    private ?int $sortOrder = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return ChartOfAccountsCategoryAttributesRead
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): ChartOfAccountsCategoryAttributesRead {
        return new ChartOfAccountsCategoryAttributesRead();
    }

    /**
     * Gets the category property value. The category property
     * @return int|null
    */
    public function getCategory(): ?int {
        return $this->category;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'category' => fn(ParseNode $n) => $o->setCategory($n->getIntegerValue()),
            'flagAssetLiability' => fn(ParseNode $n) => $o->setFlagAssetLiability($n->getIntegerValue()),
            'high' => fn(ParseNode $n) => $o->setHigh($n->getStringValue()),
            'low' => fn(ParseNode $n) => $o->setLow($n->getStringValue()),
            'name' => fn(ParseNode $n) => $o->setName($n->getStringValue()),
            'recordCreateDate' => fn(ParseNode $n) => $o->setRecordCreateDate($n->getDateTimeValue()),
            'recordDeleted' => fn(ParseNode $n) => $o->setRecordDeleted($n->getIntegerValue()),
            'recordModifyDate' => fn(ParseNode $n) => $o->setRecordModifyDate($n->getDateTimeValue()),
            'sortOrder' => fn(ParseNode $n) => $o->setSortOrder($n->getIntegerValue()),
        ];
    }

    /**
     * Gets the flagAssetLiability property value. The flagAssetLiability property
     * @return int|null
    */
    public function getFlagAssetLiability(): ?int {
        return $this->flagAssetLiability;
    }

    /**
     * Gets the high property value. The high property
     * @return string|null
    */
    public function getHigh(): ?string {
        return $this->high;
    }

    /**
     * Gets the low property value. The low property
     * @return string|null
    */
    public function getLow(): ?string {
        return $this->low;
    }

    /**
     * Gets the name property value. The name property
     * @return string|null
    */
    public function getName(): ?string {
        return $this->name;
    }

    /**
     * Gets the recordCreateDate property value. The recordCreateDate property
     * @return DateTime|null
    */
    public function getRecordCreateDate(): ?DateTime {
        return $this->recordCreateDate;
    }

    /**
     * Gets the recordDeleted property value. The recordDeleted property
     * @return int|null
    */
    public function getRecordDeleted(): ?int {
        return $this->recordDeleted;
    }

    /**
     * Gets the recordModifyDate property value. The recordModifyDate property
     * @return DateTime|null
    */
    public function getRecordModifyDate(): ?DateTime {
        return $this->recordModifyDate;
    }

    /**
     * Gets the sortOrder property value. The sortOrder property
     * @return int|null
    */
    public function getSortOrder(): ?int {
        return $this->sortOrder;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeIntegerValue('category', $this->getCategory());
        $writer->writeIntegerValue('flagAssetLiability', $this->getFlagAssetLiability());
        $writer->writeStringValue('high', $this->getHigh());
        $writer->writeStringValue('low', $this->getLow());
        $writer->writeStringValue('name', $this->getName());
        $writer->writeDateTimeValue('recordCreateDate', $this->getRecordCreateDate());
        $writer->writeIntegerValue('recordDeleted', $this->getRecordDeleted());
        $writer->writeDateTimeValue('recordModifyDate', $this->getRecordModifyDate());
        $writer->writeIntegerValue('sortOrder', $this->getSortOrder());
    }

    /**
     * Sets the category property value. The category property
     * @param int|null $value Value to set for the category property.
    */
    public function setCategory(?int $value): void {
        $this->category = $value;
    }

    /**
     * Sets the flagAssetLiability property value. The flagAssetLiability property
     * @param int|null $value Value to set for the flagAssetLiability property.
    */
    public function setFlagAssetLiability(?int $value): void {
        $this->flagAssetLiability = $value;
    }

    /**
     * Sets the high property value. The high property
     * @param string|null $value Value to set for the high property.
    */
    public function setHigh(?string $value): void {
        $this->high = $value;
    }

    /**
     * Sets the low property value. The low property
     * @param string|null $value Value to set for the low property.
    */
    public function setLow(?string $value): void {
        $this->low = $value;
    }

    /**
     * Sets the name property value. The name property
     * @param string|null $value Value to set for the name property.
    */
    public function setName(?string $value): void {
        $this->name = $value;
    }

    /**
     * Sets the recordCreateDate property value. The recordCreateDate property
     * @param DateTime|null $value Value to set for the recordCreateDate property.
    */
    public function setRecordCreateDate(?DateTime $value): void {
        $this->recordCreateDate = $value;
    }

    /**
     * Sets the recordDeleted property value. The recordDeleted property
     * @param int|null $value Value to set for the recordDeleted property.
    */
    public function setRecordDeleted(?int $value): void {
        $this->recordDeleted = $value;
    }

    /**
     * Sets the recordModifyDate property value. The recordModifyDate property
     * @param DateTime|null $value Value to set for the recordModifyDate property.
    */
    public function setRecordModifyDate(?DateTime $value): void {
        $this->recordModifyDate = $value;
    }

    /**
     * Sets the sortOrder property value. The sortOrder property
     * @param int|null $value Value to set for the sortOrder property.
    */
    public function setSortOrder(?int $value): void {
        $this->sortOrder = $value;
    }

}
