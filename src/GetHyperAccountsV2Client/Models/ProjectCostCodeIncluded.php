<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class ProjectCostCodeIncluded implements Parsable
{
    /**
     * @var ProjectBudgetCollection|null $projectBudgets The projectBudgets property
    */
    private ?ProjectBudgetCollection $projectBudgets = null;

    /**
     * @var TransactionSplitCollection|null $splits The splits property
    */
    private ?TransactionSplitCollection $splits = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return ProjectCostCodeIncluded
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): ProjectCostCodeIncluded {
        return new ProjectCostCodeIncluded();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'projectBudgets' => fn(ParseNode $n) => $o->setProjectBudgets($n->getObjectValue([ProjectBudgetCollection::class, 'createFromDiscriminatorValue'])),
            'splits' => fn(ParseNode $n) => $o->setSplits($n->getObjectValue([TransactionSplitCollection::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the projectBudgets property value. The projectBudgets property
     * @return ProjectBudgetCollection|null
    */
    public function getProjectBudgets(): ?ProjectBudgetCollection {
        return $this->projectBudgets;
    }

    /**
     * Gets the splits property value. The splits property
     * @return TransactionSplitCollection|null
    */
    public function getSplits(): ?TransactionSplitCollection {
        return $this->splits;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('projectBudgets', $this->getProjectBudgets());
        $writer->writeObjectValue('splits', $this->getSplits());
    }

    /**
     * Sets the projectBudgets property value. The projectBudgets property
     * @param ProjectBudgetCollection|null $value Value to set for the projectBudgets property.
    */
    public function setProjectBudgets(?ProjectBudgetCollection $value): void {
        $this->projectBudgets = $value;
    }

    /**
     * Sets the splits property value. The splits property
     * @param TransactionSplitCollection|null $value Value to set for the splits property.
    */
    public function setSplits(?TransactionSplitCollection $value): void {
        $this->splits = $value;
    }

}
