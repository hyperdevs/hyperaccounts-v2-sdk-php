<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Enum;

class SalesInvoiceAttributesEdit_quoteStatus extends Enum {
    public const NOT_A_QUOTE = 'NotAQuote';
    public const OPEN = 'Open';
    public const WON = 'Won';
    public const LOST = 'Lost';
    public const EXPIRED = 'Expired';
}
