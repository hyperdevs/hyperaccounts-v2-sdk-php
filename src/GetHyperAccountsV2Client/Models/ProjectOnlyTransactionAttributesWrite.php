<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use DateTime;
use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class ProjectOnlyTransactionAttributesWrite implements Parsable
{
    /**
     * @var DateTime|null $date The date property
    */
    private ?DateTime $date = null;

    /**
     * @var string|null $details The details property
    */
    private ?string $details = null;

    /**
     * @var float|null $quantity The quantity property
    */
    private ?float $quantity = null;

    /**
     * @var float|null $rate The rate property
    */
    private ?float $rate = null;

    /**
     * @var string|null $reference The reference property
    */
    private ?string $reference = null;

    /**
     * @var ProjectOnlyTransactionAttributesWrite_type|null $type The type property
    */
    private ?ProjectOnlyTransactionAttributesWrite_type $type = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return ProjectOnlyTransactionAttributesWrite
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): ProjectOnlyTransactionAttributesWrite {
        return new ProjectOnlyTransactionAttributesWrite();
    }

    /**
     * Gets the date property value. The date property
     * @return DateTime|null
    */
    public function getDate(): ?DateTime {
        return $this->date;
    }

    /**
     * Gets the details property value. The details property
     * @return string|null
    */
    public function getDetails(): ?string {
        return $this->details;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'date' => fn(ParseNode $n) => $o->setDate($n->getDateTimeValue()),
            'details' => fn(ParseNode $n) => $o->setDetails($n->getStringValue()),
            'quantity' => fn(ParseNode $n) => $o->setQuantity($n->getFloatValue()),
            'rate' => fn(ParseNode $n) => $o->setRate($n->getFloatValue()),
            'reference' => fn(ParseNode $n) => $o->setReference($n->getStringValue()),
            'type' => fn(ParseNode $n) => $o->setType($n->getEnumValue(ProjectOnlyTransactionAttributesWrite_type::class)),
        ];
    }

    /**
     * Gets the quantity property value. The quantity property
     * @return float|null
    */
    public function getQuantity(): ?float {
        return $this->quantity;
    }

    /**
     * Gets the rate property value. The rate property
     * @return float|null
    */
    public function getRate(): ?float {
        return $this->rate;
    }

    /**
     * Gets the reference property value. The reference property
     * @return string|null
    */
    public function getReference(): ?string {
        return $this->reference;
    }

    /**
     * Gets the type property value. The type property
     * @return ProjectOnlyTransactionAttributesWrite_type|null
    */
    public function getType(): ?ProjectOnlyTransactionAttributesWrite_type {
        return $this->type;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeDateTimeValue('date', $this->getDate());
        $writer->writeStringValue('details', $this->getDetails());
        $writer->writeFloatValue('quantity', $this->getQuantity());
        $writer->writeFloatValue('rate', $this->getRate());
        $writer->writeStringValue('reference', $this->getReference());
        $writer->writeEnumValue('type', $this->getType());
    }

    /**
     * Sets the date property value. The date property
     * @param DateTime|null $value Value to set for the date property.
    */
    public function setDate(?DateTime $value): void {
        $this->date = $value;
    }

    /**
     * Sets the details property value. The details property
     * @param string|null $value Value to set for the details property.
    */
    public function setDetails(?string $value): void {
        $this->details = $value;
    }

    /**
     * Sets the quantity property value. The quantity property
     * @param float|null $value Value to set for the quantity property.
    */
    public function setQuantity(?float $value): void {
        $this->quantity = $value;
    }

    /**
     * Sets the rate property value. The rate property
     * @param float|null $value Value to set for the rate property.
    */
    public function setRate(?float $value): void {
        $this->rate = $value;
    }

    /**
     * Sets the reference property value. The reference property
     * @param string|null $value Value to set for the reference property.
    */
    public function setReference(?string $value): void {
        $this->reference = $value;
    }

    /**
     * Sets the type property value. The type property
     * @param ProjectOnlyTransactionAttributesWrite_type|null $value Value to set for the type property.
    */
    public function setType(?ProjectOnlyTransactionAttributesWrite_type $value): void {
        $this->type = $value;
    }

}
