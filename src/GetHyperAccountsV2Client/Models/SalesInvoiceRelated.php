<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class SalesInvoiceRelated implements Parsable
{
    /**
     * @var int|null $invoiceNumber The invoiceNumber property
    */
    private ?int $invoiceNumber = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return SalesInvoiceRelated
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): SalesInvoiceRelated {
        return new SalesInvoiceRelated();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'invoiceNumber' => fn(ParseNode $n) => $o->setInvoiceNumber($n->getIntegerValue()),
        ];
    }

    /**
     * Gets the invoiceNumber property value. The invoiceNumber property
     * @return int|null
    */
    public function getInvoiceNumber(): ?int {
        return $this->invoiceNumber;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeIntegerValue('invoiceNumber', $this->getInvoiceNumber());
    }

    /**
     * Sets the invoiceNumber property value. The invoiceNumber property
     * @param int|null $value Value to set for the invoiceNumber property.
    */
    public function setInvoiceNumber(?int $value): void {
        $this->invoiceNumber = $value;
    }

}
