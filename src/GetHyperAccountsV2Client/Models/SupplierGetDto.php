<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class SupplierGetDto implements Parsable
{
    /**
     * @var string|null $accountRef The accountRef property
    */
    private ?string $accountRef = null;

    /**
     * @var SupplierAttributesRead|null $attributes The attributes property
    */
    private ?SupplierAttributesRead $attributes = null;

    /**
     * @var SupplierIncluded|null $included The included property
    */
    private ?SupplierIncluded $included = null;

    /**
     * @var ResourceLinks|null $links The links property
    */
    private ?ResourceLinks $links = null;

    /**
     * @var SupplierRelationships|null $relationships The relationships property
    */
    private ?SupplierRelationships $relationships = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return SupplierGetDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): SupplierGetDto {
        return new SupplierGetDto();
    }

    /**
     * Gets the accountRef property value. The accountRef property
     * @return string|null
    */
    public function getAccountRef(): ?string {
        return $this->accountRef;
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return SupplierAttributesRead|null
    */
    public function getAttributes(): ?SupplierAttributesRead {
        return $this->attributes;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'accountRef' => fn(ParseNode $n) => $o->setAccountRef($n->getStringValue()),
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([SupplierAttributesRead::class, 'createFromDiscriminatorValue'])),
            'included' => fn(ParseNode $n) => $o->setIncluded($n->getObjectValue([SupplierIncluded::class, 'createFromDiscriminatorValue'])),
            'links' => fn(ParseNode $n) => $o->setLinks($n->getObjectValue([ResourceLinks::class, 'createFromDiscriminatorValue'])),
            'relationships' => fn(ParseNode $n) => $o->setRelationships($n->getObjectValue([SupplierRelationships::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the included property value. The included property
     * @return SupplierIncluded|null
    */
    public function getIncluded(): ?SupplierIncluded {
        return $this->included;
    }

    /**
     * Gets the links property value. The links property
     * @return ResourceLinks|null
    */
    public function getLinks(): ?ResourceLinks {
        return $this->links;
    }

    /**
     * Gets the relationships property value. The relationships property
     * @return SupplierRelationships|null
    */
    public function getRelationships(): ?SupplierRelationships {
        return $this->relationships;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeStringValue('accountRef', $this->getAccountRef());
        $writer->writeObjectValue('attributes', $this->getAttributes());
        $writer->writeObjectValue('included', $this->getIncluded());
        $writer->writeObjectValue('links', $this->getLinks());
        $writer->writeObjectValue('relationships', $this->getRelationships());
    }

    /**
     * Sets the accountRef property value. The accountRef property
     * @param string|null $value Value to set for the accountRef property.
    */
    public function setAccountRef(?string $value): void {
        $this->accountRef = $value;
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param SupplierAttributesRead|null $value Value to set for the attributes property.
    */
    public function setAttributes(?SupplierAttributesRead $value): void {
        $this->attributes = $value;
    }

    /**
     * Sets the included property value. The included property
     * @param SupplierIncluded|null $value Value to set for the included property.
    */
    public function setIncluded(?SupplierIncluded $value): void {
        $this->included = $value;
    }

    /**
     * Sets the links property value. The links property
     * @param ResourceLinks|null $value Value to set for the links property.
    */
    public function setLinks(?ResourceLinks $value): void {
        $this->links = $value;
    }

    /**
     * Sets the relationships property value. The relationships property
     * @param SupplierRelationships|null $value Value to set for the relationships property.
    */
    public function setRelationships(?SupplierRelationships $value): void {
        $this->relationships = $value;
    }

}
