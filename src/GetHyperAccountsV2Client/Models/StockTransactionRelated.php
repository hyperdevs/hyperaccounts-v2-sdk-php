<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class StockTransactionRelated implements Parsable
{
    /**
     * @var int|null $tranNumber The tranNumber property
    */
    private ?int $tranNumber = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return StockTransactionRelated
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): StockTransactionRelated {
        return new StockTransactionRelated();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'tranNumber' => fn(ParseNode $n) => $o->setTranNumber($n->getIntegerValue()),
        ];
    }

    /**
     * Gets the tranNumber property value. The tranNumber property
     * @return int|null
    */
    public function getTranNumber(): ?int {
        return $this->tranNumber;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeIntegerValue('tranNumber', $this->getTranNumber());
    }

    /**
     * Sets the tranNumber property value. The tranNumber property
     * @param int|null $value Value to set for the tranNumber property.
    */
    public function setTranNumber(?int $value): void {
        $this->tranNumber = $value;
    }

}
