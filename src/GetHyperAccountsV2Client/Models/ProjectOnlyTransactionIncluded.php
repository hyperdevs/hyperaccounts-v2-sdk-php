<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class ProjectOnlyTransactionIncluded implements Parsable
{
    /**
     * @var ProjectResourceGetDto|null $projectResource The projectResource property
    */
    private ?ProjectResourceGetDto $projectResource = null;

    /**
     * @var ProjectTranGetDto|null $projectTran The projectTran property
    */
    private ?ProjectTranGetDto $projectTran = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return ProjectOnlyTransactionIncluded
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): ProjectOnlyTransactionIncluded {
        return new ProjectOnlyTransactionIncluded();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'projectResource' => fn(ParseNode $n) => $o->setProjectResource($n->getObjectValue([ProjectResourceGetDto::class, 'createFromDiscriminatorValue'])),
            'projectTran' => fn(ParseNode $n) => $o->setProjectTran($n->getObjectValue([ProjectTranGetDto::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the projectResource property value. The projectResource property
     * @return ProjectResourceGetDto|null
    */
    public function getProjectResource(): ?ProjectResourceGetDto {
        return $this->projectResource;
    }

    /**
     * Gets the projectTran property value. The projectTran property
     * @return ProjectTranGetDto|null
    */
    public function getProjectTran(): ?ProjectTranGetDto {
        return $this->projectTran;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('projectResource', $this->getProjectResource());
        $writer->writeObjectValue('projectTran', $this->getProjectTran());
    }

    /**
     * Sets the projectResource property value. The projectResource property
     * @param ProjectResourceGetDto|null $value Value to set for the projectResource property.
    */
    public function setProjectResource(?ProjectResourceGetDto $value): void {
        $this->projectResource = $value;
    }

    /**
     * Sets the projectTran property value. The projectTran property
     * @param ProjectTranGetDto|null $value Value to set for the projectTran property.
    */
    public function setProjectTran(?ProjectTranGetDto $value): void {
        $this->projectTran = $value;
    }

}
