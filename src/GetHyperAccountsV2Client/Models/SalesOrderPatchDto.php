<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class SalesOrderPatchDto implements Parsable
{
    /**
     * @var SalesOrderAttributesEdit|null $attributes The attributes property
    */
    private ?SalesOrderAttributesEdit $attributes = null;

    /**
     * @var SalesOrderRelationshipsEdit|null $relationships The relationships property
    */
    private ?SalesOrderRelationshipsEdit $relationships = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return SalesOrderPatchDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): SalesOrderPatchDto {
        return new SalesOrderPatchDto();
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return SalesOrderAttributesEdit|null
    */
    public function getAttributes(): ?SalesOrderAttributesEdit {
        return $this->attributes;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([SalesOrderAttributesEdit::class, 'createFromDiscriminatorValue'])),
            'relationships' => fn(ParseNode $n) => $o->setRelationships($n->getObjectValue([SalesOrderRelationshipsEdit::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the relationships property value. The relationships property
     * @return SalesOrderRelationshipsEdit|null
    */
    public function getRelationships(): ?SalesOrderRelationshipsEdit {
        return $this->relationships;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('attributes', $this->getAttributes());
        $writer->writeObjectValue('relationships', $this->getRelationships());
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param SalesOrderAttributesEdit|null $value Value to set for the attributes property.
    */
    public function setAttributes(?SalesOrderAttributesEdit $value): void {
        $this->attributes = $value;
    }

    /**
     * Sets the relationships property value. The relationships property
     * @param SalesOrderRelationshipsEdit|null $value Value to set for the relationships property.
    */
    public function setRelationships(?SalesOrderRelationshipsEdit $value): void {
        $this->relationships = $value;
    }

}
