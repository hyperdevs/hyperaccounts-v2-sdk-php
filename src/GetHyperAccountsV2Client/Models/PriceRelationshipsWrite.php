<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class PriceRelationshipsWrite implements Parsable
{
    /**
     * @var StockRelatedRequiredRelationship|null $stock The stock property
    */
    private ?StockRelatedRequiredRelationship $stock = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return PriceRelationshipsWrite
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): PriceRelationshipsWrite {
        return new PriceRelationshipsWrite();
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'stock' => fn(ParseNode $n) => $o->setStock($n->getObjectValue([StockRelatedRequiredRelationship::class, 'createFromDiscriminatorValue'])),
        ];
    }

    /**
     * Gets the stock property value. The stock property
     * @return StockRelatedRequiredRelationship|null
    */
    public function getStock(): ?StockRelatedRequiredRelationship {
        return $this->stock;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('stock', $this->getStock());
    }

    /**
     * Sets the stock property value. The stock property
     * @param StockRelatedRequiredRelationship|null $value Value to set for the stock property.
    */
    public function setStock(?StockRelatedRequiredRelationship $value): void {
        $this->stock = $value;
    }

}
