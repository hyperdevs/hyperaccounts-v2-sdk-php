<?php

namespace HyperAccountsV2Sdk\GetHyperAccountsV2Client\Models;

use Microsoft\Kiota\Abstractions\Serialization\Parsable;
use Microsoft\Kiota\Abstractions\Serialization\ParseNode;
use Microsoft\Kiota\Abstractions\Serialization\SerializationWriter;

class CurrencyGetDto implements Parsable
{
    /**
     * @var CurrencyAttributesRead|null $attributes The attributes property
    */
    private ?CurrencyAttributesRead $attributes = null;

    /**
     * @var ResourceLinks|null $links The links property
    */
    private ?ResourceLinks $links = null;

    /**
     * @var int|null $number The number property
    */
    private ?int $number = null;

    /**
     * Creates a new instance of the appropriate class based on discriminator value
     * @param ParseNode $parseNode The parse node to use to read the discriminator value and create the object
     * @return CurrencyGetDto
    */
    public static function createFromDiscriminatorValue(ParseNode $parseNode): CurrencyGetDto {
        return new CurrencyGetDto();
    }

    /**
     * Gets the attributes property value. The attributes property
     * @return CurrencyAttributesRead|null
    */
    public function getAttributes(): ?CurrencyAttributesRead {
        return $this->attributes;
    }

    /**
     * The deserialization information for the current model
     * @return array<string, callable>
    */
    public function getFieldDeserializers(): array {
        $o = $this;
        return  [
            'attributes' => fn(ParseNode $n) => $o->setAttributes($n->getObjectValue([CurrencyAttributesRead::class, 'createFromDiscriminatorValue'])),
            'links' => fn(ParseNode $n) => $o->setLinks($n->getObjectValue([ResourceLinks::class, 'createFromDiscriminatorValue'])),
            'number' => fn(ParseNode $n) => $o->setNumber($n->getIntegerValue()),
        ];
    }

    /**
     * Gets the links property value. The links property
     * @return ResourceLinks|null
    */
    public function getLinks(): ?ResourceLinks {
        return $this->links;
    }

    /**
     * Gets the number property value. The number property
     * @return int|null
    */
    public function getNumber(): ?int {
        return $this->number;
    }

    /**
     * Serializes information the current object
     * @param SerializationWriter $writer Serialization writer to use to serialize this model
    */
    public function serialize(SerializationWriter $writer): void {
        $writer->writeObjectValue('attributes', $this->getAttributes());
        $writer->writeObjectValue('links', $this->getLinks());
        $writer->writeIntegerValue('number', $this->getNumber());
    }

    /**
     * Sets the attributes property value. The attributes property
     * @param CurrencyAttributesRead|null $value Value to set for the attributes property.
    */
    public function setAttributes(?CurrencyAttributesRead $value): void {
        $this->attributes = $value;
    }

    /**
     * Sets the links property value. The links property
     * @param ResourceLinks|null $value Value to set for the links property.
    */
    public function setLinks(?ResourceLinks $value): void {
        $this->links = $value;
    }

    /**
     * Sets the number property value. The number property
     * @param int|null $value Value to set for the number property.
    */
    public function setNumber(?int $value): void {
        $this->number = $value;
    }

}
