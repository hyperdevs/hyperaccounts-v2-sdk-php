<?php
namespace HyperAccountsV2Sdk\Authentication;

use Http\Promise\FulfilledPromise;
use Http\Promise\Promise;
use Microsoft\Kiota\Abstractions\RequestInformation;

class AuthenticationProvider implements \Microsoft\Kiota\Abstractions\Authentication\AuthenticationProvider
{
    private string $username;
    private string $password;

    public function __construct(string $username, string $password)
    {
        $this->username = $username;
        $this->password = $password;
    }

    public function authenticateRequest(RequestInformation $request, array $additionalAuthenticationContext = []): Promise
    {
        $encoded = base64_encode(iconv("UTF-8", "ISO-8859-1", $this->username. ":" . $this->password));
        $request->addHeader('Authorization','basic ' . $encoded);

        return new FulfilledPromise(null);
    }
}
